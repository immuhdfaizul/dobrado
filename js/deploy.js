// @source: /js/source/deploy.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

(function(){'use strict';$(function(){$('#deploy-action').button().click(function(){action($('#deploy-progress > .current').attr('id'));});});function action(current){if(!current){console.log('Error: current action not set.');return false;}
$('#deploy-action').button('option','disabled',true);let data={action:current};if(current==='deploy-database-config'&&$('#deploy-database-form').length===1){data.action='save-database-config';data.server=$('#deploy-database-server').val();data.username=$('#deploy-database-username').val();data.password=$('#deploy-database-password').val();data.name=$('#deploy-database-name').val();}
else if(current==='deploy-admin-account'&&$('#deploy-account-form').length===1){data.action='save-admin-account';data.username=$('#deploy-account-username').val();data.password=$('#deploy-account-password').val();data.email=$('#deploy-account-email').val();}
$.post(location.href,data,function(response){$('#deploy-action').button('option','disabled',false);var info=JSON.parse(response);if(info.error){$('#'+current+' .info').html(info.error);$('#'+current).addClass('error');if(current==='deploy-database-config'){$('#deploy-database-form .default-action').click(function(){action(current);return false;});}
else if(current==='deploy-admin-account'){$('#deploy-account-form .default-action').click(function(){action(current);return false;});}
return;}
if(info.content){$('#'+current+' .info').html(info.content);}
if(info.next){$(info.next).show();$(info.next).addClass('current');$('#'+current).addClass('completed');$('#'+current).removeClass('current error');if(info.next==='#deploy-finished'){$('#deploy-action').button('option','label','Login');}
else{$('#deploy-action').button('option','label','Continue');}}
else if(current==='deploy-transfer'){$('#deploy-transfer').addClass('completed');$('#deploy-transfer').removeClass('current error');action('start-transfer');}
else if(current==='deploy-database-config'){$('#deploy-database-form .default-action').click(function(){action(current);return false;});}
else if(current==='deploy-admin-account'){$('#deploy-account-form .default-action').click(function(){action(current);return false;});}
else if(current==='deploy-finished'){location.href=info.location;}});}}());