/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.account) {
  dobrado.account = {};
}
(function() {

  'use strict';

  // This flag is exposed so that other modules can set it. 
  dobrado.account.preventLogout = false;
  var userExists = false;
  var currentUser = '';
  var newSupplier = false;

  $(function() {
    $('.account').dialog({
      show: true,
      autoOpen: false,
      width: 550,
      height: 500,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Manage Accounts',
      create: dobrado.fixedDialog });
  });

  function logout() {

    function createCookie(name, value, days) {
      var expires = '';

      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toGMTString();
      }
      document.cookie = name + '=' + value + expires + '; path=/';
    }

    function deleteCookie(name) {
      createCookie(name, '', -1);
    }

    if (dobrado.account.preventLogout) {
      alert('Please save data on this page before logging out.');
      return;
    }

    var user = dobrado.readCookie('user');
    if (user && user.substring(0,5) === 'guest' && user.length === 20 &&
        !confirm('Are you sure you want to log out? This is a guest ' +
                 'account, you will lose all your changes.')) {
      return;
    }

    dobrado.log('Logging out...', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'logout',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'logout')) {
          return false;
        }
        if (dobrado.localStorage) {
          localStorage.clear();
        }
        deleteCookie('user');
        deleteCookie('PHPSESSID');
        location.href = '/';
      });
  }

  function changePassword() {
    var oldPassword = $('#change-password-old').val();
    var newPassword = $('#change-password-new').val();
    var repeatPassword = $('#change-password-rpt').val();

    var message = '#change-password-form-message';
    $(message).html('');
    if (oldPassword === '') {
      $(message).html('Please enter your old password.');
    }
    else if (newPassword === '') {
      $(message).html('Please enter your new password.');
    }
    else if (repeatPassword === '') {
      $(message).html('Please repeat your new password.');
    }
    else if (newPassword !== repeatPassword) {
      $(message).html('The new passwords do not match');
    }
    else {
      dobrado.log('Changing password.', 'info');
      $.post('/php/request.php',
             { request: 'account', action: 'change-password',
               oldPassword: oldPassword, newPassword: newPassword,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'changePassword')) {
            return false;
          }
          var account = JSON.parse(response);
          if (account) {
            $('#change-password-form-message').html(account.content);
            if (account.updated) {
              setTimeout(function() {
                $('.account').dialog('close');
              }, 2000);
            }
          }
      });
    }
    return false;
  }

  function changeEmail() {
    var password = $('#change-email-password-input').val();
    dobrado.log('Changing email.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'change-email',
             email: $('#change-email-input').val(), password: password,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'changeEmail')) {
          return false;
        }
        var account = JSON.parse(response);
        if (account) {
          $('#change-email-form-message').html(account.content);
          if (account.updated) {
            setTimeout(function() {
              $('.account').dialog('close');
            }, 2000);
          }
        }
    });
    return false;
  }

  function checkUsernameEnter(event) {
    if (event.keyCode !== 13) {
      return;
    }
    event.preventDefault();
    checkUsername();
    // checkUsername will be called again when the new-user-input field loses
    // focus, so track the input value here so that a duplicate request is
    // not made.
    currentUser = $('#new-user-input').val();
  }

  function checkUsername() {
    var newUser = $('#new-user-input').val();
    if (newUser !== '' && newUser === currentUser) {
      currentUser = '';
      return;
    }

    $('#new-password-input').val('');
    $('#email-input').val('');
    $('#system-group-input').val('');
    $('#register-form .new-user-confirm').hide();
    $('#new-user-confirm-input').prop('checked', false);
    $('#active-input').prop('checked', true);
    $('#new-details-first-input').val('');
    $('#new-details-last-input').val('');
    $('#new-details-phone-input').val('');
    $('#new-details-address-textarea').val('');
    $('#new-details-description-textarea').val('');
    $('#new-details-display-input').prop('checked', true);
    $('#register-form .stock-supply-options').html('');

    if (newUser === '') {
      $('#register-form .username-info').html('');
      return;
    }

    $('#register-form .username-info').html('<i>Checking username...</i>');
    $.post('/php/request.php',
           { request: 'account', action: 'check-username', newUser: newUser,
             supplier: newSupplier, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'checkUsername')) {
          return false;
        }
        userExists = false;
        var account = JSON.parse(response);
        if (account.exists) {
          if (account.canUpdate) {
            userExists = true;
          }
          $('#register-form .username-info').html(account.message);
          $('#register-form #email-input').val(dobrado.decode(account.email));
          $('#register-form #system-group-input').val(account.group);
          if (!account.confirmed) {
            $('#register-form .new-user-confirm').show();
          }
          $('#register-form #active-input').prop('checked', account.active);
          $('#register-form #new-details-first-input').
            val(dobrado.decode(account.first));
          $('#register-form #new-details-last-input').
            val(dobrado.decode(account.last));
          $('#register-form #new-details-phone-input').
            val(dobrado.decode(account.phone));
          $('#register-form #new-details-address-textarea').
            val(dobrado.decode(account.address));
          $('#register-form #new-details-description-textarea').
            val(dobrado.decode(account.description));
          $('#register-form #new-details-display-input').
            prop('checked', account.display);
          if (account.reminderTime) {
            $('#register-form #new-details-reminder-time').
              datepicker('setDate', dobrado.formatDate(account.reminderTime));
          }
          else {
            $('#register-form #new-details-reminder-time').val('');
          }
          $('#register-form #new-details-reminder-repeat').
            val(account.reminderRepeat);
          if (account.stock) {
            $('#register-form .stock-supply-options').html(account.stock);
            var addSupplyGroupButton = '#register-form .stock-add-supply-group';
            $(addSupplyGroupButton).button().click(addSupplyGroup);
            var removeSupplyGroupButton = '#register-form .remove-stock-group';
            $(removeSupplyGroupButton).button({
              icon: 'ui-icon-closethick', showLabel: false }).
              click(removeSupplyGroup);
          }
        }
        else {
          $('#register-form .username-info').html('<i>This username is ' +
                                                  'available.</i>');
        }
      });
  }

  function updateUser() {
    var newUser = $('#new-user-input').val();
    if (newUser === '') {
      return false;
    }

    var message = '';
    if (userExists) {
      message = 'Updating user details.';
      newSupplier = false;
    }
    else {
      message = 'Adding new user.';
    }
    var first = $('#new-details-first-input').val();
    if (newSupplier) {
      // There is only one input field when creating supplier accounts, so use
      // the given input as the descriptive name in case it gets modified when
      // the account is created.
      first = newUser;
    }
    dobrado.log(message, 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'add-user', newUser: newUser,
             newPassword: $('#new-password-input').val(),
             email: $('#email-input').val(),
             group: $('#system-group-input').val(),
             confirmed: $('#new-user-confirm-input:checked').length,
             active: $('#active-input:checked').length,
             first: first,
             last: $('#new-details-last-input').val(),
             phone: $('#new-details-phone-input').val(),
             address: $('#new-details-address-textarea').val(),
             description: $('#new-details-description-textarea').val(),
             display: $('#new-details-display-input:checked').length,
             reminderTime: $('#new-details-reminder-time').val(),
             reminderRepeat: $('#new-details-reminder-repeat').val(),
             supplier: newSupplier, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'updateUser')) {
          return false;
        }
        var account = JSON.parse(response);
        $('.account').dialog('close');
        $('#register-form .username-info').html('');
        if (newSupplier && dobrado.stock) {
          dobrado.stock.addUser(account.name);
          newSupplier = false;
        }
      });
    return false;
  }

  function removeUser() {
    dobrado.log('Removing user', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'remove-user-submit',
             removeUser: $('#remove-user-input').val(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'removeUser')) {
          return false;
        }
        $('.account').dialog('close');
      });
    return false;
  }

  function showGroup(group) {
    dobrado.log('Loading group members.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'show-group', group: group,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'showGroup')) {
          return false;
        }
        var account = JSON.parse(response);
        $('.account .group-user-list').html(account.content);
        $('.account .remove-group-member').button({
          icon: 'ui-icon-closethick',
          showLabel: false
        }).click(removeGroupMember);
    });
  }

  function selectGroup(event, ui) {
    showGroup(ui.item.value);
  }

  function loadGroup(event) {
    if (event.keyCode !== 13) {
      return;
    }
    event.preventDefault();
    showGroup($(this).val());
  }

  function addGroupMember() {
    var group = $('#group-input').val();
    if (group === '') {
      dobrado.log('Please enter a group name.', 'error');
      return false;
    }
    dobrado.log('Adding user to group.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'add-group-member',
             visitor: $('#user-group-input').val(),
             edit: $('#edit-group-input:checked').length,
             group: group, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'addGroupMember')) {
          return false;
        }
        $('#user-group-input').val('');
        var account = JSON.parse(response);
        $('#group-input').autocomplete('option', 'source', account.groups);
        $('.account .group-user-list').html(account.content);
        $('.account .remove-group-member').button({
          icon: 'ui-icon-closethick',
          showLabel: false
        }).click(removeGroupMember);
    });
    return false;
  }

  function removeGroupMember() {
    var group = $('#group-input').val();
    if (group === '') {
      dobrado.log('Please enter a group name.', 'error');
      return false;
    }
    dobrado.log('Removing group member.', 'info');
    var that = this;
    $.post('/php/request.php',
           { request: 'account', action: 'remove-group-member',
             visitor: $(this).siblings('.visitor').html(),
             group: group, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'removeGroupMember')) {
          return false;
        }
        $(that).parent().remove();
    });
    return false;
  }

  function userPermission() {
    dobrado.log('Adding user permission.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'user-permission',
             visitor: $('#user-permission-input').val(),
             page: $('#user-page-input').val(),
             edit: $('#user-edit-input:checked').length,
             copy: $('#user-copy-input:checked').length,
             view: $('#user-view-input:checked').length,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'userPermission')) {
          return false;
        }
        // Reset the form.
        $('#user-permission-input').val('');
        $('#user-page-input').val('');
        $('#user-edit-input').prop('checked', false);
        $('#user-copy-input').prop('checked', false);
        $('#user-view-input').prop('checked', false);

        var account = JSON.parse(response);
        $('.account .user-permission-list').html(account.content);
        $('.account .remove-user-permission').button({
          icon: 'ui-icon-closethick',
          showLabel: false
        }).click(removeUserPermission);
    });
    return false;
  }

  function groupPermission() {
    dobrado.log('Adding group permission.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'group-permission',
             group: $('#group-permission-input').val(),
             page: $('#group-page-input').val(),
             edit: $('#group-edit-input:checked').length,
             copy: $('#group-copy-input:checked').length,
             view: $('#group-view-input:checked').length,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupPermission')) {
          return false;
        }
        // Reset the form.
        $('#group-permission-input').val('');
        $('#group-page-input').val('');
        $('#group-edit-input').prop('checked', false);
        $('#group-copy-input').prop('checked', false);
        $('#group-view-input').prop('checked', false);

        var account = JSON.parse(response);
        $('.account .group-permission-list').html(account.content);
        $('.account .remove-group-permission').button({
          icon: 'ui-icon-closethick',
          showLabel: false
      }).click(removeGroupPermission);
    });
    return false;
  }

  function removeUserPermission() {
    dobrado.log('Removing user permission.', 'info');
    var that = this;
    $.post('/php/request.php',
           { request: 'account', action: 'remove-user-permission',
             visitor: $(this).siblings('.visitor').html(),
             page: $(this).siblings('.page').html(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'removeUserPermission')) {
          return false;
        }
        $(that).parent().remove();
    });
  }

  function removeGroupPermission() {
    dobrado.log('Removing group permission.', 'info');
    var that = this;
    $.post('/php/request.php',
           { request: 'account', action: 'remove-group-permission',
             group: $(this).siblings('.group').html(),
             page: $(this).siblings('.page').html(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'removeGroupPermission')) {
          return false;
        }
        $(that).parent().remove();
    });
  }

  function addSupplyGroup() {
    var newUser = $('#new-user-input').val();
    if (newUser === '') {
      return false;
    }

    var listed = false;
    var group = $('#stock-select-supply-group').val();
    // If the selected group is already listed don't do anything.
    $('#register-form .stock-supply-group').each(function() {
        if ($(this).html() === group) {
          listed = true;
        }
    });
    if (listed) {
      return false;
    }

    dobrado.log('Adding supply group.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'add-supply-group', newUser: newUser,
             group: group, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'addSupplyGroup')) {
          return false;
        }
        var account = JSON.parse(response);
        $('#register-form .stock-all-groups').remove();
        $('#register-form .stock-current-groups').append(account.content);
        var removeSupplyGroupButton = '#register-form .remove-stock-group';
        $(removeSupplyGroupButton).button({
          icon: 'ui-icon-closethick',
          showLabel: false
        }).click(removeSupplyGroup);
    });
    return false;
  }

  function removeSupplyGroup() {
    var newUser = $('#new-user-input').val();
    if (newUser === '') {
      return false;
    }
    
    var that = this;
    var group = $(this).siblings('.stock-supply-group').html();
    dobrado.log('Removing supply group.', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'remove-supply-group',
             newUser: newUser, group: group,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'removeSupplyGroup')) {
          return false;
        }
        $(that).parent().remove();        
    });
    return false;
  }

  dobrado.account.registerSupplier = function(event) {
    event.preventDefault();
    dobrado.log('Loading add supplier dialog...', 'info');
    $.post('/php/request.php',
           { request: 'account', action: 'register-supplier',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'registerSupplier')) {
          return;
        }
        var account = JSON.parse(response);
        newSupplier = true;
        $('.account').dialog('option', 'title', 'Add Supplier');
        $('.account').dialog('option', 'height', 350);
        $('.account').dialog('open');
        $('.account').html(account.content);
        $('#register-form .submit').button().click(updateUser);
      });
    return false;
  };

  dobrado.account.option = function(event) {

    event.preventDefault();
    var option = $(this).attr('id');
    if (!option) {
      // Allow the group wizard module to open the preferences dialog.
      option = 'preferences';
    }

    if (option === 'add-start') {
      dobrado.createModule('', 'start', '');
    }
    else if (option === 'logout') {
      logout();
    }
    else if (option === 'preferences' ||
             option === 'register' ||
             option === 'remove-user') {
      dobrado.log('Loading ' + option + ' dialog...', 'info');
      $.post('/php/request.php',
             { request: 'account', action: option,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'account.option')) {
            return;
          }
          var account = JSON.parse(response);
          $('.account').html(account.content);

          if (option === 'register') {
            newSupplier = false;
            $('#new-user-input').change(checkUsername);
            $('#new-user-input').keypress(checkUsernameEnter);
            $('#account-email-info').click(function() {
              $('.account-email-check').toggle();
              return false;
            });
            $('#account-set-password').button().click(function() {
              $('#account-set-password').hide();
              $('.new-password-wrapper').show();
              return false;
            });
            $('#register-form .submit').button().click(updateUser);
            $('#new-details-reminder-time').val('').datepicker({
              dateFormat: dobrado.dateFormat });
            $('.account').dialog('option', 'title', 'Manage Accounts');
            $('.account').dialog('option', 'height', 500);
          }
          else if (option === 'remove-user') {
            $('#remove-user-form .submit').button().click(removeUser);
            $('.account').dialog('option', 'title', 'Remove Account');
            $('.account').dialog('option', 'height', 200);
          }
          else if (option === 'preferences') {
            $('#account-tabs').tabs();

            // Settings tab.
            $('#change-password-form .submit').button().click(changePassword);
            $('#change-email-form .submit').button().click(changeEmail);

            // Groups tab.
            $('#group-input').autocomplete({ minLength: 1,
                                             search: dobrado.fixAutoCompleteMemoryLeak,
                                             source: account.groups,
                                             select: selectGroup });
            $('#group-input').keypress(loadGroup);
            $('#account-groups .show-members').button().click(function() {
              showGroup($('#group-input').val());
            });
            $('#user-group-form .submit').button().click(addGroupMember);
            var tooltip = 'Current list:<br>';
            $.each(account.groups, function(i, item) {
              if (item === '') {
                tooltip += '<i>(empty string)</i><br>';
              }
              else {
                tooltip += item + '<br>';
              }
            });
            $('label[for=group-input]').tooltip({ content: tooltip });
            $('#user-group-input').autocomplete({ minLength: 1,
                                                  search: dobrado.fixAutoCompleteMemoryLeak,
                                                  source: account.users });

            // Permission tab.
            $('#user-permission-form .submit').button().click(userPermission);
            $('#group-permission-form .submit').button().click(groupPermission);
            $('.account .remove-user-permission').button({
              icon: 'ui-icon-closethick',
              showLabel: false
            }).click(removeUserPermission);
            $('.account .remove-group-permission').button({
              icon: 'ui-icon-closethick',
              showLabel: false
            }).click(removeGroupPermission);
            $('.account').dialog('option', 'title', 'Account Preferences');
            $('.account').dialog('option', 'height', 500);
          }
          $('.account').dialog('open');
      });
    }
  };

}());
