// @source: /js/source/core.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2020 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

(function(){'use strict';dobrado.arrowBackgroundColor='';dobrado.arrowBorderColor='';dobrado.editMode=false;dobrado.layoutMode=false;dobrado.editorLoading=false;dobrado.editor=null;dobrado.editors={};dobrado.permalink='';dobrado.oldPermalink='';dobrado.moduleSettings={};var sortable={};var duplicate=function(){var prev_id,prev_placement,prev_order;return function(id,placement,order){if(id===prev_id&&placement===prev_placement&&order===prev_order){return true;}
else{prev_id=id;prev_placement=placement;prev_order=order;return false;}};}();$(function(){$.post('/php/init.php',{url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'initialisation')){return;}
var attributes=JSON.parse(response);if(attributes.clear&&dobrado.localStorage){localStorage.clear();}
if(attributes.reload){location.reload(true);return;}
if(!attributes.locked){$.each(attributes.list,function(){$(this.id).data('label',this.label);});}
if(attributes.settings){dobrado.moduleSettings=attributes.settings;$.each(dobrado.moduleSettings,function(module,settings){if(dobrado[module]&&dobrado[module].settingsCallback){if($('.'+module).length!==0){dobrado[module].settingsCallback(settings);}}});}
if(attributes.tooltip){dobrado.tooltip(attributes.tooltip.selector,attributes.tooltip.content,attributes.tooltip.arrow);}
if(dobrado.localStorage&&!localStorage.indieConfig){localStorage.indieConfig=JSON.stringify({status:attributes.status});}});$('#page-input').autocomplete({minLength:2,search:dobrado.fixAutoCompleteMemoryLeak,select:changePage,source:pageSearch});CKEDITOR.disableAutoInline=true;let nickname='/php/autocomplete.php?type=nickname&match={encodedQuery}';let search='/php/autocomplete.php?type=search&match={encodedQuery}';let item='<li data-id="{id}"><img src="{photo}"> '+'<a href="{url}" class="nickname">{name}</a> '+'<span class="fullname">{fullname}</span></li>';let output='<a href="{url}">{name}</a> ';CKEDITOR.config.mentions=[{feed:nickname,marker:'@',minChars:1,itemTemplate:item,outputTemplate:output},{feed:search,marker:'#',minChars:1,outputTemplate:output}];var widget=$('<div></div>').addClass('ui-widget-content').hide().appendTo('body');dobrado.arrowBackgroundColor=$('.ui-widget-content').css('background-color');dobrado.arrowBorderColor=$('.ui-widget-content').css('border-bottom-color');widget.remove();$('.logged-in-display-none').css('display','none');if(!dobrado.mobile){$('.sortable').sortable({disabled:true,start:startSort,stop:stopSort,update:updateLayoutPosition,connectWith:'.sortable',placeholder:'ui-sortable-placeholder',opacity:0.6});}});function changePage(event,ui){if(ui){dobrado.changePage(ui.item.value);}}
function pageSearch(request,callback){$.post('/php/search.php',{term:request.term,token:dobrado.token},function(response){callback(JSON.parse(response));});}
function updateLayoutPosition(event,ui){var id='';var order='';var placement='';var previous=0;$(ui.item).prevAll().each(function(){if($(this).attr('id')){previous++;}
else{previous+=$(this).children().length;}});if($(ui.item).attr('id')){id='#'+$(ui.item).attr('id');placement=$(ui.item).parent().attr('class').match(/^\S+/)[0];order=previous;}
else{var thisId='';var thisOrder=0;$(ui.item).children().each(function(){if($(this).attr('id')){thisId='#'+$(this).attr('id');if(placement===''){placement=$(ui.item).parent().attr('class').match(/^\S+/)[0];}
if(id!==''){id+=',';}
id+=thisId;thisOrder=previous+$(thisId).prevAll().length;if(order!==''){order+=',';}
order+=thisOrder;}});}
if(duplicate(id,placement,order)){return;}
$.post('/php/layout.php',{id:id,placement:placement,box_order:order,url:location.href,token:dobrado.token},function(response){dobrado.checkResponseError(response,'layout');});}
function startSort(event,ui){sortable={maxHeight:$(ui.item).css('max-height'),overflow:$(ui.item).css('overflow')};$(ui.item).css('max-height','50px');$(ui.item).css('overflow','hidden');$('.sortable').sortable('option','forcePlaceholderSize',true);}
function stopSort(event,ui){$(ui.item).css('max-height',sortable.maxHeight);$(ui.item).css('overflow',sortable.overflow);$('.sortable').sortable('option','forcePlaceholderSize',false);}
dobrado.tooltip=function(selector,content,arrow){if(!selector||!content)return;setTimeout(function(){if(!arrow)arrow='10px';var background={top:'-17px',left:arrow,borderColor:'transparent transparent '+
dobrado.arrowBackgroundColor+' transparent'};var border={top:'-20px',position:'absolute',marginLeft:'0',left:arrow,borderColor:'transparent transparent '+
dobrado.arrowBorderColor+' transparent'};$(selector).tooltip({position:{my:'left top+5',at:'left bottom',using:function(position,feedback){$(this).css(position);$('<div>').addClass('arrow').css(background).prependTo(this);$('<div>').addClass('arrow-border').css(border).prependTo(this);$(this).click(function(){$(selector).tooltip('close');});}}});$(selector).attr('title',content).tooltip('open');$(selector).keydown(function(){$(selector).tooltip('close');});},5000);};dobrado.changePage=function(page){var user=dobrado.readCookie('user');if(!user){dobrado.log('Error: User unknown','error');return;}
if(/^[a-z0-9\/_-]+$/i.test(page)){dobrado.log('Changing page...','info');var fields=page.match(/^([^\/]+)\/?(.*)$/);if(fields&&fields.length===3){if(fields[2]===''){if(user==='admin'){location.href='/index.php?page='+fields[1];}
else{location.href='/'+user+'/index.php?page='+fields[1];}}
else{if(fields[1]==='admin'){location.href='/index.php?page='+fields[2];}
else{location.href='/'+fields[1]+'/index.php?page='+fields[2];}}}}
else{dobrado.log('Invalid page name.','error');}};dobrado.ckeditor=function(response){if(dobrado.checkResponseError(response,'ckeditor')){return;}
var editor=JSON.parse(response);dobrado.createModule('browser','browser',editor.callback);};dobrado.createModule=function(callbackModule,label,context){function style(rules){for(var selector in rules){for(var property in rules[selector]){var value=rules[selector][property];$(selector).css(property,value);}}}
if(dobrado.more){$('.more').dialog('close');}
if(label!=='post'){dobrado.log('Adding module...','info');}
$.post('/php/add.php',{label:label,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'add')){return;}
var module=JSON.parse(response);var placement='.main .'+module.placement;if(module.placement==='outside'){placement='body';}
if(module.group){var wrapperClass=module.group;if(module.content===false){wrapperClass+=' hidden';}
if($(placement+' .'+module.group).length===0){$('<div></div>').addClass(wrapperClass).appendTo(placement);}
placement+=' .'+module.group;}
var className=module.label;if(module.content===false){className+=' hidden';}
var selector='#'+module.id;if(module.id==='dobrado-0'){$('<div>'+module.content+'</div>').addClass(className).prependTo(placement);selector='.'+module.label;}
else{$('<div>'+module.content+'</div>').attr({id:module.id}).addClass(className).prependTo(placement);$(selector).data('label',module.label);}
style(module.style);if(dobrado.editMode){var element=$(selector+' .dobrado-editable').get(0);if(element){dobrado.inlineEditor(element);}}
if(dobrado.layoutMode){$('.sortable > *').css('cursor','move');}
if(module.script){$.getScript('/js/'+module.script,function(){if(dobrado[callbackModule]&&dobrado[callbackModule].newModuleCallback){dobrado[callbackModule].newModuleCallback(selector,context);}});}
else if(dobrado[callbackModule]&&dobrado[callbackModule].newModuleCallback){dobrado[callbackModule].newModuleCallback(selector,context);}});};dobrado.addModule=function(event){event.preventDefault();dobrado.createModule('',$(this).attr('id'),'');};dobrado.inlineEditor=function(element){function setCurrent(event){dobrado.closeEditor();dobrado.current=dobrado.editors[event.editor.id];$.post('/php/request.php',{id:dobrado.current,request:$(dobrado.current).data('label'),mode:'box',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'setCurrent')){return;}
var module=JSON.parse(response);event.editor.setData(module.source);event.editor.resetDirty();});}
var id='';var label='';$(element).parents('div').each(function(){if(/^dobrado-/.test($(this).attr('id'))){id='#'+$(this).attr('id');label=$(this).data('label');return false;}});if(dobrado[label]&&dobrado[label].showEditor){return;}
$(element).addClass('border').attr('contenteditable',true);var editor=CKEDITOR.inline(element,{allowedContent:true,disableNativeSpellChecker:false,enterMode:CKEDITOR.ENTER_BR,extraPlugins:'extended',filebrowserBrowseUrl:'/php/browse.php',on:{blur:dobrado.save,focus:setCurrent},removePlugins:'elementspath,tableselection,tabletools,contextmenu,liststyle',toolbar:[['Undo','Redo','-','Bold','Italic','-','Link','Unlink','-','EmojiPanel','Image','Extended']]});dobrado.editors[editor.id]=id;};dobrado.select=function(){$(this).select();return false;};dobrado.clear=function(){$(this).val('');return false;};dobrado.closeEditor=function(saveContent,customChange){var current=dobrado.current;var content={};var editorChange=false;if(saveContent){content=saveContent;}
if(dobrado.editor){content.data=dobrado.editor.getData();if(current&&dobrado.editor.checkDirty()){editorChange=true;var editable=$(current+' .dobrado-editable');if(editable.prop('tagName')==='TEXTAREA'){editable.val(content.data);}
else{editable.html(content.data);}}
dobrado.editor.destroy();dobrado.editor=null;dobrado.current='';}
var label=$(current).data('label');if(dobrado[label]&&dobrado[label].showEditor){if(!dobrado.editMode){dobrado[label].showEditor();}
return;}
if(label==='post'&&content.data){editorChange=true;}
if(editorChange||customChange){dobrado.log('Saving content...','info');$.post('/php/content.php',{id:current,label:label,content:JSON.stringify(content),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'content')){return;}
if(label==='commenteditor'){return;}
var formatted=JSON.parse(response);if(formatted.html){$(current).html(formatted.html);$(current).show();if(dobrado.editMode){var element=$(current+' .dobrado-editable').get(0);if(element){dobrado.inlineEditor(element);}}}
if(customChange&&label!=='post'){location.reload();return;}
if(label==='post'){$.post('/php/request.php',{request:'post',action:'updated',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'post updated')){return;}
var post=JSON.parse(response);if(post.status){setTimeout(function(){dobrado.log(post.status,'info');setTimeout(function(){$('.control .info').hide();},10000);},2000);}});if(customChange){var exp=new RegExp(dobrado.oldPermalink+'$');if(dobrado.permalink!==''&&dobrado.oldPermalink!==''&&dobrado.permalink!==dobrado.oldPermalink&&exp.test(location.href)){let owner='admin';let regex=/\/\/[^\/]+\/([^\/]+)\/?(.*)$/;let fields=location.href.match(regex);if(fields&&fields.length===3){if(fields[2]!==''){owner=fields[1];}}
if(owner==='admin'){location.href='/index.php?page='+dobrado.permalink;}
else{location.href='/'+owner+'/index.php?page='+
dobrado.permalink;}}}
if(dobrado.writer){dobrado.writer.showEditor();}}});}
else{$.each(CKEDITOR.instances,function(){this.fire('blur');});}};dobrado.removeModule=function(id,done){var label=$(id).data('label');$.post('/php/remove.php',{id:id,label:label,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'remove')){return;}
var remove=JSON.parse(response);if(remove.done){$(id).remove();if(done){done();}
if(label==='post'){$.post('/php/request.php',{request:'post',action:'updated',url:location.href,token:dobrado.token},function(response){dobrado.checkResponseError(response,'remove post update');});}}});};dobrado.readCookie=function(name){var exp=new RegExp(name+'=([^;]+)');var fields=document.cookie.match(exp);if(fields&&fields.length===2){return fields[1];}
return null;};dobrado.home=function(){var user=dobrado.readCookie('user');if(user){if(user==='admin'){location.href='/';}
else{location.href='/'+user;}}
else{dobrado.log('Error: User unknown','error');}};dobrado.toggleMenu=function(name){$('.'+name).toggle();};dobrado.hideOtherMenu=function(name){$('.menu-wrapper').each(function(){if(!$(this).hasClass(name)){$(this).hide();}});};dobrado.formatDate=function(year,month,day){if(year&&month&&day){return $.datepicker.formatDate(dobrado.dateFormat,new Date(year,month-1,day));}
if(year){return $.datepicker.formatDate(dobrado.dateFormat,new Date(parseInt(year,10)));}
return $.datepicker.formatDate(dobrado.dateFormat,new Date());};dobrado.fixAutoCompleteMemoryLeak=function(event,ui){$(this).data('ui-autocomplete').menu.bindings=$();};}());