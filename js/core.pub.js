// @source: /js/source/core.pub.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado){var dobrado={};}
(function(){'use strict';dobrado.current='';dobrado.dateFormat='d M yy';dobrado.dynamicPolling=true;dobrado.timer=2000;var ignoreResponse=false;$(function(){dobrado.mobile=$('.dobrado-mobile').is(':visible');dobrado.token=$('.dobrado-token').attr('id');$('.horizontal-menu').menu({position:{my:'left top',at:'left bottom'}});$('.horizontal-menu').show();if($('indie-action').length!==0&&$('.reader').length===0){dobrado.indieConfig();}});dobrado.checkResponseError=function(response,server){if($('.control .info').is(':visible')){setTimeout(function(){$('.control .info').hide();},1000);}
else{$('.control .info').hide();}
if(ignoreResponse){return true;}
if(!response){dobrado.log('No response to \''+server+'\' call.','error');return true;}
var reply=JSON.parse(response);if(!reply){dobrado.log('Cannot interpret response to \''+server+'\' call.','error');return true;}
if(reply.error){dobrado.log(reply.error,'error');if(reply.error==='Session expired: reloading page.'){location.reload();}
return true;}
if(server!=='notify'){dobrado.timer=2000;}
if(server==='logout'){ignoreResponse=true;}
return false;};dobrado.decode=function(html){if(typeof html==='string'){return html.replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&quot;/g,'"').replace(/&#039;/g,"'");}
return'';};dobrado.encode=function(data){if(typeof data==='string'){return data.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');}
return'';};dobrado.fixedDialog=function(event,ui){$(event.target).parent().css('position','fixed');};dobrado.indieConfig=function(){var indieConfigInfo='';var indieConfigFollowInfo='';function currentSettings(config){if($('#indie-config-dialog').length===0){var text='<div>Your currently stored settings:'+'<p class="indie-config-settings">';if(config.follow){text+='<b>follow</b>: '+config.follow.split('&follow')[0]+'<br>';}
if(config.like){text+='<b>like</b>: '+config.like.split('&like')[0]+'<br>';}
if(config.post){text+='<b>post</b>: '+config.post.split('&post')[0]+'<br>';}
if(config.repost){text+='<b>repost</b>: '+config.repost.split('&repost')[0]+'<br>';}
if(config.reply){text+='<b>reply</b>: '+config.reply.split('&reply')[0]+'<br>';}
if(config.status){text+='<b>status</b>: '+config.status.split('?{url}')[0]+'<br>';}
if(config.tip){text+='<b>tip</b>: '+config.tip.split('&tip')[0]+'<br>';}
text+='</p><button id="indie-config-remove">remove settings'+'</button><button id="indie-config-ok">ok</button></div>';$(text).attr('id','indie-config-dialog').appendTo('body');$('#indie-config-dialog').dialog({show:true,width:500,position:{my:'top',at:'top+50',of:window},title:'Web Action Settings',create:dobrado.fixedDialog});$('#indie-config-ok').button().click(function(){$('#indie-config-dialog').dialog('close');});$('#indie-config-remove').button().click(function(){if(dobrado.localStorage&&localStorage.indieConfig){localStorage.indieConfig='';}
$('indie-action').hide();$('.indie-config-info').html(indieConfigInfo).show();$('.indie-config-follow-info').html(indieConfigFollowInfo).show();$('#indie-config-dialog').dialog('close');$('#indie-config-dialog').remove();});}
else{$('#indie-config-dialog').dialog('open');}}
function actionSet(config){return config.follow||config.like||config.post||config.repost||config.reply||config.tip;}
function handleConfig(){function hrefConfig(action){if(action==='follow'&&config.follow){return config.follow;}
if(action==='like'&&config.like){return config.like;}
if(action==='post'&&config.post){return config.post;}
if(action==='repost'&&config.repost){return config.repost;}
if(action==='reply'&&config.reply){return config.reply;}
if(action==='tip'&&config.tip){return config.tip;}
return'';}
function createCORSRequest(url){if(location.href.substring(0,5)!==url.substring(0,5)){return null;}
var xhr=new XMLHttpRequest();if('withCredentials'in xhr){xhr.open('GET',url,true);}
else if(typeof XDomainRequest!=='undefined'){xhr=new XDomainRequest();xhr.open('GET',url);}
else{xhr=null;}
return xhr;}
function updateActions(state){$('indie-action').each(function(){var action=$(this).attr('do');var target=$(this).attr('with')||window.location.href;if((reader||config[action])&&state[target]&&state[target][action]){$(this).children('a').attr('href',state[target][action]).addClass('highlight');if(action==='follow'){var text=$(this).children('a').text();text=text.replace(/^follow\s/,'following ');$(this).children('a').text(text);$(this).siblings('.indie-config-follow-info').hide();}
else{$(this).siblings('.indie-config-info').hide();}}});}
var config=JSON.parse(localStorage.indieConfig);if(!config){return;}
var reader=$('.reader').length!==0&&$('#writer-content').length!==0;var statusQuery={};var targetList={};$('indie-action').each(function(){if($(this).data('checked')){return true;}
var action=$(this).attr('do');var href=hrefConfig(action);var status='';var url='';var target=$(this).attr('with')||window.location.href;if(!reader&&href!==''){href=href.replace('{url}',encodeURIComponent(target));$(this).children('a').attr('href',href);$(this).css('display','inline');if(action==='follow'){$(this).siblings('.indie-config-follow-info').hide();}
else{$(this).siblings('.indie-config-info').hide();}}
else{$(this).data('checked',true);}
$(this).children('a').removeClass('highlight');if(config[action+'-status']){status=action+'-status';}
else if(config.status){status='status';}
if(status!==''){if(!targetList[status]){targetList[status]=[];}
if($.inArray(target,targetList[status])===-1){targetList[status].push(target);url='url[]='+encodeURIComponent(target);if(statusQuery[status]){statusQuery[status]+='&'+url;}
else{statusQuery[status]=url;}}}});$.each(statusQuery,function(status,query){var href=config[status].replace('{url}',query);var xhr=createCORSRequest(href);if(xhr){xhr.onload=function(){if(xhr.responseText){var state=JSON.parse(xhr.responseText);updateActions(state);}};xhr.withCredentials=true;xhr.send();}});}
function openDialog(){var config=null;if(dobrado.localStorage&&localStorage.indieConfig){config=JSON.parse(localStorage.indieConfig);}
if(config&&actionSet(config)){return;}
$('.indie-config-info').html(indieConfigInfo).show();$('.indie-config-follow-info').html(indieConfigFollowInfo).show();$('indie-action').each(function(){$(this).data('checked',false);});var text='<div><p>This page displays <b>web actions</b> that '+'<i>link back to your own website</i>, so that you can own your '+'content.</p>'+'<p>If you have a website that handles web actions, you can provide '+'an address to find your web action config here:'+'<form><div class="form-spacing">'+'<label for="indie-action-handler">Address:</label>'+'<input id="indie-action-handler" type="text">'+'<button id="indie-action-submit"">submit</button>'+'</div><span id="indie-action-info"></span></form></p>'+'<p>Instead of using your own site, you can also use Quill, Twitter '+'or Facebook for web actions by clicking one of the buttons below.'+'</p>'+'<button id="indie-action-quill">Quill</button>'+'<button id="indie-action-twitter">Twitter</button>'+'<button id="indie-action-facebook">Facebook</button></div>';if($('#indie-config-dialog').length===0){$(text).attr('id','indie-config-dialog').appendTo('body');}
$('#indie-config-dialog').dialog({show:true,width:500,position:{my:'top',at:'top+50',of:window},title:'Web Actions',create:dobrado.fixedDialog});$('#indie-action-submit').button().click(function(){$('#indie-action-info').html('Checking...');$.post('/php/webaction.php',{url:$('#indie-action-handler').val(),token:dobrado.token},function(response){if(actionSet(response)){localStorage.indieConfig=JSON.stringify(response);handleConfig();$('#indie-action-info').html('<b>config found.</b>');setTimeout(function(){$('#indie-config-dialog').dialog('close');$('#indie-config-dialog').remove();},2000);}
else{$('#indie-action-info').html('<i>config was not found using '+'the address given.</i>');}});return false;});$('#indie-action-quill').button().click(function(){localStorage.indieConfig=JSON.stringify({like:'https://quill.p3k.io/favorite?url={url}',repost:'https://quill.p3k.io/repost?url={url}',reply:'https://quill.p3k.io/new?reply={url}',});$('#indie-config-dialog').dialog('close');$('#indie-config-dialog').remove();handleConfig();});$('#indie-action-twitter').button().click(function(){localStorage.indieConfig=JSON.stringify({repost:'https://twitter.com/intent/tweet?url={url}'});$('#indie-config-dialog').dialog('close');$('#indie-config-dialog').remove();handleConfig();});$('#indie-action-facebook').button().click(function(){localStorage.indieConfig=JSON.stringify({repost:'https://www.facebook.com/sharer/sharer.php?u={url}'});$('#indie-config-dialog').dialog('close');$('#indie-config-dialog').remove();handleConfig();});}
function webActionSettings(){var config=null;if(dobrado.localStorage&&localStorage.indieConfig){config=JSON.parse(localStorage.indieConfig);}
if(config&&actionSet(config)){currentSettings(config);}
else{openDialog();}
return false;}
$('.indie-config').click(webActionSettings);indieConfigInfo=$('.indie-config-info').html();indieConfigFollowInfo=$('.indie-config-follow-info').html();if(dobrado.localStorage&&localStorage.indieConfig){handleConfig();}
if($('.reader').length!==0&&$('#writer-content').length!==0){$('indie-action').css('display','inline');}};dobrado.localStorage=(function(){try{var tmp='tmp';localStorage.setItem(tmp,tmp);localStorage.removeItem(tmp);return true;}catch(e){return false;}}());dobrado.log=function(message,status){if($('.control .'+status).length===0){if(console){console.log(message);}}
else{$('.control .'+status+' .message').html(message).parent().show();if(dobrado.moduleSettings&&dobrado.moduleSettings.control&&dobrado.moduleSettings.control.position==='scroll'){$('.control .'+status).position({my:'top',at:'top+5',of:window});}
else{$('.control .'+status).position({my:'top',at:'top+5',of:$('.control')});}
if(status==='error'){setTimeout(function(){$('.control .error').hide();},5000);}}};dobrado.moduleID=function(element){var id='';$(element).parents('div').each(function(){if(/^dobrado-/.test($(this).attr('id'))){id='#'+$(this).attr('id');return false;}});return id;};dobrado.notify=(function(){var notifications=[];var track=[];var timestamp=0;var timer=2000;function checkNotifications(){var updateRequired=false;if(dobrado.dynamicPolling&&timer<dobrado.timer){timer+=2000;setTimeout(checkNotifications,2000);return;}
timer=2000;if(track.length===0){setTimeout(checkNotifications,2000);return;}
$.post('/php/notify.php',{url:location.href,track:JSON.stringify(track),timestamp:timestamp,token:dobrado.token},function(response){if(!response){dobrado.log('Network failure please reload the page.','info');return;}
var notify=JSON.parse(response);timestamp=notify.timestamp;$.each(notify.track,function(action,updated){if(updated){for(var i=0;i<notifications.length;i++){var module=notifications[i];if(module.action===action){module.callback(action);}}
updateRequired=true;}});if(updateRequired){dobrado.timer=2000;}
else{if(dobrado.timer<240000){dobrado.timer*=2;}}
setTimeout(checkNotifications,2000);});}
setTimeout(checkNotifications,2000);return function(action,callback){if($.inArray(action,track)===-1){track.push(action);}
notifications.push({action:action,callback:callback});};}());dobrado.save=function(event){if(dobrado.current&&event.editor.checkDirty()){var label=$(dobrado.current).data('label');if(dobrado[label]&&dobrado[label].showEditor){return;}
dobrado.log('Saving content...','info');var editable=$(dobrado.current+' .dobrado-editable');var content={data:event.editor.getData()};if(editable.prop('tagName')==='TEXTAREA'){editable.val(content.data);}
else{editable.html(content.data);}
$.post('/php/content.php',{id:dobrado.current,label:label,content:JSON.stringify(content),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'content')){return;}
if(label==='post'){$.post('/php/request.php',{request:'post',action:'updated',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'save post update')){return;}});}});event.editor.resetDirty();}};}());