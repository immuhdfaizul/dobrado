// @source: /js/source/dobrado.organiser.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.organiser){dobrado.organiser={};}
(function(){'use strict';$(function(){if($('.organiser').length===0){return;}
$('#organiser-member-form .submit').button().click(addOrganisationMember);$('#organiser-contact-form .submit').button().click(addOrganisationContact);$('#organiser-email-type-input').val('none-selected').change(changeType);$('#organiser-email-form .add').button().click(addRecipient);$('#organiser-email-form .submit').button().click(sendMessage);$('#organiser-email-subject-input').val('');$('#organiser-email-username-input').val('');$('#organiser-email-to-input').val('');$('#organiser-email-textarea').val('').click(function(){if(!$('.extended').dialog('isOpen')){dobrado.organiser.showEditor();}});dobrado.organiser.showEditor();$.post('/php/request.php',{request:'organiser',action:'show-organisations',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'show organisations')){return;}
var organiser=JSON.parse(response);$('#organisation-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:organiser.list,select:selectOrganisation});$('#organisation-input').keypress(loadOrganisation);var tooltip='Current list:<br>';$.each(organiser.list,function(i,item){if(item===''){tooltip+='<i>(empty string)</i><br>';}
else{tooltip+=item+'<br>';}});$('label[for=organisation-input]').tooltip({content:tooltip});});});function selectOrganisation(event,ui){showOrganisation(ui.item.value);}
function loadOrganisation(event){if(event.keyCode!==13){return;}
event.preventDefault();showOrganisation($(this).val());}
function showOrganisation(organisation){dobrado.log('Loading organisation members.','info');$.post('/php/request.php',{request:'organiser',organisation:organisation,action:'show-members',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser show members')){return;}
var organiser=JSON.parse(response);$('.organisation-member-list').html(organiser.members);$('#organiser-member-input').val('');$('.remove-organisation-member').button({icon:'ui-icon-closethick',showLabel:false}).click(removeOrganisationMember);$('.organisation-contact-list').html(organiser.contacts);$('#organiser-contact-name-input').val('');$('#organiser-contact-user-input').val('');$('.remove-organisation-contact').button({icon:'ui-icon-closethick',showLabel:false}).click(removeOrganisationContact);});}
function addOrganisationMember(){var organisation=$('#organisation-input').val();dobrado.log('Adding organisation member.','info');$.post('/php/request.php',{request:'organiser',action:'add-member',member:$('#organiser-member-input').val(),organisation:organisation,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser add member')){return false;}
$('#organiser-member-input').val('');var organiser=JSON.parse(response);$('#organisation-input').autocomplete('option','source',organiser.list);$('.organisation-member-list').html(organiser.members);$('.remove-organisation-member').button({icon:'ui-icon-closethick',showLabel:false}).click(removeOrganisationMember);});return false;}
function removeOrganisationMember(){var organisation=$('#organisation-input').val();dobrado.log('Removing organisation member.','info');var that=this;$.post('/php/request.php',{request:'organiser',action:'remove-member',member:$(this).siblings('.member').html(),organisation:organisation,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser remove member')){return false;}
$(that).parent().remove();});return false;}
function addOrganisationContact(){var organisation=$('#organisation-input').val();var contact=$('#organiser-contact-name-input').val();var username=$('#organiser-contact-user-input').val();dobrado.log('Adding organisation contact.','info');$.post('/php/request.php',{request:'organiser',action:'add-contact',contact:contact,username:username,organisation:organisation,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser add contact')){return false;}
$('#organiser-contact-name-input').val('');$('#organiser-contact-user-input').val('');var organiser=JSON.parse(response);$('#organisation-input').autocomplete('option','source',organiser.list);$('.organisation-contact-list').html(organiser.contacts);$('.remove-organisation-contact').button({icon:'ui-icon-closethick',showLabel:false}).click(removeOrganisationContact);});return false;}
function removeOrganisationContact(){var organisation=$('#organisation-input').val();var fields=$(this).siblings('.contact').html().match(/^([^:]*):\s([^@]*)@/);if(!fields||fields.length!==3){return false;}
var contact=fields[1];var username=fields[2];dobrado.log('Removing organisation contact.','info');var that=this;$.post('/php/request.php',{request:'organiser',action:'remove-contact',contact:contact,username:username,organisation:organisation,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser remove contact')){return false;}
$(that).parent().remove();});return false;}
function addRecipient(){var current=$('#organiser-email-to-input').val();var next=$('#organiser-email-username-input').val();if(current!==''){current+=', ';}
$('#organiser-email-to-input').val(current+next);$('#organiser-email-username-input').val('');return false;}
function changeType(){var value=$(this).val();if(value==='none-selected'){$('#organiser-email-form .recipients').hide();$('#organiser-email-to-input').val('');}
else if(value==='individual'){$('#organiser-email-form .recipients').show();$('#organiser-email-to-input').val('');$.post('/php/request.php',{request:'organiser',action:'list-users',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser list users')){return false;}
var organiser=JSON.parse(response);$('#organiser-email-username-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:organiser.list});});}
else{$('#organiser-email-form .recipients').hide();$('#organiser-email-to-input').val('Loading...');$.post('/php/request.php',{request:'organiser',action:'list-group',group:value,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser list group')){$('#organiser-email-to-input').val('');return false;}
var organiser=JSON.parse(response);$('#organiser-email-to-input').val(organiser.list);});}}
function sendMessage(){var recipients=$('#organiser-email-to-input').val();var subject=$('#organiser-email-subject-input').val();var message='';var info='';if(dobrado.editor){message=dobrado.editor.getData();}
else{message=$('#organiser-email-textarea').val();}
if(recipients===''){info='Please add a recipient for the email.';$('#organiser-email-form .info').html(info);return false;}
if(subject===''){info='Please add a subject for the email.';$('#organiser-email-form .info').html(info);return false;}
if(message===''){info='Please add a message for the email.';$('#organiser-email-form .info').html(info);return false;}
$.post('/php/request.php',{request:'organiser',action:'send-message',recipients:recipients,subject:subject,message:message,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'organiser send message')){return false;}
if(dobrado.editor){dobrado.editor.setData('');}
else{$('#organiser-email-textarea').val('');}
$('#organiser-email-form .info').html('Message sent.');});return false;}
dobrado.organiser.showEditor=function(){if(dobrado.editMode){dobrado.closeEditor();}
$('#organiser-email-textarea').parents('div').each(function(){if(/^dobrado-/.test($(this).attr('id'))){dobrado.current='#'+$(this).attr('id');return false;}});$(dobrado.current).data('label','organiser');CKEDITOR.addCss('.cke_editable { margin: 10px; }');dobrado.editor=CKEDITOR.replace('organiser-email-textarea',{allowedContent:true,autoGrow_minHeight:$('#organiser-email-textarea').height(),autoGrow_onStartup:true,disableNativeSpellChecker:false,enterMode:CKEDITOR.ENTER_BR,extraPlugins:'extended',filebrowserBrowseUrl:'/php/browse.php',removePlugins:'elementspath,tableselection,tabletools,contextmenu,liststyle',resize_enabled:false,toolbar:[['Undo','Redo','-','Bold','Italic','-','Link','Unlink','-','EmojiPanel','Image','Extended']]});};}());