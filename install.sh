#!/bin/bash

minify_js() {
  # Parameters are:
  # $1 = source path (either "install" or "js")
  # $2 = the source file name (dobrado.label.source.js for installable modules,
  #      and just dobrado.label.js for default modules)
  # $3 = the path to the minified file ("install" again for installable modules,
  #      and "source" for default modules)
  # $4 = the minified file name
  ./jsmin < $1/$2 > $3/$4 \
      "@source: /js/source/$4" \
      "" \
      "@licstart The following is the entire license notice" \
      "for the JavaScript code in this page." \
      "" \
      "Copyright (C) 2020 Malcolm Blaney" \
      "" \
      "This program is free software: you can redistribute it and/or modify" \
      "it under the terms of the GNU Affero General Public License as" \
      "published by the Free Software Foundation, either version 3 of the" \
      "License, or (at your option) any later version." \
      "" \
      "This program is distributed in the hope that it will be useful," \
      "but WITHOUT ANY WARRANTY; without even the implied warranty of" \
      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" \
      "GNU Affero General Public License for more details." \
      "" \
      "As additional permission under GNU AGPL version 3 section 7, you" \
      "may distribute non-source (e.g., minimized or compacted) forms of" \
      "that code without the copy of the GNU GPL normally required by" \
      "section 4, provided you include this license notice and a URL" \
      "through which recipients can access the Corresponding Source." \
      "" \
      "@licend The above is the entire license notice" \
      "for the JavaScript code in this page."
}

update_install_dir() {
  updatedInstallDir=0

  if [ ! -z $1 ]
  then
    # Uppercase the first letter of the input.
    first=`echo $1 | cut -c1 | tr '[a-z]' '[A-Z]'`
    rest=`echo $1 | cut -c2-`
    php=`echo php/modules/$first$rest.php`
    if [ ! -e install/$first$rest.php ]
    then
      if [ ! -e $php ]
      then
        echo "Couldn't find file $php to copy to install directory"
      else
        echo "Copying new file $php to install directory"
        cp $php install
        if [ -e js/source/dobrado.$1.js ]
        then
          if [ ! -x jsmin ]
          then
            echo "jsmin not found"
            echo "Copying new file js/source/dobrado.$1.js to install directory"
            cp js/source/dobrado.$1.js install/dobrado.$1.source.js
            if [ -e js/dobrado.$1.js ]
            then
            echo "Copying new file js/dobrado.$1.js to install directory"
              cp js/dobrado.$1.js install/dobrado.$1.js
            fi
          else
            echo "Copying new file js/source/dobrado.$1.js to install directory"
            cp js/source/dobrado.$1.js install/dobrado.$1.source.js
            echo "Minifying install/dobrado.$1.js"
            minify_js install dobrado.$1.source.js install dobrado.$1.js
          fi
        fi
        if [ -e css/$1.css ]
        then
          echo "Copying new file css/$1.css to install directory"
          cp css/$1.css install
        fi
        updatedInstallDir=1
      fi
    fi
  fi

  # Check for files in the install directory that have newer versions in
  # working directories (js, css and php/modules),  and copy them over so the
  # updates are seen by 'git -a commit'.
  for file in `find install -name "dobrado.*.source.js" | \
    sed 's/install\/\(.*\)\.source\.js/\1/'`
  do
    if [ -e js/source/$file.js ]
    then
      check=`diff -q install/$file.source.js js/source/$file.js`
      if [[ $check == *differ ]]
      then
        if [ ! -x jsmin ]
        then
          echo "jsmin not found"
          return
        fi
        echo "Copying updated file $file.js to install directory."
        cp js/source/$file.js install/$file.source.js
        echo "Minifying install/$file.js"
        minify_js install $file.source.js install $file.js
        updatedInstallDir=1
      fi
    fi
  done

  # Check for changes to default module javascript files.
  for file in `git diff --name-only js/source | sed 's/js\/source\/\(.*\)/\1/'`
  do
    if [ ! -x jsmin ]
    then
      echo "jsmin not found"
      return
    fi
    if [[ $file == core*.js ]] ||
       [[ $file == dobrado.*.js ]] || [[ $file == deploy.js ]]
    then
      echo "Minifying $file"
      minify_js js/source $file js $file
    fi
  done

  for file in `find install -name "*.css" | sed 's/install\/\(.*\)/\1/'`
  do
    if [ -e css/$file ]
    then
      check=`diff -q install/$file css/$file`
      if [[ $check == *differ ]]
      then
        echo "Copying updated file $file to install directory."
        cp css/$file install/$file
        updatedInstallDir=1
      fi
    fi
  done

  for file in `find install -name "*.php" | sed 's/install\/\(.*\)/\1/'`
  do
    if [ -e php/modules/$file ]
    then
      check=`diff -q install/$file php/modules/$file`
      if [[ $check == *differ ]]
      then
        echo "Copying updated file $file to install directory."
        cp php/modules/$file install/$file
        updatedInstallDir=1
      fi
    fi
  done

  if [ $updatedInstallDir == 0 ]
  then
    echo "Install directory up to date. No changes made."
  fi
}

update_install_dir $1
