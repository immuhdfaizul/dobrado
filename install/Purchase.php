<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Purchase extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view purchases.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';

    if ($us_action === 'list') {
      return $this->ListProducts();
    }
    if ($us_action === 'time') {
      return $this->CurrentTime();
    }
    if ($us_action === 'save') {
      return $this->SaveOrders();
    }
    if ($us_action === 'remove') {
      return $this->RemoveFromOrder();
    }
    if ($us_action === 'change') {
      return $this->ChangeOrder();
    }
    if ($us_action === 'saveDetails') {
      return $this->SaveDetails();
    }
    if ($us_action === 'connectSettings') {
      return $this->ConnectSettings();
    }
  }

  public function CanAdd($page) {
    // Need admin privileges to add the purchase module.
    if (!$this->user->canEditSite) return false;
    // Can only have one purchase module on a page.
    return !$this->AlreadyOnPage('purchase', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    // Need to create the select before changing the purchase group.
    $purchase_group_select = $this->PurchaseGroupSelect();
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    // Default mode for purchase page is 'volunteer mode'.
    $volunteer = 'thanks for volunteering today!';
    $save_button_text = 'submit purchases';
    $info_level_text = 'Please remind <span class="user"></span> that they ' .
      'owe $<span class="outstanding"></span> for previous purchases.';
    $warning_level_text = 'This is a warning that <span class="user"></span> ' .
      'owes $<span class="outstanding"></span>. Please ask them to make a ' .
      'payment before continuing.';
    $co_op_day_text = $this->Substitute('co-op-day');
    $purchase_next_week = $this->Substitute('purchase-next-week');
    $hide_next_week = $purchase_next_week === '' ? ' hidden' : '';
    $purchase_description =
      '<div class="order hidden">' .
        'You are currently entering purchases for ' .
        '<span class="name"></span>.<br>' .
        '<div class="roster-reminder hidden">They haven\'t volunteered ' .
          'recently, please ask them to put their name on the roster.' .
        '</div>' .
        '<div class="roster-volunteer hidden">They have already volunteered.' .
        '</div>' .
        'Their current total is: $<span class="total">0.00</span>' .
        '<div class="form-spacing' . $hide_next_week . '">' .
          '<label for="purchase-next-week-input">' . $purchase_next_week .
          '</label>' .
          '<input id="purchase-next-week-input" type="checkbox"><hr>' .
        '</div>' .
      '</div>';
    $purchase_update_quantity = '';
    if ($this->Substitute('purchase-update-quantity') === 'true') {
      $purchase_update_quantity = '<span id="purchase-connect">' .
          '<label for="purchase-update-quantity">' .
            '<span class="ui-icon ui-icon-transferthick-e-w"></span>' .
          '</label>' .
          '<input id="purchase-update-quantity" type="checkbox">' .
          '<button id="purchase-connect-settings">settings</button>' .
        '</span>';
    }
    $hide_username = '';
    $set_order_mode = false;
    $_SESSION['purchase-refresh'] = false;
    $_SESSION['purchase-order'] = false;
    // Need to know when the purchase-group session variable is changed by
    // a different page so that we can refresh.
    $_SESSION['purchase-group-changed'] = false;
    $_SESSION['purchase-order-check'] = false;

    // refresh is checked by the 'time' callback.
    if (isset($_GET['refresh']) && $_GET['refresh'] === 'true') {
      $_SESSION['purchase-refresh'] = true;
    }
    // Check if order mode was explicitly requested.
    if ((isset($_GET['order']) && $_GET['order'] === 'true') ||
        $this->Substitute('purchase-order-page') === $this->user->page) {
      $set_order_mode = true;
    }
    // Check if today is co-op-day, order mode will be used if not.
    if ($this->PurchasingAvailable($co_op_day_text)) {
      // When the page is first loaded, the browser will check if stored
      // purchase data should be saved. This causes problems when purchasing
      // is now available but the browser still has an unsaved order stored.
      // This data was never added to the group order, so shouldn't be saved
      // now, but it's not possible to differentiate it from normal data. To
      // avoid saving it set 'purchase-order-check' so that the time check
      // can tell the browser to ignore it.
      $_SESSION['purchase-order-check'] = true;
    }
    else {
      $set_order_mode = true;
    }

    $first = '';
    $thumbnail = '';
    $img = '<img class="thumb" src="/images/default_thumb.jpg">';

    $detail = new Detail($this->user, $this->owner);
    $user_details = $detail->User();
    if (is_array($user_details)) {
      // The volunteer doesn't have to match the account details due to
      // users having shared accounts, so check who is on the roster.
      $roster = new Roster($this->user, $this->owner);
      $first = $roster->FirstName();
      if ($first === '' || $first === $this->user->name) {
        $first = $user_details['first'];
        $img = $user_details['thumbnail'];
      }
    }
    // If the user hasn't filled out their details just display their username.
    if ($first === '') {
      $first = $this->user->name;      
    }

    if ($set_order_mode) {
      // If pre-order is not being used or the current page is only used for
      // packing, then return a simple message here.
      $pre_order = $this->Substitute('pre-order') === 'true';
      if (!$pre_order ||
          $this->Substitute('purchase-packing-page') === $this->user->page) {
        $group_name = $this->Substitute('group-name');
        $order_available_text = '';
        if (!$pre_order) {
          $order_available_text = 'Ordering is not available for ' .
            $group_name . '.<br>';
        }
        // A purchase date hasn't been set when it's greater than 6 months away.
        $purchase_available_text =
          'There is currently no date set for packing.';
        if (strtotime($co_op_day_text) < time() + 86400 * 182) {
          $purchase_available_text = 'This page will be available for ' .
            'packing on <b>' . $co_op_day_text . '</b>';
        }
        $this->user->group = $default_group;
        return $purchase_group_select . '<div class="message">' . $img .
          '<span class="hello">Hello ' . $first . '</span><br>' .
          $order_available_text . $purchase_available_text . '</div>';
      }

      $_SESSION['purchase-refresh'] = true;
      $_SESSION['purchase-order'] = true;
      // Some users want to be able to order for others, which just requires
      // displaying the username field here.
      if (!$this->GroupMember('purchase-other-order')) {
        $hide_username = ' hidden';
      }
      $volunteer = '';
      $save_button_text = 'submit order';
      $info_level_text = 'This is a reminder that you owe ' .
        '$<span class="outstanding"></span> for previous purchases.';
      $warning_level_text = 'This is a warning that you owe ' .
        '$<span class="outstanding"></span>. Please make a payment ' .
        'before continuing.';
      $purchase_description =
        '<div class="order hidden">' .
          'Your current total is: $' .
          '<span class="total">0.00</span>' .
          '<div class="info hidden">Once you\'ve finished ordering please ' .
            'click the <b>submit order</b> button above.<hr></div>' .
        '</div>';
      $purchase_update_quantity = '';
    }

    $content = '<div class="purchase-info-wrapper">' .
        '<div class="purchase-date-info"></div>' . $purchase_group_select .
      '</div>' .
      '<div class="message">' .
        $img . '<span class="hello">Hello ' . $first . ',</span> ' .
        $volunteer . '<br><span class="stored"></span> ' .
        '<button class="save">' . $save_button_text . '</button>' .
      '</div>' .
      '<div class="warning ui-state-highlight ui-corner-all hidden">' .
        '<b>Warning:</b> This purchasing system would work better if you ' .
        'upgraded your browser. Thanks!' .
      '</div>' .
      '<div class="info-level ui-state-highlight ui-corner-all hidden">' .
        $info_level_text .
      '</div>' .
      '<div class="warning-level ui-state-error ui-corner-all hidden">' .
        $warning_level_text .
      '</div>' .
      '<form id="purchase-form" autocomplete="off">' .
        $purchase_description .
        '<div class="form-spacing' . $hide_username . '">' .
          '<label for="purchase-name-input">Username:</label>' .
          '<input id="purchase-name-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-product-input">Product:</label>' .
          '<input id="purchase-product-input" type="text" maxlength="100">' .
          '<button class="view-all">View all</button>' .
        '</div>' .
        '<div id="purchase-grower-info"></div>' .
        '<div id="purchase-quota-info"></div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-quantity-input">Quantity:</label>' .
          '<input id="purchase-quantity-input" type="text" maxlength="8">' .
          $purchase_update_quantity .
          '<span class="purchase-quantity-info"></span>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-price-input">Price:</label>' .
          '<input id="purchase-price-input" type="text" maxlength="50" ' .
            'readonly="true">' .
        '</div>' .
        '<button class="add">add</button>' .
        '<button class="remove">remove</button>' .
      '</form>' .
      '<form id="purchase-details-form" class="hidden" autocomplete="off">' .
        'Please ask <span class="purchase-details-user"></span> for the ' .
        'following details:<br><br>' .
        '<div class="form-spacing">' .
          '<label for="purchase-details-first-input">First Name:</label>' .
          '<input id="purchase-details-first-input" type="text" ' .
            'maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-details-last-input">Last Name:</label>' .
          '<input id="purchase-details-last-input" type="text" maxlength="50">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-details-phone-input">Phone Number:</label>' .
          '<input id="purchase-details-phone-input" type="text" ' .
            'maxlength="50">' .
        '</div><br>' .
        '<div id="purchase-details-info"></div>' .
        '<button class="submit">submit</button>' .
      '</form>' .
      '<div class="purchase-all-available hidden">' .
        $this->AllAvailableCategories() .
      '</div>' .
      '<div class="purchase-all-users hidden"></div>' .
      '<div class="purchase-save-order-confirm hidden">' .
        '<p>Thank you!</p><p>Your order has been submitted.</p>' .
        '<button class="purchase-save-order-dismiss">ok</button>' .
      '</div>' .
      '<div class="purchase-unavailable hidden"></div>' .
      '<div class="purchase-connect-settings hidden"></div>';

    $this->user->group = $default_group;
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {
    if (!$this->Run(date('H:00:00'))) return;

    // Check the number of active users in each group. 'purchase-active-users'
    // should be set to a relative date such as 'first day of this month' so
    // that dates 'first day of last month' and 'last day of last month' make
    // sense in ActiveUsers. This version should be used for most groups as
    // it includes non-invoiced users.
    foreach ($this->RunGroups('purchase-active-users') as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      $description = 'Cron: <b>' . $this->ActiveUsers() . '</b> active ' .
        'users for ' . ucfirst($group);
      $this->Notification('purchase', 'Purchase', $description, 'system',
                          $group);
    }
  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      if ($fn === 'CartSales' && count($p) === 3) {
        $payment_method = $p[0];
        $data = $p[1];
        $pack_date = $p[2];
        return $this->CartSales($payment_method, $data, $pack_date);
      }
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.purchase.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.purchase.js', false);

    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS purchase (' .
      'user VARCHAR(50) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'name VARCHAR(100) NOT NULL,' .
      'supplier VARCHAR(50) NOT NULL,' .
      'grower VARCHAR(200),' .
      'quantity DECIMAL(8,3) NOT NULL,' .
      'price DECIMAL(8,2) NOT NULL,' .
      'base_price DECIMAL(8,2) NOT NULL,' .
      'purchase_group VARCHAR(50),' .
      'id INT UNSIGNED NOT NULL,' .
      'volunteer VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, timestamp, name, purchase_group, id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS purchase_totals (' .
      'user VARCHAR(50) NOT NULL,' .
      'amount DECIMAL(20,10) NOT NULL,' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Install 2: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS purchase_composite (' .
      'user VARCHAR(50) NOT NULL,' .
      'name VARCHAR(100) NOT NULL,' .
      'quantity DECIMAL(8,3) NOT NULL,' .
      'composite VARCHAR(100) NOT NULL,' .
      'volunteer VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, name, composite)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Install 3: ' . $mysqli->error);
    }
    // Create triggers to update user's total when the purchase table changes.
    // Note that purchase_totals.amount is only updated here, it expects that
    // the user has already been inserted into the table. (When their account
    // is created).
    $query = 'CREATE TRIGGER insert_purchase_totals AFTER INSERT ON purchase ' .
      'FOR EACH ROW UPDATE purchase_totals SET ' .
      'amount = amount + ROUND(NEW.price * NEW.quantity, 2) WHERE ' .
      'user = NEW.user';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Install 4: ' . $mysqli->error);
    }
    // Note that the user for an existing purchase cannot be modified as this
    // rule would not be triggered.
    $query = 'CREATE TRIGGER update_purchase_totals AFTER UPDATE ON purchase ' .
      'FOR EACH ROW UPDATE purchase_totals SET ' .
      'amount = amount + ROUND(NEW.price * NEW.quantity, 2) - ' .
      'ROUND(OLD.price * OLD.quantity, 2) WHERE user = NEW.user';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Install 5: ' . $mysqli->error);
    }
    $query = 'CREATE TRIGGER delete_purchase_totals AFTER DELETE ON purchase ' .
      'FOR EACH ROW UPDATE purchase_totals SET ' .
      'amount = amount - ROUND(OLD.price * OLD.quantity, 2) WHERE ' .
      'user = OLD.user';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Install 6: ' . $mysqli->error);
    }
    $mysqli->close();

    $template = ['"purchase-date-info", "", ' .
                   '"Purchasing from <b>!group</b> on <b>!purchase</b>."',
                 '"purchase-order-info", "", ' .
                   '"Ordering from <b>!group</b> to pick up on ' .
                   '<b>!purchase</b>.<br>This order will close !close."'];
    $this->AddTemplate($template);
    $description = ['co-op-day' => 'The day the group meets for packing.',
                    'purchase-date-info' => 'Displays information about ' .
                      'purchasing. Can substitute !group and !purchase ' .
                      '(a formatted date.)',
                    'purchase-order-info' => 'Displays information about' .
                      'ordering. Can substitute !group, !purchase and !close ' .
                      '(the last two are formatted dates.)',
                    'purchase-page' => 'The link to the purchase page from ' .
                      'the summary page.',
                    'purchase-next-week' => 'A label for the purchase page ' .
                      'checkbox, it\'s used to pre-order the same items for ' .
                      'the following week when pre-ordering is turned on, or ' .
                      'to record if the user will be attending the following ' .
                      'week when pre-ordering is off.',
                    'purchase-add-next-week' => 'The string \'true\' or ' .
                      '\'false\', will show a link next to the purchase date ' .
                      'that toggles the date between this week\'s purchases ' .
                      'and next weeks.',
                    'purchase-order-gap' => 'The number of days between ' .
                      'pre-order-final and co-op-day. It\'s used to decide ' .
                      'if the following week\'s co-op-day should be shown ' .
                      'rather than this weeks, due to the fact that orders ' .
                      'for this week have already been finalised.',
                    'purchase-force-order' => 'The string \'true\' or ' .
                      '\'false\', to force the purchase page to show order ' .
                      'mode on co-op-day, used in conjunction with the ' .
                      '\'purchase-volunteer\' override group.',
                    'purchase-pre-order' => 'The day orders should be copied ' .
                      'for next week, for users that requested it. Orders ' .
                      'are also copied when invoices are generated, but this ' .
                      'setting can be used if orders need to be finalised ' .
                      'before invoices are sent.',
                    'purchase-pre-order-time' => 'The time that orders are ' .
                      'copied when purchase-pre-order is being used.',
                    'purchase-check-quota' => 'The string \'true\' or ' .
                      '\'false\', can be used with invoice-remove-orders to ' .
                      'only remove orders that haven\'t reached quota.',
                    'pre-order' => 'The string \'true\' or \'false\' to turn ' .
                      'pre-ordering on or off.',
                    'pre-order-final' => 'The day of the week the order is ' .
                      'finalised and order emails are sent out for groups ' .
                      'and organisations.',
                    'pre-order-final-time' => 'The time of day on ' .
                      'pre-order-final that orders are sent.',
                    'pre-order-open' => 'The day of the week the order ' .
                      'opens. When the order is not open a dialog will be ' .
                      'shown on the purchase page that can\'t be closed, ' .
                      'showing the message in pre-order-closed.',
                    'pre-order-open-time' => 'The time of day on ' .
                      'pre-order-open that ordering will become available.',
                    'pre-order-unavailable' => 'The message shown in a ' .
                      'dialog on the purchase page when ordering is not ' .
                      'available.',
                    'surcharge' => 'The string \'true\' or \'false\' if ' .
                      'surcharge is used for the group.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#purchase-form","clear","both"',
                   '"","#purchase-form","background-color","#eeeeee"',
                   '"","#purchase-form","border","1px solid #aaaaaa"',
                   '"","#purchase-form","border-radius","2px"',
                   '"","#purchase-form","padding","5px"',
                   '"","#purchase-form label","width","5em"',
                   '"","#purchase-form .order .name","font-weight","bold"',
                   '"","#purchase-form .order .name","font-size","1.1em"',
                   '"","#purchase-product-input","width","400px"',
                   '"","#purchase-product-input","max-width","98%"',
                   '"","#purchase-quantity-input","height","20px"',
                   '"","#purchase-quantity-input","width","100px"',
                   '"","label[for=purchase-next-week-input]","float","none"',
                   '"","#purchase-details-form label","width","7.5em"',
                   '"","#purchase-details-form .submit","margin-left","8em"',
                   '"","#purchase-form .add","float","right"',
                   '"",".purchase-countdown","font-weight","bold"',
                   '"",".purchase-info-wrapper","float","right"',
                   '"",".purchase-info-wrapper","padding-left","15px"',
                   '"",".purchase .message .thumb","float","left"',
                   '"",".purchase .message .thumb","padding-right","10px"',
                   '"",".purchase .message","line-height","3em"',
                   '"",".purchase .hello","white-space","nowrap"',
                   '"",".purchase .warning","clear","both"',
                   '"",".purchase .warning","padding","5px"',
                   '"",".purchase .warning","margin","5px"',
                   '"",".purchase .warning-level","padding","5px"',
                   '"",".purchase .warning-level","margin","5px"',
                   '"",".purchase .info-level","padding","5px"',
                   '"",".purchase .info-level","margin","5px"',
                   '"",".purchase .order .total","font-weight","bold"',
                   '"","#purchase-quota-wrapper","border","1px solid #777777"',
                   '"","#purchase-quota-wrapper","display","inline-block"',
                   '"","#purchase-quota-wrapper","height","10px"',
                   '"","#purchase-quota-wrapper","width","100px"',
                   '"","#purchase-quota-bar","display","block"',
                   '"","#purchase-quota-bar","height","100%"',
                   '"","#purchase-grower-info","margin-left","10px"',
                   '"","#purchase-grower-info","height","20px"',
                   '"","#purchase-quota-info","margin-left","1.6em"',
                   '"",".grid-row-updated","color","red"',
                   '"","label[for=purchase-update-quantity]","height","14px"',
                   '"","label[for=purchase-update-quantity]","width","14px"',
                   '"","label[for=purchase-update-quantity]","margin-left",' .
                     '"5px"',
                   '"","#purchase-connect-settings","width","30px"',
                   '"",".purchase-spark-id","color","#aaaaaa"',
                   '"",".purchase-spark-text","color","#aaaaaa"',
                   '"","#purchase-categories","display","flex"',
                   '"","#purchase-categories","flex-wrap","wrap"',
                   '"","#purchase-categories > label","flex-grow","1"',
                   '"","#purchase-categories > label",' .
                     '"border-bottom-left-radius","0"',
                   '"","#purchase-categories > label",' .
                     '"border-bottom-right-radius","0"',
                   '"","#purchase-categories > label","padding","5px"',
                   '"",".purchase-group-select","text-align","right"',
                   '"","label[for=purchase-available-orders]","float","none"',
                   '"",".purchase-grid-image","height","65px"',
                   '"",".purchase-grid-image","width","auto"',
                   '"",".purchase-grid-image","float","left"',
                   '"",".purchase-grid-image","margin-right","5px"',
                   '"",".purchase-grid-image","border-radius","5px"',
                   '"",".purchase-grid-no-image","height","65px"',
                   '"",".purchase-grid-no-image","width","70px"',
                   '"",".purchase-grid-no-image","float","left"',
                   '"",".purchase-grid-product","white-space","normal"',
                   '"",".purchase-grid-product-wrapper","min-height","40px"',
                   '"",".purchase-grid-price","float","right"',
                   '"",".purchase-grid-price","margin-right","10px"',
                   '"",".purchase-grid-grower","font-size","0.8em"',
                   '"",".purchase-grid-grower","color","#555555"',
                   '"",".purchase-grid-grower","white-space","normal"',
                   '"",".purchase-grid-quota","float","right"',
                   '"",".purchase-grid-quota","margin-right","10px"',
                   '"",".purchase-grid-quota","font-size","0.8em"',
                   '"",".purchase-grid-quota","color","#ffffff"',
                   '"",".purchase-grid-quota","padding","2px"',
                   '"",".purchase-grid-quota","border","1px solid #777777"',
                   '"",".purchase-grid-quota","border-radius","2px"'];
    $this->AddSiteStyle($site_style);
    $this->AddSettingTypes(['"purchase","gridColumns",' .
                              '"default,showSupplier,combinedFormat","radio",' .
                              '"The supplier column isn\'t shown on the ' .
                              'purchase page by default. Combined format ' .
                              'displays product information in one column."']);
    // Check module dependenices.
    $dependencies = ['banking', 'detail', 'invite', 'payment', 'roster',
                     'stock'];
    return $this->Dependencies($dependencies);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    // Return if Remove was called for a specific module,
    // only want to remove purchases when deleting an account.
    if (isset($id)) return;

    // Note that deleting accounts with existing purchases must be done
    // very carefully because it can effect the balance of other users.
    $mysqli = connect_db();
    $products = [];
    // Calculate all quantities for purchases by this user to increase stock.
    $query = 'SELECT name, supplier, SUM(quantity) AS quantity FROM purchase ' .
      'WHERE user = "' . $this->owner . '" AND name != "surcharge" ' .
      'GROUP BY name, supplier';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $supplier = $purchase['supplier'];
        if (!isset($products[$supplier])) {
          $products[$supplier] = [];
        }
        $products[$supplier][$purchase['name']] = $purchase['quantity'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Remove 1: ' . $mysqli->error);
    }
    $stock = new Stock($this->user, $this->owner);
    $stock->Increase($products);

    $query = 'DELETE FROM purchase WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Remove 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM purchase WHERE supplier = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Remove 3: ' . $mysqli->error);
    }
    $query = 'DELETE FROM purchase_totals WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->Remove 4: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.purchase.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function ActiveUsers() {
    $count = 0;
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    $query = 'SELECT DISTINCT purchase.user FROM purchase LEFT JOIN users ON ' .
      'purchase.user = users.user WHERE ' . $organiser->GroupQuery() .
      ' AND timestamp >= ' . strtotime('first day of last month, 00:00') .
      ' AND timestamp <= ' . strtotime('last day of last month, 23:59');
    if ($result = $mysqli->query($query)) {
      $count = $result->num_rows;
      $result->close();
    }
    else {
      $this->Log('Purchase->ActiveUsers 1: ' . $mysqli->error);
    }
    // Also store the count in a template so that it's accessible by the
    // Invoice module which displays it descriptively.
    $query = 'INSERT INTO template VALUES ("invoice-active-count", ' .
      '"' . $this->user->group . '", ' . $count . ') ON DUPLICATE KEY UPDATE ' .
      'content = ' . $count;
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->ActiveUsers 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $count;
  }

  public function AdjustOrders($start, $end, $organisation = false) {
    // If invoice-order-rounding isn't set, don't do anything.
    $rounding = $this->Substitute('invoice-order-rounding');
    if ($rounding === '') return;

    $stock = new Stock($this->user, $this->owner);
    $products = $stock->AvailableProducts(false, false);
    $percent = [];
    $current = 0;
    $timestamp_query = 'timestamp >= ' . $start;
    if ($end !== 0) $timestamp_query .= ' AND timestamp <= ' . $end;

    $group_query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $group_query = $organiser->PurchaseQuery();
    }
    $mysqli = connect_db();
    $query = '';
    // First get the total quantity of each product ordered. 
    if ($organisation) {
      $query = 'SELECT name, SUM(quantity) AS quantity FROM purchase WHERE ' .
        $group_query . ' AND ' . $timestamp_query . ' AND name != "surcharge" '.
        'GROUP BY name';
    }
    else {
      $query = 'SELECT name, SUM(quantity) AS quantity FROM purchase ' .
        'WHERE purchase_group = "' . $this->user->group . '" AND ' .
        $timestamp_query . ' AND name != "surcharge" GROUP BY name';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $name = $purchase['name'];
        $size = $products[$name]['size'];
        $packs = 0;
        if ($size < 0.01) continue;

        $packs = (float)$purchase['quantity'] / $size;
        // Find the fraction of the last pack to compare rounding to.
        $fraction = $packs - floor($packs);
        if ($fraction < (float)$rounding) {
          // Need to reduce each order for this item by a percentage which is
          // calculated from this last pack fraction divided by total order.
          $current = $fraction / $packs * -1;
        }
        else {
          // Need to increase each order by a percentage which is calculated by
          // 1 - fraction, divided by the total order.
          $current = (1 - $fraction) / $packs;
        }
        // Only adjust the orders for this product if change is greater than 5%
        if ($current > 0.05 || $current < -0.05) {
          $percent[$name] = $current;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AdjustOrders 1: ' . $mysqli->error);
    }
    // Don't need to continue if no names were listed.
    if (count($percent) === 0) {
      $mysqli->close();
      return;
    }

    // Adjust each individual order to make up a full pack.
    foreach ($percent as $name => $value) {
      $quantity_query = 'quantity = quantity + ' . $value . ' * quantity';
      // Keep whole units when a product is sold as 'each'.
      if ($products[$name]['unit'] === 'each') {
        $quantity_query = 'quantity = ROUND(quantity + ' .$value.' * quantity)';
      }
      if ($organisation) {
        $query = 'UPDATE purchase SET ' . $quantity_query . ' WHERE ' .
          $group_query . ' AND ' . $timestamp_query . ' AND name = "'.$name.'"';
      }
      else {
        $query = 'UPDATE purchase SET ' . $quantity_query . ' WHERE ' .
          'purchase_group = "' . $this->user->group . '" AND ' .
          $timestamp_query . ' AND name = "' . $name . '"';
      }
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->AdjustOrders 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function AllData($start, $end, $array,
                          $organisation = false, $hide_removed = true) {
    $result = [];
    $timestamp_query = 'timestamp >= ' . $start;
    if ($end !== 0) $timestamp_query .= ' AND timestamp <= ' . $end;
    $quantity_query = '';
    if ($hide_removed) $quantity_query = 'AND quantity != 0 ';

    $query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      // In the case of a multi-group buy, still want to return the purchases
      // grouped by system_group so they can be collated. (This query still
      // also does the right thing for normal orders for an organisation.)
      $query = 'SELECT system_group AS user, timestamp, name, supplier, ' .
        'grower, SUM(quantity) AS quantity, price, base_price FROM purchase ' .
        'LEFT JOIN users ON purchase.user = users.user WHERE ' .
        $organiser->PurchaseQuery() . ' AND ' . $timestamp_query . ' AND ' .
        'name != "surcharge" ' . $quantity_query . 'GROUP BY system_group, ' .
        'name ORDER BY name, system_group';
    }
    else {
      $query = 'SELECT user, timestamp, name, supplier, grower, ' .
        'quantity, price, base_price FROM purchase WHERE ' .
        'purchase_group = "' . $this->user->group . '" AND ' . $timestamp_query.
        ' AND name != "surcharge" ' . $quantity_query . 'ORDER BY user, name';
    }

    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $user = $purchase['user'];
        $name = $purchase['name'];
        if ($organisation) {
          // When organisation is true, an array is created for each product.
          // Note that this is not used with the $array parameter equal to
          // true, so don't need to worry about that case.
          if (!isset($result[$name])) $result[$name] = [];
        }
        else if (!isset($result[$user])) {
          $result[$user] = [];
        }
        // The grid module requires this data as an indexed array, but when
        // updating the data it's easier to do the comparison with an
        // associative array keyed on item names. The 'server' value is so
        // that the client knows to tell the server when an item is removed.
        if ($array) {
          $quantity = (float)$purchase['quantity'];
          $price = (float)$purchase['price'];
          $base_price = (float)$purchase['base_price'];
          // timestamp is converted to milliseconds here for js.
          $timestamp = (int)$purchase['timestamp'] * 1000;
          $result[$user][] = ['date' => $timestamp,
                              'name' => $name,
                              'supplier' => $purchase['supplier'],
                              'grower' => $purchase['grower'],
                              'quantity' => $quantity,
                              'price' => price_string($price),
                              'basePrice' => $base_price,
                              'total' => price_string($quantity * $price),
                              'server' => true];
        }
        else {
          // The query above is grouped by user (where user is an alias for
          // system_group) so don't need to worry about duplicates in this case.
          if ($organisation) {
            $result[$name][$user] =
              ['date' => (int)$purchase['timestamp'],
               'supplier' => $purchase['supplier'],
               'grower' => $purchase['grower'],
               'quantity' => (float)$purchase['quantity'],
               'price' => (float)$purchase['price'],
               'basePrice' => (float)$purchase['base_price']];
          }
          else {
            // Since this array is indexed by product name, keep the first value
            // found so that purchases with later timestamps don't overwrite it.
            if (!isset($result[$user][$name])) {
              $result[$user][$name] =
                ['date' => (int)$purchase['timestamp'],
                 'supplier' => $purchase['supplier'],
                 'grower' => $purchase['grower'],
                 'quantity' => (float)$purchase['quantity'],
                 'price' => (float)$purchase['price'],
                 'basePrice' => (float)$purchase['base_price']];
            }
          }
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllData: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function AllQuotas($start, $end, $organisation) {
    $stock = new Stock($this->user, $this->owner);
    $products = $stock->AvailableProducts(false, false);
    $quotas = [];
    $mysqli = connect_db();
    $query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT name, SUM(quantity) AS quantity FROM purchase WHERE ' .
        $organiser->PurchaseQuery() . ' AND timestamp >= ' . $start . ' AND ' .
        'timestamp <= ' . $end . ' GROUP BY name';
    }
    else {
      $query = 'SELECT name, SUM(quantity) AS quantity FROM purchase ' .
        'WHERE purchase_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end.' GROUP BY name';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $name = $purchase['name'];
        $size = $products[$name]['size'];
        if ($size < 0.01) continue;

        $quantity = (float)$purchase['quantity'];
        // Calculate full packs.
        $count = floor($quantity / $size);
        // Calculate the fraction of the final pack.
        $fraction = $quantity - ($count * $size);
        if ($fraction > 0.001) $count++;
        $fraction = number_format($fraction, 3, '.', '');
        if (strpos($fraction, '.')) {
          // Remove trailing zeros from the fraction.
          $fraction = preg_replace('/0+$/', '', $fraction);
          if (strpos($fraction, '.') === strlen($fraction) - 1) {
            $fraction = substr($fraction, 0, -1);
          }
        }
        $quotas[] = ['name' => $name,
                     'supplier' => $products[$name]['user'],
                     'count' => $count,
                     'size' => $size,
                     'unit' => $products[$name]['unit'],
                     'fraction' => $fraction];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllQuotas: ' . $mysqli->error);
    }
    $mysqli->close();
    return $quotas;
  }

  public function AllSold($start, $end) {
    $stock = new Stock($this->user, $this->owner);
    return $this->AllSupplyTotals($stock->AllSuppliers(), $start, $end);
  }

  public function CompositeData($composite, $user, $product, $timestamp) {
    $query = '';
    if ($composite) {
      // The current timestamp is included in the results to keep the grid
      // looking nice, composite data isn't timestamped.
      $query = 'SELECT name, quantity, ' . $timestamp . ' AS timestamp FROM ' .
        'purchase_composite WHERE user = "' . $user . '" AND ' .
        'composite = "' . $product . '"';
    }
    else {
      // Look for purchases over a range of time.
      $start = strtotime(date('F j Y 00:00:00', $timestamp));
      $end = $start + 86399;
      $query = 'SELECT name, quantity, timestamp FROM purchase WHERE ' .
        'user = "' . $user . '" AND timestamp >= ' . $start . ' AND ' .
        'timestamp <= ' . $end;
    }
    $result = [];
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $quantity = (float)$purchase['quantity'];
        $timestamp = (int)$purchase['timestamp'] * 1000;
        $result[$purchase['name']] = ['quantity' => $quantity,
                                      'date' => $timestamp];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->CompositeData: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function Data($start, $end = 0,
                       $all_groups = false, $user = '', $id = 0) {
    $result = [];
    if ($user === '') {
      $user = $this->user->name;
    }
    $mysqli = connect_db();
    $timestamp_query = '';
    if ($end !== 0) $timestamp_query = ' AND timestamp <= ' . $end;
    $id_query = '';
    if ($id !== 0) $id_query = ' AND id = ' . $id;

    $group_query = '';
    if (!$all_groups) {
      $group_query = ' AND purchase_group = "' . $this->user->group . '"';
    }
    $query = 'SELECT timestamp, name, supplier, grower, quantity, price, ' .
      'base_price FROM purchase WHERE timestamp >= ' . $start .
      $timestamp_query . $id_query . ' AND user = "' . $user . '" AND ' .
      'name != "surcharge" AND quantity != 0' . $group_query . ' ORDER BY name';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $quantity = (float)$purchase['quantity'];
        $price = (float)$purchase['price'];
        $total = price_string($quantity * $price);
        $price = price_string($price);
        $timestamp = (int)$purchase['timestamp'] * 1000;
        $result[] = ['date' => $timestamp,
                     'name' => $purchase['name'],
                     'supplier' => $purchase['supplier'],
                     'grower' => $purchase['grower'],
                     'quantity' => $quantity,
                     'price' => $price,
                     'basePrice' => (float)$purchase['base_price'],
                     'total' => $total,
                     'server' => true];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Data: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function EditComposite($username, $composite, $product, $quantity) {
    $mysqli = connect_db();
    $query = '';
    if ($quantity < 0.001) {
      $query = 'DELETE FROM purchase_composite WHERE user = "' . $username.'" '.
        'AND name = "' . $product . '" AND composite = "' . $composite . '"';
    }
    else {
      $query = 'INSERT INTO purchase_composite VALUES ("' . $username . '", ' .
        '"' . $product . '", ' . $quantity . ', "' . $composite . '", ' .
        '"' . $this->user->name . '") ON DUPLICATE KEY UPDATE ' .
        'quantity = ' . $quantity . ', volunteer = "' . $this->user->name . '"';
    }
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->EditComposite: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function Sold($start, $end, $user = '', $sum = false) {
    $result = [];
    if ($user === '') $user = $this->user->name;
    // Get a list of the stock items this user supplies.
    $stock = new Stock($this->user, $this->owner);
    // When $sum parameter is true purchases are grouped, which doesn't
    // work for variable priced items, so need to exclude from query here. 
    $supplies = $stock->Supplier($user, $sum);
    $supply_query = '';
    for ($i = 0; $i < count($supplies); $i++) {
      if ($supply_query !== '') $supply_query .= ' OR ';
      $supply_query .= 'name = "' . $supplies[$i] . '"';
    }
    if ($sum) {
      if (count($supplies) !== 0) {
        $query = 'SELECT supplier, timestamp, name, SUM(quantity) AS ' .
          'quantity, base_price FROM purchase WHERE supplier = "' . $user .'" '.
          'AND timestamp >= ' . $start . ' AND timestamp <= ' . $end . ' AND ' .
          'quantity != 0 AND (' . $supply_query.') GROUP BY name ORDER BY name';
        $result = $this->AddToSold($query);
      }
      // When grouping purchases need to list items that have variable prices
      // separately.
      $supplies = $stock->SupplierVariablePricing($user);
      if (count($supplies) === 0) return $result;

      $supply_query = '';
      for ($i = 0; $i < count($supplies); $i++) {
        if ($supply_query !== '') {
          $supply_query .= ' OR ';
        }
        $supply_query .= 'name = "' . $supplies[$i] . '"';
      }
      $query = 'SELECT supplier, timestamp, name, quantity, base_price FROM ' .
        'purchase WHERE supplier = "' . $user . '" AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end . ' AND quantity != 0 AND ' .
        '(' . $supply_query . ') ORDER BY timestamp, name';
      // AddToSold will add to the existing array and return it, so here the
      // result is just passed on to the calling function.
      return $this->AddToSold($query, $result);
    }
    else if (count($supplies) === 0) {
      // If the initial Supplier check was empty don't continue here.
      return $result;
    }
    else {
      $query = 'SELECT supplier, timestamp, name, quantity, base_price FROM ' .
        'purchase WHERE supplier = "' . $user . '" AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end . ' AND quantity != 0 AND ' .
        '(' . $supply_query . ') ORDER BY timestamp, name';
      return $this->AddToSold($query);
    }
  }

  public function AllIds($start, $end, $organisation = false, $invite = false) {
    $query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT purchase.user, MAX(id) AS id FROM purchase LEFT JOIN ' .
        'users ON purchase.user = users.user WHERE ' . $organiser->GroupQuery().
        ' AND timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' GROUP BY user';
    }
    else if ($invite) {
      $query = 'SELECT user, MAX(id) AS id FROM purchase WHERE ' .
        'purchase_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' GROUP BY user';
    }
    else {
      $query = 'SELECT purchase.user, MAX(id) AS id FROM purchase LEFT JOIN ' .
        'users ON purchase.user = users.user WHERE ' .
        'users.system_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' GROUP BY user';
    }
    $ids = [];
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $ids[$purchase['user']] = (int)$purchase['id'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllIds: ' . $mysqli->error);
    }
    $mysqli->close();
    return $ids;
  }

  // AllTaxable is used by the Invoice module to generate one invoice that
  // covers all the groups a member has bought from. It's also used by the
  // Report module to provide totals for individual groups, which is why
  // both organisation and invite parameters are required here.
  public function AllTaxable($start, $end,
                             $organisation = false, $invite = false) {
    $stock = new Stock($this->user, $this->owner);
    $all_taxable = $stock->AllTaxable();
    $mysqli = connect_db();
    $taxable = [];
    $query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT purchase.user, name, supplier, ' .
        'SUM(ROUND(quantity * price, 2)) AS total FROM purchase ' .
        'LEFT JOIN users ON purchase.user = users.user WHERE ' .
        $organiser->GroupQuery() . ' AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end . ' GROUP BY user, name, supplier';
    }
    else if ($invite) {
      $query = 'SELECT user, name, supplier, ' .
        'SUM(ROUND(quantity * price, 2)) AS total FROM purchase WHERE ' .
        'purchase_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' GROUP BY user, name, supplier';
    }
    else {
      $query = 'SELECT purchase.user, name, supplier, ' .
        'SUM(ROUND(quantity * price, 2)) ' .
        'AS total FROM purchase LEFT JOIN users ON purchase.user = users.user '.
        'WHERE users.system_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' GROUP BY user, name, supplier';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $supplier = $purchase['supplier'];
        $name = $purchase['name'];
        if (!isset($all_taxable[$supplier][$name])) continue;

        $user = $purchase['user'];
        if (!isset($taxable[$user])) $taxable[$user] = 0;  
        $taxable[$user] += (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllTaxable: ' . $mysqli->error);
    }
    $mysqli->close();
    return $taxable;
  }

  // AllTotals is used by the Invoice module to generate one invoice that
  // covers all the groups a member has bought from. It's also used by the
  // Report module to provide totals for individual groups, which is why
  // both organisation and invite parameters are required here.
  public function AllTotals($start, $end, $organisation = false,
                            $invite = false, $group_by_id = false) {
    $mysqli = connect_db();
    $totals = [];
    $query = '';
    $group_query = 'GROUP BY user';
    if ($group_by_id) $group_query .= ', id';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT purchase.user, MAX(timestamp) AS timestamp, ' .
        'MAX(id) AS id, SUM(ROUND(quantity * price, 2)) AS total FROM ' .
        'purchase LEFT JOIN users ON purchase.user = users.user WHERE ' .
        $organiser->GroupQuery() . ' AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end . ' AND name != "surcharge" ' .$group_query;
    }
    else if ($invite) {
      $query = 'SELECT user, MAX(timestamp) AS timestamp, MAX(id) AS id, ' .
        'SUM(ROUND(quantity * price, 2)) AS total FROM purchase WHERE ' .
        'purchase_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' AND name != "surcharge" ' . $group_query;
    }
    else {
      $query = 'SELECT purchase.user, MAX(timestamp) AS timestamp, ' .
        'MAX(id) AS id, SUM(ROUND(quantity * price, 2)) AS total FROM ' .
        'purchase LEFT JOIN users ON purchase.user = users.user WHERE ' .
        'users.system_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' AND name != "surcharge" ' . $group_query;
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        if ($group_by_id) {
          $totals[] = ['user' => $purchase['user'],
                       'timestamp' => (int)$purchase['timestamp'],
                       'id' => (int)$purchase['id'],
                       'total' => (float)$purchase['total']];
        }
        else {
          $totals[$purchase['user']] = (float)$purchase['total'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllTotals: ' . $mysqli->error);
    }
    $mysqli->close();
    return $totals;
  }

  public function AllSurcharge($start, $end,
                               $organisation = true, $group_by_id = false) {
    $mysqli = connect_db();
    $surcharge = [];
    $query = '';
    $group_query = 'GROUP BY user';
    if ($group_by_id) $group_query .= ', id';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT purchase.user, MAX(id) AS id, ' .
        'SUM(ROUND(quantity * price, 2)) AS total FROM purchase LEFT JOIN ' .
        'users ON purchase.user = users.user WHERE ' .
        $organiser->GroupQuery() . ' AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end . ' AND name = "surcharge" ' . $group_query;
    }
    else {
      $query = 'SELECT purchase.user, MAX(id) AS id, ' .
        'SUM(ROUND(quantity * price, 2)) AS total FROM purchase LEFT JOIN ' .
        'users ON purchase.user = users.user WHERE ' .
        'users.system_group = "' . $this->user->group . '" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' AND name ="surcharge" ' . $group_query;
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $user = $purchase['user'];
        if ($group_by_id) {
          $id = (int)$purchase['id'];
          if (isset($surcharge[$user])) {
            $surcharge[$user][$id] = (float)$purchase['total'];
          }
          else {
            $surcharge[$user] = [$id => (float)$purchase['total']];
          }
        }
        else {
          $surcharge[$purchase['user']] = (float)$purchase['total'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllSurcharge: ' . $mysqli->error);
    }
    $mysqli->close();
    return $surcharge;
  }

  public function Surcharge($start, $end = 0, $user = '', $id = 0) {
    $result = [];
    if ($user === '') {
      $user = $this->user->name;
    }
    $total = 0;
    $mysqli = connect_db();
    $timestamp_query = '';
    if ($end !== 0) $timestamp_query = ' AND timestamp <= ' . $end;
    $id_query = '';
    if ($id !== 0) $id_query = ' AND id = ' . $id;

    $query = 'SELECT price FROM purchase WHERE timestamp >= ' . $start .
      $timestamp_query . $id_query . ' AND user = "' . $user . '" AND ' .
      'name = "surcharge"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $total += (float)$purchase['price'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Surcharge: ' . $mysqli->error);
    }
    $mysqli->close();
    return $total;
  }

  public function AllOutstanding($payment_totals,
                                 $timestamp, $organisation = false) {
    $mysqli = connect_db();
    $organiser = new Organiser($this->user, $this->owner);
    // First get the array of purchase totals to compare payments to.
    $purchase_totals = [];
    $query = '';
    if ($organisation) {
      $query = 'SELECT purchase_totals.user, amount FROM purchase_totals ' .
        'LEFT JOIN users ON purchase_totals.user = users.user WHERE ' .
        'users.user NOT LIKE "buyer\_%" AND ' . $organiser->GroupQuery();
    }
    else {
      $query = 'SELECT purchase_totals.user, amount FROM purchase_totals ' .
        'LEFT JOIN users ON purchase_totals.user = users.user WHERE ' .
        'users.user NOT LIKE "buyer\_%" AND ' .
        'users.system_group = "' . $this->user->group . '"';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($totals = $mysqli_result->fetch_assoc()) {
        $purchase_totals[$totals['user']] = (float)$totals['amount'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllOutstanding 1: ' . $mysqli->error);
    }

    // Need to subtract purchases with future timestamps from totals.
    $future = [];
    $query = '';
    if ($organisation) {
      $query = 'SELECT purchase.user, SUM(ROUND(quantity * price, 2)) AS ' .
        'total FROM purchase LEFT JOIN users ON purchase.user = users.user ' .
        'WHERE users.user NOT LIKE "buyer\_%" AND timestamp > ' . $timestamp .
        ' AND ' . $organiser->GroupQuery() . ' GROUP BY user';
    }
    else {
      $query = 'SELECT purchase.user, SUM(ROUND(quantity * price, 2)) AS ' .
        'total FROM purchase LEFT JOIN users ON purchase.user = users.user ' .
        'WHERE users.user NOT LIKE "buyer\_%" AND timestamp > ' . $timestamp .
        ' AND users.system_group = "' . $this->user->group . '" GROUP BY user';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $future[$purchase['user']] = (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllOutstanding 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $supply_total = $this->AllSupplyTotals(array_keys($payment_totals));
    $outstanding = [];
    foreach ($payment_totals as $user => $payments) {
      $total = 0;
      if (isset($purchase_totals[$user])) {
        $total = $purchase_totals[$user];
        if (isset($future[$user])) {
          $total -= $future[$user];
        }
        if (isset($supply_total[$user])) {
          $total -= $supply_total[$user];
        }
      }
      $outstanding[$user] = $total - $payments;
    }
    return $outstanding;
  }

  public function AddPurchase($user, $timestamp, $product, $supplier,
                              $quantity, $price, $base_price, $id, $volunteer) {
    if ($user === '' || $timestamp === 0 || $product === '' ||
        $supplier === '' || $quantity === '' || $price === '' ||
        $base_price === '') {
      return ['error' => 'Purchase details missing'];
    }

    // Check if purchasing a composite item, an entry in the purchase_composite
    // table isn't enough because sub-items might not have been added yet.
    $stock = new Stock($this->user, $this->owner);
    $composite = $stock->Composite($product, $supplier);
    $purchase = [];
    $mysqli = connect_db();
    // The purchase_group entered needs to be the system_group of the supplier.
    $purchase_group = '';
    $query = 'SELECT system_group FROM users WHERE user = "' . $supplier . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($users = $mysqli_result->fetch_assoc()) {
        $purchase_group = $users['system_group'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AddPurchase 1: ' . $mysqli->error);
    }

    if ($composite) {
      // The user could be purchasing more than one composite item.
      $composite_quantity = $quantity;
      // Also the price given is for one composite item, convert this to the
      // total price.
      $price = round($price * $composite_quantity, 2);
      // The purchase_composite table only provides quantites of each product,
      // need to check stock availability for current prices and supplier.
      $values = '';
      $composite_total = 0;
      $decrease = [];
      $banking = new Banking($this->user, $this->owner);
      $buyer_group = $banking->BuyerGroup($user);
      $available = $stock->AvailableProducts(false);
      $query = 'SELECT name, quantity FROM purchase_composite WHERE ' .
        'user = "' . $supplier . '" AND composite = "' . $product . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($purchase_composite = $mysqli_result->fetch_assoc()) {
          $name = $purchase_composite['name'];
          $quantity = $composite_quantity *
            (float)$purchase_composite['quantity'];
          if (isset($available[$name])) {
            $item_supplier = $available[$name]['user'];
            $item_price = $available[$name][$buyer_group];
            $base_price = $available[$name]['price'];
            $grower = $available[$name]['grower'];
            if ($values !== '') $values .= ',';
            $values .= '("' . $user . '", ' . $timestamp . ', ' .
              '"' . $name .'", "' . $item_supplier . '", "' . $grower . '", ' .
              $quantity . ', ' . $item_price . ', ' . $base_price . ', ' .
              '"' . $purchase_group . '", ' . $id . ', "' . $volunteer . '")';
            $total = round($quantity * $item_price, 2);
            $composite_total += $total;
            $purchase[] = ['date' => $timestamp * 1000, 'user' => $user,
                           'name' => $name, 'supplier' => $item_supplier,
                           'grower' => $grower, 'quantity' => $quantity,
                           'price' => $item_price,
                           'total' => price_string($total)];
            if (!isset($decrease[$item_supplier])) {
              $decrease[$item_supplier] = [];
            }
            $decrease[$item_supplier][$name] = $quantity;
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Purchase->AddPurchase 2: ' . $mysqli->error);
      }
      if ($values !== '') {
        $purchases_exist = false;
        // Check that purchases don't already exist for this user and timestamp
        // otherwise there will be duplicate key and stock tracking issues.
        $query = 'SELECT name FROM purchase WHERE user = "' . $user . '" AND ' .
          'timestamp = ' . $timestamp;
        if ($mysqli_result = $mysqli->query($query)) {
          if ($mysqli_result->num_rows > 0) {
            $purchases_exist = true;
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Purchase->AddPurchase 3: ' . $mysqli->error);
        }
        if ($purchases_exist) {
          $mysqli->close();
          return ['error' => 'Purchases already exist for this user and date.'];
        }
        // Add the discount item to this set of purchases, so that the total
        // price is the same as the composite item's price.
        if ($composite_total > $price) {
          $name = $product . ' discount';
          $item_price = price_string($price - $composite_total);
          $values .= ', ("' . $user . '", ' . $timestamp . ', ' .
            '"' . $name . '", "' . $supplier . '", "", 1, ' .
            $item_price . ', ' . $item_price . ', "' . $purchase_group . '", ' .
            $id . ', "' . $volunteer . '")';
          $purchase[] = ['date' => $timestamp * 1000, 'user' => $user,
                         'name' => $name, 'supplier' => $supplier,
                         'grower' => '', 'quantity' => 1,
                         'price' => $item_price, 'total' => $item_price];
          // And add the discount item to stock tracking.
          if (!isset($decrease[$supplier])) {
            $decrease[$supplier] = [];
          }
          $decrease[$supplier][$name] = 1;
        }
        $query = 'INSERT INTO purchase VALUES ' . $values;
        if (!$mysqli->query($query)) {
          $this->Log('Purchase->AddPurchase 4: ' . $mysqli->error);
        }
        $stock->Decrease($decrease);
      }
    }
    else {
      // If updating the quantity of an existing purchase, find the amount to
      // decrease stock by. Exact timestamp matches here are a quantity
      // adjustment, otherwise the purchase is in addition to the existing one.
      $decrease = 0;
      $current_quantity = 0;
      $query = 'SELECT quantity FROM purchase WHERE user = "' . $user . '" ' .
        'AND name = "' . $product . '" AND timestamp = ' . $timestamp;
      if ($mysqli_result = $mysqli->query($query)) {
        if ($purchase_result = $mysqli_result->fetch_assoc()) {
          $current_quantity = (float)$purchase_result['quantity'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Purchase->AddPurchase 5: ' . $mysqli->error);
      }
      if ($current_quantity === 0) {
        // When not updating an existing purchase, check for any other purchases
        // for this item on the given date. (The Purchase module currently can't
        // handle more than one purchase per user and item on a given date, as
        // the second entry is ignored during collation. The quantity for the
        // item at the existing timestamp is updated instead.)
        $start = strtotime(date('F j Y 00:00:00', $timestamp));
        $end = $start + 86399;
        $query = 'SELECT quantity, timestamp FROM purchase WHERE ' .
          'user = "' . $user . '" AND name = "' . $product . '" AND ' .
          'timestamp >= ' . $start . ' AND timestamp <= ' . $end;
        if ($mysqli_result = $mysqli->query($query)) {
          if ($purchase_result = $mysqli_result->fetch_assoc()) {
            $current_quantity = (float)$purchase_result['quantity'];
            $timestamp = (int)$purchase_result['timestamp'];
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Purchase->AddPurchase 6: ' . $mysqli->error);
        }
        // Need to decrease stock by the new amount here, and the purchase
        // table needs to be updated with the combined amount.
        $decrease = $quantity;
        $quantity += $current_quantity;
      }
      else {
        $decrease = $quantity - $current_quantity;
      }
      $grower = $mysqli->escape_string($stock->Grower($product, $supplier));
      $query = 'INSERT INTO purchase VALUES ("' . $user . '", ' .
        $timestamp . ', "' . $product . '", "' . $supplier . '", ' .
        '"' . $grower . '", ' . $quantity . ', ' . $price . ', ' .
        $base_price . ', "' . $purchase_group . '", ' . $id . ', ' .
        '"' . $volunteer . '") ON DUPLICATE KEY UPDATE ' .
        'supplier = "' . $supplier . '", quantity = ' . $quantity . ', ' .
        'price = ' . $price . ', base_price = ' . $base_price;
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->AddPurchase 7: ' . $mysqli->error);
      }
      if ($product !== 'surcharge') {
        $stock->Decrease([$supplier => [$product => $decrease]]);
      }
    }
    $mysqli->close();

    // Don't automatically update the surcharge when manually entered, or if
    // this group doesn't use a surcharge.
    if ($product === 'surcharge' || $this->Substitute('surcharge') !== 'true') {
      if ($composite) return ['composite' => true, 'purchase' => $purchase];
      return ['done' => true];
    }
    if ($composite) {
      // Add the updated surcharge to the list if applicable.
      $surcharge = $this->UpdateSurcharge($user, $timestamp);
      if (!isset($surcharge['done'])) $purchase[] = $surcharge;
      return ['composite' => true, 'purchase' => $purchase];
    }
    return $this->UpdateSurcharge($user, $timestamp);
  }

  public function RemovePurchase($user, $timestamp,
                                 $product, $supplier, $quantity) {
    if ($user === '' || $timestamp === 0) return;

    $mysqli = connect_db();
    $query = 'DELETE FROM purchase WHERE user = "' . $user . '" AND ' .
      'name = "' . $product . '" AND timestamp = ' . $timestamp . ' AND ' .
      'supplier = "' . $supplier . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->RemovePurchase: ' . $mysqli->error);
    }
    $mysqli->close();
    
    if ($product !== 'surcharge') {
      $stock = new Stock($this->user, $this->owner);
      $stock->Increase([$supplier => [$product => $quantity]]);
    }

    // Don't automatically update the surcharge when manually entered, or if
    // this group doesn't use a surcharge.
    if ($product === 'surcharge' || $this->Substitute('surcharge') !== 'true') {
      return ['done' => true];
    }
    return $this->UpdateSurcharge($user, $timestamp);
  }

  public function RemoveAllPurchases() {
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day contains commas, assume this is multiple weekdays in which
    // case the first day listed is used.
    if (strpos($co_op_day, ',') !== false) {
      $co_op_day = strstr($co_op_day, ',', true);
    }

    // Only remove purchases around the time of the next co-op-day (ie this
    // weeks orders).
    $next_co_op = (int)strtotime($co_op_day);
    if ($next_co_op < time()) {
      $next_co_op = (int)strtotime('next ' . $co_op_day);
    }
    // next_co_op will be false if co-op-day is not a day name and the 'next'
    // prefix is used. This shouldn't happen because the calendar date should
    // be in the future when removing orders before purchasing, but it is
    // checked for here just in case.
    if (!$next_co_op) {
      $this->Log('Purchase->RemoveAllPurchases 1: next co-op-day not set.');
      return;
    }

    // Check if only removing purchases where the quota wasn't met.
    $check_quota = $this->Substitute('purchase-check-quota') === 'true';
    $stock = new Stock($this->user, $this->owner);

    $mysqli = connect_db();
    $products = [];
    $name_query = '';
    $start = $next_co_op;
    $end = $next_co_op + 86399;
    $query = 'SELECT name, supplier, SUM(quantity) AS quantity FROM purchase ' .
      'WHERE purchase_group = "' . $this->user->group . '" AND ' .
      'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
      ' GROUP BY name, supplier';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $name = $purchase['name'];
        $supplier = $purchase['supplier'];
        $quantity = (float)$purchase['quantity'];
        if ($check_quota && !$stock->Quota($name, $supplier, $quantity)) {
          if (!isset($products[$supplier])) {
            $products[$supplier] = [];
          }
          $products[$supplier][$name] = $quantity;
          // Add to query for delete below.
          if ($name_query !== '') {
            $name_query .= ' OR ';
          }
          $name_query .= 'name = "' . $name . '"';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->RemoveAllPurchases 2: ' . $mysqli->error);
    }
    $stock->Increase($products);

    if ($name_query !== '') {
      $name_query = '(' . $name_query . ') AND ';
    }
    $query = 'DELETE FROM purchase WHERE ' . $name_query .
      'purchase_group = "' . $this->user->group . '" AND ' .
      'timestamp >= ' . $start . ' AND timestamp <= ' . $end;
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->RemoveAllPurchases 3: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function UpdateOrders($name, $supplier, $base_price,
                               $wholesale_price, $retail_price) {
    $banking = new Banking($this->user, $this->owner);
    list($users, $buyer_groups) = $banking->AllBuyers(true);
    // Need to group users into their buying groups to update orders with
    // the correct price for each user.
    $base_query = '';
    $wholesale_query = '';
    $retail_query = '';
    foreach ($buyer_groups as $user => $group) {
      if ($group === 'price') {
        if ($base_query !== '') {
          $base_query .= ' OR ';
        }
        $base_query .= 'user = "' . $user . '"';
      }
      else if ($group === 'wholesale') {
        if ($wholesale_query !== '') {
          $wholesale_query .= ' OR ';
        }
        $wholesale_query .= 'user = "' . $user . '"';
      }
      if ($group === 'retail') {
        if ($retail_query !== '') {
          $retail_query .= ' OR ';
        }
        $retail_query .= 'user = "' . $user . '"';
      }
    }

    $mysqli = connect_db();
    if ($base_query !== '') {
      $query = 'UPDATE purchase SET price = ' . $base_price . ', ' .
        'base_price = ' . $base_price . ' WHERE name = "' . $name . '"' .
        ' AND supplier = "' . $supplier . '" AND timestamp > ' . time() .
        ' AND (' . $base_query . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->UpdateOrders 1: ' . $mysqli->error);
      }
    }
    if ($wholesale_query !== '') {
      $query = 'UPDATE purchase SET price = ' . $wholesale_price . ', ' .
        'base_price = ' . $base_price . ' WHERE name = "' . $name . '"' .
        ' AND supplier = "' . $supplier . '" AND timestamp > ' . time() .
        ' AND (' . $wholesale_query . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->UpdateOrders 2: ' . $mysqli->error);
      }
    }
    if ($retail_query !== '') {
      $query = 'UPDATE purchase SET price = ' . $retail_price . ', ' .
        'base_price = ' . $base_price . ' WHERE name = "' . $name . '"' .
        ' AND supplier = "' . $supplier . '" AND timestamp > ' . time() .
        ' AND (' . $retail_query . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->UpdateOrders 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function UpdatePurchases($name, $old_name, $supplier) {
    $mysqli = connect_db();
    $query = 'UPDATE purchase SET name = "' . $name . '" WHERE ' .
      'name = "' . $old_name . '" AND supplier = "' . $supplier . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->UpdatePurchases: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function UpdateTimestamp($old_timestamp, $new_timestamp) {
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $query = 'UPDATE purchase SET timestamp = ' . $new_timestamp . ' WHERE ' .
      $organiser->PurchaseQuery() . ' AND timestamp >= ' .
      strtotime(date('F j Y 00:00:00', $old_timestamp)) . ' AND timestamp <= ' .
      strtotime(date('F j Y 23:59:59', $old_timestamp));
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->UpdateTimestamp: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function NewUser($user = '') {
    if ($user === '') {
      $user = $this->user->name;
    }
    $mysqli = connect_db();
    $query = 'INSERT INTO purchase_totals VALUES ("' . $user . '", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->NewUser: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function AllMostRecent($timestamp, $organisation = false) {
    $group_query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $group_query = $organiser->GroupQuery();
    }
    else {
      $group_query = 'purchase_group = "' . $this->user->group . '"';
    }
    $recent = [];
    $mysqli = connect_db();
    $query = 'SELECT purchase.user, MAX(timestamp) AS timestamp FROM purchase '.
      'LEFT JOIN users ON purchase.user = users.user WHERE users.user ' .
      'NOT LIKE "buyer\_%" AND active = 1 AND timestamp < ' . $timestamp .
      ' AND ' . $group_query . ' GROUP BY user ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $recent[$purchase['user']] = $purchase['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllMostRecent: ' . $mysqli->error);
    }
    $mysqli->close();
    return $recent;
  }

  public function Attendance($start, $end, $organisation) {
    $group_query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $group_query = $organiser->GroupQuery();
    }
    else {
      $group_query = 'purchase_group = "' . $this->user->group . '"';
    }
    $attendance = 0;
    $mysqli = connect_db();
    $query = 'SELECT DISTINCT purchase.user FROM purchase LEFT JOIN users ' .
      'ON purchase.user = users.user WHERE users.user NOT LIKE "buyer\_%" ' .
      'AND ' . $group_query . ' AND timestamp >= ' . $start . ' AND ' .
      'timestamp <= ' . $end;
    if ($mysqli_result = $mysqli->query($query)) {
      $attendance = $mysqli_result->num_rows;
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Attendance: ' . $mysqli->error);
    }
    $mysqli->close();
    return $attendance;
  }

  public function Search($user, $timestamp, $product, $supplier, $id = 0,
                         $quantity = '', $price = '', $start = 0, $end = 0,
                         $group = false, $taxable = 'all', $limit = false) {
    $price_query = 'price';
    $order_query = 'ORDER BY user, timestamp DESC';
    // Allow the export to be sorted by product (same as the orders email).
    if (!$group && $this->Substitute('invoice-group-format') === 'product') {
      $order_query = 'ORDER BY name, timestamp DESC';
    }

    $export_description = false;
    $all_users = [];
    // This is used by groups that put information such as a box number in
    // the users details when creating accounts.
    if ($this->Substitute('purchase-export-description') === 'true') {
      $export_description = true;
      $detail = new Detail($this->user, $this->owner);
      $all_users = $detail->AllUsers();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $group_query = $organiser->GroupQuery();
    $export_units = $this->Substitute('purchase-export-units') === 'true';
    $stock = new Stock($this->user, $this->owner);
    $all_taxable = $taxable !== 'all' ? $stock->AllTaxable() : [];
    $all_products = [];
    // Limit is true when searching based on the current purchase group only.
    if ($limit) {
      $invite = new Invite($this->user, $this->owner);
      if (isset($_SESSION['purchase-group']) &&
          in_array($_SESSION['purchase-group'], $invite->Created())) {
        $all_products = $stock->AllProducts(false, $_SESSION['purchase-group']);
        $group_query = 'purchase_group = "' . $_SESSION['purchase-group'] . '"';
      }
      else {
        $all_products = $stock->AllProducts(false, $this->user->group);
      }
    }
    else if ($export_units) {
      $all_products = $stock->AllProducts(false);
    }

    $search = '';
    // Need to specify table for user query due to join.
    if ($user !== '') {
      $search .= 'purchase.user = "' . $user . '"';
    }
    if ($timestamp !== 0) {
      if ($search !== '') $search .= ' AND ';
      $timestamp = strtotime(date('F j Y 00:00:00', (int)$timestamp));
      $search .= 'timestamp >= ' . $timestamp . ' AND timestamp <= ' .
        ($timestamp + 86399);
    }
    if ($product !== '') {
      if ($search !== '') $search .= ' AND ';
      $search .= 'name = "' . $product . '"';
    }
    if ($supplier !== '') {
      if ($search !== '') $search .= ' AND ';
      // Allow supplier to be a comma separated list of usernames.
      if (strpos($supplier, ',') !== false) {
        $supplier_search = '';
        foreach (explode(',', $supplier) as $supplier_name) {
          if ($supplier_search !== '') {
            $supplier_search .= ' OR ';
          }
          $supplier_search .= 'supplier = "' . trim($supplier_name) . '"';
        }
        $search .= '(' . $supplier_search . ')';
      }
      else {
        $search .= 'supplier = "' . $supplier . '"';
      }
      // When searching for a supplier and no user was given, show the
      // base_price which is the amount the supplier receives. (Only useful
      // when results are grouped, otherwise individual purchases look wrong.)
      if ($group && $user === '') {
        $price_query = 'base_price';
        $order_query = 'ORDER BY name, timestamp DESC';
      }
    }
    if ($id !== 0) {
      if ($search !== '') $search .= ' AND ';
      $search .= 'id = ' . $id;
    }
    if ($quantity !== '' && $quantity !== '0') {
      if ($search !== '') $search .= ' AND ';
      $search .= 'quantity = ' . (float)$quantity;
    }
    else {
      // When quantity is not explicitly set don't match zero quantities.
      if ($search !== '') $search .= ' AND ';
      $search .= 'quantity != 0';
    }
    if ($price !== '') {
      if ($search !== '') $search .= ' AND ';
      $search .= 'price = ' . (float)$price;
    }
    if ($start !== 0) {
      if ($search !== '') $search .= ' AND ';
      $search .= 'timestamp >= '.strtotime(date('F j Y 00:00:00', (int)$start));
    }
    if ($end !== 0) {
      if ($search !== '') $search .= ' AND ';
      $search .= 'timestamp <= ' . strtotime(date('F j Y 23:59:59', (int)$end));
    }
    // If no search terms were provided, avoid returning everything.
    if ($search === 'quantity != 0') return [];

    $result = [];
    $day_total = 0;
    $day_quantity = 0;
    $day_count = 0;
    $prev_day = -1;
    $prev_user = '';
    $prev_product = '';
    $prev_price = '';
    $prev_supplier = '';
    $prev_timestamp = 0;
    $mysqli = connect_db();
    $query = 'SELECT purchase.user, timestamp, name, supplier, quantity, ' .
      $price_query . ' AS price FROM purchase LEFT JOIN users ON ' .
      'purchase.user = users.user WHERE ' . $group_query . ' AND ' .
      $search . ' ' . $order_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $current_supplier = $purchase['supplier'];
        $current_product = $purchase['name'];
        if ($taxable === 'taxable' &&
            !isset($all_taxable[$current_supplier][$current_product])) {
          continue;
        }
        else if ($taxable === 'not-taxable' &&
                 isset($all_taxable[$current_supplier][$current_product])) {
          continue;
        }
        // Filter out purchases that are not part of the current purchase group.
        if ($limit &&
            !isset($all_products[$current_supplier][$current_product])) {
          continue;
        }

        $current_user = $purchase['user'];
        $current_price = (float)$purchase['price'];
        $quantity = (float)$purchase['quantity'];
        $total = price_string($quantity * $current_price);
        $timestamp = (int)$purchase['timestamp'] * 1000;
        if ($group) {
          $day = (int)date('z', (int)$purchase['timestamp']);
          // Split totals on product change if a user is not specified but a
          // supplier is. Split on user change if a supplier is not specified.
          if ($day !== $prev_day || ($user === '' &&
              $supplier !== '' && $current_product !== $prev_product) ||
              ($supplier === '' && $current_user !== $prev_user)) {
            // Allow negative totals for refunds.
            if ($day_total > 0.001 || $day_total < -0.001) {
              $display_user = $user;
              $display_name = '';
              $display_price = '';
              $display_supplier = '';
              $display_quantity = '';
              // When supplier is not provided purchases are grouped by user,
              // otherwise purchases are grouped by product name.
              if ($user !== '' || $supplier === '') {
                $display_user = $prev_user;
              }
              else if ($supplier !== '') {
                $display_name = $prev_product;
                $display_supplier = $prev_supplier;
                $display_quantity = $day_quantity === 0 ? '' : $day_quantity;
              }
              // When there's only one purchase can display extra details.
              if ($day_count === 1) {
                $display_user = $prev_user;
                $display_name = $prev_product;
                $display_price = price_string($prev_price);
                $display_supplier = $prev_supplier;
                $display_quantity = $day_quantity === 0 ? '' : $day_quantity;
              }
              $result[] = ['date' => $prev_timestamp,
                           'user' => $display_user,
                           'name' => $display_name,
                           'supplier' => $display_supplier,
                           'quantity' => $display_quantity,
                           'price' => $display_price,
                           'total' => price_string($day_total)];
              $day_total = 0;
              $day_quantity = 0;
              $day_count = 0;
            }
          }
          $prev_day = $day;
          $prev_user = $current_user;
          $prev_product = $current_product;
          $prev_price = $current_price;
          $prev_supplier = $current_supplier;
          $prev_timestamp = $timestamp;
          $day_total += round($quantity * $current_price, 2);
          $day_quantity += $quantity;
          $day_count++;
        }
        else {
          $row = ['date' => $timestamp, 'user' => $current_user];
          // If the description is being added, it needs to be after the user.
          if ($export_description) {
            $row['number'] = isset($all_users[$current_user]) ?
              $all_users[$current_user]['description'] : '';
          }
          $row['name'] = $current_product;
          $row['supplier'] = $current_supplier;
          $row['quantity'] = $quantity;
          // If units are being added, it needs to be after quantity.
          if ($export_units) {
            $row['unit'] = '';
            $row['category'] = '';
            if (isset($all_products[$current_supplier][$current_product])) {
              $row['unit'] =
                $all_products[$current_supplier][$current_product]['unit'];
              $row['category'] =
                $all_products[$current_supplier][$current_product]['category'];
            }
          }
          $row['price'] = price_string($current_price);
          $row['total'] = $total;
          $result[] = $row;
        }
      }
      if ($group && ($day_total > 0.001 || $day_total < -0.001)) {
        $display_user = $user;
        $display_name = '';
        $display_price = '';
        $display_supplier = '';
        $display_quantity = '';
        if ($user !== '' || $supplier === '') {
          $display_user = $prev_user;
        }
        else {
          $display_name = $prev_product;
          $display_supplier = $prev_supplier;
          $display_quantity = $day_quantity === 0 ? '' : $day_quantity;
        }
        if ($day_count === 1) {
          $display_user = $prev_user;
          $display_name = $prev_product;
          $display_price = price_string($prev_price);
          $display_supplier = $prev_supplier;
          $display_quantity = $day_quantity === 0 ? '' : $day_quantity;
        }
        $result[] = ['date' => $prev_timestamp,
                     'user' => $display_user,
                     'name' => $display_name,
                     'supplier' => $display_supplier,
                     'quantity' => $display_quantity,
                     'price' => $display_price,
                     'total' => price_string($day_total)];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Search: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function Total($user = '', $start = 0, $end = 0,
                        $id = 0, $exclude_group = false) {
    if ($user === '') $user = $this->user->name;
    $total = 0;
    $mysqli = connect_db();
    // If start and end dates are given, don't need to subtract future
    // purchases. Also supply totals and surcharge are calculated separately.
    if ($start !== 0 && $end !== 0) {
      $exclude_query = '';
      if ($exclude_group) {
        // This is used to show a total for all purchase groups, but the
        // purchase page calculates the total for the current group.
        $exclude_query = ' AND purchase_group != "' . $this->user->group . '"';
      }
      $id_query = '';
      if ($id !== 0) $id_query = ' AND id = ' . $id;
      $query = 'SELECT SUM(ROUND(quantity * price, 2)) AS total FROM ' .
        'purchase WHERE user = "' . $user . '" AND name != "surcharge" AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        $exclude_query . $id_query;
      if ($mysqli_result = $mysqli->query($query)) {
        if ($purchase = $mysqli_result->fetch_assoc()) {
          $total = (float)$purchase['total'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Purchase->Total 1: ' . $mysqli->error);
      }
      $mysqli->close();
      return $total;
    }

    $query = 'SELECT amount FROM purchase_totals WHERE user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase_totals = $mysqli_result->fetch_assoc()) {
        $total = (float)$purchase_totals['amount'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Total 2: ' . $mysqli->error);
    }
    // Subtract future purchases.
    $future = 0;
    $query = 'SELECT SUM(ROUND(quantity * price, 2)) AS total FROM purchase ' .
      'WHERE user = "' . $user . '" AND timestamp > ' . time();
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase = $mysqli_result->fetch_assoc()) {
        $future = (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Total 3: ' . $mysqli->error);
    }
    $mysqli->close();
    // Also check if this user is a supplier of any stock, and subtract sold
    // items from the user's total.
    $stock = new Stock($this->user, $this->owner);
    return $total - $future - $this->SupplyTotal($user);
  }

  public function SaveSales($payment_method) {
    $time = time();
    $user = '';
    $total = 0;
    $count = 0;
    $existing_user = false;
    $us_data = json_decode($_POST['data'], true);
    $us_user_index = json_decode($_POST['userIndex'], true);
    $us_new_sales = json_decode($_POST['newSales'], true);
    $start = strtotime('00:00:00');
    $end = $start + 86399;
    $current = $this->AllData($start, $end, false, false, false);
    $payment = new Payment($this->user, $this->owner);
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    // Get available usernames for this sales data.
    do {
      $exists = false;
      $query = 'SELECT user FROM users WHERE user LIKE "buyer\_' . $time . '%"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows > 0) {
          $exists = true;
          $time++;
        }
        $mysqli_result->close();
      }
    } while ($exists);

    foreach ($us_data as $us_index => $us_values) {
      // Need to know when at the last entry in the array.
      $count++;

      if ($us_index === '') continue;
      if (!is_array($us_values)) continue;

      $total = 0;
      $purchase_count = count($us_values);
      if ($purchase_count > 0) {
        $user = '';
        $existing_user = false;
        // Check if a username was given, otherwise create one from the index.
        if (isset($us_user_index[$us_index])) {
          $user = $mysqli->escape_string($us_user_index[$us_index]);
          if ($user !== '' && !$organiser->MatchUser($user)) {
            $this->Log('Purchase->SaveSales 1: User ' . $user . ' not found.');
            continue;
          }
          $existing_user = true;
        }
        if ($user === '') {
          $user = 'buyer_' . $time . '_' . $mysqli->escape_string($us_index);
          $query = 'INSERT INTO users VALUES ("' . $user . '", "", "", ' .
            '"' . $this->user->group . '", 0, "", ' . $time . ', "", 1)';
          if (!$mysqli->query($query)) {
            $this->Log('Purchase->SaveSales 2: ' . $mysqli->error);
          }
          $this->NewUser($user);
          $payment->NewUser($user, false);
          $existing_user = false;
        }
        if (!isset($us_new_sales[$us_index]) && isset($current[$user])) {
          $total = $this->UpdateSalesData($user, $us_values, $current[$user]);
        }
        else {
          $total = $this->UpdateSalesData($user, $us_values);
        }
        // The last payment is saved separately below to check the payment
        // method, which only applies to the last user (the rest are cash).
        if ($count < count($us_data) && ($total > 0 || $total < 0)) {
          $payment->Save($user, $time, $total);
        }
      }
    }
    $mysqli->close();

    if ($total > 0 || $total < 0) {
      // An existing user can delay payment by leaving the purchase as a
      // balance against their account, which they will be invoiced for.
      if (!$existing_user && $payment_method === 'account') {
        $payment_method = 'Error: Payment method \'account\' selected, ' .
          'only existing users can be invoiced.';
      }
      if ($payment_method !== 'account') {
        $payment->Save($user, $time, $total, $payment_method);
      }
    }
  }

  public function SupplyTotal($user, $start = 0, $end = 0,
                              $product = '', $organisation = true) {
    $supply_total = 0;
    $query = '';
    $product_query = '';
    if ($product !== '') {
      $product_query = 'AND name = "' . $product . '" ';
    }
    $group_query = '';
    if (!$organisation) {
      $group_query = 'AND purchase_group = "' . $this->user->group . '" ';
    }
    if ($start === 0 && $end === 0) {
      $query = 'SELECT SUM(ROUND(quantity * base_price, 2)) AS total FROM ' .
        'purchase WHERE supplier = "' . $user . '" ' . $product_query . ' ' .
        $group_query . 'AND timestamp < ' . time();
    }
    else {
      $query = 'SELECT SUM(ROUND(quantity * base_price, 2)) AS total FROM ' .
        'purchase WHERE supplier = "' . $user . '" ' . $product_query . ' ' .
        $group_query . 'AND timestamp >= ' . $start . ' AND timestamp <= '.$end;
    }
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase = $mysqli_result->fetch_assoc()) {
        $supply_total = (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->SupplyTotal: ' . $mysqli->error);
    }
    $mysqli->close();
    return $supply_total;
  }

  public function OrderingAvailable($next_co_op = 0, $order_gap = 0) {
    // Calculate opening and closing times for the order.
    $pre_order_open = $this->Substitute('pre-order-open');
    $pre_order_open_time = $this->Substitute('pre-order-open-time');
    if ($pre_order_open !== '' && $pre_order_open_time !== '') {
      $pre_order_open .= ', ' . $pre_order_open_time;
    }
    $open = strtotime($pre_order_open);
    // If $open is greater than the current time, use the previous
    // timestamp for this day and time.
    if ($open && $open > time()) {
      $open = strtotime('last ' . $pre_order_open);
    }
    $pre_order_final = $this->Substitute('pre-order-final');
    $pre_order_final_time = $this->Substitute('pre-order-final-time');
    // Want to know if ordering is weekly (ie not a calendar date).
    $pre_order_weekly = false;
    if ($pre_order_final !== '') {
      $pre_order_weekly = preg_match('/[0-9]/', $pre_order_final) === 0;
    }
    if ($pre_order_final !== '' && $pre_order_final_time !== '') {
      $pre_order_final .= ', ' . $pre_order_final_time;
    }
    $final = strtotime($pre_order_final);
    // If $final is less than the current time, use the next timestamp
    // for this day and time.
    if ($final && $final < time()) {
      $final = strtotime('next ' . $pre_order_final);
    }
    // Now go through options. If $next_co_op parameter is not given, return a
    // boolean, otherwise return an array which will set:
    //   unavailable, unavailableText, countdown, dateInfo.
    // First case: Opening and closing times aren't required.
    if ($pre_order_open === '' && $pre_order_final === '') {
      if ($next_co_op === 0) return true;
      // When pre-order-final isn't used, a countdown can still be used
      // based on the order gap calculated above. Also reduce the countdown
      // by a minute so that the formatted date info doesn't fall exactly on
      // midnight of the next co-op day.
      $countdown = $next_co_op - time() - $order_gap - 60;
      list($show_countdown, $date_info) =
        $this->DateInfo($next_co_op, $countdown);
      return [false, '', $show_countdown, $date_info];
    }
    // If both are provided they must both be valid.
    if ($final && $open) {
      // If ordering is done weekly, $final - $open must be less than a week.
      if ($pre_order_weekly && ($final - $open > 7 * 86400)) {
        if ($next_co_op === 0) return false;
        return [true, $this->Substitute('pre-order-unavailable'), false, ''];
      }
      if ($next_co_op === 0) return true;
      list($show_countdown, $date_info) =
        $this->DateInfo($next_co_op, $final - time());
      return [false, '', $show_countdown, $date_info];
    }
    // If only one value is provided, it must be valid.
    if ($pre_order_final === '' && $pre_order_open !== '' && $open) {
      if ($next_co_op === 0) return true;
      $countdown = $next_co_op - time() - $order_gap;
      list($show_countdown, $date_info) =
        $this->DateInfo($next_co_op, $countdown);
      return [false, '', $show_countdown, $date_info];
    }
    if ($pre_order_open === '' && $pre_order_final !== '' && $final) {
      if ($next_co_op === 0) return true;
      list($show_countdown, $date_info) =
        $this->DateInfo($next_co_op, $final - time());
      return [false, '', $show_countdown, $date_info];
    }
    if ($next_co_op === 0) {
      return false;
    }
    return [true, $this->Substitute('pre-order-unavailable'), false, ''];
  }

  public function PurchasingAvailable($co_op_day = '') {
    $start = 0;
    if ($co_op_day === '') {
      $co_op_day = $this->Substitute('co-op-day');
    }
    // If $co_op_day contains commas, assume this is multiple weekdays
    // and split it into an array and check if date('l') is in it.
    if (strpos($co_op_day, ',') !== false) {
      $today = date('l');
      if (in_array($today, explode(',', $co_op_day))) {
        $start = strtotime($today);
      }
    }
    else {
      $start = strtotime($co_op_day);
    }
    $end = $start + 86399;
    if ($start > time() || $end < time()) return false;
    
    // Purchasing is available, but also need to check if this group should
    // force order mode on co-op-day.
    if ($this->Substitute('purchase-force-order') === 'true') {
      // If order mode is forced, the only way to override it is if the
      // current user has permission to use the purchase page as a volunteer.
      if ($this->GroupMember('purchase-volunteer', 'admin')) {
        return true;
      }
      return false;
    }
    return true;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AllAvailableCategories() {
    $purchase_categories = $this->Substitute('purchase-categories');
    if ($purchase_categories === '') return '';

    $categories = explode(',', $purchase_categories);
    $count = count($categories);
    $controlgroup = '<div id="purchase-categories">';
    $checked = ' checked="checked"';
    for ($i = 0; $i < $count; $i++) {
      $controlgroup .= '<input type="radio" id="purchase-category-' . $i . '"' .
          ' name="radio"' . $checked . '>' .
        '<label for="purchase-category-' . $i . '">' . trim($categories[$i]) .
        '</label>';
      $checked = '';
    }
    $other = $this->Substitute('purchase-category-other');
    if ($other === '') $other = 'Other';
    $controlgroup .= '<input type="radio" id="purchase-category-' . $i . '"' .
        ' name="radio">' .
      '<label for="purchase-category-' . $i . '">' . $other . '</label></div>';
    return $controlgroup;
  }

  private function AllSupplyTotals($suppliers, $start = 0, $end = 0) {
    if (count($suppliers) === 0) return [];

    $supply_total = [];
    $supplier_query = '';
    for ($i = 0; $i < count($suppliers); $i++) {
      if ($supplier_query !== '') {
        $supplier_query .= ' OR ';
      }
      $supplier_query .= 'supplier = "' . $suppliers[$i] . '"';
    }
    $query = '';
    if ($start === 0 && $end === 0) {
      $query = 'SELECT supplier, SUM(ROUND(quantity * base_price, 2)) AS ' .
        'total FROM purchase WHERE (' . $supplier_query . ') AND ' .
        'timestamp < ' . time() . ' GROUP BY supplier';
    }
    else {
      $query = 'SELECT supplier, SUM(ROUND(quantity * base_price, 2)) AS ' .
        'total FROM purchase WHERE (' . $supplier_query . ') AND ' .
        'timestamp >= ' . $start . ' AND timestamp <= ' . $end .
        ' GROUP BY supplier';
    }
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $supply_total[$purchase['supplier']] = (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AllSupplyTotals: ' . $mysqli->error);
    }
    $mysqli->close();
    return $supply_total;
  }

  private function CartSales($payment_method, $data, $pack_date) {
    $payment = new Payment($this->user, $this->owner);
    $user = '';
    $time = time();
    if ($this->user->loggedIn) {
      $user = $this->user->name;
    }
    else {
      $mysqli = connect_db();
      // Get an available username for this sales data.
      do {
        $exists = false;
        $query = 'SELECT user FROM users WHERE user LIKE "buyer\_' . $time.'%"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($mysqli_result->num_rows > 0) {
            $exists = true;
            $time++;
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Purchase->CartSales 1: ' . $mysqli->error);
        }
      } while ($exists);
      // This is the same format as used by SaveSales.
      $user = 'buyer_' . $time . '_0';
      $query = 'INSERT INTO users VALUES ("' . $user . '", "", "", ' .
        '"' . $this->user->group . '", 0, "", ' . $time . ', "", 1)';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->CartSales 2: ' . $mysqli->error);
      }
      $mysqli->close();

      $this->NewUser($user);
      $payment->NewUser($user, false);
    }
    $total = $this->UpdateSalesData($user, $data, null, $pack_date,
                                    $this->NextId());
    if ($payment_method !== 'account' && $total > 0) {
      $payment->Save($user, $time, $total, $payment_method);
    }
  }

  private function CurrentTime() {
    if ($_SESSION['purchase-group-changed']) {
      return ['error' => 'Session expired: reloading page.'];
    }
    if ($_SESSION['purchase-order-check']) {
      $_SESSION['purchase-order-check'] = false;
      return ['save' => false];
    }

    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    // Return milliseconds for use with javascript Date object.
    $current = ['time' => time() * 1000, 'save' => true,
                'group' => $this->user->group];
    // A refresh can be forced so that the client can see new purchases.
    // This is done by setting the stock update time to the current time
    // to trigger the client.
    if ($_SESSION['purchase-refresh']) {
      $current['update'] = time() * 1000;
      // Refresh is also used when a user is placing an order, but want to
      // make sure it's the same user, so send the current username here.
      if ($_SESSION['purchase-order']) {
        $current['username'] = $this->user->name;
        // Need to tell the client if ordering is no longer available.
        // In ordering mode timestamp is set from next_co_op.
        $timestamp = isset($_POST['timestamp']) ?
          (int)$_POST['timestamp'] / 1000 : 0;
        if ($timestamp === 0) {
          $current['save'] = false;
        }
        else {
          $order_gap = (int)$this->Substitute('purchase-order-gap') * 86400;
          $ordering = $this->OrderingAvailable($timestamp, $order_gap);
          // OrderingAvailable returns an array, with the first value being
          // a boolean for *unavailable*
          $current['save'] = !$ordering[0];
        }
      }
      else {
        $current['username'] = '';
      }
    }
    else {
      // Return the last time the stock table was updated in case the
      // client's data is out of date, so they know they need to refresh.
      $stock = new Stock($this->user, $this->owner);
      $current['update'] = $stock->LastUpdate() * 1000;
      $current['username'] = '';
      $mysqli = connect_db();
      // If a new user was just added use the registration time as the latest
      // update rather than the last stock update.
      $query = 'SELECT MAX(registration_time) AS registration_time FROM ' .
        'users WHERE system_group = "' . $this->user->group . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($users = $mysqli_result->fetch_assoc()) {
          $registration_time = $users['registration_time'] * 1000;
          if ($registration_time > $current['update']) {
            $current['update'] = $registration_time;
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Purchase->CurrentTime: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    $this->user->group = $default_group;
    return $current;
  }

  private function DateInfo($date, $countdown = 0) {
    $group = $this->Substitute('group-name');
    // A purchase date more than 6 months away means a closing date hasn't been
    // set for the order yet.
    if ($date > time() + 86400 * 182) {
      return [false, 'Ordering from <b>' . $group . '</b>.'];
    }
    $purchase_date = date('l j F', $date);
    // When $countdown is not provided page is in purchase mode.
    if ($countdown === 0) {
      return $this->Substitute('purchase-date-info',
                               ['/!group/', '/!purchase/'],
                               [$group, $purchase_date]);
    }
    if ($countdown > 86400) {
      $close = 'on ' . date('l j F \a\t ga', $countdown + time());
      $info = $this->Substitute('purchase-order-info',
                                ['/!group/', '/!purchase/', '/!close/'],
                                [$group, $purchase_date, $close]);
      return [false, $info];
    }
    $hours = (int)floor($countdown / 3600);
    $minutes = (int)floor(($countdown - ($hours * 3600)) / 60);
    $close = 'in <span class="purchase-countdown">';
    if ($hours === 1) $close .= '1 hour ';
    else if ($hours > 1) $close .= $hours . ' hours ';
    if ($minutes === 1) $close .= '1 minute</span>';
    else $close .= $minutes . ' minutes</span>';
    $info = $this->Substitute('purchase-order-info',
                              ['/!group/', '/!purchase/', '/!close/'],
                              [$group, $purchase_date, $close]);
    return [$countdown, $info];
  }

  private function ListProducts() {
    if ($_SESSION['purchase-group-changed']) {
      return ['error' => 'Session expired: reloading page.'];
    }

    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    $today = date('l');
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day contains commas, assume this is multiple weekdays.
    $multiple_days = [];
    if (strpos($co_op_day, ',') !== false) {
      $multiple_days = explode(',', $co_op_day);
      // In order mode, the first day listed is used.
      $co_op_day = $multiple_days[0];
      // In purchase mode, preference is given to today if listed.
      if (!$_SESSION['purchase-order']) {
        if (in_array($today, $multiple_days)) {
          $co_op_day = $today;
        }
      }
    }
    // Need to find the next co-op date to set to. If today is co-op-day
    // find the date for next week. This date is used by both pre-order
    // mode to set the current date, and (possibly also) purchase mode to set
    // the date for next week so purchases can be added with that date.
    $next_co_op = strtotime($co_op_day);
    if ($next_co_op && $next_co_op < strtotime('23:59:59')) {
      $next_co_op = strtotime('next ' . $co_op_day);
      // If there are multiple days try them instead of using next week, which
      // will be used as a fallback if required. Go through each day and use
      // the closest day, skipping today.
      foreach ($multiple_days as $available) {
        if ($today !== $available) {
          $timestamp = strtotime($available);
          if ($timestamp < $next_co_op) $next_co_op = $timestamp;
        }
      }
    }
    if (!$next_co_op && $_SESSION['purchase-order']) {
      $this->user->group = $default_group;
      // This can happen for co-ops that order every fortnight.
      return ['unavailable' => true,
              'unavailableText' => $this->Substitute('pre-order-unavailable')];
    }

    $detail = new Detail($this->user, $this->owner);
    $result = ['suppliers' => $detail->AllSuppliers()];
    // Lists of users and products are made to autocomplete and verify input.
    $banking = new Banking($this->user, $this->owner);
    list($result['users'], $result['buyerGroup']) = $banking->AllBuyers();

    $stock = new Stock($this->user, $this->owner);
    $payment = new Payment($this->user, $this->owner);
    // Need to get the current values for the 'info' and 'warning'
    // debt levels to alert the user if there is a problem.
    list($result['info'], $result['warning']) = $payment->LevelSettings();
    $result['categories'] = [];
    // purchase-categories are stored escaped whereas stock categories are not.
    $purchase_categories =
      htmlspecialchars_decode($this->Substitute('purchase-categories'));
    if ($purchase_categories !== '') {
      foreach (explode(',', $purchase_categories) as $category) {
        $result['categories'][] = trim($category);
      }
    }

    // When purchase-order is set, the user wants to order for next week,
    // so show their username, default date of next co-op-day and load only
    // their data. Also don't show any previous purchases so set time to the
    // next co-op date.
    if ($_SESSION['purchase-order']) {
      $result['products'] = $stock->AvailableProducts(true, false);
      // There is also a timeframe in which the order must be placed.
      // If the current time is too close to the next co-op-day, ordering
      // can now only happen for the following week. (ie it's already after
      // pre-order-final). Allow this 'order-gap' to be set for the group,
      // as a number of days which are then converted to seconds.
      $order_gap = (int)$this->Substitute('purchase-order-gap') * 86400;
      if ($next_co_op < strtotime('00:00:00') + $order_gap) {
        $check_next_co_op = strtotime('next ' . $co_op_day);
        if (!$check_next_co_op) {
          // This can occur if co-op-day is a calendar date, but not further
          // away than purchase-order-gap.
          $this->user->group = $default_group;
          return ['unavailable' => true,
                  'unavailableText' =>
                     $this->Substitute('pre-order-unavailable')];
        }
        // It's possible that the date hasn't changed, so manually add a week.
        if ($check_next_co_op === $next_co_op) {
          $next_co_op += 7 * 86400;
        }
        else {
          $next_co_op = $check_next_co_op;
        }
      }

      // Check opening and closing dates.
      list($result['unavailable'], $result['unavailableText'],
           $result['countdown'], $result['dateInfo']) =
        $this->OrderingAvailable($next_co_op, $order_gap);
      // Some users can order for other people. In that case need to look up
      // all data rather than just the data for the current user.
      $user_query = '';
      if ($this->GroupMember('purchase-other-order')) {
        $result['data'] = $this->AllData($next_co_op, 0, true);
        // Skip these users in the quota query below as we already have enough
        // information for them to display in quotas.
        foreach ($result['data'] as $user => $data) {
          $user_query .= ' AND user != "' . $user . '"';
        }
      }
      else {
        $result['data'] = [$this->user->name => $this->Data($next_co_op)];
        $result['total'] = [$this->user->name =>
                              $this->Total($this->user->name, $next_co_op,
                                           $next_co_op + 86399, 0, true)];
        $user_query = ' AND user != "' . $this->user->name . '"';
      }
      // Also look up quantities for other users so quotas can be calculated.
      // Note that quotas are only available in order mode, though they are
      // also shown in the 'all users' dialog to help purchasing. In that case
      // though, the quotas are for the group only, not a whole organisation,
      // which is what is queried here.
      $organiser = new Organiser($this->user, $this->owner);
      $mysqli = connect_db();
      $query = 'SELECT user, name, quantity FROM purchase WHERE ' .
        $organiser->PurchaseQuery() . ' AND timestamp >= ' . $next_co_op .
        $user_query . ' AND name != "surcharge" AND quantity != 0';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($purchase = $mysqli_result->fetch_assoc()) {
          $user = $purchase['user'];
          $quantity = (float)$purchase['quantity'];
          if (!isset($result['data'][$user])) {
            $result['data'][$user] = [];
          }
          $result['data'][$user][] = ['name' => $purchase['name'],
                                      'quantity' => $quantity];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Purchase->ListProducts: ' . $mysqli->error);
      }
      $mysqli->close();

      // Cast data to object required for json in case of an empty array.
      if (count($result['data']) === 0) {
        $result['data'] = (object)[];
      }
      // Return milliseconds for use with javascript Date object.
      $result['date'] = $next_co_op * 1000;
      $result['username'] = $this->user->name;
      $result['details'] = [$this->user->name =>
                              $detail->User($this->user->name)];
      // Look up how much this user currently owes.
      $result['outstanding'] = $this->Outstanding($payment->Total());
    }
    else {
      $result['products'] = $stock->AvailableProducts();
      // Otherwise in normal purchasing mode, only show purchases from today's
      // date onwards, as only these can be changed in UpdateData.
      $start = strtotime('00:00:00');
      $end = 0;
      // An end timestamp is required when pre-order is true, otherwise
      // orders for next week will be shown when purchasing this week.
      if ($this->Substitute('pre-order') === 'true') {
        $end = strtotime('48 hours');
      }
      // Cast to object required for json in case of an empty array.
      $result['data'] = (object)$this->AllData($start, $end, true);
      // Return milliseconds for use with javascript Date object.
      $result['date'] = time() * 1000;
      $result['dateInfo'] = $this->DateInfo(strtotime($co_op_day));
      $result['username'] = '';
      $result['details'] = $detail->AllUsers(false, true);
      // Get a list of payment totals for all users, so that they can be
      // compared to purchase totals to find outstanding debts.
      $payment_totals = $payment->AllTotals();
      // Create a list of how much each user currently owes. Recent purchases
      // are not included to give the user a chance to pay their account.
      // Cast to object required for json in case of an empty array.
      $result['outstanding'] =
        (object)$this->AllOutstanding($payment_totals, strtotime('-24 hours'));
      if ($this->Substitute('roster-reminder') === 'true') {
        $roster = new Roster($this->user, $this->owner);
        $result['roster'] = $roster->Reminders();
      }
    }

    // Provide the purchase group so we can check when it changes.
    $result['group'] = $this->user->group;
    $result['nextWeek'] = [];
    $result['processed'] = [];
    // Need wholesale and retail percent to calculate base price for variably
    // priced products.
    $result['wholesalePercent'] =
      (float)$this->Substitute('stock-wholesale-percent');
    $result['retailPercent'] = (float)$this->Substitute('stock-retail-percent');
    // When 'stock-limited' is true, need to ensure that purchases are only
    // made up to the existing quantity for each item.
    $result['stockLimited'] = $this->Substitute('stock-limited') === 'true';
    if (!$this->user->active) {
      $result['unavailable'] = true;
      $result['unavailableText'] = 'Your account has been made inactive.' .
        '<br><br>Please contact an administrator to re-activate your account.';
    }
    $this->user->group = $default_group;
    return $result;
  }

  private function NextId() {
    $id = 0;
    $mysqli = connect_db();
    $query = 'SELECT MAX(id) AS id FROM purchase WHERE ' .
      'purchase_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase = $mysqli_result->fetch_assoc()) {
        $id = (int)$purchase['id'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->NextId: ' . $mysqli->error);
    }
    $mysqli->close();
    return $id + 1;
  }

  private function RemoveFromOrder() {
    if (!$this->user->active) {
      return ['error' => 'Inactive account.'];
    }
    if ($_SESSION['purchase-group-changed']) {
      return ['error' => 'Session expired: reloading page.'];
    }

    $default_group = $this->user->group;
    $joined = [];
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $organiser = new Organiser($this->user, $this->owner);
    $result = [];
    $mysqli = connect_db();
    $buyer = $mysqli->escape_string($_POST['buyer']);
    if ($organiser->MatchUser($buyer, $joined)) {
      $name = $mysqli->escape_string($_POST['name']);
      // Timestamps are in milliseconds in javascript, convert to seconds.
      // Need to look at a timestamp range due to rounding.
      $timestamp = (int)$_POST['timestamp'] / 1000;
      // Find the quantity for this purchase to increase stock.
      $query = 'SELECT supplier, quantity, timestamp FROM purchase WHERE ' .
        'user = "' . $buyer . '" AND timestamp <= ' . ($timestamp + 1) .' AND '.
        'timestamp >= ' . ($timestamp - 1) . ' AND name = "' . $name . '" AND '.
        'purchase_group = "' . $this->user->group . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($purchase = $mysqli_result->fetch_assoc()) {
          $supplier = $purchase['supplier'];
          $quantity = (float)$purchase['quantity'];
          $timestamp = (int)$purchase['timestamp'];
          $stock = new Stock($this->user, $this->owner);
          $stock->Increase([$supplier => [$name => $quantity]]);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Purchase->RemoveFromOrder 1: ' . $mysqli->error);
      }
      // Instead of deleting the purchase, quantity is set to zero. This is to
      // allow timestamp comparisons in UpdateData as a removed purchase could
      // otherwise be re-added when purchases are saved from another computer.
      $query = 'UPDATE purchase SET quantity = 0 WHERE user = "' . $buyer .'" '.
        'AND timestamp = ' . $timestamp . ' AND name = "' . $name . '" AND ' .
        'purchase_group = "' . $this->user->group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->RemoveFromOrder 2: ' . $mysqli->error);
      }
      $result['done'] = true;
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function SaveDetails() {
    $organiser = new Organiser($this->user, $this->owner);
    $result = [];
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    if ($organiser->MatchUser($username)) {
      $first = $mysqli->escape_string(htmlspecialchars($_POST['first']));
      $last = $mysqli->escape_string(htmlspecialchars($_POST['last']));
      $phone = $mysqli->escape_string(htmlspecialchars($_POST['phone']));
      $detail = new Detail($this->user, $this->owner);
      $detail->UpdateUser($username, $first, $last, '', $phone);
      $result['done'] = true;
    }
    else {
      $result['error'] = 'User not found';
    }
    $mysqli->close();
    return $result;
  }

  private function SaveOrders() {
    if (!$this->user->active) {
      return ['error' => 'Inactive account.'];
    }
    if ($_SESSION['purchase-group-changed']) {
      return ['error' => 'Session expired: reloading page.'];
    }
    if (!isset($_POST['timestamp'])) {
      return ['error' => 'Error saving purchase data: timestamp not set'];
    }

    $default_group = $this->user->group;
    $joined = [];
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
      $invite = new Invite($this->user, $this->owner);
      $joined = $invite->Joined();
    }
    $timestamp = (int)$_POST['timestamp'] / 1000;
    // Need to make sure ordering is available, because the user may have left
    // the page open and the order window has since closed. (And the countdown
    // which reloads the page automatically doesn't always get set.)
    if ($_SESSION['purchase-order']) {
      $order_gap = (int)$this->Substitute('purchase-order-gap') * 86400;
      $ordering = $this->OrderingAvailable($timestamp, $order_gap);
      // OrderingAvailable returns an array, with the first value being a
      // boolean for *unavailable*
      if ($ordering[0]) {
        $this->user->group = $default_group;
        return ['error' => 'Session expired: reloading page.'];
      }
    }

    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    // Need to be careful of duplicate purchases for the same user and item
    // here, (because a user could update the quantity for an item at a
    // different computer.) Can check for this by comparing the timestamp
    // associated with the item, and only keep the most recent one.
    // A further complication however is that at some point the new data
    // shouldn't be considered a duplicate, but a new purchase. Make this
    // cut-off the same day as the given timestamp.
    $start = strtotime(date('F j Y 00:00:00', $timestamp));
    $end = $start + 86399;
    $user_not_found = false;
    $current = $this->AllData($start, $end, false, false, false);
    $us_data = json_decode($_POST['data'], true);
    foreach ($us_data as $us_user => $us_values) {
      if ($us_user === '') continue;
      if (!is_array($us_values)) continue;
        
      $user = $mysqli->escape_string($us_user);
      if ($organiser->MatchUser($user, $joined)) {
        if (isset($current[$user])) {
          $this->UpdateData($user, $us_values, $start, $current[$user]);
        }
        else {
          $this->UpdateData($user, $us_values, $start);
        }
      }
      else {
        $user_not_found = true;
      }
    }
    $mysqli->close();
    $this->user->group = $default_group;
    if ($user_not_found) {
      return ['error' => 'error saving purchase data: user not found'];
    }

    $us_next_week = json_decode($_POST['nextWeek'], true);
    $banking = new Banking($this->user, $this->owner);
    // Note that SaveNextWeek only works with the user's default group.
    $banking->SaveNextWeek($us_next_week);
    // When ordering let the user know it has been saved.
    if ($_SESSION['purchase-order']) return ['order' => true];
    return ['done' => true];
  }

  private function UpdateData($user, $us_values, $timestamp, $current = null) {
    $stock = new Stock($this->user, $this->owner);
    $mysqli = connect_db();
    $products = [];
    $elapsed = time() - strtotime('00:00:00');
    $tomorrow = time() + 86400;
    $values = '';
    // Create a list of items that need updating.
    for ($i = 0; $i < count($us_values); $i++) {
      // Not all the fields are set when the quantities are given to only
      // calculate quotas. Can just skip items in this case.
      if (!isset($us_values[$i]['date'])) continue;

      // Timestamps are in milliseconds in javascript, convert to seconds.
      $new_date = (int)$us_values[$i]['date'] / 1000;
      $us_name = $us_values[$i]['name'];
      $name = $mysqli->escape_string($us_name);
      $supplier = $mysqli->escape_string($us_values[$i]['supplier']);
      $grower = $mysqli->escape_string($us_values[$i]['grower']);
      $quantity = (float)$us_values[$i]['quantity'];
      $price = (float)$us_values[$i]['price'];
      $base_price = (float)$us_values[$i]['basePrice'];

      // The current array is the recent (and future) purchases to compare new
      // values to. Anything older than these are ignored below, as it is
      // expected they would already be in the system.
      if (is_array($current) && isset($current[$us_name])) {
        $old_date = $current[$us_name]['date'];
        // If updating an order, don't need to look at which timestamp is
        // greater, always want to use the greater one.
        if ($new_date > $tomorrow && $new_date < $old_date) {
          $new_date = $old_date;
        }
        // Add the new item if it's timestamp is greater than the old one,
        // or older but by more than a day (ie the existing item has a future
        // timestamp).
        if ($new_date >= $old_date ||
            ($new_date < $old_date && $old_date - $new_date > 86400)) {
          if ($values !== '') $values .= ',';
          $values .= '("' . $user . '", ' . $new_date.', "' . $name . '", ' .
            '"' . $supplier . '", "' . $grower . '", ' . $quantity . ', ' .
            $price . ', ' . $base_price . ', "' . $this->user->group . '", ' .
            '0, "' . $this->user->name . '")';

          // Delete the old entry if it has a timestamp from today.
          if ($new_date > $old_date && $new_date - $old_date < $elapsed) {
            // Find the old quantity for this purchase to increase stock.
            // Need to look at a timestamp range due to rounding by 1ms.
            $query = 'SELECT quantity, timestamp FROM purchase WHERE ' .
              'user = "' . $user . '" AND name = "' . $name . '" AND ' .
              'timestamp <= ' . ($old_date + 1) .
              ' AND timestamp >= ' . ($old_date - 1) .
              ' AND supplier = "' . $supplier . '"';
            if ($mysqli_result = $mysqli->query($query)) {
              if ($purchase = $mysqli_result->fetch_assoc()) {
                $old_quantity = $purchase['quantity'];
                $old_date = $purchase['timestamp'];
                $stock->Increase([$supplier => [$name => $old_quantity]]);
              }
              $mysqli_result->close();
            }
            else {
              $this->Log('Purchase->UpdateData 1: ' . $mysqli->error);
            }
            $query = 'DELETE FROM purchase WHERE user = "' . $user . '" AND ' .
              'name = "' . $name . '" AND timestamp = ' . $old_date .
              ' AND supplier = "' . $supplier . '"';
            if (!$mysqli->query($query)) {
              $this->Log('Purchase->UpdateData 2: ' . $mysqli->error);
            }
          }
          else {
            // Check if there's an existing purchase with this timestamp and
            // adjust quantity to be deducted from stock levels.
            $query = 'SELECT quantity FROM purchase WHERE ' .
              'user = "' . $user . '" AND name = "' . $name . '" AND ' .
              'timestamp = ' . $new_date .
              ' AND supplier = "' . $supplier . '"';
            if ($mysqli_result = $mysqli->query($query)) {
              if ($purchase = $mysqli_result->fetch_assoc()) {
                $quantity -= (float)$purchase['quantity'];
              }
              $mysqli_result->close();
            }
            else {
              $this->Log('Purchase->UpdateData 3: ' . $mysqli->error);
            }
          }
          // Add quantity to products array to deduct from stock.
          if (!isset($products[$supplier])) {
            $products[$supplier] = [];
          }
          if (isset($products[$supplier][$name])) {
            $products[$supplier][$name] += $quantity;
          }
          else {
            $products[$supplier][$name] = $quantity;
          }
        }
      }
      // New values to enter must have a recent timestamp.
      else if ($new_date > $timestamp) {
        if ($values !== '') $values .= ',';
        $values .= '("' . $user . '", ' . $new_date . ', "' . $name . '", ' .
          '"' . $supplier . '", "' . $grower . '", ' . $quantity . ', ' .
          $price . ', ' . $base_price . ', "' . $this->user->group . '", ' .
          '0, "' . $this->user->name . '")';
        // Check if there's an existing purchase with this timestamp and
        // adjust quantity to be deducted from stock levels.
        $query = 'SELECT quantity FROM purchase WHERE user = "' . $user . '" ' .
          'AND name = "' . $name . '" AND timestamp = ' . $new_date .
          ' AND supplier = "' . $supplier . '"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($purchase = $mysqli_result->fetch_assoc()) {
            $quantity -= (float)$purchase['quantity'];
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Purchase->UpdateData 4: ' . $mysqli->error);
        }
        // Add quantity to products array to deduct from stock.
        if (!isset($products[$supplier])) {
          $products[$supplier] = [];
        }
        if (isset($products[$supplier][$name])) {
          $products[$supplier][$name] += $quantity;
        }
        else {
          $products[$supplier][$name] = $quantity;
        }
      }
    }
    if ($values !== '') {
      // Only duplicate should be when multiple orders for an item are updated,
      // since the 'current' array only contains the first entry for each item.
      $query = 'INSERT INTO purchase VALUES ' . $values .
        ' ON DUPLICATE KEY UPDATE quantity = VALUES(quantity)';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->UpdateData 5: ' . $mysqli->error);
      }
      $stock->Decrease($products);
    }
    $mysqli->close();
  }

  private function UpdateSalesData($user, $us_values,
                                   $current = null, $timestamp = 0, $id = 0) {
    if ($timestamp === 0) $timestamp = time();
    $stock = new Stock($this->user, $this->owner);
    $products = [];
    $total = 0;
    $values = '';

    $mysqli = connect_db();
    // Create a list of items that need updating.
    for ($i = 0; $i < count($us_values); $i++) {
      $us_name = $us_values[$i]['name'];
      $name = $mysqli->escape_string($us_name);
      $supplier = $mysqli->escape_string($us_values[$i]['supplier']);
      $grower = $mysqli->escape_string($us_values[$i]['grower']);
      $quantity = (float)$us_values[$i]['quantity'];
      $price = (float)$us_values[$i]['price'];
      $base_price = (float)$us_values[$i]['basePrice'];
      $total += round($quantity * $price, 2);

      if ($values !== '') $values .= ',';
      $values .= '("' . $user . '", ' . $timestamp . ', "' . $name . '", ' .
        '"' . $supplier . '", "' . $grower . '", ' . $quantity . ', ' .
        $price . ', ' . $base_price . ', "' . $this->user->group . '", ' .
        $id . ', "' . $this->user->name . '")';
      // The current array is the recent (and future) purchases to compare new
      // values to. Anything older than these are ignored below, as it is
      // expected they would already be in the system.
      if (isset($current[$us_name])) {
        $old_date = $current[$us_name]['date'];
        // Delete the old entry. Find the old quantity for this purchase to
        // increase stock. Need to look at a timestamp range due to rounding.
        $query = 'SELECT quantity, timestamp FROM purchase WHERE ' .
          'user = "' . $user . '" AND name = "' . $name . '"' .
          ' AND timestamp <= ' . ($old_date + 1) .
          ' AND timestamp >= ' . ($old_date - 1) .
          ' AND supplier = "' . $supplier . '"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($purchase = $mysqli_result->fetch_assoc()) {
            $old_quantity = $purchase['quantity'];
            $old_date = $purchase['timestamp'];
            $stock->Increase([$supplier => [$name => $old_quantity]]);
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Purchase->UpdateSalesData 1: ' . $mysqli->error);
        }
        $query = 'DELETE FROM purchase WHERE user = "' . $user . '"' .
          ' AND name = "' . $name . '" AND timestamp = ' . $old_date .
          ' AND supplier = "' . $supplier . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Purchase->UpdateSalesData 2: ' . $mysqli->error);
        }
      }
      // Add quantity to products array to deduct from stock.
      if (!isset($products[$supplier])) {
        $products[$supplier] = [];
      }
      if (isset($products[$supplier][$name])) {
        $products[$supplier][$name] += $quantity;
      }
      else {
        $products[$supplier][$name] = $quantity;
      }
    }
    if ($values !== '') {
      // Only duplicate should be when multiple orders for an item are updated,
      // since the 'current' array only contains the first entry for each item.
      $query = 'INSERT INTO purchase VALUES ' . $values .
        ' ON DUPLICATE KEY UPDATE quantity = VALUES(quantity)';
      if (!$mysqli->query($query)) {
        $this->Log('Purchase->UpdateSalesData 3: ' . $mysqli->error);
      }
      $stock->Decrease($products);
    }
    $mysqli->close();
    return $total;
  }

  private function UpdateSurcharge($user, $timestamp) {
    $total = 0;
    $start = strtotime(date('F j Y 00:00:00', (int)$timestamp));
    $end = $start + 86399;

    $mysqli = connect_db();
    // Note that the surcharge is calculated based on purchases from the
    // day of the given timestamp only.
    $query = 'SELECT SUM(ROUND(quantity * price, 2)) AS total FROM purchase ' .
      'WHERE user = "' . $user . '" AND name != "surcharge" AND ' .
      'timestamp >= ' . $start . ' AND timestamp <= ' . $end;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase = $mysqli_result->fetch_assoc()) {
        $total = (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->UpdateSurcharge 1: ' . $mysqli->error);
    }

    $payment = new Payment($this->user, $this->owner);
    $surcharge_amount = price_string($payment->Surcharge($total));
    if ($surcharge_amount < 0.01) {
      $query = 'DELETE FROM purchase WHERE name = "surcharge" AND ' .
        'user = "' . $user . '" AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end;
    }
    else {
      // Note that a surcharge is not created here if it doesn't currently
      // exist, as members can be flagged to not pay a surcharge, or it can
      // be manually removed if required.
      $query = 'UPDATE purchase SET price = ' . $surcharge_amount . ', ' .
        'base_price = ' . $surcharge_amount . ' WHERE name = "surcharge" AND ' .
        'user = "' . $user . '" AND timestamp >= ' . $start .
        ' AND timestamp <= ' . $end;
    }
    if (!$mysqli->query($query)) {
      $this->Log('Purchase->UpdateSurcharge 2: ' . $mysqli->error);
    }
    $surcharge_timestamp = 0;
    $query = 'SELECT timestamp FROM purchase WHERE ' .
      'name = "surcharge" AND user = "' . $user . '" AND ' .
      'timestamp >= ' . $start . ' AND timestamp <= ' . $end;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase = $mysqli_result->fetch_assoc()) {
        $surcharge_timestamp = (int)$purchase['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->UpdateSurcharge 3: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($surcharge_amount < 0.01 || $surcharge_timestamp === 0) {
      return ['done' => true];
    }
    $stock = new Stock($this->user, $this->owner);
    return ['date' => $surcharge_timestamp * 1000, 'user' => $user,
            'name' => 'surcharge', 'supplier' => $stock->SurchargeSupplier(),
            'quantity' => 1, 'price' => $surcharge_amount,
            'total' => $surcharge_amount];
  }

  private function AddToSold($query, $result = []) {
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($purchase = $mysqli_result->fetch_assoc()) {
        $quantity = (float)$purchase['quantity'];
        $price = (float)$purchase['base_price'];
        $timestamp = (int)$purchase['timestamp'] * 1000;
        $result[] = ['date' => $timestamp,
                     'name' => $purchase['name'],
                     'supplier' => $purchase['supplier'],
                     'quantity' => $quantity,
                     'price' => price_string($price),
                     'total' => price_string($quantity * $price)];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->AddToSold: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function Outstanding($payment_total) {
    $mysqli = connect_db();
    // First get total purchases to compare the payment total to.
    $purchase_total = 0;
    $query = 'SELECT amount FROM purchase_totals WHERE ' .
      'user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($totals = $mysqli_result->fetch_assoc()) {
        $purchase_total = (float)$totals['amount'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Outstanding 1: ' . $mysqli->error);
    }

    // Need to subtract recent and future purchases from totals.
    $query = 'SELECT SUM(ROUND(quantity * price, 2)) AS total FROM purchase ' .
      'WHERE user = "' . $this->user->name . '" AND ' .
      'timestamp > ' . strtotime('-4 hours');
    if ($mysqli_result = $mysqli->query($query)) {
      if ($purchase = $mysqli_result->fetch_assoc()) {
        $purchase_total -= (float)$purchase['total'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Purchase->Outstanding 2: ' . $mysqli->error);
    }
    $mysqli->close();

    // Also need to deduct items sold by this user.
    $purchase_total -= $this->SupplyTotal($this->user->name);
    return [$this->user->name => $purchase_total - $payment_total];
  }

  private function ConnectSettings() {
    $spark = new Module($this->user, $this->owner, 'spark');
    if ($spark->IsInstalled()) {
      return ['settings' =>
                $spark->Factory('ListConnections', $this->user->group)];
    }
    return ['error' => 'Spark module is not installed.'];
  }

  private function ChangeOrder() {
    $result = [];
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $invite = new Invite($this->user, $this->owner);
    if ($group === $this->user->group ||
        in_array($group, $invite->OpenBuyingGroups())) {
      $_SESSION['purchase-group'] = $group;
      $_SESSION['purchase-group-changed'] = true;
      $result['done'] = true;
    }
    else {
      $result['error'] = 'Purchase group is not available.';
    }
    $mysqli->close();
    return $result;
  }

  private function PurchaseGroupSelect() {
    $invite = new Invite($this->user, $this->owner);
    $groups = $invite->OpenBuyingGroups();
    $count = count($groups);
    if ($count === 0) return '';

    // Store the users default group to reset at the end.
    $default_group = $this->user->group;
    $purchase_group = '';
    if (isset($_SESSION['purchase-group'])) {
      $purchase_group = $_SESSION['purchase-group'];
    }
    $selected = '';
    // If there is no selected purchase group show the user's default group.
    if ($purchase_group === $this->user->group || $purchase_group === '') {
      $selected = ' selected="selected"';
    }
    $select = '<div class="purchase-group-select">' .
      '<label for="purchase-available-orders">' .
        $this->Substitute('summary-available-orders') . '</label>' .
      '<select id="purchase-available-orders">' .
        '<option value="' . $this->user->group . '"' . $selected . '>' .
          $this->Substitute('group-name') .
        '</option>';
    for ($i = 0; $i < $count; $i++) {
      $name = $groups[$i];
      $this->user->group = $name;
      $selected = '';
      if ($purchase_group === $name) {
        $selected = ' selected="selected"';
      }
      $select .= '<option value="' . $name . '"' . $selected . '>' .
        $this->Substitute('group-name') . '</option>';
    }
    $select .= '</select></div>';
    $this->user->group = $default_group;
    return $select;
  }

}
