/*global dobrado: true, Slick: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
//
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
//
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.payment) {
  dobrado.payment = {};
}
(function() {

  'use strict';

  // This is a representation of payments in json.
  var payments = [];
  // This is an instance of slick grid, if available on the page.
  var grid = null;
  // This is the currently selected payment from the grid.
  var currentPayment = null;
  // The list of user names.
  var allNames = [];
  // The list of user's references.
  var allReferences = [];
  // The list of inactive accounts.
  var allInactive = [];
  // The contact and bank details of all users.
  var details = {};
  // Import data loaded from a file.
  var importData = [];
  // The current processed row in the import data.
  var currentImport = 0;
  // Store the current offset when paging through payments.
  var offset = 0;

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.payment').length === 0) {
      return;
    }

    $('#payment-details-form').dialog({
      show: true, autoOpen: false, width: 530,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Edit Bank Details',
      create: dobrado.fixedDialog });
    $('#payment-details-form .submit').button().click(editBankDetails);
    $('.payment .default-action').click(savePayment);
    $('.payment .submit').button().click(savePayment);
    $('.payment .remove').button().click(removePayment);
    $('.payment .search').button().click(search);
    $('.payment .back').button().click(showPreviousImport);
    $('.payment .previous').button({ disabled: true }).click(function() {
      listPayments(false); });
    $('.payment .next').button().click(function() { listPayments(true); });
    $('#payment-username-input').val('');
    $('#payment-fullname-input').val('');
    $('#payment-date-input').change(function() {
        currentPayment = null;
        clearSearchDates();
      }).datepicker({ dateFormat: dobrado.dateFormat });
    $('#payment-start-date-input').val('').change(clearDate).datepicker({
      dateFormat: dobrado.dateFormat });
    $('#payment-end-date-input').val('').change(clearDate).datepicker({
      dateFormat: dobrado.dateFormat });
    $('#payment-reference-input').val('');
    $('#payment-toggle-import').click(function() {
      $('.payment .import-wrapper').toggle();
      return false;
    });
    $('#payment-import-input').change(loadImportData);
    $('.payment .toggle-search-options').click(function() {
      clearDate();
      clearSearchDates();
      $('.payment .search-options').toggle();
      return false;
    });

    dobrado.log('Loading payments...', 'info');
    $.post('/php/request.php',
           { request: 'payment', action: 'list',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'payment list request')) {
          return;
        }
        payments = JSON.parse(response);
        if (payments.length === 50) {
          $('.payment .paging').show();
        }
        // If a grid module is on the page load payments into it.
        if ($('.grid').length !== 0) {
          gridSetup();
        }
        loadUserDetails();
      });
  });

  function listPayments(next) {
    if (next) {
      offset += 50;
      dobrado.log('Loading next payments...', 'info');
      $('.payment .previous').button('option', 'disabled', false);
    }
    else {
      offset -= 50;
      dobrado.log('Loading previous payments...', 'info');
    }
    $.post('/php/request.php',
           { request: 'payment', action: 'list', offset: offset,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'listPayments')) {
          return;
        }
        payments = JSON.parse(response);
        if (payments.length < 50) {
          $('.payment .next').button('option', 'disabled', true);
        }
        else {
          $('.payment .next').button('option', 'disabled', false);
        }
        if (offset === 0) {
          $('.payment .previous').button('option', 'disabled', true);
        }
        if (grid) {
          grid.setData(payments);
          grid.updateRowCount();
          grid.render();
          grid.setSelectedRows([]);
        }
        $('.payment .start').text(offset + 1);
        $('.payment .end').text(offset + payments.length);
      });
  }

  function gridSetup() {
    var id = '#' + $('.grid').attr('id');
    var columns = [{ id : 'date', name: 'Date', field: 'date', width: 110,
                     sortable: true, formatter: Slick.Formatters.Timestamp },
                   { id : 'name', name: 'Username', field: 'name', width: 120,
                     sortable: true }];
    if (!dobrado.mobile) {
      columns.push({ id : 'reference', name: 'Reference', field: 'reference',
                     width: 100, sortable: true });
      columns.push({ id : 'comment', name: 'Comment', field: 'comment',
                     width: 250, sortable: true, editor: Slick.Editors.Text });
    }
    columns.push({ id : 'amount', name: 'Amount', field: 'amount', width: 80,
                   sortable: true, formatter: Slick.Formatters.Dollar,
                   editor: Slick.Editors.Float });
    var options = { autoHeight: true, editable: true, forceFitColumns: true };
    grid = dobrado.grid.instance(id, payments, columns, options);
    grid.setSelectionModel(new Slick.RowSelectionModel());
    grid.onClick.subscribe(function(e, item) {
      showPayment(payments[item.row]);
    });
    grid.onSelectedRowsChanged.subscribe(function(e, item) {
      if (item.rows.length === 1) {
        showPayment(payments[item.rows[0]]);
      }
    });
    grid.onCellChange.subscribe(updatePayment);
    grid.onSort.subscribe(function (e, args) {
      payments.sort(function(row1, row2) {
        var field = args.sortCol.field;
        var sign = args.sortAsc ? 1 : -1;
        var value1 = row1[field];
        var value2 = row2[field];
        if (field === 'amount') {
          value1 = parseFloat(value1);
          value2 = parseFloat(value2);
        }
        if (value1 === value2) {
          return 0;
        }
        if (value1 > value2) {
          return sign;
        }
        else {
          return sign * -1;
        }
      });
      grid.invalidate();
    });
  }

  function showDetails(user) {
    // Don't show details during import as the extra content causes buttons
    // to move which disrupts the import process.
    if (currentImport !== 0) return;

    var bank = details.bank[user];
    var contact = details.contact[user];
    // Check for falsiness because json can be 'null' or empty strings here.
    if (bank) {
      var html = '<b>Bank Details:</b><button class="edit">edit</button>';
      if (bank.name && bank.number && bank.bsb) {
        html += '<br>Account Name: ' + bank.name + ', Account Number: ' +
          bank.number + ', BSB: ' + bank.bsb + '<br>';
      }
      $('.payment .bank-details').html(html);
      $('.payment .bank-details .edit').button().click(openDialog);
    }
    else {
      $('.payment .bank-details').html('');
    }

    if (contact && contact.email) {
      var description = '';
      if (contact.phone) {
        description += 'Phone: ' + contact.phone;
      }
      if (description !== '') {
        description += ', ';
      }
      description += 'Email: ' + contact.email + '<br>';
      $('.payment .contact-details').html('<b>Contact Details:</b><br>' +
                                          description);
    }
    else {
      $('.payment .contact-details').html('');
    }
  }

  function openDialog() {
    var user = $('#payment-username-input').val();
    if (user === '') {
      return false;
    }

    var bank = details.bank[user];
    $('#payment-details-reference-input').val(dobrado.decode(bank.reference));
    $('#payment-details-name-input').val(dobrado.decode(bank.name));
    $('#payment-details-number-input').val(dobrado.decode(bank.number));
    $('#payment-details-bsb-input').val(dobrado.decode(bank.bsb));
    $('#payment-details-credit-input').prop('checked', bank.credit);
    $('#payment-details-surcharge-input').prop('checked', bank.surcharge);
    $('#payment-details-deposit-input').prop('checked', bank.deposit);
    $('#payment-details-buyer-group-select').val(bank.buyerGroup);
    $('#payment-details-form').dialog('open');
    return false;
  }

  function editBankDetails() {
    var user = $('#payment-username-input').val();
    if (user === '') {
      return false;
    }

    dobrado.log('Saving bank details...', 'info');
    // Store the new details in case ShowDetails is called again.
    details.bank[user].name = $('#payment-details-name-input').val();
    details.bank[user].number = $('#payment-details-number-input').val();
    details.bank[user].bsb = $('#payment-details-bsb-input').val();
    details.bank[user].credit =
      $('#payment-details-credit-input:checked').length;
    details.bank[user].surcharge =
      $('#payment-details-surcharge-input:checked').length;
    details.bank[user].deposit =
      $('#payment-details-deposit-input:checked').length;
    details.bank[user].buyerGroup =
      $('#payment-details-buyer-group-select').val();

    $.post('/php/request.php',
           { request: 'payment', username: user,
             reference: $('#payment-details-reference-input').val(),
             name: details.bank[user].name, number: details.bank[user].number,
             bsb: details.bank[user].bsb, credit: details.bank[user].credit,
             surcharge: details.bank[user].surcharge,
             deposit: details.bank[user].deposit,
             buyerGroup: details.bank[user].buyerGroup, action: 'editBanking',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'payment editBankDetails')) {
          return;
        }
        // Update details show on the page.
        var html = '<b>Bank Details:</b><button class="edit">edit</button>';
        if (details.bank[user].name !== '' &&
            details.bank[user].number !== '' && details.bank[user].bsb !== '') {
          html += '<br>Account Name: ' + details.bank[user].name +
            ', Account Number: ' + details.bank[user].number +
            ', BSB: ' + details.bank[user].bsb + '<br>';
        }
        $('.payment .bank-details').html(html);
        $('.payment .bank-details .edit').button().click(openDialog);
        $('#payment-details-form').dialog('close');
      });
    return false;
  }

  function usernameUpdate(event, ui) {
    var user = $('#payment-username-input').val();
    var fullname = '';
    $('#payment-reference-input').val('');
    $('#payment-fullname-input').val('');
    // Don't reset fields when displaying import data.
    if (currentImport === 0) {
      $('#payment-date-input').val('');
      $('#payment-amount-input').val('');
      $('#payment-comment').val('');
      clearSearchDates();
    }
    // de-select grid row.
    if (grid) {
      grid.setSelectedRows([]);
    }
    currentPayment = null;
    // ui is set for autocomplete select event, otherwise function called
    // from change event.
    if (ui) {
      user = ui.item.value;
    }
    else if (user !== '' && $.inArray(user, allInactive) !== -1) {
      $('#payment-username-input').val('');
      alert(user + ' is an inactive account.');
      return;
    }
    else if (user !== '' && $.inArray(user, allNames) === -1) {
      $('#payment-username-input').val('');
      alert('Please create an account for: ' + user);
      return;
    }
    $.each(details.bank, function(key, value) {
      if (key === user) {
        $('#payment-reference-input').val(dobrado.decode(value.reference));
        return false;
      }
    });
    $.each(details.contact, function(key, value) {
      if (key === user) {
        if (value.first) {
          fullname = value.first;
        }
        if (value.last) {
          if (fullname !== '') {
            fullname += ' ';
          }
          fullname += value.last;
        }
        $('#payment-fullname-input').val(dobrado.decode(fullname));
        return false;
      }
    });
    showDetails(user);
  }

  function fullnameUpdate(event, ui) {
    var user = '';
    var fullname = $('#payment-fullname-input').val();
    $('#payment-username-input').val('');
    $('#payment-reference-input').val('');
    // Don't reset fields when displaying import data.
    if (currentImport === 0) {
      $('#payment-date-input').val('');
      $('#payment-amount-input').val('');
      $('#payment-comment').val('');
      clearSearchDates();
    }
    // de-select grid row.
    if (grid) {
      grid.setSelectedRows([]);
    }
    currentPayment = null;
    if (ui) {
      fullname = ui.item.value;
    }
    $.each(details.contact, function(key, value) {
      var check = value.first;
      if (value.last !== '') {
        if (check !== '') {
          check += ' ';
        }
        check += value.last;
      }
      if (fullname === dobrado.decode(check)) {
        user = key;
        return false;
      }
    });
    if (user !== '') {
      $('#payment-username-input').val(user);
      $.each(details.bank, function(key, value) {
        if (key === user) {
          $('#payment-reference-input').val(dobrado.decode(value.reference));
          return false;
        }
      });
      showDetails(user);
    }
  }

  function referenceUpdate(event, ui) {
    var user = '';
    var fullname = '';
    var reference = $('#payment-reference-input').val();
    $('#payment-username-input').val('');
    $('#payment-fullname-input').val('');
    // Don't reset fields when displaying import data.
    if (currentImport === 0) {
      $('#payment-date-input').val('');
      $('#payment-amount-input').val('');
      $('#payment-comment').val('');
      clearSearchDates();
    }
    // de-select grid row.
    if (grid) {
      grid.setSelectedRows([]);
    }
    currentPayment = null;
    if (ui) {
      reference = ui.item.value;
    }
    $.each(details.bank, function(key, value) {
      if (value.reference === reference) {
        user = key;
        $('#payment-username-input').val(user);
        return false;
      }
    });
    if (user !== '') {
      $.each(details.contact, function(key, value) {
        if (key === user) {
          if (value.first) {
            fullname = value.first;
          }
          if (value.last) {
            if (fullname !== '') {
              fullname += ' ';
            }
            fullname += value.last;
          }
          $('#payment-fullname-input').val(dobrado.decode(fullname));
          return false;
        }
      });
      showDetails(user);
    }
  }

  function loadUserDetails() {
    dobrado.log('Loading user details...', 'info');
    $.post('/php/request.php',
           { request: 'payment', action: 'details',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'payment bankDetails')) {
          return;
        }
        details = JSON.parse(response);
        $.each(details.bank, function(key, value) {
          if (value.active === 1) {
            allNames.push(key);
            allReferences.push(value.reference);
          }
          else {
            allInactive.push(key);
          }
        });
        var fullname = '';
        var fullnameList = [];
        $.each(details.contact, function(key, value) {
          fullname = '';
          if (value.first) {
            fullname = dobrado.decode(value.first);
          }
          if (value.last) {
            if (fullname !== '') {
              fullname += ' ';
            }
            fullname += dobrado.decode(value.last);
          }
          if (fullname !== '') {
            fullnameList.push(fullname);
          }
        });
        $('#payment-username-input').autocomplete({ minLength: 1,
                                                    search: dobrado.fixAutoCompleteMemoryLeak,
                                                    source: allNames,
                                                    select: usernameUpdate });
        $('#payment-username-input').change(usernameUpdate);
        $('#payment-fullname-input').autocomplete({ minLength: 1,
                                                    search: dobrado.fixAutoCompleteMemoryLeak,
                                                    source: fullnameList,
                                                    select: fullnameUpdate });
        $('#payment-fullname-input').change(fullnameUpdate);
        $('#payment-reference-input').autocomplete({ minLength: 1,
                                                     search: dobrado.fixAutoCompleteMemoryLeak,
                                                     source: allReferences,
                                                     select: referenceUpdate });
        $('#payment-reference-input').change(referenceUpdate);
      });
  }

  function showPayment(item) {
    var fullname = '';
    // Exit import mode when the user clicks on an existing payment row.
    if (currentImport !== 0) {
      exitImport('');
    }
    currentPayment = item;
    // Update the form based on the item given.
    $('#payment-username-input').val(item.name);
    $('#payment-date-input').val(dobrado.formatDate(item.date));
    $('#payment-reference-input').val(dobrado.decode(item.reference));
    $('#payment-amount-input').val(item.amount);
    $('#payment-comment').val(dobrado.decode(item.comment));
    $('#payment-fullname-input').val('');
    clearSearchDates();

    $.each(details.contact, function(key, value) {
      if (key === item.name) {
        if (value.first) {
          fullname = value.first;
        }
        if (value.last) {
          if (fullname !== '') {
            fullname += ' ';
          }
          fullname += value.last;
        }
        if (fullname !== '') {
          $('#payment-fullname-input').val(dobrado.decode(fullname));
        }
        return false;
      }
    });
    showDetails(item.name);
  }

  function savePayment() {
    var name = $('#payment-username-input').val();
    var timestamp = parseInt($.datepicker.formatDate('@',
      $('#payment-date-input').datepicker('getDate')), 10);
    var reference = $('#payment-reference-input').val();
    var amount = $('#payment-amount-input').val();
    var comment = $('#payment-comment').val();
    var selectedRow = 0;

    if (name === '') {
      alert('Please select a username.');
      return false;
    }

    var newPayment = false;
    // If a payment has been selected from the grid, use the matching timestamp.
    if (currentPayment) {
      timestamp = currentPayment.date;
    }
    else {
      newPayment = true;
    }

    dobrado.log('Saving payment', 'info');
    $.post('/php/request.php',
           { request: 'payment', action: 'edit', username: name,
             timestamp: timestamp, reference: reference,
             amount: amount, comment: comment,
             newPayment: newPayment, importMode: currentImport !== 0,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'save payment')) {
          return;
        }
        var current = JSON.parse(response);
        if (newPayment) {
          if (current.exists) {
            alert('Current transaction already exists.\n' +
                  'Exiting import mode.');
            exitImport('');
            return;
          }
          // When a new payment is entered a timestamp is returned to make
          // sure this transaction is unique in the grid.
          timestamp = current.timestamp;
        }
        var newItem = true;
        // Update the json representation of this payment.
        $.each(payments, function(i, item) {
          if (item.name === name && item.date === timestamp) {
            payments[i].reference = reference;
            payments[i].comment = comment;
            payments[i].amount = amount;
            newItem = false;
            return false;
          }
        });
        // Otherwise add the new payment to the list.
        if (newItem) {
          payments.push({ date: timestamp, name: name, reference: reference,
                          comment: comment, amount: amount });
        }
        // Sort the payments by date.
        payments.sort(function(row1, row2) {
          var value1 = row1.date;
          var value2 = row2.date;
          return (value1 === value2 ? 0 : (value1 > value2 ? -1 : 1));
        });
        // Find the row in the new sort order to set the selection.
        $.each(payments, function(i, item) {
          if (item.name === name && item.date === timestamp) {
            selectedRow = i;
            return false;
          }
        });
        // Update the grid if available.
        if (grid) {
          // Need to call setData if the array was previously empty,
          // or when updated due to assignment to payments variable.
          // It also appears that pushing a new item to the array and then
          // sorting it doesn't make the new row visible, so call setData
          // every time the array is updated.
          $('.grid').show();
          grid.setData(payments);
          grid.updateRowCount();
          grid.render();
          // Don't update selected row during import, as it triggers showPayment
          if (currentImport === 0) {
            grid.setSelectedRows([selectedRow]);
            grid.scrollRowIntoView(selectedRow);
          }
        }

        if (currentImport === 0) {
          resetForm();
        }
        else {
          showImportData();
        }
      });
    return false;
  }

  function removePayment() {
    // Also use the remove button to skip the current transaction
    // when an import is in progress.
    if (currentImport !== 0) {
      showImportData();
      return false;
    }

    dobrado.log('Removing payment', 'info');
    var name = $('#payment-username-input').val();
    var timestamp = parseInt($.datepicker.formatDate('@',
      $('#payment-date-input').datepicker('getDate')), 10);
    if (currentPayment) {
      timestamp = currentPayment.date;
    }
    $.post('/php/request.php',
           { request: 'payment', action: 'remove', name: name,
             timestamp: timestamp, offset: offset,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'payment remove request')) {
          return;
        }
        // When an item is removed, a new payments list is returned.
        payments = JSON.parse(response);
        // Update the grid if available, which references payments.
        if (grid) {
          grid.setData(payments);
          grid.updateRowCount();
          grid.render();
          grid.setSelectedRows([]);
        }
        if ($('#payment-comment').val() ===
            'Automatic deposit deduction for new user.') {
          alert('Please uncheck \'Paid Deposit\' for this member, ' +
                'if applicable');
        }
        resetForm();
      });
    return false;
  }

  function search() {
    var timestamp = parseInt($.datepicker.formatDate('@',
      $('#payment-date-input').datepicker('getDate')), 10);
    if (!timestamp) {
      timestamp = '';
    }
    var start = parseInt($.datepicker.formatDate('@',
      $('#payment-start-date-input').datepicker('getDate')), 10);
    if (!start) {
      start = '';
    }
    var end = parseInt($.datepicker.formatDate('@',
      $('#payment-end-date-input').datepicker('getDate')), 10);
    if (!end) {
      end = '';
    }
    var exportData = $('#payment-export-data:checked').length;

    dobrado.log('Searching...', 'info');
    $.post('/php/request.php',
           { request: 'payment', username: $('#payment-username-input').val(),
             timestamp: timestamp, start: start, end: end,
             group: $('#payment-group-input:checked').length,
             exportData: exportData, action: 'search',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'payment search')) {
          return;
        }
        var data = JSON.parse(response);
        payments = data.search;
        if (payments.length === 0) {
          $('.payment .bank-details').html('No data found for search.');
          $('.grid').hide();
          return;
        }
        // Update the grid if available, which references payments.
        if (grid) {
          $('.grid').show();
          grid.setData(payments);
          grid.updateRowCount();
          grid.render();
          grid.setSelectedRows([]);
        }
        if (exportData) {
          // Redirect to the download.
          location.href = '/php/private.php?file=' + data.filename;
        }
      });
    return false;
  }

  function updatePayment(e, args) {
    var item = args.item;
    dobrado.log('Updating payment', 'info');
    $.post('/php/request.php',
           { request: 'payment', action: 'edit', username: item.name,
             timestamp: item.date, reference: item.reference,
             amount: item.amount, comment: item.comment,
             newPayment: false, importMode: false,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'update payment')) {
          return;
        }
        $.each(payments, function(i, current) {
          if (current.name === item.name && current.date === item.date) {
            payments[i].comment = item.comment;
            payments[i].amount = item.amount;
            return false;
          }
        });
        if (grid) {
          grid.setData(payments);
          grid.updateRowCount();
          grid.render();
        }
      });
  }

  function clearDate() {
    $('#payment-date-input').val('');
  }

  function clearSearchDates() {
    $('#payment-start-date-input').val('');
    $('#payment-end-date-input').val('');
  }

  function loadImportData() {
    importData = [];
    currentImport = 0;

    if (!window.FileReader) {
      alert('Sorry your browser doesn\'t support reading files.');
      $('#payment-import-input').val('');
      return;
    }

    var file = $('#payment-import-input').get(0).files[0];
    var reader = new FileReader();

    reader.onload = function(e) {
      var data = e.target.result.match(/^(.*)[\r\n]*$/gm);
      if (!data) {
        alert('Imported file format doesn\'t match');
        $('#payment-import-input').val('');
        return;
      }
      // Go through each of the bank import routines looking for a match.
      if (importData.length === 0) {
        processMecuData(data);
      }
      if (importData.length === 0) {
        processBoqData(data);
      }
      if (importData.length === 0) {
        processHeritageData(data);
      }
      if (importData.length === 0) {
        processSuncorpData(data);
      }
      // There is no header row for Bendigo or Kiwibank formats, so keep them
      // last so they don't get used for other formats (that might match). 
      if (importData.length === 0) {
        processBendigoData(data);
      }
      if (importData.length === 0) {
        processKiwibankData(data);
      }
      if (importData.length === 0) {
        processASBData(data);
      }
      if (importData.length === 0) {
        processNABData(data);
      }
      if (importData.length === 0) {
        processNABData2(data);
      }
      if (importData.length === 0) {
        alert('Couldn\'t process data from imported file.');
        $('#payment-import-input').val('');
      }
      else {
        $('.import .info').html('<br>Transactions found in imported data ' +
                                'file. Processing <b>1/' + importData.length +
                                '</b>:');
        // Re-use the remove button to skip through imported transactions.
        $('.payment .remove').button('option', 'label', 'skip');
        // Also show a navigation button to go back through import data.
        $('.payment .back').show();
        $('.payment .paging').hide();
        showImportData();
      }
    };
    reader.readAsText(file);
  }

  function cleanLine(item) {
    // Before checking the line format, need to check for any quotes, remove
    // commas between the quotes, and then remove the quotes themselves.
    item = item.replace(/\".*?\"/g, function(match) {
      match = match.replace(/,/g, '');
      return match.replace(/\"/g, '');
    });
    // Split on commas but first remove newlines.
    var re = /^(.*)[\r\n]*$/;
    var match = item.match(re);
    var row = [];
    if (match.length === 2) {
      row = match[1].split(',');
    }
    return row;
  }

  function processMecuData(data) {
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length >= 4) {
        if (i === 0) {
          // The first row should be the header, make sure it matches.
          if (row[0] !== 'Effective Date' || row[1] !== 'Entered Date' ||
              row[2] !== 'Transaction Description' || row[3] !== 'Amount') {
            return false;
          }
        }
        else {
          // Effective Date is ignored because it is not always found in a row,
          // and the year given for Entered Date is 2 digits, change to 4.
          row[1] = row[1].replace(/\/([0-9]{2})$/, '/20' + '$1');
          importData.push({date: row[1], description: row[2], amount: row[3]});
        }
      }
    });
  }

  function processBoqData(data) {
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length === 6) {
        if (i === 0) {
          // The first row should be the header, make sure it matches, but
          // first remove any spaces.
          for (var j = 0; j < 6; j++) {
            row[j] = row[j].replace(/\s/g, '');
          }
          if (row[0] !== 'Date' || row[1] !== 'Description' ||
              row[2] !== 'Account' || row[3] !== 'Debit' ||
              row[4] !== 'Credit' || row[5] !== 'Balance') {
            return false;
          }
        }
        else {
          // The year given is 2 digits, change to 4.
          row[0] = row[0].replace(/\/([0-9]{2})$/, '/20' + '$1');
          // If Debit column contains a value enter it as a negative amount.
          if (row[3] !== '') {
            // Remove the dollar symbol and any commas from the amount.
            row[3] = row[3].replace(/\$/g, '');
            row[3] = row[3].replace(/,/g, '');
            importData.push({ date: row[0], description: row[1],
                              amount: '-' + row[3] });
          }
          else if (row[4] !== '') {
            // Remove the dollar symbol and any commas from the amount.
            row[4] = row[4].replace(/\$/g, '');
            row[4] = row[4].replace(/,/g, '');
            importData.push({ date: row[0], description: row[1],
                              amount: row[4] });
          }
        }
      }
    });
  }

  function processHeritageData(data) {
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length === 3) {
        if (i === 0) {
          // The first row should be the header, make sure it matches.
          if (row[0] !== 'Date' || row[1] !== 'Amount' ||
              row[2] !== 'Description') {
            return false;
          }
        }
        else {
          importData.push({date: row[0], description: row[2], amount: row[1]});
        }
      }
    });
  }

  function processSuncorpData(data) {
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (i === 0) {
        // The first row should be the header, make sure it matches.
        if (row[0] !== 'Account History for Account:') {
          return false;
        }
      }
      else if (row.length === 4) {
        // Remove the dollar symbol and any commas from the amount.
        row[2] = row[2].replace(/\$/g, '');
        row[2] = row[2].replace(/,/g, '');
        importData.push({ date: row[0], description: row[1], amount: row[2] });
      }
    });
  }

  function processBendigoData(data) {
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length === 3) {
        // This format can sometimes leave an 'N' at the end of the line.
        var description = row[2].replace(/\sN$/, '');
        importData.push({ date: row[0], description: description,
                          amount: row[1] });
      }
    });
  }

  function processKiwibankData(data) {
    // The first empty entry here is so that the indexes line up. 
    var months = [ '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
                   'Sep', 'Oct', 'Nov', 'Dec' ];
    $.each(data, function(i, item) {
      // Skip the first row in this file.
      if (i === 0) {
        return true;
      }
      var row = cleanLine(item);
      if (row.length === 5) {
        // The month is given as a 3 letter text, convert to numeric.
        row[0] = row[0].replace(/(\s\w+\s)/, function(match) {
          // Remove the spaces that were matched.
          match = match.replace(/\s/g, '');
          var index = $.inArray(match, months);
          if (index !== -1) {
            return '/' + index + '/';
          }
          // If no match return the original.
          return ' ' + match + ' ';
        });
        importData.push({ date: row[0], description: row[1], amount: row[3] });
      }
    });
  }

  function processASBData(data) {
    var headerFound = false;
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length === 7) {
        if (headerFound) {
          // Skip empty rows (just check the date).
          if (row[0] === '') {
            return true;
          }
          importData.push({date: row[0], description: row[5], amount: row[6]});
        }
        // The header row is before the data, make sure it matches.
        else if (row[0] === 'Date' && row[1] === 'Unique Id' &&
                 row[2] === 'Tran Type' && row[3] === 'Cheque Number' &&
                 row[4] === 'Payee' && row[5] === 'Memo' &&
                 row[6] === 'Amount') {
          headerFound = true;
        }
      }
    });
  }

  function processNABData(data) {
    // The first empty entry here is so that the indexes line up.
    var months = [ '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
                   'Sep', 'Oct', 'Nov', 'Dec' ];
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length === 6) {
        if (i === 0) {
          // The first row should be the header, make sure it matches.
          if (row[0] !== 'Date' || row[1] !== 'Amount' ||
              row[2] !== 'Transaction Id' || row[3] !== 'Transaction type') {
            return false;
          }
        }
        else {
          // The month is given as a 3 letter text, convert to numeric.
          row[0] = row[0].replace(/(\s\w+\s)/, function(match) {
            // Remove the spaces that were matched.
            match = match.replace(/\s/g, '');
            var index = $.inArray(match, months);
            if (index !== -1) {
              // Also the year is given as 2 digits so prefix the rest.
              return '/' + index + '/20';
            }
            // If no match return the original.
            return ' ' + match + ' ';
          });
          // Remove the plus sign, dollar symbol and any commas from the amount.
          row[1] = row[1].replace(/\+/g, '');
          row[1] = row[1].replace(/\$/g, '');
          row[1] = row[1].replace(/,/g, '');
          importData.push({date: row[0], description: row[4], amount: row[1]});
        }
      }
    });
  }

  function processNABData2(data) {
    // The first empty entry here is so that the indexes line up.
    var months = [ '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
                   'Sep', 'Oct', 'Nov', 'Dec' ];
    $.each(data, function(i, item) {
      var row = cleanLine(item);
      if (row.length === 7) {
        // This format keeps changing... if dashes are used as date
        // separators convert them to spaces.
        //row[0] = row[0].replace(/-/g, ' ');
        // The month is given as a 3 letter text, convert to numeric.
        row[0] = row[0].replace(/(\s\w+\s)/, function(match) {
          // Remove the spaces that were matched.
          match = match.replace(/\s/g, '');
          var index = $.inArray(match, months);
          if (index !== -1) {
            // Also the year is given as 2 digits so prefix the rest.
            return '/' + index + '/20';
          }
          // If no match return the original.
          return ' ' + match + ' ';
        });
        // Remove the plus sign and any commas from the amount.
        row[1] = row[1].replace(/\+/g, '');
        row[1] = row[1].replace(/,/g, '');
        importData.push({date: row[0], description: row[5], amount: row[1]});
      }
    });
  }

  function showPreviousImport() {
    if (currentImport > 1) {
      // showImportData always increments currentImport counter,
      // so need to decrease by 2 to get back 1.
      currentImport -= 2;
      showImportData();
    }
    return false;
  }

  function showImportData() {
    resetForm();
    if (currentImport === importData.length) {
      exitImport('Data processing complete.');
      return;
    }
    var row = importData[currentImport++];
    // Date is assumed to be DD/MM/YYYY in imported data.
    var dateFields = row.date.match(/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/);
    if (!dateFields || dateFields.length !== 4) {
      $('#payment-date-input').val('');
      alert('Date format in imported transaction doesn\'t match');
    }
    else {
      var day = parseInt(dateFields[1], 10);
      var month = parseInt(dateFields[2], 10);
      var year = parseInt(dateFields[3], 10);
      $('#payment-date-input').val(dobrado.formatDate(year, month, day));
    }
    // Look for a reference in the description. Note that there must be a 
    // space before and after the reference otherwise it may not be unique.
    // (Add spaces to the description to cover cases where the reference is
    //  at the beginning or the end.)
    var reference = '';
    $.each(allReferences, function(i, item) {
      var re = new RegExp(' ' + item + ' ', 'i');
      if (re.test(' ' + row.description + ' ')) {
        reference = item;
        return false;
      }
    });
    if (reference !== '') {
      $('#payment-reference-input').val(dobrado.decode(reference));
      referenceUpdate();
    }
    $('#payment-comment').val(dobrado.decode(row.description));
    $('#payment-amount-input').val(row.amount);
    $('.import .info').html('<br>Transactions found in imported data file. ' +
                            'Processing <b>' + currentImport + '/' +
                            importData.length + '</b>:');
  }

  function resetForm() {
    $('#payment-username-input').val('');
    $('#payment-fullname-input').val('');
    $('#payment-date-input').val(dobrado.formatDate());
    $('#payment-reference-input').val('');
    $('#payment-amount-input').val('');
    $('#payment-comment').val('');
    $('.payment .bank-details').html('');
    $('.payment .contact-details').html('');
    clearSearchDates();
    currentPayment = null;
  }

  function exitImport(message) {
    $('.import .info').html(message);
    $('#payment-import-input').val('');
    $('.payment .remove').button('option', 'label', 'remove');
    $('.payment .back').hide();
    $('.payment .paging').show();
    importData = [];
    currentImport = 0;
  }

}());
