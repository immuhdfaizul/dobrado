<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Domaincheck extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'lookup') return $this->Lookup();
    if ($us_action === 'submit') return $this->Submit();
    if ($us_action === 'checkout') return $this->Checkout();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    return !$this->AlreadyOnPage('domaincheck', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    // Check if returning from making a payment.
    if (isset($_GET['payment']) && $_GET['payment'] === 'paypal') {
      $us_domain = $_SESSION['domaincheck-purchase-domain'];
      $us_price = $_SESSION['domaincheck-purchase-price'];
      $us_email = $_SESSION['domaincheck-purchase-email'];
      $cart = new Module($this->user, $this->owner, 'cart');
      $registrar = new Module($this->user, $this->owner, 'registrar');
      if ($cart->IsInstalled() && $cart->Factory('CheckPayment') &&
          $registrar->Factory('RegisterDomain', [$us_domain, $us_price])) {
        $_SESSION['domaincheck-domain'] = $us_domain;
        $this->Submit($us_email, false);
        $patterns = ['/!domain/', '/!email/'];
        $replacements = [$us_domain, $us_email];
        $content .= '<div class="domaincheck-payment-message">' .
          $this->Substitute('domaincheck-payment', $patterns, $replacements) .
          '<button>ok</button></div>';
      }
      else {
        $content .= '<div class="domaincheck-payment-message">'.
          '<p>There was a problem purchasing the domain, please contact ' .
          'support.</p><button>ok</button></div>';
      }
    }

    $content .= '<form id="domaincheck-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="domaincheck-input">Domain Name:</label>' .
          '<input id="domaincheck-input" type="text" maxlength="100">' .
        '</div>' .
        '<button id="domaincheck-button">Check</button>' .
        '<div id="domaincheck-info"></div>' .
      '</form>' .
      '<form id="domaincheck-email-form" class="hidden" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="domaincheck-email">Email Address:</label>' .
          '<input id="domaincheck-email" type="text" maxlength="100">' .
        '</div>' .
        '<button id="domaincheck-email-submit">Submit</button>' .
        '<div id="domaincheck-email-info"></div>' .
      '</form>' .
      '<div id="domaincheck-checkout-dialog" class="hidden">' .
        '<div id="domaincheck-checkout-message">' .
          'Please enter your contact details:' .
        '</div>' .
        '<form id="domaincheck-customer-details-form" autocomplete="off">' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-name">Name:</label>' .
            '<input id="customer-detail-name" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-email">Email:</label>' .
            '<input id="customer-detail-email" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-phone">Phone:</label>' .
            '<input id="customer-detail-phone" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-address">Address:</label>' .
            '<input id="customer-detail-address" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-postcode">Postcode:</label>' .
            '<input id="customer-detail-postcode" type="text" maxlength="32">' .
            '<label for="customer-detail-city">City:</label>' .
            '<input id="customer-detail-city" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-state">State:</label>' .
            '<input id="customer-detail-state" type="text" maxlength="100">' .
            '<label for="customer-detail-country">Country:</label>' .
            '<input id="customer-detail-country" type="text" maxlength="50">' .
          '</div>' .
          '<button id="customer-detail-submit">Submit</button>' .
          '<div id="domaincheck-checkout-info"></div>' .
        '</form>' .
      '</div>';
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'AddDomain' && $count === 2) {
        $us_domain = $p[0];
        $us_email = $p[1];
        return $this->AddDomain($us_domain, $us_email);
      }
      return;
    }

    if ($fn === 'ListDomains') return $this->ListDomains();
    if ($fn === 'Confirm') return $this->Confirm($p);
    if ($fn === 'Registered') return $this->Registered($p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.domaincheck.js');

    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS domaincheck (' .
      'domain VARCHAR(100) NOT NULL,' .
      'email VARCHAR(100) NOT NULL,' .
      'verification VARCHAR(100) NOT NULL,' .
      'confirmed TINYINT(1) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(domain)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    $description = ['domaincheck-verification' => 'The name of the TXT ' .
                      'record to look for in DNS settings to verify an ' .
                      'email address.',
                    'domaincheck-ip' => 'The IP address to set the A record ' .
                      'to in DNS settings which the domain will be hosted on.',
                    'domaincheck-login-code' => 'The body of an email sent ' .
                      'to a user containing a new login code for their ' .
                      'website. Substitutes !domain and !code.',
                    'domaincheck-hosting' => 'The body of an email sent to ' .
                      'an unconfirmed user, so that they can add a ' .
                      'verification string to their DNS settings. ' .
                      'Substitutes: !domain, !email and !verification.',
                    'domaincheck-description' => 'Provides a way to format ' .
                      'the purchase details, substitutes: !domain, !price ' .
                      'and !date.',
                    'domaincheck-checkout' => 'The text to display at ' .
                      'checkout, will substitute the details provided in the ' .
                      'checkout form as: !name, !phone, !address, !postcode, ' .
                      '!city, !state and !country which will be used as the ' .
                      'contact details for the domain registration.',
                    'domaincheck-payment' => 'The content for a payment ' .
                      'confirmation dialog shown to the user when they ' .
                      'return from paying for their domain. Substitutes: ' .
                      '!domain and !email.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#domaincheck-form","background-color","#eeeeee"',
                   '"","#domaincheck-form","border","1px solid #aaaaaa"',
                   '"","#domaincheck-form","border-radius","2px"',
                   '"","#domaincheck-form","padding","20px"',
                   '"","#domaincheck-form","margin","20px"',
                   '"","#domaincheck-button","margin-left","7.2em"',
                   '"","#domaincheck-info","margin-top","20px"',
                   '"","#domaincheck-email-form","background-color","#eeeeee"',
                   '"","#domaincheck-email-form","border","1px solid #aaaaaa"',
                   '"","#domaincheck-email-form","border-radius","2px"',
                   '"","#domaincheck-email-form","padding","20px"',
                   '"","#domaincheck-email-form","margin","20px"',
                   '"","#domaincheck-email-submit","margin-left","7.2em"',
                   '"","#domaincheck-email-info","margin-top","20px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.domaincheck.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AddDomain($us_domain, $us_email) {
    $mysqli = connect_db();
    $domain = $mysqli->escape_string($us_domain);
    $email = $mysqli->escape_string($us_email);
    $query = 'INSERT INTO domaincheck VALUES ("' . $domain . '", ' .
      '"' . $email . '", "", 0, ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->AddDomain: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function Checkout() {
    $us_name = htmlspecialchars($_POST['name']);
    $us_email = htmlspecialchars($_POST['email']);
    $us_phone = htmlspecialchars($_POST['phone']);
    $us_address = htmlspecialchars($_POST['address']);
    $us_city = htmlspecialchars($_POST['city']);
    $us_postcode = htmlspecialchars($_POST['postcode']);
    $us_state = htmlspecialchars($_POST['state']);
    $us_country = htmlspecialchars($_POST['country']);
    $us_domain = htmlspecialchars($_POST['domain']);
    $price = (float)$_POST['price'];

    $_SESSION['domaincheck-purchase-domain'] = $us_domain;
    $_SESSION['domaincheck-purchase-price'] = $_POST['price'];
    $_SESSION['domaincheck-purchase-email'] = $us_email;
    $registrar = new Module($this->user, $this->owner, 'registrar');
    if ($registrar->IsInstalled()) {
      $registrar->Factory('SaveDetails', [$us_domain, $us_name,  $us_address,
                                          $us_city, $us_state, $us_country,
                                          $us_postcode, $us_phone, $us_email]);
    }

    $patterns = ['/!domain/', '/!price/', '/!date/'];
    $replacements = [$us_domain, $price, date('M jS, Y \a\t g:ia')];
    $us_items = $this->Substitute('domaincheck-description', $patterns,
                                  $replacements);
    $patterns = ['/!name/', '/!phone/', '/!address/','/!postcode/', '/!city/',
                 '/!state/', '/!country/'];
    $replacements = [$us_name, $us_phone, $us_address, $us_postcode, $us_city,
                     $us_state, $us_country];
    $result = ['content' => $this->Substitute('domaincheck-checkout', $patterns,
                                              $replacements)];
    $cart = new Module($this->user, $this->owner, 'cart');
    if ($cart->IsInstalled()) {
      $processing = $cart->Factory('ProcessingCost', $price);
      $result['content'] .= '<p>Payment will be $' . price_string($price);
      if ($processing > 0) {
        $result['content'] .= ' plus $' . price_string($processing) .
          ' processing fee.';
      }
      $result['content'] .= '</p>';
      $result['content'] .=
        $cart->Factory('Payment', [$us_items, $price + $processing, $us_name,
                                   $us_email, $us_phone, $us_address, $us_city,
                                   $us_postcode, $us_state, $us_country, '',
                                   'paypal']);
    }
    else {
      $result['content'] .= 'Cannot process payment. Cart module must be ' .
        'installed.';
    }
    return $result;
  }

  private function Confirm($us_domain) {
    $mysqli = connect_db();
    $query = 'UPDATE domaincheck SET confirmed = 1 WHERE ' .
      'domain = "' . $mysqli->escape_string($us_domain) . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->Confirm: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ListDomains() {
    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT domain, email, verification, confirmed, timestamp FROM ' .
      'domaincheck ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($domaincheck = $mysqli_result->fetch_assoc()) {
        $domaincheck['confirmed'] = (int)$domaincheck['confirmed'];
        $domaincheck['timestamp'] = (int)$domaincheck['timestamp'] * 1000;
        $result[] = $domaincheck;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->ListDomains: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function LoginCode($us_domain, $us_email) {
    // Need a connection to the user's database, which is expected to be their
    // domain name, other details should match the current db_config.
    include 'functions/db_config.php';
    $mysqli = new mysqli($db_server, $db_user, $db_password, $us_domain);
    if ($mysqli->connect_errno) {
      $this->Log('Domaincheck->LoginCode 1: ' . $mysqli->connect_error);
      return ['error' => $mysqli->connect_error];
    }

    $error = '';
    $code = bin2hex(openssl_random_pseudo_bytes(8));
    $query = 'INSERT INTO settings VALUES ("admin", "login", "code", ' .
      '"' . $code . '") ON DUPLICATE KEY UPDATE value = "' . $code . '"';
    if (!$mysqli->query($query)) {
      $error = $mysqli->error;
      $this->Log('Domaincheck->LoginCode 2: ' . $error);
    }
    $mysqli->close();
    if ($error !== '') return ['error' => $error];

    $sender = $this->Substitute('new-user-sender');
    $sender_name = $this->Substitute('new-user-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = $this->Substitute('new-user-sender-cc');
    $bcc = $this->Substitute('new-user-sender-bcc');

    $subject = 'Login code requested';
    $us_content = $this->Substitute('domaincheck-login-code',
                                    ['/!domain/', '/!code/'],
                                    [$us_domain, $code]);
    $us_message = '<html><head><title>' . $subject . '</title></head><body>' .
      $us_content . '</body></html>';
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($bcc !== '') {
      $headers .= 'Bcc: ' . $bcc . "\r\n";
    }
    mail($us_email, $subject, wordwrap($us_message), $headers, '-f ' . $sender);
    return ['done' => true];
  }

  private function Lookup() {
    $_SESSION['domaincheck-domain'] = NULL;

    // Remove all spaces in the input.
    $us_domain = strtolower(preg_replace('/\s/', '', $_POST['domain']));
    // Remove the scheme and path if provided.
    if (preg_match('/\/\/([^\/]+)/', $us_domain, $match)) {
      $us_domain = $match[1];
    }
    if (substr_count($us_domain, '.') === 0) {
      $registrar = new Module($this->user, $this->owner, 'registrar');
      if ($registrar->IsInstalled()) {
        $result = $registrar->Factory('Search', $us_domain);
        if (isset($result['domains'])) {
          $purchasable_list = $result['domains'];
          if (count($purchasable_list) >= 1) {
            $domain_name = $purchasable_list[0]['domainName'];
            $purchase_price = $purchasable_list[0]['purchasePrice'];
            $renewal_price = isset($purchasable_list[0]['renewalPrice']) ?
              $purchasable_list[0]['renewalPrice'] : $purchase_price;
            $renewal_hidden = $purchase_price === $renewal_price ?
              ' class="hidden"' : '';

            $info = '';
            $domains = [];
            if (count($purchasable_list) > 1) {
              $info = '<p>Search results: <select id="domaincheck-select">';
              foreach ($purchasable_list as $purchasable) {
                $name = $purchasable['domainName'];
                $info .= '<option>' . $name . '</option>';
                if (!isset($purchasable['renewalPrice'])) {
                  $purchasable['renewalPrice'] = $purchasable['purchasePrice'];
                }
                $domains[$name] = ['price' => $purchasable['purchasePrice'],
                                   'renewal' => $purchasable['renewalPrice']];
              }
              $info .= '</select></p>';
            }
            $info .=  '<p>The domain name <span id="domaincheck-domain">' .
                $domain_name . '</span> is available to purchase!<br>' .
                'The <a href="https://name.com">name.com</a> price is: ' .
                '$<span id="domaincheck-price">' . $purchase_price . '</span>' .
                ' for 1 year.</p>' .
              '<p id="domaincheck-renewal-info"' . $renewal_hidden . '>' .
                'After the first year the renewal price is $' .
                '<span id="domaincheck-renewal">' . $renewal_price . '</span>' .
                ' per year.</p>' .
              '<button id="domaincheck-purchase">purchase</button>';
            return ['info' => $info, 'domains' => $domains];
          }
        }
        return ['info' => $result['info']];
      }
      return ['info' => 'Sorry the input you provided is not a domain name.'];
    }

    if (strpos($us_domain, 'www.') === 0) {
      return ['info' => 'Please enter a domain name without the \'www\' ' .
                        'subdomain. This will be created automatically if ' .
                        'you set it to the same IP address in your DNS ' .
                        'settings.'];
    }

    // Checking DNS records appears to be a bit temperamental so check twice.
    $result_list = dns_get_record($us_domain, DNS_A + DNS_TXT);
    if (!$result_list) {
      sleep(5);
      $this->Log('Domaincheck->Lookup: Checking ' . $us_domain);
      $result_list = dns_get_record($us_domain, DNS_A + DNS_TXT);
    }
    if (!$result_list) {
      $registrar = new Module($this->user, $this->owner, 'registrar');
      if ($registrar->IsInstalled()) {
        $result = $registrar->Factory('Check', $us_domain);
        if (isset($result['domains'])) {
          $purchasable = $result['domains'][0];
          $domain_name = $purchasable['domainName'];
          $purchase_price = $purchasable['purchasePrice'];
          $renewal_price = isset($purchasable['renewalPrice']) ?
            $purchasable['renewalPrice'] : $purchase_price;
          $renewal_hidden = $purchase_price === $renewal_price ?
            ' class="hidden"' : '';
          $info = '<p>The domain name <span id="domaincheck-domain">' .
            $domain_name . '</span> is available to purchase!<br>' .
              'The <a href="https://name.com">name.com</a> price is: ' .
              '$<span id="domaincheck-price">' . $purchase_price . '</span>' .
              ' for 1 year.</p>' .
            '<p id="domaincheck-renewal-info"' . $renewal_hidden . '>' .
              'After the first year the renewal price is $' .
              '<span id="domaincheck-renewal">' . $renewal_price . '</span>' .
              ' per year.</p>' .
            '<button id="domaincheck-purchase">purchase</button>';
          return ['info' => $info];
        }
        return ['info' => $result['info']];
      }
      return ['info' => 'Sorry the domain name you provided was not found. ' .
                        'Please check your details and try again.'];
    }

    $ip = '';
    $ip_ttl = 0;
    $txt = '';
    $txt_ttl = 0;
    $check_verification = $this->Substitute('domaincheck-verification');
    foreach ($result_list as $result) {
      if ($result['type'] === 'A') {
        $ip = $result['ip'];
        $ip_ttl = ceil((int)$result['ttl'] / 3600);
        continue;
      }

      if ($result['type'] === 'TXT' &&
          stripos($result['txt'], $check_verification) !== false) {
        $txt = strtolower($result['txt']);
        $txt_ttl = ceil((int)$result['ttl'] / 3600);
      }
    }
    $check_ip = $this->Substitute('domaincheck-ip');
    $next_step = '<p>Once the IP address matches, come back to this page ' .
      'and enter your domain name again for the next step! (If you visit ' .
      'your domain name and it redirects here, that\'s great it means ' .
      'you can continue!)</p>';
    if ($ip === '') {
      $info = 'This domain name is registered.<br>It looks like there\'s no ' .
        'IP address set. If you registered this domain name and would like ' .
        'it hosted here, please set it to: <b>' . $check_ip . '</b> in your ' .
        'DNS settings. (The current settings are cached for ';
      if ($ip_ttl > 1) $info .= $ip_ttl . ' hours.)';
      else $info .= '1 hour.)';
      return ['info' => $info . $next_step];
    }

    if ($ip !== $check_ip) {
      $info = 'This domain name is registered.<br>The IP address is ' .
        'currently set to: <b>' . $ip . '</b>. If you registered this domain ' .
        'name and would like it hosted here, please change it to: ' .
        '<b>' . $check_ip . '</b> in your DNS settings. (The current ' .
        'settings are cached for ';
      if ($ip_ttl > 1) $info .= $ip_ttl . ' hours.)';
      else $info .= '1 hour.)';
      return ['info' => $info . $next_step];
    }

    // If ip is set correctly check if we have already recorded the domain.
    $mysqli = connect_db();
    $verification = '';
    $confirmed = false;
    $domain = $mysqli->escape_string($us_domain);
    $query = 'SELECT verification, confirmed FROM domaincheck WHERE ' .
      'domain = "' . $domain . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($domaincheck = $mysqli_result->fetch_assoc()) {
        $verification = $domaincheck['verification'];
        $confirmed = $domaincheck['confirmed'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->Lookup: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($confirmed) {
      $_SESSION['domaincheck-domain'] = $us_domain;
      $info = 'It looks like your domain name is currently hosted here. If ' .
        'you need a new login code, please provide the email address you ' .
        'registered with using the form below and one will be emailed to you.';
      return ['info' => $info, 'email' => true];
    }

    if ($verification === '') {
      $_SESSION['domaincheck-domain'] = $us_domain;
      $info = 'Nice domain name!<br>If you would like it hosted here, please ' .
        'provide the email address you want to use to log in to your website.';
      return ['info' => $info, 'email' => true];
    }

    // If a verification string is set but the domain is not confirmed, then
    // it's waiting in a queue to be set up. Can at least let them know if they
    // have set the verification string properly.
    if ($verification === $txt) {
      return ['info' => 'Your DNS settings have been entered correctly, but ' .
                        'setup for your website is waiting in a queue. You ' .
                        'will receive an email once your website has been ' .
                        'created.'];
    }

    $info = 'Setup for your website is in a queue, thanks for waiting! ' .
      'In the mean time, please set the verification string in your DNS ' .
      'settings that was emailed to you. This will allow you to log in to ' .
      'your website when setup is complete.';
    if ($txt_ttl > 1) {
      $info .= ' (The current settings are cached for ' . $txt_ttl . ' hours.)';
    }
    else if ($txt_ttl === 1) {
      $info .= ' (The current settings are cached for 1 hour.)';
    }
    return ['info' => $info];
  }

  private function Registered($us_domain) {
    $registered = false;
    $mysqli = connect_db();
    $query = 'SELECT COUNT(*) AS count FROM domaincheck WHERE ' .
      'domain = "' . $mysqli->escape_string($us_domain) . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($domaincheck = $mysqli_result->fetch_assoc()) {
        $registered = $domaincheck['count'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->Registered: ' . $mysqli->error);
    }
    $mysqli->close();
    return $registered;
  }

  private function Submit($us_email = '', $set_verification = true) {
    if (!isset($_SESSION['domaincheck-domain'])) {
      return ['info' => 'Domain name not set.'];
    }

    $us_domain = $_SESSION['domaincheck-domain'];
    if ($us_email === '') {
      $us_email = strtolower(trim($_POST['email']));
    }
    if ($us_email === '') return ['info' => 'Email not provided.'];

    $mysqli = connect_db();
    $domain = $mysqli->escape_string($us_domain);
    $email = $mysqli->escape_string($us_email);
    $check_email = '';
    $confirmed = false;
    $query = 'SELECT email, confirmed FROM domaincheck WHERE ' .
      'domain = "' . $domain . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($domaincheck = $mysqli_result->fetch_assoc()) {
        $check_email = $domaincheck['email'];
        $confirmed = $domaincheck['confirmed'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->Submit 1: ' . $mysqli->error);
    }
    $mysqli->close();

    // If the domain is confirmed, user wants to create a new login code.
    if ($confirmed) {
      if ($us_email !== $check_email) {
        return ['info' => 'Sorry the email address you provided does not ' .
                          'match our records.'];
      }

      $result = $this->LoginCode($us_domain, $us_email);
      if (isset($result['error'])) {
        return ['info' => 'Sorry a new login code could not be created. ' .
                           'Please use the contact page for help.'];
      }

      return ['info' => 'Thanks! A login code will be emailed to you soon.'];
    }

    $mysqli = connect_db();
    // Otherwise the user has set the IP address correctly and now needs a
    // verification code to continue. If we have registered the domain name
    // for them don't need to set verification here.
    $us_verification = $set_verification ?
      $this->Substitute('domaincheck-verification') . '=' .
        bin2hex(openssl_random_pseudo_bytes(16)) : '';
    $verification = $mysqli->escape_string($us_verification);
    $query = 'INSERT INTO domaincheck VALUES ("' . $domain . '", ' .
      '"' . $email . '", "' . $verification . '", 0, ' . time() . ') ' .
      'ON DUPLICATE KEY UPDATE email = "' . $email . '", ' .
      'verification = "' . $verification . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->Submit 3: ' . $mysqli->error);
    }
    $mysqli->close();

    if (!$set_verification) return;

    $sender = $this->Substitute('new-user-sender');
    $sender_name = $this->Substitute('new-user-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = $this->Substitute('new-user-sender-cc');
    $bcc = $this->Substitute('new-user-sender-bcc');

    $subject = 'Hosting requested';
    $us_content = $this->Substitute('domaincheck-hosting', ['/!domain/',
                                      '/!email/', '/!verification/'],
                                    [$us_domain, $us_email, $us_verification]);
    $us_message = '<html><head><title>' . $subject . '</title></head><body>' .
      $us_content . '</body></html>';
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($bcc !== '') {
      $headers .= 'Bcc: ' . $bcc . "\r\n";
    }
    mail($us_email, $subject, wordwrap($us_message), $headers, '-f ' . $sender);
    return ['info' => 'Thanks! Instructions on what to do next have been ' .
                      'emailed to you.'];
  }

}
