// @source: /js/source/dobrado.manager.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2020 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.manager){dobrado.manager={};}
(function(){'use strict';var manager=null;var purchase=[];var managerGrid=null;var managerGridId='';var allProductsGrid=null;var allProductsGridId='';var allProductsData=[];var currentProduct=null;var currentDate=null;var oneDay=86400000;var confirmRemoveMultiple=false;var currentRemove=0;var composite=false;var importData=[];var currentImport=0;var saving=false;$(function(){if($('.manager').length===0){return;}
$('#manager-group-select').change(changeGroup);$('.manager-view-all-dialog').dialog({show:true,autoOpen:false,modal:true,width:830,height:520,position:{my:'top',at:'top+50',of:window},title:'Available Products',create:dobrado.fixedDialog,close:function(){if(allProductsGrid){allProductsGrid.gotoCell(0,0);}}});$('.manager .view-all').button().click(viewAll);$('#manager-form .default-action').click(submit);$('#manager-form .submit').button().click(submit);$('#manager-form .search').button().click(search);$('#manager-form .back').button().click(showPreviousImport);$('#manager-form .remove').button().click(remove);$('#manager-form .import-toggle').click(toggleImport);$('#manager-import-file').change(loadImportData);$('#manager-username-input').val('').change(function(){setTimeout(function(){showUser();},10);});$('#manager-username-input').keypress(checkUsernameEnter);$('#manager-product-input').val('').change(function(){setTimeout(function(){showProductFromMenu();},10);});$('#manager-supplier-input').val('').change(function(){setTimeout(function(){showSupplierFromMenu();},10);});$('#manager-price-input').val('');$('#manager-date-input').val('').change(clearSearchDates).datepicker({dateFormat:dobrado.dateFormat});$('#manager-start-date-input').val('').change(clearDate).datepicker({dateFormat:dobrado.dateFormat});$('#manager-end-date-input').val('').change(clearDate).datepicker({dateFormat:dobrado.dateFormat});$('#manager-quantity-input').val('');$('#manager-quantity-input').spinner({min:0,spin:setQuantity,change:setQuantity});$('.manager .toggle-search-options').click(function(){clearDate();clearSearchDates();$('.manager .search-options').toggle();return false;});if($('.grid').length!==0){gridSetup();}
dobrado.log('Loading users...','info');$.post('/php/request.php',{request:'manager',action:'list',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager list')){return;}
manager=JSON.parse(response);if(manager.date){$('#manager-date-input').datepicker('setDate',dobrado.formatDate(manager.date));}
$('#manager-username-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:manager.users,select:showUser});updateProducts();});});function decimalString(value){return(value+0.0001).toFixed(2);}
function gridSetup(){$('.grid').each(function(index){if(index===0){managerGridId='#'+$(this).attr('id');}
if(index===1){allProductsGridId='#'+$(this).attr('id');}});var columns=[{id:'date',name:'Date',field:'date',width:110,sortable:true,formatter:Slick.Formatters.Timestamp},{id:'user',name:'Username',field:'user',width:110,sortable:true},{id:'name',name:'Product',field:'name',width:190,sortable:true}];if(!dobrado.mobile){columns.push({id:'supplier',name:'Supplier',field:'supplier',width:110,sortable:true});}
columns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});if(!dobrado.mobile){columns.push({id:'price',name:'Price',field:'price',width:80,sortable:true,formatter:Slick.Formatters.Dollar});columns.push({id:'total',name:'Total',field:'total',width:80,sortable:true,formatter:Slick.Formatters.Dollar});}
var options={autoHeight:true,editable:true,forceFitColumns:true};managerGrid=dobrado.grid.instance(managerGridId,[],columns,options);managerGrid.setSelectionModel(new Slick.RowSelectionModel());managerGrid.onClick.subscribe(function(e,item){showPurchase(item.row);});managerGrid.onSelectedRowsChanged.subscribe(function(e,item){if(item.rows.length===1){showPurchase(item.rows[0]);}});managerGrid.onCellChange.subscribe(updatePurchaseFromSearch);managerGrid.onSort.subscribe(function(e,args){purchase.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field==='quantity'||field==='price'||field==='total'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});managerGrid.invalidate();});if($('.grid').length>=2){var allProductsColumns=[{id:'product',name:'Product',field:'name',width:210,sortable:true},{id:'supplier',name:'Supplier',field:'supplier',width:140,sortable:true},{id:'date',name:'Date',field:'date',width:110,sortable:true,formatter:Slick.Formatters.Timestamp},{id:'quantity',name:'Quantity',field:'quantity',width:100,sortable:true,editor:Slick.Editors.Float},{id:'price',name:'Price',field:'price',width:100,sortable:true,formatter:Slick.Formatters.Units},{id:'total',name:'Total',field:'total',width:80,sortable:true,formatter:Slick.Formatters.Dollar}];var allProductsOptions={autoHeight:true,editable:true,forceFitColumns:true};allProductsGrid=dobrado.grid.instance(allProductsGridId,[],allProductsColumns,allProductsOptions);allProductsGrid.setSelectionModel(new Slick.RowSelectionModel());allProductsGrid.onCellChange.subscribe(updatePurchaseFromAll);allProductsGrid.onSort.subscribe(function(e,args){allProductsData.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field!=='name'&&field!=='supplier'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});allProductsGrid.invalidate();});}
$(allProductsGridId).appendTo($('.manager-view-all-dialog'));$('.grid').hide();}
function checkUsernameEnter(event){if(event.keyCode!==13){return;}
event.preventDefault();showUser();}
function changeGroup(){dobrado.log('Changing group.','info');$.post('/php/request.php',{request:'manager',action:'changeGroup',group:$('#manager-group-select').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager changeGroup')){return;}
location.reload();});}
function showUser(event,ui){var user=$('#manager-username-input').val();if(ui){user=ui.item.value;}
showProductFromMenu();if(user===''){return;}
if($.inArray(user,manager.users)===-1){alert('If an account was just created for '+user+' please reload the page.');$('#manager-username-input').val('');return;}}
function showPurchase(row){var data=purchase[row];$('#manager-username-input').val(data.user);$('#manager-product-input').val(dobrado.decode(data.name));$('#manager-supplier-input').val(data.supplier);$('#manager-quantity-input').val(data.quantity);$('#manager-date-input').val(dobrado.formatDate(data.date));$('#manager-price-input').val('');$('.manager-price-info').html('');currentDate=data.date;currentProduct=null;if(data.name===''){return;}
$.each(manager.products,function(index,item){if(item.name===data.name&&item.user===data.supplier){currentProduct=item;if(item.unit==='variable'){if(data.price){$('#manager-price-input').val(data.price).attr('readonly',false);}
$('#manager-quantity-input').spinner('disable');}
else{if(data.price&&data.price!=='0.00'){var price=decimalString(data.quantity*data.price);$('#manager-price-input').val('$'+price+' @ ($'+data.price+'/'+item.unit+')');var priceLevel=manager.buyerGroup[data.user];if(!priceLevel){priceLevel='retail';}
if(decimalString(item[priceLevel])!==data.price){$('.manager-price-info').html('Stock price now $'+
decimalString(item[priceLevel])+'/'+item.unit);}}
else{$('.manager-price-info').html('Price calculated during adjustment');}
$('#manager-price-input').attr('readonly',true);$('#manager-quantity-input').spinner('enable');}
return false;}});if(!currentProduct){alert('This product was not found on the stock page.\n'+'Please check if it\'s name has been updated.');}}
function showProductFromMenu(event,ui){var user=$('#manager-username-input').val();var product=$('#manager-product-input').val();var supplier=$('#manager-supplier-input').val();var priceLevel='price';$('#manager-price-input').val('');$('.manager-price-info').html('');currentProduct=null;if(user!==''){priceLevel=manager.buyerGroup[user];if(!priceLevel){priceLevel='retail';}}
if(ui){product=ui.item.value;}
if(product===''){return false;}
$.each(manager.products,function(index,item){if(item.name===product&&(supplier===item.user||(supplier===''&&item.available))){currentProduct=item;$('#manager-supplier-input').val(item.user);if(item.unit==='variable'){$('#manager-price-input').val(decimalString(item[priceLevel]));$('#manager-price-input').attr('readonly',false);$('#manager-quantity-input').val('1').spinner('disable');}
else{$('#manager-price-input').val('($'+decimalString(item[priceLevel])+'/'+item.unit+')');$('#manager-price-input').attr('readonly',true);$('#manager-quantity-input').val('').spinner('enable');}
return false;}});if(!currentProduct){$.each(manager.products,function(index,item){if(item.name===product){currentProduct=item;$('#manager-supplier-input').val(item.user);if(item.unit==='variable'){$('#manager-price-input').val(decimalString(item[priceLevel]));$('#manager-price-input').attr('readonly',false);$('#manager-quantity-input').val('1').spinner('disable');}
else{$('#manager-price-input').val('($'+
decimalString(item[priceLevel])+'/'+item.unit+')');$('#manager-price-input').attr('readonly',true);$('#manager-quantity-input').val('').spinner('enable');}
return false;}});}}
function showSupplierFromMenu(event,ui){var user=$('#manager-username-input').val();var supplier=$('#manager-supplier-input').val();var product=$('#manager-product-input').val();var priceLevel='price';currentProduct=null;if(user!==''){priceLevel=manager.buyerGroup[user];if(!priceLevel){priceLevel='retail';}}
if(ui){supplier=ui.item.value;}
$('#manager-price-input').val('');$.each(manager.products,function(index,item){if(item.user===supplier&&product===item.name){currentProduct=item;if(item.unit==='variable'){$('#manager-price-input').val(decimalString(item[priceLevel]));$('#manager-price-input').attr('readonly',false);$('#manager-quantity-input').val('1').spinner('disable');}
else{$('#manager-price-input').val('($'+decimalString(item[priceLevel])+'/'+item.unit+')');$('#manager-price-input').attr('readonly',true);$('#manager-quantity-input').val('').spinner('enable');}
return false;}});}
function updateProducts(){var products=[];var suppliers=[];$.each(manager.products,function(index,item){if($.inArray(item.name,products)===-1&&item.hidden===0){products.push(item.name);}
if($.inArray(item.user,suppliers)===-1){suppliers.push(item.user);}});$('#manager-product-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:products,select:showProductFromMenu});$('#manager-supplier-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:suppliers,select:showSupplierFromMenu});}
function setQuantity(event,ui){var user=$('#manager-username-input').val();var priceLevel='price';if(user!==''){priceLevel=manager.buyerGroup[user];if(!priceLevel){priceLevel='retail';}}
var quantity=0;if('value'in ui){quantity=ui.value;}
else{quantity=parseFloat($(this).val());if(!quantity||quantity<0){quantity=0;$(this).val('0');}}
if(currentProduct&&currentProduct.unit!=='variable'){var total=quantity*currentProduct[priceLevel];$('#manager-price-input').val('$'+decimalString(total)+' @ ($'+
decimalString(currentProduct[priceLevel])+'/'+currentProduct.unit+')');}}
function submit(){if(saving){return false;}
var user=$('#manager-username-input').val();if(user===''){alert('Please select a username.');return false;}
if(!currentProduct){alert('Product not found for this supplier.');return false;}
var product=currentProduct.name;if(product!==$('#manager-product-input').val()){alert('Product not found.');resetForm();return false;}
var supplier=currentProduct.user;if(supplier!==$('#manager-supplier-input').val()){alert('Supplier not found.');resetForm();return false;}
var priceLevel=manager.buyerGroup[user];if(!priceLevel){priceLevel='retail';}
var price=currentProduct[priceLevel];var basePrice=currentProduct.price;if(currentProduct.unit==='variable'){price=parseFloat($('#manager-price-input').val());if(!price){$('#manager-price-input').val('');return false;}
basePrice=price;if(priceLevel==='wholesale'&&manager.wholesalePercent){basePrice-=basePrice*manager.wholesalePercent/100;}
else if(priceLevel==='retail'&&manager.retailPercent){basePrice-=basePrice*manager.retailPercent/100;}}
else if(currentImport!==0&&currentProduct.quantityAdjustment){price=0;}
var quantity=parseFloat($('#manager-quantity-input').val());if(!quantity||quantity<0){alert('Quantity required.');return false;}
var total=decimalString(quantity*price);var timestamp=parseInt($.datepicker.formatDate('@',$('#manager-date-input').datepicker('getDate')),10);if(!timestamp){alert('Date required.');return false;}
if(currentDate&&timestamp>currentDate-oneDay&&timestamp<currentDate+oneDay){timestamp=currentDate;}
else{currentDate=null;}
saving=true;$('#manager-form .submit').button('option','disabled',true);dobrado.log('Saving purchase...','info');$.post('/php/request.php',{request:'manager',username:user,timestamp:timestamp,product:product,supplier:supplier,quantity:quantity,price:decimalString(price),basePrice:decimalString(basePrice),action:'submit',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager submit')){saving=false;$('#manager-form .submit').button('option','disabled',false);return;}
var data=JSON.parse(response);var selectedRow=0;var newItem=true;if(currentDate){$.each(purchase,function(i,item){if(item.name===product&&item.date===currentDate&&item.user===user){purchase[i].supplier=supplier;purchase[i].quantity=quantity;purchase[i].price=decimalString(price);purchase[i].total=total;if(managerGrid){selectedRow=i;}
newItem=false;}
else if(!data.done&&item.user===user&&item.name==='surcharge'&&item.date===data.date){purchase[i]=data;}});}
if(newItem){if(data.composite){$.each(data.purchase,function(i,item){purchase.push({date:item.date,user:item.user,name:item.name,supplier:item.supplier,quantity:item.quantity,price:decimalString(item.price),total:item.total});});}
else{purchase.push({date:timestamp,user:user,name:product,supplier:supplier,quantity:quantity,price:decimalString(price),total:total});$.each(purchase,function(i,item){if(!data.done&&item.user===user&&item.name==='surcharge'&&item.date===data.date){purchase[i]=data;}});}
selectedRow=purchase.length-1;}
if(managerGrid){$(managerGridId).show();managerGrid.setData(purchase);managerGrid.updateRowCount();managerGrid.render();managerGrid.setSelectedRows([selectedRow]);managerGrid.scrollRowIntoView(selectedRow);}
if(currentImport===0){resetForm();}
else{showImportData();}
saving=false;$('#manager-form .submit').button('option','disabled',false);});return false;}
function search(){var username=$('#manager-username-input').val();var product=$('#manager-product-input').val();var supplier=$('#manager-supplier-input').val();var timestamp=parseInt($.datepicker.formatDate('@',$('#manager-date-input').datepicker('getDate')),10);if(!timestamp){timestamp='';}
if(currentDate){timestamp=currentDate;}
var start=parseInt($.datepicker.formatDate('@',$('#manager-start-date-input').datepicker('getDate')),10);if(!start){start='';}
var end=parseInt($.datepicker.formatDate('@',$('#manager-end-date-input').datepicker('getDate')),10);if(!end){end='';}
if(username===''&&product===''&&supplier===''&&timestamp===''&&start===''&&end===''){alert('Please provide data in at least one input field to search by.');return false;}
var exportData=$('#manager-export-data:checked').length;dobrado.log('Searching...','info');$.post('/php/request.php',{request:'manager',username:username,timestamp:timestamp,product:product,supplier:supplier,quantity:'',price:'',start:start,end:end,group:$('#manager-group-input:checked').length,taxable:$('#manager-taxable-select').val(),exportData:exportData,action:'search',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager search')){return;}
var data=JSON.parse(response);purchase=data.search;if(purchase.length===0){$('.manager .search-info').html('No data found for search.');$(managerGridId).hide();return;}
if(managerGrid){if($('#manager-username-input').val()===''&&$('#manager-supplier-input').val()!==''&&$('#manager-group-input:checked').length===1){$('.manager .search-info').html('<i>Displaying supplier price</i>');}
else{var searchTotal=0;$.each(purchase,function(i,item){searchTotal+=parseFloat(item.total);});$('.manager .search-info').html('Search total: $'+
decimalString(searchTotal));}
$(managerGridId).show();managerGrid.setData(purchase);managerGrid.updateRowCount();managerGrid.render();managerGrid.setSelectedRows([]);currentDate=null;}
if(exportData){location.href='/php/private.php?file='+data.filename;}});return false;}
function clearDate(){$('#manager-date-input').val('');}
function clearSearchDates(){$('#manager-start-date-input').val('');$('#manager-end-date-input').val('');}
function remove(){if(currentImport!==0){showImportData();return false;}
var selected=[];if(managerGrid){selected=managerGrid.getSelectedRows();if(selected.length>1){if(!confirmRemoveMultiple){if(confirm('Remove '+selected.length+' rows? '+'(Grid will be cleared when finished)')){confirmRemoveMultiple=true;currentRemove=0;}
else{return false;}}
if(currentRemove===selected.length){purchase=[];managerGrid.setData(purchase);managerGrid.updateRowCount();managerGrid.render();managerGrid.setSelectedRows([]);confirmRemoveMultiple=false;resetForm();return false;}
showPurchase(selected[currentRemove++]);}}
if(!currentProduct||!currentDate){confirmRemoveMultiple=false;return false;}
var username=$('#manager-username-input').val();var product=currentProduct.name;if(product!==$('#manager-product-input').val()){alert('Product not found.');confirmRemoveMultiple=false;resetForm();return false;}
if(selected.length===1){dobrado.log('Removing purchase...','info');}
else{dobrado.log('Removing purchase '+currentRemove+'/'+
selected.length+'...','info');}
$.post('/php/request.php',{request:'manager',username:username,product:product,supplier:$('#manager-supplier-input').val(),quantity:$('#manager-quantity-input').val(),timestamp:currentDate,action:'remove',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager remove')){return;}
if(confirmRemoveMultiple){remove();return;}
var surcharge=JSON.parse(response);$.each(purchase,function(i,item){if(item.name===product&&item.date===currentDate&&item.user===username){purchase.splice(i,1);return false;}});$.each(purchase,function(i,item){if(item.name==='surcharge'&&item.date===currentDate&&item.user===username&&item.date>currentDate-oneDay&&item.date<currentDate+oneDay){if(surcharge.done){purchase.splice(i,1);}
else{purchase[i]=surcharge;}
return false;}});if(managerGrid&&purchase.length!==0){managerGrid.setData(purchase);managerGrid.updateRowCount();managerGrid.render();managerGrid.setSelectedRows([]);}
else{$(managerGridId).hide();}
resetForm();});return false;}
function viewAll(){var user=$('#manager-username-input').val();var product=$('#manager-product-input').val();var supplier=$('#manager-supplier-input').val();composite=false;allProductsData=[];if(product!==''&&(user===''||user===supplier)){$.each(manager.products,function(index,item){if(item.name===product&&item.user===supplier){if(item.composite===1){composite=true;user=supplier;}
return false;}});}
if(!composite&&user===''){alert('Please enter a username.');return false;}
var priceLevel='price';if(user!==''){priceLevel=manager.buyerGroup[user];if(!priceLevel){priceLevel='retail';}}
var timestamp=Date.now();var date=$('#manager-date-input').datepicker('getDate');if(date){timestamp=parseInt($.datepicker.formatDate('@',date),10);}
dobrado.log('Loading available products...','info');$.post('/php/request.php',{request:'manager',action:'loadProducts',composite:composite,username:user,product:product,timestamp:timestamp,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager viewAll')){return;}
var productList=JSON.parse(response);var purchaseTotal=0;$.each(productList.available,function(index,item){var quantity=0;var currentTime=timestamp;if(productList.current[item.name]){quantity=productList.current[item.name].quantity;currentTime=productList.current[item.name].date;}
var total=decimalString(quantity*item[priceLevel]);purchaseTotal+=parseFloat(total);allProductsData.push({date:currentTime,name:item.name,supplier:item.user,quantity:quantity,unit:item.unit,price:decimalString(item[priceLevel]),basePrice:decimalString(item.price),total:total});});if(composite){$('.manager-view-all-dialog').dialog('option','title','Editing items for '+product);}
else{$('.manager-view-all-dialog').dialog('option','title','Available Products');}
$('.manager-view-all-dialog').dialog('open');if(allProductsGrid){$(allProductsGridId).show();allProductsGrid.setData(allProductsData);allProductsGrid.updateRowCount();allProductsGrid.render();$(allProductsGridId+' .slick-header-columns').children().eq(3).click().click();$('.manager-view-all-total').html('Current total: $'+
decimalString(purchaseTotal));}});return false;}
function updatePurchaseFromAll(e,args){var user=$('#manager-username-input').val();var product=$('#manager-product-input').val();var supplier=$('#manager-supplier-input').val();var item=args.item;var purchaseTotal=0;$.each(allProductsData,function(i,data){purchaseTotal+=parseFloat(decimalString(data.quantity*data.price));});$('.manager-view-all-total').html('Current total: $'+
decimalString(purchaseTotal));item.total=decimalString(item.quantity*item.price);if(allProductsGrid){allProductsGrid.invalidate();}
if(composite){user=supplier;}
else{product='';}
if(item.quantity>0){if(item.unit==='variable'){item.quantity=1;}
if(composite){dobrado.log('Updating '+product+'.','info');}
else{dobrado.log('Saving purchase.','info');}
$.post('/php/request.php',{request:'manager',username:user,timestamp:item.date,product:item.name,supplier:item.supplier,quantity:item.quantity,price:item.price,basePrice:item.basePrice,composite:product,action:'savePurchase',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager savePurchase')){return;}
var data=JSON.parse(response);purchase.push({date:item.date,user:user,name:item.name,supplier:item.supplier,quantity:item.quantity,price:item.price,total:item.total});$.each(purchase,function(i,item){if(!data.done&&item.user===user&&item.name==='surcharge'&&item.date===data.date){purchase[i]=data;}});if(managerGrid){$(managerGridId).show();managerGrid.setData(purchase);managerGrid.updateRowCount();managerGrid.render();}});}
else{if(composite){dobrado.log('Updating '+product+'.','info');}
else{dobrado.log('Removing purchase.','info');}
$.post('/php/request.php',{request:'manager',username:user,timestamp:item.date,product:item.name,supplier:item.supplier,quantity:item.quantity,composite:product,action:'removePurchase',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager removePurchase')){return;}});}}
function updatePurchaseFromSearch(e,args){if(!currentProduct){alert('Product not found for this supplier.');}
var item=args.item;if(item.quantity<0){alert('Quantity must be greater than or equal to zero.');return;}
$.post('/php/request.php',{request:'manager',username:item.user,timestamp:item.date,product:item.name,supplier:item.supplier,quantity:item.quantity,price:item.price,basePrice:currentProduct.price,action:'submit',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'manager update purchase')){return;}
$.each(purchase,function(i,current){if(current.user===item.user&&current.name===item.name&&current.date===item.date){purchase[i].total=decimalString(item.quantity*item.price);return false;}});if(managerGrid){managerGrid.setData(purchase);managerGrid.updateRowCount();managerGrid.render();}});}
function resetForm(){$('#manager-product-input').val('').focus();$('#manager-supplier-input').val('');$('#manager-price-input').val('');$('.manager-price-info').html('');$('#manager-quantity-input').val('');currentProduct=null;currentDate=null;}
function toggleImport(){$('#manager-form .import').toggle();if($('#manager-form .import').is(':hidden')){$(this).html('Show import form.');}
else{$(this).html('Hide import form.');}}
function loadImportData(){currentImport=0;importData=[];if(!window.FileReader){alert('Sorry your browser doesn\'t support reading files.');$('#manager-import-file').val('');return;}
var file=$('#manager-import-file').get(0).files[0];var reader=new FileReader();reader.onload=function(e){var data=e.target.result.match(/^(.*)[\r\n]*$/gm);if(!data){alert('Imported file format doesn\'t match');$('#manager-import-file').val('');return;}
if(importData.length===0){processExports(data);}
if(importData.length===0){processOrders(data);}
if(importData.length===0){processSovereignFoods(data);}
if(importData.length===0){alert('Couldn\'t process data from imported file.');$('#manager-import-file').val('');}
else{$('.import .info').html('<br>Purchases found in imported data file. '+'Processing <b>1/'+importData.length+'</b>:');$('.manager .remove').button('option','label','skip');$('.manager .back').show();showImportData();}};reader.readAsText(file);}
function showPreviousImport(){if(currentImport>1){currentImport-=2;showImportData();}
return false;}
function showImportData(){if(currentImport===importData.length){exitImport('Data processing complete.');return;}
var productFound=false;var row=importData[currentImport++];$('#manager-username-input').val(row.user);$('#manager-product-input').val(dobrado.decode(row.name));$('#manager-supplier-input').val(row.supplier);$('#manager-quantity-input').val(row.quantity);$('#manager-price-input').val(row.price);$('.manager-price-info').html('');var dateFields=row.date.match(/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/);if(!dateFields||dateFields.length!==4){$('#manager-date-input').val('');alert('Date format in imported purchase doesn\'t match');}
else{var day=parseInt(dateFields[3],10);var month=parseInt(dateFields[2],10);var year=parseInt(dateFields[1],10);$('#manager-date-input').val(dobrado.formatDate(year,month,day));}
$.each(manager.products,function(index,item){if(item.name.toLowerCase()===row.name.toLowerCase()&&(item.user===row.supplier||(row.supplier===''&&item.available))){productFound=true;if(row.supplier===''){$('#manager-supplier-input').val(item.user);}
currentProduct=item;$('#manager-product-input').val(dobrado.decode(item.name));if(item.unit==='variable'){$('#manager-price-input').val(row.price).attr('readonly',false);$('#manager-quantity-input').spinner('disable');}
else{$('#manager-price-input').val('$'+row.total+' @ ($'+
row.price+'/'+item.unit+')');$('#manager-price-input').attr('readonly',true);var priceLevel=manager.buyerGroup[row.user];if(!priceLevel){priceLevel='retail';}
var itemPrice=decimalString(item[priceLevel]);if(itemPrice!==row.price){$('.manager-price-info').html('Stock price now $'+itemPrice+'/'+item.unit);}
$('#manager-quantity-input').spinner('enable');}
return false;}});$('.import .info').html('<br>Purchases found in imported data file. '+'Processing <b>'+currentImport+'/'+
importData.length+'</b>:');if(!productFound){alert('Couldn\'t match supplier for: '+row.name+'.\n'+'Please check stock availability.');}}
function processExports(data){var headerFound=false;var headerCheck=0;$.each(data,function(i,item){item=item.replace(/\".*?\"/g,function(match){match=match.replace(/,/g,'');return match.replace(/\"/g,'');});var re=/^,?(.*),*[\r\n]*$/;var match=item.match(re);var row=[];if(match&&match.length===2){row=match[1].split(',');}
if(row.length===7){if(!headerFound){if(row[0]==='date'&&row[1]==='user'&&row[2]==='name'&&row[3]==='supplier'&&row[4]==='quantity'&&row[5]==='price'&&row[6]==='total'){headerFound=true;}
else if(headerCheck>40){return false;}
headerCheck++;return true;}
else{importData.push({date:row[0],user:row[1],name:row[2],supplier:row[3],quantity:row[4],price:row[5],total:row[6]});}}});}
function processOrders(data){var headerFound=false;var headerCheck=0;var processed=true;var format='';var purchaseDate='';var purchaseUser='';var price=0;var quantity=0;var groups=[];$.each(data,function(i,item){item=item.replace(/\".*?\"/g,function(match){match=match.replace(/,/g,'');return match.replace(/\"/g,'');});var re=/^,?(.*),*[\r\n]*$/;var match=item.match(re);var row=[];if(match&&match.length===2){row=match[1].split(',');}
if(row.length>=5){if(!headerFound){if(row[0]==='Product'&&row[1]==='Quantity'&&row[2]==='Pack Size'&&row[3]==='Packs'&&row[4]==='Price'){if($('#manager-username-input').val()===''){alert('Please select a username for this import.');processed=false;return false;}
if($('#manager-date-input').val()===''){alert('Please enter a purchase date for this import.');processed=false;return false;}
purchaseUser=$('#manager-username-input').val();purchaseDate=$.datepicker.formatDate('yy-mm-dd',$('#manager-date-input').datepicker('getDate'));format='single';headerFound=true;}
else if(row[0]==='Product'&&row[1]==='Co-op'&&row[2]==='Quantity'&&row[3]==='Pack Size'&&row[4]==='Packs'&&row[5]==='Price'){if($('#manager-date-input').val()===''){alert('Please enter a purchase date for this import.');processed=false;return false;}
purchaseDate=$.datepicker.formatDate('yy-mm-dd',$('#manager-date-input').datepicker('getDate'));format='vertical';headerFound=true;}
else if(row[0]==='Product'){for(var j=1;j<row.length-4;j++){if(row[j]==='Total'){break;}
groups.push(row[j]);}
if(row[groups.length+1]==='Total'&&row[groups.length+2]==='Pack Size'&&row[groups.length+3]==='Packs'&&row[groups.length+4]==='Price'){if($('#manager-date-input').val()===''){alert('Please enter a purchase date for this import.');processed=false;return false;}
purchaseDate=$.datepicker.formatDate('yy-mm-dd',$('#manager-date-input').datepicker('getDate'));format='horizontal';headerFound=true;}}
else if(headerCheck>5){processed=false;return false;}
headerCheck++;return true;}
if(format==='single'){let match=row[4].match(/^\$([0-9.]+)/);if(match&&match.length===2){price=match[1];}
else{price=0;}
quantity=parseFloat(row[1]);if(quantity&&quantity>0){importData.push({date:purchaseDate,user:purchaseUser,name:row[0],supplier:'',quantity:quantity,price:price,total:decimalString(quantity*price)});}}
else if(format==='vertical'){let match=row[5].match(/^\$([0-9.]+)/);if(match&&match.length===2){price=match[1];}
else{price=0;}
quantity=parseFloat(row[2]);if(quantity&&quantity>0){importData.push({date:purchaseDate,user:row[1],name:row[0],supplier:'',quantity:quantity,price:price,total:decimalString(quantity*price)});}}
else if(format==='horizontal'){let match=row[groups.length+4].match(/^\$([0-9.]+)/);if(match&&match.length===2){price=match[1];}
else{price=0;}
$.each(groups,function(j,user){quantity=parseFloat(row[j+1]);if(quantity&&quantity>0){importData.push({date:purchaseDate,user:user.toLowerCase(),name:row[0],supplier:'',quantity:quantity,price:price,total:decimalString(quantity*price)});}});}}});return processed;}
function processSovereignFoods(data){var headerFound=false;var headerCheck=0;var emptyLineCount=0;var purchaseDate=$.datepicker.formatDate('yy-mm-dd',$('#manager-date-input').datepicker('getDate'));var purchaseUser=$('#manager-username-input').val();$.each(data,function(i,item){item=item.replace(/\".*?\"/g,function(match){match=match.replace(/,/g,'');return match.replace(/\"/g,'');});var re=/^,?(.*),*[\r\n]*$/;var match=item.match(re);var row=[];if(match&&match.length===2){row=match[1].split(',');}
if(row.length>=7){if(!headerFound){if(row[0].trim().toLowerCase()==='name'&&row[1].trim().toLowerCase()==='unit'&&row[2].trim().toLowerCase()==='unitprice'&&row[3].trim().toLowerCase()==='quantity'&&row[4].trim().toLowerCase()==='total'&&row[6].trim().toLowerCase()==='grower'&&row[7].trim().toLowerCase()==='category'){headerFound=true;}
else if(headerCheck>40){return false;}
headerCheck++;return true;}
if(row[0]===''){emptyLineCount++;if(emptyLineCount===2){return false;}
return true;}
else if(row[3]===''||row[3]==='0'){return true;}
else{let price=decimalString(parseFloat(row[2]));let total=row[4];if(total===''){total=decimalString(parseFloat(row[2])*parseFloat(row[3]));}
importData.push({date:purchaseDate,user:purchaseUser,name:row[0],supplier:'',quantity:row[3],price:price,total:total});}}});}
function exitImport(message){$('.import .info').html(message);$('.manager .remove').button('option','label','remove');$('.manager .back').hide();$('#manager-import-file').val('');importData=[];currentImport=0;}}());