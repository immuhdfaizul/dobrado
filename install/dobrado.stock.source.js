/*global dobrado: true, CKEDITOR: true, Slick: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2020 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.stock) {
  dobrado.stock = {};
}
(function() {

  'use strict';

  // This is a representation of the stock table and users in json.
  var stock = [];
  // This is an instance of slick grid, if available on the page.
  var stockGrid = null;
  var stockGridView = null;
  var stockGridId = '';
  // If there's another grid module, it's used to display quantity adjustments.
  var adjustmentGrid = null;
  var adjustmentGridId = '';
  var adjustmentData = [];
  // Flag for exporting adjustments for one or all products.
  var allAdjustments = false;
  // The supplier username to use with the imported data.
  var importSupplier = '';
  // Import data loaded from a file.
  var importData = [];
  // The currently selected product from the grid.
  var currentProduct = null;
  // Stop submit function being called again before it returns.
  var saving = false;
  // List of all columns that can be displayed in the grid, the user can
  // decide which columns they want to show. It's populated in gridSetup once
  // we know if there's a grid module on the page so that Slick is available.
  var allColumns = [];
  // columnFilters are passed along with any download requests so that they
  // can also be applied on the server.
  var columnFilters = {};
  // Track the current product, supplier and unit for updates.
  var currentName = '';
  var currentSupplier = '';
  var currentUnit = '';
  // Track which checkboxes are displayed in the product form.
  var checkboxDisplayed = [];
  // This is the auto-complete list for categories.
  var categories = [];
  // The grid image editor needs to be called when an image is selected using
  // the browser module.
  var currentImageEditor = null;
  //  The stock module manages it's own CKEDITOR for use with descriptions.
  var editor = null;
  
  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.stock').length === 0) {
      return;
    }

    if (!dobrado.mobile) {
      $('#stock-display').dialog({
        show: true, autoOpen: false, width: 760, height: 450,
        position: { my: 'top', at: 'top+50', of: window },
        title: 'Stock Display',
        create: dobrado.fixedDialog });
      $('#stock-form').dialog({
        show: true, autoOpen: false, width: 760, height: 450,
        position: { my: 'top', at: 'top+50', of: window },
        title: 'Stock Form', close: hideCheckbox,
        create: dobrado.fixedDialog });
      $('.stock-quantity-dialog').dialog({
        show: true, autoOpen: false, width: 600, height: 400,
        position: { my: 'top', at: 'top+50', of: window },
        title: 'Stock Adjustment',
        create: dobrado.fixedDialog });
      $('#stock-profile-dialog').dialog({
        show: true, autoOpen: false, width: 600, height: 400,
        position: { my: 'top', at: 'top+50', of: window },
        title: 'Stock Column Profiles', open: checkProfile,
        create: dobrado.fixedDialog });
    }

    $('#stock-group-select').selectmenu({ change: changeGroup });
    $('#stock-import-file').val('').change(loadImportData);
    $('#stock-import-supplier').selectmenu({ select: checkOtherSupplier });
    $('#stock-open-display-form').button().click(function() {
      if (dobrado.mobile) {
        $('#stock-display').toggle();
        // Simplify the page on mobile by hiding the grid when stock-display
        // is open.
        $(stockGridId).toggle();
      }
      else {
        $('#stock-display').dialog('open');
      }
    });
    $('#stock-open-product-form').button().click(function() {
      if (dobrado.mobile) {
        $('#stock-form').toggle();
        // Simplify the page on mobile by hiding the grid when stock-form
        // is open.
        $(stockGridId).toggle();
      }
      else {
        $('#stock-form').dialog('open');
      }
      showCheckbox();
    });
    $('#stock-import-help').button().click(function() {
      $('.stock-import-info').toggle();
    });
    $('#stock-profiles').selectmenu({ change: changeProfile });
    $('#stock-save-profile').button().click(function() {
      $('#stock-profiles').val('edit');
      changeProfile();
      return false;
    });
    $('#stock-profile-update').button().click(updateProfile);
    $('#stock-profile-new').button().click(function() {
      $('#stock-profile-create').toggle();
      return false;
    });
    $('#stock-profile-add').button().click(addProfile);
    $('.stock-profile-remove').button({
      icon: 'ui-icon-closethick', showLabel: false }).click(removeProfile);
    $('#stock-reset-available').button().click(resetProfile);
    $('#stock-reset-available-retail').button().click(resetProfile);
    $('#stock-reset-available-wholesale').button().click(resetProfile);
    $('#stock-reset-all').button().click(resetProfile);
    $('#stock-download').button({
      icon: 'ui-icon-arrowthickstop-1-s', showLabel: false }).click(download);
    $('#stock-profile-download').controlgroup();
    $('#stock-import-wrapper').hide();
    $('#stock-show-import').prop('checked', false).
      checkboxradio({ icon: false }).click(showImportOptions);
    $('#stock-data input').click(updateDisplay);
    // Clear column display option checkboxes and set from local storage.
    $('#stock-columns input').prop('checked', false).click(updateColumns);
    // Show stock module (in case it was hidden in CSS) now that setup is done.
    $('.stock').show();

    // Initialise product form (which is not shown by default).
    $('#stock-default-action').button().click(submit);
    $('#stock-form-previous').button().click(previous);
    $('#stock-form-next').button().click(next);
    $('#stock-form-clear').button().click(function() {
      resetForm(true);
      return false;
    });
    $('#stock-form .remove').button().click(remove);
    $('#stock-form .submit').button().click(submit);
    $('#stock-form .adjust').button().click(adjust);
    $('#stock-name-input').val('').change(function() {
      setTimeout(function() { showProduct(); }, 10);
    });
    $('#stock-move-name-input').val('').change(function() {
      setTimeout(function() { showMoveSupplier(); }, 10);
    });
    $('#stock-user-input').val('').change(function() {
      setTimeout(function() { showProduct(); }, 10);
    });
    $('#stock-name-input').keypress(checkProductEnter);
    $('#stock-unit-select').selectmenu();
    $('#stock-price-input').change(updatePrices);
    $('#stock-wholesale-markup-input').change(updateWholesale);
    $('#stock-retail-markup-input').change(updateRetail);
    $('#stock-size-input').change(updateSize);
    $('#stock-checkbox-select').val('').selectmenu({ change: showCheckbox });
    $('#stock-add-supplier').button().click(dobrado.account.registerSupplier);
    $('#stock-new-quantity-input').change(checkTracking);
    $('#stock-image-browse').button().click(browser);
    $('.stock-quantity-dialog .submit').button().click(adjustQuantity);
    $('.stock-quantity-dialog .show-all').button().click(showAllAdjustments);
    $('.stock-quantity-dialog .export').button().click(exportAdjustments);
    $('#stock-alternative').checkboxradio({
      icon: false, disabled: true }).click(showAlternative);
    $('#stock-order-available-input').click(updateSupplierInput);
    $('#stock-purchase-available-input').click(updateSupplierInput);
    $('.stock-list-all').button().click(listAll);
    $('.stock-move-link').click(function() {
      $('.stock-move').toggle();
      $('.stock-adjust').toggle();
    });

    CKEDITOR.addCss('.cke_editable { margin: 10px; }');
    editor = CKEDITOR.replace('stock-description-textarea',
      { allowedContent: true,
        height: 150,
        autoGrow_minHeight: 150,
        autoGrow_onStartup: true,
        disableNativeSpellChecker: false,
        enterMode: CKEDITOR.ENTER_BR,
        filebrowserBrowseUrl: '/php/browse.php',
        removePlugins:
          'elementspath,tableselection,tabletools,contextmenu,liststyle',
        resize_enabled: false,
        toolbar: [[ 'Source', '-', 'NewPage' ],
                  [ 'Undo', 'Redo', '-', 'SelectAll', 'RemoveFormat' ],
                  [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea',
                    'Select', 'Button', 'ImageButton', 'HiddenField'],
                  [ 'Bold', 'Italic', 'Underline', 'Strike' ],
                  [ 'Subscript', 'Superscript'],
                  [ 'NumberedList', 'BulletedList', '-', 'Outdent',
                    'Indent', 'Blockquote', 'CreateDiv'],
                  [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                    'JustifyBlock'],
                  [ 'Link', 'Unlink', 'Anchor'],
                  [ 'EmojiPanel', 'Image', 'Table', 'HorizontalRule',
                    'SpecialChar', 'PageBreak'],
                  [ 'Format', 'Font', 'FontSize'],
                  [ 'TextColor', 'BGColor'],
                  [ 'Maximize', 'ShowBlocks', '-', 'About']]
      });
    resetForm(false);

    // If columns isn't set in local storage create a default.
    let columns = JSON.stringify(['product', 'supplier', 'price', 'unit',
                                  'available']);
    let sort = '';
    let profile = '';
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columns = localStorage.stockColumns;
      }
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
      if (localStorage.stockProfile) {
        profile = localStorage.stockProfile;
        $('#stock-profiles').val(profile).selectmenu('refresh');
      }
      if (localStorage.stockProducts) {
        $('#' + localStorage.stockProducts).prop('checked', true);
      }
      if (localStorage.stockPrice) {
        $('#' + localStorage.stockPrice).prop('checked', true);
      }
    }
    dobrado.log('Loading stock...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'list', columns: columns, sort: sort,
             profile: profile, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock list')) {
          return;
        }
        stock = JSON.parse(response);
        updateProducts();
        updateNames();

        // If a grid module is on the page load stock into it.
        if ($('.grid').length !== 0) {
          gridSetup();
        }
        else {
          alert('Please add a grid module to this page.');
        }
      });
  });

  function browser() {
    dobrado.createModule('browser', 'browser', 'stock');
    return false;
  }

  function decimalString(value) {
    // This function is required because we often have values that lack the
    // precision required by the toFixed function. For example, having the
    // number 5 in the 3rd (and last) decimal place isn't a true representation
    // of that number when converted to floating point, and so it's possible
    // that it will be rounded down here to provide 2 decimal places. To avoid
    // this ambiguity, extra precision is added before rounding the value.
    return (value + 0.0001).toFixed(2);
  }

  function addProfile() {
    var name = $('#stock-profile-input').val();
    if (name === '') return false;

    var columns = '';
    var sort = '';
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columns = localStorage.stockColumns;
      }
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
    }
    dobrado.log('Adding profile.', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'addProfile', name: name,
             columns: columns, sort: sort, url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock addProfile')) {
          return;
        }
        var profile = JSON.parse(response);
        $('#stock-profiles').html(profile.options);
        $('#stock-profiles').selectmenu('refresh');
        $('#stock-profile-input').val('');
        if (dobrado.mobile) {
          $('#stock-profile-dialog').hide();
        }
        else {
          $('#stock-profile-dialog').dialog('close');
        }
      });
    return false;
  }

  function updateProfile() {
    var profile = '';
    var columns = '';
    var sort = '';
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columns = localStorage.stockColumns;
      }
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
      if (localStorage.stockProfile) {
        profile = localStorage.stockProfile;
      }
    }
    dobrado.log('Updating profile.', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'addProfile', name: profile,
             columns: columns, sort: sort, url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock updateProfile')) {
          return;
        }
        if (dobrado.mobile) {
          $('#stock-profile-dialog').hide();
        }
        else {
          $('#stock-profile-dialog').dialog('close');
        }
      });
    return false;
  }

  function checkFormat(name) {
    // Allow for more options than the regex shows by not matching what comes
    // after the start of the unit name.
    return /\d+\s*(each|g|kg|kilo|l)/i.test(name);
  }

  function checkOtherSupplier(event, ui) {
    if (ui.item.value === 'other') {
      // Refresh the selectmenu so that the default option is displayed next
      // time the menu is displayed.
      $('#stock-import-supplier').val('').selectmenu('refresh');
      $('#stock-import-supplier-button').hide();
      $('#stock-import-input').val('').show();
      let usernames = [];
      $.each(stock.users, function(index, item) {
        usernames.push(item);
      });
      $('#stock-import-input').autocomplete({ minLength: 1,
                                              search: dobrado.fixAutoCompleteMemoryLeak,
                                              source: usernames });
    }
  }

  function checkProfile() {
    if (dobrado.localStorage && localStorage.stockProfile) {
      let profile = $('#stock-profiles > option:selected').html();
      $('#stock-current-profile').html(profile);
      $('#stock-profile-options').show();
      $('#stock-profile-create').hide();
    }
    else {
      $('#stock-profile-create').show();
      $('#stock-profile-options').hide();
    }
  }

  function download() {
    // If columns isn't set in local storage create a default.
    var columns = JSON.stringify(['product', 'supplier', 'price', 'unit',
                                  'available']);
    var sort = '';
    var profile = '';
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columns = localStorage.stockColumns;
      }
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
      if (localStorage.stockProfile) {
        profile = localStorage.stockProfile;
      }
    }

    dobrado.log('Downloading...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'download', columns: columns,
             sort: sort, profile: profile, filters: columnFilters,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock download')) {
          return;
        }
        var data = JSON.parse(response);
        location.href = '/php/private.php?file=' + data.filename;
      });
  }

  function gridSetup() {

    function categoryEditor(args) {
      var $input;
      var defaultValue;

      // This allows navigating the autocomplete list rather than the grid.
      this.keyCaptureList = [$.ui.keyCode.UP, $.ui.keyCode.DOWN];

      this.init = function() {
        $input = $('<input type="text" class="editor-text">')
          .appendTo(args.container)
          .on('keydown.nav', function (e) {
            if (e.keyCode === $.ui.keyCode.LEFT ||
                e.keyCode === $.ui.keyCode.RIGHT) {
              e.stopImmediatePropagation();
            }
          })
          .autocomplete({ minLength: 0,
                          search: dobrado.fixAutoCompleteMemoryLeak,
                          source: categories })
          .focus()
          .select();
      };

      this.destroy = function() {
        $input.autocomplete('destroy');
        $input.remove();
      };

      this.focus = function() {
        $input.focus();
      };

      this.getValue = function() {
        return $input.val();
      };

      this.setValue = function(val) {
        $input.val(val);
      };

      this.loadValue = function(item) {
        defaultValue = item[args.column.field] || '';
        $input.val(defaultValue);
        $input[0].defaultValue = defaultValue;
        $input.select();
      };

      this.serializeValue = function() {
        return $input.val();
      };

      this.applyValue = function(item, state) {
        item[args.column.field] = state;
      };

      this.isValueChanged = function() {
        return !($input.val() === '' && !defaultValue) &&
          $input.val() !== defaultValue;
      };

      this.validate = function() {
        if (args.column.validator) {
          var validationResults = args.column.validator($input.val());
          if (!validationResults.valid) {
            return validationResults;
          }
        }
        return { valid: true, msg: null };
      };

      this.init();
    }

    function imageEditor(args) {
      var $input;
      var $wrapper;
      var defaultValue;
      currentImageEditor = this;

      this.init = function() {
        var $container = $('body');

        $wrapper = $('<div style="z-index:100;position:absolute;' +
                     'background:white;width:200px;padding:5px;' +
                     'border:2px solid gray;-moz-border-radius:5px;' +
                     'border-radius:5px;"></div>').appendTo($container);

        $input = $('<img src="" style="width:190px;height:auto;">').
          appendTo($wrapper);

        $('<div><button class="stock-image-remove">Remove</button>' +
          '<button class="stock-image-edit">Edit</button></div>').
          appendTo($wrapper);

        $wrapper.find('button.stock-image-edit').button().click(this.edit);
        $wrapper.find('button.stock-image-remove').button().click(this.remove);
        $wrapper.on('keydown', this.handleKeyDown);

        currentImageEditor.position(args.position);
        $input.focus();
      };

      this.handleKeyDown = function(e) {
        if (e.which == $.ui.keyCode.ESCAPE) {
          e.preventDefault();
          currentImageEditor.cancel();
        }
        else if (e.which == $.ui.keyCode.TAB && e.shiftKey) {
          e.preventDefault();
          args.grid.navigatePrev();
        }
        else if (e.which == $.ui.keyCode.TAB) {
          e.preventDefault();
          args.grid.navigateNext();
        }
      };

      this.edit = function() {
        dobrado.createModule('browser', 'browser', 'stock');
      };

      this.save = function(filename) {
        $input.attr('src', filename);
        args.commitChanges();
        $wrapper.hide();
      };

      this.cancel = function() {
        $input.attr('src', defaultValue);
        args.cancelChanges();
      };

      this.remove = function() {
        $input.attr('src', '');
        args.commitChanges();
      };

      this.hide = function() {
        $wrapper.hide();
      };

      this.show = function() {
        $wrapper.show();
      };

      this.position = function (position) {
        $wrapper.css('top', position.top - 5).css('left', position.left - 5);
      };

      this.destroy = function() {
        $wrapper.remove();
        currentImageEditor = null;
      };

      this.focus = function() {
        $input.focus();
      };

      this.loadValue = function(item) {
        defaultValue = item[args.column.field] || '';
        // Add '_thumb' to the end of the image name.
        var fields = defaultValue.match(/^(.+)(\.[a-z]+)$/i);
        if (fields && fields.length === 3) {
          defaultValue = fields[1] + '_thumb' + fields[2];
        }
        $input.attr('src', defaultValue);
        $input.focus();
      };

      this.serializeValue = function() {
        return $input.attr('src');
      };

      this.applyValue = function(item, state) {
        item[args.column.field] = state;
      };

      this.isValueChanged = function() {
        return !($input.attr('src') === '' && !defaultValue) &&
          $input.attr('src') !== defaultValue;
      };

      this.validate = function() {
        if (args.column.validator) {
          var validationResults = args.column.validator($input.attr('src'));
          if (!validationResults.valid) {
            return validationResults;
          }
        }
        return { valid: true, msg: null };
      };

      this.init();
    }

    function imageFormatter(row, cell, value, columnDef, dataContext) {
      if (value === '') return '';

      // Add '_thumb' to the end of the image name.
      var fields = value.match(/^(.+)(\.[a-z]+)$/i);
      if (fields && fields.length === 3) {
        value = fields[1] + '_thumb' + fields[2];
      }
      return '<img src="' + value + '" style="height:20px;width:auto;">';
    }

    function markupEditor(args) {
      var $input;
      var defaultValue;

      this.init = function() {
        $input = $('<INPUT type=text class="editor-text">');
        $input.on('keydown.nav', function (e) {
          if (e.keyCode === $.ui.keyCode.LEFT ||
              e.keyCode === $.ui.keyCode.RIGHT) {
            e.stopImmediatePropagation();
          }
        });
        $input.appendTo(args.container);
        $input.focus().select();
      };

      this.destroy = function () {
        $input.remove();
      };

      this.focus = function () {
        $input.focus();
      };

      this.loadValue = function (item) {
        var value = parseFloat(item[args.column.field]);
        var price = parseFloat(item.price);
        if (price === 0 || isNaN(price) || isNaN(value)) {
          defaultValue = '';
        }
        else {
          defaultValue = ((value - price) / price * 100).toFixed(0);
        }
        $input.val(defaultValue);
        $input[0].defaultValue = defaultValue;
        $input.select();
      };

      this.serializeValue = function () {
        var rtn = parseFloat($input.val());
        if (!rtn) {
          return 0;
        }
        return rtn;
      };

      this.applyValue = function (item, state) {
        item[args.column.field] =
          decimalString(item.price * (100 + parseFloat(state)) / 100);
      };

      this.isValueChanged = function () {
        return (!($input.val() == '' && defaultValue == null)) &&
          ($input.val() != defaultValue);
      };

      this.validate = function () {
        if (isNaN($input.val())) {
          return {
            valid: false,
            msg: 'Please enter a valid number'
          };
        }

        if (args.column.validator) {
          var validationResults = args.column.validator($input.val());
          if (!validationResults.valid) {
            return validationResults;
          }
        }

        return {
          valid: true,
          msg: null
        };
      };

      this.init();
    }

    function wholesaleFormatter(row, cell, value, columnDef, dataContext) {
      var price = parseFloat(dataContext.price);
      if (price === 0 || isNaN(price)) {
        return '';
      }
      return ((value - price) / price * 100).toFixed(0) + '%';
    }

    function retailFormatter(row, cell, value, columnDef, dataContext) {
      var price = parseFloat(dataContext.price);
      if (price === 0 || isNaN(price)) {
        return '';
      }
      return ((value - price) / price * 100).toFixed(0) + '%';
    }

    function filter(item, args) {
      for (var columnId in args.columnFilters) {
        if (columnId && args.columnFilters[columnId] !== '') {
          var column = args.columns[args.index(columnId)];
          var input = args.columnFilters[columnId].toLowerCase();
          var field = item[column.field].toString().toLowerCase();
          if (columnId === 'wholesaleMarkup') {
            field = ((item.wholesale - item.price) / item.price) * 100;
          }
          else if (columnId === 'retailMarkup') {
            field = ((item.retail - item.price) / item.price) * 100;
          }
          if (input.indexOf('*') === 0) {
            if (field.indexOf(input.substr(1)) === -1) {
              return false;
            }
          }
          else if (input.indexOf('<') === 0) {
            let compare = input.match(/^<\s*([0-9.]+)/);
            if (compare && parseFloat(compare[1]) - 0.001 < parseFloat(field)) {
              return false;
            }
          }
          else if (input.indexOf('>') === 0) {
            let compare = input.match(/^>\s*([0-9.]+)/);
            if (compare && parseFloat(compare[1]) + 0.001 > parseFloat(field)) {
              return false;
            }
          }
          else if (field.indexOf(input) !== 0) {
            return false;
          }
        }
      }
      return true;
    }

    var priceName = 'Price';
    var availableName = 'Members';
    var availableToolTip = 'Available to Members';
    if (stock.availableName === 'customers') {
      availableName = 'Customers';
      availableToolTip = 'Available to Customers';
    }
    // Get the id's for all the grid modules on the page.
    $('.grid').each(function(index) {
      if (index === 0) {
        stockGridId = '#' + $(this).attr('id');
      }
      if (index === 1) {
        if (stock.trackQuantity) {
          adjustmentGridId = '#' + $(this).attr('id');
        }
        else {
          $(this).hide();
        }
      }
    });
    // Don't continue grid setup if there's no data. Also display the stock
    // form in the page in this case. Assume this is a new setup but only if
    // not viewing hidden products, and show a welcome message in this case.
    if (stock.products.length === 0) {
      $(stockGridId).hide();
      if (dobrado.mobile) {
        if (localStorage.stockProducts != 'stock-data-hidden-products') {
          $('#stock-form').show();
          $('#stock-data-type').html('Welcome to the stock page! You can ' +
                                     'create products by filling in the form ' +
                                     'below.');
        }
      }
      else {
        if (localStorage.stockProducts != 'stock-data-hidden-products') {
          $('#stock-form').dialog('open');
          $('#stock-data-type').html('Welcome to the stock page! You can ' +
                                     'create products by clicking ' +
                                     'Open Product Form.');
        }
      }
      return;
    }

    if (stock.orderAvailable) {
      availableName = 'Order';
      availableToolTip = 'Available to Order';
    }
    if (stock.wholesalePercent || stock.retailPercent) {
      priceName = 'Cost';
    }
    allColumns = [{ id: 'product', name: 'Product', field: 'name',
                    width: 200, sortable: true, editor: Slick.Editors.Text },
                  { id: 'supplier', name: 'Supplier', field: 'fullname',
                    width: 100, sortable: true },
                  { id: 'quantity', name: 'Quantity', field: 'quantity',
                    width: 60, sortable: true },
                  { id: 'price', name: priceName, field: 'price', width: 60,
                    sortable: true, formatter: Slick.Formatters.Dollar,
                    editor: Slick.Editors.Float },
                  { id : 'wholesaleMarkup', name: 'Wholesale Markup',
                    field: 'wholesale', width: 60, sortable: true,
                    formatter: wholesaleFormatter, editor: markupEditor },
                  { id: 'wholesale', name: 'Wholesale', field: 'wholesale',
                    width: 60, sortable: true,
                    formatter: Slick.Formatters.Dollar,
                    editor: Slick.Editors.Float },
                  { id : 'retailMarkup', name: 'Retail Markup',
                    field: 'retail', width: 60, sortable: true,
                    formatter: retailFormatter, editor: markupEditor },
                  { id: 'retail', name: 'Retail', field: 'retail', width: 60,
                    sortable: true, formatter: Slick.Formatters.Dollar,
                    editor: Slick.Editors.Float },
                  { id: 'unit', name: 'Unit', field: 'unit', width: 60,
                    sortable: true, formatter: Slick.Formatters.StockUnits,
                    editor: Slick.Editors.StockUnits },
                  { id: 'unitPrice', name: 'Pack Price', field: 'unitPrice',
                    width: 60, sortable: true,
                    formatter: Slick.Formatters.Dollar },
                  { id: 'size', name: 'Pack Size', field: 'size', width: 60,
                    sortable: true, editor: Slick.Editors.Float },
                  { id: 'alternative', name: 'Alternative',
                    field: 'alternative', width: 80, sortable: true,
                    formatter: Slick.Formatters.Checkmark },
                  { id: 'category', name: 'Category', field: 'category',
                    width: 90, sortable: true, editor: categoryEditor },
                  { id: 'description', name: 'Description',
                    field: 'description', width: 90, sortable: true,
                    editor: Slick.Editors.LongText },
                  { id: 'image', name: 'Image', field: 'image',
                    width: 60, sortable: true, formatter: imageFormatter,
                    editor: imageEditor },
                  { id: 'grower', name: 'Grower', field: 'grower',
                    width: 90, sortable: true, editor: Slick.Editors.LongText },
                  { id: 'taxable', name: 'Taxable', field: 'taxable',
                    width: 50, sortable: true,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'composite', name: 'Composite', field: 'composite',
                    width: 80, sortable: true,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'track', name: 'Track', field: 'track', width: 40,
                    sortable: true, formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'bulk', name: 'Bulk', field: 'bulk', width: 40,
                    sortable: true, formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'cart', name: 'Cart', field: 'cart', width: 40,
                    sortable: true, formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'available', name: availableName,
                    field: 'available', width: 50, sortable: true,
                    toolTip: availableToolTip,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'purchaseAvailable', name: 'Purchase',
                    field: 'purchaseAvailable', width: 60, sortable: true,
                    toolTip: 'Available to Purchase',
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'supplierAvailable', name: 'Available',
                    field: 'supplierAvailable', width: 60, sortable: true,
                    toolTip: 'Available from Supplier',
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'hidden', name: 'Hidden', field: 'hidden', width: 60,
                    sortable: true, formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox }];

    var columns = [];
    // If columns isn't set in local storage create a default.
    var columnIds = ['product', 'supplier', 'price', 'unit', 'available'];
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columnIds = JSON.parse(localStorage.stockColumns);
      }
      else {
        localStorage.stockColumns = JSON.stringify(columnIds);
      }
    }
    $.each(columnIds, function(i, id) {
      $('#stock-column-' + id).prop('checked', true);
      $.each(allColumns, function(i, item) {
        if (item.id === id) {
          columns.push(item);
          return false;
        }
      });
    });
    stockGridView = new Slick.Data.DataView({ inlineFilters: true });
    var options = { autoHeight: true, editable: true,
                    explicitInitialization: true, forceFitColumns: true,
                    headerRowHeight: 30, multiColumnSort: true,
                    showHeaderRow: true  };
    stockGrid = dobrado.grid.instance(stockGridId, stockGridView, columns,
                                      options);
    stockGrid.setSelectionModel(new Slick.RowSelectionModel());
    stockGrid.onBeforeEditCell.subscribe(updateCurrent);
    stockGrid.onCellChange.subscribe(updateStock);
    stockGrid.onClick.subscribe(function(e, item) {
      showProduct(stockGridView.getItem(item.row));
    });
    stockGrid.onSelectedRowsChanged.subscribe(function(e, item) {
      if (item.rows.length === 0) {
        resetForm(false);
        showCheckbox();
        $('#stock-form-buttons').show();
        $('#stock-name-input').parent().show();
        $('#stock-user-input').parent().show();
        $('label[for=stock-alternative]').show();
        $('#stock-total-quantity-input').parent().show();
        if (!dobrado.mobile) {
          $('#stock-form').dialog('option', 'title', 'Stock Form');
        }
      }
      else if (item.rows.length === 1) {
        showCheckbox();
        $('#stock-form-buttons').show();
        $('#stock-name-input').parent().show();
        $('#stock-user-input').parent().show();
        $('label[for=stock-alternative]').show();
        $('#stock-total-quantity-input').parent().show();
        if (!dobrado.mobile) {
          $('#stock-form').dialog('option', 'title', 'Stock Form');
        }
        showProduct(stockGridView.getItem(item.rows[0]));
      }
      else {
        // Otherwise reset the form, as all new values will be applied to
        // all selected rows on submit. Also hide fields that can't be updated
        // when multiple rows are selected.
        resetForm(false);
        hideCheckbox();
        $('#stock-form-buttons').hide();
        $('#stock-name-input').parent().hide();
        $('#stock-user-input').parent().hide();
        $('label[for=stock-alternative]').hide();
        $('#stock-total-quantity-input').parent().hide();
        if (dobrado.mobile) {
          $('#stock-form').show();
        }
        else {
          $('#stock-form').dialog('option', 'title', 'Stock Form: Updating ' +
                                  item.rows.length + ' products');
          $('#stock-form').dialog('open');
        }
      }
    });
    stockGrid.onSort.subscribe(function (e, args) {
      stockGridView.sort(function(row1, row2) {
        var cols = args.sortCols;
        for (var i = 0; i < cols.length; i++) {
          var id = cols[i].sortCol.id;
          var field = cols[i].sortCol.field;
          var sign = cols[i].sortAsc ? 1 : -1;
          var value1 = row1[field];
          var value2 = row2[field];
          // Markup sorting needs to be checked first because they share the
          // same fields as wholesale and retail.
          if (id === 'wholesaleMarkup' || id === 'retailMarkup') {
            value1 = (value1 - row1.price) / row1.price;
            value2 = (value2 - row2.price) / row2.price;
          }
          else if (field === 'price' || field === 'wholesale' ||
                   field === 'retail' || field === 'unitPrice') {
            value1 = parseFloat(value1);
            value2 = parseFloat(value2);
          }
          if (value1 === value2) {
            continue;
          }
          if (value1 > value2) {
            return sign;
          }
          else {
            return sign * -1;
          }
        }
        return 0;
      }, true);
      if (dobrado.localStorage) {
        localStorage.stockSortColumns =
          JSON.stringify(stockGrid.getSortColumns());
      }
    });
    stockGridView.onRowCountChanged.subscribe(function (e, args) {
      stockGrid.updateRowCount();
      stockGrid.render();
    });
    stockGridView.onRowsChanged.subscribe(function (e, args) {
      stockGrid.invalidateRows(args.rows);
      stockGrid.render();
    });
    $(stockGrid.getHeaderRow()).on('change keyup', ':input', function (e) {
      var columnId = $(this).data('columnId');
      if (columnId) {
        columnFilters[columnId] = $.trim($(this).val());
        stockGridView.setFilterArgs({ columns: stockGrid.getColumns(),
                                      columnFilters: columnFilters,
                                      index: stockGrid.getColumnIndex });
        stockGridView.refresh();
      }
    });
    stockGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
      $(args.node).empty();
      $('<input type="text">').data('columnId', args.column.id).
        val(columnFilters[args.column.id]).appendTo(args.node);
    });
    stockGrid.onColumnsReordered.subscribe(function(e, args) {
      if (dobrado.localStorage) {
        let columnIds = [];
        $.each(stockGrid.getColumns(), function(index, column) {
          columnIds.push(column.id);
        });
        localStorage.stockColumns = JSON.stringify(columnIds);
      }
    });
    stockGrid.init();
    stockGridView.beginUpdate();
    stockGridView.setItems(stock.products);
    // FilterArgs are provided here (and above, when filter input changes) so
    // that the filter can be inlined by SlickGrid, which seems to make it a
    // bit faster. The columns and columnFilters args are static when the filter
    // is called, but getColumnIndex needs to be called by the filter so the
    // function is passed in here as index.
    stockGridView.setFilterArgs({ columns: stockGrid.getColumns(),
                                  columnFilters: columnFilters,
                                  index: stockGrid.getColumnIndex });
    stockGridView.setFilter(filter);
    stockGridView.endUpdate();

    $('#stock-wide-grid').prop('checked', false).checkboxradio({ icon: false }).
      click(function() {
        dobrado.grid.setup('stock', 'wide', stockGrid, stockGridId);
      });
    if (dobrado.localStorage) {
      if (localStorage.stockWideGrid === 'true') {
        // Want to call toggleWideGrid and check the checkbox, this does both.
        $('#stock-wide-grid').click();
      }
      if (localStorage.stockSortColumns) {
        stockGrid.setSortColumns(JSON.parse(localStorage.stockSortColumns));
      }
    }
    // Now that the stock grid is rendered fix the header row to the top of the
    // page when scrolled.
    dobrado.grid.setup('stock', 'fixed', stockGrid, stockGridId);

    // If a second grid module is on the page it's used to display
    // quantity adjustments.
    if ($('.grid').length === 2 && stock.trackQuantity) {
      var adjustmentColumns =
        [{ id: 'product', name: 'Product', field: 'name', width: 200,
           sortable: true },
         { id: 'supplier', name: 'Supplier', field: 'supplier',
           width: 130, sortable: true },
         { id: 'date', name: 'Date', field: 'date', width: 110,
           sortable: true, formatter: Slick.Formatters.Timestamp },
         { id: 'adjustment', name: 'Adjustment', field: 'adjustment',
           width: 120, sortable: true }];
      var adjustmentOptions = {
        enableColumnReorder: false,
        forceFitColumns: true
      };

      // Want the grid module to be part of the dialog, so move it.
      $(adjustmentGridId).appendTo($('.stock-adjustment-grid'));
      $(adjustmentGridId + ' .view').css('width', '560px');
      adjustmentGrid = dobrado.grid.instance(adjustmentGridId, [],
                                             adjustmentColumns,
                                             adjustmentOptions);
      adjustmentGrid.setSelectionModel(new Slick.RowSelectionModel());
      adjustmentGrid.onClick.subscribe(function (e, item) {
        showAdjustment(item.row);
      });
      adjustmentGrid.onSelectedRowsChanged.subscribe(function(e, item) {
        if (item.rows.length === 1) {
          showAdjustment(item.rows[0]);
        }
      });
      adjustmentGrid.onSort.subscribe(function (e, args) {
        adjustmentData.sort(function(row1, row2) {
          var field = args.sortCol.field;
          var sign = args.sortAsc ? 1 : -1;
          var value1 = row1[field];
          var value2 = row2[field];
          if (field !== 'product' && field !== 'supplier') {
            value1 = parseFloat(value1);
            value2 = parseFloat(value2);
          }
          if (value1 === value2) {
            return 0;
          }
          if (value1 > value2) {
            return sign;
          }
          else {
            return sign * -1;
          }
        });
        adjustmentGrid.invalidate();
      });
    }
  }

  function checkProductEnter(event) {
    if (event.keyCode !== 13) {
      return;
    }
    event.preventDefault();
  }

  function checkTracking() {
    var quantity = $('#stock-new-quantity-input').val();
    if (quantity !== '' && quantity !== '0') {
      $('#stock-track-input').prop('checked', true);
    }
  }

  function hideCheckbox() {
    $('#stock-checkbox-select').parent().show();
    $.each(checkboxDisplayed, function(index, item) {
      $('#stock-' + item + '-input').parent().hide();
    });
    checkboxDisplayed = [];
  }

  function markup(value, price) {
    price = parseFloat(price);
    if (price === 0 || isNaN(price)) {
      return '';
    }
    return ((value - price) / price * 100).toFixed(0);
  }

  function next() {
    if (stockGrid) {
      var selected = stockGrid.getSelectedRows();
      var position = 0;
      if (selected.length !== 0) {
        position = selected[0] + 1;
        // Wrap to the start of the list.
        if (position === stockGridView.getLength()) {
          position = 0;
        }
      }
      stockGrid.setSelectedRows([position]);
    }
    return false;
  }

  function previous() {
    if (stockGrid) {
      var selected = stockGrid.getSelectedRows();
      var position = 0;
      if (selected.length !== 0) {
        position = selected[0] - 1;
        // Wrap to the end of the list.
        if (position === -1) {
          position = stockGridView.getLength() - 1;
        }
      }
      stockGrid.setSelectedRows([position]);
    }
    return false;
  }

  function removeProfile() {
    var button = $(this);
    var value = button.attr('id').match(/stock-profile-remove-(.+)$/)[1];
    dobrado.log('Removing profile.', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'removeProfile', value: value,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock removeProfile')) {
          return;
        }
        var profile = JSON.parse(response);
        $('#stock-profiles').html(profile.options);
        $('#stock-profiles').selectmenu('refresh');
        button.parent().remove();
        if (dobrado.localStorage) {
          localStorage.stockProfile = '';
        }
      });
    return false;
  }

  function resetProfile() {
    var value = $(this).attr('id').match(/stock-reset-(.+)$/)[1];
    dobrado.log('Resetting profile.', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'removeProfile', value: value,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock resetProfile')) {
          return;
        }
        $('#stock-profiles').val(value);
        if (dobrado.mobile) {
          $('#stock-profile-dialog').hide();
        }
        else {
          $('#stock-profile-dialog').dialog('close');
        }
        changeProfile();
      });
    return false;
  }

  function showCheckbox() {
    var value = $('#stock-checkbox-select').val();
    if (value === '') {
      // showCheckbox is called without a value being set when a single row is
      // selected in the grid, in which case we want to show all checkboxes.
      // The checkboxDisplayed array is only updated so that all the checkboxes
      // will be hidden if hideCheckbox is called.
      checkboxDisplayed = ['order-available', 'purchase-available',
                           'supplier-available', 'taxable', 'composite',
                           'bulk', 'cart', 'hidden'];
      $.each(checkboxDisplayed, function(index, value) {
        $('#stock-' + value + '-input').parent().show();
      });
      // Don't need to show the select when all checkboxes are shown.
      $('#stock-checkbox-select').parent().hide();
    }
    else {
      $('#stock-checkbox-select').val('').selectmenu('refresh');
      if ($.inArray(value, checkboxDisplayed) === -1) {
        checkboxDisplayed.push(value);
        $('#stock-' + value + '-input').prop('checked', true);
        $('#stock-' + value + '-input').parent().show();
        // When order or purchase availability is updated make sure supplier
        // availability is updated too.
        if (value === 'order-available' || value === 'purchase-available') {
          checkboxDisplayed.push('supplier-available');
          $('#stock-supplier-available-input').prop('checked', true);
          $('#stock-supplier-available-input').parent().show();
        }
      }
    }
  }

  function showMoveSupplier(product) {
    if ($('#stock-move-user-input').val() !== '') {
      return;
    }

    var name = $('#stock-move-name-input').val();
    $.each(stock.products, function(index, item) {
      if (name === item.name) {
        $('#stock-move-user-input').val(item.user);
        return false;
      }
    });
  }

  function showProduct(product) {
    var productFound = false;
    var name = $('#stock-name-input').val();
    var supplier = $('#stock-user-input').val();
    $('#stock-order-price-info').html('');
    if (product) {
      name = product.name;
      supplier = product.user;
    }

    $.each(stock.products, function(index, item) {
      // If the user didn't click on a grid row, or enter a supplier in the
      // form, just show the first item matching the given product name.
      if (name === item.name) {
        // If the name matches, then a product is found, but only update the
        // form and set currentProduct if the supplier matches (or is empty)
        // as the user could be entering a new product for a new supplier.
        productFound = true;
        if (supplier === '' || supplier === item.user) {
          currentProduct = item;
          // Update the form based on the item given.
          $('#stock-name-input').val(item.name);
          $('#stock-user-input').val(item.user);
          $('#stock-total-quantity-input').val(item.quantity);
          $('#stock-unit-select').val(item.unit).selectmenu('refresh');
          $('#stock-size-input').val(item.size);
          $('#stock-price-input').val(item.price);
          $('#stock-wholesale-input').val(item.wholesale);
          $('#stock-wholesale-markup-input').val(markup(item.wholesale,
                                                        item.price));
          $('#stock-retail-input').val(item.retail);
          $('#stock-retail-markup-input').val(markup(item.retail,
                                                     item.price));
          $('#stock-category-input').val(item.category);
          $('#stock-image-input').val(item.image);
          $('#stock-grower-input').val(item.grower);
          $('#stock-order-available-input').
            prop('checked', item.available === 1);
          $('#stock-purchase-available-input').
            prop('checked', item.purchaseAvailable === 1);
          $('#stock-supplier-available-input').
            prop('checked', item.supplierAvailable === 1);
          $('#stock-taxable-input').prop('checked', item.taxable === 1);
          $('#stock-composite-input').prop('checked', item.composite === 1);
          $('#stock-track-input').prop('checked', item.track === 1);
          $('#stock-bulk-input').prop('checked', item.bulk === 1);
          $('#stock-cart-input').prop('checked', item.cart === 1);
          $('#stock-hidden-input').prop('checked', item.hidden === 1);
          $('#stock-alternative').prop('checked', false);
          $('#stock-alternative').checkboxradio('refresh');
          $('#stock-alternative').checkboxradio('option', 'disabled',
                                                item.alternative === 0);
          editor.setData(item.description);
          return false;
        }
      }
    });
    // When a new product is being created reset total quantity.
    if (!productFound) {
      $('#stock-total-quantity-input').val(0);
    }
  }

  function showAlternative() {
    var name = $('#stock-name-input').val();
    var supplier = $('#stock-user-input').val();
    if (stock.alternative[supplier]) {
      var alternative = stock.alternative[supplier][name];
      if (alternative) {
        if ($('#stock-alternative').is(':checked')) {
          $('#stock-size-input').val(alternative.halfSize);
          $('#stock-price-input').val(alternative.halfPrice);
        }
        else {
          $('#stock-size-input').val(alternative.fullSize);
          $('#stock-price-input').val(alternative.fullPrice);
        }
        updatePrices();
      }
    }
  }

  function showImportOptions() {
    $('#stock-import-wrapper').toggle();
    if ($('#stock-import-wrapper').is(':visible')) {
      // When the import options are shown reset to show select menu by default.
      $('#stock-import-supplier-button').show();
      $('#stock-import-input').val('').hide();
      $('#stock-other-buttons-wrapper').hide();
    }
    else {
      $('#stock-other-buttons-wrapper').show();
    }
  }

  function updateColumns() {
    // When updating columns always add the product column first.
    var columns = [allColumns[0]];
    var columnIds = ['product'];
    $('#stock-columns input:checked').each(function() {
      var id = $(this).attr('id').match(/([^-]+)$/)[1];
      $.each(allColumns, function(i, item) {
        if (item.id === id) {
          columns.push(item);
          columnIds.push(id);
          return false;
        }
      });
    });
    if (dobrado.localStorage) {
      let currentColumns = JSON.parse(localStorage.stockColumns);
      // If the column count hasn't changed then don't do anything.
      // (ie not interested in this click event.)
      if (currentColumns.length === columnIds.length) {
        return;
      }

      // When a column is being removed, need to reset the sort order.
      if (currentColumns.length > columnIds.length) {
        localStorage.stockSortColumns = '';
      }
      localStorage.stockColumns = JSON.stringify(columnIds);
    }
    if (stockGrid) {
      stockGrid.resetActiveCell();
      stockGrid.setSelectedRows([]);
      stockGrid.setColumns(columns);
      stockGrid.setSortColumns([]);
      stockGrid.render();
    }
  }

  function updateCurrent(e, args) {
    if (args && args.item) {
      currentName = args.item.name;
      currentSupplier = args.item.user;
      currentUnit = args.item.unit;
    }
  }

  function updateDisplay() {
    var sort = '';
    var columns = '';
    var profile = '';
    var dataType = $(this).attr('id');
    if (dobrado.localStorage) {
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
      if (localStorage.stockColumns) {
        columns = localStorage.stockColumns;
      }
      if (localStorage.stockProfile) {
        profile = localStorage.stockProfile;
      }
      if (dataType === 'stock-data-available-products' ||
          dataType === 'stock-data-all-products' ||
          dataType === 'stock-data-hidden-products') {
        localStorage.stockProducts = dataType;
      }
      if (dataType === 'stock-data-order-price' ||
          dataType === 'stock-data-purchase-price') {
        localStorage.stockPrice = dataType;
      }
    }

    dobrado.log('Loading new data...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'list', columns: columns, sort: sort,
             profile: profile, dataType: dataType,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock updateDisplay')) {
          return;
        }
        if (!stockGrid) {
          // This can happen when switching from a display with no data.
          location.reload();
          return;
        }
        stock = JSON.parse(response);
        $('#stock-data-type').html(stock.dataType);
        updateProducts();
        updateNames();
        if (sort !== '') {
          stockGrid.setSortColumns(JSON.parse(sort));
        }
        stockGridView.setItems(stock.products);
        stockGrid.invalidate();
      });
  }

  function updateProducts() {

    function showProductFromMenu(event, ui) {
      $.each(stock.products, function(index, item) {
        if (ui.item.value === item.name) {
          // showProduct just needs the product name here, it will try
          // and match a supplier if a value is found in the input field.
          showProduct(item);
          return false;
        }
      });
    }

    // Create an autocomplete list for the product name.
    var products = [];
    $.each(stock.products, function(index, item) {
      if ($.inArray(item.name, products) === -1) {
        products.push(item.name);
      }
    });
    $('#stock-name-input').autocomplete({ minLength: 1,
                                          search: dobrado.fixAutoCompleteMemoryLeak,
                                          source: products,
                                          select: showProductFromMenu });
    // Also update the product input used when moving stock quantities.
    $('#stock-move-name-input').autocomplete({ minLength: 1,
                                               search: dobrado.fixAutoCompleteMemoryLeak,
                                               source: products });

    // Also create an autocomplete list for categories.
    categories = [];
    $.each(stock.products, function(index, item) {
      if (item.category !== '' && $.inArray(item.category, categories) === -1) {
        categories.push(item.category);
      }
    });
    $('#stock-category-input').autocomplete({ minLength: 0,
                                              search: dobrado.fixAutoCompleteMemoryLeak,
                                              source: categories });
  }

  function updateNames() {

    function showSupplierFromMenu(event, ui) {
      // The input field doesn't get updated before the select event fires,
      // so update it now so that we can call showProduct.
      $('#stock-user-input').val(ui.item.value);
      showProduct();
    }

    // Create an autocomplete list of usernames.
    var usernames = [];
    $.each(stock.users, function(index, item) {
      usernames.push(item);
    });
    $('#stock-user-input').autocomplete({ minLength: 1,
                                          search: dobrado.fixAutoCompleteMemoryLeak,
                                          source: usernames,
                                          select: showSupplierFromMenu });
    // Also update the user input used when moving stock quantities.
    $('#stock-move-user-input').autocomplete({ minLength: 1,
                                               search: dobrado.fixAutoCompleteMemoryLeak,
                                               source: usernames });
  }

  function updatePrices() {
    var price = $('#stock-price-input').val();
    // Remove a $ prefix if given.
    if (price[0] === '$') {
      price = price.substring(1);
      $('#stock-price-input').val(price);
    }
    if (!$.isNumeric(price)) {
      $('#stock-price-input').val('');
      return false;
    }

    if (stock.wholesalePercent) {
      let wholesale = parseFloat(price) * (100 + stock.wholesalePercent) / 100;
      $('#stock-wholesale-input').val(decimalString(wholesale));
      $('#stock-wholesale-markup-input').val(stock.wholesalePercent);
    }
    else {
      // The markup fields may still be shown but manually updated. In this
      // case the markup can still be updated when the cost price changes.
      let wholesale = $('#stock-wholesale-input').val();
      $('#stock-wholesale-markup-input').val(markup(wholesale, price));
    }
    if (stock.retailPercent) {
      let retail = parseFloat(price) * (100 + stock.retailPercent) / 100;
      $('#stock-retail-input').val(decimalString(retail));
      $('#stock-retail-markup-input').val(stock.retailPercent);
    }
    else {
      let retail = $('#stock-retail-input').val();
      $('#stock-retail-markup-input').val(markup(retail, price));
    }
    return false;
  }

  function updateWholesale() {
    var price = $('#stock-price-input').val();
    var markup = $('#stock-wholesale-markup-input').val();
    if (!$.isNumeric(markup)) {
      $('#stock-wholesale-markup-input').val('');
      return false;
    }

    if ($.isNumeric(price)) {
      let wholesale = parseFloat(price) * (100 + parseFloat(markup)) / 100;
      $('#stock-wholesale-input').val(decimalString(wholesale));
    }
    return false;
  }

  function updateRetail() {
    var price = $('#stock-price-input').val();
    var markup = $('#stock-retail-markup-input').val();
    if (!$.isNumeric(markup)) {
      $('#stock-retail-markup-input').val('');
      return false;
    }

    if ($.isNumeric(price)) {
      let retail = parseFloat(price) * (100 + parseFloat(markup)) / 100;
      $('#stock-retail-input').val(decimalString(retail));
    }
    return false;
  }

  function updateSize() {
    var size = parseFloat($('#stock-size-input').val());
    var price = parseFloat($('#stock-price-input').val());
    if (currentProduct && $.isNumeric(size) &&
        $.isNumeric(price) && size !== currentProduct.size) {
      var newPrice = price;
      if (size > 0.001 && currentProduct.size > 0.001) {
        newPrice = currentProduct.size / size * price;
      }
      $('#stock-price-input').val(decimalString(newPrice));
      currentProduct.size = size;
    }
    updatePrices();
    return false;
  }

  function updateStock(e, args) {
    if (!stockGrid) {
      alert('There was a problem loading the stock grid. ' +
            'Please reload the page.');
      return;
    }

    // Get the id of the column that contains the updated cell.
    var columns = stockGrid.getColumns();
    var field = columns[args.cell].field;
    var value = args.item[field];

    if (field === 'unit' &&
        value === 'adjusted' && !checkFormat(args.item.name)) {
      alert('Product name must include quantity and units to adjust by.');
      args.item.unit = currentUnit;
      stockGridView.updateItem(args.item.id, args.item);
      return;
    }
    if ((field === 'available' || field === 'purchaseAvailable') && value) {
      // Make sure supplier availability is set when available to members.
      args.item.supplierAvailable = 1;
      stockGridView.updateItem(args.item.id, args.item);
    }
    else if (field === 'supplierAvailable' && !value) {
      // Also make sure member availability is not set when unavailable from a
      // supplier.
      args.item.available = 0;
      args.item.purchaseAvailable = 0;
      stockGridView.updateItem(args.item.id, args.item);
    }

    dobrado.log('Updating product.', 'info');
    // Need to save currentName as it will change before the callback returns.
    var product = currentName;
    $.post('/php/request.php',
           { request: 'stock', action: 'save', field: field, value: value,
             product: product, supplier: currentSupplier,
             composite: args.item.composite, url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock updateStock')) {
          return;
        }
        var save = JSON.parse(response);
        if (save.other) {
          alert(product + ' from ' + save.other + ' is now unavailable');
          // Update the grid for the other supplier.
          let id = product + '-' + save.other;
          let other = stockGridView.getItemById(id);
          other.available = 0;
          other.purchaseAvailable = 0;
          stockGridView.updateItem(id, other);
        }
        if (save.wholesale || save.retail) {
          let id = product + '-' + currentSupplier;
          let update = stockGridView.getItemById(id);
          if (save.wholesale) {
            update.wholesale = save.wholesale;
          }
          if (save.retail) {
            update.retail = save.retail;
          }
          stockGridView.updateItem(id, update);
        }
      });
  }

  function remove() {

    function removeProduct() {
      if (selected.length > 1) {
        if (!confirmRemoveMultiple) {
          if (confirm('Remove ' + selected.length + ' products?')) {
            confirmRemoveMultiple = true;
            currentRemove = 0;
          }
          else {
            return false;
          }
        }
        if (currentRemove === selected.length) {
          confirmRemoveMultiple = false;
          resetForm(false);
          showAll(false);
          return false;
        }
        showProduct(stockGridView.getItem(selected[currentRemove++]));
      }

      var supplier = $('#stock-user-input').val();
      if ($.inArray(supplier, stock.users) === -1) {
        alert('Supplier not found.');
        confirmRemoveMultiple = false;
        return false;
      }

      if (selected.length === 1) {
        dobrado.log('Removing product...', 'info');
        confirmRemoveMultiple = false;
      }
      else {
        dobrado.log('Removing product ' + currentRemove + '/' +
                    selected.length + '...', 'info');
      }
      var composite = $('#stock-composite-input:checked').length;
      $.post('/php/request.php',
             { request: 'stock', action: 'remove',
               name: $('#stock-name-input').val(), supplier: supplier,
               composite: composite, multiple: confirmRemoveMultiple,
               columns: columns, sort: sort, profile: profile,
               url: location.href, token: dobrado.token },
        function(response) {
          // Don't do anything other than call remove again if multiple rows
          // are being removed, the grid will be cleared at the end. Note that
          // errors are ignored so that products with existing purchases can be
          // skipped.
          if (confirmRemoveMultiple) {
            removeProduct();
            return;
          }

          if (dobrado.checkResponseError(response, 'stock remove')) {
            return;
          }

          // When an item is removed a new stock list is returned.
          stock = JSON.parse(response);
          updateProducts();
          updateNames();
          // Update the grid if available, which also references stock.
          if (stockGrid) {
            stockGridView.setItems(stock.products);
            stockGrid.setSelectedRows([]);
          }
          resetForm(false);
        });
    }

    if (!stockGrid) {
      alert('There was a problem loading the stock grid. ' +
            'Please reload the page.');
      return;
    }

    // The index of the selected row being removed.
    var currentRemove = 0;
    // Confirm with the user when multiple rows are about to be removed.
    var confirmRemoveMultiple = false;
    var selected = stockGrid.getSelectedRows();
    // If columns isn't set in local storage create a default.
    var columns = JSON.stringify(['product', 'supplier', 'price', 'unit',
                                  'available']);
    var sort = '';
    var profile = '';
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columns = localStorage.stockColumns;
      }
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
      if (localStorage.stockProfile) {
        profile = localStorage.stockProfile;
      }
    }
    removeProduct();
    return false;
  }

  function adjust() {
    var name = $('#stock-name-input').val();
    var supplier = $('#stock-user-input').val();
    if (name === '') {
      alert('Please enter a product.');
      return false;
    }
    if ($.inArray(supplier, stock.users) === -1) {
      alert('Supplier not found.');
      return false;
    }
    $('#stock-quantity-adjust-input').val('');
    $('#stock-quantity-reason-input').val('');
    $('#stock-move-name-input').val('');
    $('#stock-move-user-input').val('');
    $('.stock-move').hide();
    $('.stock-adjust').show();
    allAdjustments = false;

    dobrado.log('Loading adjustments...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'listAdjustments', name: name,
             supplier: supplier, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock adjust')) {
          return;
        }
        adjustmentData = JSON.parse(response);
        if (adjustmentGrid) {
          adjustmentGrid.setData(adjustmentData);
          adjustmentGrid.updateRowCount();
          adjustmentGrid.render();
          adjustmentGrid.setSelectedRows([]);
        }
        // Set the current total for this product in the adjustment form.
        var total = $('#stock-total-quantity-input').val();
        $('#stock-quantity-adjust-input').val(total);
        if (dobrado.mobile) {
          $('.stock-quantity-dialog').show();
        }
        else {
          $('.stock-quantity-dialog').dialog('open');
        }
      });
    return false;
  }

  function adjustQuantity() {
    if (!stockGrid) {
      alert('There was a problem loading the stock grid. ' +
            'Please reload the page.');
      return false;
    }

    var name = $('#stock-name-input').val();
    var supplier = $('#stock-user-input').val();
    var total = parseFloat($('#stock-quantity-adjust-input').val());
    var description = $('#stock-quantity-reason-input').val();
    var moveName = $('#stock-move-name-input').val();
    var moveSupplier = $('#stock-move-user-input').val();

    if (isNaN(total)) {
      alert('Quantity must be a number.');
      return;
    }
    if (moveName === '' && moveSupplier === '') {
      if (total === parseFloat($('#stock-total-quantity-input').val())) {
        alert('Total quantity hasn\'t changed.');
        return;
      }
      if (description === '') {
        alert('Please enter a reason for the adjustment.');
        return;
      }
    }
    else {
      // When moving quantity to another product, make sure correct
      // information is provided.
      if (moveName === name) {
        alert('Product name hasn\'t changed.');
        return;
      }
      if (moveName === '' || moveName === name) {
        alert('Please provide a product to move quantity to.');
        return;
      }
      if (moveSupplier === '') {
        alert('Please provide a supplier to move quantity to.');
        return;
      }
      if (parseFloat($('#stock-total-quantity-input').val()) === 0) {
        alert('There is no quantity to move for the selected product.');
        return;
      }
    }
    
    dobrado.log('Saving adjustment.', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'saveAdjustment', name: name,
             supplier: supplier, moveName: moveName, moveSupplier: moveSupplier,
             total: total, description: description, url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock adjustQuantity')) {
          return;
        }
        var adjust = JSON.parse(response);
        if (moveName === '' && moveSupplier === '') {
          $('#stock-total-quantity-input').val(total);
        }
        else {
          $('#stock-total-quantity-input').val(0);
        }
        // Update the grid view of these stock items.
        var oldId = name + '-' + supplier;
        var oldItem = stockGridView.getItemById(oldId);
        if (moveName === '' && moveSupplier === '') {
          oldItem.quantity = total;
          stockGridView.updateItem(oldId, oldItem);
        }
        else {
          var newId = moveName + '-' + moveSupplier;
          var newItem = stockGridView.getItemById(newId);
          newItem.quantity += total;
          stockGridView.updateItem(newId, newItem);
          oldItem.quantity = 0;
          stockGridView.updateItem(oldId, oldItem);
        }
        var rows = stockGridView.mapIdsToRows([oldId]);
        stockGrid.setSelectedRows(rows);
        if (dobrado.mobile) {
          $('.stock-quantity-dialog').hide();
        }
        else {
          $('.stock-quantity-dialog').dialog('close');
        }
        if (adjust.remove && confirm('Remove ' + name + ' from stock?')) {
          remove();
        }
      });
  }

  function showAdjustment(row) {
    if (!adjustmentData) {
      return;
    }

    var data = adjustmentData[row];
    $('#stock-quantity-reason-input').val(dobrado.decode(data.description));
  }

  function showAllAdjustments() {
    dobrado.log('Loading all adjustments...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'listAllAdjustments',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock showAllAdjustments')) {
          return;
        }
        adjustmentData = JSON.parse(response);
        allAdjustments = true;
        if (adjustmentGrid) {
          adjustmentGrid.setData(adjustmentData);
          adjustmentGrid.updateRowCount();
          adjustmentGrid.render();
          adjustmentGrid.setSelectedRows([]);
        }
      });
  }

  function exportAdjustments() {
    var name = $('#stock-name-input').val();
    var supplier = $('#stock-user-input').val();

    dobrado.log('Exporting adjustments...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'exportAdjustments',
             allAdjustments: allAdjustments, name: name,
             supplier: supplier, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock exportAdjustments')) {
          return;
        }
        var data = JSON.parse(response);
        location.href = '/php/private.php?file=' + data.filename;
      });
    return false;
  }

  function submit() {
    // Don't allow submit to be called more than once before returning.
    if (saving) {
      return false;
    }

    // Want to set new and total quantity to zero if empty, but using
    // parseFloat and isNaN covers all cases.
    var quantity = parseFloat($('#stock-new-quantity-input').val());
    if (isNaN(quantity)) {
      quantity = 0;
    }
    var total = parseFloat($('#stock-total-quantity-input').val());
    if (isNaN(total)) {
      total = 0;
    }
    total += quantity;
    var unit = $('#stock-unit-select').val();
    // size can't be a string due to comparison in import function.
    var size = parseFloat($('#stock-size-input').val());
    if (isNaN(size)) {
      size = 0;
    }
    var price = $('#stock-price-input').val();
    var wholesale = $('#stock-wholesale-input').val();
    var wholesaleMarkup = $('#stock-wholesale-markup-input').val();
    var retail = $('#stock-retail-input').val();
    var retailMarkup = $('#stock-retail-markup-input').val();
    var category = $('#stock-category-input').val();
    var description = editor.getData();
    var image = $('#stock-image-input').val();
    var grower = $('#stock-grower-input').val();
    var available = $('#stock-order-available-input:checked').length;
    var purchaseAvailable = $('#stock-purchase-available-input:checked').length;
    var supplierAvailable = $('#stock-supplier-available-input:checked').length;
    var taxable = $('#stock-taxable-input:checked').length;
    var composite = $('#stock-composite-input:checked').length;
    var track = $('#stock-track-input:checked').length;
    var bulk = $('#stock-bulk-input:checked').length;
    var cart = $('#stock-cart-input:checked').length;
    var hidden = $('#stock-hidden-input:checked').length;

    if (available === 1 || purchaseAvailable === 1) {
      // Don't allow setting unavailable from the supplier when being made
      // available to members.
      supplierAvailable = 1;
    }
    var selected = stockGrid ? stockGrid.getSelectedRows() : [];
    if (selected.length > 1) {
      if (!confirm('Update ' + selected.length + ' products?')) {
        return false;
      }

      let data = [];
      $.each(selected, function(i, row) {
        var selectedItem = stockGridView.getItem(row);
        if (unit === 'adjusted' && !checkFormat(selectedItem.name)) {
          alert('Product name must include quantity and units to adjust by.');
          return false;
        }
        data.push({ name: selectedItem.name, supplier: selectedItem.user });
      });
      if (data.length === 0) {
        return false;
      }

      $.post('/php/request.php',
             { request: 'stock', action: 'editMultiple',
               data: JSON.stringify(data), quantity: quantity, unit: unit,
               size: size, price: price, wholesale: wholesale,
               wholesaleMarkup: wholesaleMarkup, retail: retail,
               retailMarkup: retailMarkup, category: category,
               description: description, image: image, grower: grower,
               orderAvailable: available, purchaseAvailable: purchaseAvailable,
               supplierAvailable: supplierAvailable, taxable: taxable,
               composite: composite, track: track, bulk: bulk, cart: cart,
               hidden: hidden, update: JSON.stringify(checkboxDisplayed),
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'stock submit multiple')) {
            return;
          }
          resetForm(false);
          setTimeout(function() { showAll(false); }, 1000);
          if (!dobrado.mobile) {
            $('#stock-form').dialog('close');
          }
        });
      return false;
    }

    let name = $('#stock-name-input').val();
    if (name === '') {
      return false;
    }
    if (unit === 'adjusted' && !checkFormat(name)) {
      alert('Product name must include quantity and units to adjust by.');
      return false;
    }

    let supplier = $('#stock-user-input').val();
    if ($.inArray(supplier, stock.users) === -1) {
      alert('Please click Add Supplier to create a new account.');
      $('#stock-user-input').val('');
      return false;
    }

    saving = true;
    $('#stock-form .submit').button('option', 'disabled', true);
    dobrado.log('Saving stock...', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'edit', name: name, supplier: supplier,
             quantity: quantity, unit: unit, size: size, price: price,
             wholesale: wholesale, retail: retail, category: category,
             description: description, image: image, grower: grower,
             orderAvailable: available, purchaseAvailable: purchaseAvailable,
             supplierAvailable: supplierAvailable, taxable: taxable,
             composite: composite, track: track, bulk: bulk, cart: cart,
             hidden: hidden, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock submit')) {
          saving = false;
          $('#stock-form .submit').button('option', 'disabled', false);
          return;
        }
        if (!stockGrid) {
          // This should only be required the first time a product is added.
          location.reload();
          return;
        }

        var edit = JSON.parse(response);
        if (edit.other) {
          alert(name + ' from ' + edit.other + ' is now unavailable');
          // Update the grid for the other supplier.
          let otherId = name + '-' + edit.other;
          let otherItem = stockGridView.getItemById(otherId);
          otherItem.available = 0;
          otherItem.purchaseAvailable = 0;
          stockGridView.updateItem(otherId, otherItem);
        }
        let id = name + '-' + supplier;
        let existingItem = stockGridView.getItemById(id);
        if (existingItem) {
          existingItem.quantity = total;
          existingItem.unit = unit;
          existingItem.size = size;
          existingItem.price = price;
          existingItem.wholesale = wholesale;
          existingItem.retail = retail;
          existingItem.category = category;
          existingItem.description = description;
          existingItem.image = image;
          existingItem.grower = grower;
          existingItem.available = available;
          existingItem.purchaseAvailable = purchaseAvailable;
          existingItem.supplierAvailable = supplierAvailable;
          existingItem.taxable = taxable;
          existingItem.composite = composite;
          existingItem.track = track;
          existingItem.bulk = bulk;
          existingItem.cart = cart;
          existingItem.hidden = hidden;
          stockGridView.updateItem(id, existingItem);
        }
        else {
          stockGridView.addItem({ id: id, name: name, user: supplier,
                                  fullname: supplier, quantity: total,
                                  unit: unit, size: size, price: price,
                                  wholesale: wholesale, retail: retail,
                                  category: category, description: description,
                                  image: image, grower: grower,
                                  available: available,
                                  purchaseAvailable: purchaseAvailable,
                                  supplierAvailable: supplierAvailable,
                                  taxable: taxable, composite: composite,
                                  track: track, bulk: bulk, cart: cart,
                                  hidden: hidden, updated: 0, alternative: 0 });
          // Add the new product to the autocomplete list.
          updateProducts();
          // Sort the stock by name.
          stockGridView.sort(function(row1, row2) {
            return row1.name > row2.name ? 1 : -1;
          }, true);
        }
        resetForm(false);
        saving = false;
        $('#stock-form .submit').button('option', 'disabled', false);
      });
    return false;
  }

  function listAll() {
    $('.stock-list-all').hide();
    $('.stock-new-available').html('');
    showAll(false);
  }

  function updateSupplierInput() {
    if ($(this).is(':checked')) {
      $('#stock-supplier-available-input').prop('checked', true);
    }
  }

  function resetForm(newProduct) {
    $('#stock-user-input').val('');
    $('#stock-name-input').val('');
    $('#stock-unit-select').val('none').selectmenu('refresh');
    $('#stock-total-quantity-input').val('');
    $('#stock-new-quantity-input').val('');
    $('#stock-size-input').val('');
    $('#stock-price-input').val('');
    $('#stock-order-price-info').html('');
    $('#stock-wholesale-input').val('');
    $('#stock-wholesale-markup-input').val('');
    $('#stock-retail-input').val('');
    $('#stock-retail-markup-input').val('');
    $('#stock-category-input').val('');
    $('#stock-image-input').val('');
    $('#stock-grower-input').val('');
    $('#stock-order-available-input').prop('checked', newProduct);
    $('#stock-purchase-available-input').prop('checked', newProduct);
    $('#stock-supplier-available-input').prop('checked', newProduct);
    $('#stock-taxable-input').prop('checked', false);
    $('#stock-composite-input').prop('checked', false);
    $('#stock-track-input').prop('checked', false);
    $('#stock-bulk-input').prop('checked', false);
    $('#stock-cart-input').prop('checked', false);
    $('#stock-hidden-input').prop('checked', false);
    if (newProduct) {
      showCheckbox();
    }
    editor.setData('');
    currentProduct = null;
    return false;
  }

  function showAll(resetColumns) {
    var columns = [];
    // If columns isn't set in local storage create a default.
    var columnIds = ['product', 'supplier', 'price', 'unit', 'available'];
    var sort = '';
    var profile = '';
    if (dobrado.localStorage) {
      if (localStorage.stockColumns) {
        columnIds = JSON.parse(localStorage.stockColumns);
      }
      if (localStorage.stockSortColumns) {
        sort = localStorage.stockSortColumns;
      }
      if (localStorage.stockProfile) {
        profile = localStorage.stockProfile;
      }
    }
    if (resetColumns) {
      $('#stock-columns input').prop('checked', false);
      $.each(columnIds, function(i, id) {
        $('#stock-column-' + id).prop('checked', true);
        $.each(allColumns, function(i, item) {
          if (item.id === id) {
            columns.push(item);
            return false;
          }
        });
      });
    }
    if (stockGrid) {
      stockGrid.resetActiveCell();
      stockGrid.setSelectedRows([]);
      if (resetColumns) {
        stockGrid.setColumns(columns);
      }
      dobrado.log('Updating grid...', 'info');
      $.post('/php/request.php',
             { request: 'stock', action: 'list',
               columns: JSON.stringify(columnIds), sort: sort,
               profile: profile, url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'stock showAll')) {
            return;
          }
          stock = JSON.parse(response);
          updateProducts();
          updateNames();
          stockGridView.setItems(stock.products);
          if (sort !== '') {
            stockGrid.setSortColumns(JSON.parse(sort));
          }
          stockGrid.invalidate();
        });
    }
    else {
      // This should only be required when a list is imported and there are no
      // existing products, so the grid was never initialised.
      location.reload();
    }
  }

  function loadImportData() {
    importSupplier = '';
    importData = [];

    if (!window.FileReader) {
      alert('Sorry your browser doesn\'t support reading files.');
      $('#stock-import-file').val('');
      return;
    }

    var file = $('#stock-import-file').get(0).files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      var data = e.target.result.match(/^(.*)[\r\n]*$/gm);
      if (!data) {
        alert('Imported file format doesn\'t match');
        $('#stock-import-file').val('');
        return;
      }
      // Go through each of the stock import routines looking for a match.
      if (processFoodConnectData(data)) {
        if (importSupplier === '') {
          alert('Please enter a supplier name before importing a file.');
          $('#stock-import-file').val('');
          return false;
        }
      }
      if (importData.length === 0) {
        if (processSovereignFoodsData(data)) {
          if (importSupplier === '') {
            alert('Please enter a supplier name before importing a file.');
            $('#stock-import-file').val('');
            return false;
          }
        }
      }
      if (importData.length === 0) {
        if (processEcoFarmsData(data)) {
          if (importSupplier === '') {
            alert('Please enter a supplier name before importing a file.');
            $('#stock-import-file').val('');
            return false;
          }
        }
      }
      if (importData.length === 0) {
        // The server uses the importSupplier as a flag for the above formats,
        // so make sure it's not set when sending data for multiple suppliers.
        importSupplier = '';
        processExportData(data);
      }
      if (importData.length === 0) {
        alert('Couldn\'t process data from imported file.');
        $('#stock-import-file').val('');
      }
      else {
        // If columns isn't set in local storage create a default.
        var columns = JSON.stringify(['product', 'supplier', 'price', 'unit',
                                      'available']);
        var sort = '';
        if (dobrado.localStorage) {
          if (localStorage.stockColumns) {
            columns = localStorage.stockColumns;
          }
          if (localStorage.stockSortColumns) {
            sort = localStorage.stockSortColumns;
          }
        }
        dobrado.log('Importing stock list...', 'info');
        $.post('/php/request.php',
               { request: 'stock', action: 'import',
                 data: JSON.stringify(importData), supplier: importSupplier,
                 columns: columns, sort: sort, profile: '',
                 url: location.href, token: dobrado.token },
          function(response) {
            if (dobrado.checkResponseError(response, 'stock loadImportData')) {
              return;
            }
            if (!stockGrid) {
              // This should only be required the first time import is used.
              location.reload();
              return;
            }
            stock = JSON.parse(response);
            $('#stock-import-file').val('');
            var text = '<b>Import finished:</b> Showing all items (0 new)<br>';
            if (stock.newCount !== 0) {
              text = '<b>Import finished:</b> ';
              if (stock.newCount === 1) {
                text += 'Showing 1 item that is now available';
              }
              else {
                text += 'Showing ' + stock.newCount +
                  ' items that are now available';
              }
              $('.stock-list-all').show();
            }
            $('.stock-new-available').html(text);
            if (stockGrid) {
              stockGridView.setItems(stock.products);
              stockGrid.setSelectedRows([]);
              stockGrid.invalidate();
            }
          });
      }
    };
    reader.readAsText(file);
  }

  function processFoodConnectData(data) {
    var headerFound = false;
    var headerCheck = 0;
    var emptyLineCount = 0;
    importSupplier = '';
    if ($('#stock-import-input').val() !== '') {
      importSupplier = $('#stock-import-input').val();
    }
    else {
      importSupplier = $('#stock-import-supplier').val();
    }

    $.each(data, function(i, item) {
      // Before checking the line format, need to check for any quotes, remove
      // commas between the quotes, and then remove the quotes themselves.
      item = item.replace(/\".*?\"/g, function(match) {
        match = match.replace(/,/g, '');
        return match.replace(/\"/g, '');
      });

      // Split on commas, but first clean up the row by removing initial and
      // final commas, and any newlines. 
      var re = /^,?(.*),*[\r\n]*$/;
      var match = item.match(re);
      var row = [];
      if (match.length === 2) {
        row = match[1].split(',');
      }
      if (row.length >= 9) {
        // First look for the header. Don't bother checking the Comments
        // header, as it can contain a newline which stuffs up the quote
        // remover above (since it doesn't check over multiple lines).
        // Once the header has been found, can process all rows after it.
        if (!headerFound) {
          if (row[0].trim().toLowerCase() === 'product' &&
              (row[1].substring(0, 11) === 'Description' ||
               row[2].substring(0, 8) === 'Quantity' ||
               row[3] === 'Pack Size' || row[4] === 'Unit' ||
               row[5] === 'Unit Price' || row[6] === 'Total' ||
               row[7].substring(0, 3) === 'GST' ||
               row[8].substring(0, 6) === 'Grower')) {
            headerFound = true;
            // Food Connect import confirmed, so make sure supplier is set.
            if (importSupplier === '') {
              return false;
            }
          }
          else if (row[0].trim().toLowerCase() === 'name' &&
                   row[1].trim().toLowerCase() === 'description' &&
                   row[2].trim().toLowerCase() === 'quantity' &&
                   row[3].trim().toLowerCase() === 'size' &&
                   row[4].trim().toLowerCase() === 'unit' &&
                   row[5].trim().toLowerCase() === 'unitprice' &&
                   row[6].trim().toLowerCase() === 'total' &&
                   row[8].trim().toLowerCase() === 'grower') {
            headerFound = true;
            // This is the single supplier export format, which is the same
            // as the Food Connect format except with a different header row.
            if (importSupplier === '') {
              return false;
            }
          }
          else if (headerCheck > 40) {
            // Stop after checking the first 40 rows of the file for a header.
            return false;
          }
          headerCheck++;
          return true;
        }
        // Skip lines with empty products, but if two consecutive empty lines
        // are found assume we've reached the end of the product list. (This
        // allows the billing lines at the end to be ignored).
        if (row[0] === '') {
          emptyLineCount++;
          if (emptyLineCount === 2) {
            return false;
          }
          return true;
        }
        else {
          // First reset the empty line count from above.
          emptyLineCount = 0;
          // The unit is free form text so try and condense to a limited set.
          // substrings are used to avoid spaces.
          var unit = row[4].toLowerCase();
          if (unit === 'l' || unit.substring(0, 5) === 'litre') {
            unit = 'L';
          }
          else if (unit === '100ml') {
            unit = '100mL';
          }
          else if (unit.substring(0, 1) === 'g' ||
                   unit.substring(0, 4) === 'gram') {
            unit = 'g';
          }
          else if (unit.substring(0, 2) === 'kg' ||
                   unit.substring(0, 4) === 'kilo') {
            unit = 'kg';
          }
          else if (unit !== 'variable' && unit !== 'adjusted') {
            unit = 'each';
          }
          // Pack size is usually just a number, but can be min-max. In this
          // case just use min.
          var size = 0;
          var matchSize = row[3].match(/^([0-9\.]+)/);
          if (matchSize && matchSize.length === 2) {
            size = parseFloat(matchSize[1]);
          }
          // The price given is per box, so divide by the pack size to get
          // a price per unit. Also first remove optional $ prefix.
          var price = row[5].trim();
          if (price[0] === '$') {
            price = price.substring(1);
          }
          price = parseFloat(price);
          if (size > 0.001) {
            price /= size;
          }
          var wholesale = price;
          if (stock.wholesalePercent) {
            wholesale = price * (100 + stock.wholesalePercent) / 100;
          }
          var retail = price;
          if (stock.retailPercent) {
            retail = price * (100 + stock.retailPercent) / 100;
          }
          let quantity = parseFloat(row[2]);
          // Quantity is used to set supplier availability, if 0 it's
          // unavailable. (Note that purchase availability is not updated
          // on the server).
          var available = 1;
          var orderAvailable = 0;
          if (quantity === 0) {
            available = 0;
          }
          else if (isNaN(quantity)) {
            // Quantity is allowed to be empty, which just means it's unknown
            // rather than unavailable.
            quantity = 0;
          }
          else {
            // Also use quantity as an indicator of order availability.
            orderAvailable = 1;
            if (size > 0.001) {
              // When provided, the import quantity is per box, so multiply by
              // pack size to get the new quantity in the given units.
              quantity *= size;
            }
          }
          var taxable = 0;
          // Just want to know if the item attracts gst.
          if (row[7] !== '' && row[7] !== '0' &&
              row[7].substring(0, 5) !== '$0.00') {
            taxable = 1;
          }
          var category = row.length >= 10 ? row[9] : '';
          importData.push({ product: row[0], supplier: importSupplier,
                            quantity: quantity, unit: unit, size: size,
                            price: price, wholesale: wholesale, retail: retail,
                            description: row[1], category: category,
                            orderAvailable: orderAvailable,
                            purchaseAvailable: 0, supplierAvailable: available,
                            taxable: taxable, grower: row[8] });
        }
      }
    });
    return headerFound;
  }

  function processSovereignFoodsData(data) {
    var headerFound = false;
    var headerCheck = 0;
    var emptyLineCount = 0;
    importSupplier = '';
    if ($('#stock-import-input').val() !== '') {
      importSupplier = $('#stock-import-input').val();
    }
    else {
      importSupplier = $('#stock-import-supplier').val();
    }

    $.each(data, function(i, item) {
      // Before checking the line format, need to check for any quotes, remove
      // commas between the quotes, and then remove the quotes themselves.
      item = item.replace(/\".*?\"/g, function(match) {
        match = match.replace(/,/g, '');
        return match.replace(/\"/g, '');
      });

      // Split on commas, but first clean up the row by removing initial and
      // final commas, and any newlines.
      let re = /^,?(.*),*[\r\n]*$/;
      let match = item.match(re);
      let row = [];
      if (match.length === 2) {
        row = match[1].split(',');
      }
      if (row.length >= 7) {
        if (!headerFound) {
          if (row[0].trim().toLowerCase() === 'name' &&
              row[1].trim().toLowerCase() === 'unit' &&
              row[2].trim().toLowerCase() === 'unitprice' &&
              row[3].trim().toLowerCase() === 'quantity' &&
              row[4].trim().toLowerCase() === 'total' &&
              row[6].trim().toLowerCase() === 'grower' &&
              row[7].trim().toLowerCase() === 'category') {
            headerFound = true;
            if (importSupplier === '') {
              return false;
            }
          }
          else if (headerCheck > 40) {
            // Stop after checking the first 40 rows of the file for a header.
            return false;
          }
          headerCheck++;
          return true;
        }
        // Skip lines with empty products, but if two consecutive empty lines
        // are found assume we've reached the end of the product list. (This
        // allows the billing lines at the end to be ignored).
        if (row[0] === '') {
          emptyLineCount++;
          if (emptyLineCount === 2) {
            return false;
          }
          return true;
        }
        else {
          // First reset the empty line count from above.
          emptyLineCount = 0;
          let unit = row[1].toLowerCase();
          if (unit === 'l' || unit.substring(0, 5) === 'litre') {
            unit = 'L';
          }
          else if (unit === '100ml') {
            unit = '100mL';
          }
          else if (unit.substring(0, 1) === 'g' ||
                   unit.substring(0, 4) === 'gram') {
            unit = 'g';
          }
          else if (unit.substring(0, 2) === 'kg' ||
                   unit.substring(0, 4) === 'kilo') {
            unit = 'kg';
          }
          else if (unit !== 'variable' && unit !== 'adjusted') {
            unit = 'each';
          }
          let price = row[2].trim();
          if (price[0] === '$') {
            price = price.substring(1);
          }
          price = parseFloat(price);
          let wholesale = price;
          if (stock.wholesalePercent) {
            wholesale = price * (100 + stock.wholesalePercent) / 100;
          }
          let retail = price;
          if (stock.retailPercent) {
            retail = price * (100 + stock.retailPercent) / 100;
          }
          let quantity = parseFloat(row[3]);
          // Quantity is used to set supplier availability, if 0 it's
          // unavailable. (Note that purchase availability is not updated
          // on the server).
          let available = 1;
          let orderAvailable = 0;
          if (quantity === 0) {
            available = 0;
          }
          else if (isNaN(quantity)) {
            // Quantity is allowed to be empty, which just means it's unknown
            // rather than unavailable.
            quantity = 0;
          }
          else {
            // Also use quantity as an indicator of order availability.
            orderAvailable = 1;
          }
          let taxable = 0;
          if (row[5] !== '' && row[5] !== '0' && row[5] !== 'no') {
            taxable = 1;
          }
          importData.push({ product: row[0], supplier: importSupplier,
                            quantity: quantity, unit: unit, size: 1,
                            price: price, wholesale: wholesale, retail: retail,
                            description: '', category: row[7],
                            orderAvailable: orderAvailable,
                            purchaseAvailable: 0, supplierAvailable: available,
                            taxable: taxable, grower: row[6] });
        }
      }
    });
    return headerFound;
  }

  function processEcoFarmsData(data) {
    var headerFound = false;
    var headerCheck = 0;
    var emptyLineCount = 0;
    importSupplier = '';
    if ($('#stock-import-input').val() !== '') {
      importSupplier = $('#stock-import-input').val();
    }
    else {
      importSupplier = $('#stock-import-supplier').val();
    }

    $.each(data, function(i, item) {
      // Before checking the line format, need to check for any quotes, remove
      // commas between the quotes, and then remove the quotes themselves.
      item = item.replace(/\".*?\"/g, function(match) {
        match = match.replace(/,/g, '');
        return match.replace(/\"/g, '');
      });

      // Split on commas, but first clean up the row by removing initial and
      // final commas, and any newlines.
      var re = /^,?(.*),*[\r\n]*$/;
      var match = item.match(re);
      var row = [];
      if (match.length === 2) {
        row = match[1].split(',');
      }
      if (row.length >= 9) {
        // First look for the header. Don't bother checking the Comments
        // header, as it can contain a newline which stuffs up the quote
        // remover above (since it doesn't check over multiple lines).
        // Once the header has been found, can process all rows after it.
        if (!headerFound) {
          if (row[0].trim().toLowerCase() === 'product' &&
              row[1].trim().toLowerCase() === 'size' &&
              row[3].trim().toLowerCase() === 'grower' &&
              row[8].trim().toLowerCase() === 'price') {
            headerFound = true;
            // Eco Farms import confirmed, so make sure supplier is set.
            if (importSupplier === '') {
              return false;
            }
          }
          else if (headerCheck > 40) {
            // Stop after checking the first 40 rows of the file for a header.
            return false;
          }
          headerCheck++;
          return true;
        }
        // Skip lines with empty products, but if two consecutive empty lines
        // are found assume we've reached the end of the product list. (This
        // allows the billing lines at the end to be ignored).
        if (row[0] === '') {
          emptyLineCount++;
          if (emptyLineCount === 2) {
            return false;
          }
          return true;
        }
        else if ((row[1] === '' && row[2] === '' && row[3] === '') ||
                 (row[0].trim().toLowerCase() === 'product' &&
                  row[1].trim().toLowerCase() === 'size' &&
                  row[3].trim().toLowerCase() === 'grower' &&
                  row[8].trim().toLowerCase() === 'price')) {
          // EcoFarms put sub-headings in the data which are ignored, which is
          // sometimes followed by another header which is also ignored.
          return true;
        }
        else {
          // First reset the empty line count from above.
          emptyLineCount = 0;
          // The unit is free form text so try and condense to a limited set.
          // substrings are used to avoid spaces.
          var unit = 'each';
          var matchUnit = row[1].trim().toLowerCase().match(/([^0-9\.]+)$/);
          if (matchUnit && matchUnit.length === 2) {
            unit = matchUnit[1];
          }
          if (unit === 'l' || unit.substring(0, 5) === 'litre') {
            unit = 'L';
          }
          else if (unit.substring(0, 1) === 'g' ||
                   unit.substring(0, 4) === 'gram') {
            unit = 'g';
          }
          else if (unit.substring(0, 2) === 'kg' ||
                   unit.substring(0, 4) === 'kilo') {
            unit = 'kg';
          }
          else if (unit !== 'variable' && unit !== 'adjusted') {
            unit = 'each';
          }
          // Pack size is usually just a number, but can be min-max. In this
          // case just use min.
          var size = 0;
          var matchSize = row[1].match(/^([0-9\.]+)/);
          if (matchSize && matchSize.length === 2) {
            size = parseFloat(matchSize[1]);
          }
          // The price given is per box, so divide by the pack size to get
          // a price per unit. Also first remove optional $ prefix.
          var price = row[8].trim();
          if (price[0] === '$') {
            price = price.substring(1);
          }
          price = parseFloat(price);
          if (size > 0.001) {
            price /= size;
          }
          var wholesale = price;
          if (stock.wholesalePercent) {
            wholesale = price * (100 + stock.wholesalePercent) / 100;
          }
          var retail = price;
          if (stock.retailPercent) {
            retail = price * (100 + stock.retailPercent) / 100;
          }
          importData.push({ product: row[0], supplier: importSupplier,
                            quantity: 0, unit: unit, size: size, price: price,
                            wholesale: wholesale, retail: retail,
                            description: '', category: '',
                            orderAvailable: 1, purchaseAvailable: 1,
                            supplierAvailable: 1, taxable: 0, grower: row[3] });
        }
      }
    });
    return headerFound;
  }

  function processExportData(data) {
    var headerFound = false;
    var oldFormat = false;
    $.each(data, function(i, item) {
      // Before checking the line format, need to check for any quotes, remove
      // commas between the quotes, and then remove the quotes themselves.
      item = item.replace(/\".*?\"/g, function(match) {
        match = match.replace(/,/g, '');
        return match.replace(/\"/g, '');
      });

      // Split on commas but first remove newlines.
      var re = /^(.*)[\r\n]*$/;
      var match = item.match(re);
      var row = [];
      if (match.length === 2) {
        row = match[1].split(',');
      }
      if (row.length >= 14) {
        // First look for the header.
        if (!headerFound) {
          // Support the old export format.
          if (row.length === 14 &&
              row[0].trim().toLowerCase() === 'name' &&
              row[1].trim().toLowerCase() === 'user' &&
              row[2].trim().toLowerCase() === 'quantity' &&
              row[3].trim().toLowerCase() === 'unit' &&
              row[4].trim().toLowerCase() === 'size' &&
              row[5].trim().toLowerCase() === 'price' &&
              row[6].trim().toLowerCase() === 'wholesale' &&
              row[7].trim().toLowerCase() === 'retail' &&
              row[8].trim().toLowerCase() === 'category' &&
              row[9].trim().toLowerCase() === 'order available' &&
              row[10].trim().toLowerCase() === 'purchase available' &&
              row[11].trim().toLowerCase() === 'supplier available' &&
              row[13].trim().toLowerCase() === 'grower') {
            oldFormat = true;
            headerFound = true;
          }
          else if (row[0].trim().toLowerCase() === 'name' &&
                   row[1].trim().toLowerCase() === 'user' &&
                   row[2].trim().toLowerCase() === 'description' &&
                   row[3].trim().toLowerCase() === 'quantity' &&
                   row[4].trim().toLowerCase() === 'price' &&
                   row[5].trim().toLowerCase() === 'wholesale' &&
                   row[6].trim().toLowerCase() === 'retail' &&
                   row[7].trim().toLowerCase() === 'size' &&
                   row[8].trim().toLowerCase() === 'unit' &&
                   row[10].trim().toLowerCase() === 'grower' &&
                   row[11].trim().toLowerCase() === 'category' &&
                   row[12].trim().toLowerCase() === 'available' &&
                   row[13].trim().toLowerCase() === 'purchaseavailable' &&
                   row[14].trim().toLowerCase() === 'supplieravailable') {
            headerFound = true;
          }
          return true;
        }
        var quantity = oldFormat ? row[2] : row[3];
        quantity = parseFloat(quantity);
        if (isNaN(quantity)) {
          // Set quantity to zero if not provided.
          quantity = 0;
        }
        var size = oldFormat ? parseFloat(row[4]) : parseFloat(row[7]);
        var price = oldFormat ? parseFloat(row[5]) : parseFloat(row[4]);
        // Calculate wholesale and retail markup from base price, only when
        // no existing value is given.
        var wholesale = oldFormat ? row[6] : row[5];
        if (stock.wholesalePercent && wholesale === '') {
          wholesale = price * (100 + stock.wholesalePercent) / 100;
        }
        var retail = oldFormat ? row[7] : row[6];
        if (stock.retailPercent && retail === '') {
          retail = price * (100 + stock.retailPercent) / 100;
        }
        var orderAvailable = oldFormat ? parseInt(row[9], 10) :
          parseInt(row[12], 10);
        var purchaseAvailable = oldFormat ? parseInt(row[10], 10) :
          parseInt(row[13], 10);
        var supplierAvailable = oldFormat ? parseInt(row[11], 10) :
          parseInt(row[14], 10);
        var taxable = oldFormat ? parseInt(row[12], 10) : parseInt(row[9], 10);
        var unit = oldFormat ? row[3] : row[8];
        var description = oldFormat ? '' : row[2];
        var category = oldFormat ? row[8] : row[11];
        var grower = oldFormat ? row[13] : row[10];
        importData.push({ product: row[0], supplier: row[1], quantity: quantity,
                          unit: unit, size: size, price: price,
                          wholesale: wholesale, retail: retail,
                          description: description, category: category,
                          orderAvailable: orderAvailable,
                          purchaseAvailable: purchaseAvailable,
                          supplierAvailable: supplierAvailable,
                          taxable: taxable, grower: grower });
      }
    });
    return headerFound;
  }

  function changeGroup() {
    dobrado.log('Changing group.', 'info');
    $.post('/php/request.php',
           { request: 'stock', action: 'changeGroup',
             group: $('#stock-group-select').val(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock changeGroup')) {
          return;
        }
        location.reload();
      });
  }

  function changeProfile() {
    var value = $('#stock-profiles').val();
    var profile = '';
    if (dobrado.localStorage && localStorage.stockProfile) {
      profile = localStorage.stockProfile;
    }
    if (value === 'edit') {
      $('#stock-profiles').val(profile).selectmenu('refresh');
      if (dobrado.mobile) {
        $('#stock-profile-dialog').toggle();
      }
      else {
        $('#stock-profile-dialog').dialog('open');
      }
    }
    else {
      dobrado.log('Changing profile.', 'info');
      $.post('/php/request.php',
             { request: 'stock', action: 'changeProfile', value: value,
               url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock changeProfile')) {
          return;
        }
        var profile = JSON.parse(response);
        if (dobrado.localStorage) {
          if (profile.columns) {
            localStorage.stockColumns = JSON.stringify(profile.columns);
          }
          if (profile.sort) {
            localStorage.stockSortColumns = JSON.stringify(profile.sort);
          }
          localStorage.stockProfile = value;
        }
        setTimeout(function() { showAll(true); }, 1000);
      });
    }
  }

  dobrado.stock.addUser = function(user) {
    $('#stock-user-input').val(user);
    stock.users.push(user);
    updateNames();
    // The stock page can be used to create supplier accounts for other
    // buying groups, so check if this new supplier has the right group.
    $.post('/php/request.php',
           { request: 'stock', action: 'updateGroup', supplier: user,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'stock updateGroup')) {
          return;
        }
        var supplier = JSON.parse(response);
        // Also use this callback to return the import supplier list.
        $('#stock-import-supplier').html(supplier.options).
          selectmenu('refresh');
      });
  };

  dobrado.stock.select = function(filename) {
    if (currentImageEditor) currentImageEditor.save(filename);
    else $('#stock-image-input').val(filename);
  };

  dobrado.stock.settingsCallback = function(settings) {
    if (dobrado.localStorage && settings.displayWideGridButton === 'hidden') {
      localStorage.stockWideGrid = 'true';
    }
  };

}());
