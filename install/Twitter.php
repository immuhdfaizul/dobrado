<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Twitter extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return !$this->AlreadyOnPage('twitter', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    if (!isset($_SESSION['twitter-oauth-token'])) {
      return '<p>OAuth Token not set.</p><p>This page is displayed after ' .
        'authorizing your Twitter account, but the required information ' .
        'was not found.</p>';
    }

    $mysqli = connect_db();
    $username = '';
    $microsub = 0;
    $oauth_token = $mysqli->escape_string($_SESSION['twitter-oauth-token']);
    $query = 'SELECT user, microsub FROM twitter_auth WHERE ' .
      'oauth_token = "' . $oauth_token . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_auth = $mysqli_result->fetch_assoc()) {
        $username = $twitter_auth['user'];
        $microsub = (int)$twitter_auth['microsub'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Content 1: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($username === '') {
      return '<p>A username was not found for the OAuth Token provided. ' .
        'Please try authorizing your Twitter account again.</p>';
    }

    if ($this->owner === 'admin') chdir('php');
    else chdir('../php');
    $connection = $this->Connection($_SESSION['twitter-oauth-token']);
    if (!isset($connection)) {
      chdir('..');
      return '<p>Twitter config is not set on this server.</p>';
    }

    $content = '<p>Hi ' . $username . '</p>';
    $data = $connection->get('account/verify_credentials');
    if ($connection->getLastHttpCode() !== 200) {
      chdir('..');
      return $content . '<p>Sorry could not connect to Twitter to verify ' .
        'your details. Please reload the page to try again.</p>';
    }

    if (!isset($data->screen_name)) {
      chdir('..');
      return $content . '<p>Sorry could not find your Twitter account. ' .
        'Please reload the page to try again.</p>';
    }

    $mysqli = connect_db();
    $twitter_name = $mysqli->escape_string($data->screen_name);
    $query = 'UPDATE twitter_auth SET twitter_name = "' . $twitter_name . '",' .
      'verified = 1 WHERE user = "' . $username . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->Content 2: ' . $mysqli->error);
    }
    $id = 0;
    $page = '';
    // Module owner is 'admin' if username is a domain. (Microsub and Indieauth
    // users, who are configured to use indieauth-page.)
    $owner = $username;
    $indieauth_page = $this->Substitute('indieauth-page');
    if (strpos($username, '.') !== false) $owner = 'admin';
    $query = 'SELECT page, box_id FROM modules WHERE ' .
      'user = "' . $owner . '" AND label = "reader" AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($modules = $mysqli_result->fetch_assoc()) {
        $id = (int)$modules['box_id'];
        $page = $modules['page'];
        if ($owner !== 'admin') break;
        if ($indieauth_page === '') break;
        if ($indieauth_page === $modules['page']) break;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Content 3: ' . $mysqli->error);
    }
    $mysqli->close();

    $user = new User($username);
    $user->SetPermission($page, $owner);
    $reader = new Module($user, $owner, 'reader');
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    if ($reader->IsInstalled()) {
      $url = $scheme . $_SERVER['SERVER_NAME'] . '/php/twitter.php?username=' .
         $username . '&oauth_token=' . $oauth_token;
      $reader->Factory('AddFeed', [$id, $url, true]);
      $content .= '<p>Your Reader is now subscribed to the home ' .
        'timeline for the Twitter account <b><a href="https://twitter.com/' .
        $data->screen_name . '">@' . $data->screen_name . '</a></b>. You ' .
        'can unsubscribe in your Reader at any time to remove permission.</p>';
    }
    else {
      $content .= 'There is a problem on this server, the Reader module ' .
        'must be installed to follow a Twitter account.';
    }
    chdir('..');
    if ($microsub === 1) {
      $content .= '<p><a href="http://' . $username . '">' .
        'Return to your website</a></p>';
    }
    else {
      $content .= '<p><a href="' . $scheme . $_SERVER['SERVER_NAME'] .
        $this->Url('', $page, $owner) . '">Return to your reader</a></p>';
    }
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {
    if (!$this->Run(date('H:00:00'))) return;

    set_time_limit(0);
    $mysqli = connect_db();
    $query = 'SELECT user, oauth_token FROM twitter_auth WHERE verified = 1';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($twitter_auth = $mysqli_result->fetch_assoc()) {
        $connection = $this->Connection($twitter_auth['oauth_token']);
        if (!isset($connection)) continue;

        $this->StoreTimeline($twitter_auth['user'], $connection);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Cron 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM twitter_items WHERE timestamp < ' .
      strtotime('-1 day');
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->Cron 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      if ($fn === 'AccessToken' && count($p) === 2) {
        $oauth_token = $p[0];
        $oauth_verifier = $p[1];
        $this->AccessToken($oauth_token, $oauth_verifier);
      }
      return;
    }

    if ($fn === 'AuthorizeUrl') return $this->AuthorizeUrl($p);
    if ($fn === 'CheckAuth') return $this->CheckAuth();
    if ($fn === 'Feed') return $this->Feed();
    if ($fn === 'RemoveAuth') return $this->RemoveAuth($p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS twitter_config (' .
      'consumer_key VARCHAR(100) NOT NULL,' .
      'consumer_secret VARCHAR(100) NOT NULL,' .
      'callback VARCHAR(200) NOT NULL' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS twitter_auth (' .
        'user VARCHAR(50) NOT NULL,' .
        'microsub TINYINT(1) NOT NULL,' .
        'verified TINYINT(1) NOT NULL,' .
        'twitter_name VARCHAR(100),' .
        'oauth_token VARCHAR(100) NOT NULL,' .
        'oauth_token_secret VARCHAR(100),' .
        'PRIMARY KEY(user)' .
        ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->Install 2: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS twitter_items (' .
      'id BIGINT UNSIGNED NOT NULL,' .
      'content MEDIUMTEXT,' .
      'user VARCHAR(50) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->Install 3: ' . $mysqli->error);
    }

    $mysqli->close();
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {

  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AccessToken($us_oauth_token, $us_oauth_verifier) {
    $mysqli = connect_db();
    $user = '';
    $oauth_token = $mysqli->escape_string($us_oauth_token);
    // Check that authorization was requested for this token.
    $query = 'SELECT user FROM twitter_auth WHERE ' .
      'oauth_token = "' . $oauth_token . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_auth = $mysqli_result->fetch_assoc()) {
        $user = $twitter_auth['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('AccessToken 1: ' . $mysqli->error);
    }
    if ($user === '') {
      $mysqli->close();
      return;
    }

    $connection = $this->Connection($us_oauth_token);
    if (!isset($connection)) {
      $mysqli->close();
      return;
    }

    $data = $connection->oauth('oauth/access_token',
                               ['oauth_verifier' => $us_oauth_verifier]);
    if (isset($data['oauth_token']) && isset($data['oauth_token_secret'])) {
      $oauth_token = $mysqli->escape_string($data['oauth_token']);
      $oauth_token_secret = $mysqli->escape_string($data['oauth_token_secret']);
      $query = 'UPDATE twitter_auth SET ' .
        'oauth_token = "' . $oauth_token . '", ' .
        'oauth_token_secret = "' . $oauth_token_secret . '" ' .
        'WHERE user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Twitter->AccessToken 2: ' . $mysqli->error);
      }
      // This is required by $this->Content() which will be called via a
      // redirect from php/twitter.php.
      session_start();
      $_SESSION['twitter-oauth-token'] = $data['oauth_token'];
    }
    $mysqli->close();
  }

  private function AuthorizeUrl($microsub) {
    $connection = $this->Connection();
    if (!isset($connection)) return ['error' => 'Twitter config not set'];

    $data = $connection->oauth('oauth/request_token',
                               ['oauth_callback' => $this->CallbackUrl()]);
    if (!isset($data['oauth_token']) || !isset($data['oauth_token_secret'])) {
      return ['error' => 'Could not contact Twitter'];
    }

    if (!isset($data['oauth_callback_confirmed']) ||
        $data['oauth_callback_confirmed'] !== 'true') {
      return ['error' => 'Twitter callback does not match'];
    }

    $mysqli = connect_db();
    $oauth_token = $mysqli->escape_string($data['oauth_token']);
    $oauth_token_secret = $mysqli->escape_string($data['oauth_token_secret']);
    $query = 'INSERT INTO twitter_auth VALUES ("' . $this->user->name . '", ' .
      (int)$microsub . ', 0, "", "' . $oauth_token . '", ' .
      '"' . $oauth_token_secret . '") ON DUPLICATE KEY UPDATE ' .
      'microsub = ' . (int)$microsub . ', verified = 0, twitter_name = "", ' .
      'oauth_token = "' . $oauth_token . '", ' .
      'oauth_token_secret = "' . $oauth_token_secret . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->AuthorizeUrl: ' . $mysqli->error);
    }
    $mysqli->close();

    $url = $connection->url('oauth/authorize',
                            ['oauth_token' => $data['oauth_token']]);
    return ['url' => $url];
  }

  private function CallbackUrl() {
    $mysqli = connect_db();
    $callback = '';
    $query = 'SELECT callback FROM twitter_config';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_config = $mysqli_result->fetch_assoc()) {
        $callback = $twitter_config['callback'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->CallbackUrl: ' . $mysqli->error);
    }
    $mysqli->close();
    return $callback;
  }

  private function CheckAuth() {
    $mysqli = connect_db();
    $twitter_auth = NULL;
    $query = 'SELECT verified, twitter_name FROM twitter_auth WHERE ' .
      'user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      $twitter_auth = $mysqli_result->fetch_assoc();
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->CheckAuth: ' . $mysqli->error);
    }
    $mysqli->close();
    return $twitter_auth;
  }

  private function Connection($us_oauth_token = NULL) {
    $mysqli = connect_db();
    $consumer_key = '';
    $consumer_secret = '';
    $callback = '';
    $query = 'SELECT consumer_key, consumer_secret, callback FROM ' .
      'twitter_config';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_config = $mysqli_result->fetch_assoc()) {
        $consumer_key = $twitter_config['consumer_key'];
        $consumer_secret = $twitter_config['consumer_secret'];
        $callback = $twitter_config['callback'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Connection 1: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($consumer_key === '' || $consumer_secret === '' || $callback === '') {
      return NULL;
    }

    include_once 'library/TwitterOAuth/autoload.php';
    if (!isset($us_oauth_token)) {
      return new Abraham\TwitterOAuth\TwitterOAuth($consumer_key,
                                                   $consumer_secret);
    }

    $mysqli = connect_db();
    $oauth_token = $mysqli->escape_string($us_oauth_token);
    $oauth_token_secret = '';
    $query = 'SELECT oauth_token_secret FROM twitter_auth WHERE ' .
      'oauth_token = "' . $oauth_token . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_auth = $mysqli_result->fetch_assoc()) {
        $oauth_token_secret = $twitter_auth['oauth_token_secret'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Connection 2: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($oauth_token_secret === '') return NULL;

    return new Abraham\TwitterOAuth\TwitterOAuth($consumer_key,
                                                 $consumer_secret,
                                                 $us_oauth_token,
                                                 $oauth_token_secret);
  }

  private function Feed() {
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_GET['username']);
    $oauth_token = $mysqli->escape_string($_GET['oauth_token']);
    $twitter_name = '';
    $verified = 0;
    $query = 'SELECT twitter_name, verified FROM twitter_auth WHERE ' .
      'user = "' . $username . '" AND oauth_token = "' . $oauth_token . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_auth = $mysqli_result->fetch_assoc()) {
        $twitter_name = $twitter_auth['twitter_name'];
        $verified = (int)$twitter_auth['verified'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Feed 1: ' . $mysqli->error);
    }
    if ($verified === 0) {
      $mysqli->close();
      return;
    }

    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    $url = $scheme . $_SERVER['SERVER_NAME'] . '/php/twitter.php?username=' .
      $username . '&oauth_token=' . $oauth_token;
    $content = '<!DOCTYPE html>' . "\n" .
      '<html><head><meta charset="utf-8"></head>' . "\n" .
      '<body class="h-feed">' . "\n" .
        '<h1 class="p-name"><a href="' . $url . '" class="u-url">' .
          'Home timeline for Twitter account ' . $twitter_name . '</a></h1>' .
          "\n";
    $modified_since = $_SERVER['HTTP_IF_MODIFIED_SINCE'] ?? 0;
    $timestamp = 0;
    $oldest = 0;
    $items_count = 0;
    $query = 'SELECT content, timestamp FROM twitter_items WHERE ' .
      'user = "' . $username . '" AND timestamp > ' . strtotime('-1 hour') .
      ' ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_items = $mysqli_result->fetch_assoc()) {
        $timestamp = (int)$twitter_items['timestamp'];
        header('Cache-Control: private, must-revalidate');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $timestamp));
        if ($timestamp <= strtotime($modified_since)) {
          header('HTTP/1.1 304 Not Modified');
          $mysqli_result->close();
          $mysqli->close();
          return;
        }

        $content .= $twitter_items['content'] . "\n";
        while ($twitter_items = $mysqli_result->fetch_assoc()) {
          $content .= $twitter_items['content'] . "\n";
          $oldest = (int)$twitter_items['timestamp'];
          $items_count++;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Twitter->Feed 2: ' . $mysqli->error);
    }
    if ($items_count < 20) {
      $remaining = 20 - $items_count;
      $oldest_query = $oldest === 0 ? '' : ' AND timestamp < ' . $oldest;
      $query = 'SELECT content, timestamp FROM twitter_items WHERE ' .
        'user = "' . $username . '"' . $oldest_query .
        ' ORDER BY timestamp DESC LIMIT ' . $remaining;
      if ($mysqli_result = $mysqli->query($query)) {
        if ($twitter_items = $mysqli_result->fetch_assoc()) {
          if ($oldest === 0) {
            $timestamp = (int)$twitter_items['timestamp'];
            header('Cache-Control: private, must-revalidate');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $timestamp));
            if ($timestamp <= strtotime($modified_since)) {
              header('HTTP/1.1 304 Not Modified');
              $mysqli_result->close();
              $mysqli->close();
              return;
            }
          }

          $content .= $twitter_items['content'] . "\n";
          while ($twitter_items = $mysqli_result->fetch_assoc()) {
            $content .= $twitter_items['content'] . "\n";
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Twitter->Feed 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $content . '</body></html>';
  }

  private function FormatItem($item) {
    $timestamp = strtotime($item->created_at);
    $formatted_date = date('j F Y, g:ia', $timestamp);
    $atom_date = date(DATE_ATOM, $timestamp);
    $url = 'https://twitter.com/' . $item->user->screen_name;
    $media_content = '';
    $full_text = $item->full_text;
    if (isset($item->entities->urls)) {
      foreach ($item->entities->urls as $replace_url) {
        $full_url = '<a href="' . $replace_url->expanded_url . '">' .
          $replace_url->display_url . '</a>';
        $full_text = str_replace($replace_url->url, $full_url, $full_text);
      }
    }
    if (isset($item->entities->user_mentions)) {
      foreach ($item->entities->user_mentions as $mention) {
        $screen_name = $mention->screen_name;
        $full_url = '<a href="https://twitter.com/' . $screen_name . '">@' .
          $screen_name . '</a>';
        $full_text = str_replace('@' . $screen_name, $full_url, $full_text);
      }
    }
    if (isset($item->extended_entities->media)) {
      foreach ($item->extended_entities->media as $media) {
        if ($media->type === 'photo') {
          $media_content .= '<img class="u-photo" ' .
            'src="' . $media->media_url_https . '">' . "\n";
        }
        else if ($media->type === 'video' &&
                 isset($media->video_info->variants)) {
          // Use the smallest bit rate mp4 available.
          $bit_rate = 0;
          $src = '';
          foreach ($media->video_info->variants as $variant) {
            if ($variant->content_type === 'video/mp4') {
              if ($bit_rate === 0) {
                $bit_rate = $variant->bitrate;
                $src = $variant->url;
              }
              else if ($variant->bitrate < $bit_rate) {
                $src = $variant->url;
              }
            }
          }
          if ($src !== '') {
            $media_content .= '<video class="u-video" src="' . $src  . '"' .
              '</video>' . "\n";
          }
        }
        // Remove this url from the text.
        $full_text = str_replace($media->url, '', $full_text);
      }
    }
    $content =
      '<time class="dt-published" datetime="' . $atom_date . '">' .
        $formatted_date . '</time>' . "\n" .
      '<span class="p-author h-card">' . "\n" .
        '<a class="p-name u-url" href="' . $url . '">' .
          $item->user->name . '</a>' . "\n" .
        '<span class="p-nickname">' . $item->user->screen_name . '</span>' .
          "\n" .
        '<img class="u-photo" ' .
          'src="' . $item->user->profile_image_url_https . '">' . "\n" .
      '</span>' . "\n" .
      '<a class="u-url" href="' . $url . '/status/' . $item->id . '">' .
        $url . '/status/' . $item->id . '</a>' . "\n" .
      '<div class="e-content">' . "\n" .
        $full_text . "\n" .
      '</div>' . "\n" .
      $media_content;
    if (isset($item->in_reply_to_status_id)) {
      $content .= "\n" .
        '<a class="u-in-reply-to" ' .
          'href="https://twitter.com/' . $item->in_reply_to_screen_name .
            '/status/' . $item->in_reply_to_status_id . '">In reply to</a>' .
        "\n";
    }
    if (isset($item->retweeted_status)) {
      $content .= "\n" .
        '<article class="u-repost-of h-cite">' . "\n" .
          $this->FormatItem($item->retweeted_status) . "\n" .
        '</article>' . "\n";
    }
    if (isset($item->quoted_status)) {
      $content .= "\n" .
        '<article class="u-quotation-of h-cite">' . "\n" .
          $this->FormatItem($item->quoted_status) . "\n" .
        '</article>' . "\n";
    }
    return $content;
  }

  private function RemoveAuth($user) {
    $mysqli = connect_db();
    $query = 'DELETE FROM twitter_auth WHERE user = "' . $user . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Twitter->RemoveAuth: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function StoreTimeline($username, $connection) {
    $mysqli = connect_db();
    $id = 0;
    $query = 'SELECT id FROM twitter_items WHERE user = "' . $username . '"' .
      'ORDER BY id DESC LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($twitter_items = $mysqli_result->fetch_assoc()) {
        $id = (int)$twitter_items['id'];
      }
    }
    else {
      $this->Log('Twitter->StoreTimeline 1: ' . $mysqli->error);
    }
    $parameters = ['tweet_mode' => 'extended', 'count' => 200];
    if ($id !== 0) $parameters['since_id'] = $id;
    $timeline = $connection->get('statuses/home_timeline', $parameters);
    foreach ($timeline as $item) {
      $id = $mysqli->escape_string($item->id);
      $timestamp = strtotime($item->created_at);
      $us_content = '<article class="h-entry">' . "\n" .
          $this->FormatItem($item) . "\n" .
        '</article>';
      $content = $mysqli->escape_string($us_content);
      $query = 'INSERT INTO twitter_items VALUES (' .
        $id . ', "' . $content . '", "' . $username . '", ' . $timestamp . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Twitter->StoreTimeline 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }
}
