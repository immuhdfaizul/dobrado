<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Autoupdate extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canEditSite) {
      return ['error' => 'You don\'t have permission ' .
                         'to use the AutoUpdate module.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'list') return $this->ListUpdates();
    if ($us_action === 'install') return $this->InstallModule();
    if ($us_action === 'finishInstall') return $this->FinishInstall();
    if ($this->Substitute('autoupdate-build-tab') === 'true') {
      if ($us_action === 'build') return $this->Build();
      if ($us_action === 'remove') return $this->RemoveUpdate();
    }
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    // Need admin privileges to add the autoupdate module.
    if (!$this->user->canEditSite) return false;
    // Can only have one autoupdate module on a page.
    return !$this->AlreadyOnPage('autoupdate', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $build_tab = $this->Substitute('autoupdate-build-tab') === 'true';
    $remove_update = '';
    $build_tab_link = '';
    $build_tab_content = '';
    if ($build_tab) {
      $remove_update = '<button id="autoupdate-remove">Remove update</button>';
      $build_tab_link = '<li><a href="#autoupdate-build-tab">Build</a></li>';
      $build_tab_content = '<div id="autoupdate-build-tab">' .
        '<form id="autoupdate-build-form" autocomplete="off">' .
          '<div class="form-spacing">' .
            '<input id="autoupdate-build-module" ' .
              'name="autoupdate-build-radio" type="radio" checked="checked">' .
            '<label for="autoupdate-build-module">Update a module</label>' .
            '<input id="autoupdate-build-core" name="autoupdate-build-radio" ' .
              'type="radio">' .
            '<label for="autoupdate-build-core">Update core files</label>' .
          '</div>' .
          '<div id="autoupdate-build-module-wrapper">' .
            '<div class="autoupdate-build-info">' .
              'Enter details for the module to build. The label must match ' .
              'the required filenames.' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="autoupdate-build-display">Display in menu:</label>' .
              '<input id="autoupdate-build-display" type="checkbox">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="autoupdate-build-label">Label:</label>' .
              '<input id="autoupdate-build-label" type="text" maxlength="50">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="autoupdate-build-css">CSS File:</label>' .
              '<input id="autoupdate-build-css" type="text" maxlength="50">' .
            '</div>' .
          '</div>' .
          '<div id="autoupdate-build-core-wrapper" class="hidden">' .
            '<div class="autoupdate-build-info">' .
              'Enter the files you want to include in this update, separated ' .
              'by spaces. If a directory is listed, all files in that ' .
              'directory will be included.' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="autoupdate-build-files">Core Files:</label>' .
              '<textarea id="autoupdate-build-files"></textarea>' .
            '</div>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="autoupdate-build-title">Title:</label>' .
            '<input id="autoupdate-build-title" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="autoupdate-build-description">Description:</label>' .
            '<textarea id="autoupdate-build-description"></textarea>' .
          '</div>' .
          '<button id="autoupdate-build-submit">submit</button>' .
          '<span id="autoupdate-build-result"></span>' .
        '</form></div>';
    }
    return '<div id="autoupdate-tabs">' .
        '<ul>' .
          $build_tab_link .
          '<li><a href="#autoupdate-install-tab">Install</a></li>' .
          '<li><a href="#autoupdate-log-tab">Log</a></li>' .
        '</ul>' .
        $build_tab_content .
        '<div id="autoupdate-install-tab">' . $this->AllModules() . '</div>' .
        '<div id="autoupdate-log-tab">' . $remove_update . '</div>' .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'SaveItem' && $count === 4) {
        $us_title = $p[0];
        $us_content = $p[1];
        $us_category_list = $p[2];
        $timestamp = $p[3];
        return $this->SaveItem($us_title, $us_content,
                               $us_category_list, $timestamp);
      }
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.autoupdate.js', false);

    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS autoupdate (' .
      'label VARCHAR(50) NOT NULL,' .
      'version INT UNSIGNED NOT NULL,' .
      'title VARCHAR(50),' .
      'description TEXT,' .
      'display TINYINT(1),' .
      'installed TINYINT(1),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(label, version)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS autoupdate_post (' .
      'label VARCHAR(50) NOT NULL,' .
      'version INT UNSIGNED NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'PRIMARY KEY(label, version, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->Install 2: ' . $mysqli->error);
    }

    // Initial entries are added for core updates and currently installed
    // modules, to record the timestamp when the user started tracking updates
    // via this module. It is expected that the user has the lastest version of
    // Dobrado installed at this point, so the timestamp recorded here will be
    // used to decide what the next update to install should be. Once updates
    // are no longer 'version 0' the version to install should be consecutive.
    $timestamp = time();
    $query = 'INSERT INTO autoupdate VALUES ("core", 0, "Core updates", ' .
      '"Tracking started.", 0, 1, ' . $timestamp . '), ' .
      '("base", 0, "Base", "", 0, 1, ' . $timestamp . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->Install 3: ' . $mysqli->error);
    }
    $query = 'SELECT label, version, title, description, display FROM ' .
      'installed_modules';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($installed_modules = $mysqli_result->fetch_assoc()) {
        $label = $mysqli->escape_string($installed_modules['label']);
        $version = (int)$installed_modules['version'];
        $title = $mysqli->escape_string($installed_modules['title']);
        $description =
          $mysqli->escape_string($installed_modules['description']);
        $display = (int)$installed_modules['display'];
        $query = 'INSERT INTO autoupdate VALUES ("' . $label . '", ' .
          $version . ', "' . $title . '", "' . $description . '", ' .
          $display . ', 1, ' . $timestamp . ')';
        if (!$mysqli->query($query)) {
          $this->Log('Autoupdate->Install 4: ' . $mysqli->error);
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->Install 5: ' . $mysqli->error);
    }
    // Add notifications for the owner installing this module.
    $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
      '"autoupdate-notifications", "' . $this->owner . '", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->Install 6: ' . $mysqli->error);
    }
    $mysqli->close();

    // This is a comma separated list of modules displayed by AllModules.
    // Note that modules that are already installed don't need to be listed.
    $modules = 'analytics,browser,commenteditor,comment,contact,detail,' .
      'graph,grid,mapper,pager,post,reader,slider,viewanalytics,writer';
    $template = ['"autoupdate-all","","https://dobrado.net/all"',
                 '"autoupdate-new","","https://dobrado.net/updates"',
                 '"autoupdate-url","","https://dobrado.net"',
                 '"autoupdate-modules","","' . $modules . '"'];
    $this->AddTemplate($template);
    $description = ['autoupdate-feed' => 'This should be the page name to ' .
                      'publish a feed to on the build server.',
                    'autoupdate-testing' => 'This should be the page name to ' .
                      'publish a testing feed to on the build server. Sites ' .
                      'that are happy to test automatic updates can ' .
                      'subscribe to this feed instead of autoupdate-feed.',
                    'autoupdate-schedule' => 'When a build server produces a ' .
                      'testing feed, this should be set to the period before ' .
                      'publishing to the updates feed to allow for testing.',
                    'autoupdate-all' => 'The feed to subscribe to list all ' .
                      'Dobrado modules. The Reader module will call the ' .
                      'AutoUpdate module to save update posts, and should ' .
                      'switch to the \'autoupdate-new\' feed once these have ' .
                      'been saved.',
                    'autoupdate-new' => 'The feed to subscribe to for ' .
                      'Dobrado updates. The Reader module will call the ' .
                      'AutoUpdate module to save update posts.',
                    'autoupdate-url' => 'The url used to fetch Dobrado ' .
                      'updates. The updates are expected to be found under ' .
                      'the /public directory in a standard format.',
                    'autoupdate-modules' => 'The list of modules made ' .
                      'available to install by the Autoupdate module.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#autoupdate-build-form label","width","9em"',
                   '"","#autoupdate-build-submit","margin-left","9.3em"',
                   '"","label[for=autoupdate-build-module]","float","none"',
                   '"","label[for=autoupdate-build-core]","float","none"',
                   '"",".autoupdate-build-info","font-size","0.8em"',
                   '"","#autoupdate-install-tab .module-info","border",' .
                     '"1px solid #aaaaaa"',
                   '"","#autoupdate-install-tab .module-info",' .
                     '"margin-bottom","20px"',
                   '"","#autoupdate-install-tab .module-info",' .
                     '"border-radius","2px"',
                   '"","#autoupdate-install-tab .module-info","padding","5px"',
                   '"","#autoupdate-install-tab .module-detail","float",' .
                     '"right"',
                   '"","#autoupdate-install-tab .module-detail","font-size",' .
                     '"0.8em"',
                   '"","#autoupdate-install-tab .module-version",' .
                     '"font-weight","bold"',
                   '"","#autoupdate-install-tab .module-title",' .
                     '"margin-top","0"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // Note that the Autoupdate module can't call Update automatically... this
    // is because the old version has already been included when install_module
    // is called by SaveItem below, so autoload doesn't get a chance to include
    // the file that has just been downloaded. To get around this create two
    // updates: the first adds the changes required to this function, the
    // second update can remove them and will trigger the first version. Make
    // sure the two updates are published at least an hour apart!
  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript, ie:
    $this->AppendScript($path, 'dobrado.autoupdate.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AllModules() {
    if ($this->Substitute('autoupdate-new') === '') {
      return '<p>The Autoupdate module relies on a url being set in the ' .
        '<b>autoupdate-new</b> template and subscribed to in your Reader.</p>' .
        '<p>You can also manually update and install modules by copying them ' .
        'to your install directory and installing them via the tools menu.</p>';
    }

    // All updates are saved by the Autoupdate module, but don't want to
    // display all modules on a default install.
    $allowed_modules = explode(',', $this->Substitute('autoupdate-modules'));
    $content = '';
    $listed = [];

    $mysqli = connect_db();
    // Add the currently installed modules to the list of allowed modules.
    $query = 'SELECT label FROM installed_modules';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($installed_modules = $mysqli_result->fetch_assoc()) {
        $allowed_modules[] = $installed_modules['label'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('AllModules 1: ' . $mysqli->error);
    }
    $query = 'SELECT label, version, title, description, installed FROM ' .
      'autoupdate WHERE label != "core" ORDER BY label, version DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($autoupdate = $mysqli_result->fetch_assoc()) {
        $label = $autoupdate['label'];
        if (in_array($label, $listed)) continue;

        // The above query lists all versions but only want to display the
        // latest update.
        $listed[] = $label;
        $version = (int)$autoupdate['version'];
        $install_info = '';
        if ($autoupdate['installed'] === '1') {
          if ($version === 0) {
            $install_info = 'This module is installed, updates will be ' .
              'downloaded and installed automatically.';
          }
          else {
            $install_info = 'This update has been installed.';
          }
        }
        else if (in_array($label, $allowed_modules)) {
          $install_info = 'This module is not installed. ' .
            '<button class="module-install">install</button>';
        }
        else {
          continue;
        }

        $title = $autoupdate['title'] === '' ? $label : $autoupdate['title'];
        $content .= '<div class="module-info">' .
            '<div class="module-detail">' .
              '<span class="module-label-description">label: </span>' .
              '<span class="module-label-name">' . $label . '</span>' .
              '<span class="module-version-description">, version: </span>' .
              '<span class="module-version">' . $autoupdate['version'] .
              '</span>' .
            '</div>' .
            '<h2 class="module-title">' . $title . '</h2>' .
            '<span class="module-install-info">' . $install_info . '</span>' .
            '<div class="module-description">' . $autoupdate['description'] .
            '</div>' .
          '</div>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->AllModules 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function Build() {
    // This script is run from the php directory, so change to the parent
    // directory so that the file paths used will be the same as required
    // when installing the tar file.
    chdir('..');

    // Allow building a default module package from the modules directory.
    // The list is also used by BuildCore to package the full version.
    $default_modules = ['account', 'base', 'control', 'extended', 'login',
                        'more', 'notification', 'organiser', 'simple'];
    if ($_POST['module'] !== '') {
      $us_module = strtolower($_POST['module']);
      return $this->BuildModule($us_module, $default_modules);
    }
    if ($_POST['core'] !== '') {
      return $this->BuildCore($default_modules);
    }
    return ['error' => 'Module or core files not listed.'];
  }

  private function BuildModule($us_module, $default_modules) {
    $default = in_array($us_module, $default_modules);
    $php_file = $default ? 'php/modules/' : 'install/';
    $php_file .= ucfirst($us_module) . '.php';
    if (!file_exists($php_file)) return ['error' => 'Module not found.'];

    $tar_files = $php_file;
    // CSS file is optional.
    if ($_POST['css'] !== '') {
      if (!file_exists($_POST['css'])) {
        return ['error' => 'CSS file not found.'];
      }
      $tar_files .= ' ' . $_POST['css'];
    }
    // Modules don't need to have javascript files, but if they do there
    // must be source and minified versions.
    $minified_js = $default ? 'js/dobrado.' . $us_module . '.js' :
      'install/dobrado.' . $us_module . '.js';
    $source_js = $default ? 'js/source/dobrado.' . $us_module . '.js' :
      'install/dobrado.' . $us_module . '.source.js';
    $minified_exists = file_exists($minified_js);
    $source_exists = file_exists($source_js);
    if ($minified_exists && $source_exists) {
      $tar_files .= ' ' . $minified_js . ' ' . $source_js;
    }
    else if ($minified_exists) {
      return ['error' => 'Source javascript file not found.'];
    }
    else if ($source_exists) {
      return ['error' => 'Minified javascript file not found.'];
    }

    $mysqli = connect_db();
    $module = $mysqli->escape_string($us_module);
    $version = 0;
    $query = 'SELECT MAX(version) AS version FROM autoupdate WHERE ' .
      'label = "' . $module . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($autoupdate = $mysqli_result->fetch_assoc()) {
        $version = (int)$autoupdate['version'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->BuildModule 1: ' . $mysqli->error);
    }
    $version++;
    $display = (int)$_POST['display'];
    $us_title = $_POST['title'];
    $title = $mysqli->escape_string($us_title);
    $us_description = $_POST['description'];
    $description = $mysqli->escape_string($us_description);
    $installed = 0;
    // The Base module isn't listed in installed_modules but it should be set
    // as installed in the autoupdate table.
    if ($module === 'base') {
      $installed = 1;
    }
    else {
      $query = 'SELECT label FROM installed_modules WHERE ' .
        'label = "' . $module . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($installed_modules = $mysqli_result->fetch_assoc()) {
          $installed = 1;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Autoupdate->BuildModule 2: ' . $mysqli->error);
      }
    }
    $query = 'INSERT INTO autoupdate VALUES ("' . $module . '", ' .
      $version . ', "' . $title . '", "' . $description . '", ' .
      $display . ', ' . $installed . ', ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->BuildModule 3: ' . $mysqli->error);
    }
    $mysqli->close();

    // Create the new tar file in the public directory.
    exec('tar czf public/dobrado-' . $us_module . '-' . $version . '.tgz ' .
         $tar_files);
    $this->CreatePost($us_module, $version, $us_title,
                      $us_description, $display);
    return ['result' => $module . ' module version ' . $version . ' created.'];
  }

  private function BuildCore($default_modules) {
    foreach (explode(' ', $_POST['core']) as $file) {
      if (!file_exists($file)) return ['error' => $file . ' not found.'];
    }

    $mysqli = connect_db();
    $version = 0;
    $query = 'SELECT MAX(version) AS version FROM autoupdate WHERE ' .
      'label = "core"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($autoupdate = $mysqli_result->fetch_assoc()) {
        $version = (int)$autoupdate['version'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->BuildCore 1: ' . $mysqli->error);
    }
    $version++;
    $us_title = $_POST['title'];
    $title = $mysqli->escape_string($us_title);
    $us_description = $_POST['description'];
    $description = $mysqli->escape_string($us_description);
    $query = 'INSERT INTO autoupdate VALUES ("core", ' . $version . ', ' .
      '"' . $title . '", "' . $description . '", 0, 1, ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->BuildCore 2: ' . $mysqli->error);
    }
    $mysqli->close();

    // Create the new tar file in the public directory. 
    exec('tar czf public/dobrado-core-' . $version . '.tgz ' . $_POST['core']);
    // The full version of Dobrado is also updated when creating core updates.
    $tar_files = '';
    $css_list = ['farbtastic.css', 'jquery-ui.structure.css', 'lightbox.css'];
    foreach ($css_list as $css_file) {
      if (!file_exists('css/' . $css_file)) {
        return ['error' => 'css/' . $css_file . ' not found.'];
      }
      $tar_files .= ' css/' . $css_file;
    }
    $images_list = ['arrow-next.png', 'arrow.png', 'arrow-prev.png',
                    'calendar.gif', 'close.png', 'default_thumb.jpg',
                    'edit_menu_off.png', 'edit_menu_on.png', 'extended.png',
                    'facebook.png', 'layers-2x.png', 'layers.png',
                    'loading.gif', 'marker-icon-2x.png', 'marker-icon.png',
                    'marker.png', 'marker-shadow.png', 'mask.png', 'next.png',
                    'pagination.png', 'prev.png', 'sort-asc.gif',
                    'sort-desc.gif', 'spacer.png', 'tick.png', 'twitter.png',
                    'wheel.png'];
    foreach ($images_list as $images_file) {
      if (!file_exists('images/' . $images_file)) {
        return ['error' => 'images/' . $images_file . ' not found.'];
      }
      $tar_files .= ' images/' . $images_file;
    }
    $js_list = ['ckeditor', '3rdparty.js', 'core.js', 'core.pub.js',
                'deploy.js'];
    foreach ($js_list as $js_file) {
      if (!file_exists('js/' . $js_file)) {
        return ['error' => 'js/' . $js_file . ' not found.'];
      }
      $tar_files .= ' js/' . $js_file;
    }
    $js_source_list = ['core.js', 'core.pub.js', 'deploy.js', 'jquery-ui.js',
                       'jquery.js', 'lightbox.js'];
    foreach ($js_source_list as $js_source_file) {
      if (!file_exists('js/source/' . $js_source_file)) {
        return ['error' => 'js/source/' . $js_source_file . ' not found.'];
      }
      $tar_files .= ' js/source/' . $js_source_file;
    }
    // Add default modules to the list, but they don't all have js so don't
    // return an error if not found here.
    foreach ($default_modules as $label) {
      $php_file = 'php/modules/' . ucfirst($label) . '.php';
      if (!file_exists($php_file)) {
        return ['error' => $php_file . ' not found.'];
      }
      $tar_files .= ' ' . $php_file;
      $minified_js = 'js/dobrado.' . $label . '.js';
      $source_js = 'js/source/dobrado.' . $label . '.js';
      $minified_exists = file_exists($minified_js);
      $source_exists = file_exists($source_js);
      if ($minified_exists && $source_exists) {
        $tar_files .= ' ' . $minified_js . ' ' . $source_js;
      }
      else if ($minified_exists) {
        return ['error' => 'Source javascript file not found.'];
      }
      else if ($source_exists) {
        return ['error' => 'Minified javascript file not found.'];
      }
    }
    $php_list = ['add.php', 'api.php', 'auth_endpoint.php', 'auth.php',
                 'autoloader.php', 'box_style.php', 'browse.php', 'cloud.php',
                 'config.php', 'confirm.php', 'content.php', 'copy.php',
                 'cron.php', 'default.php', 'deploy.php', 'download.php',
                 'image.php', 'init.php', 'installer.php', 'layout_editor.php',
                 'layout.php', 'media_endpoint.php', 'microformats.php',
                 'micropub.php', 'microsub.php', 'module.php', 'move.php',
                 'notify.php', 'page.php', 'page_style.php', 'private.php',
                 'publish.php', 'rebuild.php', 'remove.php', 'request.php',
                 'search.php', 'site_style.php', 'spark.php', 'start.php',
                 'status.php', 'token_endpoint.php', 'update.php', 'user.php',
                 'webaction.php', 'webmention.php', 'write_instance.php'];
    foreach ($php_list as $php_file) {
      if (!file_exists('php/' . $php_file)) {
        return ['error' => 'php/' . $php_file . ' not found.'];
      }
      $tar_files .= ' php/' . $php_file;
    }
    // Note that db_config.php is not included here as it should be generated
    // by deploy.php. Also to distribute a copy of Dobrado with error reporting
    // turned off, it should be set to 0 in the local copy of db.php.
    $php_function_list = ['copy_page.php', 'create.php', 'db.php',
                          'install_module.php', 'microformats.php',
                          'new_module.php', 'new_user.php', 'page_owner.php',
                          'permission.php', 'rss.php', 'session.php',
                          'style.php', 'update_layout.php',
                          'update_page_style.php', 'write_style.php'];
    foreach ($php_function_list as $php_file) {
      if (!file_exists('php/functions/' . $php_file)) {
        return ['error' => 'php/functions/' . $php_file . ' not found.'];
      }
      $tar_files .= ' php/functions/' . $php_file;
    }
    if (!file_exists('php/idn/idna_convert.class.php')) {
      return ['error' => 'php/idn/idna_convert.class.php not found.'];
    }
    $tar_files .= ' php/idn/idna_convert.class.php';
    if (!file_exists('php/library')) {
      return ['error' => 'php/library not found.'];
    }
    $tar_files .= ' php/library';
    // Don't want to list all directories here, so just exclude Serializer
    // directory created by HTMLPurifier.
    $exclude = '--exclude=php/library/HTMLPurifier/DefinitionCache/Serializer';

    if (!file_exists('rss/index.php')) {
      return ['error' => 'rss/index.php not found.'];
    }
    $tar_files .= ' rss/index.php';
    if (!file_exists('themes')) {
      return ['error' => 'themes not found.'];
    }
    $tar_files .= ' themes';
    if (!file_exists('index.php')) {
      return ['error' => 'index.php not found.'];
    }
    $tar_files .= ' index.php';
    if (!file_exists('robots.txt')) {
      return ['error' => 'robots.txt not found.'];
    }
    $tar_files .= ' robots.txt';
    exec('tar czf public/dobrado.tgz ' . $exclude . $tar_files);
    $this->CreatePost('core', $version, $us_title, $us_description);
    return ['result' => 'core update version ' . $version . ' created.'];
  }

  private function CreatePost($us_label, $version, $us_title,
                              $us_description, $display = 0) {
    $autoupdate_feed = $this->Substitute('autoupdate-feed');
    if ($autoupdate_feed === '') {
      $this->Log('Autoupdate->CreatePost: autoupdate-feed not set.');
      return;
    }

    // Switch back to php directory for calling other modules.
    chdir('php');
    // Note that category must be label,version,display as stored by the Post
    // module, but used by SaveItem below. Also SimplePie only returns unique
    // categories so can't use integers for both version and display.
    $display_string = $display === 1 ? 'true' : 'false';
    $us_category = $us_label . ',' . $version . ',' . $display_string;
    $us_content = ['data' => $us_description, 'dataOnly' => false,
                   'title' => $us_title, 'category' => $us_category];
    $current_page = $this->user->page;
    // When autoupdate-testing is set, the post is first published to a testing
    // feed. The post is later published to autoupdate-feed by creating another
    // post and scheduling a publish time.
    $autoupdate_testing = $this->Substitute('autoupdate-testing');
    if ($autoupdate_testing !== '') {
      $this->user->page = $autoupdate_testing;
      $post = new Module($this->user, $this->owner, 'post');
      if ($post->IsInstalled()) {
        $id = new_module($this->user, $this->owner, 'post', $this->user->page,
                         $post->Group(), $post->Placement());
        $this->StorePost($us_label, $version, $id);
        $post->Add($id);
        $result = $post->SetContent($id, $us_content);
        if (isset($result['permalink'])) {
          $post->Factory('ContentUpdated', [$id, $us_description, $us_category,
                                            $this->user->page,
                                            $result['permalink']]);
        }
      }
    }

    $this->user->page = $autoupdate_feed;
    $post = new Module($this->user, $this->owner, 'post');
    if ($post->IsInstalled()) {
      $id = new_module($this->user, $this->owner, 'post', $this->user->page,
                       $post->Group(), $post->Placement());
      $this->StorePost($us_label, $version, $id);
      $post->Add($id);
      // If a testing feed is used, a schedule time should also be set so
      // that the post gets published to the updates feed at a later time.
      $schedule = $this->Substitute('autoupdate-schedule');
      if ($schedule !== '') {
        $us_content['draft'] = true;
        $us_content['schedule'] = $schedule;
      }
      $result = $post->SetContent($id, $us_content);
      if ($schedule === '' && isset($result['permalink'])) {
        $post->Factory('ContentUpdated', [$id, $us_description, $us_category,
                                          $this->user->page,
                                          $result['permalink']]);
      }
    }
    $this->user->page = $current_page;
  }

  private function FinishInstall() {
    // This function is required because the new module was written to
    // instance.php in InstallModule but the Module class could not be
    // re-declared to load the new module without a new request.
    $mysqli = connect_db();
    $installed = false;
    $version = 0;
    $label = $mysqli->escape_string($_POST['label']);
    $query = 'SELECT installed, version FROM autoupdate WHERE ' .
      'label = "' . $label . '" ORDER BY version DESC LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($autoupdate = $mysqli_result->fetch_assoc()) {
        $installed = $autoupdate['installed'] === '1';
        $version = (int)$autoupdate['version'];
      }
    }
    else {
      $this->Log('Autoupdate->FinishInstall 1: ' . $mysqli->error);
    }
    if ($installed) {
      $mysqli->close();
      return ['error' => 'The latest version of ' . $label .
                         ' is already installed.'];
    }

    $query = 'UPDATE autoupdate SET installed = 1 WHERE ' .
      'label = "' . $label . '" AND version = ' . $version;
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->FinishInstall 2: ' . $mysqli->error);
    }
    $mysqli->close();

    if (!isset($_SESSION['autoupdate-install'])) {
      return ['error' => 'InstallModule was not called.'];
    }

    $result = $_SESSION['autoupdate-install'];
    $_SESSION['autoupdate-install'] = NULL;
    // If install_module couldn't call Install method need to do that now.
    if (isset($result['done']) && $result['done'] === false) {
      $module = new Module($this->user, $this->owner, $label);
      return ['dependencies' => $module->Install('../js')];
    }
    return $result;
  }

  private function InstallModule() {
    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $version = '';
    $title = '';
    $description = '';
    $display = '';
    $installed = true;
    $query = 'SELECT version, title, description, display, installed FROM ' .
      'autoupdate WHERE label = "' . $label . '" ORDER BY version DESC LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($autoupdate = $mysqli_result->fetch_assoc()) {
        $version = $mysqli->escape_string($autoupdate['version']);
        $title = $mysqli->escape_string($autoupdate['title']);
        $description = $mysqli->escape_string($autoupdate['description']);
        $display = $mysqli->escape_string($autoupdate['display']);
        $installed = $autoupdate['installed'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->InstallModule: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($installed) {
      return ['error' => $label . ' module is already installed.'];
    }
    if ($version === '') {
      return ['error' => 'Version must be numeric to use Autoupdate.'];
    }
    $result = install_module($label, $version, $title, $description,
                             $display, $this->user);
    // Need to store the result of install_module to know what to do when
    // FinishInstall is called next.
    $_SESSION['autoupdate-install'] = $result;
    return $result;
  }

  private function ListUpdates() {
    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT label, version, title, description, display, installed, ' .
      'timestamp FROM autoupdate ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($autocomplete = $mysqli_result->fetch_assoc()) {
        $autocomplete['timestamp'] *= 1000;
        if ($autocomplete['title'] === '') {
          $autocomplete['title'] = $autocomplete['label'];
          $autocomplete['description'] =
            htmlspecialchars_decode($autocomplete['description']);
        }
        $result[] = $autocomplete;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->ListUpdates: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function RemoveUpdate() {
    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $version = (int)$_POST['version'];
    $remove = false;
    // Can only remove the latest version of the update.
    $query = 'SELECT MAX(version) AS version FROM autoupdate WHERE ' .
      'label = "' . $label . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($autoupdate = $mysqli_result->fetch_assoc()) {
        $remove = $version === (int)$autoupdate['version'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->RemoveUpdate 1: ' . $mysqli->error);
    }
    if ($remove) {
      $query = 'DELETE FROM autoupdate WHERE label = "' . $label . '" AND ' .
        'version = ' . $version;
      if (!$mysqli->query($query)) {
        $this->Log('Autoupdate->RemoveUpdate 2: ' . $mysqli->error);
      }
      $post = new Module($this->user, $this->owner, 'post');
      if ($post->IsInstalled()) {
        $query = 'SELECT box_id FROM autoupdate_post WHERE ' .
          'label = "' . $label . '" AND version = ' . $version;
        if ($mysqli_result = $mysqli->query($query)) {
          while ($autoupdate_post = $mysqli_result->fetch_assoc()) {
            $post->Remove((int)$autoupdate_post['box_id']);
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Autoupdate->RemoveUpdate 3: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();

    if ($remove) return $this->ListUpdates();
    return ['error' => 'Only the latest version of the update can be removed.'];
  }

  private function SaveItem($us_title, $us_content,
                            $us_category_list, $timestamp) {
    // This function is called from Reader->SaveItem which returns the category
    // list as an array. The values must be label, version and display as set
    // in CreatePost above.
    if (!is_array($us_category_list) || count($us_category_list) !== 3) {
      $this->Log('Autoupdate->SaveItem 1: Category error.');
      return;
    }

    $mysqli = connect_db();
    list($us_label, $us_version, $us_display) = $us_category_list;
    $label = $mysqli->escape_string($us_label);
    $version = $mysqli->escape_string($us_version);
    $display = $us_display === 'true' ? 1 : 0;
    $title = $mysqli->escape_string($us_title);
    $content = $mysqli->escape_string($us_content);

    if ($title === '' && $content === '') {
      // Empty title and content means the update has been removed, so it's
      // also removed here so that the new update with the same version number
      // can be applied when it's re-built.
      $query = 'DELETE FROM autoupdate WHERE label = "' . $label . '" AND ' .
        'version = ' . $version;
      if ($mysqli->query($query)) {
        $this->Log('Autoupdate->SaveItem 2: ' . $label . ' update version ' .
                   $version . ' has been removed.');
      }
      else {
        $this->Log('Autoupdate->SaveItem 3: ' . $mysqli->error);
      }
      // Also need to remove the current version from installed modules.
      $query = 'UPDATE installed_modules SET version = "" WHERE ' .
        'label = "' . $label . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Autoupdate->SaveItem 4: ' . $mysqli->error);
      }
      $mysqli->close();
      return;
    }

    $current_version = 0;
    $current_timestamp = 0;
    $query = 'SELECT version, timestamp FROM autoupdate WHERE ' .
      'label = "' . $label . '" AND installed = 1 ORDER BY version DESC ' .
      'LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($autoupdate = $mysqli_result->fetch_assoc()) {
        $current_version = (int)$autoupdate['version'];
        $current_timestamp = (int)$autoupdate['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Autoupdate->SaveItem 5: ' . $mysqli->error);
    }
    $installed = 0;
    // Check if this update should be installed. If not, it will be saved as
    // uninstalled below so that the user can choose to install it later. Also
    // current version equal to zero is a special case, it means the timestamp
    // should be checked rather than looking for a consecutive version number.
    if (($current_version === 0 && $timestamp > $current_timestamp) ||
        ($current_version !== 0 && (int)$version === $current_version + 1)) {
      set_time_limit(0);
      $url = $this->Substitute('autoupdate-url');
      $filename = 'dobrado-' . $label . '-' . $version . '.tgz';
      $file = file_get_contents($url . '/public/' . $filename);
      if ($file) {
        chdir('..');
        if ($handle = fopen($filename, 'w')) {
          fwrite($handle, $file);
          fclose($handle);
          exec('tar xzf ' . $filename);
          unlink($filename);
          $result = [];
          chdir('php');
          if ($label === 'core') {
            // This optional file allows core updates to make database changes,
            // and to rebuild javascript and css files when required.
            $core_update = 'functions/core_update-' . $version . '.php';
            if (file_exists($core_update)) {
              include $core_update;
              if (function_exists('core_update')) {
                $result = core_update($this->user);
                unlink($core_update);
              }
            }
          }
          // When the current timestamp is 0 this module should not be installed
          // automatically. It has been downloaded if the user wants to install
          // it later, but don't need to do anything else here.
          else if ($current_timestamp !== 0) {
            $result = install_module($label, $version, $title, $content,
                                     $display, $this->user, true);
          }
          // If SaveItem is called from the Reader module directly after adding
          // the feed, the owner may not be set to admin here for Notifications.
          if ($current_timestamp !== 0) {
            $current_owner = $this->owner;
            $this->owner = 'admin';
            if (isset($result['error'])) {
              $this->Log('Autoupdate->SaveItem 6: Error: ' . $result['error']);
              $this->Notification('autoupdate', 'Autoupdate', 'Error: ' .
                                  $result['error'], 'system');
            }
            else {
              $this->Log('Autoupdate->SaveItem 7: ' . $label .
                         ' updated to version ' . $version);
              $this->Notification('autoupdate', 'Autoupdate', $label .
                                  ' updated to version ' . $version, 'system');
              $installed = 1;
            }
            $this->owner = $current_owner;
          }
        }
        else {
          $this->Log('Autoupdate->SaveItem 8: Could not write local file ' .
                     $filename . ' to ' . getcwd() . '. Update not installed.');
        }
        // If haven't changed back to the php directory above do that now.
        if (basename(getcwd()) !== 'php') chdir('php');
      }
      else {
        $this->Log('Autoupdate->SaveItem 9: There was a problem transferring ' .
                   'the file ' . $filename . '. Update not installed.');
      }
    }
    // If the current timestamp is more recent assume a newer version is
    // installed and mark this older update as already installed.
    else if ($current_version === 0 && $timestamp <= $current_timestamp) {
      $installed = 1;
    }
    $query = 'INSERT INTO autoupdate VALUES ("' . $label . '", ' .
      $version . ', "' . $title . '", "' . $content . '", ' . $display . ', ' .
      $installed . ', ' . $timestamp . ') ON DUPLICATE KEY UPDATE ' .
      'title = "' . $title . '", description = "' . $content . '", ' .
      'display = ' . $display . ', timestamp = ' . $timestamp;
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->SaveItem 10: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function StorePost($us_label, $version, $id) {
    $mysqli = connect_db();
    $label = $mysqli->escape_string($us_label);
    $query = 'INSERT INTO autoupdate_post VALUES ("' . $label . '", ' .
      $version . ', ' . $id . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Autoupdate->StorePost: ' . $mysqli->error);
    }
    $mysqli->close();
  }

}
