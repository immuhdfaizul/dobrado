/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.groupwizard) {
  dobrado.groupwizard = {};
}
(function() {

  'use strict';

  // Sections are shown one at a time, so track the current one.
  var section = 0;
  var total = 0;

  $(function() {
    total = $('.groupwizard-content > div').length - 1;
    // Only create a dialog if the groupwizard hasn't been permanently added
    // to the page, in which case it's parent div has class 'middle'.
    if (!$('.groupwizard').parent().hasClass('middle')) {
      $('.groupwizard').dialog({
        show: true,
        width: 700,
        height: 450,
        position: { my: 'top', at: 'top+50', of: window },
        create: dobrado.fixedDialog,
        close: function() { $('.groupwizard').remove(); }
      });
    }
    init();
  });

  function init() {
    if ($('.groupwizard').parent().hasClass('middle')) {
      $('.groupwizard').show();
    }
    else {
      var title = 'Group Settings';
      if ($('.groupwizard-selected-group').length === 1) {
        title += ': Updating ' + $('.groupwizard-selected-group').html();
      }
      $('.groupwizard').dialog('option', 'title', title);
    }
    // Set up navigation between sections.
    $('.groupwizard .previous').button({ disabled: true }).click(previous);
    $('.groupwizard .next').button().click(next);
    $('#groupwizard-new-group').button().click(function() {
      $('.groupwizard-new-group-info').show();
      $('.groupwizard-existing-group-info').hide();
    });
    $('#groupwizard-existing-group').button().click(function() {
      if ($('#groupwizard-existing-group-select').length === 0) {
        // If there's no select then only the default group is available,
        // go to the next section as default settings are already provided.
        next();
      }
      else {
        $('.groupwizard-existing-group-info').show();
        $('.groupwizard-new-group-info').hide();
      }
    });
    $('#groupwizard-create-group').button().click(createGroup);
    $('#groupwizard-update-markup').button().click(updateMarkup);
    $('#groupwizard-existing-group-select').change(changeGroup);
    // Some links in the wizard are used to open the preferences dialog.
    $('.groupwizard-purchase-other-order').click(dobrado.account.option);
    $('.groupwizard-purchase-volunteer').click(dobrado.account.option);
    $('.groupwizard-invoice-notifications').click(dobrado.account.option);
    $('.groupwizard-invite').click(dobrado.account.option);
    // Want to know when the dialog is open to show the requested group.
    $('.account').on('dialogopen', showGroup);
    // Need to also update on focus in case it's already open.
    $('.account').on('dialogfocus', showGroup);
    // Some fields switch between datepickers and selects.
    $('#groupwizard-pre-order-open-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-pre-order-final-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-co-op-day-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-day-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-remove-orders-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-send-orders-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-group-order-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-update-order-cycle').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-timezone').autocomplete({
      minLength: 1,
      search: dobrado.fixAutoCompleteMemoryLeak,
      source: dobrado.control.timezones() });
    // Save any changes as they are made.
    $('.groupwizard :input').change(save);
    // Some sections have hidden content and should be displayed depending
    // on current settings.
    if ($('#groupwizard-pre-order').is(':checked')) {
      $('.groupwizard-pre-order-settings').show();
    }
    if ($('#groupwizard-surcharge').is(':checked')) {
      $('.groupwizard-payment-settings').show();
    }
    if ($('#groupwizard-stock-order-available').is(':checked')) {
      $('#groupwizard-stock-order-update').parent().show();
    }
    // Some input methods need to be switched depending on their current value.
    switchInput('pre-order-open');
    switchInput('pre-order-final');
    switchInput('co-op-day');
    switchInput('invoice-day');
    switchInput('invoice-remove-orders');
    switchInput('invoice-send-orders');
    switchInput('invoice-group-order');
  }

  function previous() {
    $('.groupwizard-' + section).hide();
    section--;
    $('.groupwizard-' + section).show();
    if (section === 0) {
      $('.groupwizard .previous').button('option', 'disabled', true);
    }
    else {
      $('.groupwizard .previous').button('option', 'disabled', false);
    }
    $('.groupwizard .next').button('option', 'disabled', false);
  }

  function next() {
    $('.groupwizard-' + section).hide();
    section++;
    $('.groupwizard-' + section).show();
    if (section === total) {
      $('.groupwizard .next').button('option', 'disabled', true);
    }
    else {
      $('.groupwizard .next').button('option', 'disabled', false);
    }
    $('.groupwizard .previous').button('option', 'disabled', false);
  }

  function save() {
    var value = '';
    // Remove the 'groupwizard-' prefix from the label.
    var label = $(this).attr('id').substr(12);
    // Toggle input types when date input checkboxes are changed, and clear
    // the current value to force a change event.
    if (label === 'pre-order-open-date') {
      $('#groupwizard-pre-order-open-input').val('').parent().toggle();
      $('#groupwizard-pre-order-open-select').val('').parent().toggle();
      return;
    }
    if (label === 'pre-order-final-date') {
      $('#groupwizard-pre-order-final-input').val('').parent().toggle();
      $('#groupwizard-pre-order-final-select').val('').parent().toggle();
      return;
    }
    if (label === 'co-op-day-date') {
      $('#groupwizard-co-op-day-input').val('').parent().toggle();
      $('#groupwizard-co-op-day-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-day-date') {
      $('#groupwizard-invoice-day-input').val('').parent().toggle();
      $('#groupwizard-invoice-day-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-send-orders-date') {
      $('#groupwizard-invoice-send-orders-input').val('').parent().toggle();
      $('#groupwizard-invoice-send-orders-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-group-order-date') {
      $('#groupwizard-invoice-group-order-input').val('').parent().toggle();
      $('#groupwizard-invoice-group-order-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-remove-orders-date') {
      $('#groupwizard-invoice-remove-orders-input').val('').parent().toggle();
      $('#groupwizard-invoice-remove-orders-select').val('').parent().toggle();
      return;
    }
    if (label === 'new-group-input' || label === 'existing-group-select') {
      return;
    }

    // Some labels have a suffix for different input modes.
    if (label === 'pre-order-open-select' || label === 'pre-order-open-input') {
      label = 'pre-order-open';
    }
    else if (label === 'pre-order-final-select' ||
             label === 'pre-order-final-input') {
      label = 'pre-order-final';
    }
    else if (label === 'co-op-day-select' || label === 'co-op-day-input') {
      label = 'co-op-day';
    }
    else if (label === 'invoice-day-select' || label === 'invoice-day-input') {
      label = 'invoice-day';
    }
    else if (label === 'invoice-send-orders-select' ||
             label === 'invoice-send-orders-input') {
      label = 'invoice-send-orders';
    }
    else if (label === 'invoice-group-order-select' ||
             label === 'invoice-group-order-input') {
      label = 'invoice-group-order';
    }
    else if (label === 'invoice-remove-orders-select' ||
             label === 'invoice-remove-orders-input') {
      label = 'invoice-remove-orders';
    }

    if ($(this).is(':checkbox')) {
      value = $(this).is(':checked') ? 'true' : 'false';
    }
    else {
      value = $(this).val();
    }
    // If not showing both availability settings and updates are performed,
    // warn the user that they should only continue if update is finished.
    if (label === 'stock-order-available' && value === 'false' &&
        $('#groupwizard-stock-order-update').val() !== '') {
      if (!confirm('This setting should only be turned off once the last ' +
                   'update has run, to make sure all stock values match. ' +
                   'Updates will also be turned off now, continue?')) {
        $('#groupwizard-stock-order-available').prop('checked', true);
        return;
      }
    }

    dobrado.log('Saving setting.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'saveSetting', label: label,
             value: value, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard saveSetting')) {
          return false;
        }
        // Some sections are toggled when a particular setting changes.
        if (label === 'pre-order') {
          $('.groupwizard-pre-order-settings').toggle();
        }
        if (label === 'surcharge') {
          $('.groupwizard-payment-settings').toggle();
        }
        if (label === 'stock-order-available') {
          $('#groupwizard-stock-order-update').parent().toggle();
          // If not showing both availability settings then don't allow updates.
          if (value === 'false' &&
              $('#groupwizard-stock-order-update').val() !== '') {
            $.post('/php/request.php',
                   { request: 'groupwizard', action: 'saveSetting',
                     label: 'stock-order-update', value: '',
                     url: location.href, token: dobrado.token },
              function(response) {
                if (dobrado.checkResponseError(response, 'saveSetting 2')) {
                  return false;
                }
                $('#groupwizard-stock-order-update').val('');
              });
          }
        }
        $('#groupwizard-info').html('All changes saved.');
    });
  }

  function switchInput(label) {
    label = '#groupwizard-' + label;
    if ($(label + '-input').val() !== '') {
      $(label + '-date').prop('checked', true);
      $(label + '-input').parent().show();
      $(label + '-select').parent().hide();
    }
  }

  function showGroup() {
    // Activate the preferences tab.
    $('#account-tabs').tabs('option', 'active', 1);
    // It's a bit hard to know which link was clicked so use the section
    // variable as a guide.
    if (section === 1) {
      $('#group-input').val('admin/invite-notifications');
    }
    if (section === 3) {
      $('#group-input').val('admin/purchase-other-order');
    }
    else if (section === 4) {
      $('#group-input').val('admin/purchase-volunteer');
    }
    else if (section === 6) {
      var group = $('#groupwizard-current-group').val();
      if (group) {
        $('#group-input').val('admin/invoice-notifications-' + group);
      }
      else {
        $('#group-input').val('admin/invoice-notifications');
      }
    }
    $('#account-groups .show-members').click();
  }

  function createGroup() {
    dobrado.log('Creating new group.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'createGroup',
             group: $('#groupwizard-new-group-input').val(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard createGroup')) {
          return false;
        }
        var groupwizard = JSON.parse(response);
        $('.groupwizard-content').html(groupwizard.content);
        init();
        // Move to the next section once a group has been created.
        next();
      });
  }

  function changeGroup() {
    var group = $(this).val();
    if (group === '') return;

    dobrado.log('Changing group.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'changeGroup',
             group: group, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard changeGroup')) {
          return false;
        }
        var groupwizard = JSON.parse(response);
        $('.groupwizard-content').html(groupwizard.content);
        init();
        // Move to the next section once the group has changed.
        next();
      });
  }

  function updateMarkup() {
    dobrado.log('Updating markup.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'updateMarkup',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard updateMarkup')) {
          return false;
        }
        var groupwizard = JSON.parse(response);
        $('#groupwizard-update-markup-info').html(groupwizard.content);
      });
  }

}());
