// @source: /js/source/dobrado.cart.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.cart){dobrado.cart={};}
(function(){'use strict';var cart={};var settings=null;var editor=null;var itemIndex=0;var shippingIndex=0;var browserTarget='';var previousScroll=0;var oldName='';var showUnavailable=false;var showTab='All tabs';var itemChanged=false;var cartIds={};var searchTerm='';var cartTabs=[];var startUnavailable='';var endUnavailable='';var cartItemDisplay='';var cartEditMode=false;$(function(){if(dobrado.localStorage){if(localStorage.cart){cart=JSON.parse(localStorage.cart);cartMiniTotal();}}
if($('.cart').length===0){return;}
if($('.cart').is(':hidden')){setTimeout(init,500);}
else{init();}});function decimalString(value){return(value+0.0001).toFixed(2);}
function init(){function resetHash(){if(history.pushState){history.pushState(null,null,'#');}
else{window.location.hash='#';}}
checkDownloads();$('.cart-item .item-add').button().click(cartAdd);$('.cart-item .item-quantity').val('0');$('.cart-item .item-quantity').spinner({min:0,spin:cartQuantity,change:cartQuantity});$('#cart-checkout').button({disabled:true}).click(checkout);$('#anchor-cart-checkout-link').click(checkout);$('#cart-tabs').tabs();$('#cart-confirm-button').button().click(confirmPurchase);$('#cart-continue-button').button().click(function(){$('#cart-checkout-dialog').dialog('close');});$('#customer-detail-method').change(toggleCustomInput);if($('#customer-detail-method').val()==='pickup'){$('#customer-detail-description').parent().show();}
$('#customer-detail-submit').button().click(customerDetail);$('.cart-item .item-name > a').click(cartItem);$('.cart-item a .item-image').click(cartItem);$('.cart-item .item-group').change(sortGroup);startUnavailable=$('#cart-start-unavailable').val();endUnavailable=$('#cart-end-unavailable').val();$(window).scroll(fixMiniTotal);fixMiniTotal();$('.cart').show();cartItemDisplay=$('.cart-item:not(.hidden)').css('display');if($('.cart-payment-message').html()!==''){$('.cart-payment-message button').button().click(paymentDone);$('.cart-payment-message').dialog({show:true,modal:true,width:450,height:200,position:{my:'top',at:'top+50',of:window},title:'Payment',close:paymentDone,create:dobrado.fixedDialog});}
$('#cart-checkout-dialog').dialog({show:true,autoOpen:false,modal:true,width:550,height:550,position:{my:'top',at:'top+50',of:window},title:'Checkout',create:dobrado.fixedDialog});$('#cart-editor').dialog({show:true,autoOpen:false,close:close,width:780,height:500,position:{my:'top',at:'top+50',of:window},title:'Cart Editor',create:dobrado.fixedDialog});$('#cart-item-dialog').dialog({show:true,autoOpen:false,close:resetHash,width:700,height:500,position:{my:'top',at:'top+50',of:window},title:'Cart Item',create:dobrado.fixedDialog});$('#cart-edit-button').button({icon:'ui-icon-pencil',showLabel:false}).click(cartEditor);cartTotal();$.each(cart,function(name,item){var id=item.id;if(id){var cssName=id.match(/^cart-item-id-(.+)$/);if(item.selected&&cssName&&cssName.length===2){$('#'+id+' .item-add').button({label:'Remove from cart'});$('#item-quantity-'+cssName[1]).spinner('value',item.quantity);}}});var cartNames=[];$('.cart-item:not(.hidden)').each(function(index){var name=$(this).children('.item-name').children('a').text();cartNames.push(name);var cssName=$(this).attr('id').match(/^cart-item-id-(.+)$/);if(cssName.length===2){cartIds[name]=cssName[1];}});if(window.location.hash){displayItem($(window.location.hash));}
$('#cart-search').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:cartNames,select:cartDisplay});$('#cart-search').val('').keyup(cartSearch);$('#cart-clear-search').button().click(function(){$('#cart-search').val('');$('.cart-item:not(.hidden)').show();});$('#cart-tabs > ul > li').children('a').map(function(){cartTabs.push($(this).attr('href'));});}
function checkDownloads(){$('.cart-download-list li').each(function(i){var file=$(this).html();setTimeout(function(){location.href='/php/download.php?file='+file;},i*10000);});}
function cartDisplay(event,ui){$('.cart-item:not(.hidden)').each(function(){var name=$(this).children('.item-name').text().toLowerCase();if(ui.item.value.toLowerCase()===name){window.location.hash=$(this).attr('id');displayItem($(this));return false;}});}
function cartSearch(){var current=$('#cart-search').val().toLowerCase();if(searchTerm!==current)searchTerm=current;if(searchTerm===''){$('.cart-item:not(.hidden)').show();return;}
var active=$('#cart-tabs').tabs('option','active');var id=cartTabs[active];var tabItems=id+' > .cart-item:not(.hidden)';var switchTab=true;$(tabItems).each(function(){var name=$(this).children('.item-name').text().toLowerCase();if(name.indexOf(searchTerm)!==-1){$(this).show();switchTab=false;return true;}
$(this).hide();});for(var i=0;i<cartTabs.length;i++){if(i===active)continue;id=cartTabs[i];tabItems=id+' > .cart-item:not(.hidden)';$(tabItems).each(function(){var name=$(this).children('.item-name').text().toLowerCase();if(name.indexOf(searchTerm)!==-1){if(switchTab){$('#cart-tabs').tabs('option','active',i);}
$(this).show();switchTab=false;return true;}
$(this).hide();});}}
function cartPrice(item){var price=0;var checkMinimum=false;if(item.attr('id')){price=item.val();checkMinimum=true;}
else{price=item.text();}
var fields=price.match(/([0-9.]+)$/);if(fields&&fields.length===2){price=parseFloat(fields[1]);}
else{price=0;}
if(checkMinimum){var minimum=item.parents('.cart-item').children('.item-minimum').text();if(minimum!==''&&price<parseFloat(minimum)){price=minimum;item.val(minimum);}}
return price;}
function cartAdd(){var name=$(this).siblings('.item-name').children('a').text();var price=cartPrice($(this).siblings('.item-price'));if(price===0){$(this).siblings('.item-download').show();}
else{$(this).siblings('.item-download').hide();}
if(cart[name]){if(cart[name].selected){cart[name].selected=false;$(this).button({label:'Add to cart'});}
else{cart[name].selected=true;$(this).button({label:'Remove from cart'});}
cart[name].price=price;}
else{cart[name]={selected:true,price:price,quantity:1,id:$(this).parents('.cart-item').attr('id')};$(this).button({label:'Remove from cart'});}
if(dobrado.localStorage){localStorage.cart=JSON.stringify(cart);}
cartTotal();cartMiniTotal();return false;}
function cartQuantity(event,ui){var item=$(this).parents('.cart-item');var name=item.children('.item-name').children('a').text();var price=cartPrice(item.children('.item-price'));var quantity=0;if('value'in ui){quantity=ui.value;}
else{quantity=parseFloat($(this).val());if(quantity<0){quantity=0;$(this).val('0');}}
if(cart[name]){if(quantity===0){cart[name].selected=false;}
else{cart[name].selected=true;}
cart[name].price=price;cart[name].quantity=quantity;}
else if(quantity!==0){cart[name]={selected:true,price:price,quantity:quantity,id:item.attr('id')};}
if(dobrado.localStorage){localStorage.cart=JSON.stringify(cart);}
cartTotal();cartMiniTotal();if($('#cart-item-dialog').dialog('isOpen')){if($('#cart-item-dialog .item-name').text()===name){$('#cart-dialog-quantity').val(quantity);}}}
function cartMiniTotal(){var total=0;$.each(cart,function(name,item){if(item.selected&&item.price!==0){total+=parseFloat(decimalString(item.price*item.quantity));}});$('.cart-total .number').html('$'+decimalString(total));if(total>0){$('.cart-total').show();}}
function fixMiniTotal(){var currentScroll=$(this).scrollTop();if(previousScroll<=100&&currentScroll>100){$('.cart-total').parent().parent().addClass('fixed-wrapper-style');$('.cart-total').parent().addClass('fixed-style');}
else if(previousScroll>100&&currentScroll<=100){$('.cart-total').parent().parent().removeClass('fixed-wrapper-style');$('.cart-total').parent().removeClass('fixed-style');}
previousScroll=currentScroll;}
function cartTotal(){var itemTotal=0;var total=0;var quantity=0;var table='<table id="cart-total-table">';$.each(cart,function(name,item){if(item.selected&&item.price!==0){itemTotal=decimalString(item.price*item.quantity);total+=parseFloat(itemTotal);quantity+=item.quantity;table+='<tr><td>'+name+'</td><td>'+item.quantity+'</td><td>$'+itemTotal+'</td></tr>';}});$('.cart-total .number').html('$'+decimalString(total));if(total===0){$('#cart-checkout').button('option','disabled',true);$('#cart-total').html('');return;}
$('#cart-checkout').button('option','disabled',false);table+='<tr><td>Total</td><td>'+quantity+'</td><td>$'+
decimalString(total)+'</td></tr></table>';$('#cart-total').html(table);}
function cartSummary(shipping,processing,completed){var itemTotal=0;var total=0;var quantity=0;var table='<table id="cart-total-table">';var remove='';var emptyCell='';var emptyHeader='';if(!completed){remove='<td><button class="cart-checkout-remove">remove</button></td>';emptyCell='<td></td>';emptyHeader='<th></th>';}
table+='<tr>'+emptyHeader+'<th class="item">Item Name</th>'+'<th class="quantity">Quantity</th><th class="price">Price</th></tr>';$.each(cart,function(name,item){if(item.selected&&item.price!==0){itemTotal=decimalString(item.price*item.quantity);total+=parseFloat(itemTotal);quantity+=item.quantity;table+='<tr>'+remove+'<td class="item">'+name+'</td><td class="quantity">'+item.quantity+'</td><td class="price">$'+itemTotal+'</td></tr>';}});$('.cart-total .number').html('$'+decimalString(total));if(shipping){total+=shipping;table+='<tr><td class="item">Shipping Cost</td><td></td>'+'<td class="price">$'+decimalString(shipping)+'</td></tr>';}
if(processing){total+=processing;table+='<tr><td class="item">Processing Fee</td><td></td>'+'<td class="price">$'+decimalString(processing)+'</td></tr>';}
if(total===0){$('#cart-checkout').button('option','disabled',true);$('#cart-dispatch-info').hide();$('.cart-dispatch-date').hide();$('#cart-confirm-button').hide();$('#cart-summary').show().html('<i>Your shopping cart is empty.</i>');return;}
$('#cart-checkout').button('option','disabled',false);table+='<tr>'+emptyCell+'<td class="item">Total</td>'+'<td class="quantity">'+quantity+'</td>'+'<td class="price">$'+decimalString(total)+'</td></tr></table>';if(!completed){$('#cart-dispatch-info').show();$('.cart-dispatch-date').show();}
$('#cart-summary').show().html('<p>You have selected the following items:</p>'+table);if(completed){return;}
$('.cart-checkout-remove').button({icon:'ui-icon-closethick',showLabel:false}).click(cartRemove);if($('#cart-dispatch').length===0){return;}
$('#cart-dispatch').datepicker({dateFormat:dobrado.dateFormat,minDate:'+2d',beforeShowDay:function(date){if(startUnavailable&&endUnavailable){let start=startUnavailable*1000;let end=endUnavailable*1000;let time=date.getTime();if(time>=start&&time<end){return[false,''];}}
let day=date.getDay();return[day!==6&&day!==0,''];}});let defaultDate='+2d';let today=new Date().getDay();if(today===4){defaultDate='+4d';}
else if(today===5){defaultDate='+3d';}
if(startUnavailable&&endUnavailable){let minimum=2;if(defaultDate==='+3d'){minimum=3;}
else if(defaultDate==='+4d'){minimum=4;}
minimum*=86400;let start=(startUnavailable-minimum)*1000;let end=endUnavailable*1000;let time=new Date().getTime();if(time>=start&&time<end){defaultDate=new Date();defaultDate.setTime(end);}}
$('#cart-dispatch').datepicker('setDate',defaultDate);}
function cartItem(){function updateName(){var itemName=$(this).siblings('.item-name').children('a');var oldName=itemName.text();var newName=$(this).val();if(oldName!==newName){dobrado.log('Updating item name.','info');$.post('/php/request.php',{request:'cart',action:'updateName',oldName:oldName,name:newName,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart updateName')){return;}
itemName.text(newName);});}
$(this).siblings('.item-name').show();$(this).remove();}
if(cartEditMode){let inputHTML='<input class="item-name-input" type="text">';$(this).parents('.item-name').hide().after(inputHTML);let input=$(this).parents('.cart-item').children('.item-name-input');input.val($(this).text()).blur(updateName).keypress(function(event){if(event.keyCode===13){input.blur();}});setTimeout(function(){input.focus();},100);return false;}
else{displayItem($(this).parents('.cart-item'));}}
function cartRemove(){var name=$(this).parent('td').siblings().first().text();if(cart[name]){cart[name].selected=false;$('#item-quantity-'+cartIds[name]).val(0);}
if(dobrado.localStorage){localStorage.cart=JSON.stringify(cart);}
cartSummary();cartTotal();cartMiniTotal();}
function checkout(){if(!$('#cart-checkout').button('option','disabled')){$('#cart-checkout-dialog').dialog('open');$('#cart-checkout-message').html('');$('#cart-customer-details-form').hide();$('#cart-confirm-button').show();$('#cart-continue-button').show();cartSummary();}}
function confirmPurchase(){if($('#cart-dispatch').length===1&&!$('#cart-dispatch').datepicker('getDate')){alert('Please select a date.');return false;}
$('#cart-summary').hide();$('#cart-dispatch-info').hide();$('.cart-dispatch-date').hide();$('#cart-confirm-button').hide();$('#cart-continue-button').hide();$('#cart-checkout-message').html('Please enter your contact details:');$('#cart-customer-details-form').show();}
function customerDetail(){if($('#customer-detail-name').val()===''){$('#cart-checkout-info').html('Please enter your name.');return false;}
if($('#customer-detail-email').val()===''){$('#cart-checkout-info').html('Please enter your email.');return false;}
if($('#customer-detail-address').val()===''){$('#cart-checkout-info').html('Please enter your address.');return false;}
if($('#customer-detail-postcode').val()===''){$('#cart-checkout-info').html('Please enter your postcode.');return false;}
if($('#customer-detail-city').val()===''){$('#cart-checkout-info').html('Please enter your city.');return false;}
if($('#customer-detail-state').val()===''){$('#cart-checkout-info').html('Please enter your state.');return false;}
if($('#customer-detail-country').val()===''){$('#cart-checkout-info').html('Please enter your country.');return false;}
var date=$('#cart-dispatch').length===0?'':$('#cart-dispatch').datepicker('getDate').toDateString();$('#cart-checkout-info').html('Processing... please wait.');$.post('/php/request.php',{request:'cart',action:'checkout',cart:JSON.stringify(cart),date:date,name:$('#customer-detail-name').val(),email:$('#customer-detail-email').val(),phone:$('#customer-detail-phone').val(),address:$('#customer-detail-address').val(),city:$('#customer-detail-city').val(),state:$('#customer-detail-state').val(),country:$('#customer-detail-country').val(),postcode:$('#customer-detail-postcode').val(),notes:$('#customer-detail-notes').val(),description:$('#customer-detail-description').val(),method:$('#customer-detail-method').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart checkout')){return;}
var checkout=JSON.parse(response);$('#cart-checkout-info').html('');$('#cart-customer-details-form').hide();$('#cart-checkout-message').html(checkout.content);$('#cart-payment').button().click(clearCart);cartSummary(checkout.shipping,checkout.processing,true);});return false;}
function close(){if(editor){editor.destroy();}}
function cartEditor(){function imageBrowser(){browserTarget='#cart-item-image';dobrado.createModule('browser','browser','cart');return false;}
function downloadBrowser(){browserTarget='#cart-item-download';dobrado.createModule('browser','browser','cart');return false;}
dobrado.log('Editor loading...','info');$.post('/php/request.php',{request:'cart',action:'list',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart edit')){return;}
settings=JSON.parse(response);$('#cart-editor').html(settings.content);$('#cart-settings-tabs').tabs();$('#cart-editor').dialog('open');let height=$('#cart-item-full').height();if(!height)height=240;CKEDITOR.addCss('.cke_editable { margin: 10px; }');editor=CKEDITOR.replace('cart-item-full',{allowedContent:true,autoGrow_minHeight:height,autoGrow_maxHeight:height,autoGrow_onStartup:true,disableNativeSpellChecker:false,enterMode:CKEDITOR.ENTER_BR,filebrowserBrowseUrl:'/php/browse.php',removePlugins:'elementspath,tableselection,tabletools,contextmenu,liststyle',resize_enabled:false,toolbar:[['Source','-','NewPage'],['Undo','Redo','-','SelectAll','RemoveFormat'],['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField'],['Bold','Italic','Underline','Strike'],['Subscript','Superscript'],['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Link','Unlink','Anchor'],['EmojiPanel','Image','Table','HorizontalRule','SpecialChar','PageBreak'],['Format','Font','FontSize'],['TextColor','BGColor'],['Maximize','ShowBlocks','-','About']]});$('#cart-filter-available').change(function(){showUnavailable=!showUnavailable;$('#cart-item-previous').button('enable');$('#cart-item-next').button('enable');});$('#cart-filter-tabs').change(function(){showTab=$(this).val();$('#cart-item-previous').button('enable');$('#cart-item-next').button('enable');});$('#cart-item-save').button().click(saveItem);$('#cart-item-remove').button().click(removeItem);$('#cart-item-new').button().click(clearItem);$('#cart-item-previous').button().click(previousItem);$('#cart-item-next').button().click(nextItem);$('.cart-item-image-browse').button().click(imageBrowser);$('.cart-item-download-browse').button().click(downloadBrowser);$('#cart-item-variable').click(function(){$('.show-item-minimum').toggle();});$('#cart-item-reset').button().click(resetItem);$('#cart-item-reset-help').button().click(function(){$('.cart-modified-info').toggle();return false;});$('#cart-item-form :input').change(function(){itemChanged=true;});$('#cart-shipping-save').button().click(saveShipping);$('#cart-shipping-remove').button().click(removeShipping);$('#cart-shipping-new').button().click(newShipping);$('#cart-shipping-previous').button().click(previousShipping);$('#cart-shipping-next').button().click(nextShipping);$('#cart-checkout-save').button().click(saveCheckout);if(settings.items){showItem(settings.items[0]);}
else{settings.items=[];}
updateItems();if(settings.shipping.length!==0){showShipping(settings.shipping[0]);}
else{settings.shipping=[];newShipping();}
if(settings.checkout){$('#cart-checkout-email').val(settings.checkout.email);$('#cart-checkout-currency').val(settings.checkout.currency);}
if(settings.method){$.each(settings.method,function(i,method){var checked=method.available==='1';$('#cart-checkout-method-'+method.name).prop('checked',checked);if(method.name==='credit'){$('#cart-checkout-gateway').val(method.gateway);$('#cart-checkout-gateway-api-key').val(method.gateway_api_key);$('#cart-checkout-credit-fee').val(method.fee);}
else if(method.name==='paypal'){$('#cart-checkout-paypal-fee').val(method.fee);}
else if(method.name==='pickup'){$('#cart-checkout-pickup-fee').val(method.fee);}
else if(method.name==='account'){$('#cart-checkout-account-fee').val(method.fee);}});}});}
function clearCart(){cart={};if(dobrado.localStorage){localStorage.cart=JSON.stringify(cart);}
var method=$('#customer-detail-method').val();if(method==='pickup'||method==='account'){paymentDone();}}
function clearItem(){oldName='';$('#cart-item-name').val('');$('#cart-item-image').val('');$('#cart-item-short').val('');editor.setData('');$('#cart-item-weight').val('');$('#cart-item-price').val('');$('#cart-item-variable').prop('checked',false);$('#cart-item-minimum').val('');$('#cart-item-download').val('');$('#cart-item-tab').val('');$('#cart-item-order').val('');$('#cart-item-available').prop('checked',false);$('.cart-tracking').html('');$('.cart-modified').hide();if(settings.items.length!==0){itemIndex=settings.items.length;$('#cart-item-previous').button('enable');$('#cart-item-next').button('disable');}}
function dialogQuantity(event,ui){var quantity=0;if('value'in ui){quantity=ui.value;}
else{quantity=parseFloat($(this).val());if(quantity<0){quantity=0;$(this).val('0');}}
var name=$('#cart-item-dialog .item-name').text();var price=parseFloat($('#cart-item-dialog .item-price').text());if(cart[name]){if(quantity===0){cart[name].selected=false;}
else{cart[name].selected=true;}
cart[name].price=price;cart[name].quantity=quantity;}
else if(quantity!==0){cart[name]={selected:true,price:price,quantity:quantity,id:'cart-item-id-'+cartIds[name]};}
if(dobrado.localStorage){localStorage.cart=JSON.stringify(cart);}
cartTotal();cartMiniTotal();$('#item-quantity-'+cartIds[name]).val(quantity);}
function displayItem(item){if(item.children('.item-name').length===0){window.location.hash='#';return;}
var name=item.children('.item-name').children('a').text();var price=cartPrice(item.children('.item-price'));var quantity=$('#item-quantity-'+cartIds[name]).val();$.post('/php/request.php',{request:'cart',action:'item',name:name,price:price,quantity:quantity,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart item')){return;}
var dialog=JSON.parse(response);$('#cart-item-dialog').html(dialog.content);$('#cart-item-dialog').dialog('open');$('.main').one('click',function(){$('#cart-item-dialog').dialog('close');});$('#cart-dialog-quantity').spinner({min:0,spin:dialogQuantity,change:dialogQuantity});});}
function nextItem(){if(settings.items.length===0||itemIndex===settings.items.length-1){$('#cart-item-next').button('disable');return;}
if(itemChanged&&!confirm('Discard changes to the current item?')){return;}
itemChanged=false;var item=settings.items[++itemIndex];if(!showUnavailable&&item.available==='0'||(showTab!=='All tabs'&&showTab!==item.tab)){nextItem();}
else{showItem(item);}}
function paymentDone(){$.post('/php/request.php',{request:'cart',action:'paymentDone',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart payment done')){return;}
var payment=JSON.parse(response);location.href=payment.location;});}
function previousItem(){if(itemIndex===0){$('#cart-item-previous').button('disable');return;}
if(itemChanged&&!confirm('Discard changes to the current item?')){return;}
itemChanged=false;var item=settings.items[--itemIndex];if(!showUnavailable&&item.available==='0'||(showTab!=='All tabs'&&showTab!==item.tab)){previousItem();}
else{showItem(item);}}
function removeItem(){if(!confirm('Are you sure you want to remove this item?'))return false;dobrado.log('Removing item','info');$.post('/php/request.php',{request:'cart',action:'removeItem',name:$('#cart-item-name').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart remove item')){return;}
settings.items=JSON.parse(response);clearItem();updateItems();});return false;}
function resetItem(){if(!confirm('Reset all fields from stock list?'))return false;dobrado.log('Resetting item','info');$.post('/php/request.php',{request:'cart',action:'resetItem',name:$('#cart-item-name').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart reset item')){return;}
settings.items=JSON.parse(response);showItem(settings.items[itemIndex]);updateItems();});return false;}
function saveItem(){dobrado.log('Saving item','info');var name=$('#cart-item-name').val();var image=$('#cart-item-image').val();var shortDescription=$('#cart-item-short').val();var fullDescription=editor.getData();var weight=$('#cart-item-weight').val();var price=$('#cart-item-price').val();var variable=$('#cart-item-variable:checked').length.toString();var minimum=$('#cart-item-minimum').val();var download=$('#cart-item-download').val();var itemTab=$('#cart-item-tab').val();var itemOrder=$('#cart-item-order').val();var available=$('#cart-item-available:checked').length.toString();var trackingText='';if(settings.items&&settings.items[itemIndex]){trackingText=settings.items[itemIndex].tracking;}
var tracking=trackingText===''?0:1;$.post('/php/request.php',{request:'cart',action:'saveItem',name:name,oldName:oldName,image:image,short:shortDescription,full:fullDescription,weight:weight,price:price,variable:variable,minimum:minimum,download:download,itemTab:itemTab,itemOrder:itemOrder,available:available,tracking:tracking,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart save item')){return;}
var newItem=true;$.each(settings.items,function(i,item){if(item.name===name||item.name===oldName){settings.items[i]={name:name,image:image,short:shortDescription,full:fullDescription,weight:weight,price:price,variable:variable,minimum:minimum,download:download,tab:itemTab,item_order:itemOrder,available:available,tracking:trackingText};newItem=false;return false;}});if(newItem){settings.items.push({name:name,image:image,short:shortDescription,full:fullDescription,weight:weight,price:price,variable:variable,minimum:minimum,download:download,tab:itemTab,item_order:itemOrder,available:available,tracking:trackingText});itemIndex=settings.items.length-1;showItem(settings.items[itemIndex]);updateItems();}});itemChanged=false;return false;}
function showItem(item){if(!item){return;}
oldName=dobrado.decode(item.name);$('#cart-item-name').val(oldName);$('#cart-item-image').val(dobrado.decode(item.image));$('#cart-item-short').val(dobrado.decode(item.short));editor.setData(item.full);$('#cart-item-weight').val(item.weight);$('#cart-item-price').val(item.price);if(item.tracking){$('#cart-item-price').attr('readonly',true);$('.cart-tracking').html(item.tracking);if(item.modified){$('.cart-modified').show();}
else{$('.cart-modified').hide();}}
else{$('#cart-item-price').attr('readonly',false);$('.cart-tracking').html('');$('.cart-modified').hide();}
if(item.variable==='1'){$('#cart-item-variable').prop('checked',true);$('.show-item-minimum').show();}
else{$('#cart-item-variable').prop('checked',false);$('.show-item-minimum').hide();}
$('#cart-item-minimum').val(item.minimum);$('#cart-item-download').val(dobrado.decode(item.download));$('#cart-item-tab').val(dobrado.decode(item.tab));$('#cart-item-order').val((item.item_order));if(item.available==='1'){$('#cart-item-available').prop('checked',true);}
else{$('#cart-item-available').prop('checked',false);}
if(settings.items.length===0||itemIndex===0){$('#cart-item-previous').button('disable');}
else{$('#cart-item-previous').button('enable');}
if(settings.items.length===0||itemIndex===settings.items.length-1){$('#cart-item-next').button('disable');}
else{$('#cart-item-next').button('enable');}
itemChanged=false;}
function saveShipping(){dobrado.log('Saving shipping','info');var id=settings.shipping[shippingIndex].id;var type=$('#cart-shipping-type').val();var destinationName=$('#cart-shipping-destination-name').val();var destinationCode=$('#cart-shipping-destination-code').val();var amount=$('#cart-shipping-amount').val();var minimum=$('#cart-shipping-minimum').val();var maximum=$('#cart-shipping-maximum').val();var rule=$('#cart-shipping-rule').val();$.post('/php/request.php',{request:'cart',action:'saveShipping',id:id,type:type,destinationName:destinationName,destinationCode:destinationCode,amount:amount,minimum:minimum,maximum:maximum,rule:rule,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart save shipping')){return;}
settings.shipping[shippingIndex]={id:id,type:type,destination_name:destinationName,destination_code:destinationCode,amount:amount,minimum:minimum,maximum:maximum,rule:rule};});return false;}
function removeShipping(){dobrado.log('Removing shipping','info');var id=settings.shipping[shippingIndex].id;$.post('/php/request.php',{request:'cart',action:'removeShipping',id:id,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart remove shipping')){return;}
settings.shipping=JSON.parse(response);newShipping();});return false;}
function showShipping(shipping){$('#cart-shipping-type').val(dobrado.decode(shipping.type));$('#cart-shipping-destination-name').val(dobrado.decode(shipping.destination_name));$('#cart-shipping-destination-code').val(dobrado.decode(shipping.destination_code));$('#cart-shipping-amount').val(shipping.amount);$('#cart-shipping-minimum').val(shipping.minimum);$('#cart-shipping-maximum').val(shipping.maximum);$('#cart-shipping-rule').val(shipping.rule);if(settings.shipping.length===0||shippingIndex===0){$('#cart-shipping-previous').button('disable');}
else{$('#cart-shipping-previous').button('enable');}
if(settings.shipping.length===0||shippingIndex===settings.shipping.length-1){$('#cart-shipping-next').button('disable');}
else{$('#cart-shipping-next').button('enable');}}
function newShipping(){if(settings.shipping.length===0||settings.shipping[settings.shipping.length-1].type!==''){settings.shipping.push({id:settings.shipping.length,type:'',destination_name:'',destination_code:'',amount:'',minimum:'',maximum:'',rule:'weight'});}
shippingIndex=settings.shipping.length-1;showShipping(settings.shipping[shippingIndex]);}
function previousShipping(){showShipping(settings.shipping[--shippingIndex]);}
function nextShipping(){showShipping(settings.shipping[++shippingIndex]);}
function saveCheckout(){dobrado.log('Saving checkout','info');$.post('/php/request.php',{request:'cart',action:'saveCheckout',email:$('#cart-checkout-email').val(),currency:$('#cart-checkout-currency').val(),credit:$('#cart-checkout-method-credit:checked').length,gateway:$('#cart-checkout-gateway').val(),gatewayApiKey:$('#cart-checkout-gateway-api-key').val(),gatewayPassword:$('#cart-checkout-gateway-password').val(),creditFee:$('#cart-checkout-credit-fee').val(),paypal:$('#cart-checkout-method-paypal:checked').length,paypalFee:$('#cart-checkout-paypal-fee').val(),pickup:$('#cart-checkout-method-pickup:checked').length,pickupFee:$('#cart-checkout-pickup-fee').val(),account:$('#cart-checkout-method-account:checked').length,accountFee:$('#cart-checkout-account-fee').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'cart save checkout')){return;}});return false;}
function sortGroup(){if($(this).prop('checked')){$(this).parents('.cart-item').addClass('group');}
else{$(this).parents('.cart-item').removeClass('group');}}
function toggleCustomInput(){if($('#customer-detail-method').val()==='pickup'){$('#customer-detail-description').parent().show();}
else{$('#customer-detail-description').parent().hide();}}
function updateItems(){function autoCompleteItem(event,ui){$.each(settings.items,function(i,item){if(ui.item.value===item.name){showItem(item);return false;}});}
var items=[];$.each(settings.items,function(i,item){items.push(item.name);});$('#cart-item-name').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:items,select:autoCompleteItem});}
dobrado.cart.select=function(filename){$(browserTarget).val(filename);};dobrado.cart.sort=function(editMode){function startMove(event,ui){position=ui.item.prevAll().length;}
function updatePosition(event,ui){var dragged=ui.item.children('.item-name').text();var names=[dragged];$('.cart-item.group').each(function(){var itemName=$(this).children('.item-name').text();if(itemName!==dragged){names.push(itemName);}});$.post('/php/request.php',{request:'cart',action:'updatePosition',names:JSON.stringify(names),oldPosition:position,newPosition:ui.item.prevAll().length,url:location.href,token:dobrado.token},function(response){dobrado.checkResponseError(response,'cart update position');if(names.length>1){location.reload();}});}
cartEditMode=editMode;var position=0;if($('#cart-editor').length===1){if(editMode){$('.cart-item.hidden').css('display',cartItemDisplay);$('.cart-item').addClass('border');$('.cart-item .item-quantity-wrapper').hide();$('.cart-item .item-group-wrapper').show();$('#cart-tabs > div').sortable({start:startMove,update:updatePosition});}
else{$('.cart-item.hidden').css('display','none');$('.cart-item').removeClass('border');$('.cart-item .item-quantity-wrapper').show();$('.cart-item .item-group').prop('checked',false);$('.cart-item .item-group-wrapper').hide();$('#cart-tabs > div').sortable('destroy');}}};}());