<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Post extends Base {

  public function Add($id) {
    // Add default content here so that SetContent doesn't need to check if
    // a table row already exists. This also updates paging for the feed.
    $this->Insert($id, $this->user->name, $this->user->page);
  }

  public function Callback() {
    // This is called after SetContent and NewComment return.
    if (isset($_POST['action'])) {
      $updated = false;
      if ($_POST['action'] === 'updated') {
        $updated = true;
      }
      else if ($_POST['action'] !== 'newComment') {
        return ['error' => 'Unknown action'];
      }

      $status = '';
      $key = $this->owner . '-' . $this->user->page;
      if (isset($_SESSION['post-update'][$key])) {
        $post = $_SESSION['post-update'][$key];
        if ($post['published'] === 1) {
          $status = $this->ContentUpdated($post['box_id'], $post['content'],
                                          $post['category'], $post['feed'],
                                          $post['permalink'], $updated);
        }
        $_SESSION['post-update'][$key] = NULL;
      }
      else if (isset($_SESSION['micropub-status'])) {
        $status = $_SESSION['micropub-status'];
        $_SESSION['micropub-status'] = NULL;
      }
      return ['status' => $status];
    }
    // 'mode' is used by the Extended module, which calls this function.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      return $this->CustomSettings();
    }
    return ['error' => 'Unknown mode'];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function CanEdit($id) {
    return true;
  }

  public function Content($id) {
    // If the reader module is on the page, it is responsible for aggregating
    // post module content, so return false here.
    if ($this->AlreadyOnPage('reader')) return false;

    $title = '';
    $description = '';
    $author = '';
    $repost = '';
    $category = '';
    $media = '';
    $permalink = '';
    $feed = '';
    $timestamp = 0;
    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $this->user->config->ServerName();
    $mysqli = connect_db();
    $query = 'SELECT title, description, author, category, enclosure, ' .
      'permalink, feed, timestamp FROM post WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        // Allow the title to be stored with literal special chars as this
        // simplifies creating permalinks, but it must be escaped here.
        $title = htmlspecialchars($post['title']);
        $description = $post['description'];
        $enclosure = json_decode($post['enclosure'], true);
        $count = is_array($enclosure) ? count($enclosure) : 0;
        if ($count === 1) {
          $media .= '<p><img class="u-photo" src="' . $enclosure[0] . '"></p>';
        }
        else if ($count > 1) {
          $image_set_id = preg_replace('/[[:^alnum:]]/', '', $enclosure[0]);
          $media .= '<p class="photo-list">';
          for ($i = 0; $i < $count; $i++) {
            $hidden = $i <= 1 ? '' : 'class="hidden" ';
            $media .= '<a href="' . $enclosure[$i] . '" ' . $hidden .
              'data-lightbox="' . $image_set_id . '">' .
              '<img class="u-photo" src="' . $enclosure[$i] . '"></a>';
          }
          $media .= '<br><b>' . $count . ' photos</b></p>';
        }
        // The author is escaped in SetContent.
        $author = $post['author'];
        $prev_tag = '';
        // Category can be comma separated values.
        if (strpos($post['category'], ',') !== false) {
          foreach (explode(',', $post['category']) as $tag) {
            if ($prev_tag === 'Like of') {
              $category .= category_markup(htmlspecialchars($tag), 'like-of',
                                           $this->LookupName($tag));
            }
            else if (preg_match('/^reposted by (.+)$/', $prev_tag, $match)) {
              // Reposted items get their original author, which is stored in
              // the nickname cache when the repost is created.
              $repost = $this->LookupUser($match[1], $base_url);
              $author = $this->LookupAuthor($author, $tag) .
                ' <span class="post-reposted-by">reposted by ' .
                $repost . '</span>';
              $category .= category_markup(htmlspecialchars($tag), 'repost-of');
            }
            else if ($prev_tag === 'In reply to') {
              $category .= category_markup(htmlspecialchars($tag),
                                           'in-reply-to',
                                           $this->LookupName($tag));
            }
            else {
              $category .= category_markup(htmlspecialchars($tag), '',
                                           $this->LookupName($tag));
            }
            $prev_tag = $tag;
          }
        }
        else {
          $category .= category_markup(htmlspecialchars($post['category']), '',
                                       $this->LookupName($post['category']));
        }
        if ($category !== '') {
          $category = '<span class="post-tag-label ui-icon ui-icon-tag" ' .
            'title="tags"></span> ' . $category;
        }
        $permalink = $post['permalink'];
        $feed = $post['feed'];
        $timestamp = (int)$post['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Content 1: ' . $mysqli->error);
    }

    $permalink_url = $this->Url('', $permalink);
    $permalink_page = $this->user->page === $permalink;
    $formatted_date = date('j F Y, g:ia', $timestamp);
    $atom_date = date(DATE_ATOM, $timestamp);
    // Check if this is a tombstone post.
    if ($description === $this->Tombstone()) {
      if ($permalink_page) {
        header('HTTP/1.1 410 Gone');
        return '<article class="h-entry">' .
            '<div class="published">' .
              '<time class="dt-deleted" datetime="' . $atom_date . '">' .
                '<a href="' . $permalink_url . '" class="u-url">' .
                $formatted_date . '</a>' .
              '</time></div>' .
            '<div class="p-name e-content">' . $description . '</div>' .
          '</article>';
      }
      else {
        // Show an empty h-entry on the feed page so that readers can remove
        // cached entries.
        return '<article class="h-entry">' .
          '<time class="dt-deleted" datetime="' . $atom_date . '">' .
            '<a href="' . $permalink_url . '" class="u-url"></a>' .
          '</time></article>';
      }
    }

    // When creating a new post, the writer module can designate a different
    // page, so only return content when on this post's feed or permalink page.
    if (!$permalink_page &&
        !preg_match('/^' . $feed . '(-p[0-9]+)?$/', $this->user->page)) {
      return false;
    }

    // Link to previous and next posts on permalink pages.
    $navigation = '';
    if ($permalink_page) {
      // When a post is copied it keeps the same permalink, so don't show
      // duplicate posts in this case.
      $duplicate = false;
      $query = 'SELECT box_id FROM post WHERE user = "' . $this->owner . '" ' .
        'AND permalink = "' . $permalink . '" ORDER BY box_id LIMIT 1';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($post = $mysqli_result->fetch_assoc()) {
          $duplicate = (int)$post['box_id'] !== $id;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->Content 2: ' . $mysqli->error);
      }
      if ($duplicate) {
        $mysqli->close();
        return false;
      }

      // Show the webmention endpoint on permalink pages.
      header('Link: <' . $base_url . '/php/webmention.php>; rel="webmention"');
      // Also specify that this is the canonical version of the content.
      header('Link: <' . $base_url . $permalink_url . '>; rel="canonical"',
             false);
      $navigation = $this->Navigation($feed, $timestamp);
    }
    $mysqli->close();

    // If title and description are both empty, assume this post module
    // should not be shown yet and return false so that it's hidden.
    if ($title === '' && $description === '') return false;

    if ($author === '') $author = $this->owner;
    $author = $this->LookupUser($author, $base_url);
    // Permalink page requires h-entry class on the parent group div so that
    // comments can be nested. This is handled by the Page class.
    $post_format = $permalink_page ? 'permalink-' : '';
    if ($title === '') {
      $post_format .= $repost === '' ? 'post-no-title' : 'repost-no-title';
    }
    else {
      $post_format .= $repost === '' ?
        'post-with-title' : 'repost-with-title';
      $title = '<h2 class="title p-name">' . $title . '</h2>';
    }
    // Don't add u-url to reposts as that is handled by category_markup.
    $url_class = $repost === '' ? ' class="u-url"' : '';
    $date = '<time class="dt-published" datetime="' . $atom_date . '">' .
      '<a href="' . $permalink_url . '" ' . $url_class . '>' . $formatted_date .
      '</a></time>';
    $patterns = ['/!title/', '/!description/', '/!author/', '/!category/',
                 '/!media/', '/!date/', '/!navigation/'];
    $replacements = [$title, $description, $author, $category, $media, $date,
                     $navigation];
    if ($repost !== '') {
      $patterns[] = '/!repost/';
      $replacements[] = $repost;
      // Also need the permalink for the post, which is only provided above
      // after formatting so add it again here.
      $patterns[] = '/!permalink/';
      $replacements[] = $permalink_url;
    }
    $content = $this->Substitute($post_format, $patterns, $replacements);
    if ($this->Substitute('post-web-actions') === 'true') {
      // Add the base url to the permalink url for web actions.
      $permalink_url = $base_url . $permalink_url;
      $content .= '<div class="post-web-actions">' .
        '<indie-action do="like" with="' . $permalink_url . '">' .
          '<a href="#" class="action">' .
            '<span class="ui-icon ui-icon-star"></span>like</a>' .
        '</indie-action>' .
        '<indie-action do="repost" with="' . $permalink_url . '">' .
          '<a href="#" class="action">' .
            '<span class="ui-icon ui-icon-refresh"></span>share</a>' .
        '</indie-action>' .
        '<indie-action do="reply" with="' . $permalink_url . '">' .
          '<a href="#" class="action">' .
            '<span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>' .
            'reply</a>' .
        '</indie-action>' .
        '<span class="indie-config-info">' .
          'Want to share this? Click to choose a site:</span>' .
        '<a href="#" class="indie-config" title="web action settings">' .
          '<span class="ui-icon ui-icon-gear"></span>settings</a></div>';
    }
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $title = '';
    $description = '';
    $category = '';
    $enclosure = '';
    $permalink = '';
    $timestamp = 0;
    $mysqli = connect_db();
    $query = 'SELECT title, description, author, category, enclosure, ' .
      'permalink, timestamp FROM post WHERE user = "' . $old_owner . '" AND ' .
      'box_id = ' . $old_id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $title = $mysqli->escape_string($post['title']);
        $description = $mysqli->escape_string($post['description']);
        $author = $mysqli->escape_string($post['author']);
        $category = $mysqli->escape_string($post['category']);
        $enclosure = $mysqli->escape_string($post['enclosure']);
        $permalink = $mysqli->escape_string($post['permalink']);
        $timestamp = (int)$post['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Copy 1: ' . $mysqli->error);
    }
    // Point the new module at the existing permalink page.
    $query = 'INSERT INTO modules VALUES ("' . $this->owner . '", ' .
      '"' . $permalink . '", ' . $id . ', "post", "' . $this->Group() . '", ' .
      $this->user->config->PermalinkOrder() . ', ' .
      '"' . $this->Placement() . '", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Copy 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $this->Insert($id, $author, $new_page, $title, $description, $category,
                  $enclosure, $permalink, $timestamp, true);
    $this->CopyStyle($id, $old_owner, $old_id);
    $this->Notify($id, 'post', 'feed', $new_page, $this->Published($new_page));
  }

  public function Cron() {
    set_time_limit(0);
    $mysqli = connect_db();
    // Published scheduled draft posts.
    $publish = [];
    $query = 'SELECT user, box_id FROM post_draft WHERE timestamp != 0 AND ' .
      'timestamp <= ' . time();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($post_draft = $mysqli_result->fetch_assoc()) {
        $publish[] = $post_draft;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Cron 1: ' . $mysqli->error);
    }
    if (count($publish) > 0) {
      $current_owner = $this->owner;
      foreach ($publish as $draft) {
        $this->owner = $draft['user'];
        $this->PublishDraft($draft['box_id']);
      }
      $this->owner = $current_owner;
    }

    // Process queued webmentions.
    $receive = [];
    $send = [];
    $query = 'SELECT source, target FROM post_queue WHERE source != ""';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($post_queue = $mysqli_result->fetch_assoc()) {
        $receive[] = $post_queue;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Cron 2: ' . $mysqli->error);
    }
    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $this->user->config->ServerName();
    foreach ($receive as $webmention) {
      $us_source = $webmention['source'];
      $us_target = $webmention['target'];
      $full_target = $us_target;
      if (stripos($full_target, 'http') !== 0) {
        $full_target = $base_url . $us_target;
      }
      list($this->user->page, $this->owner) = page_owner($full_target);
      $result = $this->ProcessReceivedWebmention($us_source, $us_target);
      // The third result parameter indicates if a comment was updated,
      // in which case webmentions are resent for all links in the post.
      if ($result[2]) $send[] = $us_target;
    }
    $already_sent = [];
    foreach ($send as $us_target) {
      if (!in_array($us_target, $already_sent)) {
        $full_target = $us_target;
        if (stripos($full_target, 'http') !== 0) {
          $full_target = $base_url . $us_target;
        }
        list($this->user->page, $this->owner) = page_owner($full_target);
        if ($this->IsPermalink()) {
          $query = 'SELECT box_id, description, category, feed FROM post ' .
            'WHERE user = "' . $this->owner . '" AND ' .
            'permalink = "' . $this->user->page . '"';
          if ($mysqli_result = $mysqli->query($query)) {
            if ($post = $mysqli_result->fetch_assoc()) {
              $this->ContentUpdated($post['box_id'], $post['description'],
                                    $post['category'], $post['feed'],
                                    $this->user->page, false);
            }
            $mysqli_result->close();
          }
          else {
            $this->Log('Post->Cron 3: ' . $mysqli->error);
          }
        }
        $already_sent[] = $us_target;
      }
    }
    $query = 'DELETE FROM post_queue';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Cron 4: ' . $mysqli->error);
    }

    // Tombstone posts are removed from the feed here after a week, which
    // should've given readers time to update their cache.
    $tombstone = $mysqli->escape_string($this->Tombstone());
    $query = 'UPDATE modules LEFT JOIN post ON modules.box_id = post.box_id ' .
      'SET deleted = 1 WHERE description = "' . $tombstone . '" ' .
      'AND timestamp < ' . strtotime('-7 days');
    if (!$mysqli->query($query)) {
      $this->Log('Post->Cron 5: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'ReceiveWebmention' && $count === 2) {
        $us_source = $p[0];
        $us_target = $p[1];
        return $this->ReceiveWebmention($us_source, $us_target);
      }
      if ($fn === 'ContentUpdated' && $count === 5) {
        $id = $p[0];
        $data = $p[1];
        $category = $p[2];
        $feed = $p[3];
        $permalink = $p[4];
        return $this->ContentUpdated($id, $data, $category, $feed, $permalink);
      }
      if ($fn === 'Status' && $count === 2) {
        $target = $p[0];
        $me = $p[1];
        return $this->Status($target, $me);
      }
      return;
    }
    if ($fn === 'Syndicated') {
      return $this->Syndicated($p);
    }
    if ($fn === 'NewComment') {
      return $this->NewComment();
    }
  }

  public function Group() {
    return 'post-comment';
  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS post (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'title VARCHAR(180),' .
      'description TEXT,' .
      'author VARCHAR(50),' .
      'category VARCHAR(200),' .
      'enclosure TEXT,' .
      'permalink VARCHAR(200),' .
      'feed VARCHAR(200),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS post_history (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'title VARCHAR(180),' .
      'description TEXT,' .
      'author VARCHAR(50),' .
      'category VARCHAR(200),' .
      'enclosure TEXT,' .
      'permalink VARCHAR(200),' .
      'feed VARCHAR(200),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'modified_by VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(user, box_id, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Install 2: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS post_queue (' .
      'source VARCHAR(200),' .
      'target VARCHAR(200),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(source(100), target(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Install 3: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS post_draft (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Install 4: ' . $mysqli->error);
    }

    $site_style = ['"",".post","padding","20px"',
                   '"",".post .published","font-size","1.2em"',
                   '"",".post .published","line-height","2em"',
                   '"",".post .published time","float","right"',
                   '"",".post .published .thumb","width","30px"',
                   '"",".post .published .thumb","border-radius","2px"',
                   '"",".post .published .thumb","float","left"',
                   '"",".post .published .thumb","margin-right","5px"',
                   '"",".post .media img","box-shadow","5px 5px 5px #aaaaaa"',
                   '"",".post .options > *","padding","5px"',
                   '"",".post .dobrado-editable","min-height","0"',
                   '"",".post .dobrado-editable","padding","5px"',
                   '"",".post .navigation .next","float","right"',
                   '"",".post-reposted-by","font-size","0.8em"',
                   '"",".post-reposted-by .thumb","display","none"',
                   '"",".post-web-actions","display","flex"',
                   '"",".post-web-actions","padding-top","10px"',
                   '"",".post-web-actions a","padding","10px"',
                   '"",".post-web-actions indie-action","flex-grow","1"',
                   '"",".post-web-actions indie-action a.highlight",' .
                     '"padding","3px"',
                   '"",".post-web-actions indie-action a.highlight",' .
                     '"border-radius","3px"'];
    $this->AddSiteStyle($site_style);

    // The two formats are required because p-name needs to be either on the
    // title or on the same div as e-content.
    $post_no_title = '<article class="h-entry">' .
      '<div class="published">' .
        '!date<span class="h-card p-author">!author</span></div>' .
      '<div class="dobrado-editable p-name e-content">!description</div>' .
      '<div class="media">!media</div><div class="options">!category</div>' .
      '!navigation</article>';
    $post_with_title = '<article class="h-entry">' .
      '!title<div class="published">' .
        '!date<span class="h-card p-author">!author</span></div>' .
      '<div class="dobrado-editable e-content">!description</div>' .
      '<div class="media">!media</div><div class="options">!category</div>' .
      '!navigation</article>';
    // The h-entry class is added to a parent element on the permalink page.
    // The u-comment markup is required on permalink pages to differentiate a
    // microformats comments feed from a single h-entry in a feed.
    $permalink_post_no_title = '<article>' .
      '<div class="published">' .
        '!date<span class="h-card p-author">!author</span></div>' .
      '<div class="dobrado-editable p-name e-content">!description</div>' .
      '<div class="media">!media</div><div class="options">!category</div>' .
      '!navigation</article><div class="u-comment h-entry"></div>';
    $permalink_post_with_title = '<article>' .
      '!title<div class="published">' .
        '!date<span class="h-card p-author">!author</span></div>' .
      '<div class="dobrado-editable e-content">!description</div>' .
      '<div class="media">!media</div><div class="options">!category</div>' .
      '!navigation</article><div class="u-comment h-entry"></div>';
    // Reposts need to be marked up using h-cite so need separate templates for
    // all four of the above cases.
    $repost_no_title = '<article class="h-entry">' .
      '<div class="h-cite u-repost-of">' .
        '<div class="published">' .
          '!date<span class="h-card p-author">!author</span></div>' .
        '<div class="dobrado-editable p-name e-content">!description</div>' .
        '<div class="media">!media</div><div class="options">!category</div>' .
      '</div>' .
      '<div class="hidden">' .
        '<span class="h-card p-author">!repost</span>' .
        '<a class="u-url" href="!permalink"></a></div>' .
      '!navigation</article>';
    $repost_with_title = '<article class="h-entry">' .
      '<div class="h-cite u-repost-of">' .
        '!title<div class="published">' .
          '!date<span class="h-card p-author">!author</span></div>' .
        '<div class="dobrado-editable e-content">!description</div>' .
        '<div class="media">!media</div><div class="options">!category</div>' .
      '</div>' .
      '<div class="hidden">' .
        '<span class="h-card p-author">!repost</span>' .
        '<a class="u-url" href="!permalink"></a></div>' .
      '!navigation</article>';
    $permalink_repost_no_title = '<article>' .
      '<div class="h-cite u-repost-of">' .
        '<div class="published">' .
          '!date<span class="h-card p-author">!author</span></div>' .
        '<div class="dobrado-editable p-name e-content">!description</div>' .
        '<div class="media">!media</div><div class="options">!category</div>' .
      '</div>' .
      '<div class="hidden">' .
        '<span class="h-card p-author">!repost</span>' .
        '<a class="u-url" href="!permalink"></a></div>' .
      '!navigation</article><div class="u-comment h-entry"></div>';
    $permalink_repost_with_title = '<article>' .
      '<div class="h-cite u-repost-of">' .
        '!title<div class="published">' .
          '!date<span class="h-card p-author">!author</span></div>' .
        '<div class="dobrado-editable e-content">!description</div>' .
        '<div class="media">!media</div><div class="options">!category</div>' .
      '</div>' .
      '<div class="hidden">' .
        '<span class="h-card p-author">!repost</span>' .
        '<a class="u-url" href="!permalink"></a></div>' .
      '!navigation</article><div class="u-comment h-entry"></div>';
    $template =
      ['"post-no-title", "", "' . $mysqli->escape_string($post_no_title) . '"',
       '"post-with-title", "", ' .
         '"' . $mysqli->escape_string($post_with_title) . '"',
       '"permalink-post-no-title", "", ' .
         '"' . $mysqli->escape_string($permalink_post_no_title) . '"',
       '"permalink-post-with-title", "", ' .
         '"' . $mysqli->escape_string($permalink_post_with_title) . '"',
       '"repost-no-title", "", ' .
         '"' . $mysqli->escape_string($repost_no_title) . '"',
       '"repost-with-title", "", ' .
         '"' . $mysqli->escape_string($repost_with_title) . '"',
       '"permalink-repost-no-title", "", ' .
         '"' . $mysqli->escape_string($permalink_repost_no_title) . '"',
       '"permalink-repost-with-title", "", ' .
         '"' . $mysqli->escape_string($permalink_repost_with_title) . '"',
       '"post-web-actions", "", "true"',
       '"post-webmention-debug", "", "true"'];
    $mysqli->close();

    $this->AddTemplate($template);
    $description = ['post-no-title' => 'The template for a blog post with no ' .
                      'title on the feed page, substitutes: !date, !author, ' .
                      '!description, !media, !category and !navigation.',
                    'post-with-title' => 'The template for a blog post with ' .
                      'a title on the feed page, substitutes: !title, !date, ' .
                      '!author, !description, !media, !category and ' .
                      '!navigation.',
                    'permalink-post-no-title' => 'The template for a blog ' .
                      'post with no title on the permalink page, ' .
                      'substitutes: !date, !author, !description, !media, ' .
                      '!category and !navigation.',
                    'permalink-post-with-title' => 'The template for a blog ' .
                      'post with a title on the permalink page, substitutes: ' .
                      '!title, !date, !author, !description, !media, ' .
                      '!category and !navigation.',
                    'repost-no-title' => 'The template for a repost with no ' .
                      'title on the feed page, substitutes: !date, !author, ' .
                      '!description, !media and !category from the original ' .
                      'post and !repost (author), !permalink and !navigation ' .
                      'from the new post.',
                    'repost-with-title' => 'The template for a repost with ' .
                      'a title on the feed page, substitutes: !title, !date, ' .
                      '!author, !description, !media and !category from the ' .
                      'original post and !repost (author), !permalink and ' .
                      '!navigation from the new post.',
                    'permalink-repost-no-title' => 'The template for a ' .
                      'repost with no title on the permalink page, ' .
                      'substitutes: !date, !author, !description, !media and ' .
                      '!category from the original post and !repost (author),' .
                      ' !permalink and !navigation from the new post.',
                    'permalink-repost-with-title' => 'The template for a ' .
                      'repost with a title on the permalink page, ' .
                      'substitutes: !title, !date, !author, !description, ' .
                      '!media and !category from the original post and ' .
                      '!repost (author), !permalink and !navigation from the ' .
                      'new post.',
                    'post-web-actions' => 'true or false to display ' .
                      'indie-action tags on posts.',
                    'post-webmention-debug' => 'true or false to log debug ' .
                      'information when sending and receiving webmentions.'];
    $this->AddTemplateDescription($description);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {
    if (isset($id)) {
      $this->Notify($id, 'post', 'feed', $this->user->page, $update);
    }
    else {
      // When $id is not set the page has been made private. Remove users from
      // any post notifications groups if they don't have access permission.
      $this->RemoveNotification('post', $this->user->page);
    }
  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $feed = '';
      // Copies of this post can point to the same permalink page, so only
      // remove the permalink page when there's one reference left. (Note that
      // posts are tombstoned rather than deleted from the database, so need to
      // skip those when looking for copies to count.)
      $tombstone = $mysqli->escape_string($this->Tombstone());
      $query = 'SELECT permalink, feed FROM post WHERE ' .
        'user = "' . $this->owner . '" AND ' .
        'description != "' . $tombstone . '" AND ' .
        'permalink = (SELECT permalink FROM post WHERE ' .
          'user = "' . $this->owner . '" AND box_id = ' . $id . ')';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($post = $mysqli_result->fetch_assoc()) {
          if ($mysqli_result->num_rows === 1) {
            // Store the fee to call the reader module below.
            $feed = $post['feed'];
            $this->RemovePermalinkPage($id);
          }
          else {
            $permalink = $post['permalink'];
            // Otherwise just set this module as deleted on the permalink page.
            $query = 'UPDATE modules SET deleted = 1 WHERE ' .
              'box_id = ' . $id . ' AND page = "' . $permalink . '" AND ' .
              'user = "' . $this->owner . '"';
            if (!$mysqli->query($query)) {
              $this->Log('Post->Remove 1: ' . $mysqli->error);
            }
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->Remove 2: ' . $mysqli->error);
      }
      $this->RemoveNotify($id);
      $this->UpdatePager($id);
      $query = 'UPDATE post SET title = "", ' .
        'description = "' . $tombstone . '", author = "", category = "", ' .
        'enclosure = "" WHERE user = "' . $this->owner . '" AND ' .
        'box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Post->Remove 3: ' . $mysqli->error);
      }
      $query = 'DELETE FROM post_draft WHERE user = "' . $this->owner . '" ' .
        'AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Post->Remove 4: ' . $mysqli->error);
      }
      if ($feed !== '') {
        // Update the feed, which will also notify subscribers.
        $reader = new Module($this->user, $this->owner, 'reader');
        if ($reader->IsInstalled()) {
          $url = $this->user->config->Secure() ? 'https://' : 'http://';
          $url .= $this->user->config->ServerName();
          $page = 'rss/index.php?page=' . $feed;
          $path = $this->owner === 'admin' ? $page : $this->owner . '/' . $page;
          $reader->Factory('UpdateFeed', $url . '/' . $path);
          // Also update the microformats feed and provide the post content as
          // the payload for the update. Content needs the current page to be
          // set to the feed (or permalink) page to return any content.
          $current_page = $this->user->page;
          $this->user->page = $feed;
          $data = $this->Content($id);
          $this->user->page = $current_page;
          $reader->Factory('UpdateFeed', [$url . $this->Url('', $feed), $data]);
        }
      }
    }
    else {
      $query = 'DELETE FROM post WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Remove 5: ' . $mysqli->error);
      }
      $query = 'DELETE FROM post_history WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Remove 6: ' . $mysqli->error);
      }
      $query = 'DELETE FROM post_draft WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Remove 7: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    $micropub = isset($this->user->settings['micropub']['endpoint']);
    $us_data = $micropub ? ['type' => ['h-entry'], 'properties' => []] : '';
    if (isset($us_content['data']) && $us_content['data'] !== '') {
      if ($this->owner === 'admin') {
        if ($micropub) {
          $us_data['properties']['content'] = [['html' => $us_content['data']]];
        }
        else {
          $us_data .= $us_content['data'];
        }
      }
      else {
        include_once 'library/HTMLPurifier.auto.php';
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Attr.EnableID', true);
        $config->set('Attr.IDPrefix', 'anchor-');
        // Allow iframes from youtube and vimeo.
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp',
                     '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|' .
                     'player\.vimeo\.com/video/)%');
        $config->set('HTML.DefinitionID', 'dobrado-post');
        $config->set('HTML.DefinitionRev', 1);
        if ($def = $config->maybeGetRawHTMLDefinition()) {
          $def->addElement('data', 'Inline', 'Inline', 'Common');
          $def->addAttribute('data', 'value', 'CDATA');
        }
        $purifier = new HTMLPurifier($config);
        if ($micropub) {
          $us_data['properties']['content'] =
            [['html' => $purifier->purify($us_content['data'])]];
        }
        else {
          $us_data .= $purifier->purify($us_content['data']);
        }
      }
    }
    $us_title = isset($us_content['title']) ? $us_content['title'] : '';
    $us_category = isset($us_content['category']) ?
      $us_content['category'] : '';
    // If the user has chosen to send the post to twitter, add a link in the
    // post to brid.gy to enable webmentions.
    if (isset($us_content['twitter']) && (int)$us_content['twitter'] === 1) {
      if ($micropub) {
        $us_data['properties']['mp-syndicate-to'] = 'twitter';
      }
      else {
        $twitter_link = 'https://brid.gy/publish/twitter';
        if (strpos($us_data, $twitter_link) === false) {
          // A link back to the user's site is omitted when the post is less
          // than 280 characters and it doesn't have a title (when a title is
          // given, only that and the link back are shown). For quote-tweets
          // need to check content up to <cite> tag.
          $us_check = strstr($us_data, '<cite', true);
          if (!$us_check) $us_check = $us_data;
          if (strlen(strip_tags($us_check)) < 280 && $us_title === '') {
            $twitter_link .= '?bridgy_omit_link=true';
          }
          $us_data .= '<a href="' . $twitter_link . '"></a>';
        }
      }
    }
    $us_enclosure = '';
    if (isset($us_content['enclosure'])) {
      if ($micropub) {
        $us_data['properties']['photo'] = $us_content['enclosure'];
      }
      else {
        $us_enclosure = json_encode($us_content['enclosure']);
      }
    }
    $us_author = $this->user->name;
    if (isset($us_content['author'])) {
      $us_author = htmlspecialchars(strip_tags($us_content['author']));
    }
    $action = '';
    // If a webaction is set, make sure it's marked up correctly.
    if (isset($us_content['webactionType']) &&
        isset($us_content['webactionUrl'])) {
      $us_url = htmlspecialchars($us_content['webactionUrl']);
      $us_type = $us_url === '' ? '' : $us_content['webactionType'];
      if ($us_type === 'like') {
        $action = 'like-of';
        if (!$micropub) {
          $like_of = ',Like of,';
          if (strpos($us_category, $like_of) === false) {
            $us_category .= $like_of . $us_url;
          }
          // Prevent creating an empty post below.
          if ($us_data === '') {
            $us_data = '<a href="' . $us_url . '">' . $us_url . '</a>';
          }
        }
      }
      else if ($us_type === 'share') {
        $action = 'repost-of';
        if (!$micropub) {
          // The author is kept as that of the original post, so show the user
          // who is sharing it as a category. Make sure it's not already added.
          $reposted_by = ',reposted by ' . $this->user->name . ',';
          if (strpos($us_category, $reposted_by) === false) {
            $us_category .= $reposted_by . $us_url;
          }
          if ($us_data === '') {
            $us_data = '<a href="' . $us_url . '">' . $us_url . '</a>';
          }
        }
      }
      else if ($us_type === 'reply' || $us_type === 'reply to') {
        $action = 'in-reply-to';
        if (!$micropub) {
          // Add the url as a category for our permalink post so that it doesn't
          // need to be visible in the content of the comment.
          $in_reply_to = ',In reply to,';
          if (strpos($us_category, $in_reply_to) === false) {
            $us_category .= $in_reply_to . $us_url;
          }
          if ($us_data === '') {
            $us_data = '<a href="' . $us_url . '">' . $us_url . '</a>';
          }
        }
      }
      // Store the author in the nickname cache so their details can be used
      // when Content is called. The reader module is used because if the url
      // is not already in the nickname cache it's fetched using microsub.
      $reader = new Module($this->user, $this->owner, 'reader');
      if ($reader->IsInstalled()) {
        $reader->Factory('Nickname', [$us_url, $us_author]);
      }
    }
    if ($micropub) {
      if ($us_title !== '') $us_data['properties']['name'] = $us_title;
      if ($action !== '') $us_data['properties'][$action] = $us_url;
      if ($us_category !== '') {
        foreach (explode(',', $us_category) as $tag) {
          $us_data['properties']['category'][] = $tag;
        }
      }
      if (isset($us_content['paymentType']) &&
          isset($us_content['paymentAmount'])) {
        $us_payment = $us_content['paymentType'];
        if ($us_payment === 'to') {
          $us_data['properties']['debit'] = $us_content['paymentAmount'];
        }
        else if ($us_payment === 'from') {
          $us_data['properties']['credit'] = $us_content['paymentAmount'];
        }
      }
      $micropub_config = isset($this->user->settings['micropub']['config']) ?
        json_decode($this->user->settings['micropub']['config'], true) : [];
      if (isset($us_content['syndicate']) &&
          count($us_content['syndicate']) > 0) {
        // The syndicate-to uid's are modified for use in html, so fetch them
        // again here to compare to the values set in content.
        foreach ($us_content['syndicate'] as $syndicate) {
          foreach ($micropub_config['syndicate-to'] as $syndicate_option) {
            if (isset($syndicate_option['uid'])) {
              $id = $syndicate_option['uid'];
              $check = preg_replace('/[[:^alnum:]]/', '', $id);
              if ($check === $syndicate) {
                if (!isset($us_data['properties']['mp-syndicate-to'])) {
                  $us_data['properties']['mp-syndicate-to'] = '';
                }
                if ($us_data['properties']['mp-syndicate-to'] !== '') {
                  $us_data['properties']['mp-syndicate-to'] .= ',';
                }
                $us_data['properties']['mp-syndicate-to'] .= $id;
              }
            }
          }
        }
      }
      if (isset($us_content['destination']) &&
          count($us_content['destination']) > 0) {
        // destination uid's are used the same as above.
        foreach ($us_content['destination'] as $destination) {
          foreach ($micropub_config['destination'] as $destination_option) {
            if (isset($destination_option['uid'])) {
              $id = $destination_option['uid'];
              $check = preg_replace('/[[:^alnum:]]/', '', $id);
              if ($check === $destination) {
                if (!isset($us_data['properties']['mp-destination'])) {
                  $us_data['properties']['mp-destination'] = '';
                }
                if ($us_data['properties']['mp-destination'] !== '') {
                  $us_data['properties']['mp-destination'] .= ',';
                }
                $us_data['properties']['mp-destination'] .= $id;
              }
            }
          }
        }
      }
      $this->Micropub($us_data);
      return;
    }

    $mysqli = connect_db();
    // Titles can be a maximum of 180 characters when stored in the database.
    $title = $mysqli->escape_string(substr($us_title, 0, 180));
    $description = $mysqli->escape_string($us_data);
    $author = $mysqli->escape_string($us_author);
    // If in single user mode and author matches a preferred username in
    // account settings then use that.
    if ($this->Substitute('account-single-user') === 'true') {
      $query = 'SELECT value FROM settings WHERE user = "' . $author . '" ' .
        'AND label = "account" AND name = "username"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($settings = $mysqli_result->fetch_assoc()) {
          $author = $mysqli->escape_string($settings['value']);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->SetContent 1: ' . $mysqli->error);
      }
    }
    $category = $mysqli->escape_string($us_category);
    $enclosure = $mysqli->escape_string($us_enclosure);
    $permalink = '';
    $old_title = '';
    $us_old_description = '';
    $old_author = '';
    $old_category = '';
    $us_old_category = '';
    $old_enclosure = '';
    $old_permalink = '';
    $feed = '';
    $query = 'SELECT title, description, author, category, enclosure, ' .
      'permalink, feed FROM post WHERE user = "' . $this->owner . '" AND ' .
      'box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $old_title = $mysqli->escape_string($post['title']);
        $us_old_description = $post['description'];
        $old_author = $mysqli->escape_string($post['author']);
        $us_old_category = $post['category'];
        $old_category = $mysqli->escape_string($us_old_category);
        $old_enclosure = $mysqli->escape_string($post['enclosure']);
        $old_permalink = $mysqli->escape_string($post['permalink']);
        $permalink = $old_permalink;
        $feed = $mysqli->escape_string($post['feed']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->SetContent 2: ' . $mysqli->error);
    }
    // If only data was sent, use the previous values for other fields.
    if (!isset($us_content['dataOnly']) || $us_content['dataOnly'] !== false) {
      $title = $old_title;
      $author = $old_author;
      $category = $old_category;
      $enclosure = $old_enclosure;
    }
    // Only create a new permalink if the title was changed, or if a new post.
    // Otherwise don't update the timestamp for the post.
    $time = time();
    $timestamp_query = '';
    $empty_post = '<i>empty post.</i>';
    if ($permalink === '' || $title !== $old_title) {
      $timestamp_query = ', timestamp = ' . $time;
      $date = date('Y-m-d', $time);
      // Remove characters from the title or description that don't belong in
      // permalinks, but replace spaces with underscores.
      $patterns = ['/[^a-z0-9\s_-]/i', '/ /'];
      $replacements = ['', '_'];
      // When the title is empty, use the start of description as the peramlink.
      if ($title === '') {
        if (strip_tags($description) === '') {
          $permalink = $date . '-' . mt_rand();
          // Having both an empty title and description is really bad because
          // Content() will return false, which means the post can't be removed.
          if ($description === '') {
            $description = $empty_post;
          }
        }
        else {
          $start = substr(strip_tags($description), 0, 50);
          // description has entities encoded, want to decode them so they
          // can be removed.
          $start = html_entity_decode($start, ENT_QUOTES);
          // it can also have escaped newlines because of calling
          // mysqli escape_string above, replace them with underscores.
          $start = preg_replace('/\\\n/', '_', $start);
          $permalink = $date . '-' .
            preg_replace($patterns, $replacements, $start);
        }
      }
      else {
        $permalink = $date . '-' .
          preg_replace($patterns, $replacements, $title);
      }
      // Now check if this permalink already exists as a page name, and
      // increment a suffix if there's a conflict.
      $query = 'SELECT DISTINCT page FROM modules WHERE ' .
        'user = "' . $this->owner . '" AND page LIKE "' . $permalink . '%"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows > 0) {
          $permalink .= '-' . ($mysqli_result->num_rows + 1);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->SetContent 3: ' . $mysqli->error);
      }
      // The permalink may not have changed just because the title has
      // (due to characters that are removed), so compare first.
      if ($permalink !== $old_permalink) {
        $this->CreatePermalinkPage($id, $permalink, $old_permalink, $feed);
      }
    }
    // Otherwise check if anything else has changed and return if not.
    // Note that having a notify flag set to true overrides the content check.
    else if ((!isset($us_content['notify']) ||
              $us_content['notify'] === false) &&
             $us_data === $us_old_description && $author === $old_author &&
             $category === $old_category && $enclosure === $old_enclosure) {
      $mysqli->close();
      return;
    }

    if (isset($us_content['draft']) && $us_content['draft'] === true) {
      $_POST['notify'] = 'false';
      $schedule = isset($us_content['schedule']) ?
        (int)strtotime($us_content['schedule']) : 0;
      $query = 'INSERT INTO post_draft VALUES ("' . $this->owner . '", ' .
        $id . ', ' . $schedule . ') ON DUPLICATE KEY UPDATE ' .
        'timestamp = ' . $schedule;
    }
    else {
      $query = 'UPDATE post SET title = "' . $title . '", ' .
        'description = "' . $description . '", author = "' . $author . '", ' .
        'category = "' . $category . '", enclosure = "' . $enclosure . '", ' .
        'permalink = "' . $permalink . '"' . $timestamp_query .
        ' WHERE user = "' . $this->owner . '" AND box_id = ' . $id;
    }
    if (!$mysqli->query($query)) {
      $this->Log('Post->SetContent 4: ' . $mysqli->error);
    }
    $query = 'INSERT INTO post_history VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $title . '", "' . $description . '", "' . $author . '", ' .
      '"' . $category . '", "' . $enclosure . '", "' . $permalink . '", ' .
      '"' . $feed . '", ' . $time . ', "' . $this->user->name . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Post->SetContent 5: ' . $mysqli->error);
    }
    $mysqli->close();

    // When a post has been updated, the only time Notify is not called is
    // when notify is set and the given value is set to false.
    if (isset($_POST['notify']) && $_POST['notify'] === 'false') return;

    // If a post is still empty the second time SetContent is called, remove
    // it before any notifications are sent. (SetContent is first called from
    // an Extended Editor callback, where notify is set to false.)
    if ($description === $empty_post) {
      // Passing the permalink in here means a tombstone post isn't created.
      $this->RemovePermalinkPage($id, $permalink);
      $this->RemoveNotify($id);
      $this->UpdatePager($id);
      $mysqli = connect_db();
      $query = 'DELETE FROM post WHERE user = "' . $this->owner . '" AND ' .
        'box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Post->SetContent 6: ' . $mysqli->error);
      }
      $mysqli->close();
      return;
    }

    $published = $this->Published($feed);
    // Note that modules need to decide what notifications they want to give.
    // Not bothering with add or edit notifications here, instead just
    // pushing all changes into the main page feed when the page is published.
    $this->Notify($id, 'post', 'feed', $feed, $published);
    // Also create individual notifications for users that request them.
    // Note that the feed name is also passed in as the description here.
    $this->Notification('post', $author, $feed, 'feed', $feed, $permalink);
    // Webmentions will be sent to any links in the description and categories.
    // Also check for links in the old content because they might have been
    // removed, in which case we still send a webmention to notify the target
    // of the removal. (The data is stored here and processed from a callback.)
    $us_data .= $us_old_description;
    $us_category .= ',' . $old_category;
    // url is only required by micropub.php.
    $url = $this->user->config->Secure() ? 'https://' : 'http://';
    $url .= $this->user->config->ServerName() . $this->Url('', $permalink);
    $update = ['box_id' => $id, 'content' => $us_data,
               'category' => $us_category, 'feed' => $feed,
               'permalink' => $permalink, 'published' => $published,
               'url' => $url];
    $key = $this->owner . '-' . $this->user->page;
    if (isset($_SESSION['post-update'])) {
      $_SESSION['post-update'][$key] = $update;
    }
    else {
      $_SESSION['post-update'] = [$key => $update];
    }
    // This is returned for our own micropub endpoint to call ContentUpdated.
    return $_SESSION['post-update'][$key];
  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AddSyndicationLink($us_url, $permalink) {
    $mysqli = connect_db();
    $us_url = '<a href="' . $us_url . '" class="u-syndication"></a>';
    $url = $mysqli->escape_string($us_url);
    $query = 'UPDATE post SET description = CONCAT(description, ' .
      '"' . $url . '") WHERE user = "' . $this->owner . '" AND ' .
      'permalink = "' . $permalink . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->AddSyndicationLink: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ContentUpdated($id, $data, $category, $feed,
                                  $permalink, $updated = true) {
    $status = '';
    $already_sent = [];
    $doc = new DOMDocument();
    @$doc->loadHTML($data);
    $xpath = new DOMXpath($doc);
    foreach ($xpath->query('//a[@href]') as $link) {
      $url = $link->getAttribute('href');
      if (!in_array($url, $already_sent)) {
        $status .= $this->SendWebmention($url, $permalink);
        $already_sent[] = $url;
      }
    }
    // Also check for links in category. Note that the anchor is created in the
    // Content method, so need to match only the url.
    if (preg_match_all('/https?:\/\/[^,]+/', $category, $match)) {
      foreach (array_unique($match[0]) as $url) {
        // Links can be added to both the content and as a category, so check
        // if a webmention has just been sent.
        if (!in_array($url, $already_sent)) {
          $status .= $this->SendWebmention($url, $permalink);
        }
      }
    }

    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $this->user->config->ServerName();
    // Only update the feeds when the post content is updated (which is false
    // here when sending webmentions for a new comment).
    if ($updated) {
      // Update the feed, which will also notify subscribers.
      $reader = new Module($this->user, $this->owner, 'reader');
      if ($reader->IsInstalled()) {
        $page = 'rss/index.php?page=' . $feed;
        $path = $this->owner === 'admin' ? $page : $this->owner . '/' . $page;
        $reader->Factory('UpdateFeed', $base_url . '/' . $path);
        // Also update the microformats feed and provide the post content as
        // the payload for the update. Content needs the current page to be set
        // to the feed (or permalink) page to return any content.
        $current_page = $this->user->page;
        $this->user->page = $feed;
        $data = $this->Content($id);
        $this->user->page = $current_page;
        $feed_url = $base_url . $this->Url('', $feed);
        $reader->Factory('UpdateFeed', [$feed_url, $data]);
      }
    }
    if ($status !== '') return $status;
    // This is also called when updating a post, but dobrado.save in core.pub.js
    // doesn't display the message.
    return 'Created post: ' .
      '<a href="' . $base_url . $this->Url('', $permalink) . '">' .
      $permalink . '</a>';
  }

  private function CustomSettings() {
    $settings = [];
    $id = (int)substr($_POST['id'], 9);

    $mysqli = connect_db();
    $query = 'SELECT title, description, author, category, enclosure, ' .
      'permalink FROM post WHERE user = "' . $this->owner . '" AND ' .
      'box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $settings['editor'] = true;
        $settings['source'] = $post['description'];
        // When notify is set to false, it's possible that the current version
        // of the post will be saved without sending notifications. In this
        // case create a notify flag to ignore the content check done later.
        if (isset($_POST['notify']) && $_POST['notify'] === 'false') {
          $settings['notify'] = true;
        }
        $settings['permalink'] = $post['permalink'];
        // Check the 'Send to Twitter' box if a link was added previously.
        $twitter_checked = '';
        $twitter_link = 'https://brid.gy/publish/twitter';
        if (strpos($post['description'], $twitter_link) !== false) {
          $twitter_checked = ' checked="checked"';
        }
        // Set Webaction if a class name is found in the description.
        $action = '';
        $url = '';
        $options = '<option></option><option>like</option>' .
          '<option>reply</option><option>share</option>';
        if (strpos($post['description'], 'u-like-of') !== false) {
          $action = 'u-like-of';
          $options = '<option></option>' .
            '<option selected="selected">like</option>' .
            '<option>reply</option><option>share</option>';
        }
        else if (strpos($post['description'], 'u-repost-of') !== false) {
          $action = 'u-repost-of';
          $options = '<option></option><option>like</option>' .
            '<option>reply</option>' .
            '<option selected="selected">share</option>';
        }
        else if (strpos($post['description'], 'u-in-reply-to') !== false) {
          $action = 'u-in-reply-to';
          $options = '<option></option><option>like</option>' .
            '<option selected="selected">reply</option>' .
            '<option>share</option>';
        }
        if ($action !== '') {
          $regex = '/<a class="' . $action . '" href="(.*)"/i';
          if (preg_match($regex, $post['description'], $match)) {
            $url = $match[1];
          }
        }
        $media = '<fieldset id="post-media"><legend>Media</legend>';
        $enclosure_list = json_decode($post['enclosure'], true);
        if (is_array($enclosure_list)) {
          foreach ($enclosure_list as $enclosure) {
            $media .= '<div class="form-spacing">' .
                '<button type="button" class="post-remove-photo">remove' .
                '</button>' .
                '<input name="enclosure" type="text" maxlength="200" ' .
                  'value="' . htmlspecialchars($enclosure) . '">' .
              '</div>';
          }
        }
        // type="button" is used to avoid default submit action.
        $media .= '<button type="button" id="post-add-photo">add photo' .
          '</button></fieldset>';
        // The Extended module uses the name attribute to assign content.
        $settings['custom'] = '<form id="extended-custom-settings">' .
            '<div class="form-spacing">' .
              '<label for="post-title-input">Title:</label>' .
              '<input id="post-title-input" name="title" type="text" ' .
                'value="' . htmlspecialchars($post['title']) . '" ' .
                'maxlength="180">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="post-author-input">Author:</label>' .
              '<input id="post-author-input" name="author" type="text" ' .
                'value="' . $post['author'] . '" maxlength="50">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="post-category-input">Category:</label>' .
              '<input id="post-category-input" name="category" type="text" ' .
                'value="' . htmlspecialchars($post['category']) . '" ' .
                'maxlength="200">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="post-action-url-input">Webaction Url:</label>' .
              '<input id="post-action-url-input" name="webactionUrl" ' .
                'type="text" value="' . $url . '" maxlength="200">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="post-action-type-input">Webaction Type:</label>' .
              '<select id="post-action-type-input" name="webactionType">' .
                $options . '</select>' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="post-twitter-input">Send to Twitter:</label>' .
              '<input id="post-twitter-input" name="twitter" type="checkbox"' .
                $twitter_checked . '>' .
            '</div>' .
            $media .
            '<button type="submit">Submit</button>' .
          '</form>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->CustomSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $settings;
  }

  private function Endpoint($url) {
    $endpoint = discover_endpoint($url, 'webmention');
    if ($endpoint === '') {
      if ($this->Substitute('post-webmention-debug') === 'true') {
        $this->Log('Post->Endpoint: No endpoint found at ' . $url);
      }
      return '';
    }
    if (stripos($endpoint, 'http') !== 0) {
      if ($this->Substitute('post-webmention-debug') === 'true') {
        $this->Log('Post->Endpoint: ' . $endpoint);
      }
      return '';
    }

    if (strpos($url, '?bridgy_omit_link=true') !== false) {
      // The bridgy_omit_link query parameter needs to be on the request to the
      // endpoint, not the url of the target, but the current method of marking
      // up the content provides an easy way to know when to add it here.
      $endpoint .= '?bridgy_omit_link=true';
    }
    return $endpoint;
  }

  private function Hide($id, $page, $deleted) {
    $mysqli = connect_db();
    $query = 'UPDATE modules SET deleted = ' . $deleted . ' WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id .
      ' AND page = "' . $page . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Hide: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function Insert($id, $author, $feed, $title = '',
                          $description = '', $category = '',
                          $enclosure = '', $permalink = '',
                          $timestamp = 0, $history = false) {
    if ($timestamp === 0) {
      $timestamp = time();
    }
    $mysqli = connect_db();
    $query = 'INSERT INTO post VALUES ("' . $this->owner . '", ' . $id . ', ' .
      '"' . $title . '", "' . $description . '", "' . $author . '", ' .
      '"' . $category . '", "' . $enclosure . '", "' . $permalink . '", ' .
      '"' . $feed . '", ' . $timestamp . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Insert 1: ' . $mysqli->error);
    }
    // Don't copy into history unless specified. Need to be careful due to
    // the way new posts are created and then immediately saved with their
    // new content, both Insert and SetContent can be called within a
    // second which will duplicate the primary key.
    if ($history) {
      $query = 'INSERT INTO post_history VALUES ("' . $this->owner . '", ' .
        $id . ', "' . $title . '", "' . $description . '", ' .
        '"' . $category . '", "' . $author . '", "' . $enclosure . '", ' .
        '"' . $permalink . '", "' . $feed . '", ' . $timestamp . ', ' .
        '"' . $this->user->name . '")';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Insert 2: ' . $mysqli->error);
      }
    }
    else {
      // If using the pager module, older posts are moved to a new page when
      // the posts per page limit is reached. (Note: this isn't used when
      // copying pages of posts).
      $pager = new Module($this->user, $this->owner, 'pager');
      if ($pager->IsInstalled()) {
        $pager->Factory('Move', $feed);
      }
    }
    $mysqli->close();
  }

  private function IsPermalink() {
    if ($this->Published() === 0) return false;

    $permalink = false;
    $mysqli = connect_db();
    $query = 'SELECT permalink FROM post WHERE user = "' . $this->owner . '" ' .
      'AND permalink = "' . $this->user->page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $permalink = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->IsPermalink: ' . $mysqli->error);
    }
    $mysqli->close();
    return $permalink;
  }

  private function LookupAuthor($us_name, $us_url) {
    $us_photo = '';
    if ($us_name === '' &&
        preg_match('/^https?:\/\/([^\/]+)/i', $us_url, $match)) {
      $us_name = $match[1];
    }
    else {
      list($us_name, $us_url, $us_photo) =
        $this->LookupNickname($us_name, $us_url);
    }
    // Note that a u-url is required here because in the case of reposts, the
    // url of the user making the repost will be used if not found previously.
    $us_author = '';
    if ($us_photo !== '') {
      $us_author = '<a href="' . $us_url . '">' .
        '<img class="thumb u-photo" src="' . $us_photo . '"></a> ';
    }
    $us_author .= '<a class="author p-name u-url" href="' . $us_url . '">' .
      $us_name . '</a>';
    return $us_author;
  }

  private function LookupName($tag) {
    $name = '';
    $tag = trim($tag);
    if (stripos($tag, 'http') === 0) {
      if (preg_match('/^https?:\/\/([^\/]+\/?[^\/]*)/i', $tag, $match)) {
        $tag = $match[1];
      }
      $mysqli = connect_db();
      $tag = $mysqli->escape_string($tag);
      $query = 'SELECT name FROM nickname WHERE url LIKE "%' . $tag . '"' .
        ' OR url LIKE "%' . $tag . '/"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($nickname = $mysqli_result->fetch_assoc()) {
          $name = $nickname['name'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->LookupName: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    return $name;
  }

  private function LookupUser($user, $base_url) {
    $result = $user;
    // If using single user mode, check if the username provided is from
    // account settings and switch to user details for admin in this case.
    if ($this->Substitute('account-single-user') === 'true') {
      // Can only access $this->user->settings when logged in so look up the
      // values required here in the settings table.
      $mysqli = connect_db();
      $query = 'SELECT value FROM settings WHERE ' .
        'user = "' . $this->owner . '" AND label = "account" AND ' .
        'name = "username"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($settings = $mysqli_result->fetch_assoc()) {
          if ($settings['value'] === $user) {
            $result = '<a class="p-name u-url" href="' . $base_url . '">' .
              $user . '</a>';
            // Switch to admin now to check user details below.
            $user = 'admin';
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->LookupUser: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    $detail = new Module($this->user, $this->owner, 'detail');
    if ($detail->IsInstalled()) {
      $user_detail = $detail->Factory('User', $user);
      $name = '';
      if ($user_detail['first'] !== '') {
        $name = $user_detail['first'];
      }
      if ($user_detail['last'] !== '') {
        if ($name !== '') $name .= ' ';
        $name .= $user_detail['last'];
      }
      // If name is empty, the current value for user given to the Detail
      // module didn't match an existing user.
      if ($name !== '') {
        $url = $base_url;
        if ($user !== 'admin') $url .= '/' . $user;
        if ($user_detail['thumbnail'] !== '') {
          $result = '<a href="' . $url . '">' .
            $user_detail['thumbnail'] . '</a> ';
        }
        $result .= '<a class="p-name u-url" href="' . $url . '">' .
          $name . '</a>';
      }
    }
    return $result;
  }

  private function Micropub($us_data) {
    $micropub = $this->user->settings['micropub']['endpoint'];
    $curl_headers = ['Authorization: Bearer ' .
                       $this->user->settings['micropub']['token'],
                     'Content-Type: application/json'];
    $ch = curl_init($micropub);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($us_data));
    curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($ch, $header) {
      if (preg_match('/^Location:(.+)$/', $header, $match)) {
        $_SESSION['micropub-status'] = 'Published to: ' .
          '<a href="' . $match[1] . '">' . $match[1] . '</a>';
      }
      return strlen($header);
    });
    $this->Log('Post->Micropub 1: curl ' . $micropub);
    $body = curl_exec($ch);
    if (curl_errno($ch) === 0) {
      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if ($http_code !== 201 && $http_code !== 202) {
        $_SESSION['micropub-status'] = 'Publish error. HTTP code: ' .$http_code;
        $this->Log('Post->Micropub 2: Error posting to ' . $micropub .
               "\nHTTP code: " . $http_code . "\nBody: " . $body);
      }
    }
    else {
      $_SESSION['micropub-status'] = 'Couldn\'t connect to ' . $micropub;
      $this->Log('Post->Micropub 3: Error connecting to ' . $micropub .
                 "\nCurl error: " . curl_error($ch));
    }
    curl_close($ch);
  }

  private function Navigation($feed, $timestamp) {
    $mysqli = connect_db();
    $navigation = '';
    $tombstone = $mysqli->escape_string($this->Tombstone());
    $query = 'SELECT title, permalink FROM post WHERE ' .
      'user = "' . $this->owner . '" AND description != "' . $tombstone . '" ' .
      'AND feed = "' . $feed . '" AND timestamp < ' . $timestamp .
      ' ORDER BY timestamp DESC LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $navigation = '<div class="navigation">' .
          '<a href="' . $this->Url('', $post['permalink']) . '" class="prev">';
        $navigation .= '&lt; ';
        if ($post['title'] === '') {
          $navigation .= 'older';
        }
        else {
          $navigation .= $post['title'];
        }
        $navigation .= '</a>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Navigation 1: ' . $mysqli->error);
    }
    $query = 'SELECT title, permalink FROM post WHERE ' .
      'user = "' . $this->owner . '" AND description != "' . $tombstone . '" ' .
      'AND feed = "' . $feed . '" AND timestamp > ' . $timestamp .
      ' ORDER BY timestamp LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        if ($navigation === '') {
          $navigation = '<div class="navigation">';
        }
        $navigation .=
          '<a href="' . $this->Url('', $post['permalink']) . '" class="next">';
        if ($post['title'] === '') {
          $navigation .= 'newer';
        }
        else {
          $navigation .= $post['title'];
        }
        $navigation .= ' &gt;</a>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Navigation 2: ' . $mysqli->error);
    }
    if ($navigation !== '') {
      $navigation .= '</div>';
    }
    $mysqli->close();
    return $navigation;
  }

  private function NewComment() {
    if (!$this->IsPermalink()) return;

    $url = $this->user->config->Secure() ? 'https://' : 'http://';
    $url .= $this->user->config->ServerName() .
      $this->Url('', $this->user->page);
    $key = $this->owner . '-' . $this->user->page;

    $mysqli = connect_db();
    $query = 'SELECT box_id, description, category, feed FROM post WHERE ' .
      'user = "' . $this->owner . '" AND permalink = "' . $this->user->page.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $update = ['box_id' => $post['box_id'],
                   'content' => $post['description'],
                   'category' => $post['category'], 'feed' => $post['feed'],
                   'permalink' => $this->user->page, 'published' => 1,
                   'url' => $url];
        if (isset($_SESSION['post-update'])) {
          $_SESSION['post-update'][$key] = $update;
        }
        else {
          $_SESSION['post-update'] = [$key => $update];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->NewComment: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function CreatePermalinkPage($id, $permalink, $old_permalink, $feed) {
    $this->RemovePermalinkPage($id, $old_permalink);

    // Need to override the user set in config to match the current page owner.
    if ($this->user->name !== $this->owner) {
      $this->user->config->SetUser($this->owner);
    }
    // The box_order for the post module on the permalink page is configurable.
    $order = $this->user->config->PermalinkOrder();
    $placement = $this->Placement();
    $group = $this->Group();
    copy_page($this->user->config->PermalinkDefault(), $this->owner,
              $permalink, $this->owner, $feed, $order, $placement);
    if ($this->user->name !== $this->owner) {
      $this->user->config->SetUser($this->user->name);
    }

    $mysqli = connect_db();
    $query = 'INSERT INTO modules VALUES ("' . $this->owner . '", ' .
      '"' . $permalink . '", ' . $id . ', "post", "' . $group . '", ' .
      $order . ', "' . $placement . '", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Post->Permalink 1: ' . $mysqli->error);
    }
    // If the current user is not the owner of the page, give them edit
    // permission for the permalink page. (They may have only had view
    // permission for the page containing the writer module, so this gives
    // them a way to edit their post).
    if ($this->user->name !== $this->owner) {
      $query = 'INSERT INTO user_permission VALUES ("' . $this->owner . '", ' .
        '"' . $permalink . '", "' . $this->user->name . '", 1, 0, 0) ' .
        'ON DUPLICATE KEY UPDATE edit = 1';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Permalink 2: ' . $mysqli->error);
      }
    }
    // Update the comment notifications group for this page if the permalink
    // has changed, otherwise add the current user to the group if it's a new
    // permalink.
    if ($old_permalink === '') {
      $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
        '"comment-notifications-' . $permalink . '", ' .
        '"' . $this->user->name . '", 0)';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Permalink 3: ' . $mysqli->error);
      }
    }
    else {
      $query = 'UPDATE group_names SET ' .
        'name = "comment-notifications-' . $permalink . '" WHERE ' .
        'user = "' . $this->owner . '" AND ' .
        'name = "comment-notifications-' . $old_permalink . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Post->Permalink 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  private function ProcessReceivedWebmention($us_source, $us_target) {
    $comment = new Module($this->user, $this->owner, 'comment');
    $comment_installed = $comment->IsInstalled();
    $escrow = new Module($this->user, $this->owner, 'escrow');
    $escrow_installed = $escrow->IsInstalled();
    $reader = new Module($this->user, $this->owner, 'reader');
    $reader_installed = $reader->IsInstalled();

    // Check that a link to the target is found in the source (or that the
    // source has been removed, in which case try and delete a comment).
    $ch = curl_init($us_source);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_HEADER, false);
    $this->Log('Post->ProcessReceivedWebmention 1: curl ' . $us_source);
    $body = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_code === 410) {
      if ($comment_installed) {
        $id = $comment->Factory('MatchUrl', $us_source);
        if ($id !== 0) {
          $comment->Remove($id);
          return ['HTTP/1.1 200 OK',
                  'Thanks! Your webmention has been removed.', true];
        }
      }
      if ($reader_installed &&
          $reader->Factory('RemoveFollowPost', [$us_source, $us_target])) {
        return ['HTTP/1.1 200 OK',
                'Thanks! Your subscription has been removed.', false];
      }
      // TODO: If escrow is installed need to check if $us_source was syndicated
      // and then remove the post and update the escrow tables.
      return ['HTTP/1.1 400 Bad Request',
              'Could not process webmention removal.', false];
    }

    if ($http_code !== 200) {
      return ['HTTP/1.1 400 Bad Request', 'Source URL not found: ' . $us_source,
              false];
    }

    // Look for meta http-equiv to support deleted static posts.
    if ($position = stripos($body, '<meta http-equiv')) {
      $start = $position < 200 ? 0 : $position - 200;
      $check = substr($body, $start, 400);
      $regex = '/<meta http-equiv="([^"]+)" content="([0-9]{3})[^"]*"\/>/i';
      if (preg_match($regex, $check, $match)) {
        if (strtolower($match[1]) === 'status' && $match[2] === '410') {
          if ($comment_installed) {
            $id = $comment->Factory('MatchUrl', $us_source);
            if ($id !== 0) {
              $comment->Remove($id);
              return ['HTTP/1.1 200 OK',
                      'Thanks! Your webmention has been removed.', true];
            }
          }
          if ($reader_installed &&
              $reader->Factory('RemoveFollowPost', [$us_source, $us_target])) {
            return ['HTTP/1.1 200 OK',
                    'Thanks! Your subscription has been removed.', false];
          }
          // TODO: same as above...
          return ['HTTP/1.1 400 Bad Request',
                  'Could not process webmention removal.', false];
        }
      }
    }

    $us_result = parse_hentry($us_source, $us_target, $body);
    if (stripos($body, $us_target) === false) {
      if ($comment_installed) {
        $id = $comment->Factory('MatchUrl', $us_result['url']);
        if ($id !== 0) $comment->Remove($id);
      }
      return ['HTTP/1.1 400 Bad Request',
              'Source URL does not contain a link to the target: ' . $us_source,
              false];
    }

    if ($this->Substitute('post-webmention-debug') === 'true') {
      $this->Log('Post->ProcessReceivedWebmention 2: ' .
                 'Accepted webmention from ' . $us_source);
    }
    $webmention_status = 'HTTP/1.1 200 OK';
    $webmention_message = 'Thanks! Your webmention has been accepted.';
    $commenteditor = new Module($this->user, $this->owner, 'commenteditor');
    if ($commenteditor->IsInstalled() &&
        $commenteditor->Factory('Blocked', $us_result['url'])) {
      $this->Log('Post->ProcessReceivedWebmention 3: ' .
                 'Blocking webmention from ' . $us_result['url']);
      return [$webmention_status, $webmention_message, false];
    }

    include_once 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('HTML.DefinitionID', 'dobrado-comment');
    $config->set('HTML.DefinitionRev', 1);
    if ($def = $config->maybeGetRawHTMLDefinition()) {
      $def->addElement('data', 'Inline', 'Inline', 'Common');
      $def->addAttribute('data', 'value', 'CDATA');
      $def->addAttribute('a', 'data-lightbox', 'Text');
      $def->addAttribute('a', 'data-title', 'Text');
    }
    $purifier = new HTMLPurifier($config);
    $us_url = $purifier->purify($us_result['url']);
    $us_description = $purifier->purify($us_result['content']);
    $us_author = $purifier->purify($us_result['author-name']);
    $us_author_photo = $purifier->purify($us_result['author-photo']);
    $us_author_url = $purifier->purify($us_result['author-url']);
    $us_mention = $us_result['mention'];
    $resend = false;
    $id = '';
    // Create a list of mention types that can be displayed as comments.
    $display_comment = ['comment', 'tag-comment', 'star', 'share'];
    // If this is a follow post need to check if this user has set a follow
    // page, in which case Reader->CheckFollowPost will create a notification.
    $label = 'reader-follow-' . $this->owner . '-' . $this->user->page;
    if ($us_mention === 'follow' &&
        $reader_installed && $this->Substitute($label) === 'true') {
      list($webmention_status, $webmention_message) =
        $reader->Factory('CheckFollowPost', [$us_result, $us_target]);
    }
    else if ($comment_installed && in_array($us_mention, $display_comment)) {
      // The current page needs to be a permalink to display the comment.
      if ($this->IsPermalink()) {
        $us_content = ['author' => $us_author,
                       'author_photo' => $us_author_photo,
                       'author_url' => $us_author_url, 'url' => $us_url,
                       'description' => $us_description,
                       'permalink' => $this->user->page . '#' . $id,
                       'category' => $us_mention];
        $id = $comment->Factory('MatchUrl', $us_url);
        if ($id !== 0) {
          if ($comment->Factory('Modified', [$id, $us_description])) {
            $resend = true;
            if ($us_description === '') {
              $comment->Remove($id);
            }
            else {
              // SetContent will create a Notification for the updated comment.
              $comment->SetContent($id, $us_content);
            }
          }
        }
        else if ($us_description !== '') {
          // $this->user->page must have a post module to create the comment.
          $id = new_module($this->user, $this->owner, 'comment',
                           $this->user->page, $comment->Group(),
                           $comment->Placement());
          $comment->Add($id);
          // SetContent will create a Notification for the new comment.
          $comment->SetContent($id, $us_content);
          $resend = true;
        }
      }
    }
    else if ($escrow_installed && $us_result['payment'] !== '') {
      list($webmention_status, $webmention_message) =
        $escrow->Factory('NewTransaction', $us_result);
    }
    else {
      $this->Notification('comment', [$us_author, $us_author_photo,
                          $us_author_url], $us_url, $us_mention,
                          $this->user->page, $this->user->page);
    }
    if ($id !== '') $id = '#' . $id;
    // Process any responses to the webmention parsed in the microformats.
    foreach (['comment', 'like', 'share'] as $action) {
      foreach ($us_result[$action] as $us_response) {
        // Ignore an empty comment on permalink pages which is used to show
        // that it's possible to subscribe to a comments feed on the page.
        if ($us_response['url'] === '') continue;

        $us_author = $purifier->purify($us_response['author-name']);
        $us_author_photo = $purifier->purify($us_response['author-photo']);
        $us_author_url = $purifier->purify($us_response['author-url']);
        $us_url = $purifier->purify($us_response['url']);
        $this->Notification('comment', [$us_author, $us_author_photo,
                            $us_author_url], $us_url, $action . '-mention',
                            $this->user->page, $this->user->page . $id);
        if ($escrow_installed && $us_response['payment'] !== '') {
          list($webmention_status, $webmention_message) =
            $escrow->Factory('SettleTransaction', [$us_result, $us_response]);
        }
      }
    }
    return [$webmention_status, $webmention_message, $resend];
  }

  private function PublishDraft($id) {
    $title = '';
    $description = '';
    $author = '';
    $category = '';
    $enclosure = '';
    $permalink = '';
    $feed = '';
    $timestamp = 0;

    $mysqli = connect_db();
    $query = 'SELECT title, description, author, category, enclosure, ' .
      'permalink, feed FROM post_history WHERE user = "' . $this->owner . '" ' .
      'AND box_id = ' . $id . ' ORDER BY timestamp DESC LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post_history = $mysqli_result->fetch_assoc()) {
        $title = $mysqli->escape_string($post_history['title']);
        $description = $mysqli->escape_string($post_history['description']);
        $author = $mysqli->escape_string($post_history['author']);
        $category = $mysqli->escape_string($post_history['category']);
        $enclosure = $mysqli->escape_string($post_history['enclosure']);
        $permalink = $mysqli->escape_string($post_history['permalink']);
        $feed = $mysqli->escape_string($post_history['feed']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->PublishDraft 1: ' . $mysqli->error);
    }
    $query = 'UPDATE post SET title = "' . $title . '", ' .
      'description = "' . $description . '", author = "' . $author . '", ' .
      'category = "' . $category . '", enclosure = "' . $enclosure . '", ' .
      'permalink = "' . $permalink . '", timestamp = ' . time() .
      ' WHERE user = "' . $this->owner . '" AND box_id = ' . $id;
    if (!$mysqli->query($query)) {
      $this->Log('Post->PublishDraft 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM post_draft WHERE user = "' . $this->owner . '" ' .
      'AND box_id = ' . $id;
    if (!$mysqli->query($query)) {
      $this->Log('Post->PublishDraft 3: ' . $mysqli->error);
    }
    $mysqli->close();

    $published = $this->Published($feed);
    $this->Notify($id, 'post', 'feed', $feed, $published);
    $this->Notification('post', $author, $feed, 'feed', $feed, $permalink);
    $this->ContentUpdated($id, $description, $category, $feed, $permalink);
  }

  private function ReceiveWebmention($us_source, $us_target) {
    // The server name is required by page_owner, but it doesn't have to be
    // matched in the source if given a relative url (ie by our own server).
    $full_target = $us_target;
    if (stripos($full_target, 'http') !== 0) {
      $full_target = $this->user->config->Secure() ? 'https://' : 'http://';
      $full_target .= $this->user->config->ServerName() . $us_target;
    }
    list($this->user->page, $this->owner) = page_owner($full_target);
    // Check that target is an existing page on this server, and that it's a
    // permalink for a post module, or a page containing a detail module. The
    // latter case assumes there is a representative h-card on the page.
    if (!$this->IsPermalink() && !$this->AlreadyOnPage('detail')) {
      return ['HTTP/1.1 400 Bad Request',
              'Target URL does not accept webmentions: ' . $us_target];
    }

    $recent = false;
    $mysqli = connect_db();
    $target = $mysqli->escape_string($us_target);
    $query = 'SELECT timestamp FROM post_queue WHERE ' .
      'target = "' . $target . '" ORDER BY timestamp LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post_queue = $mysqli_result->fetch_assoc()) {
        // This is the oldest item in the queue, so continue waiting if it's
        // less than 5 minutes old.
        $recent = $post_queue['timestamp'] > strtotime('-5 minutes');
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->ReceiveWebmention 1: ' . $mysqli->error);
    }
    if ($recent) {
      $source = $mysqli->escape_string($us_source);
      $query = 'INSERT INTO post_queue VALUES ("' . $source . '", ' .
        '"' . $target . '", ' . time() . ') ON DUPLICATE KEY UPDATE ' .
        'timestamp = ' . time();
      if (!$mysqli->query($query)) {
        $this->Log('Post->ReceiveWebmention 2: ' . $mysqli->error);
      }
      $mysqli->close();
      return ['HTTP/1.1 202 Accepted',
              'Thanks! Your webmention has been queued for processing.'];
    }

    $updated = false;
    // Process all queued webmentions for this target.
    $query = 'SELECT source FROM post_queue WHERE target = "' . $target . '" ' .
      'AND source != "" ORDER BY timestamp';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($post_queue = $mysqli_result->fetch_assoc()) {
        $received = $this->ProcessReceivedWebmention($post_queue['source'],
                                                     $us_target);
        if ($received[2]) $updated = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->ReceiveWebmention 3: ' . $mysqli->error);
    }
    // Clear the queue for the target, but add back in a row with an empty
    // source so that the timestamp can be checked by the next webmention.
    $query = 'DELETE FROM post_queue WHERE target = "' . $target . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->ReceiveWebmention 4: ' . $mysqli->error);
    }
    $query = 'INSERT INTO post_queue VALUES ("", "' . $target . '", ' .
      time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Post->ReceiveWebmention 5: ' . $mysqli->error);
    }
    // Lastly process received webmention for the most recent source.
    $received = $this->ProcessReceivedWebmention($us_source, $us_target);
    // If any of the received webmentions updated comments, resend webmentions
    // for this page.
    if (($updated || $received[2]) && $this->IsPermalink()) {
      $query = 'SELECT box_id, description, category, feed FROM post WHERE ' .
        'user = "' . $this->owner . '" AND ' .
        'permalink = "' . $this->user->page . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($post = $mysqli_result->fetch_assoc()) {
          $this->ContentUpdated($post['box_id'], $post['description'],
                                $post['category'], $post['feed'],
                                $this->user->page, false);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Post->ReceiveWebmention 6: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return [$received[0], $received[1]];
  }

  private function RemovePermalinkPage($id, $old_permalink = '') {
    $mysqli = connect_db();
    $us_old_description = '';
    $us_old_category = '';
    $permalink = '';
    $feed = '';
    // First store the old content so that webmentions can be sent to notify
    // links that they've been removed.
    $query = 'SELECT description, category, permalink, feed FROM post ' .
      'WHERE user = "' . $this->owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $us_old_description = $post['description'];
        $us_old_category = $post['category'];
        $permalink = $mysqli->escape_string($post['permalink']);
        $feed = $mysqli->escape_string($post['feed']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->RemovePermalinkPage 1: ' . $mysqli->error);
    }
    // Return if old_permalink was not provided and current permalink not found.
    if ($old_permalink === '' && $permalink === '') {
      $mysqli->close();
      return;
    }

    $tombstone = true;
    // When old_permalink is provided it means the permalink is being updated,
    // so the old page is removed here and a tombstone post is not required.
    // Otherwise the current permalink is used to remove the contents of that
    // page, which then also requires a tombstone post.
    if ($old_permalink !== '') {
      $tombstone = false;
      $permalink = $old_permalink;
    }
    // Call the Remove method for all other modules on the page.
    $query = 'SELECT box_id, label FROM modules WHERE ' .
      'user = "' . $this->owner . '" AND box_id != ' . $id . ' AND ' .
      'page = "' . $permalink . '" AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($module_list = $mysqli_result->fetch_assoc()) {
        $module = new Module($this->user, $this->owner, $module_list['label']);
        $module->Remove($module_list['box_id']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->RemovePermalinkPage 2: ' . $mysqli->error);
    }
    $query = 'UPDATE modules SET deleted = 1 WHERE ' .
      'page = "' . $permalink . '" AND user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->RemovePermalinkPage 3: ' . $mysqli->error);
    }
    $query = 'DELETE FROM page_style WHERE name = "' . $permalink . '" ' .
      'AND user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->RemovePermalinkPage 4: ' . $mysqli->error);
    }
    $path = $this->owner === 'admin' ? '../' : '../' . $this->owner . '/';
    if (file_exists($path . $permalink . '.css')) {
      unlink($path . $permalink . '.css');
    }
    $query = 'DELETE FROM user_permission WHERE page = "' . $permalink . '" ' .
      'AND user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->RemovePermalinkPage 5: ' . $mysqli->error);
    }
    $query = 'DELETE FROM group_permission WHERE page = "' . $permalink . '" ' .
      'AND user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->RemovePermalinkPage 6: ' . $mysqli->error);
    }
    $query = 'DELETE FROM notify WHERE page = "' . $permalink . '" ' .
      'AND user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Post->RemovePermalinkPage 7: ' . $mysqli->error);
    }
    if ($tombstone) {
      $description = $mysqli->escape_string($this->Tombstone());
      $new_id = new_module($this->user, $this->owner, 'post', $permalink,
                           $this->Group(), $this->Placement());
      $query = 'INSERT INTO post VALUES ("' . $this->owner . '", ' .
        $new_id . ', "", "' . $description . '", "", "", "", ' .
        '"' . $permalink . '", "' . $feed . '", ' . time() . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Post->RemovePermalinkPage 8: ' . $mysqli->error);
      }
      // Use SetContent to notify others that the feed has changed.
      $this->SetContent($new_id, ['data' => $description, 'notify' => true]);
    }
    $mysqli->close();

    // Once all the updates have been done send webmentions to old links.
    $this->ContentUpdated($id, $us_old_description, $us_old_category, $feed,
                          $permalink, false);
  }

  private function SendWebmention($url, $permalink, $endpoint = '') {
    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $this->user->config->ServerName();
    // Use our own endpoint for relative urls.
    if (stripos($url, 'http') !== 0) {
      $endpoint = $base_url . '/php/webmention.php';
    }
    else if ($endpoint === '') {
      // Do discovery on the url if an endpoint isn't provided.
      $endpoint = $this->Endpoint($url);
      if ($endpoint === '') return '';
    }

    $status = '';
    $source = $base_url . $this->Url('', $permalink);
    $post_fields = 'source=' . urlencode($source) . '&target=' .urlencode($url);
    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $this->Log('Post->SendWebmention 1: curl ' . $endpoint);
    $body = curl_exec($ch);
    if (curl_errno($ch) === 0) {
      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if ($http_code === 200 || $http_code === 201 || $http_code === 202) {
        if ($this->Substitute('post-webmention-debug') === 'true') {
          $this->Log('Post->SendWebmention 2: Success posting to ' . $endpoint .
                     "\HTTP code: " . $http_code . "\nBody: " . $body);
          if (strpos($endpoint, 'https://brid.gy') === 0) {
            $result = json_decode($body, true);
            if (isset($result['url'])) {
              $this->AddSyndicationLink($result['url'], $permalink);
              $status = 'brid.gy published to: ' .
                '<a href="' . $result['url'] . '">' . $result['url'] .
                '</a><br>';
            }
          }
        }
      }
      else {
        $this->Log('Post->SendWebmention 3: Error posting to ' . $endpoint .
                   "\nHTTP code: " . $http_code . "\nBody: " . $body);
        if (strpos($endpoint, 'https://brid.gy') === 0) {
          $result = json_decode($body, true);
          if (isset($result['error'])) {
            $status = 'brid.gy error: ' . $result['error'];
          }
        }
      }
    }
    else {
      $this->Log('Post->SendWebmention 4: Error connecting to endpoint ' .
                 $endpoint . "\nCurl error: " . curl_error($ch));
    }
    curl_close($ch);
    return $status;
  }

  private function Status($target, $me) {
    $target_status = [];
    $mysqli = connect_db();
    $query = 'SELECT category, permalink FROM post WHERE ' .
      'user = "' . $this->owner . '" AND category LIKE "%' . $target . '%"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($post = $mysqli_result->fetch_assoc()) {
        $category = $post['category'];
        if (strpos($category, ',Like of,') !== false) {
          $target_status['like'] = $me . '/' . $post['permalink'];
        }
        else if (strpos($category, ',reposted by ' . $this->owner) !== false) {
          $target_status['repost'] = $me . '/' . $post['permalink'];
        }
        else if (strpos($category, ',In reply to,') !== false) {
          $target_status['reply'] = $me . '/' . $post['permalink'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Status: ' . $mysqli->error);
    }
    $mysqli->close();
    return $target_status;
  }

  private function Syndicated($us_url) {
    $id = 0;
    $description = '';
    $permalink = '';

    $mysqli = connect_db();
    $url = $mysqli->escape_string($us_url);
    $query = 'SELECT box_id, description, permalink FROM post WHERE ' .
      'user = "' . $this->user->name . '" AND ' .
      'feed = "' . $this->user->page . '" AND description LIKE ' .
      '"%<a href=\"' . $url . '\" class=\"u-syndication\">%"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $id = $post['box_id'];
        $description = $post['description'];
        $permalink = $post['permalink'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->Syndicated: ' . $mysqli->error);
    }
    $mysqli->close();
    return [$id, $description, $permalink];
  }

  private function Tombstone() {
    return 'This post has been removed.';
  }

  private function UpdatePager($id) {
    // If the Pager module is installed it requires the feed name to move
    // posts back to previous pages when required.
    $pager = new Module($this->user, $this->owner, 'pager');
    if (!$pager->IsInstalled()) return;

    $feed = '';
    $mysqli = connect_db();
    $query = 'SELECT feed FROM post WHERE user = "' . $this->owner . '" ' .
      'AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($post = $mysqli_result->fetch_assoc()) {
        $feed = $mysqli->escape_string($post['feed']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->UpdatePager 1: ' . $mysqli->error);
    }
    // Move posts on pages after the the page containing the post being
    // removed. This requires giving the Pager the starting page, so look
    // up the page this post is on and add 1 to give it the next page.
    $page = '';
    $query = 'SELECT page FROM modules WHERE user = "' . $this->owner . '" ' .
      'AND box_id = ' . $id . ' AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($modules = $mysqli_result->fetch_assoc()) {
        $page = $mysqli->escape_string($modules['page']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Post->UpdatePager 2: ' . $mysqli->error);
    }
    $mysqli->close();

    // If the page doesn't have a pager suffix, then the module is being
    // removed from the first page and the next page starts at 2.
    $count = 2;
    if (preg_match('/-p([0-9]+)$/', $page, $match)) {
      $count = (int)$match[1] + 1;
    }
    if ($count >= 2) {
      // The pager expects this post to already be deleted, so flag it
      // first and then reinstate it once Move has been called.
      $this->Hide($id, $page, 1);
      $pager->Factory('Move', [$feed, $count, false]);
      $this->Hide($id, $page, 0);
    }
  }

}
