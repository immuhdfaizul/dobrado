<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Browser extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->loggedIn) {
      return ['error' => 'Permission denied.'];
    }

    $us_action = $_POST['action'];

    if ($us_action === 'remove') {
      return $this->RemoveFile();
    }
    if ($us_action === 'upload') {
      return $this->Upload();
    }
    if ($us_action === 'rotate') {
      return $this->RotateImage();
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    if (!$this->user->loggedIn) return '';

    $content = '';
    if ($this->Substitute('account-single-user') === 'true') {
      $handle = popen('/usr/bin/du -sm ' . $this->PublicDirectory(), 'r');
      $size = fgets($handle);
      pclose($handle);
      $space_used = 0;
      if (preg_match('/^([0-9]+)/', $size, $match)) {
        $space_used = (int)$match[1];
      }
      $max_upload = $this->user->config->MaxUpload();
      if ($space_used > 1 && $max_upload !== 0) {
        $content .= '<div id="browser-upload-info">You have used <b>' .
          $space_used . 'MB</b> of your available space, limit currently set ' .
          'to <b>' . $max_upload . 'MB</b>.</div>';
      }
    }
    // Allow image sharing by switching to a shared account on a specific admin
    // page. (This is currently used to share stock images.)
    $current_user = $this->user->name;
    // Note that a specific group is set here so that users can't override the
    // shared account, which would allow viewing other user's images.
    $shared_page = $this->Substitute('browser-shared-page', '', '', 'admin');
    if ($this->user->canViewPage &&
        $this->owner === 'admin' && $this->user->page === $shared_page) {
      $this->user->name =
        $this->Substitute('browser-shared-account', '', '', 'admin');
      $content .= $this->Substitute('browser-shared-info', '', '', 'admin');
    }
    $content .= '<form id="browser-upload-form">' .
        '<div class="form-spacing">' .
          '<label for="browser-upload-input">Upload a file:</label>' .
          '<input id="browser-upload-input" name="file[]" type="file" ' .
            'multiple>' .
          '<button id="browser-upload">upload</button>' .
        '</div>' .
        '<div id="browser-upload-progress"></div>' .
      '</form>' .
      '<div class="form-spacing">' .
        '<label for="browser-search">Search:</label>' .
        '<input id="browser-search" type="text">' .
      '</div>';

    $html_path = $this->user->name === 'admin' ? '/public/' :
      '/' . $this->user->name . '/public/';
    // Display thumbnails for all files in this user's public folder.
    if ($handle = opendir($this->PublicDirectory())) {
      while (($filename = readdir($handle)) !== false) {
        // Ignore actual thumbnail files.
        if (strpos($filename, '_thumb.')) {
          continue;
        }

        if (preg_match('/^(.+)\.(.+)$/', $filename, $match)) {
          $name = $match[1];
          $type = $match[2];
          $thumbnail = '';
          $updated = '';
          if (in_array(strtolower($type), ['gif', 'jpeg', 'jpg', 'png'])) {
            $name .= '_thumb.' . $type;
            if (file_exists($this->PublicDirectory($name))) {
              if (isset($_SESSION['browser-rotated']) &&
                  in_array($name, $_SESSION['browser-rotated'])) {
                $updated = '?updated=' . time();
              }
              $thumbnail = '<img title = "' . $filename . '" ' .
                'src="' . $html_path . $name . $updated . '">';
            }
            else {
              $thumbnail = '<b title="' . $filename . '">no thumbnail</b>';
            }
          }
          else {
            $thumbnail = '<b title="' . $filename . '">' . $type . '</b>';
          }
          $content .=
            $this->ImageContent($thumbnail, $html_path . $filename . $updated);
        }
      }
    }
    $this->user->name = $current_user;
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    // Upload is called from micropub.php where the required return value is
    // the location of the uploaded file.
    if ($fn === 'Upload') return $this->Upload($p, 'location');
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.browser.js', false);
    $site_style = ['"",".browser","display","none"',
                   '"","#browser-upload","float","right"',
                   '"","#browser-upload-form","padding","20px 0px"',
                   '"","#browser-upload-form","border-bottom",' .
                     '"2px solid #777777"',
                   '"","#browser-upload-progress","display","none"',
                   '"",".thumbnail.highlight","background-color","#fffeee"',
                   '"",".thumbnail.highlight","border","2px solid #cccccc"',
                   '"",".thumbnail.highlight","padding","5px"',
                   '"",".thumbnail.hidden","display","none"',
                   '"",".thumbnail","display","inline-block"',
                   '"",".thumbnail","margin","5px"',
                   '"",".thumbnail","padding","7px"',
                   '"",".thumbnail","min-width","120px"',
                   '"",".thumbnail","vertical-align","top"',
                   '"",".thumbnail .remove","float","right"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.browser.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function RemoveFile() {
    $current_user = $this->user->name;
    $shared_page = $this->Substitute('browser-shared-page', '', '', 'admin');
    if ($this->user->canViewPage &&
        $this->owner === 'admin' && $this->user->page === $shared_page) {
      // Require site permission to remove images on the shared page.
      if (!$this->user->canEditSite) {
        return ['error' => 'Cannot remove shared image.'];
      }

      $this->user->name =
        $this->Substitute('browser-shared-account', '', '', 'admin');
    }

    $regex = '/^([a-z0-9_-]{1,200})\.([a-z0-9]{1,10})/i';
    $us_file = basename($_POST['file']);
    if (preg_match($regex, $us_file, $match)) {
      $name = $match[1];
      $type = $match[2];
      unlink($this->PublicDirectory($name . '.' . $type));
      if (in_array(strtolower($type), ['gif', 'jpeg', 'jpg', 'png'])) {
        unlink($this->PublicDirectory($name . '_thumb.' . $type));
      }
      $this->user->name = $current_user;
      return ['done' => true];
    }
    $this->user->name = $current_user;
    return ['error' => 'Filename does not have correct format.'];
  }

  private function Upload($name = 'file', $return = 'content') {
    $max_upload = 0;
    $current_user = $this->user->name;
    $shared_page = $this->Substitute('browser-shared-page', '', '', 'admin');
    if ($this->user->canViewPage &&
        $this->owner === 'admin' && $this->user->page === $shared_page) {
      $max_upload =
        (int)$this->Substitute('browser-shared-upload', '', '', 'admin');
      $this->user->name =
        $this->Substitute('browser-shared-account', '', '', 'admin');
    }

    // First check if the user's upload directory is at capacity.
    $handle = popen('/usr/bin/du -sm ' . $this->PublicDirectory(), 'r');
    $size = fgets($handle);
    pclose($handle);
    $remaining = 0;
    if (preg_match('/^([0-9]+)/', $size, $match)) {
      if ($max_upload === 0) $max_upload = $this->user->config->MaxUpload();
      $remaining = $max_upload - (int)$match[1];
    }
    else {
      $this->user->name = $current_user;
      return ['error' => 'Could not check upload directory'];
    }
    if ($remaining <= 0) {
      $this->user->name = $current_user;
      return ['error' => 'Upload directory full.'];
    }
    if (!isset($_FILES[$name]['size'])) {
      $this->user->name = $current_user;
      return ['error' => 'Could not check uploaded file size.'];
    }

    if (is_array($_FILES[$name]['size'])) {
      $content = '';
      $location = [];
      for ($i = 0; $i < count($_FILES[$name]['size']); $i++) {
        $filename = $_FILES[$name]['name'][$i];
        $result = $this->UploadFile($filename, $_FILES[$name]['tmp_name'][$i],
                                    $_FILES[$name]['size'][$i], $remaining,
                                    $return);
        if (isset($result['error'])) {
          $this->user->name = $current_user;
          return $result;
        }

        if ($return === 'content') {
          $content .= $result['content'];
        }
        else if ($return === 'location') {
          $location[] = $result;
        }
        $remaining -= (int)$_FILES[$name]['size'][$i] / 1000000;
        // Create a notification when other users upload shared images.
        if ($current_user !== $this->user->name && $current_user !== 'admin') {
          $src = '/' . $this->user->name . '/public/' . $filename;
          $this->Notification('browser', 'Browser', 'User: ' . $current_user .
                                ' uploaded: <a href="' . $src . '">' .
                                '<img src="' . $src . '"></a>', 'system');
        }
      }
      $this->user->name = $current_user;
      if ($return === 'content') return ['content' => $content];
      if ($return === 'location') return $location;
    }

    $filename = $_FILES[$name]['name'];
    if ($current_user !== $this->user->name && $current_user !== 'admin') {
      $src = '/' . $this->user->name . '/public/' . $filename;
      $this->Notification('browser', 'Browser', 'User: ' . $current_user .
                            ' uploaded: <a href="' . $src . '">' .
                            '<img src="' . $src . '"></a>', 'system');
    }
    $result = $this->UploadFile($filename, $_FILES[$name]['tmp_name'],
                                $_FILES[$name]['size'], $remaining, $return);
    $this->user->name = $current_user;
    return $result;
  }

  private function UploadFile($filename, $tmp, $size, $remaining, $return) {
    if (!isset($size)) {
      return ['error' => 'Could not check uploaded file size.'];
    }
    $max_file_size = $this->user->config->MaxFileSize();
    // size is in bytes, MaxFileSize is in megabytes.
    if ((int)$size > $max_file_size * 1000000) {
      return ['error' => 'Upload file is too large. (max ' .
                         $max_file_size . 'M)'];
    }
    // Replace spaces in the uploaded file name.
    $filename = preg_replace('/ /', '_', basename($filename));
    $regex = '/^([a-z0-9_-]{1,200})\.([a-z0-9]{1,10})$/i';
    if (!preg_match($regex, $filename, $match)) {
      return ['error' => 'Filename does not have correct format.'];
    }
    $path = $this->PublicDirectory($filename);
    if ($return !== 'location' && file_exists($path)) {
      // Allow overriding files from micropub.
      return ['error' => 'A file with that name already exists.'];
    }
    $name = $match[1];
    $type = $match[2];
    // Whitelist all file extensions.
    $allowed = ['gif', 'jpeg', 'jpg', 'png', 'aac', 'mpeg', 'oga', 'ogg', 'wav',
                'mp3', 'ogv', 'webm', 'mp4', 'mov', 'pdf'];
    if (!in_array(strtolower($type), $allowed)) {
      return ['error' => 'File type not allowed.'];
    }
    if (!move_uploaded_file($tmp, $path)) {
      return ['error' => 'File: ' . $filename . ' was not uploaded.'];
    }
    // If this user has a media endpoint configured transfer the file to it.
    if ($return === 'content' &&
        isset($this->user->settings['micropub']['config'])) {
      $config = json_decode($this->user->settings['micropub']['config'], true);
      if (isset($config['media-endpoint'])) {
        return $this->TransferMedia($path, $config['media-endpoint']);
      }
    }

    $name .= '_thumb.' . $type;
    $html_path = $this->user->name === 'admin' ? '/public/' :
      '/' . $this->user->name . '/public/';
    $thumbnail = '';

    if (in_array(strtolower($type), ['gif', 'jpeg', 'jpg', 'png'])) {
      list($old_width, $old_height) = getimagesize($path);
      // Create a thumbnail, and resize if height greater than 200px or
      // width greater than 300px.
      $new_height = $old_height;
      if ($new_height > 200) $new_height = 200;
      $new_width = round($old_width / ($old_height / $new_height));
      if ($new_width > 300) {
        $new_width = 300;
        $new_height = round($old_height / ($old_width / 300));
      }
      $thumbnail_path = $this->PublicDirectory($name);
      $this->ResizeImage($new_width, $new_height, $old_width, $old_height,
                         $type, $thumbnail_path, $path);

      // Also if the original image is greater than 700px in either
      // dimension, reduce it.
      $new_width = $old_width;
      $new_height = $old_height;
      if ($old_width > 700 || $old_height > 700) {
        if ($old_width > $old_height) {
          $new_width = 700;
          $new_height = round($old_height / ($old_width / 700));
        }
        else {
          $new_height = 700;
          $new_width = round($old_width / ($old_height / 700));
        }
        $this->ResizeImage($new_width, $new_height, $old_width, $old_height,
                           $type, $path);
      }
      $thumbnail =
        '<img title="' . $filename . '" src="' . $html_path . $name . '">';
    }
    else {
      $thumbnail = '<span title="' . $filename . '">' . $type . '</span>';
    }
    if ($return === 'content') {
      return ['content' =>
              $this->ImageContent($thumbnail, $html_path . $filename)];
    }
    if ($return === 'location') return $html_path . $filename;
    return ['error' => 'Unknown return format.'];
  }

  private function ImageContent($thumbnail, $filename, $local = true) {
    // Display rotate and remove buttons on local thumbnails only.
    $buttons = $local ?
      '<button class="rotate-left hidden">rotate left</button>' .
      '<button class="rotate-right hidden">rotate right</button>' .
      '<button class="remove hidden">remove</button>' : '';

    return '<span class="thumbnail hidden">' . $thumbnail .
        '<span class="filename hidden">' . $filename . '</span><br>' .
        '<button class="select hidden">select</button>' . $buttons .
      '</span>';
  }

  private function TransferMedia($path, $endpoint) {
    $error = '';
    $location = '';
    $uid = uniqid();
    $mime_type = strtolower(mime_content_type($path));
    $curl_headers = ['Authorization: Bearer ' .
                       $this->user->settings['micropub']['token'],
                     'Content-Type: multipart/form-data; boundary=' . $uid];
    $post_fields = '--' . $uid . "\r\n" .
      'Content-Type: ' . $mime_type . "\r\n" .
      "Content-Transfer-Encoding: binary\r\n" .
      'Content-Disposition: form-data; name="file"; ' .
      'filename="' . basename($path) . "\r\n\r\n" .
      file_get_contents($path) . "\r\n" .
      '--' . $uid . '--';

    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    curl_setopt($ch, CURLOPT_HEADERFUNCTION,
      function($ch, $header) use(&$location) {
        if (preg_match('/^Location:(.+)$/', $header, $match)) {
          $location = trim($match[1]);
        }
        return strlen($header);
      });
    $this->Log('Browser->TransferMedia 1: curl ' . $endpoint);
    $body = curl_exec($ch);
    if (curl_errno($ch) === 0) {
      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if ($http_code !== 201) {
        $error = 'Error saving to media endpoint.';
        $this->Log('Browser->TransferMedia 2: Error posting to ' . $endpoint .
                   "\nHTTP code: " . $http_code . "\nBody: " . $body);
      }
    }
    else {
      $error = 'Error connection to media endpoint.';
      $this->Log('Browser->TransferMedia 3: Error connecting to ' . $endpoint .
                 "\nCurl error: " . curl_error($ch));
    }
    curl_close($ch);
    unlink($path);

    if ($error !== '') return ['error' => $error];
    if ($location === '') {
      return ['error' => 'Media endpoint did not return location.'];
    }
    if (strpos($location, 'http') !== 0) {
      return ['error' =>
              'Media endpoint did not return a URL: (' . $location . ')'];
    }
    $thumbnail = '';
    if (in_array($mime_type, ['image/gif', 'image/jpeg', 'image/png'])) {
      $thumbnail = '<img title="' . $location . '" src="' . $location . '">';
    }
    else {
      $thumbnail = '<span title="' . $location . '">' . $mime_type . '</span>';
    }
    return ['content' => $this->ImageContent($thumbnail, $location, false)];
  }

  private function ResizeImage($new_width, $new_height,
                               $old_width, $old_height,
                               $type, $new_path, $old_path = '') {
    // If old path isn't given, saving the new image over the old one.
    if ($old_path === '') {
      $old_path = $new_path;
    }
    $old_image = null;
    if ($type === 'gif') {
      $old_image = imagecreatefromgif($old_path);
    }
    else if ($type === 'jpg' || $type === 'jpeg') {
      $old_image = imagecreatefromjpeg($old_path);
    }
    else if ($type === 'png') {
      $old_image = imagecreatefrompng($old_path);
    }

    $new_image = imagecreatetruecolor($new_width, $new_height);
    if ($type === 'png') {
      imagealphablending($new_image, false);
      imagesavealpha($new_image, true);
    }

    imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $new_width,
                       $new_height, $old_width, $old_height);
    if ($type === 'gif') {
      imagegif($new_image, $new_path);
    }
    else if ($type === 'jpg' || $type === 'jpeg') {
      imagejpeg($new_image, $new_path);
    }
    else if ($type === 'png') {
      imagepng($new_image, $new_path);
    }
    imagedestroy($old_image);
    imagedestroy($new_image);
  }

  private function RotateImage() {
    $current_user = $this->user->name;
    $shared_page = $this->Substitute('browser-shared-page', '', '', 'admin');
    if ($this->user->canViewPage &&
        $this->owner === 'admin' && $this->user->page === $shared_page) {
      // Require site permission to rotate images on the shared page.
      if (!$this->user->canEditSite) {
        return ['error' => 'Cannot rotate shared image.'];
      }

      $this->user->name =
        $this->Substitute('browser-shared-account', '', '', 'admin');
    }

    $name = '';
    $type = '';
    // Default is left (90 degrees anticlockwise)
    $angle = 90;
    if ($_POST['direction'] === 'right') $angle = 270;
    $filename = basename($_POST['file']);
    $regex = '/^([a-z0-9_-]{1,200})\.([a-z0-9]{1,10})/i';
    if (preg_match($regex, $filename, $match)) {
      $name = $match[1];
      $type = $match[2];
      if (!in_array(strtolower($type), ['gif', 'jpeg', 'jpg', 'png'])) {
        $this->user->name = $current_user;
        return ['error' => 'Format not supported.'];
      }
    }
    else {
      $this->user->name = $current_user;
      return ['error' => 'Filename does not have correct format.'];
    }

    $filename = $name . '.' . $type;
    $image_path = $this->PublicDirectory($filename);
    $thumb_path = $this->PublicDirectory($name . '_thumb.' . $type);
    if (!file_exists($image_path)) {
      $this->user->name = $current_user;
      return ['error' => 'Image file not found.'];
    }
    if (!file_exists($thumb_path)) {
      $this->user->name = $current_user;
      return ['error' => 'Thumbnail file not found.'];
    }

    if ($type === 'gif') {
      $image = imagecreatefromgif($image_path);
      $rotate = imagerotate($image, $angle, 0);
      imagegif($rotate, $image_path);
      imagedestroy($image);
      imagedestroy($rotate);

      $thumb = imagecreatefromgif($thumb_path);
      $rotate = imagerotate($thumb, $angle, 0);
      imagegif($rotate, $thumb_path);
      imagedestroy($thumb);
      imagedestroy($rotate);
    }
    else if ($type === 'jpg' || $type === 'jpeg') {
      $image = imagecreatefromjpeg($image_path);
      $rotate = imagerotate($image, $angle, 0);
      imagejpeg($rotate, $image_path);
      imagedestroy($image);
      imagedestroy($rotate);

      $thumb = imagecreatefromjpeg($thumb_path);
      $rotate = imagerotate($thumb, $angle, 0);
      imagejpeg($rotate, $thumb_path);
      imagedestroy($thumb);
      imagedestroy($rotate);
    }
    else if ($type === 'png') {
      $image = imagecreatefrompng($image_path);
      $rotate = imagerotate($image, $angle, 0);
      imagealphablending($rotate, false);
      imagesavealpha($rotate, true);
      imagepng($rotate, $image_path);
      imagedestroy($image);
      imagedestroy($rotate);

      $thumb = imagecreatefrompng($thumb_path);
      $rotate = imagerotate($thumb, $angle, 0);
      imagealphablending($rotate, false);
      imagesavealpha($rotate, true);
      imagepng($rotate, $thumb_path);
      imagedestroy($thumb);
      imagedestroy($rotate);
    }
    // Return the path to the image, making sure it's not cached, and track this
    // image as rotated so that the cache is skipped next time the browser
    // module is loaded.
    $html_path = $this->user->name === 'admin' ? '/public/' :
      '/' . $this->user->name . '/public/';
    if (isset($_SESSION['browser-rotated']) &&
        !in_array($html_path, $_SESSION['browser-rotated'])) {
      $_SESSION['browser-rotated'][] = $name . '_thumb.' . $type;
    }
    else {
      $_SESSION['browser-rotated'] = [$name . '_thumb.' . $type];
    }
    $thumb_path = $html_path . $name . '_thumb.' . $type . '?update=' .time();
    $image_path = $html_path . $filename . '?update=' . time();
    if ($current_user !== $this->user->name && $current_user !== 'admin') {
      $this->Notification('browser', 'Browser', 'User: ' . $current_user .
                            ' rotated: <a href="/' . $this->user->name .
                            '/public/' . $filename . '">' . $filename . '</a>',
                          'system');
    }
    $this->user->name = $current_user;
    return ['src' => $thumb_path, 'filename' => $image_path];
  }

}
