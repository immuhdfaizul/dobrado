/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.xero) {
  dobrado.xero = {};
}
(function() {

  'use strict';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.xero').length === 0) {
      return;
    }

    $('.xero-token-toggle').click(function() {
      if ($('#xero-token-input').attr('type') === 'text') {
        $('#xero-token-input').attr('type', 'password');
      }
      else {
        $('#xero-token-input').attr('type', 'text');
      }
      return false;
    });
    $('.xero-secret-toggle').click(function() {
      if ($('#xero-secret-input').attr('type') === 'text') {
        $('#xero-secret-input').attr('type', 'password');
      }
      else {
        $('#xero-secret-input').attr('type', 'text');
      }
      return false;
    });
    $('.xero-public-key-toggle').click(function() {
      $('#xero-public-key-input').parent().toggle();
      return false;
    });
    $('.xero-private-key-toggle').click(function() {
      $('#xero-private-key-input').parent().toggle();
      return false;
    });
    $('#xero-diagnostics').button().click(diagnostics);
    $('#xero-submit').button().click(submit);
  });

  function diagnostics() {
    $.post('/php/request.php', { request: 'xero',
                                 action : 'diagnostics',
                                 url: location.href,
                                 token: dobrado.token },

      function(response) {
        if (dobrado.checkResponseError(response, 'xero diagnostics')) {
          return;
        }
        var xero = JSON.parse(response);
        if (xero.status) {
          $('.xero-diagnostics-info').html(xero.status);
        }
        else {
          $('.xero-diagnostics-info').html('Xero config OK.');
        }
      });
    return false;
  }

  function submit() {
    var formData = new FormData();
    if (!formData) {
      dobrado.log('Your browser doesn\'t support file uploading.', 'error');
      return;
    }

    dobrado.log('Submitting config...', 'info');
    var publicKeyCert = $('#xero-public-key-input').get(0).files[0];
    var privateKeyCert = $('#xero-private-key-input').get(0).files[0];
    if (publicKeyCert) {
      formData.append('publicKeyCert', publicKeyCert);
    }
    if (privateKeyCert) {
      formData.append('privateKeyCert', privateKeyCert);
    }
    formData.append('enabled', $('#xero-enabled-input:checked').length);
    formData.append('key', $('#xero-token-input').val());
    formData.append('secret', $('#xero-secret-input').val());
    formData.append('request', 'xero');
    formData.append('action', 'submit');
    formData.append('url', location.href);
    formData.append('token', dobrado.token);
    $.ajax({
      url: '/php/request.php',
      data: formData,
      contentType: false,
      processData: false,
      type: 'POST',
      success: function(response) {
        if (dobrado.checkResponseError(response, 'xero submit')) {
          return;
        }
        // Diagnostics are returned when details are updated.
        var xero = JSON.parse(response);
        if (xero.status) {
          $('.xero-diagnostics-info').html(xero.status);
        }
        else {
          $('.xero-diagnostics-info').html('Xero config updated.');
        }
      }
    });
    return false;
  }
  
})();
