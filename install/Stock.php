<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Stock extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view stock.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'list') return $this->AllData();
    if ($us_action === 'save') return $this->Save();
    if ($us_action === 'download') return $this->Download();
    if ($us_action === 'edit') return $this->EditProduct();
    if ($us_action === 'editMultiple') return $this->EditMultiple();
    if ($us_action === 'remove') return $this->RemoveProduct();
    if ($us_action === 'import') return $this->ImportData();
    if ($us_action === 'listAdjustments') return $this->ListAdjustments();
    if ($us_action === 'listAllAdjustments') return $this->ListAllAdjustments();
    if ($us_action === 'saveAdjustment') return $this->SaveAdjustment();
    if ($us_action === 'exportAdjustments') return $this->ExportAdjustments();
    if ($us_action === 'updateGroup') return $this->UpdateGroup();
    if ($us_action === 'changeGroup') return $this->ChangeGroup();
    if ($us_action === 'changeProfile') return $this->ChangeProfile();
    if ($us_action === 'addProfile') return $this->AddProfile();
    if ($us_action === 'removeProfile') return $this->RemoveProfile();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    // Need admin privileges to add the stock module.
    if (!$this->user->canEditSite) return false;
    // Can only have one stock module on a page.
    return !$this->AlreadyOnPage('stock', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $default_group = $this->user->group;
    $purchase_group = isset($_SESSION['purchase-group']) ?
      $_SESSION['purchase-group'] : '';
    $content = '<p id="stock-display-group">';
    // Display a group select if this user has created invite groups.
    $invite = new Invite($this->user, $this->owner);
    $created = $invite->Created();
    if (count($created) > 0) {
      if (in_array($purchase_group, $created)) {
        $this->user->group = $purchase_group;
      }
      $content .= 'Displaying stock for ' .
        '<select id="stock-group-select">' .
          '<option value="' . $default_group . '">' .
            $this->Substitute('group-name', '', '', $default_group) .
          '</option>';
      foreach ($created as $group) {
        if ($group === $this->user->group) {
          $content .= '<option selected="selected" value="' . $group . '">' .
            $this->Substitute('group-name') . '</option>';
        }
        else {
          $content .= '<option value="' . $group . '">' .
            $this->Substitute('group-name', '', '', $group) . '</option>';
        }
      }
      $content .= '</select>';
    }

    $products = 'available';
    if (isset($_SESSION['stock-products'])) {
      $products = $_SESSION['stock-products'];
    }
    else {
      $_SESSION['stock-products'] = $products;
    }
    $available_checked = $products === 'available' ? ' checked="checked"' : '';
    $all_checked = $products === 'all' ? ' checked="checked"' : '';
    $hidden_checked = $products === 'hidden' ? ' checked="checked"' : '';

    $stock_data_price = '';
    if ($this->Substitute('stock-order-update') !== '') {
      // Default to displaying order prices when both are used.
      $current = 'order';
      if (isset($_SESSION['stock-price'])) {
        $current = $_SESSION['stock-price'];
      }
      else {
        $_SESSION['stock-price'] = $current;
      }
      $order_checked = $current === 'order' ? ' checked="checked"' : '';
      $current_checked = $current === 'current' ? ' checked="checked"' : '';
      $stock_data_price = '<fieldset><legend>Prices:</legend>' .
          '<input type="radio" id="stock-data-order-price" ' .
            'name="stock-data-price"' . $order_checked . '>' .
          '<label for="stock-data-order-price">order</label>' .
          '<input type="radio" id="stock-data-purchase-price" ' .
            'name="stock-data-price"' . $current_checked . '>' .
          '<label for="stock-data-purchase-price">current</label>' .
        '</fieldset>';
    }
    $content .= '<span id="stock-data-type">' . $this->DataType() .
      '</span></p>';

    $base = '';
    $extra_pricing = '';
    $extra_pricing_columns = '';
    if ($this->Substitute('stock-wholesale-percent') !== '') {
      $base = 'Cost ';
      $extra_pricing .=
        '<div class="form-spacing">' .
          '<label for="stock-wholesale-input">Wholesale Price:</label>' .
          '<input id="stock-wholesale-input" type="text" maxlength="12">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-wholesale-markup-input">Wholesale Markup:</label>'.
          '<input id="stock-wholesale-markup-input" type="text" ' .
            'maxlength="12">' .
        '</div>';
      $extra_pricing_columns .=
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-wholesaleMarkup">' .
          '<label for="stock-column-wholesaleMarkup">' .
            'Wholesale Markup</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-wholesale">' .
          '<label for="stock-column-wholesale">Wholesale Price</label>' .
        '</div>';
    }
    if ($this->Substitute('stock-retail-percent') !== '') {
      $base = 'Cost ';
      $extra_pricing .=
        '<div class="form-spacing">' .
          '<label for="stock-retail-input">Retail Price:</label>' .
          '<input id="stock-retail-input" type="text" maxlength="12">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-retail-markup-input">Retail Markup:</label>' .
          '<input id="stock-retail-markup-input" type="text" maxlength="12">' .
        '</div>';
      $extra_pricing_columns .=
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-retailMarkup">' .
          '<label for="stock-column-retailMarkup">Retail Markup</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-retail">' .
          '<label for="stock-column-retail">Retail Price</label>' .
        '</div>';
    }

    $order_label = 'Available to Members';
    $order_column_label = 'Member Availability';
    if ($this->Substitute('stock-available-name') === 'customers') {
      $order_label = 'Available to Customers';
      $order_column_label = 'Customer Availability';
    }
    // The checkboxes in the product form are hidden so that user can select
    // which options they want to update, without changing all of them.
    // (Note that the value of these options must match the checkbox id.)
    $checkbox_options = '<option value="">Select a field...</option>' .
      '<option value="order-available">' . $order_label . '</option>';
    $purchase_available = '';
    $purchase_available_column = '';
    if ($this->Substitute('stock-order-available') === 'true') {
      $order_label = 'Available to Order';
      $order_column_label = 'Order Availability';
      $purchase_available =
        '<div class="form-spacing hidden">' .
          '<label for="stock-purchase-available-input">' .
            'Available to Purchase:</label>' .
          '<input id="stock-purchase-available-input" type="checkbox">' .
        '</div>';
      $purchase_available_column =
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-purchaseAvailable">' .
          '<label for="stock-column-purchaseAvailable">Purchase Availability' .
          '</label>' .
        '</div>';
      $checkbox_options = '<option value="">Select a field...</option>' .
        '<option value="order-available">' . $order_label . '</option>' .
        '<option value="purchase-available">Available to Purchase</option>';
    }
    $checkbox_options .=
      '<option value="supplier-available">Available from Supplier</option>';

    $taxable = '';
    $taxable_column = '';
    $taxable_label = $this->Substitute('stock-taxable');
    if ($taxable_label !== '') {
      $taxable =
        '<div class="form-spacing hidden">' .
          '<label for="stock-taxable-input">' .
            $taxable_label . ':</label>' .
          '<input id="stock-taxable-input" type="checkbox">' .
        '</div>';
      $taxable_column =
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-taxable">' .
          '<label for="stock-column-taxable">Tax Status</label>' .
        '</div>';
      $checkbox_options .=
        '<option value="taxable">' . $taxable_label . '</option>';
    }
    $composite = '';
    $composite_column = '';
    if ($this->Substitute('stock-show-composite') === 'true') {
      $composite =
        '<div class="form-spacing hidden">' .
          '<input id="stock-composite-input" type="checkbox">' .
          '<label for="stock-composite-input">Composite Item:</label>' .
        '</div>';
      $composite_column =
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-composite">' .
          '<label for="stock-column-composite">Composite Status</label>' .
        '</div>';
      $checkbox_options .= '<option value="composite">Composite Item</option>';
    }
    $bulk = '';
    $bulk_column = '';
    if ($this->Substitute('stock-bulk') === 'true') {
      $bulk =
        '<div class="form-spacing hidden">' .
          '<label for="stock-bulk-input">Bulk Item:</label>' .
          '<input id="stock-bulk-input" type="checkbox">' .
        '</div>';
      $bulk_column =
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-bulk">' .
          '<label for="stock-column-bulk">Bulk Status</label>' .
        '</div>';
      $checkbox_options .= '<option value="bulk">Bulk Item</option>';
    }
    $cart = '';
    $cart_column = '';
    if ($this->Substitute('stock-cart') === 'true') {
      $cart =
        '<div class="form-spacing hidden">' .
          '<label for="stock-cart-input">Sync to Cart:</label>' .
          '<input id="stock-cart-input" type="checkbox">' .
        '</div>';
      $cart_column =
        '<div class="form-spacing">' .
          '<input type="checkbox" id="stock-column-cart">' .
          '<label for="stock-column-cart">Sync to Cart</label>' .
        '</div>';
      $checkbox_options .= '<option value="cart">Sync to Cart</option>';
    }
    $checkbox_options .= '<option value="hidden">Hidden</option>';

    list($quantity, $quantity_columns, $quantity_dialog) =
      $this->TrackQuantity();
    $add_supplier = $this->GroupMember('admin', 'admin') ?
      '<button id="stock-add-supplier">Add Supplier</button>' : '';

    $wide_grid_button = ' class="hidden"';
    if (isset($this->user->settings['stock']['displayWideGridButton']) &&
        $this->user->settings['stock']['displayWideGridButton'] === 'display') {
      $wide_grid_button = '';
    }

    $content .=
      '<div id="stock-buttons">' .
        '<label for="stock-show-import">Import</label>' .
        '<input id="stock-show-import" type="checkbox">' .
        '<span id="stock-import-wrapper"><select id="stock-import-supplier">' .
          $this->SupplierSelect() . '</select>' .
          '<input type="text" id="stock-import-input">' .
          '<input id="stock-import-file" type="file">' .
        '</span>' .
        '<span' . $wide_grid_button . '>' .
          '<input type="checkbox" id="stock-wide-grid">' .
          '<label for="stock-wide-grid">Wide grid</label>' .
        '</span>' .
        '<span id="stock-other-buttons-wrapper">' .
          '<button id="stock-open-display-form">Edit Display</button>' .
          '<button id="stock-open-product-form">Open Product Form</button>' .
          '<span id="stock-profile-download">' .
            '<select id="stock-profiles">' .
              $this->CustomProfiles() .
            '</select> ' .
            '<button id="stock-download">Download</button> ' .
          '</span>' .
        '</span>' .
      '</div>' .
      '<form id="stock-display" class="hidden" autocomplete="off">' .
        '<input type="checkbox" id="stock-import-help">' .
        '<label for="stock-import-help">Help</label>' .
        '<div class="stock-import-info hidden">' .
          $this->Substitute('stock-import-info') .
        '</div>' .
        '<div id="stock-data">' .
          '<h4>Change the data displayed in the grid:</h4>' .
          $stock_data_price .
          '<fieldset><legend>Products:</legend>' .
            '<input type="radio" id="stock-data-available-products" ' .
              'name="stock-data-products"' . $available_checked . '>' .
            '<label for="stock-data-available-products">' .
              'available from supplier only</label>' .
            '<input type="radio" id="stock-data-all-products" ' .
              'name="stock-data-products"' . $all_checked . '>' .
            '<label for="stock-data-all-products">' .
              'all except hidden</label>' .
            '<input type="radio" id="stock-data-hidden-products" ' .
              'name="stock-data-products"' . $hidden_checked . '>' .
            '<label for="stock-data-hidden-products">' .
              'hidden only</label>' .
          '</fieldset>' .
        '</div>' .
        '<h4>Change the columns displayed in the grid:</h4>' .
        '<div id="stock-columns">' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-supplier">' .
            '<label for="stock-column-supplier">Supplier</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-unit">' .
            '<label for="stock-column-unit">Unit</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-unitPrice">' .
            '<label for="stock-column-unitPrice">Pack Price</label>' .
          '</div>' .
          $quantity_columns .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-size">' .
            '<label for="stock-column-size">Pack Size</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-alternative">' .
            '<label for="stock-column-alternative">Alternative</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-price">' .
            '<label for="stock-column-price">' . $base . 'Price</label>' .
          '</div>' .
          $extra_pricing_columns .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-category">' .
            '<label for="stock-column-category">Category</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-description">' .
            '<label for="stock-column-description">Description</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-image">' .
            '<label for="stock-column-image">Image</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-grower">' .
            '<label for="stock-column-grower">Grower</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-available">' .
            '<label for="stock-column-available">' . $order_column_label .
            '</label>' .
          '</div>' .
          $purchase_available_column .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-supplierAvailable">' .
            '<label for="stock-column-supplierAvailable">' .
              'Supplier Availability</label>' .
          '</div>' .
          $taxable_column . $composite_column . $bulk_column . $cart_column .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="stock-column-hidden">' .
            '<label for="stock-column-hidden">Hidden Status</label>' .
          '</div>' .
        '</div>' .
        '<p>If you would like to store your column choices, you can save ' .
          'them as a profile. ' .
          '<button id="stock-save-profile">Save Profile</button></p>' .
      '</form>' .
      '<form id="stock-form" class="hidden" autocomplete="off">' .
        '<button id="stock-default-action" class="hidden">' .
          'default action is submit</button>' .
        '<div id="stock-form-buttons">' .
          '<button id="stock-form-previous">Previous</button>' .
          '<button id="stock-form-clear">New Product</button>' .
          $add_supplier .
          '<button id="stock-form-next">Next</button>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-name-input">Product:</label>' .
          '<input id="stock-name-input" type="text" maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-user-input">Supplier:</label>' .
          '<input id="stock-user-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-category-input">Category:</label>' .
          '<input id="stock-category-input" type="text" maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-grower-input">Grower:</label>' .
          '<input id="stock-grower-input" type="text" maxlength="200">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-image-input">Image:</label>' .
          '<input id="stock-image-input" type="text" maxlength="200"> ' .
          '<button id="stock-image-browse">browse</button>' .
        '</div>' .
        $quantity .
        '<div class="form-spacing">' .
          '<label for="stock-checkbox-select">Update:</label>' .
          '<select id="stock-checkbox-select">' . $checkbox_options .
          '</select>' .
        '</div>' .
        '<div id="stock-checkbox-wrapper">' .
          '<div class="form-spacing hidden">' .
            '<label for="stock-order-available-input">' .
              $order_label . ':</label>' .
            '<input id="stock-order-available-input" type="checkbox">' .
          '</div>' .
          $purchase_available .
          '<div class="form-spacing hidden">' .
            '<label for="stock-supplier-available-input">' .
              'Available from Supplier:</label>' .
            '<input id="stock-supplier-available-input" type="checkbox">' .
          '</div>' .
          $taxable . $composite . $bulk . $cart .
          '<div class="form-spacing hidden">' .
            '<label for="stock-hidden-input">Hidden:</label>' .
            '<input id="stock-hidden-input" type="checkbox">' .
          '</div>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-unit-select">Unit:</label>' .
          '<select id="stock-unit-select">' .
            '<option value="none"></option>' .
            '<option value="each">each</option>' .
            '<option value="kg">kg</option>' .
            '<option value="g">grams</option>' .
            '<option value="L">litre</option>' .
            '<option value="100mL">100mL</option>' .
            '<option value="variable">as marked</option>' .
            '<option value="adjusted">random weight</option>' .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-size-input">Pack Size:</label>' .
          '<input id="stock-size-input" type="text" maxlength="12">' .
          '<label for="stock-alternative">alternative</label>' .
          '<input type="checkbox" id="stock-alternative">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-price-input">' . $base . 'Price:</label>' .
          '<input id="stock-price-input" type="text" maxlength="12">' .
          '<span id="stock-order-price-info"></span>' .
        '</div>' .
        '<div id="stock-extra-pricing">' .
          $extra_pricing .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="stock-description-textarea">Description:</label>' .
          '<textarea id="stock-description-textarea"></textarea>' .
        '</div>' .
        '<button class="submit">submit</button>' .
        '<button class="remove">remove</button>' .
      '</form>' .
      '<form id="stock-profile-dialog" class="hidden" autocomplete="off">' .
        '<div id="stock-profile-options" class="hidden">' .
          '<p>You can update the current profile ' .
            '<span id="stock-current-profile"></span> or create a new one.' .
          '</p>' .
          '<button id="stock-profile-update">Update Profile</button> ' .
          '<button id="stock-profile-new">New Profile</button> ' .
        '</div>' .
        '<div id="stock-profile-create" class="hidden">' .
          '<p>The currently displayed columns can be saved as a profile, ' .
            'please provide a name so that it can be added to the menu:</p>' .
          '<div class="form-spacing">' .
            '<label for="stock-profile-input">Profile Name:</label>' .
            '<input id="stock-profile-input" type="text" maxlength="100">' .
          '</div>' .
          '<button id="stock-profile-add">add</button>' .
        '</div>' .
        $this->CustomProfiles('remove') .
      '</form>' .
      '<div class="stock-info">' .
        '<span class="stock-new-available"></span>' .
        '<button class="stock-list-all hidden">show all</button>' .
      '</div>' .
      $quantity_dialog;

    $this->user->group = $default_group;
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {
    if (!$this->Run(date('H:00:00'))) return;

    $mysqli = connect_db();
    // Order availability is imported by the user and then used here to
    // overwrite purchase availabilty.
    $run_groups = $this->RunGroups('stock-order-update');
    for ($i = 0; $i < count($run_groups); $i++) {
      $group = $run_groups[$i]['group'];
      $timezone = $run_groups[$i]['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      $category_query = '';
      // Allow the update to be done for only a limited list of categories.
      // Note that templates are html encoded so decode them here.
      $us_categories = $this->Substitute('stock-update-categories');
      if ($us_categories !== '') {
        $us_category_list =
          explode(',', htmlspecialchars_decode($us_categories));
        foreach ($us_category_list as $us_category) {
          if ($category_query !== '') {
            $category_query .= ' OR ';
          }
          $category = $mysqli->escape_string(trim($us_category));
          $category_query .= 'category = "' . $category . '"';
        }
        if ($category_query !== '') {
          $category_query = '(' . $category_query . ') AND ';
        }
      }
      $query = 'UPDATE stock LEFT JOIN users ON stock.user = users.user SET ' .
        'purchase_available = order_available WHERE ' . $category_query .
        'system_group = "' . $group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Cron 1: ' . $mysqli->error);
      }
      if ($this->Substitute('stock-order-available') === 'true') {
        $query = 'UPDATE stock LEFT JOIN users ON stock.user = users.user ' .
          'INNER JOIN stock_order_price ON stock.name = stock_order_price.name'.
          ' AND stock.user = stock_order_price.user SET unit = order_unit, ' .
          'pack_size = order_pack_size, base_price = order_base_price, ' .
          'wholesale_price = order_wholesale_price, retail_price = ' .
          'order_retail_price WHERE system_group = "' . $group . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->Cron 2: ' . $mysqli->error);
        }
        // Automatically update items tracked by the Cart module too.
        $query = 'SELECT stock_order_price.name, stock_order_price.user FROM ' .
          'stock_order_price LEFT JOIN stock ON stock_order_price.name = ' .
          'stock.name AND stock_order_price.user = stock.user LEFT JOIN ' .
          'users ON stock_order_price.user = users.user WHERE ' .
          'system_group = "' . $group . '" AND cart = 1';
        if ($mysqli_result = $mysqli->query($query)) {
          while ($stock_order_price = $mysqli_result->fetch_assoc()) {
            $name = $mysqli->escape_string($stock_order_price['name']);
            $user = $mysqli->escape_string($stock_order_price['user']);
            $this->UpdateCart($name, $user);
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Stock->Cron 3: ' . $mysqli->error);
        }
        // Delete from stock_order_price table after the update, so that prices
        // are only updated each week for items found in any imported lists.
        $query = 'DELETE stock_order_price FROM stock_order_price LEFT JOIN ' .
          'users ON stock_order_price.user = users.user WHERE ' .
          'system_group = "' . $group . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->Cron 4: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();
  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'AddSupplyGroup' && $count === 2) {
        $user = $p[0];
        $group = $p[1];
        return $this->AddSupplyGroup($user, $group);
      }
      if ($fn === 'RemoveSupplyGroup' && $count === 2) {
        $user = $p[0];
        $group = $p[1];
        return $this->RemoveSupplyGroup($user, $group);
      }
      if ($fn === 'UpdateCart' && $count === 2) {
        $product = $p[0];
        $supplier = $p[1];
        return $this->UpdateCart($product, $supplier);
      }
      return;
    }
    if ($fn === 'AvailableProducts') {
      // These parameters are set for the Cart module, which is the only module
      // currently calling this function via the Factory method.
      return $this->AvailableProducts(false, false, true);
    }
    if ($fn === 'ShowSupplyGroups') return $this->ShowSupplyGroups();
    if ($fn === 'AllSuppliers') return $this->AllSuppliers($p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.stock.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.stock.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS stock (' .
      'name VARCHAR(100) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'unit ENUM("each", "kg", "L", "variable", "g", "adjusted", "100mL") ' .
        'NOT NULL,' .
      'pack_size DECIMAL(8,3) NOT NULL,' .
      'base_price DECIMAL(8,2) NOT NULL,' .
      'wholesale_price DECIMAL(8,2) NOT NULL,' .
      'retail_price DECIMAL(8,2) NOT NULL,' .
      'category VARCHAR(100),' .
      'description TEXT,' .
      'image VARCHAR(200),' .
      'grower VARCHAR(200),' .
      'order_available TINYINT(1),' .
      'purchase_available TINYINT(1),' .
      'supplier_available TINYINT(1),' .
      'taxable TINYINT(1),' .
      'quantity DECIMAL(8,3) NOT NULL,' .
      'composite TINYINT(1) NOT NULL,' .
      'track TINYINT(1),' .
      'bulk TINYINT(1),' .
      'hidden TINYINT(1),' .
      'cart TINYINT(1),' .
      'PRIMARY KEY(name, user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS stock_history (' .
      'name VARCHAR(100) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'unit ENUM("each", "kg", "L", "variable", "g", "adjusted", "100mL") ' .
        'NOT NULL,' .
      'pack_size DECIMAL(8,3) NOT NULL,' .
      'base_price DECIMAL(8,2) NOT NULL,' .
      'wholesale_price DECIMAL(8,2) NOT NULL,' .
      'retail_price DECIMAL(8,2) NOT NULL,' .
      'category VARCHAR(100),' .
      'description TEXT,' .
      'image VARCHAR(200),' .
      'grower VARCHAR(200),' .
      'order_available TINYINT(1),' .
      'purchase_available TINYINT(1),' .
      'supplier_available TINYINT(1),' .
      'taxable TINYINT(1),' .
      'quantity DECIMAL(8,3) NOT NULL,' .
      'composite TINYINT(1) NOT NULL,' .
      'track TINYINT(1),' .
      'bulk TINYINT(1),' .
      'hidden TINYINT(1),' .
      'cart TINYINT(1),' .
      'action ENUM("edit", "remove"),' .
      'modified_by VARCHAR(50),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(name, user, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 2: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS stock_supply_group (' .
      'user VARCHAR(50) NOT NULL,' .
      'system_group VARCHAR(50),' .
      'PRIMARY KEY(user, system_group)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 3: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS stock_adjustment (' .
      'name VARCHAR(100) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'adjustment DECIMAL(8,3) NOT NULL,' .
      'description TEXT,' .
      'modified_by VARCHAR(50),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(name, user, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 4: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS stock_alternative (' .
      'name VARCHAR(100) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'half_size DECIMAL(8,3) NOT NULL,' .
      'half_price DECIMAL(8,2) NOT NULL,' .
      'full_size DECIMAL(8,3) NOT NULL,' .
      'full_price DECIMAL(8,2) NOT NULL,' .
      'PRIMARY KEY(name, user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 5: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS stock_order_price (' .
      'name VARCHAR(100) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'order_unit ENUM("each", "kg", "L", "variable", "g", "adjusted", ' .
        '"100mL") NOT NULL,' .
      'order_pack_size DECIMAL(8,3) NOT NULL,' .
      'order_base_price DECIMAL(8,2) NOT NULL,' .
      'order_wholesale_price DECIMAL(8,2) NOT NULL,' .
      'order_retail_price DECIMAL(8,2) NOT NULL,' .
      'PRIMARY KEY(name, user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 6: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS stock_column_profile (' .
      'name VARCHAR(100) NOT NULL,' .
      'value VARCHAR(100) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'columns TEXT NOT NULL,' .
      'sort_columns TEXT NOT NULL,' .
      'PRIMARY KEY(name, user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Install 7: ' . $mysqli->error);
    }

    $us_info = '<p><b>Using Import</b><br><br>To use the import function you ' .
        'first need to have created accounts for each of your suppliers. If ' .
        'you haven\'t done that, you can create them using the <b>add ' .
        'supplier</b> button in the product form. Once you\'ve done this, ' .
        'browse for the file you want to import. After a successful import ' .
        'the grid will show you products that have become available. This ' .
        'will be the full list the first time you import a file for a ' .
        'supplier, but usually only a few products change their availability ' .
        'week to week. You can return to the full list of products by ' .
        'clicking the <b>show all</b> button.</p>' .
      '<p><b>Availability</b><br><br>Next you can update any products that ' .
        'should be made available to your members. Clicking in the grid will ' .
        'allow you to edit a checkbox inline for each product. This will ' .
        'save automatically as you move to the next product. You can also ' .
        'hold down <b>shift</b> or <b>ctrl</b> as you click to select ' .
        'multiple rows, and the product form will open automatically for ' .
        'you. When changing multiple items at once, only the fields that you ' .
        'enter values for will be updated. The rest will keep the existing ' .
        'values for each item.</p>' .
      '<p>By default only products that are available from suppliers are ' .
        'shown in the grid. To display all products from suppliers except ' .
        'those you\'ve set as hidden, click the button next to <b>all ' .
        'except hidden</b> in this dialog. It\'s in the first section below, ' .
        'titled \'Change the data displayed in the grid\'. You can also ' .
        'choose to view hidden products only. You might choose to hide ' .
        'products from a supplier that were imported from their list, but ' .
        'that your group aren\'t interested in. To do that, see the next ' .
        'section in this dialog, \'Change the columns displayed in the ' .
        'grid\'. Clicking the checkbox next to <b>Hidden Status</b> will add ' .
        'that column to the grid so you can then change the value. If you ' .
        'change your mind about a product you can use view \'hidden only\' ' .
        'to update the value and make it available again.</p>' .
      '<p><b>Alternatives</b><br><br>Products with alternative pack sizes ' .
        'and prices can be listed in the import file (they must be on ' .
        'consecutive lines). To see which products have alternatives click ' .
        'the checkbox next to \'Alternatives\' in the list of columns  ' .
        'displayed in the grid. You can switch between the two options by ' .
        'opening the product form and clicking the <b>alternative</b> ' .
        'button, which is found next to Pack Size. Your preferred values ' .
        'will be saved, along with any other changes you\'ve made to the '.
        'product, when you click submit.</p>' .
      '<p><b>File Types</b><br><br>For those wanting to know more about the ' .
        'file types used when importing, there are two different formats ' .
        'available. One is for products from a single supplier, the other ' .
        'contains products from multiple suppliers. The format for the first ' .
        'file is csv <i>(comma separated values)</i> with the first entry ' .
        'being the header row containing:<br>Name,Description,Quantity,Size,' .
        'Unit,UnitPrice,Total,Taxable,Grower,Category</p>' .
      '<p>Note that the supplier is not listed here. When using this format ' .
        'you pick the supplier from a list <i>before importing</i>. If you ' .
        'want to use this format to share your product list, also note that ' .
        'the unit price is calculated by multiplying the pack size by the ' .
        '<i>wholesale price</i>.</p>' .
      '<p>The second import format is also csv, however since it contains ' .
        'the usernames of your suppliers, you don\'t need to select one when ' .
        'importing. The header row for this format is:<br>Name,User,' .
        'Description,Quantity,Price,Wholesale,Retail,Size,Unit,Taxable,' .
        'Grower,Category,<br>Available,PurchaseAvailable,SupplierAvailable</p>'.
      '<p>These formats can also be exported by switching to one of the two ' .
        'default profiles and then clicking the download button.</p>';
    $template = ['"stock-taxable","","Attracts GST"',
                 '"stock-not-taxable","","Exempt from GST"',
                 '"stock-taxable-header","","taxable"',
                 '"stock-import-info","",' .
                 '"' . $mysqli->escape_string($us_info) . '"',
                 '"stock-update-orders","","true"'];
    $mysqli->close();

    $this->AddTemplate($template);
    $description = ['stock-order-available' => 'The string \'true\' or ' .
                      '\'false\', when \'true\' two availability checkboxes ' .
                      'are shown on the stock page and the purchase page ' .
                      'will show only items that are available depending on ' .
                      'which mode it is in.',
                    'stock-order-update' => 'When stock-order-available is ' .
                      'true, this is the day when available to purchase is ' .
                      'updated from available to order.',
                    'stock-taxable' => 'The label used for the tax checkbox ' .
                      'on the stock page.',
                    'stock-not-taxable' => 'This is used as a filter label ' .
                      'on the manager page.',
                    'stock-taxable-header' => 'Sets a custom header when ' .
                      'exporting data containing the taxable column.',
                    'stock-wholesale-percent' => 'The default percent markup ' .
                      'when automatically setting the wholesale price.',
                    'stock-retail-percent' => 'The default percent markup ' .
                      'when automatically setting the retail price.',
                    'stock-group-select' => 'The string \'true\' or ' .
                      '\'false\' to show options to select which groups a ' .
                      'member can supply in the \'Manage Accounts\' dialog.',
                    'stock-import-orders-only' => 'The string \'true\' or ' .
                      '\'false\' to show a checkbox on the stock page that ' .
                      'when true, will only import rows that have a value in ' .
                      'their quantity column.',
                    'stock-track-quantity' => 'The string \'true\' or ' .
                      '\'false\' to show extra stock tracking options on the ' .
                    'stock page.',
                    'stock-update-orders' => 'The string \'true\' or ' .
                      '\'false\' to automatically update existing orders ' .
                      'when the order price is changed on the stock page.'];
    $this->AddTemplateDescription($description);

    // This is a default media query for mobile devices.
    $media = '@media screen and (max-device-width: 480px)';
    $site_style = ['"","#stock-form label","width","14em"',
                   '"","#stock-form label[for=stock-alternative]",' .
                     '"width","auto"',
                   '"","#stock-form label[for=stock-alternative]",' .
                     '"float","none"',
                   '"","#stock-form label[for=stock-alternative]",' .
                     '"margin-left","5px"',
                   '"","#stock-form label[for=stock-alternative]","top","-4px"',
                   '"","label[for=stock-track-input]","float","none"',
                   '"","label[for=stock-track-input]","margin-left","5px"',
                   '"","#stock-form .submit","float","right"',
                   '"",".stock-quantity-dialog .export","float","right"',
                   '"",".stock-adjust","margin-top","15px"',
                   '"",".stock-info","padding","5px"',
                   '"",".stock-info > button","margin-left","10px"',
                   '"",".stock-price-info","float","right"',
                   '"",".stock-price-info","font-size","0.9em"',
                   '"",".stock-price-info","color","#555555"',
                   '"",".stock-price-info","margin-top","5px"',
                   '"",".stock-price-info a","color","#555555"',
                   '"","#stock-group-select-button","width","180px"',
                   '"","label[for=stock-import-help]","float","right"',
                   '"","#stock-import-wrapper","display","inline-block"',
                   '"","#stock-import-supplier-button","width","200px"',
                   '"","#stock-import-input","width","170px"',
                   '"","#stock-import-file","max-width","240px"',
                   '"","#stock-buttons","display","flex"',
                   '"","#stock-buttons","justify-content","space-evenly"',
                   '"","#stock-buttons","align-items","start"',
                   '"","#stock-profiles-button","width","100px"',
                   '"","#stock-other-buttons-wrapper > *","margin-left","5px"',
                   '"","#stock-data fieldset","margin-top","10px"' ,
                   '"","#stock-data label","margin-right","10px"' ,
                   '"","#stock-checkbox-wrapper","display","grid"',
                   '"' . $media . '","#stock-checkbox-wrapper","display",' .
                     '"block"',
                   '"","#stock-checkbox-wrapper","grid-template-columns",' .
                     '"max-content 1fr"',
                   '"","#stock-columns","display","grid"',
                   '"","#stock-columns","grid-template-columns","50% 50%"',
                   '"","#stock-columns label","float","none"',
                   '"","#stock-form-buttons","display","flex"',
                   '"","#stock-form-buttons","justify-content","space-between"',
                   '"","#stock-form-buttons","margin-bottom","10px"',
                   '"","#stock-profile-add","margin-left","6.4em"',
                   '"",".stock-profile-remove","margin-right","10px"',
                   '"","#stock-quantity-reason-input","width","98%"',
                   '"","#stock-checkbox-select-button","width","180px"',
                   '"","#stock-unit-select-button","width","120px"',
                   '"","#stock-display-group","text-align","center"',
                   '"","#stock-data-type","margin-left","10px"',
                   '"","#stock-current-profile","font-weight","bold"',
                   '"","#stock-size-input","width","70px"',
                   '"","#stock-price-input","width","70px"',
                   '"","#stock-extra-pricing","display","grid"',
                   '"' . $media . '","#stock-extra-pricing","display","block"',
                   '"","#stock-extra-pricing","grid-template-columns",' .
                     '"max-content 1fr"',
                   '"","#stock-wholesale-input","width","70px"',
                   '"","#stock-form label[for=stock-wholesale-markup-input]",' .
                     '"width","10em"',
                   '"","#stock-wholesale-markup-input","width","30px"',
                   '"","#stock-retail-input","width","70px"',
                   '"","#stock-form label[for=stock-retail-markup-input]",' .
                     '"width","10em"',
                   '"","#stock-retail-markup-input","width","30px"',
                   '"","#stock-form label[for=stock-description-textarea]",' .
                     '"float","none"'];
    $this->AddSiteStyle($site_style);
    $this->AddSettingTypes(['"stock","displayWideGridButton",' .
                              '"display,hidden","radio","When the wide grid ' .
                              'button is hidden the grid is set to the full ' .
                              'width of the window automatically."']);
    return $this->Dependencies(['detail', 'invite', 'purchase']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    // Return if Remove was called for a specific module,
    // only want to remove stock when deleting an account.
    if (isset($id)) return;

    // Note that deleting accounts with existing stock must be done
    // very carefully because it can effect the balance of other users.
    $mysqli = connect_db();
    $query = 'DELETE FROM stock WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Remove 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock_history WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Remove 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock_supply_group WHERE user = "' . $this->owner.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Remove 3: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock_adjustment WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Remove 4: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock_alternative WHERE user = "' . $this->owner .'"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Remove 5: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock_order_price WHERE user = "' . $this->owner .'"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Remove 6: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.stock.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function AllProducts($array = true,
                              $purchase_group = '', $show_hidden = true) {
    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $hidden_query = $show_hidden === true ? '' : 'hidden = 0 AND ';
    // Some modules want all products in all groups, others want all products
    // in a limited set of groups.
    $group_query = '';
    if ($purchase_group === '') {
      $group_query .= $organiser->GroupQuery();
      foreach ($invite->AllBuyingGroups() as $group) {
        $group_query .= ' OR system_group = "' . $group . '"';
      }
    }
    else if ($purchase_group === $this->user->group) {
      $group_query .= $organiser->GroupQuery();
    }
    else if (in_array($purchase_group, $invite->AllBuyingGroups())) {
      $group_query .= 'system_group = "' . $purchase_group . '"';
    }
    else {
      // This shouldn't happen so just set to the user's own group.
      $group_query .= 'system_group = "' . $this->user->group . '"';
    }

    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT name, stock.user, unit, base_price, wholesale_price, ' .
      'retail_price, category, order_available, purchase_available, taxable, ' .
      'composite, hidden FROM stock LEFT JOIN users ON ' .
      'stock.user = users.user WHERE ' . $hidden_query .
      '(' . $group_query . ') ORDER BY name, user';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        $available = (int)$stock['order_available'] === 1 ||
          (int)$stock['purchase_available'] === 1;
        $product = [];
        if ($stock['unit'] === 'adjusted') {
          $product = $this->ChangeUnits($stock);
          $product['available'] = $available;
        }
        else {
          $product = ['unit' => $stock['unit'],
                      'price' => (float)$stock['base_price'],
                      'wholesale' => (float)$stock['wholesale_price'],
                      'retail' => (float)$stock['retail_price'],
                      'category' => $stock['category'],
                      'available' => $available,
                      'taxable' => (int)$stock['taxable'],
                      'composite' => (int)$stock['composite'],
                      'hidden' => (int)$stock['hidden']];
        }
        if ($array) {
          $product['user'] = $stock['user'];
          $product['name'] = $stock['name'];
          $result[] = $product;
        }
        else {
          $user = $stock['user'];
          $name = $stock['name'];
          if (!isset($result[$user])) {
            $result[$user] = [];
          }
          $result[$user][$name] = $product;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AllProducts: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function AllSuppliers($all_groups = false) {
    $query = '';
    // Suppliers across all groups are required for multi-group buys.
    if ($all_groups) {
      $query = 'SELECT DISTINCT user FROM stock';
    }
    else {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT DISTINCT stock.user FROM stock LEFT JOIN users ON ' .
        'stock.user = users.user WHERE ' . $organiser->GroupQuery();
    }

    $result = [];
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        $result[] = $stock['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AllSuppliers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function AllTaxable() {
    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $group_query = $organiser->GroupQuery();
    foreach ($invite->AllBuyingGroups() as $group) {
      $group_query .= ' OR system_group = "' . $group . '"';
    }

    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT name, stock.user FROM stock LEFT JOIN users ON ' .
      'stock.user = users.user WHERE (' . $group_query . ') AND taxable = 1';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        $user = $stock['user'];
        $name = $stock['name'];
        if (!isset($result[$user])) {
          $result[$user] = [];
        }
        $result[$user][$name] = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AllTaxable: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function AvailableProducts($array = true,
                                    $purchase = true, $cart = false) {
    $organiser = new Organiser($this->user, $this->owner);
    $available_query = 'AND order_available = 1';
    $order_available = $this->Substitute('stock-order-available') === 'true';
    // Only use purchase_available if requested and both availability options
    // are being used. Otherwise order_available is the default.
    if ($purchase && $order_available) {
      $available_query = 'AND purchase_available = 1';
    }
    if ($cart) {
      $available_query .= ' AND cart = 1';
    }
    // The first part of this query retrieves prices from the stock table and
    // possibly also the stock_order_price table depending on group settings
    // (and the purchase parameter). Also the cart module does not want order
    // prices here because it is always provided with the current price.
    $query = '';
    if (!$purchase && !$cart && $order_available) {
      $query = 'SELECT stock.name, stock.user, order_unit, unit, ' .
        'order_pack_size, pack_size, order_base_price, base_price, ' .
        'order_wholesale_price, wholesale_price, order_retail_price, ' .
        'retail_price, category, image, grower, quantity, track ' .
        'FROM stock LEFT JOIN stock_order_price ON stock.name = ' .
        'stock_order_price.name AND stock.user = stock_order_price.user ';
    }
    else {
      $query = 'SELECT name, stock.user, unit, pack_size, base_price, ' .
        'wholesale_price, retail_price, category, image, grower, quantity, ' .
        'track FROM stock ';
    }
    // The second part of the query finds all available products where the
    // suppliers are members of the same organisation, and then further filters
    // the set by making sure each supplier found supplies to the current user's
    // group (or the empty group, which is used to specify that they supply all
    // groups). Note that the system_group check will produce duplicates if
    // both are found, so the stock_supply_group table should never store both
    // the empty system_group and a list of specific groups (must be one or the
    // other).
    $query .= 'LEFT JOIN users ON stock.user = users.user LEFT JOIN ' .
      'stock_supply_group ON stock_supply_group.user = stock.user WHERE ' .
      'composite = 0 AND ' . $organiser->GroupQuery() . ' AND ' .
      '(stock_supply_group.system_group = "' . $this->user->group . '" OR ' .
      'stock_supply_group.system_group = "") ' . $available_query .
      ' ORDER BY name, user';

    $result = [];
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        // If this product has values set in the stock_order_price table,
        // use them in preference to the stock table here. (Assume that if
        // the first value is not null, that this row is safe to use).
        if (!$purchase && !$cart &&
            $order_available && isset($stock['order_unit'])) {
          $stock['unit'] = $stock['order_unit'];
          $stock['pack_size'] = $stock['order_pack_size'];
          $stock['base_price'] = $stock['order_base_price'];
          $stock['wholesale_price'] = $stock['order_wholesale_price'];
          $stock['retail_price'] = $stock['order_retail_price'];
        }
        $product = [];
        // When unit is 'adjusted' the purchase page shows 'each' when ordering
        // and per weight when purchasing. The stock page makes sure the
        // quantity and units are part of the product name to do this.
        if ($stock['unit'] === 'adjusted') {
          $product = $this->ChangeUnits($stock, $purchase, false);
        }
        else {
          $product = ['user' => $stock['user'],
                      'unit' => $stock['unit'],
                      'size' => (float)$stock['pack_size'],
                      'price' => (float)$stock['base_price'],
                      'wholesale' => (float)$stock['wholesale_price'],
                      'retail' => (float)$stock['retail_price'],
                      'category' => $stock['category'],
                      'image' => $stock['image'],
                      'grower' => $stock['grower'],
                      'quantity' => $stock['quantity'],
                      'track' => (int)$stock['track'] === 1];
        }
        // The grid module requires this data as an indexed array, but it's
        // also useful to have an associative array keyed on product names.
        if ($array) {
          $product['name'] = $stock['name'];
          $result[] = $product;
        }
        else {
          // Note here that even though product names aren't unique, we can
          // still key off them because we make sure only one product is
          // available at a time where the names are the same.
          $result[$stock['name']] = $product;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AvailableProducts: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function Composite($product, $user) {
    $composite = false;
    $mysqli = connect_db();
    $query = 'SELECT composite FROM stock WHERE name = "' . $product . '" ' .
      'AND user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock = $mysqli_result->fetch_assoc()) {
        $composite = (int)$stock['composite'] === 1;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->Composite: ' . $mysqli->error);
    }
    $mysqli->close();
    return $composite;
  }

  public function Decrease($product_list) {
    if ($this->Substitute('stock-track-quantity') !== 'true') return;

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $mysqli = connect_db();
    foreach ($product_list as $user => $product) {
      if (!$organiser->MatchUser($user, $invite->AllBuyingGroups())) {
        $this->Log('Stock->Decrease 1: Supplier not found');
        continue;
      }
      foreach ($product as $name => $quantity) {
        $query = 'UPDATE stock SET quantity = quantity - ' . $quantity .
          ' WHERE name = "' . $name . '" AND user = "' . $user . '" AND ' .
          'track = 1';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->Decrease 2: ' . $mysqli->error);
        }
      }
    }
    // TODO: Need to check total quantity of tracked items that are synced with
    // the cart module and update their availability if stock-limited is used.
    $mysqli->close();
  }

  public function Grower($name, $user) {
    $grower = '';
    $mysqli = connect_db();
    $query = 'SELECT grower FROM stock WHERE name = "' . $name . '" AND ' .
      'user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock = $mysqli_result->fetch_assoc()) {
        $grower = $stock['grower'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->Grower: ' . $mysqli->error);
    }
    $mysqli->close();
    return $grower;
  }

  public function Increase($product_list) {
    if ($this->Substitute('stock-track-quantity') !== 'true') return;

    $organiser = new Organiser($this->user, $this->owner);
    $invite = new Invite($this->user, $this->owner);
    $mysqli = connect_db();
    foreach ($product_list as $user => $product) {
      if (!$organiser->MatchUser($user, $invite->AllBuyingGroups())) {
        $this->Log('Stock->Increase 1: Supplier not found');
        continue;
      }
      // TODO: Need to get the quantity before the increase for each product,
      // and if adding the new quantity makes total quantity greater than zero
      // update the cart module when the item is synced and stock-limited used.
      foreach ($product as $name => $quantity) {
        $query = 'UPDATE stock SET quantity = quantity + ' . $quantity .
          ' WHERE name = "' . $name . '" AND user = "' . $user . '" AND ' .
          'track = 1';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->Increase 2: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();
  }

  public function LastUpdate() {
    $mysqli = connect_db();
    $timestamp = 0;
    $query = 'SELECT MAX(timestamp) AS timestamp FROM stock_history ' .
      'LEFT JOIN users ON stock_history.user = users.user WHERE ' .
      'system_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock_history = $mysqli_result->fetch_assoc()) {
        $timestamp = (int)$stock_history['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->LastUpdate: ' . $mysqli->error);
    }
    $mysqli->close();
    return $timestamp;
  }

  public function Supplier($user = '', $no_variable_pricing = false) {
    if ($user === '') $user = $this->user->name;

    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT name FROM stock WHERE user = "' . $user . '"';
    if ($no_variable_pricing) {
      $query .= ' AND unit != "variable"';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        $result[] = $stock['name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->Supplier: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function SupplierVariablePricing($user = '') {
    if ($user === '') $user = $this->user->name;

    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT name FROM stock WHERE user = "' . $user . '" ' .
      'AND unit = "variable"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        $result[] = $stock['name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->Supplier: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  public function SurchargeSupplier() {
    $organiser = new Organiser($this->user, $this->owner);
    $supplier = '';
    $mysqli = connect_db();
    $query = 'SELECT stock.user FROM stock LEFT JOIN users ON ' .
      'stock.user = users.user WHERE name = "surcharge" AND ' .
      $organiser->GroupQuery();
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock = $mysqli_result->fetch_assoc()) {
        $supplier = $stock['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->SurchargeSupplier: ' . $mysqli->error);
    }
    $mysqli->close();
    return $supplier;
  }

  public function Quota($name, $user, $quantity) {
    $quota = false;
    $mysqli = connect_db();
    $query = 'SELECT pack_size FROM stock WHERE name = "' . $name . '" AND ' .
      'user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock = $mysqli_result->fetch_assoc()) {
        $quota = $quantity >= (float)$stock['pack_size'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->Quota: ' . $mysqli->error);
    }
    $mysqli->close();
    return $quota;
  }

  public function UpdateMarkup() {
    $organiser = new Organiser($this->user, $this->owner);
    $group_query = $organiser->GroupQuery();
    $wholesale_percent =
      (100 + (float)$this->Substitute('stock-wholesale-percent')) / 100;
    $retail_percent =
      (100 + (float)$this->Substitute('stock-retail-percent')) / 100;

    $mysqli = connect_db();
    $query = 'UPDATE stock LEFT JOIN users ON stock.user = users.user SET ' .
      'wholesale_price = base_price * ' . $wholesale_percent .
      ', retail_price = base_price * ' . $retail_percent . ' WHERE ' .
      $group_query;
    if (!$mysqli->query($query)) {
      $this->Log('Stock->UpdateMarkup 1: ' . $mysqli->error);
    }
    $query = 'UPDATE stock_order_price LEFT JOIN users ON ' .
      'stock_order_price.user = users.user SET ' .
      'order_wholesale_price = order_base_price * ' . $wholesale_percent .
      ', order_retail_price = order_base_price * ' . $retail_percent .
      ' WHERE ' . $group_query;
    if (!$mysqli->query($query)) {
      $this->Log('Stock->UpdateMarkup 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddProfile() {
    $us_name = $_POST['name'];
    if ($us_name === '' || $us_name === 'edit') {
      return ['error' => 'Profile name not available.'];
    }
    $us_value = strtolower(preg_replace('/[[:^alnum:]]/', '-', $us_name));

    $mysqli = connect_db();
    $columns = $mysqli->escape_string($_POST['columns']);
    $sort = $mysqli->escape_string($_POST['sort']);
    $name = $mysqli->escape_string($us_name);
    $value = $mysqli->escape_string($us_value);
    $query = 'INSERT INTO stock_column_profile VALUES ("' . $name . '", ' .
      '"' . $value . '", "' . $this->user->name . '", "' . $columns . '", ' .
      '"' . $sort . '") ON DUPLICATE KEY UPDATE columns = "' . $columns . '", '.
      'sort_columns = "' . $sort . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->AddProfile: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['options' => $this->CustomProfiles()];
  }

  private function AllData($new_available = [], $selected_only = false) {
    if (!isset($_POST['columns']) || !isset($_POST['sort'])) {
      return ['error' => 'Column settings not found.'];
    }

    $default_group = $this->user->group;
    $this->SetPurchaseGroup();
    $organiser = new Organiser($this->user, $this->owner);
    $group_query = $organiser->GroupQuery();
    $detail = new Detail($this->user, $this->owner);
    $all_suppliers = $detail->AllSuppliers();

    $us_column_options = json_decode($_POST['columns'], true);
    $us_sort_options = json_decode($_POST['sort'], true);
    $show_supplier = in_array('supplier', $us_column_options);
    $show_quantity = in_array('quantity', $us_column_options);
    $show_price = in_array('price', $us_column_options);
    $show_wholesale = in_array('wholesale', $us_column_options);
    $show_retail = in_array('retail', $us_column_options);
    $show_unit = in_array('unit', $us_column_options);
    $show_unit_price = in_array('unitPrice', $us_column_options);
    $show_size = in_array('size', $us_column_options);
    $show_alternative = in_array('alternative', $us_column_options);
    $show_category = in_array('category', $us_column_options);
    $show_description = in_array('description', $us_column_options);
    $show_image = in_array('image', $us_column_options);
    $show_grower = in_array('grower', $us_column_options);
    $show_taxable = in_array('taxable', $us_column_options);
    $show_composite = in_array('composite', $us_column_options);
    $show_track = in_array('track', $us_column_options);
    $show_bulk = in_array('bulk', $us_column_options);
    $show_cart = in_array('cart', $us_column_options);
    $show_hidden = in_array('hidden', $us_column_options);
    $show_available = in_array('available', $us_column_options);
    $show_purchase_available =
      in_array('purchaseAvailable', $us_column_options);
    $show_supplier_available =
      in_array('supplierAvailable', $us_column_options);

    $mysqli = connect_db();
    $profile = $mysqli->escape_string($_POST['profile']);
    $markdown = 0;
    $exclude_categories = [];
    if ($profile === 'available-retail') {
      // This profile is to allow creating an availability list using retail
      // prices and a specified markdown, instead of wholesale prices. There
      // can also be a list of categories where the markdown is not applied.
      $markdown =
        (float)$this->Substitute('stock-available-retail-markdown') / 100;
      $categories_text = $this->Substitute('stock-available-retail-categories');
      $exclude_categories =
        explode(',', htmlspecialchars_decode($categories_text));
    }
    // Autocomplete users and products.
    $users_list = [];
    $query = 'SELECT user FROM users WHERE active = 1 AND user NOT LIKE ' .
      '"buyer\_%" AND (' . $group_query . ')';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $users_list[] = $users['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AllData 1: ' . $mysqli->error);
    }

    // List products with alternative pack_size and prices.
    $alternatives_list = [];
    $query = 'SELECT name, stock_alternative.user, half_size, half_price, ' .
      'full_size, full_price FROM stock_alternative LEFT JOIN users ON ' .
      'stock_alternative.user = users.user WHERE active = 1 AND ' .$group_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock_alternative = $mysqli_result->fetch_assoc()) {
        $user = $stock_alternative['user'];
        $name = $stock_alternative['name'];
        if (!isset($alternatives_list[$user])) {
          $alternatives_list[$user] = [];
        }
        $alternatives_list[$user][$name] =
          ['halfSize' => $stock_alternative['half_size'],
          'halfPrice' => $stock_alternative['half_price'],
          'fullSize' => $stock_alternative['full_size'],
          'fullPrice' => $stock_alternative['full_price']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AllData 2: ' . $mysqli->error);
    }

    $data = [];
    $new_count = 0;
    foreach ($new_available as $user => $product_list) {
      if (is_array($product_list)) $new_count += count($product_list);
    }
    if (isset($_POST['dataType'])) {
      $data_type = $mysqli->escape_string($_POST['dataType']);
      if ($data_type === 'stock-data-available-products') {
        $_SESSION['stock-products'] = 'available';
      }
      else if ($data_type === 'stock-data-all-products') {
        $_SESSION['stock-products'] = 'all';
      }
      else if ($data_type === 'stock-data-hidden-products') {
        $_SESSION['stock-products'] = 'hidden';
      }
      else if ($data_type === 'stock-data-order-price') {
        $_SESSION['stock-price'] = 'order';
      }
      else if ($data_type === 'stock-data-purchase-price') {
        $_SESSION['stock-price'] = 'current';
      }
    }
    $show_products = $_SESSION['stock-products'];
    $hidden_query = $show_products === 'hidden' ?
      ' AND hidden = 1' : ' AND hidden = 0';
    $available_query = $show_products === 'available' ?
      ' AND supplier_available = 1' : '';
    // When one of the 'available' profiles are used, list products that are
    // available to order (or purchase if not available-wholesale profile).
    if ($profile === 'available' || $profile === 'available-retail') {
      $available_query = ' AND (order_available = 1 OR purchase_available = 1)'.
        ' AND supplier_available = 1';
    }
    else if ($profile === 'available-wholesale') {
      $available_query = ' AND order_available = 1 AND supplier_available = 1';
    }
    if (isset($_SESSION['stock-price']) &&
        $_SESSION['stock-price'] === 'order') {
      $query = 'SELECT stock.name, stock.user, order_unit, unit, ' .
        'order_pack_size, pack_size, order_base_price, base_price, ' .
        'order_wholesale_price, wholesale_price, order_retail_price, ' .
        'retail_price, category, description, image, grower, order_available, '.
        'purchase_available, supplier_available, taxable, quantity, ' .
        'composite, track, bulk, cart, hidden FROM stock LEFT JOIN ' .
        'stock_order_price ON stock.name = stock_order_price.name AND ' .
        'stock.user = stock_order_price.user LEFT JOIN users ON ' .
        'stock.user = users.user';
    }
    else {
      $query = 'SELECT name, stock.user, unit, pack_size, base_price, ' .
        'wholesale_price, retail_price, category, description, image, grower, '.
        'order_available, purchase_available, supplier_available, taxable, ' .
        'quantity, composite, track, bulk, cart, hidden FROM stock LEFT JOIN ' .
        'users ON stock.user = users.user';
    }
    $query .= ' WHERE active = 1 AND ' . $group_query . $hidden_query .
      $available_query . ' ORDER BY name, user';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock = $mysqli_result->fetch_assoc()) {
        $name = $stock['name'];
        $user = $stock['user'];
        // When there's at least one newly available item show only new items.
        if ($new_count !== 0 && !isset($new_available[$user][$name])) continue;

        $unit = isset($stock['order_unit']) ?
          $stock['order_unit'] : $stock['unit'];
        $size = isset($stock['order_pack_size']) ?
          $stock['order_pack_size'] : $stock['pack_size'];
        $base_price = isset($stock['order_base_price']) ?
          (float)$stock['order_base_price'] : (float)$stock['base_price'];
        $base_price = price_string($base_price);
        $wholesale_price = isset($stock['order_wholesale_price']) ?
          (float)$stock['order_wholesale_price'] :
          (float)$stock['wholesale_price'];
        $retail_price = isset($stock['order_retail_price']) ?
          (float)$stock['order_retail_price'] : (float)$stock['retail_price'];
        $unit_price = $wholesale_price;
        if ($profile === 'available-retail') {
          $unit_price = $retail_price;
          if ($markdown !== 0 &&
              !in_array($stock['category'], $exclude_categories)) {
            $unit_price -= $retail_price * $markdown;
          }
        }
        $pack_size = (float)$size;
        // Leave the pack size empty rather than show a zero.
        if ($pack_size > 0.001) $unit_price *= $pack_size;
        else $pack_size = '';
        $wholesale_price = price_string($wholesale_price);
        $retail_price = price_string($retail_price);
        $unit_price = price_string($unit_price);
        $alternative = isset($alternatives_list[$user][$name]) ? 1 : 0;
        if ($selected_only) {
          // This is used when downloading data to create a file containing
          // the selected columns only. Note that the 'available' profile adds
          // some extra columns, quantity and total which are both empty.
          // Also the taxable column can be given a custom header in CreateCSV,
          // but requires the generic key 'data'.
          $product = ['name' => $name];
          if ($show_supplier) $product['user'] = $user;
          if ($show_description) {
            $product['description'] = $stock['description'];
          }
          if ($profile === 'available' || $profile === 'available-retail') {
            $product['quantity'] = '';
          }
          else if ($show_quantity) {
            $product['quantity'] = (float)$stock['quantity'];
          }
          if ($show_price) $product['price'] = $base_price;
          if ($show_wholesale) $product['wholesale'] = $wholesale_price;
          if ($show_retail) $product['retail'] = $retail_price;
          if ($show_size) $product['size'] = $pack_size;
          if ($show_unit) $product['unit'] = $unit;
          if ($show_unit_price) $product['unitPrice'] = $unit_price;
          // available-wholesale format wants quantity after unitPrice.
          if ($profile === 'available-wholesale') {
            $product['quantity'] = '';
          }
          if ($profile === 'available' ||
              $profile === 'available-retail' ||
              $profile === 'available-wholesale') {
            $product['total'] = '';
          }
          if ($show_alternative) $product['alternative'] = $alternative;
          if ($show_image) $product['image'] = $stock['image'];
          if ($show_taxable) $product['data'] = (int)$stock['taxable'];
          if ($show_grower) $product['grower'] = $stock['grower'];
          if ($show_category) $product['category'] = $stock['category'];
          if ($show_composite) $product['composite'] = (int)$stock['composite'];
          if ($show_track) $product['track'] = (int)$stock['track'];
          if ($show_bulk) $product['bulk'] = (int)$stock['bulk'];
          if ($show_cart) $product['cart'] = (int)$stock['cart'];
          if ($show_hidden) $product['hidden'] = (int)$stock['hidden'];
          if ($show_available) {
            $product['available'] = (int)$stock['order_available'];
          }
          if ($show_purchase_available) {
            $product['purchaseAvailable'] = (int)$stock['purchase_available'];
          }
          if ($show_supplier_available) {
            $product['supplierAvailable'] = (int)$stock['supplier_available'];
          }
          // The filters used in the grid are also passed in here, so use them
          // to also filter the download data.
          if ($this->Filter($product)) continue;
          $data[] = $product;
        }
        else {
          $fullname =
            trim(htmlspecialchars_decode($all_suppliers[$user]['first']) . ' ' .
                 htmlspecialchars_decode($all_suppliers[$user]['last']));
          if ($fullname === '') $fullname = $user;
          $data[] = ['id' => $name . '-' . $user, 'name' => $name,
                     'user' => $user, 'fullname' => $fullname,
                     'description' => $stock['description'],
                     'quantity' => (float)$stock['quantity'],
                     'price' => $base_price, 'wholesale' => $wholesale_price,
                     'retail' => $retail_price, 'size' => (float)$size,
                     'unit' => $unit, 'unitPrice' => $unit_price,
                     'category' => $stock['category'],
                     'image' => $stock['image'], 'grower' => $stock['grower'],
                     'taxable' => (int)$stock['taxable'],
                     'composite' => (int)$stock['composite'],
                     'track' => (int)$stock['track'],
                     'bulk' => (int)$stock['bulk'],
                     'cart' => (int)$stock['cart'],
                     'hidden' => (int)$stock['hidden'],
                     'available' => (int)$stock['order_available'],
                     'purchaseAvailable' => (int)$stock['purchase_available'],
                     'supplierAvailable' => (int)$stock['supplier_available'],
                     'alternative' => $alternative];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->AllData 3: ' . $mysqli->error);
    }
    $mysqli->close();

    // array_multisort works by sorting $data according to the reference arrays
    // created by array_column. Note that wholesaleMarkup and retailMarkup
    // are sort options that can be set here but aren't values in $data so
    // trying to create an array from them would cause an error.
    if (is_array($us_sort_options) && count($us_sort_options) > 0) {
      $args = [];
      foreach ($us_sort_options as $sort) {
        $id = $sort['columnId'];
        if ($id === 'product') $id = 'name';
        else if ($id === 'supplier') $id = 'user';
        else if ($selected_only && $id === 'taxable') $id = 'data';
        else if ($id === 'wholesaleMarkup' || $id === 'retailMarkup') continue;

        $args[] = array_column($data, $id);
        $args[] = $sort['sortAsc'] ? SORT_ASC : SORT_DESC;
      }
      $args[] = &$data;
      call_user_func_array('array_multisort', $args);
    }
    $wholesale_percent = (float)$this->Substitute('stock-wholesale-percent');
    $retail_percent = (float)$this->Substitute('stock-retail-percent');
    $track_quantity = $this->Substitute('stock-track-quantity') === 'true';
    $order_available = $this->Substitute('stock-order-available') === 'true';
    $available_name = $this->Substitute('stock-available-name');

    $this->user->group = $default_group;
    return ['wholesalePercent' => $wholesale_percent,
            'retailPercent' => $retail_percent,
            'trackQuantity' => $track_quantity,
            'orderAvailable' => $order_available, 'products' => $data,
            'users' => $users_list, 'alternative' => $alternatives_list,
            'newCount' => $new_count, 'dataType' => $this->DataType(),
            'availableName' => $available_name];
  }

  private function ChangeProfile() {
    $columns = [];
    $sort = [];
    $mysqli = connect_db();
    $value = $mysqli->escape_string($_POST['value']);
    $query = 'SELECT columns, sort_columns FROM stock_column_profile WHERE ' .
      'value = "' . $value . '" AND user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($profile = $mysqli_result->fetch_assoc()) {
        $columns = json_decode($profile['columns'], true);
        $sort = json_decode($profile['sort_columns'], true);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->ChangeProfile: ' . $mysqli->error);
    }
    $mysqli->close();

    if (count($columns) !== 0) return ['columns' => $columns, 'sort' => $sort];

    // Return default profiles here if not found.
    if ($value === 'available' || $value === 'available-retail') {
      return ['columns' => ['product', 'description', 'size', 'unit',
                            'unitPrice', 'taxable', 'grower', 'category'],
              'sort' => [['columnId' => 'category', 'sortAsc' => true]]];
    }
    if ($value === 'available-wholesale') {
      return ['columns' => ['product', 'unit', 'unitPrice', 'taxable', 'grower',
                            'category'],
              'sort' => [['columnId' => 'product', 'sortAsc' => true]]];
    }
    if ($value === 'all') {
      return ['columns' => ['product', 'supplier', 'description', 'quantity',
                            'price', 'wholesale', 'retail', 'size', 'unit',
                            'taxable', 'grower', 'category', 'available',
                            'purchaseAvailable', 'supplierAvailable' ],
              'sort' => [['columnId' => 'supplier', 'sortAsc' => true],
                         ['columnId' => 'product', 'sortAsc' => true]]];
    }
    return ['columns' => ['product', 'supplier', 'price', 'unit', 'available'],
            'sort' => [['columnId' => 'product', 'sortAsc' => true]]];
  }

  private function ChangeUnits($stock, $purchase = true, $all_products = true) {
    // The unit 'each' is used when ordering and as a fallback if no match.
    $unit = 'each';
    $quantity = 1;
    $name = $stock['name'];
    $price = (float)$stock['base_price'];
    $wholesale = (float)$stock['wholesale_price'];
    $retail = (float)$stock['retail_price'];
      
    $matches = [];
    $regex = '/([\d\.]+)\s*(each|g|kg|kilo|l)/i';
    if ($purchase && preg_match($regex, $name, $matches)) {
      $quantity = (float)$matches[1];
      $unit = strtolower($matches[2]);
      // Fix the extra cases we allow for units.
      if ($unit === 'kilo') {
        $unit = 'kg';
      }
      else if ($unit === 'l') {
        $unit = 'L';
      }
      else if ($unit === 'g') {
        // Special case for grams, if the quantity is greater than 99 grams
        // convert to kgs so that this product doesn't get priced per gram.
        if ($quantity > 99) {
          $quantity /= 1000;
          $unit = 'kg';
        }
      }
      // Convert the 'each' price to a per unit price.
      $price /= $quantity;
      $wholesale /= $quantity;
      $retail /= $quantity;
    }
    // The purchase quantity also needs to be adjusted, but this is done in the
    // browser when purchases are loaded for each user.
    if ($all_products) {
      return ['unit' => $unit,
              'price' => $price,
              'wholesale' => $wholesale,
              'retail' => $retail,
              'category' => $stock['category'],
              'taxable' => (int)$stock['taxable'],
              'composite' => (int)$stock['composite'],
              'hidden' => (int)$stock['hidden'],
              'quantityAdjustment' => $quantity];
    }
    return ['user' => $stock['user'],
            'unit' => $unit,
            'size' => (float)$stock['pack_size'],
            'price' => $price,
            'wholesale' => $wholesale,
            'retail' => $retail,
            'category' => $stock['category'],
            'image' => $stock['image'],
            'grower' => $stock['grower'],
            'quantity' => $stock['quantity'],
            'track' => (int)$stock['track'] === 1,
            'quantityAdjustment' => $quantity];
  }

  private function CustomProfiles($type = 'options') {
    $content = '';
    $export_available = false;
    $export_retail = false;
    $export_wholesale = false;
    $export_all = false;
    $mysqli = connect_db();
    $query = 'SELECT name, value FROM stock_column_profile WHERE ' .
      'user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($profile = $mysqli_result->fetch_assoc()) {
        $value = $profile['value'];
        if ($value === 'available') {
          $export_available = true;
        }
        else if ($value === 'available-retail') {
          $export_retail = true;
        }
        else if ($value === 'available-wholesale') {
          $export_wholesale = true;
        }
        else if ($value === 'all') {
          $export_all = true;
        }
        else if ($type === 'options') {
          $content .= '<option value="' . $value . '">' . $profile['name'] .
            '</option>';
        }
        else if ($type === 'remove') {
          $content .= '<div class="form-spacing">' .
            '<button id="stock-profile-remove-' . $value . '" ' .
              'class="stock-profile-remove">remove</button>' .
            htmlspecialchars($profile['name']) . '</div>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->CustomProfiles: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($type === 'options') {
      $available_options = '';
      if ($this->Substitute('stock-available-retail-profile') === 'true') {
        $available_options .=
          '<option value="available-retail">Available-Retail</option>';
      }
      if ($this->Substitute('stock-available-wholesale-profile') === 'true') {
        $available_options .=
          '<option value="available-wholesale">Available-Wholesale</option>';
      }
      return '<option value="">Profiles</option>' .
        $content . $available_options .
        '<option value="available">Available</option>' .
        '<option value="all">All</option>' .
        '<option value="edit">Options...</option>';
    }

    if ($type === 'remove') {
      if ($content !== '') {
        $content = '<hr><p>You have created the profiles listed below. Click ' .
          'the remove button next to a name if you would like to remove them ' .
          'from the profile menu.</p>' . $content;
      }
      if ($export_available ||
          $export_retail || $export_wholesale || $export_all) {
        $content .= '<p>You have modified a default profile, if you would ' .
          'like to reset the profile to it\'s default options please click ' .
          'the button below.</p>';
      }
      if ($export_available) {
        $content .= '<button id="stock-reset-available">' .
          'Reset Available Profile</button>';
      }
      if ($export_retail) {
        $content .= '<button id="stock-reset-available-retail">' .
          'Reset Available-Retail Profile</button>';
      }
      if ($export_wholesale) {
        $content .= '<button id="stock-reset-available-wholesale">' .
          'Reset Available-Wholesale Profile</button>';
      }
      if ($export_all) {
        $content .= '<button id="stock-reset-all">Reset All Profile</button>';
      }
    }
    return $content;
  }

  private function DataType() {
    $result = 'Viewing <b>' . $_SESSION['stock-products'] . '</b> products';
    if (isset($_SESSION['stock-price'])) {
      $result .= ' at <b>' . $_SESSION['stock-price'] . '</b> prices';
    }
    return $result . '. Click Edit Display to change data.';
  }

  private function Download() {
    $date = date('Y-m-d');
    $us_profile = $_POST['profile'] === '' ? '' : $_POST['profile'] . '-';
    $us_filename = 'stock-' . $us_profile . $date . '.csv';
    $all_data = $this->AllData([], true);
    if ($this->CreateCSV($us_filename, $all_data['products'], false,
                         $this->Substitute('stock-taxable-header'))) {
      return ['filename' => $us_filename];
    }
    return ['error' => 'No data to download.'];
  }

  private function EditProduct() {
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $config->set('Attr.AllowedFrameTargets', ['_blank']);
    $purifier = new HTMLPurifier($config);
    $us_description = $purifier->purify($_POST['description']);

    $default_group = $this->user->group;
    $this->SetPurchaseGroup();

    $mysqli = connect_db();
    // Leave name unescaped to check for some characters that aren't allowed.
    $us_name = $_POST['name'];
    $user = $mysqli->escape_string($_POST['supplier']);
    $unit = $mysqli->escape_string($_POST['unit']);
    $size = (float)$_POST['size'];
    $base_price = (float)$_POST['price'];
    $category = $mysqli->escape_string($_POST['category']);
    $description = $mysqli->escape_string($us_description);
    $image = isset($_POST['image']) ?
      $mysqli->escape_string($_POST['image']) : '';
    $grower = $mysqli->escape_string($_POST['grower']);
    $order_available = (int)$_POST['orderAvailable'];
    $supplier_available = (int)$_POST['supplierAvailable'];
    $taxable = (int)$_POST['taxable'];
    $quantity = (float)$_POST['quantity'];
    $composite = (int)$_POST['composite'];
    $track = (int)$_POST['track'];
    $bulk = (int)$_POST['bulk'];
    $hidden = (int)$_POST['hidden'];
    $cart = (int)$_POST['cart'];
    // Wholesale and retail prices are only shown in the form if a percent
    // is set for them, so check the substitutions first.
    $retail_price = $base_price;
    $wholesale_price = $base_price;
    if ($this->Substitute('stock-wholesale-percent') !== '') {
      $wholesale_price = (float)$_POST['wholesale'];
      // Default to wholesale price when available as the retail price is the
      // default on the purchase page, and want to avoid showing cost price.
      $retail_price = $wholesale_price;
    }
    if ($this->Substitute('stock-retail-percent') !== '') {
      $retail_price = (float)$_POST['retail'];
    }
    // The purchase available checkbox is only shown in the form when both
    // availability options are being used. If not shown keep both options
    // the same in the stock table in case stock-order-available is switched on.
    $purchase_available = $order_available;
    if ($this->Substitute('stock-order-available') === 'true') {
      $purchase_available = (int)$_POST['purchaseAvailable'];
    }
    $mysqli->close();

    $result = $this->Edit($us_name, $user, $unit, $size, $base_price,
                          $wholesale_price, $retail_price, $category,
                          $description, $image, $grower, $order_available,
                          $purchase_available, $supplier_available, $taxable,
                          $quantity, $composite, $track, $bulk, $hidden, $cart);
    $this->user->group = $default_group;
    return $result;
  }

  private function Edit($us_name, $user, $unit, $size, $base_price,
                        $wholesale_price, $retail_price, $category,
                        $description, $image, $grower, $order_available,
                        $purchase_available, $supplier_available, $taxable,
                        $quantity, $composite, $track, $bulk, $hidden, $cart) {
    if ($us_name === '') {
      return ['error' => 'No product name given'];
    }
    if (strpos($us_name, '"') !== false) {
      return ['error' => 'Double quotes not allowed in product name'];
    }
    if (strpos($us_name, '\\') !== false) {
      return ['error' => 'Backslash not allowed in product name'];
    }
    if ($user === '') {
      return ['error' => 'No supplier given'];
    }
    if ($unit === '' || $unit === 'none') {
      return ['error' => 'No units given'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($user)) {
      return ['error' => 'Supplier not found'];
    }

    $mysqli = connect_db();
    // If this item is hidden, make sure it's unavailable.
    if ($hidden === 1) {
      $order_available = 0;
      $purchase_available = 0;
    }
    $other = '';
    $name = $mysqli->escape_string($us_name);
    // When making an item available, must make sure there isn't already
    // an item with the same name available by another user.
    if ($order_available === 1 || $purchase_available === 1) {
      $query = 'SELECT stock.user FROM stock LEFT JOIN users ON ' .
        'stock.user = users.user WHERE name = "' . $name . '" ' .
        'AND (order_available = 1 OR purchase_available = 1) ' .
        'AND stock.user != "' . $user . '" AND ' . $organiser->GroupQuery();
      if ($mysqli_result = $mysqli->query($query)) {
        if ($stock = $mysqli_result->fetch_assoc()) {
          $other = $stock['user'];
          $query = 'UPDATE stock SET order_available = 0, ' .
            'purchase_available = 0 WHERE user = "' . $other . '" AND ' .
            'name = "' . $name . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Stock->Edit 1: ' . $mysqli->error);
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Stock->Edit 2: ' . $mysqli->error);
      }
      // Also, when making a product available make sure it's also set as
      // available from the supplier.
      $supplier_available = 1;
    }
    $duplicate_query = 'unit = "' . $unit . '", pack_size = ' . $size . ', ' .
      'base_price = ' . $base_price . ', ' .
      'wholesale_price = ' . $wholesale_price . ', ' .
      'retail_price = ' . $retail_price . ', ';
    // If stock-order-update is used, then stock-price will be set here.
    // Save some fields in the stock_order_price table as they will be
    // required when updating the product from cron.
    if (isset($_SESSION['stock-price'])) {
      $query = 'INSERT INTO stock_order_price VALUES ("' . $name . '", ' .
        '"' . $user . '", "' . $unit . '", ' . $size . ', ' .
        $base_price . ', ' . $wholesale_price . ', ' . $retail_price .
        ') ON DUPLICATE KEY UPDATE order_unit = "' . $unit . '", ' .
        'order_pack_size = ' . $size . ', order_base_price = ' . $base_price .
        ', order_wholesale_price = ' . $wholesale_price . ', ' .
        'order_retail_price = ' . $retail_price;
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Edit 3: ' . $mysqli->error);
      }
      if ($_SESSION['stock-price'] === 'order' &&
          $this->Substitute('stock-update-orders') === 'true') {
        $purchase = new Purchase($this->user, $this->owner);
        $purchase->UpdateOrders($name, $user, $base_price,
                                $wholesale_price, $retail_price);
        // Don't update the above fields in the stock table when viewing order
        // prices, they aren't used until the time set by stock-order-update.
        $duplicate_query = '';
      }
    }
    $query = 'INSERT INTO stock VALUES ("' . $name . '", "' . $user . '", ' .
      '"' . $unit . '", ' . $size . ', ' . $base_price . ', ' .
      $wholesale_price . ', ' . $retail_price . ', "' . $category . '", ' .
      '"' . $description . '", "' . $image . '", "' . $grower . '", ' .
      $order_available . ', ' . $purchase_available . ', ' .
      $supplier_available . ', ' . $taxable . ', ' . $quantity . ', ' .
      $composite . ', ' . $track . ', ' . $bulk . ', ' . $hidden . ', ' .
      $cart . ') ON DUPLICATE KEY UPDATE ' . $duplicate_query .
      'category = "' . $category . '", description = "' . $description . '", ' .
      'image = "' . $image . '", grower = "' . $grower . '", ' .
      'order_available = ' . $order_available . ', ' .
      'purchase_available = ' . $purchase_available . ', ' .
      'supplier_available = ' . $supplier_available . ', ' .
      'taxable = ' . $taxable . ', quantity = quantity + ' . $quantity . ', ' .
      'composite = ' . $composite . ', track = ' . $track . ', ' .
      'bulk = ' . $bulk . ', hidden = ' . $hidden . ', cart = ' . $cart;
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Edit 4: ' . $mysqli->error);
    }
    // Automatically create a discount item when a composite item is added.
    // It's price is set to 'variable' as it's automatically calculated.
    if ($composite === 1) {
      $query = 'INSERT INTO stock VALUES ("' . $name . ' discount", ' .
        '"' . $user . '", "variable", 1, 0, 0, 0, "' . $category . '", "", ' .
        '"", "' . $grower . '", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) ' .
        'ON DUPLICATE KEY UPDATE category = "' . $category . '", ' .
        'grower = "' . $grower . '", hidden = ' . $hidden;
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Edit 5: ' . $mysqli->error);
      }
    }
    $query = 'INSERT INTO stock_history VALUES ("' . $name . '", ' .
      '"' . $user . '", "' . $unit . '", ' . $size . ', ' . $base_price . ', ' .
      $wholesale_price . ', ' . $retail_price . ', "' . $category . '", ' .
      '"' . $description . '", "' . $image . '", "' . $grower . '", ' .
      $order_available . ', ' . $purchase_available . ', ' .
      $supplier_available . ', ' . $taxable . ', ' . $quantity . ', ' .
      $composite . ', ' . $track . ', ' . $bulk . ', ' . $hidden . ', ' .
      $cart . ', "edit", "' . $this->user->name . '", ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Edit 6: ' . $mysqli->error);
    }
    // Allow admin users to perform system-wide image updates when an image is
    // provided and no other image is set for an item.
    if ($this->user->canEditSite && $image !== '') {
      $query = 'UPDATE stock SET image = "' . $image . '" WHERE ' .
        'name = "' . $name . '" AND image = ""';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Edit 7: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    $this->CheckSupplyGroup($user);

    if ($this->Substitute('stock-cart') === 'true') {
      if ($track === 1) {
        // TODO: Need to look up the current quantity and only make
        // available in the cart module if greater than zero when
        // stock-limited is used.
      }
      // Note that cart->UpdateItem can't be called directly here because
      // the current retail price was not updated if viewing order prices.
      return $this->UpdateCart($name, $user);
    }
    // Tell the client if a product from other supplier was made unavailable.
    return $other === '' ? ['done' => true] : ['other' => $other];
  }

  private function EditMultiple() {
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $config->set('Attr.AllowedFrameTargets', ['_blank']);
    $purifier = new HTMLPurifier($config);
    $us_description = $purifier->purify($_POST['description']);

    $default_group = $this->user->group;
    $this->SetPurchaseGroup();

    $mysqli = connect_db();
    $new_unit = $mysqli->escape_string($_POST['unit']);
    $new_size = $_POST['size'];
    if ($new_size !== '') $new_size = (float)$new_size;
    $new_base_price = (float)$_POST['price'];
    $new_category = $mysqli->escape_string($_POST['category']);
    $new_description = $mysqli->escape_string($us_description);
    $new_image = isset($_POST['image']) ?
      $mysqli->escape_string($_POST['image']) : '';
    $new_grower = $mysqli->escape_string($_POST['grower']);
    $order_available = (int)$_POST['orderAvailable'];
    $supplier_available = (int)$_POST['supplierAvailable'];
    $taxable = (int)$_POST['taxable'];
    $quantity = (float)$_POST['quantity'];
    $composite = (int)$_POST['composite'];
    $track = (int)$_POST['track'];
    $bulk = (int)$_POST['bulk'];
    $hidden = (int)$_POST['hidden'];
    $cart = (int)$_POST['cart'];
    // Wholesale and retail prices are only shown in the form if a percent is
    // set for them, so check the substitutions first. The markup can also be
    // provided here rather than using the default, and will be applied to the
    // existing price below when a price isn't set for the selected products.
    $wholesale_percent = '';
    $new_wholesale_price = $new_base_price;
    if ($this->Substitute('stock-wholesale-percent') !== '') {
      if ($_POST['wholesaleMarkup'] !== '') {
        $wholesale_percent = (int)$_POST['wholesaleMarkup'];
      }
      $new_wholesale_price = (float)$_POST['wholesale'];
    }
    $retail_percent = '';
    $new_retail_price = $new_base_price;
    if ($this->Substitute('stock-retail-percent') !== '') {
      if ($_POST['retailMarkup'] !== '') {
        $retail_percent = (int)$_POST['retailMarkup'];
      }
      $new_retail_price = (float)$_POST['retail'];
    }
    // The purchase available checkbox is only shown in the form when both
    // availability options are being used. If not shown keep both options
    // the same in the stock table in case stock-order-available is switched on.
    $purchase_available = $order_available;
    if ($this->Substitute('stock-order-available') === 'true') {
      $purchase_available = (int)$_POST['purchaseAvailable'];
    }
    // Get the existing values for checkboxes that haven't been updated.
    $us_update = json_decode($_POST['update'], true);
    $order_available_updated = in_array('order-available', $us_update);
    $purchase_available_updated = in_array('purchase-available', $us_update);
    $supplier_available_updated = in_array('supplier-available', $us_update);
    $taxable_updated = in_array('taxable', $us_update);
    $composite_updated = in_array('composite', $us_update);
    $bulk_updated = in_array('bulk', $us_update);
    $hidden_updated = in_array('hidden', $us_update);
    $cart_updated = in_array('cart', $us_update);

    $result = [];
    $us_data = json_decode($_POST['data'], true);
    for ($i = 0; $i < count($us_data); $i++) {
      // Leave name unescaped to check for some characters that aren't allowed.
      $us_name = $us_data[$i]['name'];
      $name = $mysqli->escape_string($us_name);
      $user = $mysqli->escape_string($us_data[$i]['supplier']);
      // Update item with new values given and use old values where required.
      $unit = $new_unit;
      $size = $new_size;
      $base_price = $new_base_price;
      $wholesale_price = $new_wholesale_price;
      $retail_price = $new_retail_price;
      $category = $new_category;
      $description = $new_description;
      $image = $new_image;
      $grower = $new_grower;
      // If new values weren't provided, use existing values for each product.
      // Exception is quantity, which is always accumulative.
      $query = 'SELECT unit, pack_size, base_price, wholesale_price, ' .
        'retail_price, category, description, image, grower, order_available, '.
        'purchase_available, supplier_available, taxable, quantity, ' .
        'composite, track, bulk, hidden, cart FROM stock WHERE ' .
        'name = "' . $name . '" AND user = "' . $user . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($stock = $mysqli_result->fetch_assoc()) {
          if ($unit === 'none') $unit = $mysqli->escape_string($stock['unit']);
          if ($size === '') {
            $size = (float)$stock['pack_size'];
          }
          if ($base_price < 0.01 && $base_price > -0.01) {
            $base_price = (float)$stock['base_price'];
          }
          if ($wholesale_price < 0.01 && $wholesale_price > -0.01) {
            $wholesale_price = (float)$stock['wholesale_price'];
            if ($wholesale_percent !== '') {
              $wholesale_price = $base_price * (100 + $wholesale_percent) / 100;
            }
          }
          if ($retail_price < 0.01 && $retail_price > -0.01) {
            $retail_price = (float)$stock['retail_price'];
            if ($retail_percent !== '') {
              $retail_price = $base_price * (100 + $retail_percent) / 100;
            }
          }
          if ($category === '') {
            $category = $mysqli->escape_string($stock['category']);
          }
          if ($description === '') {
            $description = $mysqli->escape_string($stock['description']);
          }
          if ($image === '') {
            $image = $mysqli->escape_string($stock['image']);
          }
          if ($grower === '') {
            $grower = $mysqli->escape_string($stock['grower']);
          }
          if (!$order_available_updated) {
            $order_available = (int)$stock['order_available'];
          }
          if (!$purchase_available_updated) {
            $purchase_available = (int)$stock['purchase_available'];
          }
          if (!$supplier_available_updated) {
            $supplier_available = (int)$stock['supplier_available'];
          }
          if (!$taxable_updated) $taxable = (int)$stock['taxable'];
          if (!$composite_updated) $composite = (int)$stock['composite'];
          if (!$bulk_updated) $bulk = (int)$stock['bulk'];
          if (!$hidden_updated) $hidden = (int)$stock['hidden'];
          if (!$cart_updated) $cart = (int)$stock['cart'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Stock->EditMultiple: ' . $mysqli->error);
      }
      $result = $this->Edit($us_name, $user, $unit, $size, $base_price,
                            $wholesale_price, $retail_price, $category,
                            $description, $image, $grower, $order_available,
                            $purchase_available, $supplier_available, $taxable,
                            $quantity, $composite, $track, $bulk, $hidden,
                            $cart);
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function Filter($product) {
    if (!isset($_POST['filters'])) return false;

    foreach ($_POST['filters'] as $field => $value) {
      if (trim($value) === '') continue;

      // Some fields don't match ids in the grid so need to swap them here.
      if ($field === 'product') $field = 'name';
      else if ($field === 'supplier') $field = 'user';
      if (isset($product[$field])) {
        // If value starts with an asterisk then match anywhere in the field.
        if (strpos($value, '*') === 0) {
          if (strlen($value) > 1 &&
              stripos($product[$field], substr($value, 1)) === false) {
            return true;
          }
        }
        // If value starts with a less than or greater than sign, convert the
        // rest of the filter to a float for comparison.
        else if (strpos($value, '<') === 0) {
          if (preg_match('/^<\s*([0-9.]+)/', $value, $match)) {
            if ((float)$product[$field] >= (float)$match[1]) return true;
          }
        }
        else if (strpos($value, '>') === 0) {
          if (preg_match('/^>\s*([0-9.]+)/', $value, $match)) {
            if ((float)$product[$field] <= (float)$match[1]) return true;
          }
        }
        else if (stripos($product[$field], $value) !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private function RemoveProduct() {
    $us_name = $_POST['name'];
    $us_supplier = $_POST['supplier'];
    if ($us_name === '') {
      return ['error' => 'No product name given'];
    }
    if ($us_supplier === '') {
      return ['error' => 'No supplier given'];
    }

    $default_group = $this->user->group;
    $this->SetPurchaseGroup();

    $mysqli = connect_db();
    $user = $mysqli->escape_string($us_supplier);
    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($user)) {
      $mysqli->close();
      $this->user->group = $default_group;
      return ['error' => 'Supplier not found'];
    }

    $name = $mysqli->escape_string($us_name);
    // Can only remove stock items if no purchases have been made.
    $purchase = new Purchase($this->user, $this->owner);
    if (count($purchase->Search('', 0, $name, $user)) !== 0) {
      $mysqli->close();
      $this->user->group = $default_group;
      return ['error' => 'Purchases exist for this product'];
    }

    $query = 'INSERT INTO stock_history (name, user, unit, pack_size, ' .
      'base_price, wholesale_price, retail_price, category, description, ' .
      'image, grower, order_available, purchase_available, ' .
      'supplier_available, taxable, quantity, composite, track, bulk, ' .
      'hidden, cart, action, modified_by, timestamp) SELECT name, user, ' .
      'unit, pack_size, base_price, wholesale_price, retail_price, ' .
      'category, description, image, grower, order_available, ' .
      'purchase_available, supplier_available, taxable, quantity, ' .
      'composite, track, bulk, hidden, cart, "remove", ' .
      '"' . $this->user->name . '", ' . time() . ' FROM stock WHERE ' .
      'user = "' . $user . '" AND name = "' . $name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->RemoveProduct 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock WHERE name = "' . $name . '" AND ' .
      'user = "' . $user . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->RemoveProduct 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM stock_alternative WHERE name = "' . $name . '" ' .
      'AND user = "' . $user . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->RemoveProduct 3: ' . $mysqli->error);
    }
    // Also remove the discount item if this is a composite item.
    if ($_POST['composite'] === '1') {
      $query = 'DELETE FROM stock WHERE name = "' . $name . ' discount" ' .
        'AND user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->RemoveProduct 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    $result = $_POST['multiple'] === 'true' ?
      ['done' => true] : $this->AllData();
    $this->user->group = $default_group;
    return $result;
  }

  private function RemoveProfile() {
    $mysqli = connect_db();
    $value = $mysqli->escape_string($_POST['value']);
    $query = 'DELETE FROM stock_column_profile WHERE value = "' . $value . '" '.
      'AND user = "' . $this->user->name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->RemoveProfile: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['options' => $this->CustomProfiles()];
  }

  private function Save() {
    $default_group = $this->user->group;
    $this->SetPurchaseGroup();
    $mysqli = connect_db();
    $product = $mysqli->escape_string($_POST['product']);
    $supplier = $mysqli->escape_string($_POST['supplier']);
    $field = $mysqli->escape_string($_POST['field']);
    $value = $mysqli->escape_string(trim($_POST['value']));
    $composite = $mysqli->escape_string($_POST['composite']);
    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($supplier)) {
      $mysqli->close();
      $this->user->group = $default_group;
      return ['error' => 'Supplier not found.'];
    }

    $error = '';
    $other = '';
    $new_name = '';
    $available_query = '';
    $purchase = new Purchase($this->user, $this->owner);
    // Check if the user is choosing to change the product name. All previous
    // purchases are also updated in this case.
    if ($field === 'name' && $product !== $value) {
      $us_name = trim($_POST['value']);
      if ($value === '') {
        $error = 'No product name given';
      }
      else if (strpos($us_name, '"') !== false) {
        $error = 'Double quotes not allowed in product name';
      }
      else if (strpos($us_name, '\\') !== false) {
        $error = 'Backslash not allowed in product name';
      }
      else {
        $new_name = $value;
        $purchase->UpdatePurchases($new_name, $product, $supplier);
        if ($composite === 'true') {
          $purchase->UpdatePurchases($new_name . ' discount',
                                     $product . ' discount', $supplier);
          $query = 'UPDATE stock SET name = "' . $new_name . ' discount" ' .
            'WHERE name = "' . $product . ' discount" AND ' .
            'user = "' . $supplier . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Stock->Save 1: ' . $mysqli->error);
          }
        }
      }
    }
    else if ($field === 'user') {
      // The supplier column isn't editable, so this is a sanity check.
      $error = 'Please use the product form to create new products.';
    }
    else if ($field === 'unit' && ($value === '' || $value === 'none')) {
      $error = 'No units given';
    }
    else if (($field === 'hidden' && $value === 'true') ||
             ($field === 'supplierAvailable' && $value === 'false')) {
      // When an item is hidden or made unavailable from a supplier also update
      // the other availability fields.
      $available_query = ', order_available = 0, purchase_available = 0';
    }
    else if ($field === 'composite' && $value === 'true') {
      $query = 'INSERT INTO stock VALUES ("' . $product . ' discount", ' .
        '"' . $supplier . '", "variable", 1, 0, 0, 0, "", "", "", "", 0, 0, ' .
        '0, 0, 0, 0, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE ' .
        'name = "' . $product . ' discount"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Save 2: ' . $mysqli->error);
      }
    }
    else if ($field === 'available' || $field === 'purchaseAvailable') {
      if ($value === 'true') {
        // When making an item available, must make sure there isn't already
        // an item with the same name available by another user.
        $query = 'SELECT stock.user FROM stock LEFT JOIN users ON ' .
          'stock.user = users.user WHERE name = "' . $product . '" ' .
          'AND (order_available = 1 OR purchase_available = 1) ' .
          'AND stock.user != "' . $supplier . '" AND ' .
          $organiser->GroupQuery();
        if ($mysqli_result = $mysqli->query($query)) {
          if ($stock = $mysqli_result->fetch_assoc()) {
            $other = $stock['user'];
            $query = 'UPDATE stock SET order_available = 0, ' .
              'purchase_available = 0 WHERE user = "' . $other . '" AND ' .
              'name = "' . $product . '"';
            if (!$mysqli->query($query)) {
              $this->Log('Stock->Save 3: ' . $mysqli->error);
            }
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Stock->Save 4: ' . $mysqli->error);
        }
        // Also make sure this item is set as available from the supplier.
        $available_query = ', supplier_available = 1';
      }
      if ($field === 'available') {
        // When updating availability and purchase availability isn't used,
        // keep it the same as order availability in case it's changed later.
        // (And so the available download profile works correctly.)
        if ($this->Substitute('stock-order-available') !== 'true') {
          $available_query .= ', purchase_available = ';
          $available_query .= $value === 'true' ? '1' : '0';
        }
      }
    }
    if ($error !== '') {
      $mysqli->close();
      $this->user->group = $default_group;
      return ['error' => $error];
    }

    if ($field === 'description') {
      include 'library/HTMLPurifier.auto.php';
      $config = HTMLPurifier_Config::createDefault();
      $config->set('Attr.EnableID', true);
      $config->set('Attr.IDPrefix', 'anchor-');
      $config->set('Attr.AllowedFrameTargets', ['_blank']);
      $purifier = new HTMLPurifier($config);
      $value = $purifier->purify($_POST['value']);
    }
    else if ($field === 'price') $field = 'base_price';
    else if ($field === 'wholesale') $field = 'wholesale_price';
    else if ($field === 'retail') $field = 'retail_price';
    else if ($field === 'size') $field = 'pack_size';
    else if ($field === 'available') $field = 'order_available';
    else if ($field === 'purchaseAvailable') $field = 'purchase_available';
    else if ($field === 'supplierAvailable') $field = 'supplier_available';
    $convert = ['order_available', 'purchase_available', 'supplier_available',
                'taxable', 'composite', 'track', 'bulk', 'cart', 'hidden'];
    if (in_array($field, $convert)) {
      $value = $value === 'true' ? 1 : 0;
    }
    $update_required = true;
    $order_fields = ['unit', 'pack_size', 'base_price', 'wholesale_price',
                     'retail_price'];
    // Update wholesale and retail prices when base_price is being changed and
    // a markup is set.
    $wholesale_price = '';
    $retail_price = '';
    if (isset($_SESSION['stock-price']) &&
        $_SESSION['stock-price'] === 'order' &&
        in_array($field, $order_fields)) {
      $query = 'UPDATE stock_order_price SET ' .
        'order_' . $field . ' = "' . $value . '" WHERE ' .
        'name = "' . $product . '" AND user = "' . $supplier . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Save 5: ' . $mysqli->error);
      }
      // If viewing order prices but the product isn't found in the
      // stock_order_price table then update the stock table instead.
      // Can also use this check to know where to update markup prices.
      $update_required = $mysqli->affected_rows === 0;
      if (!$update_required && $field === 'base_price') {
        list($wholesale_price, $retail_price) =
          $this->UpdateProductMarkup($value, $product, $supplier, true);
      }
    }
    if ($update_required) {
      $query = 'UPDATE stock SET ' . $field . ' = "' . $value . '"' .
        $available_query . ' WHERE name = "' . $product . '" AND ' .
        'user = "' . $supplier . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->Save 6: ' . $mysqli->error);
      }
      if ($field === 'base_price') {
        list($wholesale_price, $retail_price) =
          $this->UpdateProductMarkup($value, $product, $supplier, false);
      }
      else if ($field === 'image') {
        // Allow admin users to perform system-wide image updates when an image
        // is provided and no other image is set for an item.
        if ($this->user->canEditSite && $value !== '') {
          $query = 'UPDATE stock SET image = "' . $value . '" WHERE ' .
            'name = "' . $product . '" AND image = ""';
          if (!$mysqli->query($query)) {
            $this->Log('Stock->Save 7: ' . $mysqli->error);
          }
        }
      }
    }
    // Don't update orders if viewing current prices.
    $update_orders = (!isset($_SESSION['stock-price']) ||
      $_SESSION['stock-price'] === 'order') &&
      $this->Substitute('stock-update-orders') === 'true';
    if ($update_orders && ($field === 'base_price' ||
        $field === 'wholesale_price' || $field === 'retail_price')) {
      $query = 'SELECT base_price, wholesale_price, retail_price FROM stock ' .
        'WHERE user = "' . $supplier . '" AND name = "' . $product . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($stock = $mysqli_result->fetch_assoc()) {
          $purchase->UpdateOrders($product, $supplier,
                                  (float)$stock['base_price'],
                                  (float)$stock['wholesale_price'],
                                  (float)$stock['retail_price']);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Stock->Save 8: ' . $mysqli->error);
      }
    }
    $query = 'INSERT INTO stock_history (name, user, unit, pack_size, ' .
      'base_price, wholesale_price, retail_price, category, description, ' .
      'image, grower, order_available, purchase_available, ' .
      'supplier_available, taxable, quantity, composite, track, bulk, ' .
      'hidden, cart, action, modified_by, timestamp) SELECT name, user, ' .
      'unit, pack_size, base_price, wholesale_price, retail_price, ' .
      'category, description, image, grower, order_available, ' .
      'purchase_available, supplier_available, taxable, quantity, ' .
      'composite, track, bulk, hidden, cart, "edit", ' .
      '"' . $this->user->name . '", ' . time() . ' FROM stock WHERE ' .
      'user = "' . $supplier . '" AND name = "' . $product . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->Save 9: ' . $mysqli->error);
    }
    $mysqli->close();

    $save = ['done' => true];
    // The cart module needs to be updated when certain fields change, if the
    // cart option is set for the product.
    $cart_fields = ['name', 'user', 'unit', 'retail_price', 'category',
                    'description', 'image', 'grower', 'order_available',
                    'supplier_available', 'hidden', 'cart'];
    if (in_array($field, $cart_fields) &&
        $this->Substitute('stock-cart') === 'true') {
      $save = $this->UpdateCart($product, $supplier, $new_name);
    }
    $this->CheckSupplyGroup($supplier);
    $this->user->group = $default_group;
    if ($other !== '') $save['other'] = $other;
    if ($wholesale_price !== '') {
      $save['wholesale'] = price_string($wholesale_price);
    }
    if ($retail_price !== '') {
      $save['retail'] = price_string($retail_price);
    }
    return $save;
  }

  private function AddSupplyGroup($user, $group) {
    $mysqli = connect_db();
    if ($group === '') {
      // The empty group means the supplier is available to all groups, so
      // remove any specific groups as can't have both at once.
      $query = 'DELETE FROM stock_supply_group WHERE user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->AddSupplyGroup 1: ' . $mysqli->error);
      }
    }
    else {
      // When given a specific group, make sure the empty group isn't also set.
      $query = 'DELETE FROM stock_supply_group WHERE user = "' . $user . '" ' .
        'AND system_group = ""';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->AddSupplyGroup 2: ' . $mysqli->error);
      }
    }
    $query = 'INSERT INTO stock_supply_group VALUES ("' . $user . '", ' .
      '"' . $group . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->AddSupplyGroup 3: ' . $mysqli->error);
    }
    $mysqli->close();

    // This is called by the Account module, return the new group to display
    // in Account's register form.
    return '<div class="stock-group">' .
        '<button class="remove-stock-group">remove</button> ' .
        '<span class="stock-supply-group">' . ucfirst($group) . '</span>' .
      '</div>';
  }

  private function RemoveSupplyGroup($user, $group) {
    $mysqli = connect_db();
    // Can only remove specific groups, not the empty group.
    if ($group !== '') {
      $query = 'DELETE FROM stock_supply_group WHERE user = "' . $user . '" ' .
        'AND system_group = "' . $group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->RemoveSupplyGroup: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    // If the last group was removed, revert back to all groups.
    $this->CheckSupplyGroup($user);
  }

  private function CheckSupplyGroup($user) {
    // When a product is updated check that there is an entry for this
    // supplier in the stock_supply_group table and add a default if not.
    $mysqli = connect_db();
    $query = 'SELECT user FROM stock_supply_group WHERE user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 0) {
        $query = 'INSERT INTO stock_supply_group VALUES ("' . $user . '", "")';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->CheckSupplyGroup 1: ' . $mysqli->error);
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->CheckSupplyGroup 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ShowSupplyGroups() {
    // Look up the current groups for the user first, as this will change
    // what is shown to the user.
    $current_groups = '';
    $mysqli = connect_db();
    $query = 'SELECT system_group FROM stock_supply_group WHERE ' .
      'user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock_supply_group = $mysqli_result->fetch_assoc()) {
        $group = $stock_supply_group['system_group'];
        if ($group === '') {
          $current_groups .= '<div class="stock-all-groups">' .
            '<b>Currently supplying all groups.</b></div>';
        }
        else {
          $current_groups .= '<div class="stock-group">' .
              '<button class="remove-stock-group">remove</button> ' .
              '<span class="stock-supply-group">' .
                ucfirst($stock_supply_group['system_group']) . '</span>' .
            '</div>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->ShowSupplyGroups: ' . $mysqli->error);
    }
    $mysqli->close();

    $content = '<i>There are no stock groups to choose from.<i>';
    $options = '';
    $organiser = new Organiser($this->user, $this->owner);
    // Ignore a group in the organisation such as 'non-purchasing'.
    $ignore_group = $this->Substitute('invoice-ignore-group');
    $show_groups = $organiser->Siblings($ignore_group);
    for ($i = 0; $i < count($show_groups); $i++) {
      $group = $show_groups[$i];
      $options .= '<option value="' . $group . '">' . ucfirst($group) .
        '</option>';
    }
    if ($options !== '') {
      $content = '<div class="form-spacing">';
      if ($current_groups === '') {
        $content .= '<hr>If this is a supplier account, ' .
          'they can choose which groups they want to supply for:<br>';
      }
      else {
        $content .= '<hr>Update the groups this account will supply for:<br>';
      }
      $content .= '<i>(Default is to supply to all groups if no groups ' .
        'are selected.)</i><br>' .
        '<select id="stock-select-supply-group">' . $options . '</select>' .
        '<button class="stock-add-supply-group">add</button></div>' .
        '<div class="stock-current-groups">' . $current_groups . '</div>';
    }
    return $content;
  }

  private function ImportData() {
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $config->set('Attr.AllowedFrameTargets', ['_blank']);
    $purifier = new HTMLPurifier($config);

    $pre_order = false;
    $all_available = false;
    $default_group = $this->user->group;
    $this->SetPurchaseGroup();
    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $user = $mysqli->escape_string($_POST['supplier']);
    // When a supplier is provided, first make all items for this supplier
    // unavailable. Once the new supplier availability has been updated,
    // any newly unavailable products can be made unavailable to order.
    // (When a supplier is not provided the data contains both available and
    //  unavailable products).
    if ($user !== '') {
      if (!$organiser->MatchUser($user)) {
        $mysqli->close();
        $this->user->group = $default_group;
        return ['error' => 'Supplier not found'];
      }

      $query = 'UPDATE stock SET supplier_available = 0 WHERE ' .
        'user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->ImportData 1: ' . $mysqli->error);
      }
      // Check if all products in the list should be updated automatically.
      $all_available = $this->Substitute('stock-all-available') === 'true';
      // Also when pre-order is used, the value provided for order availability
      // can be ignored and instead will always be set to true.
      $pre_order = $this->Substitute('pre-order') === 'true';
    }

    $stock_order_available =
      $this->Substitute('stock-order-available') === 'true';
    $description_category =
      $this->Substitute('stock-description-category') === 'true';
    $new_available = [];
    $previous_name = '';
    $previous_supplier = '';
    $us_data = json_decode($_POST['data'], true);
    for ($i = 0; $i < count($us_data); $i++) {
      $us_name = trim($us_data[$i]['product']);
      $supplier =
        $mysqli->escape_string(strtolower(trim($us_data[$i]['supplier'])));
      $quantity = (float)$us_data[$i]['quantity'];
      $unit = $mysqli->escape_string($us_data[$i]['unit']);
      $size = (float)$us_data[$i]['size'];
      $base_price = (float)$us_data[$i]['price'];
      $wholesale_price = (float)$us_data[$i]['wholesale'];
      $retail_price = (float)$us_data[$i]['retail'];
      $category = $mysqli->escape_string($us_data[$i]['category']);
      $description =
        $mysqli->escape_string($purifier->purify($us_data[$i]['description']));
      $grower = $mysqli->escape_string($us_data[$i]['grower']);
      $order_available = $pre_order ? 1 : (int)$us_data[$i]['orderAvailable'];
      $purchase_available = (int)$us_data[$i]['purchaseAvailable'];
      $supplier_available = (int)$us_data[$i]['supplierAvailable'];
      $taxable = (int)$us_data[$i]['taxable'];

      // Make sure all the imported suppliers are part of this group.
      if ($supplier === '') continue;
      if ($previous_supplier !== $supplier) {
        if (!$organiser->MatchUser($supplier)) continue;
        $this->CheckSupplyGroup($supplier);
        $previous_supplier = $supplier;
        // Also reset previous_name since it's not the same supplier.
        $previous_name = '';
      }
      // Check for any typos in the name and correct rather than create a
      // new product.
      if (($name = $this->CheckProduct($us_name, $supplier, $unit)) === '') {
        continue;
      }
      // If a duplicate is found, add entry in stock_alternative then continue.
      if ($previous_name === $name) {
        $query = 'INSERT INTO stock_alternative VALUES ("' . $name . '", ' .
          '"' . $supplier . '"';
        // Find if the current entry or the previous one is the half size.
        if ($size < $previous_size) {
          $query .= ', ' . $size . ', ' . $base_price . ', ' .
            $previous_size . ', ' . $previous_price . ') ON DUPLICATE KEY ' .
            'UPDATE half_size = ' . $size . ', ' .
            'half_price = ' . $base_price . ', ' .
            'full_size = ' . $previous_size . ', ' .
            'full_price = ' . $previous_price;
        }
        else {
          $query .= ', ' . $previous_size . ', ' . $previous_price . ', ' .
            $size . ', ' . $base_price . ') ON DUPLICATE KEY UPDATE ' .
            'half_size = ' . $previous_size . ', ' .
            'half_price = ' . $previous_price . ', ' .
            'full_size = ' . $size . ', ' . 'full_price = ' . $base_price;
          // When the full box is listed second, want to store this as the
          // default price so update the item.
          $query2 = 'UPDATE stock SET pack_size = ' . $size . ', ' .
            'base_price = ' . $base_price . ' WHERE name = "' . $name . '" ' .
            'AND user = "' . $supplier . '"';
          if (!$mysqli->query($query2)) {
            $this->Log('Stock->ImportData 2: ' . $mysqli->error);
          }
        }
        if (!$mysqli->query($query)) {
          $this->Log('Stock->ImportData 3: ' . $mysqli->error);
        }
        continue;
      }

      $previous_name = $name;
      $previous_size = $size;
      $previous_price = $base_price;
      // Check if this product is newly available and not currently hidden.
      if ($supplier_available === 1) {
        $query = 'SELECT hidden, supplier_available FROM stock_history ' .
          'WHERE name = "' . $name . '" AND user = "' . $supplier . '" ' .
          'ORDER BY timestamp DESC LIMIT 1';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($stock = $mysqli_result->fetch_assoc()) {
            if ($stock['hidden'] === '0' &&
                $stock['supplier_available'] === '0') {
              if (!isset($new_available[$supplier])) {
                $new_available[$supplier] = [];
              }
              $new_available[$supplier][$name] = true;
            }
          }
          else {
            // If there's no result the product is also newly available.
            $new_available[$supplier][$name] = true;
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Stock->ImportData 4: ' . $mysqli->error);
        }
      }
      // Make the same item from different suppliers unavailable to order.
      if ($order_available === 1) {
        $query = 'UPDATE stock LEFT JOIN users ON stock.user = users.user ' .
          'SET order_available = 0 WHERE name = "' . $name . '" AND ' .
          'stock.user != "' . $supplier . '" AND ' . $organiser->GroupQuery();
        if (!$mysqli->query($query)) {
          $this->Log('Stock->ImportData 5: ' . $mysqli->error);
        }
      }
      // Tracking isn't imported, so use quantity to set. If a quantity is
      // provided, set this product as being tracked. When not provided,
      // don't update the existing value.
      $track = 0;
      $track_query = '';
      if ($quantity > 0) {
        $track = 1;
        $track_query = ', track = 1';
      }
      $duplicate_query = 'order_available = ' . $order_available . ', ' .
        'purchase_available = ' . $purchase_available . ', ' .
        'description = "' . $description . '", ' .
        'category = "' . $category . '", ';
      // If user is provided, don't update purchase_available as it's not
      // imported. Also, the pre-order flag is used to make everything
      // available, but also need to check if this group forces availability
      // updates for every product from a supplier.
      if ($user !== '') {
        if ($all_available) {
          $duplicate_query = 'order_available = ' . $order_available . ', ';
        }
        else {
          $duplicate_query = '';
        }
        // The description field can be used to import the category, this is a
        // legacy from the old format where category wasn't imported by default.
        // Now that it is, the current value is not overwritten in single
        // supplier format in case it's been modified manually.
        if ($description_category) {
          $duplicate_query .= 'category = "' . $description . '", ';
        }
      }
      if ($stock_order_available) {
        $query = 'INSERT INTO stock_order_price VALUES ("' . $name . '", ' .
          '"' . $supplier . '", "' . $unit . '", ' . $size . ', ' .
          $base_price . ', ' . $wholesale_price . ', ' . $retail_price .
          ') ON DUPLICATE KEY UPDATE order_unit = "' . $unit . '", ' .
          'order_pack_size = ' . $size . ', order_base_price = ' .
          $base_price . ', order_wholesale_price = ' . $wholesale_price . ', ' .
          'order_retail_price = ' . $retail_price;
        if (!$mysqli->query($query)) {
          $this->Log('Stock->ImportData 6: ' . $mysqli->error);
        }
      }
      else {
        // Update all details when not stored in stock_order_price table.
        $duplicate_query .= 'unit = "' . $unit . '", pack_size = ' . $size .
          ', base_price = ' . $base_price . ', wholesale_price = ' .
          $wholesale_price . ', retail_price = ' . $retail_price . ', ';
      }
      // For single supplier formats quantity is considered new stock.
      // Otherwise quantity is considered to be the total, in which case check
      // if it has changed and record the adjustment.
      if ($user === '') {
        $adjustment = 0;
        $query = 'SELECT quantity FROM stock WHERE name = "' . $name . '" ' .
          'AND user = "' . $supplier . '"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($stock = $mysqli_result->fetch_assoc()) {
            $adjustment = $quantity - (float)$stock['quantity'];
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Stock->ImportData 7: ' . $mysqli->error);
        }
        if ($adjustment > 0.01 || $adjustment < -0.01) {
          $query = 'INSERT INTO stock_adjustment VALUES ("' . $name . '", ' .
            '"' . $supplier . '", ' . $adjustment .
            ', "Adjusted in bulk import.", ' .
            '"' . $this->user->name . '", ' . time() . ')';
          if (!$mysqli->query($query)) {
            $this->Log('Stock->ImportData 8: ' . $mysqli->error);
          }
        }
        $duplicate_query .= 'quantity = ' . $quantity . ', ';
      }
      else {
        $duplicate_query .= 'quantity = quantity + ' . $quantity . ', ';
      }
      // Try adding an image for this product, which requires the admin user to
      // keep a matching list of products and images available in their group.
      $image = '';
      if ($this->user->name !== 'admin') {
        $query = 'SELECT image FROM stock LEFT JOIN users ON ' .
          'stock.user = users.user WHERE system_group = "admin" AND ' .
          'name = "' . $name . '" AND image != ""';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($stock = $mysqli_result->fetch_assoc()) {
            $image = $mysqli->escape_string($stock['image']);
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Stock->ImportData 9: ' . $mysqli->error);
        }
      }
      $query = 'INSERT INTO stock VALUES ("' . $name . '", "' . $supplier.'", '.
        '"' . $unit . '", ' . $size . ', ' . $base_price . ', ' .
        $wholesale_price . ', ' . $retail_price . ', "' . $category . '", ' .
        '"' . $description . '", "' . $image . '", "' . $grower . '", ' .
        $order_available . ', ' . $purchase_available . ', ' .
        $supplier_available . ', ' . $taxable . ', ' . $quantity . ', 0, ' .
        $track . ', 0, 0, 0) ' .
        'ON DUPLICATE KEY UPDATE ' . $duplicate_query .
        'grower = "' . $grower . '", supplier_available = ' .
        $supplier_available . ', taxable = ' . $taxable . $track_query;
      if (!$mysqli->query($query)) {
        $this->Log('Stock->ImportData 10: ' . $mysqli->error);
      }
      $query = 'INSERT INTO stock_history VALUES ("' . $name . '", ' .
        '"' . $supplier . '", "' . $unit . '", ' . $size . ', ' .
        $base_price . ', ' . $wholesale_price . ', ' . $retail_price . ', ' .
        '"' . $category . '", "' . $description . '", "", "' . $grower . '", ' .
        $order_available . ', ' . $purchase_available . ', ' .
        $supplier_available . ', ' . $taxable . ', ' . $quantity . ', 0, ' .
        $track . ', 0, 0, 0, "edit", "' . $this->user->name . '", ' .time().')';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->ImportData 11: ' . $mysqli->error);
      }
    }

    // Import done - turn off order_available for items not found in data or
    // that are currently hidden.
    if ($user !== '') {
      $query = 'UPDATE stock SET order_available = 0 WHERE ' .
        '(supplier_available = 0 OR hidden = 1) AND user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->ImportData 12: ' . $mysqli->error);
      }
      // Make sure that items currently available to purchase aren't made
      // unavailable from the supplier.
      $query = 'UPDATE stock SET supplier_available = 1 WHERE ' .
        'purchase_available = 1 AND user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->ImportData 13: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    $result = $this->AllData($new_available);
    $this->user->group = $default_group;
    return $result;
  }

  private function CheckProduct($us_name, $supplier, $unit) {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($us_name);

    // Do some simple checks first, note that errors are logged, but are
    // otherwise ignored so as not to interfere with the import process.
    if (strpos($us_name, '"') !== false) {
      $this->Log('Stock->CheckProduct 1: Double quotes not allowed in ' .
                 'product, name = ' . $name . ', supplier = ' . $supplier);
      $name = '';
    }
    else if (strpos($us_name, '\\') !== false) {
      $this->Log('Stock->CheckProduct 2: Backslash not allowed in product, ' .
                 'name = ' . $name . ', supplier = ' . $supplier);
      $name = '';
    }
    else if ($unit === '' || $unit == 'none') {
      $this->Log('Stock->CheckProduct 3: No units given, ' .
                 'name = ' . $name . ', supplier = ' . $supplier);
      $name = '';
    }
    else if ($unit === 'adjusted' &&
             !preg_match('/(\d+)\s*(each|g|kg|kilo|l)/i', $name)) {
      $this->Log('Stock->CheckProduct 4: Product name must include quantity ' .
                 'and units to adjust by, name = ' . $name . ', supplier = ' .
                 $supplier);
      $name = '';
    }

    // Check if the product already exists.
    $exists = false;
    $query = 'SELECT name FROM stock where name = "' . $name . '" AND ' .
      'user = "' . $supplier . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        // Use the existing value of name to avoid case changes.
        if ($stock = $mysqli_result->fetch_assoc()) {
          $name = $mysqli->escape_string($stock['name']);
        }
        $exists = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->CheckProduct 4: ' . $mysqli->error);
    }
    // If it doesn't exist it may just be a typo, check for similar names by
    // removing the last letter and looking for matches.
    if (!$exists && strlen($name) > 2) {
      $start = substr($name, 0, -1);
      $query = 'SELECT name FROM stock where name LIKE "' . $start . '" OR ' .
        'name LIKE "' . $start . '_" OR name LIKE "' . $start . '__" ' .
        'AND user = "' . $supplier . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows === 1) {
          if ($stock = $mysqli_result->fetch_assoc()) {
            $name = $mysqli->escape_string($stock['name']);
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Stock->CheckProduct 5: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $name;
  }

  private function ListAdjustments() {
    $default_group = $this->user->group;
    $this->SetPurchaseGroup();
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $user = $mysqli->escape_string($_POST['supplier']);
    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($user)) {
      $this->user->group = $default_group;
      return ['error' => 'Supplier not found'];
    }

    $result = [];
    $query = 'SELECT adjustment, description, timestamp FROM ' .
      'stock_adjustment WHERE name = "' . $name . '" AND ' .
      'user = "' . $user . '" ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock_adjustment = $mysqli_result->fetch_assoc()) {
        $result[] = ['name' => $name,
                     'supplier' => $user,
                     'adjustment' => (float)$stock_adjustment['adjustment'],
                     'description' => $stock_adjustment['description'],
                     'date' => (int)$stock_adjustment['timestamp'] * 1000];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->ListAdjustments: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function ListAllAdjustments() {
    $default_group = $this->user->group;
    $this->SetPurchaseGroup();

    $result = [];
    $mysqli = connect_db();
    $organiser = new Organiser($this->user, $this->owner);
    $query = 'SELECT name, stock_adjustment.user, adjustment, description, ' .
      'timestamp FROM stock_adjustment LEFT JOIN users ON ' .
      'stock_adjustment.user = users.user WHERE ' . $organiser->GroupQuery() .
      ' ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock_adjustment = $mysqli_result->fetch_assoc()) {
        $result[] = ['name' => $stock_adjustment['name'],
                     'supplier' => $stock_adjustment['user'],
                     'adjustment' => (float)$stock_adjustment['adjustment'],
                     'description' => $stock_adjustment['description'],
                     'date' => (int)$stock_adjustment['timestamp'] * 1000];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->ListAllAdjustments: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function SaveAdjustment() {
    $default_group = $this->user->group;
    $this->SetPurchaseGroup();
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $user = $mysqli->escape_string($_POST['supplier']);
    $move_name = $mysqli->escape_string($_POST['moveName']);
    $move_user = $mysqli->escape_string($_POST['moveSupplier']);
    $total = (float)$_POST['total'];
    $description = $mysqli->escape_string($_POST['description']);
    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($user)) {
      $mysqli->close();
      $this->user->group = $default_group;
      return ['error' => 'Supplier not found'];
    }

    $result = ['remove' => false];
    $adjustment = 0;
    // Look up the original stock quantity to calculate the difference.
    $query = 'SELECT quantity FROM stock WHERE name = "' . $name . '" ' .
      'AND user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock = $mysqli_result->fetch_assoc()) {
        $adjustment = $total - (float)$stock['quantity'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->SaveAdjustment 1: ' . $mysqli->error);
    }
    if ($move_name === '' && $move_user === '') {
      $query = 'UPDATE stock SET quantity = ' . $total . ' WHERE ' .
        'name = "' . $name . '" AND user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->SaveAdjustment 2: ' . $mysqli->error);
      }
      $query = 'INSERT INTO stock_adjustment VALUES ("' . $name . '", ' .
        '"' . $user . '", ' . $adjustment . ', "' . $description . '", ' .
        '"' . $this->user->name . '", ' . time() . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->SaveAdjustment 3: ' . $mysqli->error);
      }
      // TODO: If total is zero need to update cart if stock-limited is used.
    }
    else {
      if ($organiser->MatchUser($move_user)) {
        // When quantity is being moved to a different product, deduct total
        // from the old product (it might not all be moved) and add it to the
        // new one.
        $query = 'UPDATE stock SET quantity = quantity - ' . $total . ' WHERE '.
          'name = "' . $name . '" AND user = "' . $user . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->SaveAdjustment 4: ' . $mysqli->error);
        }
        $query = 'UPDATE stock SET quantity = quantity + ' . $total . ' WHERE '.
          'name = "' . $move_name . '" AND user = "' . $move_user . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Stock->SaveAdjustment 5: ' . $mysqli->error);
        }
        // An adjustment is not recorded when quantity is moved, as it may have
        // just been a typo in the imported product name. If the total quantity
        // for a product is being moved and no purchases exist, the user is
        // asked if they would like to remove the old product.
        if (($total > 0.001 || $total < -0.001) &&
            $adjustment < 0.001 && $adjustment > -0.001) {
          $purchase = new Purchase($this->user, $this->owner);
          $search = $purchase->Search('', 0, $name, $user);
          $result['remove'] = count($search) === 0;
        }
        // TODO: Check if cart needs to be updated for both products too.
      }
      else {
        $result['error'] = 'Supplier not found';
      }
    }
    $mysqli->close();
    $this->user->group = $default_group;
    return $result;
  }

  private function ExportAdjustments() {
    $default_group = $this->user->group;
    $this->SetPurchaseGroup();

    $result = [];
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $user = $mysqli->escape_string($_POST['supplier']);
    $all_adjustments = $mysqli->escape_string($_POST['allAdjustments']);
    $organiser = new Organiser($this->user, $this->owner);

    $query = '';
    if ($all_adjustments === 'true') {
      $query = 'SELECT name, stock_adjustment.user, adjustment, description, ' .
        'modified_by, timestamp FROM stock_adjustment LEFT JOIN users ON ' .
        'stock_adjustment.user = users.user WHERE ' . $organiser->GroupQuery() .
        ' ORDER BY timestamp DESC';
    }
    else {
      // When exporting adjustments for only one product match the supplier.
      if (!$organiser->MatchUser($user)) {
        $this->user->group = $default_group;
        return ['error' => 'Supplier not found'];
      }
      $query = 'SELECT name, user, adjustment, description, modified_by, ' .
        'timestamp FROM stock_adjustment WHERE name = "' . $name . '" AND ' .
        'user = "' . $user . '" ORDER BY timestamp DESC';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($stock_adjustment = $mysqli_result->fetch_assoc()) {
        $result[] = ['name' => $stock_adjustment['name'],
                     'supplier' => $stock_adjustment['user'],
                     'adjustment' => (float)$stock_adjustment['adjustment'],
                     'description' => $stock_adjustment['description'],
                     'modified by' => $stock_adjustment['modified_by'],
                     'date' => (int)$stock_adjustment['timestamp'] * 1000];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->ExportAdjustments: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->user->group = $default_group;
    $date = date('Y-m-d');
    $filename = 'stock-adjustments-' . $date . '.csv';
    if ($this->CreateCSV($filename, $result)) return ['filename' => $filename];
    return ['error' => 'No data to export.'];
  }

  private function UpdateGroup() {
    // UpdateGroup is called after a new supplier account has been created,
    // so update the import select menu on the page.
    $options = $this->SupplierSelect();
    if (!isset($_SESSION['purchase-group'])) return ['options' => $options];

    $purchase_group = $_SESSION['purchase-group'];
    if ($this->user->group === $purchase_group) return ['options' => $options];

    $invite = new Invite($this->user, $this->owner);
    if (in_array($purchase_group, $invite->Created())) {
      $mysqli = connect_db();
      $user = $mysqli->escape_string($_POST['supplier']);
      $query = 'UPDATE users SET system_group = "' . $purchase_group . '" ' .
        'WHERE user = "' . $user . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Stock->UpdateGroup: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    return ['options' => $options];
  }

  private function ChangeGroup() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $mysqli->close();

    $invite = new Invite($this->user, $this->owner);
    if ($group === $this->user->group || in_array($group, $invite->Created())) {
      $_SESSION['purchase-group'] = $group;
      $_SESSION['purchase-group-changed'] = true;
      return ['done' => true];
    }
    return ['error' => 'Group not found.'];
  }

  private function SetPurchaseGroup() {
    if (!isset($_SESSION['purchase-group'])) return;

    $purchase_group = $_SESSION['purchase-group'];
    if ($this->user->group === $purchase_group) return;

    $invite = new Invite($this->user, $this->owner);
    if (in_array($purchase_group, $invite->Created())) {
      $this->user->group = $purchase_group;
    }
  }

  private function SupplierSelect() {
    $detail = new Detail($this->user, $this->owner);
    $options = '<option value="">Multiple Suppliers</option>';
    foreach ($detail->SupplierOnly() as $supplier => $name) {
      $options .= '<option value="' . $supplier . '">' . $name . '</option>';
    }
    return $options . '<option value="other">other...</option>';
  }

  private function TrackQuantity() {
    if ($this->Substitute('stock-track-quantity') !== 'true') {
      return ['', '', ''];
    }

    $quantity =
      '<div class="form-spacing">' .
        '<label for="stock-total-quantity-input">Total Quantity:</label>' .
        '<input id="stock-total-quantity-input" readonly="readonly" ' .
          'type="text"> ' .
        '<button class="adjust">adjust</button>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="stock-new-quantity-input">New Quantity:</label>' .
        '<input id="stock-new-quantity-input" type="text" maxlength="12">' .
        '<label for="stock-track-input">Track:</label>' .
        '<input id="stock-track-input" type="checkbox">' .
      '</div>';
    $quantity_columns =
      '<div class="form-spacing">' .
        '<input type="checkbox" id="stock-column-quantity">' .
        '<label for="stock-column-quantity">Quantity</label>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<input type="checkbox" id="stock-column-track">' .
        '<label for="stock-column-track">Track</label>' .
      '</div>';
    $quantity_dialog =
      '<div class="stock-quantity-dialog hidden">' .
        '<a href="#" class="stock-move-link">Click here to move stock</a>' .
        '<div class="stock-move hidden"><b>Move to:</b>' .
          '<div class="form-spacing">' .
            '<label for="stock-move-name-input">Product:</label>' .
            '<input id="stock-move-name-input" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="stock-move-user-input">Supplier:</label>' .
            '<input id="stock-move-user-input" type="text" maxlength="50">' .
          '</div>' .
        '</div>' .
        '<div class="stock-adjust">' .
          '<div class="form-spacing">' .
            '<label for="stock-quantity-adjust-input">Total Quantity:' .
            '</label>' .
            '<input id="stock-quantity-adjust-input" type="text" ' .
              'maxlength="12">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="stock-quantity-reason-input">Reason for adjustment:'.
            '</label><br>' .
            '<textarea id="stock-quantity-reason-input"></textarea>' .
          '</div>' .
        '</div>' .
        '<button class="submit">submit</button>' .
        '<hr>' .
        '<button class="show-all">show all</button>' .
        '<button class="export">export</button>' .
        '<div class="stock-adjustment-grid"></div>' .
      '</div>';
    return [$quantity, $quantity_columns, $quantity_dialog];
  }

  private function UpdateCart($product, $supplier, $new_name = '') {
    $unit = '';
    $image = '';
    $grower = '';
    $description = '';
    $retail_price = 0;
    $order_available = 0;
    $cart_setting = 0;
    $category = '';
    $old_name = '';
    if ($new_name !== '') {
      $old_name = $product;
      $product = $new_name;
    }
    $mysqli = connect_db();
    $query = 'SELECT unit, category, description, image, grower, ' .
      'retail_price, order_available, cart FROM stock WHERE ' .
      'name = "' . $product . '" AND user = "' . $supplier . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($stock = $mysqli_result->fetch_assoc()) {
        $unit = $mysqli->escape_string($stock['unit']);
        $category = $mysqli->escape_string($stock['category']);
        $image = $mysqli->escape_string($stock['image']);
        $grower = $mysqli->escape_string($stock['grower']);
        $description = $mysqli->escape_string($stock['description']);
        $retail_price = (float)$stock['retail_price'];
        $order_available = (int)$stock['order_available'];
        $cart_setting = (int)$stock['cart'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Stock->UpdateCart: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($unit === 'each') {
      $cart = new Module($this->user, $this->owner, 'cart');
      return $cart->Factory('UpdateItem', [$product, $supplier, $image, $grower,
                                           $description, $retail_price,
                                           $order_available, $category,
                                           $cart_setting, $old_name]);
    }
    else if ($cart_setting === 1) {
      // Only report an error if trying to sync an item with non-each units.
      return ['error' => 'Units must be \'each\' when updating cart item.'];
    }
    return ['done' => true];
  }

  private function UpdateProductMarkup($base_price, $product,
                                       $supplier, $order) {
    // If markup percentages aren't set then wholesale and retail prices will
    // be kept the same as the base price here. When the percent is set to
    // zero wholesale and retail should not be updated automatically.
    $wholesale_percent = $this->Substitute('stock-wholesale-percent');
    $retail_percent = $this->Substitute('stock-retail-percent');
    if ($wholesale_percent === '0' && $retail_percent === '0') {
      return ['', ''];
    }

    // Allow updating only one price automatically by building a query based
    // on the percent values that are set.
    $wholesale_price = $wholesale_percent === '0' ? '' :
      (float)$base_price * (100 + (float)$wholesale_percent) / 100;
    $retail_price = $retail_percent === '0' ? '' :
      (float)$base_price * (100 + (float)$retail_percent) / 100;

    $mysqli = connect_db();
    $query = '';
    if ($order) {
      $query = 'UPDATE stock_order_price SET ';
      if ($wholesale_price !== '') {
        $query .= 'order_wholesale_price = ' . $wholesale_price;
      }
      if ($retail_price !== '') {
        if ($wholesale_price !== '') $query .= ', ';
        $query .= 'order_retail_price = ' . $retail_price;
      }
    }
    else {
      $query = 'UPDATE stock SET ';
      if ($wholesale_price !== '') {
        $query .= 'wholesale_price = ' . $wholesale_price;
      }
      if ($retail_price !== '') {
        if ($wholesale_price !== '') $query .= ', ';
        $query .= 'retail_price = ' . $retail_price;
      }
    }
    $query .= ' WHERE name = "' . $product . '" AND user = "' . $supplier . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Stock->UpdateProductMarkup: ' . $mysqli->error);
    }
    $mysqli->close();
    return [$wholesale_price, $retail_price];
  }

}
