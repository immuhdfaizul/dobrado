<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Cart extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($action === 'item') return $this->DisplayItem();
    if ($action === 'checkout') return $this->Checkout();
    if ($action === 'updatePosition') {
      if ($this->user->canEditPage) return $this->UpdatePosition();
      return ['error' => 'You don\'t have permission to move cart items.'];
    }
    if ($action === 'list') {
      if ($this->user->canEditPage) return $this->ShowSettings();
      return ['error' => 'You don\'t have permission to list cart settings.'];
    }
    if ($action === 'saveItem') {
      if ($this->user->canEditPage) return $this->SaveItem();
      return ['error' => 'You don\'t have permission to edit cart items.'];
    }
    if ($action === 'saveShipping') {
      if ($this->user->canEditPage) return $this->SaveShipping();
      return ['error' => 'You don\'t have permission to edit cart shipping.'];
    }
    if ($action === 'saveCheckout') {
      if ($this->user->canEditPage) return $this->SaveCheckout();
      return ['error' => 'You don\'t have permission to edit cart checkout.'];
    }
    if ($action === 'removeItem') {
      if ($this->user->canEditPage) return $this->RemoveItem();
      return ['error' => 'You don\'t have permission to remove cart items.'];
    }
    if ($action === 'removeShipping') {
      if ($this->user->canEditPage) return $this->RemoveShipping();
      return ['error' => 'You don\'t have permission to remove cart shipping.'];
    }
    if ($action === 'resetItem') {
      if ($this->user->canEditPage) return $this->ResetItem();
      return ['error' => 'You don\'t have permission to reset cart items.'];
    }
    if ($action === 'paymentDone') {
      // This allows the page to refresh without the query string added by
      // the payment gateway.
      return ['location' => $this->Url()];
    }
    if ($action === 'updateName') {
      if ($this->user->canEditPage) return $this->SaveItem(true);
      return ['error' => 'You don\'t have permission to update cart names.'];
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    // Can only have one cart module on a page.
    return !$this->AlreadyOnPage('cart', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $items = '';
    $content = $this->CompleteOrder() .
      '<div id="cart-total"></div>' .
      '<button id="cart-checkout">checkout</button>' .
      '<div class="form-spacing cart-search-wrapper">' .
        '<label for="cart-search">Search:</label>' .
        '<input id="cart-search" type="text">' .
        '<button id="cart-clear-search">Clear Search</button>' .
      '</div>' .
      '<form id="cart-form">';
    $dispatch_date = '<div class="form-spacing cart-dispatch-date">' .
        '<label for="cart-dispatch">Pack on:</label>' .
        '<input id="cart-dispatch" type="text" readonly>' .
      '</div>';
    $tabs = '';
    $show_tabs = false;
    $tab_names =
      explode(',', $this->Substitute('cart-tabs-' . $this->user->page));
    if (count($tab_names) > 1) {
      $tabs .= '<div id="cart-tabs"><ul id="cart-tabs-list">';
      foreach ($tab_names as $current_tab) {
        // The tab names template is stored encoded for display in html, but
        // the tabs used below from cart_item_page is not encoded so decode
        // here so that tab links match the generated ids.
        $decode_current_tab = strtolower(htmlspecialchars_decode($current_tab));
        $tab_id = preg_replace('/[^a-z0-9_-]/', '-', $decode_current_tab);
        $tabs .= '<li><a href="#cart-tab-' . $tab_id . '">' . $current_tab .
          '</a></li>';
      }
      $tabs .= '</ul>';
      $show_tabs = true;
    }

    $tracking = [];
    $user_detail = [];
    // Get the default text for logged out users.
    $welcome_message = $this->Substitute('cart-welcome-message');
    // And store details to fill in checkout form for logged in users.
    $first = '';
    $last = '';
    $email = '';
    $phone = '';
    $street = '';
    $postcode = '';
    $city = '';
    $state = '';
    $payment_methods = '';
    $dispatch_info = '';
    if ($this->user->loggedIn) {
      $banking = new Module($this->user, $this->owner, 'banking');
      $buyer_group = $banking->Factory('BuyerGroup');
      $tracking = $this->Tracking($buyer_group);
      $detail = new Module($this->user, $this->owner, 'detail');
      $user_detail = $detail->Factory('User');
      $first = $user_detail['first'];
      $last = $user_detail['last'];
      $email = $user_detail['email'];
      $phone = $user_detail['phone'];
      $address = explode(',', $user_detail['address']);
      if (count($address) === 4) {
        list($street, $postcode, $city, $state) = $address;
      }

      $name = $first === '' ? $this->user->name : $first;
      $welcome_message = '<h4>Welcome ' . $name . '</h4>';
      if ($buyer_group === 'wholesale') {
        $welcome_message .= '<p>You are currently viewing our wholesale ' .
          'prices.</p>';
      }
      else if ($buyer_group === 'price') {
        $welcome_message .= '<p>You are currently viewing our cost prices.</p>';
      }
      $dispatch_info = $this->Substitute('cart-dispatch-info-logged-in');
      if ($this->Substitute('cart-skip-payment-options') === 'true') {
        // Display delivery options instead of payment options when a user is
        // logged in. Note that both options must be available in cart_methods.
        $payment_methods = '<label for="customer-detail-method">Delivery:' .
          '</label><select id="customer-detail-method">' .
            '<option value="account">Use address provided</option>' .
            '<option value="pickup">I will pick up</option>' .
          '</select>' .
          '<div id="customer-detail-account">(You will receive an invoice ' .
            'for your order since you are currently logged in.)</div>';
      }
    }
    else {
      $dispatch_info = $this->Substitute('cart-dispatch-info-logged-out');
      // Dispatch datepicker is optional for logged out users.
      if ($this->Substitute('cart-dispatch-date-logged-out') === 'false') {
        $dispatch_date = '';
      }
    }
    if ($payment_methods === '') {
      $payment_methods = $this->PaymentMethods();
    }
    $prev_tab = '';
    $current_tab = '';
    // Need to create a list of tab display areas created below, to make sure
    // that all the tabs created above have matching display areas to link to.
    $display_tabs = [];

    $mysqli = connect_db();
    $available_query = $this->user->canEditPage ? '' : ' AND available = 1';
    $query = 'SELECT cart_items.name, image, short, full, price, variable, ' .
      'minimum, download, tab, available FROM cart_items LEFT JOIN ' .
      'cart_item_page ON cart_items.user = cart_item_page.user AND ' .
      'cart_items.name = cart_item_page.name WHERE ' .
      'cart_items.user = "' . $this->owner . '"' . $available_query .
      ' AND page = "' . $this->user->page . '" ORDER BY tab, item_order';
    if ($mysqli_result = $mysqli->query($query)) {
      $price_text = $this->Substitute('cart-price-text');
      $quantity_text = $this->Substitute('cart-quantity-text');
      while ($cart_items = $mysqli_result->fetch_assoc()) {
        if ($show_tabs) {
          $current_tab = $cart_items['tab'];
          $encode_current_tab = htmlspecialchars($current_tab);
          if (!in_array($encode_current_tab, $tab_names)) continue;

          if ($current_tab !== $prev_tab) {
            // Close the previous tab before creating the next one.
            if ($items !== '') $items .= '</div>';
            $tab_id = preg_replace('/[^a-z0-9_-]/', '-',
                                   strtolower($current_tab));
            $items .= '<div id="cart-tab-' . $tab_id . '">';
            $display_tabs[] = $encode_current_tab;
            $prev_tab = $current_tab;
          }
        }
        if (isset($tracking[$cart_items['name']])) {
          $cart_items['price'] = $tracking[$cart_items['name']]['price'];
        }
        $items .= $this->FormatItem($cart_items, $price_text, $quantity_text);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->Content 1: ' . $mysqli->error);
    }
    $mysqli->close();

    // First close the last tab, then add display areas for any tabs where
    // matching items weren't found, then close #cart-tabs if tabs are shown.
    if ($items !== '') $items .= '</div>';
    foreach ($tab_names as $current_tab) {
      if (!in_array($current_tab, $display_tabs)) {
        $decode_current_tab = strtolower(htmlspecialchars_decode($current_tab));
        $tab_id = preg_replace('/[^a-z0-9_-]/', '-', $decode_current_tab);
        $items .= '<div id="cart-tab-' . $tab_id . '"></div>';
      }
    }
    if ($show_tabs) $items .= '</div>';

    $description = '';
    $input = '';
    $label = $this->Substitute('cart-description-label');
    $value = $this->Substitute('cart-description-value');
    $type = $this->Substitute('cart-description-type');
    // If value is set make it readonly for input and textarea types.
    $readonly = $value === '' ? '' : ' readonly';
    if ($type === 'input') {
      $input = '<input id="customer-detail-description" type="text"' .
        $readonly . ' value="' . $value . '">';
    }
    else if ($type === 'textarea') {
      $input = '<textarea id="customer-detail-description"' . $readonly . '>' .
        $value . '</textarea>';
    }
    else if ($type === 'select') {
      $input = '<select id="customer-detail-description">';
      foreach (explode(',', $value) as $option) {
        $input .= '<option>' . $option . '</option>';
      }
      $input .= '</select>';
    }
    if ($label !== '' && $input !== '') {
      $description = '<div class="form-spacing hidden">' .
        '<label for="customer-detail-description">' . $label . '</label>' .
        $input . '</div>';
    }
    $content .= $tabs . $items . '</form>' .
      '<div id="cart-item-dialog"></div>' .
      '<div id="cart-checkout-dialog" class="hidden">' .
        '<div id="cart-summary"></div>' .
        '<div id="cart-dispatch-info" class="hidden">' .
          $dispatch_info .
        '</div>' .
        $dispatch_date .
        '<button id="cart-confirm-button">confirm</button>' .
        '<button id="cart-continue-button">continue shopping</button>' .
        '<div id="cart-checkout-message"></div>' .
        '<form id="cart-customer-details-form">' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-name">Name:</label>' .
            '<input id="customer-detail-name" type="text" maxlength="100" ' .
              'value="' . $first . ' ' . $last . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-email">Email:</label>' .
            '<input id="customer-detail-email" type="text" maxlength="100" ' .
              'value="' . $email . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-phone">Phone:</label>' .
            '<input id="customer-detail-phone" type="text" maxlength="100" ' .
              'value="' . $phone . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-address">Address:</label>' .
            '<input id="customer-detail-address" type="text" maxlength="100" ' .
              'value="' . trim($street) . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-postcode">Postcode:</label>' .
            '<input id="customer-detail-postcode" type="text" maxlength="10" '.
              'value="' . trim($postcode) . '">' .
            '<label for="customer-detail-city">City:</label>' .
            '<input id="customer-detail-city" type="text" maxlength="100" ' .
              'value="' . trim($city) . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-state">State:</label>' .
            '<input id="customer-detail-state" type="text" maxlength="100" ' .
              'value="' . trim($state) . '">' .
            '<label for="customer-detail-country">Country:</label>' .
            '<input id="customer-detail-country" type="text" maxlength="50" ' .
              'value="' . $this->Substitute('cart-default-country') . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-notes">Notes:</label>' .
            '<textarea id="customer-detail-notes"></textarea>' .
          '</div>' .
          '<div class="form-spacing">' . $payment_methods . '</div>' .
          $description .
          '<button id="customer-detail-submit">Submit Details</button>' .
          '<div id="cart-checkout-info"></div>' .
        '</form>' .
      '</div>';
    $edit = '';
    if ($this->user->canEditPage) {
      $edit = '<button id="cart-edit-button"></button>' .
        '<div id="cart-editor"></div>';
    }
    $unavailable = '';
    $start_unavailable = $this->Substitute('cart-start-unavailable');
    $end_unavailable = $this->Substitute('cart-end-unavailable');
    if ($start_unavailable !== '' && $end_unavailable !== '') {
      $start_timestamp = strtotime($start_unavailable);
      $end_timestamp = strtotime($end_unavailable);
      if ($start_timestamp && $end_timestamp) {
        $start = '<data id="cart-start-unavailable" ' .
          'value="' . $start_timestamp . '">' . $start_unavailable . '</data>';
        $end = '<data id="cart-end-unavailable" ' .
          'value="' . $end_timestamp . '">' . $end_unavailable . '</data>';
        $unavailable = '<div class="cart-unavailable ui-state-highlight ' .
          'ui-corner-all">';
        $unavailable .= $this->Substitute('cart-unavailable',
                                          ['/!start/', '/!end/'],
                                          [$start, $end]);
        $unavailable .= '</div>';
      }
      else {
        $unavailable = '<div class="cart-unavailable ui-state-highlight ' .
          'ui-corner-all">The start and end dates for shutdown are not ' .
          'valid. Please check the format used for ' .
          '<b>cart-start-unavailable</b> and <b>cart-end-unavailable</b> ' .
          'templates.</div>';
      }
    }
    $cart_title = $this->Substitute('cart-title');
    return $unavailable . '<div id="cart-welcome-message">' . $welcome_message .
      '</div><div id="cart-title">' . $cart_title . $edit . '</div>' . $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'Payment' && $count === 12) {
        $us_items = $p[0];
        $amount = $p[1];
        $us_name = $p[2];
        $us_email = $p[3];
        $us_phone = $p[4];
        $us_address = $p[5];
        $us_city = $p[6];
        $us_postcode = $p[7];
        $us_state = $p[8];
        $us_country = $p[9];
        $us_notes = $p[10];
        $us_method = $p[11];
        return $this->Payment($us_items, $amount, $us_name, $us_email,
                              $us_phone, $us_address, $us_city, $us_postcode,
                              $us_state, $us_country, $us_notes, $us_method);
      }
      if ($fn === 'PaymentMethods' && $count === 2) {
        $display_cash = $p[0];
        $id = $p[1];
        return $this->PaymentMethods($display_cash, $id);
      }
      if ($fn === 'UpdateItem' && $count >= 9) {
        $product = $p[0];
        $supplier = $p[1];
        $image = $p[2];
        $short = $p[3];
        $full = $p[4];
        $price = $p[5];
        $available = $p[6];
        $category = $p[7];
        $tracking = $p[8];
        $old_name = $count === 10 ? $p[9] : '';
        return $this->UpdateItem($product, $supplier, $image, $short, $full,
                                 $price, $available, $category, $tracking,
                                 $old_name);
      }
      return;
    }

    if ($fn === 'CheckPayment') return $this->CheckPayment();
    if ($fn === 'Gateway') return $this->Gateway($p);
    if ($fn === 'ProcessingCost') return $this->ProcessingCost($p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.cart.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.cart.js');
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS cart_items (' .
      'user VARCHAR(50) NOT NULL,' .
      'name VARCHAR(100) NOT NULL,' .
      'image VARCHAR(200),' .
      'short TEXT,' .
      'full TEXT,' .
      'weight DECIMAL(8,2),' .
      'price DECIMAL(8,2),' .
      'variable TINYINT(1),' .
      'minimum DECIMAL(8,2),' .
      'download VARCHAR(200),' .
      'PRIMARY KEY(user, name)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS cart_item_page (' .
      'user VARCHAR(50) NOT NULL,' .
      'name VARCHAR(100) NOT NULL,' .
      'page VARCHAR(200),' .
      'available TINYINT(1),' .
      'tab VARCHAR(100),' .
      'item_order INT UNSIGNED,' .
      'PRIMARY KEY(user, name, page(50))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 2: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS cart_tracking (' .
      'user VARCHAR(50) NOT NULL,' .
      'name VARCHAR(100) NOT NULL,' .
      'supplier VARCHAR(50) NOT NULL,' .
      'product VARCHAR(100) NOT NULL,' .
      'modified TINYINT(1),' .
      'PRIMARY KEY(user, name)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 3: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS cart_shipping (' .
      'user VARCHAR(50) NOT NULL,' .
      'id INT UNSIGNED NOT NULL,' .
      'type VARCHAR(100) NOT NULL,' .
      'destination_name VARCHAR(100),' .
      'destination_code VARCHAR(4),' .
      'amount DECIMAL(8,2) NOT NULL,' .
      'minimum DECIMAL(8,2),' .
      'maximum DECIMAL(8,2),' .
      'rule ENUM("weight", "fixed", "percent") NOT NULL,' .
      'PRIMARY KEY(user, id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 4: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS cart_checkout (' .
      'user VARCHAR(50) NOT NULL,' .
      'email VARCHAR(200) NOT NULL,' .
      'currency VARCHAR(3) NOT NULL,' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 5: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS cart_method (' .
      'user VARCHAR(50) NOT NULL,' .
      'name ENUM("credit", "paypal", "pickup", "account") NOT NULL,' .
      'available TINYINT(1),' .
      'gateway ENUM("eway", "migs", ""),' .
      'gateway_api_key TEXT,' .
      'gateway_password VARCHAR(100),' .
      'fee DECIMAL(8,2),' .
      'PRIMARY KEY(user, name)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 6: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS cart_order (' .
      'user VARCHAR(50) NOT NULL,' .
      'id INT UNSIGNED NOT NULL AUTO_INCREMENT,' .
      'email VARCHAR(200) NOT NULL,' .
      'description TEXT,' .
      'method ENUM("credit", "paypal", "pickup", "account") NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'completed INT(10) UNSIGNED,' .
      'PRIMARY KEY(user, id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Install 7: ' . $mysqli->error);
    }
    $mysqli->close();

    $template = ['"cart-price-text","","Price:"',
                 '"cart-quantity-text","","Quantity:"',
                 '"cart-title","","Shopping Cart"',
                 '"cart-pickup","","<p>Thanks for your order! It will be ' .
                   'available for you to pick up on !date. For your ' .
                   'reference, the order number is <b>!order-id</b>.</p>"',
                 '"cart-account","","<p>Thanks for your order! It will be ' .
                   'packed on: !date and delivered to:</p><p>!name<br>' .
                   '!address<br>!postcode<br>!city<br>!state<br>!country</p>' .
                   '<p>For your reference, the order number is ' .
                   '<b>!order-id</b>. Delivery costs may be added to your ' .
                   'invoice before completing the order.</p>"',
                 '"cart-checkout","","<p>Your order will be packed on: !date ' .
                   'and delivered to:</p><p>!name<br>!address<br>' .
                   '!postcode<br>!city<br>!state<br>!country</p>"',
                 '"cart-display-item","","<h2>!name</h2><img src=\"!image\">' .
                   '<p>!price</p><p>!quantity<p>!short</p><p>!full</p>"',
                 '"cart-price-comparison","",' .
                   '"<b>$<span class=\"item-price\">!price</span></b> ' .
                   '(Retail: $<span class=\"item-retail\">!retail</span>)"',
                 '"cart-email","","<p>To !name<br>Thanks for your order! ' .
                   'The following is a confirmation of your purchases and ' .
                   'contact details:</p><p>!items</p><p>Email: !email<br>' .
                   'Phone: !phone</p><p><b>Address:</b><br>!address<br>!city' .
                   '<br>!postcode<br>!state<br>!country</p><p>!notes</p>"',
                 '"cart-dispatch-info-logged-in","","<hr>You can choose the ' .
                   'day your order will be packed for dispatch or pickup, ' .
                   'otherwise it will be packed by the second business day ' .
                   'after your order is placed."',
                 '"cart-dispatch-info-logged-out","","<hr>Pick up details ' .
                   'will be shown once you click confirm, otherwise your ' .
                   'order will be packed for dispatch by the second business ' .
                   'day after your order is placed."'];
    $this->AddTemplate($template);
    $description = ['cart-price-text' => 'The text before the price for ' .
                      'each item.',
                    'cart-quantity-text' => 'The text before the quantity ' .
                      'for each item.',
                    'cart-title' => 'The text for the title above the cart ' .
                      'items.',
                    'cart-pickup' => 'The text to display at checkout, ' .
                      'will substitute !date for the date that the order is '.
                      'packed, and !order-id since payment not yet required.',
                    'cart-account' => 'The text to display at checkout, ' .
                      'will substitute the following to show where items ' .
                      'will be sent: !name, !address, !postcode, !city, ' .
                      '!state, !country, !description. It will also ' .
                      'substitute !date for the date the order is packed and ' .
                      '!order-id since payment not yet required.',
                    'cart-checkout' => 'The text to display at checkout, ' .
                      'will substitute the following to show where items ' .
                      'will be sent: !name, !address, !postcode, !city, ' .
                      '!state, !country, !description. It will also '.
                      'substitute !date for the date the order is packed.',
                    'cart-email' => 'The full text of the email that will be ' .
                      'sent to the customer, confirming their order. It will ' .
                      'also be sent to the admin email address set in the ' .
                      'cart checkout settings. Substitutes: !items, !name, ' .
                      '!email, !phone, !address, !postcode, !city, !state, ' .
                      '!country, !notes.',
                    'cart-description-label' => 'A label for the input ' .
                      'field set in the \'cart-description-type\' template.',
                    'cart-description-type' => 'A configurable input field ' .
                      'added to \'cart-customer-details-form\' to show or ' .
                      'request extra details. It can be set to \'input\', ' .
                      '\'textarea\', or \'select\'. ',
                    'cart-description-value' => 'The value for the input ' .
                      'field set in \'cart-description-type\'.',
                    'cart-start-unavailable' => 'The date packing will be ' .
                      'unavailable from during a shutdown period. This will ' .
                      'be used when calculating available dates for packing ' .
                      'in the checkout date picker.',
                    'cart-end-unavailable' => 'The date packing will be ' .
                      'available again after a shutdown period. This will ' .
                      'be used when calculating available dates for packing ' .
                      'in the checkout date picker.',
                    'cart-unavailable' => 'A message to display when dates ' .
                      'set for a shutdown period. Substitutes !start and ' .
                      '!end dates.',
                    'cart-pack-default' => 'A relative date suitable for ' .
                      'passing to strtotime, which can be used if the pack ' .
                      'date requested by the user is not recognised.',
                    'cart-pack-default-pickup' => 'A relative date suitable ' .
                      'for passing to strtotime, which can be used as a ' .
                      'pickup date when the datepicker isn\'t shown.',
                    'cart-dispatch-info-logged-in' => 'A message to display ' .
                      'about dispatch times when the user is logged in.',
                    'cart-dispatch-info-logged-out' => 'A message to display ' .
                      'about dispatch times when the user is logged out.'];
    $this->AddTemplateDescription($description);

    $media = '@media screen and (max-device-width: 480px)';
    $site_style = ['"","#cart-summary","margin-bottom","20px"',
                   '"","#cart-clear-search","margin-left","5px"',
                   '"","#cart-tabs-list","display","flex"',
                   '"","#cart-tabs-list","flex-wrap","wrap"',
                   '"","#cart-tabs-list > li","flex-grow","1"',
                   '"","#cart-tabs-list > li","display","flex"',
                   '"","#cart-tabs-list a","flex-grow","1"',
                   '"","#cart-confirm-button","float","right"',
                   '"","#cart-customer-details-form","background-color",' .
                     '"#eeeeee"',
                   '"","#cart-customer-details-form","border",' .
                     '"1px solid #aaaaaa"',
                   '"","#cart-customer-details-form","border-radius","2px"',
                   '"","#cart-customer-details-form","padding","5px"',
                   '"","#cart-customer-details-form","margin","5px"',
                   '"","#cart-customer-details-form label","width","7em"',
                   '"","#customer-detail-postcode","width","50px"',
                   '"' . $media . '","#customer-detail-postcode","width",' .
                     '"300px"',
                   '"","label[for=customer-detail-city]","float","none"',
                   '"","label[for=customer-detail-city]","margin-left","41px"',
                   '"' . $media . '","label[for=customer-detail-city]",' .
                     '"margin-left","0"',
                   '"","#customer-detail-city","width","150px"',
                   '"' . $media . '","#customer-detail-city","width","300px"',
                   '"","#customer-detail-state","width","50px"',
                   '"' . $media . '","#customer-detail-state","width","300px"',
                   '"","label[for=customer-detail-country]","float","none"',
                   '"","label[for=customer-detail-country]","margin-left",' .
                     '"10px"',
                   '"' . $media . '","label[for=customer-detail-country]",' .
                     '"margin-left","0"',
                   '"","#customer-detail-country","width","150px"',
                   '"' . $media . '","#customer-detail-country","width",' .
                     '"300px"',
                   '"","#customer-detail-notes","height","40px"',
                   '"","#customer-detail-account","margin-left","3em"',
                   '"","#customer-detail-account","font-style","italic"',
                   '"","#customer-detail-submit","margin-left","7.3em"',
                   '"","#customer-detail-submit","margin-top","1em"',
                   '"","#cart-total-table","margin","10px"',
                   '"","#cart-total-table td.item","min-width","200px"',
                   '"","#cart-total-table td.quantity","text-align","center"',
                   '"","#cart-total-table td.price","text-align","right"',
                   '"","#cart-total-table tr:last-child","font-weight","bold"',
                   '"","#cart-dispatch-info","font-size","0.9em"',
                   '"","#cart-item-filters","margin","0 5px 5px 5px"',
                   '"","#cart-item-filters","padding","0 0 5px 5px"',
                   '"","#cart-item-filters","border-bottom",' .
                     '"1px solid #aaaaaa"',
                   '"","#cart-item-button-wrapper","display","grid"',
                   '"","#cart-item-button-wrapper","grid-template-columns",' .
                     '"auto 1fr auto 1fr auto"',
                   '"","#cart-item-button-wrapper","margin-left","5px"',
                   '"","#cart-item-button-wrapper","margin-right","5px"',
                   '"","#cart-item-new","justify-self","end"',
                   '"","#cart-item-save","justify-self","start"',
                   '"","#cart-item-form","background-color","#eeeeee"',
                   '"","#cart-item-form","border","1px solid #aaaaaa"',
                   '"","#cart-item-form","border-radius","2px"',
                   '"","#cart-item-form","padding","5px"',
                   '"","#cart-item-form","margin","5px"',
                   '"","#cart-item-form label","width","10em"',
                   '"","#cart-shipping-form","background-color","#eeeeee"',
                   '"","#cart-shipping-form","border","1px solid #aaaaaa"',
                   '"","#cart-shipping-form","border-radius","2px"',
                   '"","#cart-shipping-form","padding","5px"',
                   '"","#cart-shipping-form","margin","5px"',
                   '"","#cart-shipping-form label","width","10em"',
                   '"","#cart-checkout-form","background-color","#eeeeee"',
                   '"","#cart-checkout-form","border","1px solid #aaaaaa"',
                   '"","#cart-checkout-form","border-radius","2px"',
                   '"","#cart-checkout-form","padding","5px"',
                   '"","#cart-checkout-form","margin","5px"',
                   '"","#cart-checkout-form label","width","10em"',
                   '"","#cart-migs-format","font-size","0.8em"',
                   '"","#cart-migs-format","font-style","italic"',
                   '"","#cart-migs-format","margin-left","13em"',
                   '"","#eway-form label","width","7em"',
                   '"","#eway-form button","margin-left","7.3em"',
                   '"",".cart-item.border","border","1px dashed #828282"',
                   '"",".cart-item .item-quantity","width","25px"',
                   '"",".cart-item .item-quantity-wrapper input","padding",' .
                     '"0.222em 0"',
                   '"",".cart-item label","margin-right","5px"',
                   '"",".cart-modified-info","margin-left","10em"',
                   '"",".cart-tracking","margin-left","10em"',
                   '"",".cart-tracking","font-style","italic"',
                   '"",".cart-unavailable","padding","5px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    // Return if Remove was called for a specific module,
    // only want to remove cart settings when deleting an account.
    if (isset($id)) return;

    $mysqli = connect_db();
    $query = 'DELETE FROM cart_items WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Remove 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM cart_item_page WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Remove 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM cart_shipping WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Remove 3: ' . $mysqli->error);
    }
    $query = 'DELETE FROM cart_checkout WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Remove 4: ' . $mysqli->error);
    }
    $query = 'DELETE FROM cart_method WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Remove 5: ' . $mysqli->error);
    }
    $query = 'DELETE FROM cart_tracking WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Remove 6: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.cart.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AllItems() {
    $result = [];
    $current = [];
    $mysqli = connect_db();
    // Get the availability and item_order for the current page. This is done
    // in two steps because the queries don't join well. Need everything from
    // the cart_items table and only existing values from cart_item_page table.
    $query = 'SELECT name, available, tab, item_order FROM cart_item_page ' .
      'WHERE user = "' . $this->owner . '" AND ' .
      'page = "' . $this->user->page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_item_page = $mysqli_result->fetch_assoc()) {
        $current[$cart_item_page['name']] =
          ['available' => $cart_item_page['available'],
           'tab' => $cart_item_page['tab'],
           'item_order' => $cart_item_page['item_order']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->AllItems 1: ' . $mysqli->error);
    }
    $tracking = $this->Tracking();
    $query = 'SELECT name, image, short, full, weight, price, variable, ' .
      'minimum, download FROM cart_items WHERE user = "' . $this->owner . '" ' .
      'ORDER BY name';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_items = $mysqli_result->fetch_assoc()) {
        $name = $cart_items['name'];
        if (isset($current[$name])) {
          $cart_items['available'] = $current[$name]['available'];
          $cart_items['tab'] = $current[$name]['tab'];
          $cart_items['item_order'] = $current[$name]['item_order'];
        }
        else {
          $cart_items['available'] = '0';
          $cart_items['tab'] = '';
          $cart_items['item_order'] = '0';
        }
        if (isset($tracking[$name])) {
          $cart_items['tracking'] = 'Tracking stock item: ' .
            $tracking[$name]['name'] . ', from: ' .
            $tracking[$name]['supplier'] . '.';
          $cart_items['modified'] = $tracking[$name]['modified'];
        }
        $cart_items['name'] = $cart_items['name'];
        $cart_items['short'] = $cart_items['short'];
        $result[] = $cart_items;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->AllItems 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function CalculateWeight($name) {
    if (preg_match('/([0-9]+\.?[0-9]*)\s*(g|kg|ml|l)/i', $name, $match)) {
      $number = $match[1];
      $unit = strtolower($match[2]);
      // Special case for some bulk packs.
      if ($number === '1' && $unit = 'kg' && strpos($name, '6 x 1') !== false) {
        return 6;
      }
      if ($unit === 'ml' || $unit === 'g') $number /= 1000;
      return $number;
    }
    $this->Notification('cart', 'Cart', 'Couldn\'t calculate weight for: ' .
                          $name, 'system');
    return 0;
  }

  private function Checkout() {
    $us_name = htmlspecialchars($_POST['name']);
    $us_email = htmlspecialchars($_POST['email']);
    $us_phone = htmlspecialchars($_POST['phone']);
    $us_address = htmlspecialchars($_POST['address']);
    $us_city = htmlspecialchars($_POST['city']);
    $us_postcode = htmlspecialchars($_POST['postcode']);
    $us_state = htmlspecialchars($_POST['state']);
    $us_country = htmlspecialchars($_POST['country']);
    $us_notes = nl2br(htmlspecialchars($_POST['notes']), false);
    $us_method = htmlspecialchars($_POST['method']);
    $timestamp = (int)strtotime($_POST['date']);
    // If there's a problem with the date format check if there's a default.
    // Also allow for a default pickup day.
    if ($timestamp <= 0) {
      $pack_default = $us_method === 'pickup' ?
        $this->Substitute('cart-pack-default-pickup') :
        $this->Substitute('cart-pack-default');
      $timestamp = $pack_default === '' ?
        time() : (int)strtotime($pack_default);
      // The datepicker isn't always shown, so need to check that the default
      // date calculated is available for packing.
      $day = date('l', $timestamp);
      // First don't allow the pack date to be set on the weekend.
      if ($day === 'Saturday') $timestamp += 86400 * 2;
      else if ($day === 'Sunday') $timestamp += 86400;
      // And also need to check if there's a shutdown period set.
      $start_unavailable = $this->Substitute('cart-start-unavailable');
      $end_unavailable = $this->Substitute('cart-end-unavailable');
      if ($start_unavailable !== '' && $end_unavailable !== '') {
        $start_timestamp = strtotime($start_unavailable);
        $end_timestamp = strtotime($end_unavailable);
        if ($timestamp >= $start_timestamp && $timestamp < $end_timestamp) {
          $timestamp = $end_timestamp;
        }
      }
    }
    $pack_date = date('l F j Y', $timestamp);
    $us_description = isset($_POST['description']) ?
      htmlspecialchars($_POST['description']) : '';
    $us_cart = json_decode($_POST['cart'], true);
    // Initialise the cart session array to store downloadable items, also
    // items may need to be saved in the Purchase module once order is complete.
    $_SESSION['cart'] = [];
    $_SESSION['cart-purchase'] = [];
    $_SESSION['cart-owner'] = $this->owner;
    $_SESSION['cart-timestamp'] = $timestamp;
    // Paypal does not allow a detailed item description so generate a more
    // concise description field for it.
    $us_paypal_data = '';
    $us_item_names =
      '<table><tr><th>Item Name</th><th>Quantity</th><th>Price</th></tr>';
    $total_price = 0;
    $total_weight = 0;
    $order_verified = true;
    $purchase = [];
    $buyer_group = 'retail';
    if ($this->user->loggedIn) {
      $banking = new Module($this->user, $this->owner, 'banking');
      $buyer_group = $banking->Factory('BuyerGroup');
    }
    $tracking = $this->Tracking($buyer_group);
    foreach ($us_cart as $us_item => $us_values) {
      if ($us_values['selected'] === false) continue;
      $quantity = (float)$us_values['quantity'];
      if ($quantity < 0.01) continue;

      $price = (float)$us_values['price'];
      $stock_price = 0;
      if (isset($tracking[$us_item])) {
        $stock_price = (float)$tracking[$us_item]['price'];
        // Tracking returns the format required for the Purchase module to add
        // the sale, just need to add quantity.
        $tracking[$us_item]['quantity'] = $quantity;
        $_SESSION['cart-purchase'][] = $tracking[$us_item];
      }
      list($verified, $weight) =
        $this->VerifyItem($us_item, $price, $stock_price);
      if (!$verified) continue;

      if ($us_method === 'paypal') {
        if ($us_paypal_data !== '') $us_paypal_data .= ', ';
        $us_paypal_data .= $us_item;
        if ($quantity > 1) {
          $us_paypal_data .= ' (' . $quantity . ')';
        }
      }
      $us_item_names .= '<tr><td>' . $us_item .
        '</td><td style="text-align:center">' .
        $quantity . '</td><td style="text-align:right">$' .
        price_string($price * $quantity) . '</td></tr>';
      $total_price += round($price * $quantity, 2);
      $total_weight += $weight * $quantity;
    }
    if ($us_name === '' || $us_email === '' || $us_address === '') {
      $this->Log('Cart->Checkout: Order not verified due to missing ' .
                 'personal details.');
      $order_verified = false;
    }
    else if ($total_price === 0) {
      $order_verified = false;
    }
    if (!$order_verified) {
      return ['shipping' => 0, 'processing' => 0,
              'content' => 'There was a problem processing your order.<br>' .
                           'Please check your details and try again.'];
    }

    $result = [];
    $shipping = 0;
    // Shipping is not calculated for payment on account here as that should
    // be added when a final invoice is generated.
    if ($us_method !== 'pickup' && $us_method !== 'account') {
      // Look for shipping rules that match the given locations.
      $rules = $this->ShippingRules($us_city, $us_state,
                                    $us_country, $total_weight);
      $shipping = $this->ShippingCost($total_price, $rules);
    }
    $result['shipping'] = $shipping;
    $processing = $this->ProcessingCost($total_price);
    $result['processing'] = $processing;
    if ($shipping > 0.01) {
      $us_item_names .= '<tr><td>Shipping</td><td></td>' .
        '<td style="text-align:right">$' . price_string($shipping) .
        '</td></tr>';
    }
    if ($processing > 0.01) {
      $us_item_names .= '<tr><td>Processing</td><td></td>' .
        '<td style="text-align:right">$' . price_string($processing) .
        '</td></tr>';
    }
    $total_price += $shipping + $processing;
    $us_item_names .= '<tr><td></td><td><b>Total:</b></td>' .
      '<td style="text-align:right"><b>' . price_string($total_price) .
      '</b></td></table>';
    if ($us_method === 'pickup' || $us_method === 'account') {
      $order_id = $this->Payment($us_item_names, $total_price, $us_name,
                                 $us_email, $us_phone, $us_address, $us_city,
                                 $us_postcode, $us_state, $us_country,
                                 $us_notes, $us_method, $us_description,
                                 $pack_date);
      if (is_int($order_id)) {
        if (count($_SESSION['cart-purchase']) > 0) {
          $purchase = new Module($this->user, $this->owner, 'purchase');
          $purchase->Factory('CartSales',
                             ['account', $_SESSION['cart-purchase'],
                              $timestamp]);
        }
        if ($us_method === 'pickup') {
          $result['content'] =
            $this->Substitute('cart-pickup', ['/!order-id/', '/!date/'],
                              [$order_id, $pack_date]);
        }
        else {
          $patterns = ['/!name/', '/!address/', '/!postcode/', '/!city/',
                       '/!state/', '/!country/', '/!description/',
                       '/!order-id/', '/!date/'];
          $replacements = [$us_name, $us_address, $us_postcode, $us_city,
                           $us_state, $us_country, $us_description, $order_id,
                           $pack_date];
          $result['content'] = $this->Substitute('cart-account', $patterns,
                                                 $replacements);
        }
        $result['content'] .= '<button id="cart-payment">done</button>';
      }
      else {
        // Payment method can return an error string instead of order id here.
        $result['content'] = $order_id;
      }
    }
    else {
      $patterns = ['/!name/', '/!address/', '/!postcode/', '/!city/',
                   '/!state/', '/!country/', '/!description/', '/!date/'];
      $replacements = [$us_name, $us_address, $us_postcode, $us_city,
                       $us_state, $us_country, $us_description, $pack_date];
      $result['content'] = $this->Substitute('cart-checkout', $patterns,
                                             $replacements);
      if ($us_method === 'paypal') {
        $us_paypal_data = 'Items: ' . $us_paypal_data;
      }
      $result['content'] .=
        $this->Payment($us_item_names, $total_price, $us_name, $us_email,
                       $us_phone, $us_address, $us_city, $us_postcode,
                       $us_state, $us_country, $us_notes, $us_method,
                       $us_description, $pack_date, $us_paypal_data);
    }
    return $result;
  }

  private function CheckPayment() {
    // PayPal returns a 'tx' parameter as part of their PDT protocol.
    if (!isset($_GET['tx']) || !isset($_SESSION['cart-order-id'])) return false;

    // Also need an identity token from PayPal for PDT.
    $token = '';
    $mysqli = connect_db();
    $query = 'SELECT value FROM settings WHERE user = "' . $this->owner . '" ' .
      'AND label = "cart" AND name = "paypal"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($settings = $mysqli_result->fetch_assoc()) {
        $token = $settings['value'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->CheckPayment 1: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($token === '') return false;

    $endpoint = 'https://www.paypal.com/cgi-bin/webscr';
    $post_fields = 'cmd=_notify-synch&tx=' . $_GET['tx'] . '&at=' . $token;
    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $this->Log('Cart->CheckPayment 2: curl ' . $endpoint);
    $body = curl_exec($ch);

    $success = false;
    if (curl_errno($ch) === 0) {
      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if ($http_code === 200) {
        $success = strpos($body, 'SUCCESS') === 0;
      }
      else {
        $this->Log('Cart->CheckPayment 3: Error response from ' . $endpoint .
                   "\nHTTP code: " . $http_code . "\nBody: " . $body);
      }
    }
    else {
      $this->Log('Cart->CheckPayment 4: Error connecting to ' . $endpoint .
                 "\nCurl error: " . curl_error($ch));
    }
    curl_close($ch);

    if ($success) {
      $mysqli = connect_db();
      $query = 'UPDATE cart_order SET completed = ' . time() . ' WHERE ' .
        'user = "' . $this->owner . '" AND id = ' . $_SESSION['cart-order-id'];
      if (!$mysqli->query($query)) {
        $this->Log('Cart->CheckPayment 5: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    return $success;
  }

  private function CompleteOrder() {
    if (!isset($_GET['payment']) || !isset($_SESSION['cart-order-id'])) {
      return '';
    }

    $content = '';
    $email = '';
    $message = '';
    $order_id = $_SESSION['cart-order-id'];
    $mysqli = connect_db();
    $query = 'SELECT email, description FROM cart_order WHERE ' .
      'user = "' . $this->owner . '" AND id = ' . $order_id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_order = $mysqli_result->fetch_assoc()) {
        $email = $cart_order['email'];
        $message = $cart_order['description'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->CompleteOrder 1:' . $mysqli->error);
    }
    $mysqli->close();

    if ($_GET['payment'] === 'paypal') {
      $paypal = '';
      if ($this->CheckPayment()) {
        if (count($_SESSION['cart-purchase']) > 0) {
          $purchase = new Module($this->user, $this->owner, 'purchase');
          $purchase->Factory('CartSales', ['PayPal', $_SESSION['cart-purchase'],
                                           $_SESSION['cart-timestamp']]);
        }
        $this->SendEmail('Confirmation for order number ' . $order_id,
                         $message . 'Payment made using PayPal.', $email);
        $paypal = 'Thanks your payment has been processed.';
      }
      else {
        $this->SendEmail('Error processing order number ' . $order_id,
                         $message);
        $paypal = 'There was a problem processing your payment.<br>' .
          'Please contact support.';
      }
      $content = '<div class="cart-payment-message">' . $paypal .
        '<p>For your reference, the order number is: ' . $order_id . '</p>' .
        '<button>ok</button></div>';
    }
    else if ($_GET['payment'] === 'credit') {
      $gateway = $this->Gateway('credit');
      if ($gateway === true) {
        if (count($_SESSION['cart-purchase']) > 0) {
          $purchase = new Module($this->user, $this->owner, 'purchase');
          $purchase->Factory('CartSales',
                             ['Credit Card', $_SESSION['cart-purchase'],
                              $_SESSION['cart-timestamp']]);
        }
        $this->SendEmail('Confirmation for order number ' . $order_id,
                         $message . 'Payment made using Credit Card.', $email);
        // Gateway returns an error message when unsuccesful, so set the message
        // here when it returns true.
        $gateway = 'Thanks your payment has been processed.';
      }
      else {
        $this->SendEmail('Error processing order number ' . $order_id,
                         $message);
      }
      $content = '<div class="cart-payment-message">' . $gateway .
        '<p>For your reference, the order number is: ' . $order_id . '</p>' .
        '<button>ok</button></div>';
    }

    $_SESSION['cart-order-id'] = NULL;
    $_SESSION['cart-purchase'] = [];
    $_SESSION['cart-timestamp'] = 0;
    // Check if there are any items that need downloading.
    $count = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
    if ($count !== 0) {
      $_SESSION['cart-owner'] = $this->owner;
      $download = $count > 1 ? 'downloads' : 'download';
      $content .= '<div class="cart-download-message">' .
        'Your ' . $download . ' will begin shortly.</div>' .
        '<ul class="cart-download-list hidden">';
      for ($i = 0; $i < $count; $i++) {
        $content .= '<li>' . $_SESSION['cart'][$i] . '</li>';
      }
      $content .= '</ul>';
    }
    return $content;
  }

  private function DisplayItem() {
    $result = [];
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $price = price_string((float)$_POST['price']);
    $query = 'SELECT name, image, short, full, price, download FROM ' .
      'cart_items WHERE user = "' . $this->owner . '" AND name = "' . $name.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_items = $mysqli_result->fetch_assoc()) {
        // Note that names cannot be html escaped when stored because they are
        // shared with the stock module.
        $name = '<span class="item-name">' .
          htmlspecialchars($cart_items['name']) . '</span>';
        $image = $cart_items['image'];
        $short = $cart_items['short'];
        $full = $cart_items['full'];
        $retail = $cart_items['price'];
        $price_comparison = $price === $retail ?
          '$<span class="item-price">' . $retail . '</span>' :
          $this->Substitute('cart-price-comparison', ['/!price/', '/!retail/'],
                            [$price, $retail]);
        $quantity = '';
        // TODO: Now that quantity can be changed from the dialog, add a button
        // for downloadable items and price input for variable prices.
        if ($cart_items['download'] === '') {
          $quantity = '<div class="form-spacing">' .
            '<label for="cart-dialog-quantity">Quantity:</label>' .
            '<input id="cart-dialog-quantity" type="text" value="' .
              (int)$_POST['quantity'] . '"></div>';
        }
        $result['content'] =
          $this->Substitute('cart-display-item', ['/!name/', '/!image/',
                            '/!short/', '/!full/', '/!price/', '/!quantity/'],
                            [$name, $image, $short, $full, $price_comparison,
                             $quantity]);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->DisplayItem: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function FormatItem($item, $price_text, $quantity_text) {
    // Note that names cannot be html escaped when stored because they are
    // shared with the stock module.
    $name = htmlspecialchars($item['name']);

    // Want to use item name in css class and id's, so replace some chars.
    $id = preg_replace('/[^a-z0-9_-]/i', '-', $name);
    $download = $item['download'];
    $price = '<span class="item-price">' . $price_text . '$' . $item['price'] .
      '</span>';
    // If 'variable' is true need to make price an input field.
    if ($item['variable'] === '1') {
      $minimum = $item['minimum'];
      // If minimum price is zero for a downloadable item provide a
      // download link.
      $download_content = '';
      if ($download !== '' && $minimum === '0.00') {
        $download_content = '<a class="item-download hidden" ' .
          'href="' . $download . '">Yes you can download for free!</a>';
      }
      $minimum_content = '';
      if ($minimum !== '0.00') {
        $minimum_content = '(min: $' .
          '<span class="item-minimum">' . $minimum . '</span>)';
      }
      $price = '<i>You can choose a price for this item, suggested ' .
          'price is displayed.</i><br>' .
        '<label for="item-price-' . $id . '">Price:</label>' .
        '<input id="item-price-' . $id . '" class="item-price" ' .
          'value="' . $item['price'] . '" type="text" maxlength="11"> ' .
        $minimum_content . $download_content;
    }
    // If an item can be downloaded, display an 'add to cart' button
    // rather than an input for quantity.
    $quantity = '<button class="item-add">Add to cart</button>';
    if ($download === '') {
      $quantity = '<span class="item-quantity-wrapper">' .
          '<label for="item-quantity-' . $id . '">' . $quantity_text .
          '</label>' .
          '<input id="item-quantity-' . $id . '" class="item-quantity" ' .
            'type="text" value="0">' .
        '</span> <span class="item-after-quantity-' . $id . '"></span>';
    }
    $image = '';
    if (preg_match('/^(.+)\.(.+)$/', $item['image'], $matches)) {
      $image = '<img class="item-image" src="' . $matches[1];
      $type = $matches[2];
      if (in_array(strtolower($type), ['gif', 'jpeg', 'jpg', 'png'])) {
        $image .= '_thumb.' . $type;
      }
      $image .= '">';
    }
    // Items are hidden when unavailable and when the user is logged in and has
    // permission to edit the page, so that they can be sorted.
    $hidden = isset($item['available']) && $item['available'] === '0' ?
      ' hidden' : '';
    return '<span id="cart-item-id-' . $id . '" ' .
        'class="cart-item' . $hidden . '"> ' .
      '<span class="item-name">' .
        '<a href="#cart-item-id-' . $id . '">' . $name . '</a>' .
      '</span> <a href="#cart-item-id-' . $id . '">' . $image . '</a> ' .
      '<span class="item-short-description">' . $item['short'] . '</span> ' .
      $price . ' ' . $quantity .
      '<span class="item-group-wrapper hidden">' .
        '<label for="item-group-' . $id . '">Group: </label>' .
        '<input id="item-group-' . $id . '" class="item-group" ' .
          'type="checkbox">' .
      '</span> <span class="clear"></span> </span>';
  }

  private function Gateway($name, $data = NULL) {
    $gateway = '';
    $gateway_api_key = '';
    $gateway_password = '';
    $mysqli = connect_db();
    $query = 'SELECT gateway, gateway_api_key, gateway_password FROM ' .
      'cart_method WHERE user = "' . $this->owner . '" AND ' .
      'name = "' . $name . '" AND available = 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_method = $mysqli_result->fetch_assoc()) {
        $gateway = $cart_method['gateway'];
        $gateway_api_key = $cart_method['gateway_api_key'];
        $gateway_password = $cart_method['gateway_password'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->Gateway: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($gateway === 'eway') {
      if ($gateway_api_key === '' || $gateway_password === '') {
        return '<div class="error ui-state-error ui-corner-all">' .
          'Gateway not set.</div>';
      }

      $auth = base64_encode($gateway_api_key . ':' . $gateway_password);
      $curl_headers = ['Authorization: Basic ' . $auth,
                       'Content-Type: application/json'];
      // For sandbox set domain to: api.sandbox.ewaypayments.com
      $url = 'https://api.ewaypayments.com/';
      // If data was passed to this function, user wants to make a payment
      // so need to get an access code. Otherwise querying results.
      if (isset($data)) {
        $url .= 'AccessCodes';
      }
      else {
        if (!isset($_GET['AccessCode'])) {
          return '<div class="error ui-state-error ui-corner-all">' .
            'Transaction not found.</div>';
        }
        $url .= 'AccessCode/' . $_GET['AccessCode'];
      }
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      if (isset($data)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
      }
      $result = json_decode(curl_exec($ch), true);
      curl_close($ch);

      // If data was passed to the function now return the payment form.
      if (isset($data)) {
        $access_code = NULL;
        $form_action_url = NULL;
        if (isset($result['AccessCode'])) {
          $access_code = $result['AccessCode'];
        }
        if (isset($result['FormActionURL'])) {
          $form_action_url = $result['FormActionURL'];
        }
        if (!isset($access_code) ||
            !isset($form_action_url) || $result['Errors']) {
          return '<div class="error ui-state-error ui-corner-all">' .
            'There was a problem processing your details.<br>' .
            'Please contact support.</div>';
        }
        // Look up customer in data to automatically set the card name.
        $customer = $data['Customer'];
        $full_name = $customer['FirstName'] . ' ' . $customer['LastName'];
        // Generate year options.
        $long = (int)date('Y');
        $short = (int)date('y');
        $options = '';
        for ($i = 0; $i < 7; $i++) {
          $options .= '<option value="' . $short++ . '">' . $long++ .
            '</option>';
        }
        return
          '<form id="eway-form" action="' . $form_action_url . '" ' .
            'method="post">' .
            'Please enter your credit card details:' .
            '<div class="form-spacing">' .
              '<label for="eway-name">Name:</label>' .
              '<input id="eway-name" name="EWAY_CARDNAME" ' .
                'value="' . $full_name . '" type="text">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="eway-number">Number:</label>' .
              '<input id="eway-number" name="EWAY_CARDNUMBER" type="text" ' .
                'maxlength="50">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="eway-number">Security Code:</label>' .
              '<input id="eway-cvn" name="EWAY_CARDCVN" type="text" ' .
                'maxlength="3">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="eway-expiry-month">Expiry:</label>' .
              '<select id="eway-expiry-month" name="EWAY_CARDEXPIRYMONTH">' .
                '<option value="01">Jan</option>' .
                '<option value="02">Feb</option>' .
                '<option value="03">Mar</option>' .
                '<option value="04">Apr</option>' .
                '<option value="05">May</option>' .
                '<option value="06">Jun</option>' .
                '<option value="07">Jul</option>' .
                '<option value="08">Aug</option>' .
                '<option value="09">Sep</option>' .
                '<option value="10">Oct</option>' .
                '<option value="11">Nov</option>' .
                '<option value="12">Dec</option>' .
              '</select> ' .
              '<select name="EWAY_CARDEXPIRYYEAR">' .
                $options .
              '</select>' .
            '</div>' .
            '<input type="hidden" name="EWAY_ACCESSCODE" ' .
              'value="' . $access_code . '">' .
            '<button id="cart-payment">Submit Payment</button>' .
          '</form>';
      }
      // If no data was provided, check the payment result.
      if (isset($result['TransactionStatus']) &&
          $result['TransactionStatus'] === true) {
        return true;
      }
      return '<div class="error ui-state-error ui-corner-all">' .
        'There was a problem processing your payment.<br>' .
        'Please contact support.</div>';
    }
    else if ($gateway === 'migs') {
      if (isset($data)) {
        if ($gateway_password === '' ||
            strpos($gateway_api_key, ':') === false) {
          return '<div class="error ui-state-error ui-corner-all">' .
            'Gateway not set.</div>';
        }

        list($merchant_id, $access_code) = explode(':', $gateway_api_key);
        $amount = $data['Payment']['TotalAmount'];
        $merch_txn_ref = time();
        $order_id = $_SESSION['cart-order-id'];
        $hash_input = 'vpc_AccessCode=' . $access_code . '&vpc_Amount=' .
          $amount . '&vpc_Command=pay&vpc_Locale=en&vpc_MerchTxnRef=' .
          $merch_txn_ref . '&vpc_Merchant=' . $merchant_id . '&vpc_OrderInfo=' .
          $order_id . '&vpc_ReturnURL=' . $data['RedirectURL'].'&vpc_Version=1';
        $secure_hash = strtoupper(hash_hmac('sha256', $hash_input,
                                            pack('H*', $gateway_password)));
        return
          '<form id="migs-form" action="https://migs.mastercard.com.au/vpcpay"'.
            ' method="post">' .
            '<input type="hidden" name="vpc_AccessCode" ' .
              'value="' . $access_code . '">' .
            '<input type="hidden" name="vpc_Amount" value="' . $amount . '">' .
            '<input type="hidden" name="vpc_Command" value="pay">' .
            '<input type="hidden" name="vpc_Locale" value="en">' .
            '<input type="hidden" name="vpc_MerchTxnRef" ' .
              'value="' . $merch_txn_ref . '">' .
            '<input type="hidden" name="vpc_Merchant" ' .
              'value="' . $merchant_id . '">' .
            '<input type="hidden" name="vpc_OrderInfo" value="' .$order_id.'">'.
            '<input type="hidden" name="vpc_ReturnURL" ' .
              'value="' . $data['RedirectURL'] . '">' .
            '<input type="hidden" name="vpc_Version" value="1">' .
            '<input type="hidden" name="vpc_SecureHash" ' .
              'value="' . $secure_hash . '">' .
            '<input type="hidden" name="vpc_SecureHashType" value="SHA256">' .
            '<button id="cart-payment">Go to payment page</button>' .
          '</form>';
      }
      // If no data was provided, check the payment result.
      if (isset($_GET["vpc_TxnResponseCode"]) &&
          $_GET['vpc_TxnResponseCode'] === '0') {
        return true;
      }
      return '<div class="error ui-state-error ui-corner-all">' .
        'There was a problem processing your payment.<br>' .
        'Please contact support.</div>';
    }
    else {
      return '<div class="error ui-state-error ui-corner-all">' .
        'Gateway not found.</div>';
    }
  }

  private function Payment($us_items, $amount, $us_name, $us_email,
                           $us_phone, $us_address, $us_city,
                           $us_postcode, $us_state, $us_country,
                           $us_notes, $us_method, $us_description = '',
                           $pack_date = '', $us_paypal_data = '') {
    $business = '';
    $currency = '';
    $mysqli = connect_db();
    $query = 'SELECT email, currency FROM cart_checkout WHERE ' .
      'user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_checkout = $mysqli_result->fetch_assoc()) {
        // email is not escaped when stored.
        $business = htmlspecialchars($cart_checkout['email']);
        $currency = $cart_checkout['currency'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->Payment 1: ' . $mysqli->error);
    }
    $payment_method = '';
    $method = $mysqli->escape_string($us_method);
    $query = 'SELECT name FROM cart_method WHERE ' .
      'user = "' . $this->owner . '" AND name = "' . $method . '" AND ' .
      'available = 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        // Don't allow setting method to account if the user isn't logged in.
        if ($method !== 'account' || $this->user->loggedIn) {
          $payment_method = $method;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->Payment 2: ' . $mysqli->error);
    }
    $method = $payment_method;
    // Store the details to send an email for the order in the session, but
    // it's not sent until return from payment processing. Also want a title
    // for customer notes, but not if it's empty.
    if ($us_notes !== '') {
      $us_notes = '<b>Customer notes:</b><br>' . $us_notes;
    }
    $patterns = ['/!items/', '/!name/', '/!email/', '/!phone/', '/!address/',
                 '/!postcode/', '/!city/', '/!state/', '/!country/',
                 '/!notes/'];
    $replacements = [$us_items, $us_name, $us_email, $us_phone, $us_address,
                     $us_postcode, $us_city, $us_state, $us_country, $us_notes];
    $us_email_text = $this->Substitute('cart-email', $patterns, $replacements);
    if ($method === 'pickup') {
      if ($us_description !== '' && $pack_date !== '') {
        $us_email_text .= 'Your order will be packed on: ' . $pack_date .
          ', it will be availabe to pick up on: ' . $us_description;
      }
    }
    else if ($pack_date !== '') {
      $us_email_text .= $this->Substitute('cart-delivery-info', '/!date/',
                                          $pack_date);
    }
    $email = $mysqli->escape_string($us_email);
    $email_text = $mysqli->escape_string($us_email_text);
    $query = 'INSERT INTO cart_order (user, email, description, method, ' .
      'timestamp) VALUES ("' . $this->owner . '", "' . $email . '", ' .
      '"' . $email_text . '", "' . $method . '", ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->Payment 3: ' . $mysqli->error);
    }
    $_SESSION['cart-order-id'] = $mysqli->insert_id;
    $mysqli->close();

    if ($business === '' || $currency === '' || $method === '') {
      return '<div class="error ui-state-error ui-corner-all">' .
        'Payment option not available.</div>';
    }

    if ($method === 'pickup' || $method === 'account') {
      $order_id = $_SESSION['cart-order-id'];
      $this->SendEmail('Confirmation for order number ' . $order_id,
                       $us_email_text, $us_email);
      return $order_id;
    }

    $us_first = strstr($us_name, ' ', true);
    $us_last = '';
    if ($us_first === false) {
      $us_first = $us_name;
    }
    else {
      $us_last = trim(strstr($us_name, ' '));
    }
    if ($method === 'paypal') {
      if ($us_paypal_data !== '') $us_items = $us_paypal_data;
      // Maximum item_name string to paypal is 127 chars.
      if (strlen($us_items) > 127) {
        $us_items = substr($us_items, 0, 124);
        $us_items .= '...';
      }
      $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
        'https://' : 'http://';
      $url = $scheme . $_SERVER['SERVER_NAME'] . $this->Url('payment=paypal');
      return
        '<form action="https://www.paypal.com/cgi-bin/webscr" method="post">' .
          '<input type="hidden" name="cmd" value="_ext-enter">' .
          '<input type="hidden" name="redirect_cmd" value="_xclick">' .
          '<input type="hidden" name="business" value="' . $business . '">' .
          '<input type="hidden" name="item_name" value="' . $us_items . '">' .
          '<input type="hidden" name="currency_code" value="' . $currency .'">'.
          '<input type="hidden" name="amount" value="' . $amount . '">' .
          '<input type="hidden" name="email" value="' . $us_email . '">' .
          '<input type="hidden" name="first_name" value="' . $us_first . '">' .
          '<input type="hidden" name="last_name" value="' . $us_last . '">' .
          '<input type="hidden" name="address1" value="' . $us_address . '">' .
          '<input type="hidden" name="city" value="' . $us_city . '">' .
          '<input type="hidden" name="zip" value="' . $us_postcode . '">' .
          '<input type="hidden" name="return" value="' . $url . '">' .
          '<button id="cart-payment">Pay with PayPal</button>' .
        '</form>';
    }

    if ($method === 'credit') {
      // TODO: Not using country here because eway requires two letter codes.
      $us_customer = ['Email' => $us_email, 'FirstName' => $us_first,
                      'LastName' => $us_last, 'Street1' => $us_address,
                      'City' => $us_city, 'PostalCode' => $us_postcode,
                      'State' => $us_state];
      $payment = ['TotalAmount' => (int)($amount * 100)];
      $redirect = $_SERVER['SERVER_NAME'] . $this->Url('payment=credit');
      $us_data = ['Customer' => $us_customer, 'Payment' => $payment,
                  'RedirectURL' => 'https://' . $redirect];
      return $this->Gateway('credit', $us_data);
    }

    return '<div class="error ui-state-error ui-corner-all">' .
      'Payment method not found.</div>';
  }

  private function PaymentMethods($display_cash = false,
                                  $id = 'customer-detail-method') {
    $mysqli = connect_db();
    $payment_method = '';
    $select_finished = false;
    // When cash and eftpos options need to be displayed always show a select,
    // otherwise the select is only shown if there's more than one option.
    if ($display_cash) {
      $payment_method = '<label for="' . $id . '">Payment:</label>' .
        '<select id="' . $id . '">' .
          '<option value=""></option>' .
          '<option value="cash">Cash</option>' .
          '<option value="eftpos">Eftpos</option>' .
          '<option value="account">Account</option>';
    }
    $query = 'SELECT name FROM cart_method WHERE user = "' . $this->owner .'" '.
      'AND available = 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($payment_method === '' && $mysqli_result->num_rows === 1) {
        if ($cart_method = $mysqli_result->fetch_assoc()) {
          if ($cart_method['name'] !== 'account' || $this->user->loggedIn) {
            $payment_method = '<input id="' . $id . '" type="hidden" ' .
              'value="' . $cart_method['name'] . '">';
          }
          // (There never was a select in this case.)
          $select_finished = true;
        }
      }
      else {
        if ($payment_method === '') {
          $payment_method = '<label for="' . $id . '">Payment:</label>' .
            '<select id="' . $id . '">';
        }
        while ($cart_method = $mysqli_result->fetch_assoc()) {
          $method = $cart_method['name'];
          $description = '';
          if ($method === 'credit') {
            $description = 'Credit Card';
          }
          else if ($method === 'paypal') {
            $description = 'PayPal';
          }
          else if ($method === 'pickup') {
            // Don't need pickup method when display_cash is true.
            if ($display_cash) continue;
            $description = 'On pick up';
          }
          else if ($method === 'account') {
            // The user needs to be logged in to use account method.
            if (!$this->user->loggedIn) continue;
            // Don't need account method when display_cash is true.
            if ($display_cash) continue;
            $description = 'Account';
          }
          $payment_method .= '<option value="' . $method . '">' . $description .
            '</option>';
        }
        $payment_method .= '</select>';
        $select_finished = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->PaymentMethods: ' . $mysqli->error);
    }
    $mysqli->close();
    // If displaying cash but there were no other options, finish the select.
    if ($display_cash && !$select_finished) {
      $payment_method .= '</select>';
    }
    return $payment_method;
  }

  private function ProcessingCost($total) {
    $mysqli = connect_db();
    $fee = 0;
    $query = 'SELECT name, fee FROM cart_method WHERE ' .
      'user = "' . $this->owner . '" AND available = 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        if ($cart_method = $mysqli_result->fetch_assoc()) {
          $fee = (float)$cart_method['fee'];
        }
      }
      else if ($mysqli_result->num_rows > 1) {
        // When more than one payment method is allowed, make sure the method
        // selected by the user is in the results.
        $payment_method = $mysqli->escape_string($_POST['method']);
        while ($cart_method = $mysqli_result->fetch_assoc()) {
          if ($payment_method === $cart_method['name']) {
            $fee = (float)$cart_method['fee'];
            break;
          }
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->ProcessingCost: ' . $mysqli->error);
    }
    $mysqli->close();
    // Assume fee is a percentage of the total when less than 20%.
    return $fee < 0.20 ? $fee * $total : $fee;
  }

  private function RemoveShipping() {
    $mysqli = connect_db();
    $id = (int)$_POST['id'];
    $query = 'DELETE FROM cart_shipping WHERE user = "' . $this->owner . '" ' .
      'AND id = ' . $id;
    if (!$mysqli->query($query)) {
      $this->Log('Cart->RemoveShipping: ' . $mysqli->error);
    }
    $mysqli->close();
    return $this->Shipping();
  }

  private function RemoveItem($name = '', $user = '', $all_items = true) {
    $mysqli = connect_db();
    if ($name === '') $name = $mysqli->escape_string($_POST['name']);
    if ($user === '') $user = $this->owner;
    $query = 'DELETE FROM cart_items WHERE user = "' . $user . '" AND ' .
      'name = "' . $name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->RemoveItem 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM cart_tracking WHERE user = "' . $user . '" ' .
      'AND name = "' . $name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->RemoveItem 2: ' . $mysqli->error);
    }
    // RemoveItem is called when an item is no longer tracked, but don't want
    // to lose the item's order in case tracking is used again later so leave
    // the entry in the cart_item_page table in this case. (The all_items flag
    // is only set to false in this case so use that here.)
    if ($all_items) {
      $query = 'DELETE FROM cart_item_page WHERE user = "' . $user . '" ' .
        'AND name = "' . $name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Cart->RemoveItem 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    $this->UpdateItemOrder($name);
    if ($all_items) return $this->AllItems();
  }

  private function ResetItem() {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $query = 'UPDATE cart_tracking SET modified = 0 WHERE ' .
      'user = "' . $this->owner . '" AND name = "' . $name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->ResetItem 1: ' . $mysqli->error);
    }
    $product = '';
    $supplier = '';
    $query = 'SELECT product, supplier FROM cart_tracking WHERE ' .
      'user = "' . $this->owner . '" AND name = "'. $name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_tracking = $mysqli_result->fetch_assoc()) {
        $product = $cart_tracking['product'];
        $supplier = $cart_tracking['supplier'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->ResetItem 2: ' . $mysqli->error);
    }
    $mysqli->close();
    $stock = new Module($this->user, $this->owner, 'stock');
    $stock->Factory('UpdateCart', [$product, $supplier]);
    return $this->AllItems();
  }

  private function SaveItem($name_only = false) {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $old_name = $mysqli->escape_string($_POST['oldName']);

    if ($name === '') {
      return ['error' => 'Please provide a name for the item.'];
    }

    if ($old_name !== '' && $old_name !== $name) {
      // Don't allow changing the name to an item that already exists.
      $exists = false;
      $query = 'SELECT name FROM cart_items WHERE name = "' . $name . '" AND ' .
        'user = "' . $this->owner . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($cart_items = $mysqli_result->fetch_assoc()) {
          $exists = true;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Cart->SaveItem 1: ' . $mysqli->error);
      }
      if ($exists) {
        $mysqli->close();
        return ['error' => 'An item with that name already exists.'];
      }

      $query = 'UPDATE cart_items SET name = "' . $name . '" WHERE ' .
        'user = "' . $this->owner . '" AND name = "' . $old_name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Cart->SaveItem 2: ' . $mysqli->error);
      }
      $query = 'UPDATE cart_item_page SET name = "' . $name . '" WHERE ' .
        'user = "' . $this->owner . '" AND name = "' . $old_name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Cart->SaveItem 3: ' . $mysqli->error);
      }
      $query = 'UPDATE cart_tracking SET name = "' . $name . '" WHERE ' .
        'user = "' . $this->owner . '" AND name = "' . $old_name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Cart->SaveItem 4: ' . $mysqli->error);
      }
      if ($name_only) {
        $mysqli->close();
        return ['done' => true];
      }
    }

    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $config->set('Attr.AllowedFrameTargets', ['_blank']);
    $purifier = new HTMLPurifier($config);
    $us_full = $purifier->purify($_POST['full']);

    $us_image = htmlspecialchars($_POST['image']);
    $image = $mysqli->escape_string($us_image);
    $us_short = htmlspecialchars($_POST['short']);
    $short = $mysqli->escape_string($us_short);
    $full = $mysqli->escape_string($us_full);
    $weight = price_string((float)$_POST['weight']);
    $price = price_string((float)$_POST['price']);
    $variable = (int)$_POST['variable'];
    $minimum = price_string((float)$_POST['minimum']);
    $download = $mysqli->escape_string(htmlspecialchars($_POST['download']));
    // Note that tabs are not escaped as they are not expected to be displayed
    // as they are in html and are required to be used elsewere unescaped.
    $item_tab = $mysqli->escape_string($_POST['itemTab']);
    $item_order = (int)$_POST['itemOrder'];
    $available = (int)$_POST['available'];
    $tracking = (int)$_POST['tracking'];

    if ($tracking === 1) {
      $modified = false;
      // If the user modifies image, short or full, flag the item so that any
      // further updates to these fields from the stock module don't overwrite
      // the user's changes.
      $query = 'SELECT image, short, full FROM cart_items WHERE ' .
        'user = "' . $this->owner . '" AND name = "' . $name . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($cart_items = $mysqli_result->fetch_assoc()) {
          if ($cart_items['image'] !== $us_image ||
              $cart_items['short'] !== $us_short ||
              $cart_items['full'] !== $us_full) {
            $modified = true;
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Cart->SaveItem 5: ' . $mysqli->error);
      }
      if ($modified) {
        $query = 'UPDATE cart_tracking SET modified = 1 WHERE ' .
          'user = "' . $this->owner . '" AND name = "' . $name . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Cart->SaveItem 7: ' . $mysqli->error);
        }
      }
    }

    $query = 'INSERT INTO cart_items VALUES ("' . $this->owner . '", ' .
      '"' . $name . '", "' . $image . '", "' . $short . '", "' . $full . '", ' .
      $weight . ', ' . $price . ', ' . $variable . ', ' . $minimum . ', ' .
      '"' . $download . '") ON DUPLICATE KEY UPDATE image = "' . $image . '", '.
      'short = "' . $short . '", full = "' . $full . '", weight = ' . $weight .
      ', price = ' . $price . ', variable = ' . $variable . ', ' .
      'minimum = ' . $minimum . ', download = "' . $download . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveItem 8: ' . $mysqli->error);
    }
    $query = 'INSERT INTO cart_item_page VALUES ("' . $this->owner . '", ' .
      '"' . $name . '", "' . $this->user->page . '", ' . $available . ', ' .
      '"' . $item_tab . '", ' . $item_order . ') ON DUPLICATE KEY UPDATE ' .
      'available = ' . $available . ', tab = "' . $item_tab . '", ' .
      'item_order = ' . $item_order;
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveItem 9: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->UpdateItemOrder($name);
    return ['done' => true];
  }

  private function SaveCheckout() {
    $mysqli = connect_db();
    $email = $mysqli->escape_string($_POST['email']);
    $currency = $mysqli->escape_string(htmlspecialchars($_POST['currency']));
    $paypalAvailable = (int)$_POST['paypal'];
    $paypalFee = (float)$_POST['paypalFee'];
    $pickupAvailable = (int)$_POST['pickup'];
    $pickupFee = (float)$_POST['pickupFee'];
    $accountAvailable = (int)$_POST['account'];
    $accountFee = (float)$_POST['accountFee'];
    $creditAvailable = (int)$_POST['credit'];
    $creditFee = (float)$_POST['creditFee'];
    $gateway = $mysqli->escape_string(htmlspecialchars($_POST['gateway']));
    $key = $mysqli->escape_string($_POST['gatewayApiKey']);
    $password = $mysqli->escape_string($_POST['gatewayPassword']);
    $query = 'INSERT INTO cart_checkout VALUES ("' . $this->owner . '", ' .
      '"' . $email . '", "' . $currency . '") ON DUPLICATE KEY UPDATE ' .
      'email = "' . $email . '", currency = "' . $currency . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveCheckout 1: ' . $mysqli->error);
    }
    // The current gateway password is not shown in the form, so don't update
    // it if empty.
    $password_query = $password === '' ? '' :
      'gateway_password = "' . $password . '", ';
    $query = 'INSERT INTO cart_method VALUES ("' . $this->owner . '", ' .
      '"credit", ' . $creditAvailable . ', "' . $gateway . '", ' .
      '"' . $key . '","' . $password . '", ' . $creditFee . ') ' .
      'ON DUPLICATE KEY UPDATE available = ' . $creditAvailable . ', ' .
      'gateway = "' . $gateway . '", gateway_api_key = "' . $key . '", ' .
      $password_query . 'fee = ' . $creditFee;
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveCheckout 2: ' . $mysqli->error);
    }
    $query = 'INSERT INTO cart_method VALUES ("' . $this->owner . '", ' .
      '"paypal", ' . $paypalAvailable . ', "", "", "", ' . $paypalFee . ') ' .
      'ON DUPLICATE KEY UPDATE available = ' . $paypalAvailable . ', fee = ' .
      $paypalFee;
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveCheckout 3: ' . $mysqli->error);
    }
    $query = 'INSERT INTO cart_method VALUES ("' . $this->owner . '", ' .
      '"pickup", ' . $pickupAvailable . ', "", "", "", ' . $pickupFee . ') ' .
      'ON DUPLICATE KEY UPDATE available = ' . $pickupAvailable . ', fee = ' .
      $pickupFee;
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveCheckout 4: ' . $mysqli->error);
    }
    $query = 'INSERT INTO cart_method VALUES ("' . $this->owner . '", ' .
      '"account", ' . $accountAvailable . ', "", "", "", ' .
      $accountFee . ') ON DUPLICATE KEY UPDATE ' .
      'available = ' . $accountAvailable . ', fee = ' . $accountFee;
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveCheckout 5: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function SaveShipping() {
    $mysqli = connect_db();
    $type = $mysqli->escape_string(htmlspecialchars($_POST['type']));
    $id = (int)$_POST['id'];
    $destination_name =
      $mysqli->escape_string(htmlspecialchars($_POST['destinationName']));
    $destination_code =
      $mysqli->escape_string(htmlspecialchars($_POST['destinationCode']));
    $amount = price_string((float)$_POST['amount']);
    $minimum = price_string((float)$_POST['minimum']);
    $maximum = price_string((float)$_POST['maximum']);
    $rule = $mysqli->escape_string($_POST['rule']);
    $query = 'INSERT INTO cart_shipping VALUES ("' . $this->owner . '", ' .
      $id . ', "' . $type . '", "' . $destination_name . '", ' .
      '"' . $destination_code . '", ' . $amount . ', ' . $minimum . ', ' .
      $maximum . ', "' . $rule . '") ON DUPLICATE KEY UPDATE ' .
      'type = "' . $type . '", destination_name = "' . $destination_name .'", '.
      'destination_code = "' . $destination_code . '", ' .
      'amount = ' . $amount . ', minimum = ' . $minimum . ', ' .
      'maximum = ' . $maximum . ', rule = "' . $rule . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->SaveShipping: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function ShowSettings() {
    $result = ['content' => $this->Settings(),
               'items' => $this->AllItems(),
               'shipping' => $this->Shipping()];
    $mysqli = connect_db();
    $query = 'SELECT email, currency FROM cart_checkout WHERE ' .
      'user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_checkout = $mysqli_result->fetch_assoc()) {
        $result['checkout'] = $cart_checkout;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->ShowSettings 1: ' . $mysqli->error);
    }

    $result['method'] = [];
    $query = 'SELECT name, available, gateway, gateway_api_key, fee FROM ' .
      'cart_method WHERE user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_method = $mysqli_result->fetch_assoc()) {
        $result['method'][] = $cart_method;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->ShowSettings 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function Shipping() {
    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT type, id, destination_name, destination_code, amount, ' .
      'minimum, maximum, rule FROM cart_shipping WHERE ' .
      'user = "' . $this->owner . '" ' .
      'ORDER BY type, destination_name, destination_code, amount';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_shipping = $mysqli_result->fetch_assoc()) {
        $result[] = $cart_shipping;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->Shipping: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function ShippingCost($total_price, $rules) {
    $shipping = 0;
    for ($i = 0; $i < count($rules); $i++) {
      if ($rules[$i]['rule'] === 'percent') {
        $shipping += $total_price * (float)$rules[$i]['amount'] / 100;
      }
      else {
        $shipping += (float)$rules[$i]['amount'];
      }
    }
    return $shipping;
  }

  private function ShippingDestination($us_destination, $total_weight) {
    $rules = [];
    $mysqli = connect_db();
    $destination = $mysqli->escape_string($us_destination);
    $destination_query = '(destination_name = "' . $destination . '" ' .
      'OR destination_code = "' . $destination . '")';
    if ($destination === '') {
      // Need to be careful with an empty destination parameter, don't
      // want to match when only one of the two fields has been filled out.
      $destination_query = 'destination_name = "" AND destination_code = ""';
    }
    $query = 'SELECT amount, minimum, maximum, rule FROM cart_shipping WHERE ' .
      'user = "' . $this->owner . '" AND ' . $destination_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_shipping = $mysqli_result->fetch_assoc()) {
        // When weight is used, make sure this rule matches the total weight
        // otherwise leave it for a more generic rule to match. Also there
        // doesn't need to be a maximum set, but that is stored as 0.
        if ($cart_shipping['rule'] === 'weight') {
          $minimum = (float)$cart_shipping['minimum'];
          $maximum = (float)$cart_shipping['maximum'];
          if ($total_weight < $minimum ||
              ($maximum > 0.01 && $total_weight > $maximum)) {
            continue;
          }
        }
        $rules[] = $cart_shipping;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->ShippingDestination: ' . $mysqli->error);
    }
    $mysqli->close();
    return $rules;
  }

  private function ShippingRules($us_city, $us_state,
                                 $us_country, $total_weight) {
    // Return a set of rules when a location is matched with precedence going
    // to matching a city first, then state, then country.
    $rules = $this->ShippingDestination($us_city, $total_weight);
    if (!$rules) {
      $rules = $this->ShippingDestination($us_state, $total_weight);
    }
    if (!$rules) {
      $rules = $this->ShippingDestination($us_country, $total_weight);
    }
    // Lastly try an empty destination as a catch-all for other locations.
    if (!$rules) {
      $rules = $this->ShippingDestination('', $total_weight);
    }
    return $rules;
  }

  private function SendEmail($subject, $message, $email = '') {
    $business = '';
    $mysqli = connect_db();
    $query = 'SELECT email FROM cart_checkout WHERE ' .
      'user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_checkout = $mysqli_result->fetch_assoc()) {
        $business = $cart_checkout['email'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->SendEmail: ' . $mysqli->error);
    }
    $mysqli->close();

    $message = wordwrap('<html><head><title>Order Confirmation</title>' .
                        "</head>\n<body>\n" . $message . '</body></html>');
    $sender = $this->Substitute('system-sender', '/!host/',
                                $this->user->config->ServerName());
    $sender_name = $this->Substitute('system-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = substitute('system-sender-cc', $this->user->group);
    $bcc = substitute('system-sender-bcc', $this->user->group);

    $headers = "MIME-Version: 1.0\r\n".
      "Content-type: text/html; charset=utf-8\r\n" .
      "From: " . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($email !== '') {
      $user = $this->user->loggedIn ? $this->user->name : '';
      // Don't use bcc here because the email is sent again below.
      dobrado_mail($email, $subject, $message, $headers, '',
                   $sender, $user, 'cart', 'sendEmail');
    }
    $headers .= 'Reply-To: ' . $email . "\r\n";
    dobrado_mail($business, $subject, $message, $headers, $bcc,
                 $sender, $this->owner, 'cart', 'sendEmail');
  }

  private function Tracking($price_level = 'retail') {
    $tracking_list = [];
    $stock = new Module($this->user, $this->owner, 'stock');
    $available = $stock->Factory('AvailableProducts');
    $mysqli = connect_db();
    $query = 'SELECT name, supplier, product, modified FROM cart_tracking ' .
      'WHERE user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_tracking = $mysqli_result->fetch_assoc()) {
        $name = $cart_tracking['name'];
        $product = $cart_tracking['product'];
        if (isset($available[$product])) {
          $supplier = $cart_tracking['supplier'];
          $modified = $cart_tracking['modified'] === '1';
          $price = $available[$product][$price_level];
          $base_price = $available[$product]['price'];
          $grower = $available[$product]['grower'];
          $tracking_list[$name] = ['name' => $product, 'supplier' => $supplier,
                                   'modified' => $modified, 'grower' => $grower,
                                   'basePrice' => price_string($base_price),
                                   'price' => price_string($price)];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->Tracking: ' . $mysqli->error);
    }
    $mysqli->close();
    return $tracking_list;
  }

  private function UpdateItem($product, $supplier, $image,
                              $short, $full, $price, $available,
                              $category, $tracking, $old_name) {
    $mysqli = connect_db();
    // cart-user is required because this function is called from the stock
    // module, which could have a different page owner. It should be set to
    // the owner of the page where the cart module is used for this group.
    $user = $mysqli->escape_string($this->Substitute('cart-user'));
    if ($user === '') {
      $mysqli->close();
      return ['error' => 'User not set for cart module.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    if (!$organiser->MatchUser($user)) {
      $mysqli->close();
      return ['error' => 'User not found for cart module.'];
    }

    // First update the product name from the stock list that is used to track
    // this cart item, if it has changed.
    if ($old_name !== '') {
      $query = 'UPDATE cart_tracking SET product = "' . $product . '" WHERE ' .
        'product = "' . $old_name . '" AND supplier = "' . $supplier . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Cart->UpdateItem 1: ' . $mysqli->error);
      }
    }
    $name = '';
    $modified = false;
    $current_tracking = false;
    $query = 'SELECT name, modified FROM cart_tracking WHERE ' .
      'user = "' . $user . '" AND product = "' . $product . '" AND ' .
      'supplier = "' . $supplier . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_tracking = $mysqli_result->fetch_assoc()) {
        $name = $mysqli->escape_string($cart_tracking['name']);
        $modified = $cart_tracking['modified'] === '1';
        $current_tracking = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->UpdateItem 2: ' . $mysqli->error);
    }
    if ($current_tracking) {
      // If tracking is off for an item then remove it from the cart.
      // Nothing is done if the item doesn't exist here because the stock
      // module sends updates for all items when cart syncing is used.
      if ($tracking === 0) {
        $this->RemoveItem($name, $user, false);
        $mysqli->close();
        return ['done' => true];
      }

      // If old_name is set here and the product is tracked, need to also reset
      // the name used for the cart item in case it contains size information.
      if ($old_name !== '') {
        $weight = $this->CalculateWeight($product);
        $query = 'UPDATE cart_items SET name = "' . $product . '", ' .
          'weight = ' . $weight . ' WHERE name = "' . $name . '" AND ' .
          'user = "' . $user . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Cart->UpdateItem 3: ' . $mysqli->error);
        }
        $query = 'UPDATE cart_item_page SET name = "' . $product . '" WHERE ' .
          'name = "' . $name . '" AND user = "' . $user . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Cart->UpdateItem 4: ' . $mysqli->error);
        }
      }
    }
    else if ($tracking === 0) {
      // Not currently tracked so don't start tracking this item.
      $mysqli->close();
      return ['done' => true];
    }

    // Use the stock name from the product when it doesn't exist here.
    if ($name === '') $name = $product;
    $query = 'INSERT INTO cart_tracking VALUES ("' . $user . '", ' .
      '"' . $name . '", "' . $supplier . '", "' . $product . '", 0) ' .
      'ON DUPLICATE KEY UPDATE supplier = "' . $supplier . '", ' .
      'product = "' . $product . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->UpdateItem 5: ' . $mysqli->error);
    }
    // The modified flag is used here to decide which fields can be updated for
    // existing items.
    $price = price_string($price);
    $weight = $this->CalculateWeight($name);
    $query = 'INSERT INTO cart_items VALUES ("' . $user . '", ' .
      '"' . $name . '", "' . $image . '", "' . $short . '", "' . $full . '", ' .
      $weight . ', ' . $price . ', 0, 0, "") ON DUPLICATE KEY UPDATE ';
    if ($modified) {
      $query .= 'price = ' . $price;
    }
    else {
      $query .= 'image = "' . $image . '", short = "' . $short . '", ' .
        'full = "' . $full . '", price = ' . $price;
    }
    if (!$mysqli->query($query)) {
      $this->Log('Cart->UpdateItem 6: ' . $mysqli->error);
    }
    // Updating availability from stock can only be done for a specified page.
    $page = $mysqli->escape_string($this->Substitute('cart-page'));
    if ($page === '') {
      $mysqli->close();
      return ['error' => 'Page not set for cart module.'];
    }

    $query = 'INSERT INTO cart_item_page VALUES ("' . $user . '", ' .
      '"' . $name . '", "' . $page . '", ' . $available . ', ' .
      '"' . $category . '", 0) ON DUPLICATE KEY UPDATE ' .
      'available = ' . $available . ', tab = "' . $category . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->UpdateItem 7: ' . $mysqli->error);
    }
    $mysqli->close();
    $this->UpdateItemOrder($name, $user, $page);
    return ['done' => true];
  }

  private function UpdateItemOrder($name, $user = '', $page = '') {
    if ($user === '') $user = $this->owner;
    if ($page === '') $page = $this->user->page;

    $tab = '';
    $mysqli = connect_db();
    // Only need to update the tab this item is on if tabs are used.
    $query = 'SELECT tab FROM cart_item_page WHERE user = "' . $user . '" ' .
      'AND name = "' . $name . '" AND page = "' . $page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_item_page = $mysqli_result->fetch_assoc()) {
        $tab = $cart_item_page['tab'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->UpdateItemOrder 1: ' . $mysqli->error);
    }
    $tab_query = $tab === '' ? '' : 'AND tab = "' . $tab . '" ';
    // Store all available items in order so that they can be re-indexed.
    $items = [];
    $query = 'SELECT name, tab FROM cart_item_page WHERE ' .
      'user = "' . $user . '" AND page = "' . $page . '" AND available = 1 ' .
      $tab_query . 'ORDER BY tab, item_order';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($cart_item_page = $mysqli_result->fetch_assoc()) {
        $tab = $cart_item_page['tab'];
        if (isset($items[$tab])) {
          $items[$tab][] = $cart_item_page['name'];
        }
        else {
          $items[$tab] = [$cart_item_page['name']];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->UpdateItemOrder 2: ' . $mysqli->error);
    }
    foreach ($items as $tab) {
      $order = 0;
      foreach ($tab as $name) {
        $query = 'UPDATE cart_item_page SET item_order = ' . $order .
          ' WHERE user = "' . $user . '" AND name = "' . $name . '"' .
          ' AND page = "' . $page . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Cart->UpdateItemOrder 3: ' . $mysqli->error);
        }
        $order++;
      }
    }
    $mysqli->close();
  }

  private function UpdatePosition() {
    $us_names = json_decode($_POST['names'], true);
    if (!is_array($us_names) || count($us_names) === 0) {
      return ['error' => 'Item name not provided.'];
    }

    $mysqli = connect_db();
    $name = $mysqli->escape_string($us_names[0]);
    $old_position = (int)$_POST['oldPosition'];
    $new_position = (int)$_POST['newPosition'];
    $tab = '';
    $query = 'SELECT tab FROM cart_item_page WHERE ' .
      'user = "' . $this->owner . '" AND name = "' . $name . '" AND ' .
      'page = "' . $this->user->page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_item_page = $mysqli_result->fetch_assoc()) {
        $tab = $cart_item_page['tab'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->UpdatePosition 1: ' . $mysqli->error);
    }
    if ($new_position < $old_position) {
      // The item has been moved up, move the displaced items down.
      $query = 'UPDATE cart_item_page SET item_order = item_order + 1 WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"' .
        ' AND tab = "' . $tab . '" AND item_order >= ' . $new_position .
        ' AND item_order < ' . $old_position;
      if (!$mysqli->query($query)) {
        $this->Log('Cart->UpdatePosition 2: ' . $mysqli->error);
      }
    }
    else if ($new_position > $old_position) {
      // The item has been moved down, move the displaced items up.
      $query = 'UPDATE cart_item_page SET item_order = item_order - 1 WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"' .
        ' AND tab = "' . $tab . '" AND item_order > ' . $old_position .
        ' AND item_order <= ' . $new_position;
      if (!$mysqli->query($query)) {
        $this->Log('Cart->UpdatePosition 3: ' . $mysqli->error);
      }
    }
    $name_query = 'name = "' . $name . '"';
    if (count($us_names) > 1) {
      // When multiple items are given just set them all to the same position
      // and re-order the whole tab.
      $name_query = '';
      foreach ($us_names as $us_item_name) {
        if ($name_query !== '') $name_query .= ' OR ';
        $name_query .= 'name = "' . $mysqli->escape_string($us_item_name) . '"';
      }
      $name_query = '(' . $name_query . ')';
    }
    $query = 'UPDATE cart_item_page SET item_order = ' . $new_position .
      ' WHERE user = "' . $this->owner . '" AND ' . $name_query .
      ' AND page = "' . $this->user->page . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Cart->UpdatePosition 4: ' . $mysqli->error);
    }
    if (count($us_names) > 1) {
      $order = 0;
      $query = 'SELECT name FROM cart_item_page WHERE ' .
        'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"' .
        ' AND tab = "' . $tab . '" ORDER BY item_order';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($cart_item_page = $mysqli_result->fetch_assoc()) {
          $name = $mysqli->escape_string($cart_item_page['name']);
          $query = 'UPDATE cart_item_page SET item_order = ' . $order .
            ' WHERE user = "' . $this->owner . '" AND name = "' . $name . '"' .
            ' AND page = "' . $this->user->page . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Cart->UpdatePosition 5: ' . $mysqli->error);
          }
          $order++;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Cart->UpdatePosition 6: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function VerifyItem($us_item, $price, $stock_price) {
    $verified = false;
    $original = 0;
    $weight = 0;
    $mysqli = connect_db();
    $item = $mysqli->escape_string($us_item);
    $query = 'SELECT weight, price, variable, minimum, download FROM ' .
      'cart_items WHERE user = "' . $this->owner . '" AND name = "' . $item.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($cart_items = $mysqli_result->fetch_assoc()) {
        // If the price is variable, make sure it's at least the minimum.
        if ($cart_items['variable'] === '1') {
          $verified = $price >= (float)$cart_items['minimum'];
        }
        // If not variable then make sure it hasn't changed.
        else {
          $original = $stock_price === 0 ?
            (float)$cart_items['price'] : $stock_price;
          // Convert to cents to do integer comparison.
          $verified = (int)($price * 100) === (int)($original * 100);
        }
        $weight = (float)$cart_items['weight'];
        // Unverified items are bad and shouldn't happen so log here.
        if (!$verified) {
          $this->Log('Cart->VerifyItem 1: ' . $item . ' not verified. ' .
                     'price = ' . $price . ', original = ' . $original);
        }
        // If the item has been verified and the price is not zero, check if
        // this item should be automatically downloaded when the buyer returns
        // to the page and therefore stored in the session.
        else if ($price !== 0 && $cart_items['download'] !== '') {
          $_SESSION['cart'][] = $cart_items['download'];
        }
        // If not downloadable want to know if weight isn't set as this affects
        // shipping costs.
        else if ($weight === 0) {
          $this->Log('Cart->VerifyItem 2: ' . $item . ' weight not set!');
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Cart->VerifyItem 3: ' . $mysqli->error);
    }
    $mysqli->close();
    return [$verified, $weight];
  }

  private function Settings() {
    $tab_filter = '';
    $tab_names =
      explode(',', $this->Substitute('cart-tabs-' . $this->user->page));
    if (count($tab_names) > 1) {
      $tab_filter = '<label for="cart-filter-tabs">Show tab: </label> ' .
        '<select id="cart-filter-tabs"><option>All tabs</option>';
      foreach ($tab_names as $tab) {
        $tab_filter .= '<option>' . $tab . '</option>';
      }
      $tab_filter .= '</select>';
    }
    return '<div id="cart-settings-tabs">' .
          '<ul>' .
            '<li><a href="#cart-items-tab">Items</a></li>' .
            '<li><a href="#cart-shipping-tab">Shipping</a></li>' .
            '<li><a href="#cart-checkout-tab">Checkout</a></li>' .
          '</ul>' .
          '<div id="cart-items-tab">' .
            '<div id="cart-item-filters">Navigation filters: ' .
              '<label for="cart-filter-available">Show unavailable:' .
              '</label> <input id="cart-filter-available" type="checkbox"> ' .
              $tab_filter .
            '</div>' .
            '<div id="cart-item-button-wrapper">' .
              '<button id="cart-item-previous">Previous</button>' .
              '<button id="cart-item-new">New</button>' .
              '<button id="cart-item-remove">Remove</button>' .
              '<button id="cart-item-save">Save</button>' .
              '<button id="cart-item-next">Next</button>' .
            '</div>' .
            '<form id="cart-item-form">' .
              '<div class="form-spacing">' .
                '<label for="cart-item-name">Name:</label>' .
                '<input id="cart-item-name" type="text" maxlength="100">' .
              '</div>' .
              '<div class="form-spacing cart-tracking"></div>' .
              '<div class="form-spacing cart-modified hidden">' .
                '<label>Modified:</label>' .
                '<button id="cart-item-reset">reset</button> ' .
                '<button id="cart-item-reset-help">help</button> ' .
                '<div class="cart-modified-info hidden">This item has been ' .
                  'modified and no longer matches the stock item it was ' .
                  'created from. This means only price and availability are ' .
                  'updated automatically from your stock list. You can update '.
                  'all details from the stock item by clicking the reset. ' .
                  '<i>Note that this means you will lose any custom ' .
                    'description that has been entered.</i>'.
                '</div>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-item-image">Image:</label>' .
                '<input id="cart-item-image" type="text" maxlength="200"> ' .
                '<button class="cart-item-image-browse">browse</button>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-item-price">Price:</label>' .
                '<input id="cart-item-price" type="text" maxlength="11">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-item-weight">Weight:</label>' .
                '<input id="cart-item-weight" type="text" maxlength="10">' .
              '</div>' .
              '<div class="form-spacing cart-item-download-wrapper hidden">' .
                '<label for="cart-item-download">Download:</label>' .
                '<input id="cart-item-download" type="text" maxlength="200"> ' .
                '<button class="cart-item-download-browse">browse</button>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-item-available">Available:</label>' .
                '<input type="checkbox" id="cart-item-available">' .
              '</div>' .
              '<i>Specify the position to display this item in the store.</i>' .
              '<div class="form-spacing">' .
                '<label for="cart-item-tab">Tab:</label>' .
                '<input id="cart-item-tab" type="text" maxlength="100">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-item-order">Order:</label>' .
                '<input id="cart-item-order" type="text" maxlength="10">' .
              '</div>' .
              '<div class="form-spacing cart-item-variable-wrapper hidden">' .
                '<i>You can allow the buyer to enter their own price for the ' .
                  'item.</i>' .
                '<label for="cart-item-variable">Variable Price:</label>' .
                '<input type="checkbox" id="cart-item-variable">' .
              '</div>' .
              '<div class="form-spacing show-item-minimum hidden">' .
                '<label for="cart-item-minimum">Minimum Price:</label>' .
                '<input id="cart-item-minimum" type="text" maxlength="11">' .
              '</div>' .
              '<div class="form-spacing cart-item-short-wrapper">' .
                '<label for="cart-item-short">Short Description:</label>' .
                '<textarea id="cart-item-short"></textarea>' .
              '</div>' .
              '<div id="cart-item-full"></div>' .
            '</form>' .
          '</div>' .
          '<div id="cart-shipping-tab">' .
            '<button id="cart-shipping-new">new</button>' .
            '<button id="cart-shipping-previous">previous</button>' .
            '<button id="cart-shipping-next">next</button>' .
            '<form id="cart-shipping-form">' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-type">Type:</label>' .
                '<input id="cart-shipping-type" type="text" maxlength="100">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-destination-name">' .
                  'Destination Name:</label>' .
                '<input id="cart-shipping-destination-name" type="text" ' .
                  'maxlength="100">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-destination-code">' .
                  'Destination Code:</label>' .
                '<input id="cart-shipping-destination-code" type="text" ' .
                  'maxlength="4">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-amount">Amount:</label>' .
                '<input id="cart-shipping-amount" type="text" maxlength="11">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-minimum">Minimum:</label>' .
                '<input id="cart-shipping-minimum" type="text" maxlength="11">'.
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-maximum">Maximum:</label>' .
                '<input id="cart-shipping-maximum" type="text" maxlength="11">'.
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-shipping-rule">Rule:</label>' .
                '<select id="cart-shipping-rule">' .
                  '<option value="weight">weight</option>' .
                  '<option value="fixed">fixed</option>' .
                  '<option value="percent">percent</option>' .
                '</select>' .
              '</div>' .
              '<button id="cart-shipping-save">Save shipping</button>' .
              '<button id="cart-shipping-remove">Remove shipping</button>' .
            '</form>' .
          '</div>' .
          '<div id="cart-checkout-tab">' .
            '<form id="cart-checkout-form">' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-email">Email:</label>' .
                '<input id="cart-checkout-email" type="text" maxlength="200">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-currency">Currency:</label>' .
                  '<select id="cart-checkout-currency">' .
                  '<option value="AUD">AUD</option>' .
                  '<option value="CAD">CAD</option>' .
                  '<option value="EUR">EUR</option>' .
                  '<option value="GBP">GBP</option>' .
                  '<option value="JPY">JPY</option>' .
                  '<option value="USD">USD</option>' .
                  '<option value="NZD">NZD</option>' .
                  '<option value="CHF">CHF</option>' .
                  '<option value="HKD">HKD</option>' .
                  '<option value="SGD">SGD</option>' .
                  '<option value="SEK">SEK</option>' .
                  '<option value="DKK">DKK</option>' .
                  '<option value="PLN">PLN</option>' .
                  '<option value="NOK">NOK</option>' .
                  '<option value="HUF">HUF</option>' .
                  '<option value="CZK">CZK</option>' .
                  '<option value="ILS">ILS</option>' .
                  '<option value="MXN">MXN</option>' .
                  '<option value="BRL">BRL</option>' .
                  '<option value="MYR">MYR</option>' .
                  '<option value="PHP">PHP</option>' .
                  '<option value="TWD">TWD</option>' .
                  '<option value="THB">THB</option>' .
                  '<option value="TRY">TRY</option>' .
                '</select>' .
              '</div>' .
              '<div class="cart-checkout-payment-methods">' .
                '<h3>Payment methods:<h3>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-method-credit">Credit Card ' .
                  'available:</label>' .
                '<input type="checkbox" id="cart-checkout-method-credit">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-gateway">Gateway:</label>' .
                '<select id="cart-checkout-gateway">' .
                  '<option value="eway">eway</option>' .
                  '<option value="migs">migs</option>' .
                '</select>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-gateway-api-key">Gateway Api Key:' .
                '</label>' .
                '<input id="cart-checkout-gateway-api-key" type="text">' .
                '<div id="cart-migs-format">' .
                  '(For migs store MerchantID:AccessCode in Api Key)' .
                '</div>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-gateway-password">Gateway Password:'.
                '</label>' .
                '<input id="cart-checkout-gateway-password" type="password" ' .
                  'maxlength="100">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-credit-fee">Processing Fee:</label>'.
                '<input id="cart-checkout-credit-fee" type="text">' .
              '</div>' .
              '<hr>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-method-paypal">PayPal Available:' .
                '</label>' .
                '<input type="checkbox" id="cart-checkout-method-paypal">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-paypal-fee">Processing Fee:</label>'.
                '<input id="cart-checkout-paypal-fee" type="text">' .
              '</div>' .
              '<hr>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-method-pickup">Pick Up Available:' .
                '</label>' .
                '<input type="checkbox" id="cart-checkout-method-pickup">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-pickup-fee">Processing Fee:</label>'.
                '<input id="cart-checkout-pickup-fee" type="text">' .
              '</div>' .
              '<hr>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-method-account">Account Available:' .
                '</label>' .
                '<input type="checkbox" id="cart-checkout-method-account">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="cart-checkout-account-fee">Processing Fee:' .
                '</label>'.
                '<input id="cart-checkout-account-fee" type="text">' .
              '</div>' .
              '<button id="cart-checkout-save">Save checkout</button>' .
            '</form>' .
          '</div>' .
      '</div>';
  }

}
