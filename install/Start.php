<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Start extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if ($_POST['action'] === 'tooltip') return $this->Tooltip();
    if ($_POST['action'] === 'loginSubmit') return $this->LoginSubmit();
    if ($_POST['action'] === 'changeTheme') return $this->ChangeTheme();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    if ($this->owner === 'admin' &&
        $page === $this->Substitute('indieauth-page') &&
        $this->user->group === $this->Substitute('indieauth-group')) {
      return true;
    }
    // Add start module for all users on Unicyclic, but only want to add it for
    // users in the admin group or single user mode sites otherwise.
    $profile = $this->Substitute('start-profile');
    if ($this->owner === $this->user->name &&
        ($this->GroupMember('admin', 'admin') ||
         $profile === 'ihaza' || $profile === 'unicyclic')) {
      return true;
    }
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    $profile = $this->Substitute('start-profile');
    if ($this->owner === 'admin' &&
        $this->user->page === $this->Substitute('indieauth-page') &&
        $this->user->group === $this->Substitute('indieauth-group')) {
      $content = $this->Substitute('start-indieauth');
    }
    else if ($this->owner === $this->user->name &&
             ($this->GroupMember('admin', 'admin') ||
              $profile === 'ihaza' || $profile === 'unicyclic')) {
      $content = '<div class="start-show ui-state-highlight ui-corner-all">' .
        '<input type="checkbox" id="start-show-message" checked="checked">' .
        '<label for="start-show-message">Show this message and navigation ' .
          'tips when I log in.</label></div>' .
        $this->Substitute('start-content');
      if ($profile === 'ihaza') $content .= $this->CustomizeForm();
    }
    else {
      return false;
    }
    // This session variable is checked by init.php
    $_SESSION['start'] = true;
    // Wrap the content to go in a dialog, otherwise placing the whole module
    // in a dialog that gets added to the page messes with the layout editor.
    return '<div id="start-content">' . $content . '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'Tooltip') {
      return $this->Tooltip();
    }
    if ($fn === 'Settings') {
      return $this->Settings();
    }
    if ($fn === 'ChangeTheme') {
      return $this->ChangeTheme();
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.start.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.start.js', false);

    $us_content = '<p>Thanks for creating an account on ' .
        $this->user->config->ServerName() . '!</p>' .
      '<p>When you uncheck the option above, you can always bring back this ' .
        'dialog and the navigation tips on each page by clicking the ' .
        'account button and selecting <b>Help</b> from the menu.</p>' .
      '<p>You will see navigation tips on some pages. If at any time you\'re ' .
        'not sure what to do, please use the <a href="/contact">contact</a> ' .
        'page to ask questions.</p>';

    $us_indieauth = '<p>Thanks for logging in at ' .
        $this->user->config->ServerName() . '!</p>' .
      '<p>If you need any help please use the <a href="/contact">contact</a> ' .
        'page to ask questions.</p>';

    $mysqli = connect_db();
    $template = ['"start-content","","' .
                   $mysqli->escape_string($us_content) . '"',
                 '"start-indieauth","","' .
                   $mysqli->escape_string($us_indieauth) . '"',
                 '"start-login-help","","<h4>Create your account</h4>"',
                 '"start-login-customize","",' .
                   '"<h4>Customize your website</h4>"'];
    $mysqli->close();

    $this->AddTemplate($template);
    $description = ['start-login-help' => 'Help information to display ' .
                      'above a form that allows a new user to set their ' .
                      'username and password.',
                    'start-login-customize' => 'Help information to display ' .
                      'above a color picker that allows users to pick a ' .
                      'basic theme for their website.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"",".start","display","none"',
                   '"",".start-show","padding","5px"',
                   '"","#start-show-message","float","none"',
                   '"","#start-customize-form label","width","9em"',
                   '"","#start-customize-login-submit","margin-left","9.3em"',
                   '"","#start-theme-picker a.theme","display","inline-block"',
                   '"","#start-theme-picker a.theme","width","80px"',
                   '"","#start-theme-picker a.theme","height","50px"',
                   '"","#start-theme-picker a.theme","margin","10px"',
                   '"","#start-theme-picker a.theme","border-radius","3px"',
                   '"","#start-theme-picker a.theme","border",' .
                     '"1px solid #cccccc"',
                   '"","#start-theme-picker a.theme:hover","border",' .
                     '"1px solid #555555"',
                   '"","#start-theme-picker .blue.theme","background-color",' .
                     '"#bdfffd"',
                   '"","#start-theme-picker .purple.theme",' .
                     '"background-color","#9583ff"',
                   '"","#start-theme-picker .orange.theme",' .
                     '"background-color","#ff9b65"',
                   '"","#start-theme-picker .red.theme","background-color",' .
                     '"#ff4f7d"',
                   '"","#start-theme-picker .green.theme","background-color",' .
                     '"#84ff9e"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    // This allows users logged in with indieauth to view the Start module.
    if ($this->owner === 'admin' &&
        $this->user->page === $this->Substitute('indieauth-page') &&
        $this->user->group === $this->Substitute('indieauth-group')) {
      return 'outside';
    }
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    unset($_SESSION['start']);
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.start.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function ChangeTheme() {
    if (!$this->user->canEditSite) {
      return ['error' => 'You don\'t have permission to change the theme.'];
    }
    if ($this->Substitute('account-single-user') !== 'true') {
      return ['error' => 'This is not a single user site.'];
    }

    $blue = '#009590';
    $purple = '#4f3eb3';
    $orange = '#ef651b';
    $red = '#f02a5e';
    $green = '#16b137';
    $color = '';
    $us_theme = isset($_POST['theme']) ? $_POST['theme'] : '';
    if ($us_theme === 'blue theme') $color = $blue;
    else if ($us_theme === 'purple theme') $color = $purple;
    else if ($us_theme === 'orange theme') $color = $orange;
    else if ($us_theme === 'red theme') $color = $red;
    else if ($us_theme === 'green theme') $color = $green;
    else {
      $random = [$blue, $purple, $orange, $red, $green];
      $color = $random[mt_rand(0, 4)];
    }
    // Find the id of the login module to create a selector.
    $id = 0;
    $mysqli = connect_db();
    $query = 'SELECT box_id FROM modules WHERE label = "login" AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($modules = $mysqli_result->fetch_assoc()) {
        $id = (int)$modules['box_id'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Start->ChangeTheme 1: ' . $mysqli->error);
    }
    if ($id !== 0) {
      $selector = '#dobrado-' . $id;
      $query = 'INSERT INTO box_style VALUES ("' . $this->owner . '", "", ' .
        '"' . $selector  . ' a", "color", "' . $color . '") ON DUPLICATE KEY ' .
        'UPDATE value = "' . $color . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Start->ChangeTheme 2: ' . $mysqli->error);
      }
      write_box_style($this->owner, '../style.css');
    }
    // Also update colors in site style.
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '"input[type=text]:focus", "border-color", "' . $color . '") ' .
      'ON DUPLICATE KEY UPDATE value = "' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 3: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '"input[type=password]:focus", "border-color", "' . $color . '") ' .
      'ON DUPLICATE KEY UPDATE value = "' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 4: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '"textarea:focus", "border-color", "' . $color . '") ' .
      'ON DUPLICATE KEY UPDATE value = "' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 5: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '"a", "color", "' . $color . '") ' .
      'ON DUPLICATE KEY UPDATE value = "' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 6: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".detail a", "color", "' . $color . '") ON DUPLICATE KEY UPDATE ' .
      'value = "' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 7: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".detail img", "border", "3px solid ' . $color. '") ON DUPLICATE KEY ' .
      'UPDATE value = "3px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 8: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".detail indie-action", "border", "3px solid ' . $color. '") ' .
      'ON DUPLICATE KEY UPDATE value = "3px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 9: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".main > .middle", "border", "3px solid ' . $color. '") ' .
      'ON DUPLICATE KEY UPDATE value = "3px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 10: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".reader-group", "border-bottom", "3px solid ' . $color. '") ' .
      'ON DUPLICATE KEY UPDATE value = "3px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 11: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".reader-actions a", "color", "' . $color. '") ' .
      'ON DUPLICATE KEY UPDATE value = "' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 12: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".reader-show-hidden-wrapper", "border-bottom", ' .
      '"3px solid ' . $color. '") ON DUPLICATE KEY UPDATE ' .
      'value = "3px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 13: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".reader a.read-more", "text-decoration", ' .
      '"underline ' . $color. '") ON DUPLICATE KEY UPDATE ' .
      'value = "underline ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 14: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".reader-actions indie-action a.highlight", "border", ' .
      '"1px solid ' . $color. '") ON DUPLICATE KEY UPDATE ' .
      'value = "1px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 15: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".reader-web-actions indie-action a.highlight", "border", ' .
      '"1px solid ' . $color. '") ON DUPLICATE KEY UPDATE ' .
      'value = "1px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 16: ' . $mysqli->error);
    }
    $query = 'INSERT INTO site_style VALUES ("' . $this->owner . '", "", ' .
      '".post-web-actions indie-action a.highlight", "border", ' .
      '"1px solid ' . $color. '") ON DUPLICATE KEY UPDATE ' .
      'value = "1px solid ' . $color . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->ChangeTheme 17: ' . $mysqli->error);
    }
    $mysqli->close();
    write_site_style();
    return ['done' => true];
  }

  private function CustomizeForm() {
    $login_form = '';
    if (isset($this->user->settings['account']['username'])) {
      $login_form = '<p>Now that you\'ve set a username and password you can ' .
        'update your account details via the account menu.</p><p>Other ' .
        'settings for your site can be found by clicking the <b>tools</b> ' .
        'button in the control bar, and then the <b>site</b> button.</p>';
    }
    else {
      $login_form = $this->Substitute('start-login-help') .
        '<form id="start-customize-form" autocomplete="off">' .
          '<div class="form-spacing">' .
            '<label for="start-customize-username">Username:</label>' .
            '<input id="start-customize-username" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="start-customize-password">Password:</label>' .
            '<input id="start-customize-password" type="password" ' .
              'maxlength="200">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="start-customize-repeat-password">Repeat password:' .
            '</label>' .
            '<input id="start-customize-repeat-password" type="password" ' .
              'maxlength="200">' .
          '</div>' .
          '<button id="start-customize-login-submit">submit</button> ' .
          '<span id="start-customize-login-info"></span>' .
        '</form>';
    }
    return $login_form . $this->Substitute('start-login-customize') .
      '<p id="start-theme-picker">' .
        '<a href="#" class="blue theme"></a>' .
        '<a href="#" class="purple theme"></a>' .
        '<a href="#" class="orange theme"></a>' .
        '<a href="#" class="red theme"></a>' .
        '<a href="#" class="green theme"></a></p>';
  }

  private function Ihaza() {
    if ($this->user->page === 'index') {
      // Show a tool tip on their home page.
      return ['selector' => '#page-select-button',
              'content' => 'Select reader here to add people to follow!',
              'arrow' => '200px'];
    }
    // Show a tooltip once the user has navigated to their reader page.
    if ($this->user->page === 'reader') {
      return ['selector' => '.writer-edit-settings',
              'content' => 'The edit button is used to manage your feeds.' .
                'You are already subscribed to your own posts, they are ' .
                'currently saved to your blog page.',
              'arrow' => '450px'];
    }
    // Show a tooltip once the user has navigated to their blog page.
    if ($this->user->name === $this->owner && $this->user->page === 'blog') {
      return ['selector' => '.writer-options-add',
              'content' => 'Your posts can be written either here or on your ' .
                'reader page (they will be displayed on both). Try clicking ' .
                'the add button to add a title or a different author to a ' .
                'post.'];
    }
  }

  private function Lettuceshare() {
    // First set of tooltips are for when a user is on their home page.
    if ($this->user->name === $this->owner && $this->user->page === 'index') {
      // The first thing they should do is visit the stock page.
      if (can_view_page('admin/stock') || can_edit_page('admin/stock')) {
        return ['selector' => '#page-select-button',
                'content' => 'To update your product list, choose ' .
                  'stock from the menu to go to the stock page.',
                'arrow' => '400px'];
      }
      // Otherwise let admin users know how to create accounts.
      if ($this->GroupMember('admin', 'admin')) {
        return ['selector' => '.account-button',
                'content' => 'To create new accounts, click the ' .
                  'account button and select Manage Accounts.',
                'arrow' => '5px'];
      }
    }
    // Show some tooltips once the user has navigated to the stock page. 
    if ($this->owner === 'admin' && $this->user->page === 'stock') {
      // First check if they have created any suppliers.
      $stock = new Module($this->user, $this->owner, 'stock');
      if ($stock->IsInstalled()) {
        if (count($stock->Factory('AllSuppliers')) === 0) {
          return ['selector' => '#stock-open-product-form',
                  'content' => 'To add products, you first need to create an ' .
                    'account for each supplier. To do this open the product ' .
                    'form and click Add Supplier.',
                  'arrow' => '100px'];
        }
        else {
          return ['selector' => 'label[for=stock-show-import]',
                  'content' => 'You can import a product list by clicking ' .
                    'Import and selecting a file from your computer, or by ' .
                    'opening the product form to the right and creating ' .
                    'products manually. Click the help button for more ' .
                    'information.'];
        }
      }
    }
    if ($this->owner === 'admin' && $this->user->page === 'payments') {
      return ['selector' => '#payment-import-input',
              'content' => 'You can add payments by filling in this form, or ' .
                'click the import button and select a file from your ' .
                'computer. If you\'re not sure if your bank\'s file format ' .
                'is supported by Lettuceshare, please email: ' .
                'info@lettuceshare.org.'];
    }
  }

  private function LoginSubmit() {
    if (!$this->user->canEditSite) {
      return ['error' => 'You don\'t have permission to edit login details.'];
    }
    if ($this->Substitute('account-single-user') !== 'true') {
      return ['error' => 'This is not a single user site.'];
    }

    $mysqli = connect_db();
    $username = $mysqli->escape_string(strtolower(trim($_POST['username'])));
    $query = 'INSERT INTO settings VALUES ("admin", "account", "username", ' .
      '"' . $username . '") ON DUPLICATE KEY UPDATE value = "' . $username .'"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->LoginSubmit 1: ' . $mysqli->error);
    }
    $password_hash = $mysqli->escape_string(password_hash($_POST['password'],
                                                          PASSWORD_DEFAULT));
    $query = 'UPDATE users SET password = "' . $password_hash . '" WHERE ' .
      'user = "admin"';
    if (!$mysqli->query($query)) {
      $this->Log('Start->LoginSubmit 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function PageCreated($page) {
    $created = true;
    $mysqli = connect_db();
    $query = 'SELECT box_id FROM modules WHERE user = "' . $this->owner . '" ' .
      'AND page = "' . $page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      $created = $mysqli_result->num_rows !== 0;
      $mysqli_result->close();
    }
    else {
      $this->Log('Start->PageCreated: ' . $mysqli->error);
    }
    $mysqli->close();
    return $created;
  }

  private function Settings() {
    $profile = $this->Substitute('start-profile');
    if ($profile === 'ihaza' || $profile === 'unicyclic') {
      return ['control' => ['displayMessageButton' => 'hidden',
                            'displayToolsButton' => 'display'],
              'reader' => ['defaultChannel' => 'default',
                           'showChannels' => 'yes']];
    }
    if ($profile === 'lettuceshare' || $profile === 'hidden') {
      return ['control' => ['displayMessageButton' => 'hidden',
                            'displayToolsButton' => 'hidden'],
              'stock' => ['displayWideGridButton' => 'hidden'],
              'members' => ['displayWideGridButton' => 'hidden']];
    }
    return [];
  }

  private function Tooltip() {
    // Since tooltips are very design dependent, at least allow different
    // profiles to be added.
    $profile = $this->Substitute('start-profile');
    if ($profile === 'ihaza') return $this->Ihaza();
    if ($profile === 'lettuceshare') return $this->Lettuceshare();
    if ($profile === 'unicyclic') return $this->Unicyclic();
  }

  private function Unicyclic() {
    if ($this->user->name === $this->owner && $this->user->page === 'index') {
      // Use this profile to create some default pages for a new user if they
      // don't already exist. The blog page is where new posts are published.
      if (!$this->PageCreated('blog')) {
        copy_page('default_blog', 'admin', 'blog', $this->owner);
      }
      // By default likes and replies are not published to the users blog.
      // Instead separate pages are created to list each of these.
      if (!$this->PageCreated('likes')) {
        copy_page('default_likes', 'admin', 'likes', $this->owner);
      }
      if (!$this->PageCreated('replies')) {
        copy_page('default_replies', 'admin', 'replies', $this->owner);
      }
      // The default_post page is used as a template for permalink pages, the
      // value is set in the user's config settings.
      if (!$this->PageCreated('default_post')) {
        copy_page('default_post', 'admin', 'default_post', $this->owner);
      }
      // The default_pager page is used as a template for 'pager' pages, the
      // value is set in the 'pager-default' template.
      if (!$this->PageCreated('default_pager')) {
        copy_page('default_pager', 'admin', 'default_pager', $this->owner);
      }
      // The reader page contains both reader and writer modules, so to ensure
      // that posts on this page are visible on their own, designate that they
      // get posted to the blog page. Then also subscribe to the blog page so
      // that they show up on the reader page too.
      if (!$this->PageCreated('reader')) {
        copy_page('default_reader', 'admin', 'reader', $this->owner);
        $writer = new Module($this->user, $this->owner, 'writer');
        if ($writer->IsInstalled()) {
          $writer->Factory('Designate', ['post', 'reader', 'blog']);
          $writer->Factory('Designate', ['share', 'reader', 'blog']);
          $writer->Factory('Designate', ['like', 'reader', 'likes']);
          $writer->Factory('Designate', ['reply', 'reader', 'replies']);
          // Also need to redirect likes and replies on the blog page.
          $writer->Factory('Designate', ['like', 'blog', 'likes']);
          $writer->Factory('Designate', ['reply', 'blog', 'replies']);
        }
        $reader = new Module($this->user, $this->owner, 'reader');
        if ($reader->IsInstalled()) {
          $id = 0;
          $mysqli = connect_db();
          $query = 'SELECT box_id FROM modules WHERE ' .
            'user = "' . $this->owner . '" AND label = "reader" AND ' .
            'page="reader"';
          if ($mysqli_result = $mysqli->query($query)) {
            if ($modules = $mysqli_result->fetch_assoc()) {
              $id = (int)$modules['box_id'];
            }
            $mysqli_result->close();
          }
          else {
            $this->Log('Start->Unicyclic: ' . $mysqli->error);
          }
          $mysqli->close();
          if ($id !== 0) {
            $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
            $server = $this->user->config->ServerName();
            $feed = $scheme . $server .
              $this->Url('', 'blog', $this->user->name);
            $reader->Factory('AddFeed', [$id, $feed, true]);
            // Also add a default follow for this server if set.
            $follow_url = $this->Substitute('start-follow-url');
            if ($follow_url !== '') {
              $reader->Factory('AddFeed', [$id, $follow_url, true]);
            }
          }
        }
      }

      // Show a tool tip on their home page.
      return ['selector' => '#page-input',
              'content' => 'Type reader here to get to your page where ' .
                'you can manage feeds, and also write content.',
              'arrow' => '400px'];
    }
    // Show a tooltip once the user has navigated to their reader page. 
    if ($this->user->page === 'reader') {
      if ($this->user->name === $this->owner) {
        return ['selector' => '.writer-edit-settings',
                'content' => 'Click the edit button to manage your feeds.' .
                  'You are already subscribed to your own posts, they are ' .
                  'currently saved to your blog page.',
                'arrow' => '450px'];
      }
      else {
        // This can be seen by users logged in via indieauth.
        return ['selector' => '.writer-edit-settings',
                'content' => 'Click the edit button to manage your feeds.' .
                  'You might already be subscribed to your own, if a feed ' .
                  'was found on your site when you logged in.',
                'arrow' => '450px'];
      }
    }
    // Show a tooltip once the user has navigated to their blog page. 
    if ($this->user->name === $this->owner && $this->user->page === 'blog') {
      return ['selector' => '.writer-options-add',
              'content' => 'Your posts can be written either here or on your ' .
                'reader page (they will be displayed on both). Try clicking ' .
                'the add button to add a title or a different author to a ' .
                'post.'];
    }
  }

}
