<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Subdomaincheck extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'lookup') return $this->Lookup();
    if ($us_action === 'submit') return $this->Submit();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return
      '<form id="subdomaincheck-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="subdomaincheck-input">Subdomain:</label>' .
          '<input id="subdomaincheck-input" type="text" maxlength="100">' .
        '</div>' .
        '<button id="subdomaincheck-button">Check</button>' .
        '<div id="subdomaincheck-info"></div>' .
      '</form>' .
      '<form id="subdomaincheck-email-form" class="hidden" autocomplete="off">'.
        'Please provide an email address to create your website.<br>' .
        'You will be emailed a login code when it\'s ready!' .
        '<div class="form-spacing">' .
          '<label for="subdomaincheck-email">Email Address:</label>' .
          '<input id="subdomaincheck-email" type="text" maxlength="100">' .
        '</div>' .
        '<button id="subdomaincheck-email-submit">Submit</button>' .
        '<div id="subdomaincheck-email-info"></div>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $description = ['subdomaincheck-domain' => 'The base domain used for ' .
                      'creating subdomains.',
                    'subdomaincheck-hosting' => 'The body of an email sent ' .
                      'to an unconfirmed user to let them know their website ' .
                      'is in a queue to be created. Substitutes: !domain and ' .
                      '!email.'];
    $this->AddTemplateDescription($description);

    $site_style = [
      '"","#subdomaincheck-form","background-color","#eeeeee"',
      '"","#subdomaincheck-form","border","1px solid #aaaaaa"',
      '"","#subdomaincheck-form","border-radius","2px"',
      '"","#subdomaincheck-form","padding","20px"',
      '"","#subdomaincheck-form","margin","20px"',
      '"","#subdomaincheck-button","margin-left","5.6em"',
      '"","#subdomaincheck-info","margin-top","20px"',
      '"","#subdomaincheck-email-form","background-color","#eeeeee"',
      '"","#subdomaincheck-email-form","border","1px solid #aaaaaa"',
      '"","#subdomaincheck-email-form","border-radius","2px"',
      '"","#subdomaincheck-email-form","padding","20px"',
      '"","#subdomaincheck-email-form","margin","20px"',
      '"","#subdomaincheck-email-submit","margin-left","7em"',
      '"","#subdomaincheck-email-info","margin-top","20px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript, ie:
    $this->AppendScript($path, 'dobrado.subdomaincheck.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function Lookup() {
    $_SESSION['subdomaincheck-domain'] = NULL;

    $subdomain = strtolower(preg_replace('/[[:^alnum:]]/', '',
                                         $_POST['subdomain']));
    if ($subdomain === '') {
      return ['info' => 'Please only use numbers and letters for the ' .
                        'subdomain.'];
    }

    $domain = $subdomain . '.' . $this->Substitute('subdomaincheck-domain');
    $domaincheck = new Module($this->user, $this->owner, 'domaincheck');
    if (!$domaincheck->IsInstalled()) {
      return ['info' => 'Sorry cannot check subdomains. Module not installed.'];
    }

    if ($domaincheck->Factory('Registered', $domain)) {
      return ['info' => 'Sorry this subdomain is not available.'];
    }

    $_SESSION['subdomaincheck-domain'] = $domain;
    return ['info' => 'This subdomain is available!<br>' .
                      'Your website will be created at: ' . $domain,
            'email' => true];
  }

  private function Submit() {
    if (!isset($_SESSION['subdomaincheck-domain'])) {
      return ['info' => 'Domain name not set.'];
    }

    $us_domain = $_SESSION['subdomaincheck-domain'];
    $us_email = strtolower(trim($_POST['email']));
    if ($us_email === '') return ['info' => 'Email not provided.'];

    $domaincheck = new Module($this->user, $this->owner, 'domaincheck');
    if (!$domaincheck->IsInstalled()) {
      return ['info' => 'Sorry cannot add subdomain. Module not installed.'];
    }

    $domaincheck->Factory('AddDomain', [$us_domain, $us_email]);
    $sender = $this->Substitute('new-user-sender');
    $sender_name = $this->Substitute('new-user-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = $this->Substitute('new-user-sender-cc');
    $bcc = $this->Substitute('new-user-sender-bcc');

    $subject = 'Hosting requested';
    $us_content = $this->Substitute('subdomaincheck-hosting',
                                    ['/!domain/', '/!email/'],
                                    [$us_domain, $us_email]);
    $us_message = '<html><head><title>' . $subject . '</title></head><body>' .
      $us_content . '</body></html>';
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($bcc !== '') {
      $headers .= 'Bcc: ' . $bcc . "\r\n";
    }
    mail($us_email, $subject, wordwrap($us_message), $headers, '-f ' . $sender);
    return ['info' => 'Thanks! Your website will be set up soon. An email ' .
                      'has also been sent to the address provided.<br>' .
                      '(Please check your spam folder if you don\'t see it.)'];
  }

}
