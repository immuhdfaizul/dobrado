/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
//
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
//
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.domaincheck) {
  dobrado.domaincheck = {};
}
(function() {

  'use strict';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.domaincheck').length === 0) {
      return;
    }

    $('#domaincheck-button').button().click(check);
    $('#domaincheck-email-submit').button().click(submit);
    $('#customer-detail-submit').button().click(customerDetail);

    if ($('.domaincheck-payment-message').html() !== '') {
      $('.domaincheck-payment-message').dialog({
        show: true,
        modal: true,
        width: 500,
        height: 280,
        position: { my: 'top', at: 'top+50', of: window },
        title: 'Payment done',
        create: dobrado.fixedDialog });
      $('.domaincheck-payment-message button').button().click(function() {
        $('.domaincheck-payment-message').dialog('close');
      });
    }

    $('#domaincheck-checkout-dialog').dialog({
      show: true,
      autoOpen: false,
      modal: true,
      width: 550,
      height: 400,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Purchase your domain name',
      create: dobrado.fixedDialog });
  });

  function check() {

    function purchaseDomain() {
      $('#domaincheck-checkout-message').html('Please enter your contact ' +
                                              'details:');
      $('#domaincheck-customer-details-form').show();
      $('#domaincheck-checkout-dialog').dialog('open');
      return false;
    }

    function showDomain(event, ui) {
      var domainSelect = ui.item.value;
      $('#domaincheck-renewal-info').hide();

      if (domainList[domainSelect]) {
        $('#domaincheck-domain').text(domainSelect);
        if (domainList[domainSelect].price) {
          let price = domainList[domainSelect].price;
          $('#domaincheck-price').text(price);
          if (domainList[domainSelect].renewal) {
            let renewal = domainList[domainSelect].renewal;
            $('#domaincheck-renewal').text(renewal);
            if (price != renewal) {
              $('#domaincheck-renewal-info').show();
            }
          }
        }
      }
    }

    var domainList = [];
    var domainInput = $('#domaincheck-input').val();
    if (domainInput === '') {
      return false;
    }

    $('#domaincheck-info').html('checking...');
    $.post('/php/request.php',
           { request: 'domaincheck', action: 'lookup', domain: domainInput,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'domaincheck lookup')) {
          return;
        }
        let check = JSON.parse(response);
        $('#domaincheck-info').html(check.info);
        if (check.domains) {
          domainList = check.domains;
          $('#domaincheck-select').selectmenu({ change: showDomain });
        }
        $('#domaincheck-purchase').button().click(purchaseDomain);
        $('#domaincheck-email-form').hide();
        if (check.email) {
          $('#domaincheck-email-form').show();
        }
      });
    return false;
  }

  function customerDetail() {
    if ($('#customer-detail-name').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your name.');
      return false;
    }
    if ($('#customer-detail-email').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your email.');
      return false;
    }
    if ($('#customer-detail-address').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your address.');
      return false;
    }
    if ($('#customer-detail-postcode').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your postcode.');
      return false;
    }
    if ($('#customer-detail-city').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your city.');
      return false;
    }
    if ($('#customer-detail-state').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your state.');
      return false;
    }
    if ($('#customer-detail-country').val() === '') {
      $('#domaincheck-checkout-info').html('Please enter your country.');
      return false;
    }
    $('#domaincheck-checkout-info').html('Processing... please wait.');

    $.post('/php/request.php',
           { request: 'domaincheck', action: 'checkout',
             name: $('#customer-detail-name').val(),
             email: $('#customer-detail-email').val(),
             phone: $('#customer-detail-phone').val(),
             address: $('#customer-detail-address').val(),
             city: $('#customer-detail-city').val(),
             state: $('#customer-detail-state').val(),
             country: $('#customer-detail-country').val(),
             postcode: $('#customer-detail-postcode').val(),
             domain: $('#domaincheck-domain').text(),
             price: $('#domaincheck-price').text(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'customerDetail')) {
          return;
        }
        var checkout = JSON.parse(response);
        $('#domaincheck-checkout-info').html('');
        $('#domaincheck-customer-details-form').hide();
        $('#domaincheck-checkout-message').html(checkout.content);
        // The cart module provides the form and submit button.
        $('#cart-payment').button();
      });
    return false;
  }

  function submit() {
    var email = $('#domaincheck-email').val();
    if (email === '') {
      return false;
    }

    $.post('/php/request.php',
           { request: 'domaincheck', action: 'submit', email: email,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'domaincheck submit')) {
          return;
        }
        let check = JSON.parse(response);
        $('#domaincheck-email-info').html(check.info);
      });
    return false;
  }

}());
