<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Pager extends Base {

  public function Add($id) {
    $limit = (int)$this->Substitute('pager-limit');
    $mysqli = connect_db();
    $query = 'INSERT INTO pager VALUES ("'.$this->owner.'", '.
      '"'.$this->user->page.'", '.$limit.')';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->Add: '.$mysqli->error);
    }
    $mysqli->close();
    // Check if there are existing posts on this page that need to be split up.
    // The Move function only moves one post at a time, so keep calling it
    // until it reaches the posts per page limit (which will return false).
    do {
      $moved = $this->Move($this->user->page, 2, true, $limit);
    } while ($moved);
  }

  public function Callback() {
    if (!$this->user->canEditPage) {
      return ['error' => 'You don\'t have permission to edit limit.'];
    }

    $old_limit = $this->Limit();
    $page = $this->user->page;
    // Ignore the pager suffix when updating the posts per page limit, Move
    // also expects the canonical version of the page.
    if (preg_match('/^(.*)-p[0-9]+$/', $page, $matches)) {
      $page = $matches[1];
    }
    $mysqli = connect_db();
    $limit = isset($_POST['limit']) ? (int)$_POST['limit'] : 0;
    $query = 'UPDATE pager SET post_limit = '.$limit.' WHERE '.
      'user = "'.$this->owner.'" AND page = "'.$page.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->Callback: '.$mysqli->error);
    }
    $mysqli->close();

    // If post_limit is greater than old_limit or set to zero, need to decrease
    // the number of pages by moving posts back until the new limit is reached,
    // or all posts are back on one page when post_limit is set to zero.
    if ($limit === 0 || ($old_limit !== 0 && $limit > $old_limit)) {
      do {
        $moved = $this->Move($page, 2, false, $limit);
      } while ($moved);
    }
    // If post_limit is less than old_limit, increase the number of pages by
    // calling Move until new limit is reached.
    else if ($old_limit === 0 || $limit < $old_limit) {
      do {
        $moved = $this->Move($page, 2, true, $limit);
      } while ($moved);
    }
    return ['done' => true];
  }

  public function CanAdd($page) {
    return !$this->AlreadyOnPage('pager', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    $limit = $this->Limit();
    if ($this->user->canEditPage) {
      $content .= '<button class="edit-limit">edit</button>'.
        '<span class="pager-limit hidden">'.
          '<label for="pager-limit-input">Posts per page:</label>'.
          '<input id="pager-limit-input" type="text" maxlength="5" '.
            'value = "'.$limit.'">'.
          '<button class="save">save</button>'.
        '</span>';
    }
    if ($limit === 0) return $content;

    // Find the current page with pager suffix and current pager number.
    $page = $this->user->page;
    $count = 0;
    if (preg_match('/^(.*)-p([0-9]+)$/', $page, $matches)) {
      $page = $matches[1];
      $count = (int)$matches[2];
    }
    $pager_previous = $count === 2 ? $page : $page.'-p'.($count - 1);
    $pager_next = $count === 0 ? $page.'-p2' : $page.'-p'.($count + 1);
    // Check if pager-next should be displayed.
    $next_count = 0;
    $mysqli = connect_db();
    $query = 'SELECT box_id FROM modules WHERE user = "'.$this->owner.'" '.
      'AND page = "'.$pager_next.'" AND label = "post" AND deleted = 0';
    if ($result = $mysqli->query($query)) {
      $next_count = $result->num_rows;
      $result->close();
    }
    else {
      $this->Log('Pager->Content: '.$mysqli->error);
    }
    $mysqli->close();

    $content .= '<div class="pager-navigation">';
    if ($count === 0) {
      if ($next_count !== 0) {
        $content .= '<a href="'.$this->Url('', $pager_next).'" '.
          'class="pager-next">Page 2 &gt;</a>';
      }
    }
    else {
      $content .= '<a href="'.$this->Url('', $pager_previous).'" '.
        'class="pager-previous">&lt; Page '.($count - 1).'</a> ';
      if ($next_count !== 0) {
        $content .= '<a href="'.$this->Url('', $pager_next).'" '.
          'class="pager-next">Page '.($count + 1).' &gt;</a>';
      }
    }
    $content .= '</div>';
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    // Don't add to the pager table when copying to a pager page. 
    if (preg_match('/^.*-p[0-9]+$/', $new_page)) return;

    $old_page = '';
    $mysqli = connect_db();
    $query = 'SELECT page FROM modules WHERE user = "'.$old_owner.'" AND '.
      'box_id = '.$old_id;
    if ($result = $mysqli->query($query)) {
      if ($modules = $result->fetch_assoc()) {
        $old_page = $modules['page'];
      }
      $result->close();
    }
    else {
      $this->Log('Pager->Copy 1: '.$mysqli->error);
    }
    $post_limit = (int)$this->Substitute('pager-limit');
    $query = 'SELECT post_limit FROM pager WHERE user = "'.$old_owner.'" AND '.
      'page = "'.$old_page.'"';
    if ($result = $mysqli->query($query)) {
      if ($pager = $result->fetch_assoc()) {
        $post_limit = $pager['post_limit'];
      }
      $result->close();
    }
    else {
      $this->Log('Pager->Copy 2: '.$mysqli->error);
    }
    $query = 'INSERT INTO pager VALUES ("'.$this->owner.'", "'.$new_page.'", '.
      $post_limit.')';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->Copy 3: '.$mysqli->error);
    }
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'Move' && $count === 3) {
        $page = $p[0];
        $count = $p[1];
        $increase = $p[2];
        return $this->Move($page, $count, $increase);
      }
      return;
    }
    if ($fn === 'Move') {
      return $this->Move($p);
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Need to call AppendScript here if module uses javascript.
    $this->AppendScript($path, "dobrado.pager.js");
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS pager ('.
      'user VARCHAR(50) NOT NULL,'.
      'page VARCHAR(200),'.
      'post_limit INT UNSIGNED,'.
      'PRIMARY KEY(user, page)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->Install: '.$mysqli->error);
    }
    $mysqli->close();

    $template = ['"pager-limit","","10"'];
    $this->AddTemplate($template);
    $site_style = ['"",".pager","min-height","40px"',
                   '"",".pager .edit-limit","float","left"',
                   '"",".pager-navigation a","text-decoration","none"',
                   '"",".pager-next","float","right"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    // Check if $id is on the original page (ie no suffix) and remove from
    // the pager table.
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript.
    $this->AppendScript($path, "dobrado.pager.js");
  }

  // Private functions below here ////////////////////////////////////////////

  private function Limit() {
    $limit = 0;
    $page = $this->user->page;
    // Ignore the pager suffix when looking up the posts per page limit.
    if (preg_match('/^(.*)-p[0-9]+$/', $page, $matches)) {
      $page = $matches[1];
    }
    $mysqli = connect_db();
    $query = 'SELECT post_limit FROM pager WHERE user = "'.$this->owner.'" '.
      'AND page = "'.$page.'"';
    if ($result = $mysqli->query($query)) {
      if ($pager = $result->fetch_assoc()) {
        $limit = (int)$pager['post_limit'];
      }
      $result->close();
    }
    else {
      $this->Log('Pager->Limit: '.$mysqli->error);
    }
    $mysqli->close();
    return $limit;
  }

  private function Move($page, $count = 2, $increase = true, $limit = false) {
    if ($limit === false) $limit = $this->Limit();
    $old_page = $count === 2 ? $page : $page.'-p'.($count - 1);
    $new_page = $page.'-p'.$count;

    $mysqli = connect_db();
    // Count the current number of posts on old_page when limit is non-zero.
    if ($limit !== 0) {
      $old_count = 0;
      $query = 'SELECT box_id FROM modules WHERE user = "'.$this->owner.'" '.
        'AND page = "'.$old_page.'" AND label = "post" AND deleted = 0';
      if ($result = $mysqli->query($query)) {
        $old_count = $result->num_rows;
        $result->close();
      }
      else {
        $this->Log('Pager->Move 1: '.$mysqli->error);
      }
      // Stop when increasing the number of posts and not yet over the limit,
      // or decreasing the number of posts and the limit has been reached.
      if ($old_count === $limit || ($increase && $old_count < $limit)) {
        $mysqli->close();
        // Stop if old_count is zero.
        if ($old_count === 0) return false;

        // Otherwise need to check subsequent pages too.
        return $this->Move($page, $count + 1, $increase, $limit);
      }
    }
    else if ($increase) {
      // When the limit is zero but increase is true, can't continue.
      $mysqli->close();
      return false;
    }

    // The number of posts on new_page is used by both increase and decrease.
    $new_count = 0;
    $query = 'SELECT box_id FROM modules WHERE user = "'.$this->owner.'" AND '.
      'page = "'.$new_page.'" AND label = "post" AND deleted = 0';
    if ($result = $mysqli->query($query)) {
      $new_count = $result->num_rows;
      $result->close();
    }
    else {
      $this->Log('Pager->Move 2: '.$mysqli->error);
    }
    if ($new_count === 0) {
      if ($increase) {
        // When increase is true and new_count is zero, need to copy the default
        // page before moving a post.
        $current_page = $this->Substitute('pager-default');
        if ($current_page === '') {
          $this->Log('Pager->Move 3: pager-default is not set.');
          $mysqli->close();
          return false;
        }
        copy_page($current_page, $this->owner, $new_page, $this->owner);
      }
      else {
        // When increase is false and new_count is zero, this is a stopping
        // condition for moving posts back to the previous page.
        $query = 'UPDATE modules SET deleted = 1 WHERE '.
          'user = "'.$this->owner.'" AND page = "'.$new_page.'"';
        if (!$mysqli->query($query)) {
          $this->Log('Pager->Move 3:'.$mysqli->error);
        }
        $mysqli->close();
        return false;
      }
    }
    // When increase is true, find the oldest post on old_page to move forward.
    // (This will be the post with the smallest id). Otherwise find the newest
    // post (biggest id) on new_page to move backwards.
    if ($increase) {
      $query = 'SELECT MIN(box_id) AS box_id FROM modules WHERE '.
        'user = "'.$this->owner.'" AND page = "'.$old_page.'" AND '.
        'label = "post" AND deleted = 0';
    }
    else {
      $query = 'SELECT MAX(box_id) AS box_id FROM modules WHERE '.
        'user = "'.$this->owner.'" AND page = "'.$new_page.'" AND '.
        'label = "post" AND deleted = 0';
    }
    $id = 0;
    if ($result = $mysqli->query($query)) {
      if ($modules = $result->fetch_assoc()) {
        $id = (int)$modules['box_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Pager->Move 4: '.$mysqli->error);
    }
    $mysqli->close();
    if ($id === 0) return false;

    // The post is moved in UpdateModules, and the box_order of modules
    // affected by the move are also updated.
    $this->UpdateModules($id, $old_page, $new_page, $increase);
    // Call Move again for new_page if new_count has reached the pager limit,
    // or when reducing the number of pages per post.
    if (!$increase || $new_count >= $limit) {
      $this->Move($page, $count + 1, $increase, $limit);
    }
    // Return true when a post was moved on the original page.
    return true;
  }

  private function UpdateModules($id, $old_page, $new_page, $increase) {
    $box_order = 0;
    $mysqli = connect_db();
    // When increase is true, find a box_order for the moved post so that it's
    // the first post on the new page. Otherwise the moved post becomes the last
    // post on the old page.
    $source = $increase ? $old_page : $new_page;
    $destination = $increase ? $new_page : $old_page;
    $limit = $increase ? 'MIN' : 'MAX';
    $query = 'SELECT '.$limit.'(box_order) AS box_order FROM modules WHERE '.
      'user = "'.$this->owner.'" AND page = "'.$destination.'" AND '.
      'label = "post" AND deleted = 0';
    if ($result = $mysqli->query($query)) {
      if ($modules = $result->fetch_assoc()) {
        $box_order = (int)$modules['box_order'];
      }
      $result->close();
    }
    else {
      $this->Log('Pager->UpdateModules 1: '.$mysqli->error);
    }
    // When increase is true, the old box order is used to decrement the box
    // order of modules below it on the old page, otherwise it's used to
    // decrement the box order of modules below it on the new page.
    $old_box_order = 0;
    $query = 'SELECT box_order FROM modules WHERE user = "'.$this->owner.'" '.
      'AND page = "'.$source.'" AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($modules = $result->fetch_assoc()) {
        $old_box_order = (int)$modules['box_order'];
      }
      $result->close();
    }
    else {
      $this->Log('Pager->UpdateModules 2: '.$mysqli->error);
    }
    $query = 'UPDATE modules SET box_order = box_order - 1 WHERE '.
      'user = "'.$this->owner.'" AND page = "'.$source.'" AND '.
      'placement = "middle" AND box_order > '.$old_box_order.' AND deleted = 0';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->UpdateModules 3: '.$mysqli->error);
    }
    $query = 'UPDATE modules SET box_order = box_order + 1 WHERE '.
      'user = "'.$this->owner.'" AND page = "'.$destination.'" AND '.
      'placement = "middle" AND box_order > '.$box_order.' AND deleted = 0';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->UpdateModules 4: '.$mysqli->error);
    }
    if ($increase) {
      // When moving a post to a new page, also increment the first post.
      $query = 'UPDATE modules SET box_order = box_order + 1 WHERE '.
        'user = "'.$this->owner.'" AND page = "'.$destination.'" AND '.
        'placement = "middle" AND box_order = '.$box_order.' AND deleted = 0';
      if (!$mysqli->query($query)) {
        $this->Log('Pager->UpdateModules 5: '.$mysqli->error);
      }
    }
    else {
      // When moving a post back, need to increment the order number for the
      // post being moved.
      $box_order++;
    }
    $query = 'UPDATE modules SET page = "'.$destination.'", '.
      'box_order = '.$box_order.' WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND page = "'.$source.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Pager->UpdateModules 6:'.$mysqli->error);
    }
    $mysqli->close();
  }

}
