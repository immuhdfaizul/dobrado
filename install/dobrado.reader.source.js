/*global dobrado: true, CKEDITOR: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
//
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
//
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.reader) {
  dobrado.reader = {};
}
(function() {

  'use strict';

  // addImportedFeed is called for each feed in importList, but the list is
  // reset if the dialog is closed during the import process.
  var importList = [];
  var importCount = 0;
  // Store currentChannel rather than checking #page-select as the correct value
  // is not always available depending on how the page was loaded.
  var currentChannel = '';
  // The reader module manages it's own CKEDITOR for use with actions.
  var editor = null;
  // Keep track of the currently selected action.
  var currentAction = null;

  $(function() {
    if ($('.reader').length === 0) {
      return;
    }

    var tenMinutes = 600000;
    // Set the initial elapsed time to greater than the cut-off to force update.
    var elapsed = tenMinutes + 1;
    // Also update when the current url is different from the stored one.
    var pageChanged = true;

    $('.reader-edit-settings').button({
      icon: 'ui-icon-signal-diag',
      showLabel: false
    }).click(function() {
      // This can't be cleared on page load as it's used by the follow web
      // action, but it's also auto-filled by browsers from previous urls.
      $('#reader-add-input').val('');
      $('.reader-settings').toggle();
    });
    $('.reader-more').button().click(function() {
      $('.reader-more').button('option', 'label', 'Loading...');
      request('older', 'feed');
    });
    $('.reader-reset').button().click(reset);
    $('#reader-show-hidden').button().click(showHidden);
    $('.reader-discovered').dialog({
      show: true,
      autoOpen: false,
      width: 600,
      height: 300,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Discovered Feeds',
      close: function() { importList = []; importCount = 0; },
      create: dobrado.fixedDialog });
    $('.reader-channel-settings').dialog({
      show: true,
      autoOpen: false,
      width: 600,
      height: 300,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Channel List',
      create: dobrado.fixedDialog });
    feedSettings();

    // Retrieve existing feed content from localStorage if available.
    if (dobrado.localStorage) {
      if (localStorage.reader) {
        $('.reader-content').html(localStorage.reader);
        $('.reader-writer').show();
        $('.reader-more').show();
        $('.reader-reset').show();
        // If there is reader content, might not need to check the server
        // so get the last update time.
        if (localStorage.readerUpdate) {
          elapsed = new Date().getTime() - localStorage.readerUpdate;
        }
        if (localStorage.readerPage) {
          pageChanged = location.href !== localStorage.readerPage;
        }
        if (localStorage.readerCurrentChannel) {
          currentChannel = localStorage.readerCurrentChannel;
          $('#page-select').val(currentChannel);
        }
        else {
          currentChannel = $('#page-select').val();
          localStorage.readerCurrentChannel = currentChannel;
        }
      }
    }

    if (currentChannel === '') {
      currentChannel = $('#page-select').val();
    }
    if (pageChanged) {
      if (dobrado.localStorage) {
        localStorage.readerChannelInfo = '';
        localStorage.readerLists = '';
      }
      reset(true);
    }
    else if (elapsed > tenMinutes) {
      // In this case don't want to call reset if the page is scrolled, but
      // need to wait for content to load before checking scroll position.
      setTimeout(function() {
        if ($(window).scrollTop() < 200) {
          reset(true);
        }
        else {
          newFeedEvents();
          dobrado.notify('feed', dobrado.reader.update);
        }
      }, 2000);
    }
    else {
      // If the user hasn't subscribed to any feeds yet, automatically open
      // the reader settings.
      if ($('.reader-settings .feed-item').length <= 5) {
        $('.reader-settings').show();
      }
      newFeedEvents();
      dobrado.notify('feed', dobrado.reader.update);
    }
  });

  function channelSettings() {

    function addChannel() {
      var option = $('#reader-channel-add').val();
      if (option === '') {
        return;
      }

      $('#reader-channel-add').val('');
      setChannel(option, true);
    }

    function checkFeed() {
      var feed = $('#reader-channel-url').attr('href');
      dobrado.log('Checking for updates', 'info');
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'checkFeed', feed: feed,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'reader checkFeed')) {
            return;
          }
        });
    }

    function setChannel(option, add) {
      var feed = $('#reader-channel-url').attr('href');
      dobrado.log('Setting channel for feed', 'info');
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'setChannel', channel: option, feed: feed,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'reader setChannel')) {
            return;
          }
          var channel = JSON.parse(response);
          var status = channel.name === 'not set' ?
            'The channel for this feed is no longer set.' :
            'The channel for this feed has been updated to <b>' +
              channel.name + '</b>';
          $('#reader-channel-status').html(status);
          // Also update feed settings if provided, which is required when the
          // channel is changed for a feed list, otherwise just need to update
          // one channel button in the feed setting list.
          if (channel.settings) {
            $('.reader-settings').html(channel.settings);
            feedSettings();
          }
          else {
            $('.reader-feed-list .feed-url').each(function() {
              if ($(this).attr('href') === feed) {
                let channelButton =
                  $(this).parents('.feed-item').find('.reader-channel-button');
                channelButton.button('option', 'label', channel.name);
              }
            });
          }
          // If called from addChannel add option to reader-channel-select.
          if (add) {
            $('#reader-channel-select').append('<option selected="selected">' +
                                               channel.name + '</option>');
            $('#reader-channel-select').selectmenu('refresh');
            // The select is initially hidden if there are no channel options.
            $('#reader-channel-select').parent().show();
          }
          if (dobrado.localStorage) {
            let authorChannel = {};
            if (localStorage.readerAuthorChannel) {
              authorChannel = JSON.parse(localStorage.readerAuthorChannel);
            }
            authorChannel[channel.feed] = channel.name;
            localStorage.readerAuthorChannel = JSON.stringify(authorChannel);
          }
        });
    }

    function manualAdd() {
      var feed = $(this).parent().children('.reader-subscribe-url').text();
      var addButton = $(this).parent().children('.reader-subscribe-add');
      dobrado.log('Adding feed.', 'info');
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'manualAdd', feed: feed, url: location.href,
               token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'reader manualAdd')) {
            return;
          }
          addButton.hide();
        });
    }

    var title = $(this).parent().find('.feed-title');
    var link = $(this).parent().find('.feed-url');
    var channel = $(this).html();
    var check = '<button id="reader-channel-check">check for updates</button>';
    var info = '';
    var subscribe = '';
    if (link.length !== 0) {
      let href = link.attr('href');
      // The link can optionally be wrapped in a .feed-title span.
      if (title.length !== 0) {
        info = check + '<span id="reader-channel-title">' + title.text() +
          '</span><br>' +
          '[<a href="' + href + '" id="reader-channel-url">' + href + '</a>]';
      }
      else {
        info += check + '<a href="' + href + '" id="reader-channel-url">' +
          link.text() + '</a>';
      }
    }
    $('#reader-channel-select').selectmenu({ change: function(event, ui) {
      setChannel(ui.item.value, false); }});
    $('#reader-channel-submit').button().click(addChannel);

    if (channel === 'not set') {
      info += '<p id="reader-channel-status">' +
        'This feed does not have a channel set. ';
      if ($('#reader-channel-select').children().length === 0) {
        info += 'You can add one below to set the channel for this feed.</p>';
      }
      else {
        let firstOption = $('#reader-channel-select').children().first().html();
        if (firstOption !== 'not set') {
          $('#reader-channel-select').prepend('<option selected="selected">' +
                                              'not set</option>');
        }
        info += 'You can select an existing channel from the list or add a ' +
          'new channel below.</p>';
      }
    }
    else {
      info += '<p id="reader-channel-status">' +
        'The channel set for this feed is <b>' + channel + '</b></p>';
    }
    // Also check if the link is a feed list than should be displayed.
    if (link && dobrado.localStorage && localStorage.readerLists) {
      let url = link.attr('href');
      let readerLists = JSON.parse(localStorage.readerLists);
      if (readerLists && readerLists[url]) {
        subscribe += '<p>This feed automatically subscribed you to the feeds ' +
          'listed below, and will be shown in the same channel. You can ' +
          'choose to add them to your own feed list by clicking the ' +
          '<b>add</b> button next to a feed, otherwise they will be kept ' +
          'synchronised with the feed list whenever it is updated.</p>';
        $.each(readerLists[url], function(index, item) {
          subscribe += '<div class="form-spacing">' +
            '<span class="reader-subscribe-url">' + item.feed + '</span>';
          if (item.auto) {
            subscribe += '<button class="reader-subscribe-add">add</button>';
          }
          subscribe += '</div>';
        });
      }
    }
    $('#reader-channel-select').val(channel);
    $('#reader-channel-select').selectmenu('refresh');
    $('#reader-channel-info').html(info);
    $('#reader-channel-check').button().click(checkFeed);
    $('#reader-subscribe-list').html(subscribe);
    $('.reader-subscribe-add').button().click(manualAdd);
    $('.reader-channel-settings').dialog('open');
  }

  function feedSettings() {

    function authorizeTwitter() {
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'authorizeTwitter', url: location.href,
               token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'authorizeTwitter')) {
            return;
          }

          let authorize = JSON.parse(response);
          if (authorize.url) location.href = authorize.url;
        });
    }

    function channelDisplay(channel) {
      if (dobrado.localStorage && localStorage.readerChannelInfo) {
        let channelInfo = JSON.parse(localStorage.readerChannelInfo);
        if (channelInfo[channel]) {
          let info = channelInfo[channel];
          if ($.isNumeric(info.unread)) {
            $('#reader-channel-unread-count').prop('checked', true);
          }
          else {
            $('#reader-channel-unread-' + info.unread).prop('checked', true);
          }
          $('#reader-channel-order').val(info.order);
        }
      }
      $('#reader-channel-update').val(channel);
      $('#reader-channel-display').show();
    }

    function channelFeeds(event, ui) {
      // First reset the feed list so that the new channel selection is shown.
      $('.reader-feed-list .feed-item').show();
      // If an event wasn't fired show the first option in the select.
      let channel = event ? ui.item.value : $('#reader-channel-show').val();
      channelDisplay(channel);
      $('.reader-feed-list .reader-channel-button').each(function() {
        if ($(this).html() !== channel) {
          $(this).parents('.feed-item').hide();
        }
      });
    }

    function exportFeeds() {
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'export', url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'removeChannel')) {
            return;
          }
          var feeds = JSON.parse(response);
          if (feeds.name) {
            location.href = '/php/private.php?file=' + feeds.name;
          }
        });
    }

    function readerOption(event, ui) {
      if (ui.item.value === 'search') {
        $('.reader-feed-list').show();
        $('.reader-feed-list .feed-item').show();
        $('#reader-feed-input-wrapper').show();
        $('#reader-add-input-wrapper').hide();
        $('#reader-channel-wrapper').hide();
        $('#reader-feed-count').hide();
        $('#reader-file-import-wrapper').hide();
        $('.writer .designate').hide();
      }
      else if (ui.item.value === 'channels') {
        $('#reader-channel-wrapper').show();
        $('.reader-feed-list').show();
        $('#reader-feed-input-wrapper').hide();
        $('#reader-add-input-wrapper').hide();
        $('#reader-feed-count').hide();
        $('#reader-file-import-wrapper').hide();
        $('.writer .designate').hide();
        channelFeeds();
      }
      else if (ui.item.value === 'manage') {
        $('#reader-feed-count').show();
        $('.reader-feed-list').hide();
        $('#reader-file-import-wrapper').show();
        $('#reader-feed-input-wrapper').hide();
        $('#reader-add-input-wrapper').show();
        $('#reader-channel-wrapper').hide();
        $('.writer .designate').hide();
      }
      else if (ui.item.value === 'writer') {
        $('.writer .designate').show();
        $('.reader-feed-list').hide();
        $('#reader-feed-input-wrapper').hide();
        $('#reader-add-input-wrapper').hide();
        $('#reader-channel-wrapper').hide();
        $('#reader-feed-count').hide();
        $('#reader-file-import-wrapper').hide();
      }
    }

    function removeChannel() {
      dobrado.log('Removing channel.', 'info');
      var name = $('#reader-channel-show').val();
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'removeChannel', name: name,
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'removeChannel')) {
            return;
          }
          var reader = JSON.parse(response);
          if (reader.settings) {
            $('.reader-settings').html(reader.settings);
            feedSettings();
          }
        });
    }

    function updateChannelDisplay() {
      dobrado.log('Updating channel display.', 'info');
      var name = $('#reader-channel-show').val();
      var unread = $('input[name=reader-channel-unread]:checked').val();
      var order = $('#reader-channel-order').val();
      var update = $('#reader-channel-update').val();
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'updateChannelDisplay', name: name, unread: unread,
               order: order, update: update, url: location.href,
               token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'updateChannelDisplay')) {
            return;
          }
          if (dobrado.localStorage) {
            // channel info is returned here because changing the order for
            // one channel updates all of them.
            localStorage.readerChannelInfo = response;
          }
        });
    }

    function updateFeedList() {
      var search = $('#reader-feed-input').val().toLowerCase();
      $('.reader-feed-list .feed-item').show();
      $('.reader-feed-list .feed-item').each(function() {
        var text = $(this).find('.feed-text').text().toLowerCase();
        var url = $(this).find('.feed-url').attr('href');
        if (text.indexOf(search) === -1 && url.indexOf(search) === -1) {
          $(this).hide();
        }
      });
    }

    $('#reader-feed-input').keyup(function(event) {
      if (event.keyCode !== 13) {
        updateFeedList();
        return;
      }
      event.preventDefault();
      dobrado.reader.addFeed();
    });
    $('.reader-add-feed').button().click(dobrado.reader.addFeed);
    $('.remove-feed-item').button({
      icon: 'ui-icon-closethick',
      showLabel: false
    }).click(removeFeed);
    if ($('.writer .designate').length !== 0) {
      $('#reader-options').
        append('<option value="writer">Writer Settings</option>');
    }
    $('#reader-options').val('search').selectmenu({ change: readerOption });
    $('#reader-channel-display-submit').button().click(updateChannelDisplay);
    $('#reader-channel-display-remove').button().click(removeChannel);
    $('#reader-file-import').change(importFile);
    $('#reader-file-export').button().click(exportFeeds);
    $('#reader-channel-show').selectmenu({ change: channelFeeds });
    $('.reader-channel-button').button().click(channelSettings);
    $('.reader-authorize-twitter').button().click(authorizeTwitter);
    // Any newly added entry will be highlighted, remove the extra
    // class after a few seconds.
    if ($('.feed-text.added').length !== 0) {
      $('.feed-text.added').get(0).scrollIntoView();
      setTimeout(function() {
        $('.feed-text.added').removeClass('added');
      }, 5000);
    }
  }

  function importFile() {

    function addImportedFeed() {
      if (!importList[importCount] && !importList[importCount].xmlUrl) {
        return;
      }

      var xmlUrl = importList[importCount].xmlUrl;
      var title = importList[importCount].title ?
        importList[importCount].title : '';
      var displayUrl = importList[importCount].htmlUrl ?
        importList[importCount].htmlUrl : importList[importCount].xmlUrl;
      var link = '<a href="' + displayUrl + '">' + displayUrl + '</a>';
      if (title !== '') {
        link = '[' + link + ']';
      }
      var channel = importList[importCount].channel;
      var last = importCount === importList.length - 1;
      var text = '<br><span class="reader-imported-feed-' + importCount + '">' +
        'Adding: <b>' + title + '</b> ' +
        '<span class="reader-discovered-feed-url">' + link +
        '</span> ... </span>';
      $('.reader-discovered').append(text);
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'), request: 'reader',
               action: 'addImport', last: last, force: true, xmlUrl: xmlUrl,
               channel: channel, url: location.href, token: dobrado.token },
        function(response) {
          // Handle our own errors here so we can display them in the dialog.
          if (!response) {
            dobrado.log('No response to reader addImport call.', 'error');
            return;
          }

          var reader = JSON.parse(response);
          if (!reader) {
            dobrado.log('Cannot interpret response to reader addImport call.',
                        'error');
            return;
          }
          if (reader.add) {
            $('.reader-discovered').append('ok');
          }
          if (reader.error) {
            $('.reader-discovered').append('<p><i>' + reader.error +
                                           '</i></p>');
          }
          $('.reader-imported-feed-' + importCount).get(0).scrollIntoView();
          importCount++;
          if (importCount < importList.length) {
            addImportedFeed();
          }
          else {
            var text = 'ok<p class="reader-import-finished">Finished ' +
              'importing feeds.</p>';
            $('.reader-discovered').append(text);
            $('.reader-import-finished').get(0).scrollIntoView();
            // After imported feeds are added the content is reset.
            if (reader.content) {
              $('.reader-content').html(reader.content);
            }
            if (reader.more) {
              $('.reader-more').show();
            }
            else {
              $('.reader-more').hide();
            }
            // Also update the feed list.
            $('.reader-settings').html(reader.settings);
            feedSettings();
            newFeedEvents();
            if (dobrado.localStorage && reader.content) {
              localStorage.reader = reader.content;
              localStorage.readerUpdate = new Date().getTime();
              localStorage.readerPage = location.href;
            }
          }
        });
    }

    var formData = new FormData();
    if (!formData) {
      dobrado.log('Your browser doesn\'t support file uploading.', 'error');
      return;
    }

    dobrado.log('Importing file...', 'info');
    var file = $('#reader-file-import').get(0).files[0];
    if (file) {
      formData.append('file', file);
      formData.append('request', 'reader');
      formData.append('action', 'import');
      formData.append('url', location.href);
      formData.append('token', dobrado.token);
      $.ajax({
        url: '/php/request.php',
        data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(response) {
          if (dobrado.checkResponseError(response, 'reader importFile')) {
            return;
          }
          importList = JSON.parse(response);
          importCount = 0;
          if (importList.length !== 0) {
            var text = '<p>Found <b>' + importList.length +
              '</b> feeds in imported file:</p>';
            $('.reader-discovered').html(text).dialog('open');
            addImportedFeed();
          }
        }
      });
    }
    return false;
  }

  function newFeedEvents() {

    (function tooltipEvents() {

      var tooltipClick = false;
      var channelOptions = $('#reader-channel-select').html();

      function tooltipContent() {
        var name = $(this).html();
        var link = $(this).attr('href');
        var shortLink = link;
        var fields = shortLink.match(/^https?:\/\/(.*)$/);
        if (fields && fields.length === 2) {
          shortLink = fields[1];
        }
        if (shortLink.length > 25) {
          shortLink = shortLink.substring(0, 20) + '...';
        }
        var feed = $(this).parents('.reader-group').find('a.reader-group-link');
        var feedName = feed.html();
        var feedUrl = feed.attr('href');
        var photo = $(this).siblings('.author-photo-url');
        var repostUrl =
          $(this).siblings('.reader-reposted-by').find('a').attr('href');
        var repostName =
          $(this).siblings('.reader-reposted-by').find('a').html();
        var content = '';

        if (photo.length !== 0) {
          content += '<a class="tooltip-photo" href="' +
            photo.attr('href') + '">' + photo.html() + '</a>';
        }
        // The text for tooltip-url and tooltip-note are updated in tooltipOpen
        // if a nickname and bio are found in local storage for the author.
        content += '<a class="tooltip-name" href="' + link + '">' +
          name + '</a>';
        // The repost author is added but hidden here so that the channel can
        // be changed when the tooltip opens.
        if (repostUrl) {
          content += '<span class="tooltip-repost hidden">' + repostUrl +
            '</span>';
        }
        content += '<br><a class="tooltip-url" href="' + link + '">' +
          shortLink + '</a><div class="tooltip-note"></div>';
        if (channelOptions !== '') {
          content += '<div class="tooltip-channel-info">';
          // Switch to the repost author now if available.
          if (repostName) {
            name = repostName;
          }
          // If author matches feed don't need to show as much detail here.
          // (Also special case Facebook and Twitter feeds.)
          if (feedUrl === link || feedName === '' ||
              feedName.indexOf(name) !== -1 ||
              feedUrl === 'https://twitter-atom.appspot.com' ||
              feedUrl === 'https://facebook-atom.appspot.com') {
            content += 'Channel: <a class="tooltip-feed" href="' +
              feedUrl + '"></a>';
          }
          else {
            content += 'Channel for ' + name +
              ' in <a class="tooltip-feed" href="' + feedUrl + '">' +
              feedName + '</a>: ';
          }
          content += '<select class="tooltip-channel-select">' +
                channelOptions +
              '<option>Add channel</option></select></div>' +
            '<div class="hidden">' +
              '<label>Add channel:</label>' +
              '<input class="tooltip-channel-add" type="text">' +
              '<button class="tooltip-channel-submit">submit</button>' +
            '</div>';
        }
        return content;
      }

      function tooltipOpen(event, ui) {

        function tooltipAddChannel() {
          var option = $('#' + tooltipId + ' .tooltip-channel-add').val();
          if (option === '') {
            return;
          }
          $('#' + tooltipId + ' .tooltip-channel-add').val('');
          tooltipSetChannel(option, true);
        }

        function tooltipSelectChannel(event, ui) {
          if (ui.item.value === 'Add channel') {
            $('#' + tooltipId + ' .tooltip-channel-add').parent().show();
          }
          else {
            tooltipSetChannel(ui.item.value, false);
          }
        }

        function tooltipSetChannel(option, add) {
          var feed = $('#' + tooltipId + ' .tooltip-feed').attr('href');
          var author = $('#' + tooltipId + ' .tooltip-repost').html();
          if (!author) {
            author = $('#' + tooltipId + ' .tooltip-name').attr('href');
          }
          dobrado.log('Setting channel for author', 'info');
          $.post('/php/request.php',
                 { id: '#' + $('.reader').attr('id'), request: 'reader',
                   action: 'setChannel', channel: option, feed: feed,
                   author: author, url: location.href, token: dobrado.token },
            function(response) {
              if (dobrado.checkResponseError(response, 'tooltipsetChannel')) {
                return;
              }
              var channel = JSON.parse(response);
              if (add) {
                $('#reader-channel-select').append('<option>' + channel.name +
                                                   '</option>');
                channelOptions = $('#reader-channel-select').html();
              }
              // Remove all tooltips after setting a channel because
              // tooltipClose doesn't get called in this case.
              $('.ui-tooltip').remove();
              if (dobrado.localStorage) {
                let authorChannel = {};
                if (localStorage.readerAuthorChannel) {
                  authorChannel = JSON.parse(localStorage.readerAuthorChannel);
                }
                authorChannel[author + ':' + feed] = channel.name;
                localStorage.readerAuthorChannel =
                  JSON.stringify(authorChannel);
              }
            });
        }

        function tooltipSetDetails(tooltipId, details, author, feed) {
          if (details[author + ':' + feed]) {
            let authorDetails = details[author + ':' + feed];
            if (authorDetails.nickname) {
              let nickname = '@' + authorDetails.nickname;
              $('#' + tooltipId + ' .tooltip-url').html(nickname);
            }
            if (authorDetails.note) {
              $('#' + tooltipId + ' .tooltip-note').html(authorDetails.note);
            }
          }
          else if (details[feed]) {
            let authorDetails = details[feed];
            if (authorDetails.nickname) {
              let nickname = '@' + authorDetails.nickname;
              $('#' + tooltipId + ' .tooltip-url').html(nickname);
            }
            if (authorDetails.note) {
              $('#' + tooltipId + ' .tooltip-note').html(authorDetails.note);
            }
          }
        }

        var tooltipId = ui.tooltip.attr('id');

        $('#' + tooltipId + ' .tooltip-channel-submit').button().
          click(tooltipAddChannel);
        let author = $('#' + tooltipId + ' .tooltip-name').attr('href');
        let repost = $('#' + tooltipId + ' .tooltip-repost').html();
        if (!repost) {
          repost = author;
        }
        let feed = $('#' + tooltipId + ' .tooltip-feed').attr('href');
        let defaultChannel = currentChannel;
        if (dobrado.localStorage && localStorage.readerAuthorChannel) {
          let authorChannel = JSON.parse(localStorage.readerAuthorChannel);
          if (authorChannel[repost + ':' + feed]) {
            defaultChannel = authorChannel[repost + ':' + feed];
          }
          else if (authorChannel[feed]) {
            defaultChannel = authorChannel[feed];
          }
        }
        let details = {};
        if (dobrado.localStorage && localStorage.readerAuthorDetails) {
          details = JSON.parse(localStorage.readerAuthorDetails);
          tooltipSetDetails(tooltipId, details, author, feed);
        }
        if (!details[author + ':' + feed] && !details[feed]) {
          // If not found in local storage fetch and cache the details.
          $.post('/php/request.php',
                 { id: '#' + $('.reader').attr('id'), request: 'reader',
                   action: 'nickname', authorUrl: author,
                   url: location.href, token: dobrado.token },
            function(response) {
              if (dobrado.checkResponseError(response, 'nickname')) {
                return;
              }
              let authorDetails = JSON.parse(response);
              if (dobrado.localStorage) {
                if (localStorage.readerAuthorDetails) {
                  details = JSON.parse(localStorage.readerAuthorDetails);
                }
                details[author + ':' + feed] =
                  { nickname: authorDetails.nickname,
                    note: authorDetails.note };
                tooltipSetDetails(tooltipId, details, author, feed);
                localStorage.readerAuthorDetails = JSON.stringify(details);
              }
            });
        }
        $('#' + tooltipId + ' .tooltip-channel-select').val(defaultChannel).
          selectmenu({ change: tooltipSelectChannel });
      }

      function tooltipClose(event, ui) {

        function over() {
          $(this).stop(true).fadeIn();
        }

        function out() {
          // The selectmenu in the tooltip can extended outside the tooltip,
          // triggering this function, so do nothing when it's open.
          var selectmenu = '#' + tooltipId + ' .ui-selectmenu-button';
          if (!$(selectmenu).hasClass('ui-selectmenu-button-open')) {
            $(this).fadeOut(400, function() { $(this).remove(); });
          }
        }

        var tooltipId = ui.tooltip.attr('id');
        ui.tooltip.hover(over, out);
        // Reset tooltipClick when close is called in case it was from a
        // different event.
        tooltipClick = false;
      }

      // Tooltips are only displayed for a user's own feed, in which case
      // readerChannelInfo is returned from calling listChannels.
      if (dobrado.localStorage && localStorage.readerChannelInfo) {
        $('.reader-item .author-name').tooltip({
          items: '.author-name',
          content: tooltipContent,
          open: tooltipOpen,
          close: tooltipClose
        }).click(function() {
          // Also allowing toggling the tooltip by clicking on the author.
          tooltipClick = !tooltipClick;
          if (tooltipClick) {
            $(this).tooltip('open');
          }
          else {
            $(this).tooltip('close');
          }
          return false;
        });
        // Existing tooltips get opened when tooltipEvents is run again so close
        // them here.
        $('.reader-item .author-name').tooltip('close');
      }
    }());

    $('.reader-actions a').click(readerAction);
    $('.reader-content .read-more').click(function() {
      $(this).parent().hide().siblings('.real-content').show();
      return false;
    });
    $('.reader-content .show-group').click(function() {
      $(this).siblings('.reader-item').show();
      $(this).hide();
      return false;
    });
    dobrado.indieConfig();
  }

  function readerAction() {

    function quoteContent() {
      // When the user clicks in the editor, the content gets quoted and the
      // webactionType is reset to post on the server. The original url needs
      // to be added to the body of the post here too.
      $('#writer-author').val(dobrado.readCookie('user'));
      var data = editor.getData();
      // If data is just a link to the post remove it since it's added here.
      if (data === '<a href="' + permalink + '">' + permalink + '</a>') {
        data = '';
      }
      editor.setData('<br><br><cite class="h-cite u-quotation-of">' + data +
                     '<br><a class="u-url" href="' + permalink + '">' +
                       permalink + '</a></cite>');
      if (dobrado.writer) {
        dobrado.writer.action('post', permalink);
      }
    }

    function resetAction() {
      if (currentAction) {
        currentAction.removeClass('selected');
      }
      if (editor) {
        editor.destroy();
        editor = null;
      }
      $('#reader-action-wrapper').remove();
    }

    function cancelAction() {
      resetAction();
      dobrado.writer.removeAction(true);
    }

    function submitAction() {
      // This is required by dobrado.writer.newModuleCallback.
      dobrado.editor.setData(editor.getData());
      dobrado.log('Creating post...', 'info');
      dobrado.createModule('writer', 'post', 'post');
      resetAction();
    }

    var content = '';
    var sendTo = '';
    var item = $(this).parents('.reader-item');
    var permalink = item.find('.permalink').attr('href');
    // Don't need to create the editor if it has already been done for this
    // item, otherwise try removing it from another item first.
    if (item.find('#reader-action-wrapper').length === 0) {
      if (editor) {
        editor.destroy();
        editor = null;
      }
      $('#reader-action-wrapper').remove();
      item.append('<div id="reader-action-wrapper">' +
                    '<div id="reader-action-content"></div>' +
                    '<button id="reader-action-cancel">Cancel</buton>' +
                    '<button id="reader-action-submit">Submit</buton></div>');
      $('#reader-action-cancel').button().click(cancelAction);
      $('#reader-action-submit').button().click(submitAction);
      let css = '.photo-hidden img { display: none; } ' +
        '.cke_editable { margin: 10px; }';
      CKEDITOR.addCss(css);
      editor = CKEDITOR.replace('reader-action-content',
        { allowedContent: true,
          autoGrow_minHeight: $('#reader-action-content').height(),
          autoGrow_onStartup: true,
          disableNativeSpellChecker: false,
          enterMode: CKEDITOR.ENTER_BR,
          filebrowserBrowseUrl: '/php/browse.php',
          removePlugins:
            'elementspath,tableselection,tabletools,contextmenu,liststyle',
          resize_enabled: false,
          toolbar: [[ 'Undo', 'Redo', '-', 'Bold', 'Italic', '-',
                      'Link', 'Unlink', '-', 'EmojiPanel', 'Image' ]]
        });
    }
    if (item.hasClass('twitter-atom-appspot-com')) {
      sendTo = 'twitter';
    }

    if (currentAction) {
      currentAction.removeClass('selected');
    }
    currentAction = $(this);
    currentAction.addClass('selected');
    if (currentAction.hasClass('like')) {
      content = '<a href="' + permalink + '">' + permalink + '</a>';
      editor.setData(content);
      if (dobrado.writer) {
        dobrado.writer.action('like', permalink, sendTo);
      }
    }
    else if (currentAction.hasClass('share')) {
      // Check if this author has set repost allowed microformats on this item.
      let repostAllowed = item.find('.p-repost-allowed').text();
      if (repostAllowed === 'none') {
        alert('Author requests that repost is not created.');
        return false;
      }

      // Look for class 'real-content' first, so that extra markup is not
      // included when the content is currently hidden. Also make an exception
      // that items from twitter can be reposted without requiring microformats.
      if (repostAllowed === 'full' || sendTo === 'twitter') {
        content = item.find('.real-content').html();
        if (!content) {
          content = item.find('.content').html();
        }
        $('#writer-title').val(item.find('.title').text());
        let media = item.find('.media img').map(function() {
          return this.outerHTML;
        }).get().join();
        if (media !== '') {
          $('#writer-photo-selected').append(media);
        }
      }
      else {
        content = '<a href="' + permalink + '">' + permalink + '</a>';
      }
      editor.setData(content);
      editor.once('focus', quoteContent);
      $('#writer-author').val(item.find('.author-name').html());
      if (dobrado.writer) {
        dobrado.writer.action('share', permalink, sendTo);
      }
    }
    else if (currentAction.hasClass('reply')) {
      editor.setData('');
      if (dobrado.writer) {
        dobrado.writer.action('reply', permalink, sendTo);
      }
    }
    return false;
  }

  function removeFeed() {
    dobrado.log('Removing feed...', 'info');
    var channelButton = $(this).siblings('.reader-channel-button');
    var channel = '';
    if (channelButton.length === 1) {
      channel = channelButton.button('option', 'label');
    }
    $.post('/php/request.php',
           { id: '#' + $('.reader').attr('id'),
             request: 'reader', action: 'remove',
             xmlUrl: $(this).siblings('.feed-text').
                       find('a.feed-url').prop('href'),
             channel: channel, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'reader remove')) {
          return;
        }
        var reader = JSON.parse(response);
        // When a new feed is removed the content is reset.
        if (reader.content) {
          $('.reader-content').html(reader.content);
        }
        else {
          $('.reader-content').html('');
        }
        if (reader.more) {
          $('.reader-more').show();
        }
        else {
          $('.reader-more').hide();
        }
        // Also update the feed list.
        $('.reader-settings').html(reader.settings);
        feedSettings();
        newFeedEvents();
        if (dobrado.localStorage) {
          localStorage.reader = reader.content;
          localStorage.readerUpdate = new Date().getTime();
          localStorage.readerPage = location.href;
        }
      });
  }

  function request(update, action) {
    if (update === 'newer' && !$('.control .info').is(':visible')) {
      // This can conflict with a status update from brid.gy, so only display
      // if a message isn't currently shown.
      dobrado.log('Checking reader updates...', 'info');
    }
    $.post('/php/request.php',
           { id: '#' + $('.reader').attr('id'), request: 'reader',
             update: update, action: action, channel: currentChannel,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'reader ' + update)) {
          return;
        }
        var reader = JSON.parse(response);
        if (update === 'older') {
          if (reader.content) {
            $('.reader-writer').show();
            $('.reader-content').append(reader.content);
          }
          $('.reader-more').button('option', 'label', 'Load more items');
          if (reader.more) {
            $('.reader-more').show();
          }
          else {
            $('.reader-more').hide();
          }
          $('.reader-reset').show();
        }
        else if (update === 'newer') {
          if (reader.content) {
            $('.reader-writer').show();
            // Only show new content at the top of the page if scroll position
            // is near the top of the window. (Or when previously no content.)
            if ($(window).scrollTop() < 200 ||
                $('.reader-content').html() === '') {
              $('.reader-content').prepend(reader.content);
              // Also use this case to minimise the space taken up by existing
              // reader-content that has previously been expanded.
              $('.reader-content .content-excerpt').show();
              $('.reader-content .real-content').hide();
              // Don't show unread status for the current channel in this case.
              if (reader.info && reader.info[currentChannel]) {
                reader.info[currentChannel] = 0;
              }
            }
            else {
              $('.reader-content').prepend('<div class="reader-hidden">' +
                                           reader.content + '</div>');
              $('.reader-show-hidden-wrapper').show();
              // The unread count for items in the current channel gets updated
              // with each request for newer items, so need to add them to the
              // current unread count in this case only.
              if (reader.info && $.isNumeric(reader.info[currentChannel])) {
                let currentUnread = 0;
                let selected = $('#page-select > option:selected');
                let fields = selected.html().match(/\(([0-9]+)\)$/);
                if (fields && fields.length === 2) {
                  currentUnread = parseInt(fields[1], 10);
                  reader.info[currentChannel] += currentUnread;
                }
              }
            }
            // reader.info is a list of channel names and their unread status
            // which can be used to update the channel names in the page select.
            if (dobrado.control && reader.info) {
              $.each(reader.info, updatePageSelect);
              $('#page-select').pageselectmenu('refresh');
            }
          }
        }
        newFeedEvents();
        if (dobrado.localStorage) {
          localStorage.reader = $('.reader-content').html();
          localStorage.readerUpdate = new Date().getTime();
          localStorage.readerPage = location.href;
        }
      });
  }

  function reset(notify) {

    function loadChannelInfo() {
      $.post('/php/request.php',
             { id: '#' + $('.reader').attr('id'),
               request: 'reader', action: 'listChannels',
               url: location.href, token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, 'reader listChannels')) {
            return;
          }
          if (dobrado.localStorage) {
            let channel = JSON.parse(response);
            if (channel.info) {
              localStorage.readerChannelInfo = JSON.stringify(channel.info);
            }
            else {
              localStorage.readerChannelInfo = '';
            }
            if (channel.author) {
              localStorage.readerAuthorChannel = JSON.stringify(channel.author);
            }
            else {
              localStorage.readerAuthorChannel = '';
            }
            if (channel.lists) {
              localStorage.readerLists = JSON.stringify(channel.lists);
            }
            else {
              localStorage.readerLists = '';
            }
          }
          // Call newFeedEvents once channel info is loaded as tooltips aren't
          // used if channel info is not set.
          newFeedEvents();
        });
    }

    window.scroll(0, 0);
    $('.reader-show-hidden-wrapper').hide();
    dobrado.log('Loading feeds...', 'info');
    // Remove old content so the change doesn't flash on load.
    $('.reader-content').html('');
    $.post('/php/request.php',
           { id: '#' + $('.reader').attr('id'), request: 'reader',
             action: 'reset', channel: currentChannel,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'reader reset')) {
          return;
        }
        var reader = JSON.parse(response);
        var update = 0;
        if (reader.content) {
          $('.reader-writer').show();
          $('.reader-content').html(reader.content);
          update = new Date().getTime();
        }
        if (reader.more) {
          $('.reader-more').show();
        }
        else {
          $('.reader-more').hide();
        }
        // Feed settings are provided on reset for indieauth users, and are
        // otherwise loaded with the page for other users.
        if (reader.settings) {
          $('.reader-settings').html(reader.settings);
          feedSettings();
        }
        if ($('.reader-settings .feed-item').length <= 5) {
          $('.reader-settings').show();
        }
        if (dobrado.control && reader.info) {
          // reader.info can contain the unread count for the current channel,
          // which we don't want to show when resetting the page.
          if (reader.info[currentChannel]) {
            reader.info[currentChannel] = 0;
          }
          $.each(reader.info, updatePageSelect);
          $('#page-select').pageselectmenu('refresh');
        }
        if (dobrado.localStorage) {
          localStorage.reader = reader.content;
          localStorage.readerUpdate = update;
          localStorage.readerPage = location.href;
        }
        if ($('.reader-channel-settings').length === 1) {
          loadChannelInfo();
        }
        else {
          newFeedEvents();
        }
        if (notify) {
          // Set up notify now that the session variables have been reset.
          dobrado.notify('feed', dobrado.reader.update);
        }
      });
  }

  function showHidden() {
    $('.reader-hidden').show();
    $('.reader-show-hidden-wrapper').hide();
    // Channel unread status is updated when new content is hidden, so when
    // the user displays the content remove the unread status.
    if (currentChannel === 'global') {
      // When viewing all feeds can remove every unread status.
      $('#page-select > option').each(function() {
        var value = $(this).val();
        if (value !== 'global') {
          $(this).html(value);
          $(this).attr('data-class', 'false');
        }
      });
    }
    else {
      updatePageSelect(currentChannel, false);
    }
    if (dobrado.control) {
      $('#page-select').pageselectmenu('refresh');
    }
  }

  function updatePageSelect(channel, unread) {
    $('#page-select > option').each(function() {
      if ($(this).val() === channel) {
        if (unread === false || unread === 0) {
          $(this).html(channel);
          $(this).attr('data-class', 'false');
        }
        else if (unread === true) {
          $(this).html(channel);
          $(this).attr('data-class', 'true');
        }
        else {
          if (unread === 99) unread = '99+';
          $(this).html(channel + ' (' + unread + ')');
          $(this).attr('data-class', 'true');
        }
        return false;
      }
    });
  }

  dobrado.reader.addFeed = function(xmlUrl, force, multiple) {

    function addDiscoveredFeed() {
      var discovered = [];
      $('.reader-discovered input:checked').each(function(index) {
        discovered.push($(this).val());
      });
      if (discovered.length === 1) {
        dobrado.reader.addFeed(discovered[0], true);
      }
      else if (discovered.length > 1) {
        dobrado.reader.addFeed(JSON.stringify(discovered), true, true);
      }
    }

    // Need to set xmlUrl if not called from addDiscovered feed, otherwise the
    // parameter is assigned the click event data.
    if (!force) {
      xmlUrl = $('#reader-add-input').val();
    }
    if (xmlUrl === '') return;

    dobrado.log('Adding feed...', 'info');
    var action = multiple ? 'addMultiple' : 'add';
    $.post('/php/request.php',
           { id: '#' + $('.reader').attr('id'), request: 'reader',
             action: action, xmlUrl: xmlUrl, force: force,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'reader addFeed')) {
          return;
        }
        // The writer module can call addFeed so try removing the action.
        $('#writer-webaction-info').html('');
        var reader = JSON.parse(response);
        // When the url is used for feed discovery no other content is returned.
        if (reader.discovered) {
          $('.reader-discovered').html(reader.discovered).dialog('open');
          $('.reader-discovered-cancel').button().click(function() {
            $('.reader-discovered').dialog('close');
          });
          $('.reader-discovered-add').button().click(addDiscoveredFeed);
          return;
        }

        if ($('.reader-discovered').dialog('isOpen')) {
          $('.reader-discovered').dialog('close');
        }
        // When a new feed is added the content is reset.
        if (reader.content) {
          $('.reader-content').html(reader.content);
        }
        if (reader.more) {
          $('.reader-more').show();
        }
        else {
          $('.reader-more').hide();
        }
        // Also update the feed list.
        $('#reader-add-input').val('');
        $('.reader-settings').html(reader.settings);
        feedSettings();
        newFeedEvents();
        if (dobrado.localStorage) {
          localStorage.reader = reader.content;
          localStorage.readerUpdate = new Date().getTime();
          localStorage.readerPage = location.href;
        }
      });
  };

  dobrado.reader.channelSelect = function(channel) {
    dobrado.log('Checking channel...', 'info');
    $.post('/php/request.php',
           { id: '#' + $('.reader').attr('id'), request: 'reader',
             action: 'changeChannel', channel: channel,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'reader change channel')) {
          return;
        }
        var change = JSON.parse(response);
        if (change.channel) {
          // Reset the html for this option to remove unread status, can ignore
          // 'global' which is a special case and doesn't need updating.
          if (channel !== 'global') {
            updatePageSelect(channel, false);
            $('#page-select').pageselectmenu('refresh');
          }
          currentChannel = channel;
          if (dobrado.localStorage) {
            localStorage.readerCurrentChannel = currentChannel;
          }
          // Delay reset so the log message displayed above has time to close.
          setTimeout(function() { reset(false); }, 1000);
        }
        else {
          $('.reader-content').html('<p>No feeds were found for this channel.' +
                                    '</p><p>You can update your channels in ' +
                                    '<b>feed settings</b> by clicking the ' +
                                    'channel button next to each feed.</p>');
        }
      });
  };

  dobrado.reader.update = function(action) {
    request('newer', action);
  };

  dobrado.reader.updateAction = function(url) {
    // Set checked to false on the indie-action's for this item so
    // that indieConfig will add a highlight to the actioned link and
    // also update the href to point to the new permalink.
    var item = $('.reader-item a[href="' + url + '"]').parents('.reader-item');
    item.find('indie-action').data('checked', false);
    dobrado.indieConfig();
  };

}());
