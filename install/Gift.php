<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Gift extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $object = array();
    $mysqli = connect_db();
    $us_mode = isset($_POST['mode']) ? $_POST['mode'] : '';
    if (isset($_POST['category'])) {
      $category = $mysqli->escape_string($_POST['category']);
    }
    $mysqli->close();

    if ($us_mode === 'display') {
      return array('content' => $this->DisplayCategory($category));
    }
    if ($us_mode === 'remove') {
      return $this->RemoveClaim();
    }
    if ($us_mode === 'claim') {
      return $this->Claim();
    }
    if ($us_mode === 'edit') {
      return $this->EditRegistry();
    }
    if ($us_mode === 'change') {
      return $this->SelectionChange();
    }
    if ($us_mode === 'messages') {
      return $this->Messages();
    }
    if ($us_mode === 'moderators') {
      return $this->MailModerators();
    }
    return array('error' => 'Unknown gift callback mode.');
  }

  public function CanAdd($page) {
    // Can only have one gift module on a page.
    return !$this->AlreadyOnPage('gift', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $mysqli = connect_db();
    $content = '';
    $mode = $mysqli->escape_string($_GET['mode']);

    // These values are set once the user has already claimed an item.
    $claim_id = $mysqli->escape_string($_GET['claim_id']);
    $claim_email = $mysqli->escape_string($_GET['claim_email']);
    if (!empty($claim_id) && !empty($claim_email)) {
      $mysqli->close();
      return $this->DisplayItem($mode, $claim_id, $claim_email);
    }
    
    // If the user can edit the page, they can manage the registry.
    if ($this->user->canEditPage) {
      if ($result = $mysqli->query('SELECT name FROM gift_type')) {
        if ($mode === 'manage' || $result->num_rows === 0) {
          $result->close();
          $mysqli->close();
          return $this->ManageRegistry();
        }
        else {
          $content = '<div class="manage"><a href="' .
            $this->Url('mode=manage') . '">Edit your gift registry</a></div>';
        }
        $result->close();
      }
      else {
        $this->Log('Gift->Content: ' . $mysqli->error);
      }
    }
    if ($mode === 'display') {
      $content .= $this->DisplayCategory();
    }
    $mysqli->close();
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.gift.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.gift.js');
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS gift_registry (' .
      'email_message TEXT,' .
      'moderators TEXT' .
      ') ENGINE=MyISAM';
    if (!$q = $mysqli->query($query)) {
      $this->Log('Gift->Install 1: ' . $mysqli->error);
    }
    // Insert empty values into gift_registry table to simplify updates.
    if (!$mysqli->query('INSERT INTO gift_registry VALUES ("", "")')) {
      $this->Log('Gift->Install 2: ' . $mysqli->error);
    }

    // TODO: This module can't handle installs by multiple users. Need to add
    // a 'user' column to these tables and then update the rest of the module. 
    $query = 'CREATE TABLE IF NOT EXISTS gift_type (' .
      'name VARCHAR(50) NOT NULL,' .
      'category VARCHAR(50),' .
      'title VARCHAR(100),' .
      'description TEXT,' .
      'image VARCHAR(100),' .
      'price INT UNSIGNED NOT NULL,' .
      'claim_max INT UNSIGNED NOT NULL,' .
      'claim_count INT UNSIGNED NOT NULL,' .
      'PRIMARY KEY(name)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Gift->Install 3: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS gift_item (' .
      'claim_id INT UNSIGNED NOT NULL,' .
      'claim_name VARCHAR(50) NOT NULL, ' .
      'claim_email VARCHAR(100),' .
      'claim_message TEXT,' .
      'message_approved TINYINT(1),' .
      'message_id VARCHAR(32),' .
      'name VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(claim_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Gift->Install 4: ' . $mysqli->error);
    }
    $mysqli->close();

    $site_style =
      array('"",".gift-type-display .claim","float","right"',
            '"",".gift-type-display .claim","padding","0.2em"',
            '"",".gift-type-display .title","background-color","#bbbbbb"',
            '"",".gift-type-display .title","font-size","1.4em"',
            '"",".gift-type-display .title","padding","0.2em"',
            '"",".gift-type-display .title","border-radius",' .
              '"10px 10px 0px 0px"',
            '"",".gift-type-display .description","border","4px solid #bbbbbb"',
            '"",".gift-type-display .description","min-height","100px"',
            '"",".gift-type-display","padding-bottom","10px"',
            '"",".gift-type-display .claim-form","background-color","#dddddd"',
            '"",".gift-type-display .claim-form","border-left",' .
              '"4px solid #bbbbbb"',
            '"",".gift-type-display .claim-form","border-right",' .
              '"4px solid #bbbbbb"',
            '"",".gift-type-display .claim-form","padding","5px"',
            '"",".gift-type-display .claim-form .submit","float","right"',
            '"",".gift-type-display .reply","background-color","#ffeead"',
            '"",".gift-type-display .reply","border-left","4px solid #bbbbbb"',
            '"",".gift-type-display .reply","border-right","4px solid #bbbbbb"',
            '"",".manage-gifts","font-size","2em"',
            '"",".manage-response","font-size","1em"',
            '"",".manage-response","background-color","yellow"',
            '"",".manage-response","border","1px solid red"',
            '"","#email-message-form","background-color","#dddddd"',
            '"","#email-message-form","padding","10px"',
            '"","#email-message-form","margin","10px"',
            '"","#moderators-form","background-color","#dddddd"',
            '"","#moderators-form","padding","10px"',
            '"","#moderators-form","margin","10px"',
            '"",".gift-type-message","background-color","#dddddd"',
            '"",".gift-type-message","margin","10px 10px 0px"',
            '"",".gift-type-message","padding","10px"',
            '"","#gift-type-form","background-color","#dddddd"',
            '"","#gift-type-form","margin","0px 10px"',
            '"","#gift-type-form","padding","10px"',
            '"","#gift-type-form","line-height","2em"');
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.gift.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.gift.js');
  }

  // Private functions below here ////////////////////////////////////////////

  private function RemoveClaim($type) {
    $mysqli = connect_db();
    $type = $mysqli->escape_string($_POST['type']);
    $claim_id = $mysqli->escape_string($_POST['claim_id']);

    $query = 'DELETE FROM gift_item WHERE claim_id = "' . $claim_id . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Gift->RemoveClaim 1: ' . $mysqli->error);
    }

    $query = 'REPLACE INTO gift_type (name, category, title, description, ' .
      'image, price, claim_max, claim_count) SELECT name, category, title, ' .
      'description, image, price, claim_max, claim_count-1 FROM gift_type ' .
      'WHERE name = "' . $type . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Gift->RemoveClaim 2: ' . $mysqli->error);
    }
    $mysqli->close();

    return array('done' => true);
  }

  private function Claim() {
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $us_message = $purifier->purify($_POST['message']);

    $object = array();
    $mysqli = connect_db();
    $type = $mysqli->escape_string($_POST['type']);
    $name = $mysqli->escape_string($_POST['name']);
    $email = $mysqli->escape_string($_POST['email']);
    $message = $mysqli->escape_string($us_message);

    $claim_count = 0;
    $query = 'SELECT claim_max, claim_count FROM gift_type WHERE ' .
      'name = "' . $type . '"';
    if ($result = $mysqli->query($query)) {
      if ($gift_type = $result->fetch_assoc()) {
        $claim_count = (int)$gift_type['claim_count'];
        $remaining = (int)$gift_type['claim_max'] - $claim_count;
        if ($remaining === 0) {
          $result->close();
          $mysqli->close();
          $object['unavailable'] = true;
          return $object;
        }
        $object['remaining'] = $remaining - 1;
      }
      $result->close();
    }
    else {
      $this->Log('Gift->Claim 1: ' . $mysqli->error);
    }

    $object['unavailable'] = false;
    $query = 'UPDATE gift_type SET claim_count = ' . ($claim_count + 1).
      ' WHERE name = "' . $type . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Gift->Claim 2: ' . $mysqli->error);
    }
    
    $id = 0;
    $query = 'SELECT MAX(claim_id) AS claim_id FROM gift_item';
    if ($result = $mysqli->query($query)) {
      if ($gift_item = $result->fetch_assoc()) {
        $id = (int)$gift_item['claim_id'] + 1;
      }
      $result->close();
    }
    else {
      $this->Log('Gift->Claim 3: ' . $mysqli->error);
    }
    $object['id'] = $id;
      
    $message_id = bin2hex(openssl_random_pseudo_bytes(16));
    $query = 'INSERT INTO gift_item VALUES (' . $id . ', "' . $name . '", ' .
      '"' . $email . '", "' . $message . '", 0, "' . $message_id . '", ' .
      '"' . $type . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Gift->Claim 4: ' . $mysqli->error);
    }

    // The email content.
    $body = 'Dear ' . $name . ",\n\n";
    $query = 'SELECT email_message, moderators FROM gift_registry';
    if ($result = $mysqli->query($query)) {
      if ($gift_registry = $result->fetch_assoc()) {
        $body .= $gift_registry['email_message'];
      }
      $result->close();
    }
    else {
      $this->Log('Gift->Claim 5: ' . $mysqli->error);
    }
    $mysqli->close();

    $server = $this->user->config->ServerName();
    $url = $this->Url('claim_id=' . $id . '&claim_email=' . $email);
    $body .= "\n\nPrint a copy of your gift here:\n\n" . $server . $url .
      "\n\nIf you want to change your gift use the following link:\n\n" .
      $server . $url . "&mode=change";
    $body = wordwrap($body);
    $sender = $this->Substitute('system-sender', '/!host/', $server);
    $sender_name = $this->Substitute('system-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $headers = 'From: ' . $sender_name . "\r\n";
    mail($email, 'Your gift at ' . $server, $body, $headers, '-f ' . $sender);
    return $object;
  }

  private function MailModerators() {
    if (!isset($_POST['claimId']) || $_POST['claimId'] === '') {
      return array('error' => 'No claim id');
    }

    $mysqli = connect_db();
    $claim_id = $mysqli->escape_string($_POST['claimId']);
    // Get the message and name for the claim_id.
    $name = '';
    $email = '';
    $message = '';
    $message_id = '';
    $query = 'SELECT claim_name, claim_email, claim_message, message_id FROM ' .
      'gift_item WHERE claim_id=' . $claim_id;
    if ($result = $mysqli->query($query)) {
      if ($gift_item = $result->fetch_assoc()) {
        $name = $gift_item['claim_name'];
        $email = $gift_item['claim_email'];
        $message = $gift_item['claim_message'];
        $message_id = $gift_item['message_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Gift->MailModerators 1: ' . $mysqli->error);
    }

    // Get the list of moderators.
    $moderators = '';
    $query = 'SELECT moderators FROM gift_registry';
    if ($result = $mysqli->query($query)) {
      if ($gift_registry = $result->fetch_assoc()) {
        $moderators = $gift_registry['moderators'];
      }
      $result->close();
    }
    else {
      $this->Log('Gift->MailModerators: ' . $mysqli->error);
    }
    $mysqli->close();

    $server = $this->user->config->ServerName();
    $url = $this->Url('claim_id=' . $claim_id . '&claim_email=' . $email);
    if ($message !== '' && $moderators !== '') {
      $body = 'User: ' . $name."\n\n".
        "Left the following message:\n\n" . $message . "\n\n" .
        "To approve, click the following link:\n\n" .
        $server . $url . '&message_id=' . $message_id . '&mode=approve';
      $body = wordwrap($body);
      $sender = 'noreply@' . $server;
      $headers = 'From: ' . $sender . "\r\n";
      mail($moderators, 'moderate message from ' . $server,
           $body, $headers, '-f ' . $sender);
    }
    return array('done' => true);
  }

  private function EditRegistry() {
    $object = array();
    $mysqli = connect_db();
    $type = $mysqli->escape_string($_POST['type']);
    $name = $mysqli->escape_string($_POST['name']);
    $message = $mysqli->escape_string($_POST['message']);

    if (!$this->user->canEditPage) {
      $object['error'] = 'Permission denied editing gift registry';
    }
    else if ($type === 'email-message') {
      $query = 'UPDATE gift_registry SET email_message = "' . $message . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Gift->EditRegistry 1: ' . $mysqli->error);
      }
      $object['content'] = 'Email message updated.';
    }
    else if ($type === 'moderators') {
      $query = 'UPDATE gift_registry SET moderators = "' . $message . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Gift->EditRegistry 2: ' . $mysqli->error);
      }
      $object['content'] = 'Moderators updated.';
    }
    else if ($type === 'gift-type') {
      // Firstly check for name formatting.
      if ($name === '') {
        $object['error'] = 'Gift type name is empty.';
      }
      else if (strpos($name, ' ') === false && strpos($name, '-') === false) {
        $title = $mysqli->escape_string($_POST['title']);
        $category = $mysqli->escape_string($_POST['category']);
        $image = $mysqli->escape_string($_POST['image']);
        $price = $mysqli->escape_string($_POST['price']);
        $claim_max = $mysqli->escape_string($_POST['claimMax']);
        $claim_count = $mysqli->escape_string($_POST['claimCount']);

        // Remove the record if the posted title is empty.
        if ($title === '') {
          $query = 'DELETE FROM gift_type WHERE name = "' . $name . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Gift->EditRegistry 3: ' . $mysqli->error);
          }
          $object['content'] = "Gift type '$name' removed.";
        }
        else {
          $query = 'INSERT INTO gift_type VALUES ("' . $name . '", ' .
            '"' . $category . '", "' . $title . '", "' . $message . '", ' .
            '"' . $image . '", ' . $price . ', "' . $claim_max . ', ' .
            $claim_count . ') ON DUPLICATE KEY UPDATE ' .
            'title = "' . $title . '", category = "' . $category . '", ' .
            'description = "' . $message . '", image = "' . $image . '", ' .
            'price = ' . $price . ', claim_max = ' . $claim_max . ', ' .
            'claim_count = ' . $claim_count;
          if (!$mysqli->query($query)) {
            $this->Log('Gift->EditRegistry 4: ' . $mysqli->error);
          }
          $object['content'] = "Gift type '$name' saved.";
        }
      }
      else {
        $object['error'] = 'Gift type name must be one word.';
      }
    }
    else {
      $object['error'] = "Unknown type '$type' editing gift registry";
    }
    $mysqli->close();
    return $object;
  }

  private function SelectionChange() {
    $gift_type = array();
    $mysqli = connect_db();
    $type = $mysqli->escape_string($_POST['type']);
    $query = 'SELECT name, category, title, description, image, price, ' .
      'claim_max, claim_count FROM gift_type WHERE name = "' . $type . '"';
    if ($result = $mysqli->query($query)) {
      $gift_type = $result->fetch_assoc();
      $result->close();
    }
    else {
      $this->Log('Gift->SelectionChange: ' . $mysqli->error);
    }
    $mysqli->close();
    return $gift_type;
  }

  private function ManageRegistry() {
    $gift_registry = array();
    $mysqli = connect_db();
    // Get existing content.
    $query = 'SELECT email_message, moderators FROM gift_registry';
    if ($result = $mysqli->query($query)) {
      $gift_registry = $result->fetch_assoc();
      $result->close();
    }
    else {
      $this->Log('Gift->ManageRegistry 1: ' . $mysqli->error);
    }

    $options = '<option value="">new gift</option>';
    if ($result = $mysqli->query('SELECT name FROM gift_type')) {
      while ($gift_type = $result->fetch_assoc()) {
        $name = $gift_type['name'];
        $options .= '<option value="' . $name . '">' . $name . '</option>';
      }
      $result->close();
    }
    else {
      $this->Log('Gift->ManageRegistry 2: ' . $mysqli->error);
    }
    $mysqli->close();

    return '<div class="manage-gifts">Manage your gifts</div>' .
      '<form id="email-message-form">' .
        '<div class="email-message">' .
          'This is the message that will be emailed to guests when they ' .
          'select a gift:' .
        '</div>' .
        '<textarea name="email-message" rows="5" cols="50">' .
          $gift_registry['email_message'] .
        '</textarea>' .
        '<button class="submit">submit</button>' .
        '<span class="manage-response email hidden"></span>' .
      '</form>' .
      '<form id="moderators-form">' .
        '<div class="moderators-message">' .
          'Moderator email addresses (comma separated):</div>' .
        '<label id="moderators-label" for="moderators-input">Email: ' .
        '</label>' .
        '<input id="moderators-input" type="text" name="moderators" ' .
          'value="' . $gift_registry['moderators'] .
          '" size="20"><br>' .
        '<button class="submit">submit</button>' .
        '<span class="manage-response moderators hidden"></span>' .
      '</form>' .
      '<div class="gift-type-message">' .
        'Select a new gift, or modify an existing one: ' .
        '<select id="gift-type-select">' . $options . '</select>' .
      '</div>' .
      '<form id="gift-type-form">' .
        '<label id="gift-type-name-label" for="gift-type-name-input">' .
          'Name: </label>' .
        '<input id="gift-type-name-input" type="text" ' .
          'name="gift-type-name" size="20" maxlength="50"><br>' .
        '<label id="gift-type-category-label" for="gift-type-category-input">' .
          'Category: </label>' .
        '<input id="gift-type-category-input" type="text" ' .
          'name="gift-type-category" size="20" maxlength="50"><br>' .
        '(Submit with a blank title to remove an entry)<br>' .
        '<label id="gift-type-title-label" for="gift-type-title-input">' .
          'Title: </label>' .
        '<input id="gift-type-title-input" type="text" ' .
          'name="gift-type-title" size="20" maxlength="100">' .
        '<div class="description-message">Description:</div>' .
        '<textarea name="description-message" rows="5" cols="50">' .
        '</textarea><br>' .
        '<label id="gift-type-image-label" for="gift-type-image-input">' .
          'Image: </label>' .
        '<input id="gift-type-image-input" type="text" ' .
          'name="gift-type-image" size="20" maxlength="100"><br>' .
        '<label id="gift-type-price-label" for="gift-type-price-input">' .
          'Price: </label>' .
        '<input id="gift-type-price-input" type="text" ' .
          'name="gift-type-price" size="10" maxlength="10"><br>' .
        '<label id="gift-type-claim-max-label" ' .
          'for="gift-type-claim-max-input">Claim max: </label>' .
        '<input id="gift-type-claim-max-input" type="text" ' .
          'name="gift-type-claim-max" size="10" maxlength="10"><br>' .
        '<label id="gift-type-claim-count-label" ' .
          'for="gift-type-claim-count-input">Claim count: </label>' .
        '<input id="gift-type-claim-count-input" type="text" value="0" ' .
          'name="gift-type-claim-count" size="10" maxlength="10"><br>' .
        '<button class="submit">submit</button>' .
        '<span class="manage-response gift hidden"></span>' .
      '</form>';
  }

  private function DisplayItem($mode, $claim_id, $claim_email) {
    $content = "";
    $mysqli = connect_db();
    $query = 'SELECT claim_name, claim_message, message_id, name ' .
      'FROM gift_item WHERE claim_id = "' . $claim_id . '" AND ' .
      'claim_email = "' . $claim_email . '"';
    if ($result = $mysqli->query($query)) {
      if ($gift_item = $result->fetch_assoc()) {
        $claim_name = htmlspecialchars($gift_item['claim_name']);
        $claim_message = $gift_item['claim_message'];
        $message_id = $gift_item['message_id'];
        $name = $gift_item['name'];

        $result->close();
        $query = 'SELECT title, description, image FROM gift_type ' .
          'WHERE name = "' . $name . '"';
        if ($result = $mysqli->query($query)) {
          if ($gift_type = $result->fetch_assoc()) {
            $result->close();

            $img = empty($gift_type['image']) ? '' :
              '<img src="' . $gift_type['image'] . '">';

            // Allow the user to release their claim on a gift.
            if ($mode === 'change') {
              $content = '<div class="display-item"><div class="claim" ' .
                'id="' . $claim_id . '">Hi ' . $claim_name .
                ', changed your mind? <button id="change-' . $name .
                '-button">change gift</button></div><div class="title">' .
                $gift_type['title'] . '</div><div class="reply"></div>' .
                '<div class="description">' . $img .
                $gift_type['description'] . '</div></div>';
            }
            else if ($mode === 'approve') {
              if ($_GET['message_id'] === $message_id) {
                $query = 'UPDATE gift_item SET message_approved = 1 ' .
                  'WHERE claim_id = "' . $claim_id . '"';
                if (!$mysqli->query($query)) {
                  $this->Log('Gift->DisplayItem 1: ' . $mysqli->error);
                }
                $content = '<div class="approved">' .
                  'Thankyou the message has been approved.</div>';
              }
            }
            else {
              // Otherwise displaying just the user's gift.
              $content = '<div class="display-item"><div class="title">' .
                $gift_type['title'] . '</div><div class="description">' . $img .
                $gift_type['description'] . '</div><div class="message">' .
                $claim_message . '</div><div class="claim-name">From ' .
                $claim_name . '</div></div>';
            }
          }
          else {
            $result->close();
          }
        }
        else {
          $this->Log('Gift->DisplayItem 2: ' . $mysqli->error);
        }
      }
      else {
        $result->close();
      }
    }
    else {
      $this->Log('Gift->DisplayItem 3: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function DisplayCategory($category = '') {
    $category_query = '';
    if ($category !== '') {
      $category_query = 'WHERE (category = "' . $category.'" OR category = "")';
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT gift_type.name AS name, title, description, image, ' .
      'claim_max, claim_count, claim_name, claim_message, message_approved ' .
      'FROM gift_type LEFT JOIN gift_item ON gift_type.name=gift_item.name ' .
      $category_query . ' ORDER BY price DESC';
    if ($result = $mysqli->query($query)) {
      $previous = '';
      $messages = '';
      while ($gift_item = $result->fetch_assoc()) {
        $name = $gift_item['name'];
        if ($previous !== $name) {
          // First add previous gift type's messages.
          if ($messages !== '') {
            $content .= '<div class="gift-messages">' . $messages . '</div>';
            $messages = '';
          }
          
          $previous = $name;
          $img = $gift_item['image'] === '' ? '' :
            '<img src="' . $gift_item['image'] . '">';
          $claim_count = $gift_item['claim_count'];
          $content .= '<div class="gift-type-display">' .
            '<div class="claim"><span class="claim-left">' .
              ($gift_item['claim_max'] - $claim_count) .
              '</span> available <button id="claim-' . $name .
              '-button">claim this</button></div>' .
            '<div class="title">' . $gift_item['title'] . '</div>' .
            '<form id="claim-' . $name . '-form" class="claim-form">' .
              '<label id="claim-' . $name . '-name-label" for="claim-' . $name .
                '-name-input">Name: ' .
              '</label>' .
              '<input id="claim-' . $name . '-name-input" type="text" ' .
                'name="claim-name" size="15" maxlength="50">' .
              '<label id="claim-' . $name . '-email-label" for="claim-' . $name.
                '-email-input"> Email: ' .
              '</label>' .
              '<input id="claim-' . $name . '-email-input" type="text" ' .
                'name="claim-email" size="15" maxlength="100"><br>' .
              '<div class="explain">Let everyone know why you chose this gift:'.
              '</div>' .
              '<textarea name="claim-message" rows="2" cols="40"></textarea>' .
              '<button class="submit">submit</button>' .
            '</form>' .
            '<div class="reply"></div>' .
            '<div class="description">' . $img . $gift_item['description'] .
            '</div>' .
            '</div>';
        }
        if ($gift_item['message_approved'] === '1') {
          $messages .= $gift_item['claim_name'] . ' chose this gift';
          $claim_message = $gift_item['claim_message'];
          $messages .= $claim_message === '' ? ' . <br>' :
            ', and said:<br>&quot;' . $claim_message . '&quot;<br>';
        }
      }
      $result->close();
      // And final messages for gift type if any.
      if ($messages !== '') {
        $content .= '<div class="gift-messages">' . $messages . '</div>';
      }
    }
    else {
      $this->Log('Gift->DisplayCategory: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function Messages() {
    $object = array();
    $mysqli = connect_db();
    $query = 'SELECT category, claim_name, claim_message FROM gift_type, ' .
      'gift_item WHERE gift_type.name=gift_item.name AND ' .
      'message_approved = 1 ORDER BY price';
    if ($result = $mysqli->query($query)) {
      while ($gift_item = $result->fetch_assoc()) {
        if ($object[$gift_item['category']]) continue;
        $object[$gift_item['category']] = $gift_item['claim_name']." says: '".
          $gift_item["claim_message"]."'";
      }
      $result->close();
    }
    else {
      $this->Log('Gift->Messages: ' . $mysqli->error);
    }
    $mysqli->close();
    return $object;
  }

}
