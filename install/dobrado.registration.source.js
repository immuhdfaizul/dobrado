/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2015 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.registration) {
  dobrado.registration = {};
}
(function() {

  'use strict';
  // Remember the previously selected organisation to hide it's groups.
  var prevOrganisation = "";

  $(function() {
    // Don't run if the module isn't on the page.
    if ($(".registration").length === 0) {
      return;
    }

    $("#registration-organisation").change(showGroup);
    $("#registration-username").change(checkUsername);
    $("#registration-password").change(checkPassword);
    $("#registration-password-confirm").change(checkPasswordConfirm);
    $(".registration-form .submit").button().click(submit);
    showGroup();
  });

  function checkUsername() {
    if ($("#registration-username").val() === "") {
      return;
    }
    $(".registration-form .username-check").html("Checking username...");
    $.post("/php/request.php", { request: "registration",
                                 mode: "check",
                                 username: $("#registration-username").val(),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "registration check")) {
          $(".registration-form .username-check").
            html("There was a problem checking your username.").show();
        }
        var registration = JSON.parse(response);
        $(".registration-form .username-check").html(registration.check).show();
      });
  }

  function checkPassword() {
    var password = $("#registration-password").val();
    if (password === "") {
      $(".registration-form .password-check").
        html("Please provide a password.").show();
      return;
    }

    var confirm = $("#registration-password-confirm").val();
    if (confirm !== "" && password !== confirm) {
      $(".registration-form .password-check").
        html("Passwords do not match.").show();
    }
    else {
      $(".registration-form .password-check").html("").hide();
    }
  }

  function checkPasswordConfirm() {
    var password = $("#registration-password").val();
    if (password === "") {
      $(".registration-form .password-check").
        html("Please provide a password.").show();
      return;
    }

    var confirm = $("#registration-password-confirm").val();
    if (confirm === "") {
      $(".registration-form .password-check").
        html("Please confirm your password.").show();
      return;
    }

    if (password == confirm) {
      $(".registration-form .password-check").html("").hide();
    }
    else {
      $(".registration-form .password-check").
        html("Passwords do not match.").show();
    }
  }

  function showGroup() {
    // First hide the groups of the previous organisation.
    $("#" + prevOrganisation + "-groups").parent().hide();
    prevOrganisation = $("#registration-organisation").val();
    $("#" + prevOrganisation + "-groups").parent().show();
  }

  function submit() {
    var organisation = $("#registration-organisation").val();
    var group = "";

    if (organisation) {
      group = $("#" + organisation + "-groups").val();
    }
    else {
      var match = null;
      organisation = "";
      // When the organisation select isn't used, expect to find a select in
      // the registration form with an id of <organisation>-groups.
      $(".registration-form select").each(function() {
        match = $(this).attr("id").match(/^(.*)-groups$/);
        if (match && match.length === 2) {
          organisation = match[1];
          group = $(this).val();
          return false;
        }
      });
    }

    $(".registration-form .submit").button("option", "disabled", true);
    $.post("/php/request.php",
           { request: "registration",
             mode: "submit",
             organisation: organisation,
             group: group,
             username: $("#registration-username").val(),
             password: $("#registration-password").val(),
             email: $("#registration-email").val(),
             first: $("#registration-first").val(),
             last: $("#registration-last").val(),
             phone: $("#registration-phone").val(),
             address: $("#registration-address").val(),
             description: $("#registration-description").val(),
             url: location.href,
             token: dobrado.token },
      function(response) {
        $(".registration-form .submit").button("option", "disabled", false);
        if (dobrado.checkResponseError(response, "registration check")) {
          $(".registration-form .registration-info").
            html("There was a problem creating your account please try again.").
            show();
          return false;
        }
        var registration = JSON.parse(response);
        $(".registration-form .registration-info").html(registration.content).
          show();
      });
    return false;
  }

})();
