<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Detail extends Base {

  public function Add($id) {
    if ($this->user->page === 'index') {
      $mysqli = connect_db();
      $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
        '"comment-notifications-index", "' . $this->owner . '", 0)';
      if (!$mysqli->query($query)) {
        $this->Log('Detail->Add: ' . $mysqli->error);
      }
      $mysqli->close();
    }
  }

  public function Callback() {
    if (!$this->user->canEditPage) {
      return ['error' => 'Permission denied editing user details.'];
    }

    $mysqli = connect_db();
    $first = $mysqli->escape_string(htmlspecialchars($_POST['first']));
    $last = $mysqli->escape_string(htmlspecialchars($_POST['last']));
    $thumb = $mysqli->escape_string(htmlspecialchars($_POST['thumb']));
    $phone = $mysqli->escape_string(htmlspecialchars($_POST['phone']));
    $address = $mysqli->escape_string(htmlspecialchars($_POST['address']));
    $description = '';
    if (isset($_POST['description'])) {
      include_once 'library/HTMLPurifier.auto.php';
      $config = HTMLPurifier_Config::createDefault();
      $config->set('Attr.AllowedRel', ['me']);
      $config->set('HTML.DefinitionID', 'dobrado-detail');
      $config->set('HTML.DefinitionRev', 1);
      $purifier = new HTMLPurifier($config);
      $us_description = $purifier->purify($_POST['description']);
      $description = $mysqli->escape_string($us_description);
    }
    $follow = $mysqli->escape_string(htmlspecialchars($_POST['follow']));
    $action = $mysqli->escape_string(htmlspecialchars($_POST['action']));
    $query = 'INSERT INTO user_detail VALUES ("' . $this->user->name . '", ' .
      '"' . $first . '", "' . $last . '", "' . $thumb . '", ' .
      '"' . $phone . '", "' . $address . '", "' . $description . '", ' .
      '"' . $follow . '", "' . $action . '", 1, 0, "", "", "", 0) ' .
      'ON DUPLICATE KEY UPDATE first = "' . $first . '", ' .
      'last = "' . $last . '", thumbnail = "' . $thumb . '", ' .
      'phone = "' . $phone . '", address = "' . $address . '", ' .
      'description = "' . $description . '", follow = "' . $follow . '", ' .
      'action = "' . $action . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Detail->Callback: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function CanAdd($page) {
    // Can only have one detail module on a page.
    return !$this->AlreadyOnPage('detail', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $email = '';
    $full_name = '';
    $first = '';
    $last = '';
    $thumbnail = '';
    $phone = '';
    $address = '';
    $description = '';
    $follow = '';
    $action = '';
    $img = '<img class="thumb u-photo" src="/images/default_thumb.jpg">';

    $mysqli = connect_db();
    $query = 'SELECT email, first, last, thumbnail, phone, address, ' .
      'description, follow, action FROM users LEFT JOIN user_detail ON ' .
      'users.user = user_detail.user WHERE users.user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($detail = $mysqli_result->fetch_assoc()) {
        // email is the only value here that is not escaped when stored.
        $email = htmlspecialchars($detail['email']);
        $first = $detail['first'];
        $last = $detail['last'];
        $phone = $detail['phone'];
        $address = $detail['address'];
        $description = $detail['description'];
        $follow = $detail['follow'];
        $action = $detail['action'];
        $thumbnail = $detail['thumbnail'];
        if (preg_match('/^(.+)\.(.+)$/', $thumbnail, $match)) {
          $name = $match[1];
          $type = $match[2];
          if (in_array($type, ['gif', 'jpeg', 'jpg', 'png'])) {
            $name .= '_thumb.' . $type;
            $img = '<img class="thumb u-photo" src="' . $name . '">';
          }
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->Content 1: ' . $mysqli->error);
    }
    // Can only access $this->user->settings when logged in so look up the
    // values required here in the settings table.
    $username_set = false;
    $follow_name = $this->owner;
    $query = 'SELECT value FROM settings WHERE user = "' . $this->owner . '" ' .
      'AND label = "account" AND name = "username"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($settings = $mysqli_result->fetch_assoc()) {
        $follow_name = $settings['value'];
        $username_set = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->Content 2: ' . $mysqli->error);
    }
    $microsub = '';
    if ($this->Substitute('reader-microsub-client') === 'true') {
      $query = 'SELECT value FROM settings WHERE ' .
        'user = "' . $this->owner . '" AND label = "reader" AND ' .
        'name = "microsub"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($settings = $mysqli_result->fetch_assoc()) {
          $microsub = $settings['value'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Detail->Content 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    if ($first !== '') {
      $full_name = $first;
    }
    if ($last !== '') {
      if ($full_name !== '') $full_name .= ' ';
      $full_name .= $last;
    }
    $nickname = '';
    if ($username_set) {
      // Use the user's preferred username as a nickname in their h-card.
      $nickname = '@<span class="p-nickname">' . $follow_name . '</span>';
    }
    else if ($this->Substitute('account-single-user') === 'true') {
      // Don't want to use 'admin' as the follow name in single user mode.
      $follow_name = $this->user->config->ServerName();
    }
    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $this->user->config->ServerName();
    $url = $base_url;
    if ($this->owner !== 'admin') $url .= '/' . $this->owner;
    $fancy = $this->user->config->FancyUrl();
    $action_content = '';
    // follow is a list of comma separated pages that the user wants to display
    // with a follow webaction in their h-card.
    $follow_list = explode(',', $follow);
    $follow_count = count($follow_list);
    for ($i = 0; $i < $follow_count; $i++) {
      $page = trim($follow_list[$i]);
      if ($page === '') continue;

      $with = $fancy ? $url . '/' . $page : $url . '/index.php?page=' . $page;
      // If there's only one action just show follow name.
      $follow_page = $follow_name;
      if ($follow_count > 1) $follow_page .= '/' . $page;
      $action_content .= '<div class="detail-web-actions">' .
        '<indie-action do="follow" with="' . $with . '">' .
          '<a href="#" class="action">follow ' . $follow_name . '</a>' .
        '</indie-action>' .
        '<span class="indie-config-follow-info">' .
          'To follow <a href="' . $with . '" rel="feed">' . $follow_name .
          '</a> add a reader:' .
        '</span>';
      if ($i === 0) {
        $action_content .= '<a href="#" class="indie-config" ' .
            'title="web action settings">' .
          '<span class="ui-icon ui-icon-gear"></span>settings</a>';
      }
      $action_content .= '</div>';
    }
    $patterns = ['/!name/', '/!nickname/', '/!url/', '/!email/', '/!photo/',
                 '/!note/', '/!actions/'];
    $replacements = [$full_name, $nickname, $url, $email, $img, $description,
                     $action_content];
    $content = $this->Substitute('detail-h-card', $patterns, $replacements);
    // If detail-h-card contains a rel=me link to the user's home page,
    // advertise all supported endpoints.
    $regex = '/<a[^>]+href="' . preg_quote($url, '/') . '" rel="me">/';
    if (preg_match($regex, $content)) {
      header('Link: <' . $base_url . '/php/webmention.php>; rel="webmention"');
      if ($action !== '') {
        header('Link: <' . $base_url . '/php/webaction.php?username=' .
               $this->owner . '&page=' . $action . '>; rel="webaction"', false);
      }
      header('Link: <' . $base_url . '/php/micropub.php>; rel="micropub"',
             false);
      if (strpos($microsub, 'http') === 0) {
        header('Link: <' . $microsub . '>; rel="microsub"', false);
      }
      else {
        header('Link: <' . $base_url . '/php/microsub.php>; rel="microsub"',
               false);
      }
      header('Link: <' . $base_url . '/php/auth_endpoint.php>; ' .
               'rel="authorization_endpoint"', false);
      header('Link: <' . $base_url . '/php/token_endpoint.php>; ' .
               'rel="token_endpoint"', false);
      // Also add rel=feed's for designated actions from Writer module. These
      // are required for bridgy to do post discovery from syndication links.
      $writer = new Module($this->user, $this->owner, 'writer');
      if ($writer->IsInstalled()) {
        foreach ($writer->Factory('AllDesignated', $follow) as $designated) {
          $content .= '<a href="' . $url . '/index.php?page=' .
            $designated . '" rel="feed"></a>';
        }
      }
    }

    // Make sure only the owner can see other details.
    if ($this->user->name === $this->owner) {
      $description_input = '';
      $label = $this->Substitute('detail-description-label');
      if ($label !== '') {
        $description_input = '<div class="form-spacing">' .
            '<label for="detail-description-textarea">' . $label . ':</label>' .
            '<textarea id="detail-description-textarea">' . $description .
            '</textarea>' .
          '</div>';
      }
      $content .= '<div class="edit"><a href="#">Edit your details</a></div>' .
        '<form id="detail-form" class="hidden">' .
          '<div class="form-spacing">' .
            '<label for="detail-first-input">First Name:</label>' .
            '<input id="detail-first-input" value="' . $first . '" ' .
              'type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="detail-last-input">Last Name:</label>' .
            '<input id="detail-last-input" value="' . $last . '" type="text"' .
              'maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="detail-thumb-input">Picture:</label>' .
            '<input id="detail-thumb-input" value="' . $thumbnail . '" ' .
              'type="text" maxlength="200"> ' .
            '<button class="detail-thumb-browse">browse</button>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="detail-follow-input">Follow:</label>' .
            '<input id="detail-follow-input" value="' . $follow . '" ' .
              'type="text" maxlength="200">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="detail-action-input">Web Actions:</label>' .
            '<input id="detail-action-input" value="' . $action . '" ' .
              'type="text" maxlength="200">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="detail-phone-input">Phone:</label>' .
            '<input id="detail-phone-input" value="' . $phone . '" ' .
              'type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="detail-address-textarea">Address:</label>' .
            '<textarea id="detail-address-textarea">' . $address .
            '</textarea>' .
          '</div>' .
          $description_input .
          '<button class="submit">Submit</button>' .
        '</form>';
    }
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    if ($new_page === 'index') {
      $mysqli = connect_db();
      $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
        '"comment-notifications-index", "' . $this->owner . '", 0)';
      if (!$mysqli->query($query)) {
        $this->Log('Detail->Copy: ' . $mysqli->error);
      }
      $mysqli->close();
    }
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'UpdateField' && $count === 3) {
        $user = $p[0];
        $field = $p[1];
        $value = $p[2];
        return $this->UpdateField($user, $field, $value);
      }
      // Account module calls UpdateUser through module interface.
      if ($fn === 'UpdateUser' && $count >= 4) {
        $user = $p[0];
        $first = $p[1];
        $last = $p[2];
        $thumbnail = '';
        $phone = '';
        $address = '';
        $description = '';
        $display = 1;
        $update_all = false;
        $reminder_time = 0;
        $reminder_repeat = '';
        $supplier_only = 0;
        if ($count === 4) {
          $thumbnail = $p[3];
        }
        else if ($count >= 8) {
          $phone = $p[3];
          $address = $p[4];
          $description = $p[5];
          $display = $p[6];
          $update_all = $p[7];
          if ($count >= 10) {
            $reminder_time = $p[8];
            $reminder_repeat = $p[9];
            if ($count === 11) $supplier_only = $p[10];
          }
        }
        return $this->UpdateUser($user, $first, $last, $thumbnail, $phone,
                                 $address, $description, $display, $update_all,
                                 $reminder_time, $reminder_repeat,
                                 $supplier_only);
      }
      return;
    }
    if ($fn === 'User') {
      return $this->User($p);
    }
    if ($fn === 'Follow') {
      return $this->Follow();
    }
    if ($fn === 'Action') {
      return $this->Action();
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.detail.js to the existing dobrado.js file.
    // Note that updating the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.detail.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS user_detail (' .
      'user VARCHAR(50) NOT NULL,' .
      'first VARCHAR(50),' .
      'last VARCHAR(50),' .
      'thumbnail VARCHAR(200),' .
      'phone VARCHAR(50),' .
      'address TEXT,' .
      'description TEXT,' .
      'follow VARCHAR(200),' .
      'action VARCHAR(200),' .
      'display TINYINT(1),' .
      'reminder_time INT(10) UNSIGNED,' .
      'reminder_repeat ENUM("6 months", "12 months", ""),' .
      'delivery_group VARCHAR(200),' .
      'notes TEXT,' .
      'supplier_only TINYINT(1),' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Detail->Install 1: ' . $mysqli->error);
    }
    $query = 'INSERT INTO user_detail VALUES ("admin", "", "", "", "", "", ' .
      '"", "", "", 1, 0, "", "", "", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Detail->Install 2: ' . $mysqli->error);
    }
    $site_style = ['"",".detail","padding","10px"',
                   '"",".detail a","text-decoration","none"',
                   '"",".detail a","color","#222222"',
                   '"",".detail a:hover","color","#aaaaaa"',
                   '"",".detail .edit","margin-top","10px"',
                   '"",".detail .edit a","font-size","0.8em"',
                   '"","#detail-form label","width","7em"',
                   '"","#detail-form .submit","margin-left","7.3em"',
                   '"","#detail-thumb-input","width","200px"',
                   '"",".detail-web-actions","margin","15px 0 15px 0"'];
    $this->AddSiteStyle($site_style);
    $h_card = '<div class="h-card">!photo<br>' .
      '<a class="p-name u-url" href="!url" rel="me">!name</a><br>' .
      '<a class="u-email" href="mailto:!email" rel="me">!email</a><br>' .
      '<p class="p-note">!note</p>!actions</div>';
    $this->AddTemplate(['"detail-h-card", "", ' .
                          '"' . $mysqli->escape_string($h_card) . '"']);
    $mysqli->close();

    $description = ['detail-h-card' => 'An HTML template to display your ' .
                      'h-card from the details provided in the User Details ' .
                      'form. Can substitute: !name, !nickname, !url, !email, ' .
                      '!photo, !note and !actions'];
    $this->AddTemplateDescription($description);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      if ($this->user->page === 'index') {
        $query = 'DELETE FROM group_names WHERE user = "' . $this->owner . '" '.
          'AND name = "comment-notifications-index"';
        if (!$mysqli->query($query)) {
          $this->Log('Detail->Remove: ' . $mysqli->error);
        }
      }
    }
    else {
      // When id is not set, deleting an account.
      $query = 'DELETE FROM user_detail WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Detail->Remove: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.detail.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.detail.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  public function AllSuppliers() {
    $stock = new Module($this->user, $this->owner, 'stock');
    // TODO: Need to pass a list of relevant groups in here for a multi-group
    // buy, which means creating a list from Invite module.
    $all_suppliers = $stock->IsInstalled() ?
      $stock->Factory('AllSuppliers') : [];
    $count = count($all_suppliers);
    if ($count === 0) return [];

    $user_query = '';
    for ($i = 0; $i < $count; $i++) {
      if ($user_query !== '') {
        $user_query .= ' OR ';
      }
      $user_query .= 'users.user = "' . $all_suppliers[$i] . '"';
    }
    $all_suppliers = [];
    $mysqli = connect_db();
    $query = 'SELECT users.user, first, last FROM users LEFT JOIN ' .
      'user_detail ON users.user = user_detail.user WHERE ' . $user_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($detail = $mysqli_result->fetch_assoc()) {
        $all_suppliers[$detail['user']] = ['first' => $detail['first'],
                                           'last' => $detail['last']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->AllSuppliers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $all_suppliers;
  }

  public function AllUsers($organisation = false, $invited = false) {
    $query = '';
    $invite = new Module($this->user, $this->owner, 'invite');
    if ($invite->IsInstalled()) {
      $joined = $invite->Factory('Joined');
      // Only want to return the members that have joined a multi-group buy when
      // invited is true, otherwise this function couldn't return the actual
      // members of the host group.
      if ($invited && count($joined) !== 0) {
        $group_query = '';
        for ($i = 0; $i < count($joined); $i++) {
          if ($group_query !== '') {
            $group_query .= ' OR ';
          }
          $group_query .= 'users.system_group = "' . $joined[$i] . '"';
        }
        // Add the host group too.
        $group_query .= ' OR users.system_group = "' . $this->user->group . '"';
        // Ignore point of sale accounts which look like: buyer_<timestamp>.
        $query = 'SELECT users.user, email, system_group, active, first, ' .
          'last, thumbnail, phone, address, description, display, ' .
          'reminder_time, reminder_repeat, delivery_group, notes, ' .
          'supplier_only FROM users LEFT JOIN user_detail ON ' .
          'users.user = user_detail.user WHERE ' .
          'users.user NOT LIKE "buyer\_%" AND (' . $group_query . ')';
      }
    }
    if ($query === '') {
      if ($organisation) {
        $organiser = new Organiser($this->user, $this->owner);
        $query = 'SELECT users.user, email, system_group, active, first, ' .
          'last, thumbnail, phone, address, description, display, ' .
          'reminder_time, reminder_repeat, delivery_group, notes, ' .
          'supplier_only FROM users LEFT JOIN user_detail ON ' .
          'users.user = user_detail.user WHERE ' .
          'users.user NOT LIKE "buyer\_%" AND ' . $organiser->GroupQuery();
      }
      else {
        $query = 'SELECT users.user, email, system_group, active, first, ' .
          'last, thumbnail, phone, address, description, display, ' .
          'reminder_time, reminder_repeat, delivery_group, notes, ' .
          'supplier_only FROM users LEFT JOIN user_detail ON ' .
          'users.user = user_detail.user WHERE ' .
          'users.user NOT LIKE "buyer\_%" AND ' .
          'users.system_group = "' . $this->user->group . '"';
      }
    }
    $all_users = [];
    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($detail = $mysqli_result->fetch_assoc()) {
        $img = '<img class="thumb" src="/images/default_thumb.jpg">';
        if (preg_match('/^(.+)\.(.+)$/', $detail['thumbnail'], $match)) {
          $name = $match[1];
          $type = $match[2];
          if (in_array($type, ['gif', 'jpeg', 'jpg', 'png'])) {
            $name .= '_thumb.' . $type;
            $img = '<img class="thumb" src="' . $name . '">';
          }
        }
        $all_users[$detail['user']] =
          ['first' => $detail['first'], 'last' => $detail['last'],
           'thumbnail' => $img, 'phone' => $detail['phone'],
           'address' => $detail['address'],
           'description' => $detail['description'],
           'display' => $detail['display'] === '1',
           'active' => $detail['active'] === '1',
           'reminderTime' => (int)$detail['reminder_time'],
           'reminderRepeat' => $detail['reminder_repeat'],
           'delivery' => $detail['delivery_group'],
           'notes' => $detail['notes'],
           'supplierOnly' => $detail['supplier_only'] === '1',
           'email' => htmlspecialchars($detail['email']),
           'group' => $detail['system_group']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->AllUsers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $all_users;
  }

  public function Follow() {
    $follow = '';
    $mysqli = connect_db();
    $query = 'SELECT follow FROM user_detail WHERE user = "' . $this->owner.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($user_detail = $mysqli_result->fetch_assoc()) {
        $follow = $mysqli->escape_string($user_detail['follow']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->Follow: ' . $mysqli->error);
    }
    $mysqli->close();
    return $follow;
  }

  public function MembershipReminder($user) {
    $reminder_time = 0;
    $reminder_repeat = '';
    $mysqli = connect_db();
    $query = 'SELECT reminder_time, reminder_repeat FROM user_detail ' .
      'WHERE user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($user_detail = $mysqli_result->fetch_assoc()) {
        $reminder_time = (int)$user_detail['reminder_time'];
        $reminder_repeat = $user_detail['reminder_repeat'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->MembershipReminder 1: ' . $mysqli->error);
    }
    $mysqli->close();
    // Return false if values aren't set for this user.
    if ($reminder_time === 0 || $reminder_repeat === '') return false;

    // Check if membership is due in the next two weeks.
    return $reminder_time < time() + (86400 * 14);
  }

  // This function is used by the Stock module to provide import options.
  public function SupplierOnly() {
    $suppliers = [];
    $organiser = new Organiser($this->user, $this->owner);
    $query = 'SELECT users.user, first, last FROM users LEFT JOIN user_detail '.
      'ON users.user = user_detail.user WHERE supplier_only = 1 AND ' .
      $organiser->GroupQuery();

    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($user_detail = $mysqli_result->fetch_assoc()) {
        $user = $user_detail['user'];
        $name = $user_detail['first'];
        $last = $user_detail['last'];
        if ($name === '') {
          $name = $user;
        }
        else if ($last !== '') {
          $name .= ' ' . $last;
        }
        $suppliers[$user] = $name;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->SupplierOnly: ' . $mysqli->error);
    }
    $mysqli->close();
    return $suppliers;
  }

  public function UpdateField($user, $field, $value) {
    $mysqli = connect_db();
    $query = 'UPDATE user_detail SET ' . $field . ' = "' . $value . '" WHERE ' .
      'user = "' . $user . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Detail->UpdateField: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function UpdateUser($user, $first, $last, $thumbnail = '', $phone = '',
                             $address = '', $description = '', $display = 1,
                             $update_all = false, $reminder_time = 0,
                             $reminder_repeat = '', $supplier_only = 0) {
    $query = '';
    $mysqli = connect_db();
    if ($update_all) {
      // Don't update supplier_only as it's not displayed in the form.
      $query = 'INSERT INTO user_detail VALUES ("' . $user . '", ' .
        '"' . $first . '", "' . $last . '", "' . $thumbnail . '", ' .
        '"' . $phone . '", "' . $address . '", "' . $description . '", ' .
        '"", "", ' . $display . ', ' . $reminder_time . ', ' .
        '"' . $reminder_repeat . '", "", "", ' . $supplier_only . ') ' .
        'ON DUPLICATE KEY UPDATE first = "' . $first . '", ' .
        'last = "' . $last . '", phone = "' . $phone . '", ' .
        'address = "' . $address . '", description = "' . $description . '", ' .
        'display = ' . $display . ', reminder_time = ' . $reminder_time . ', ' .
        'reminder_repeat = "' . $reminder_repeat . '"';
    }
    else {
      $update_query = '';
      if ($thumbnail !== '') {
        $update_query .= ', thumbnail = "' . $thumbnail . '"';
      }
      if ($phone !== '') {
        $update_query .= ', phone = "' . $phone . '"';
      }
      $query = 'INSERT INTO user_detail VALUES ("' . $user . '", ' .
        '"' . $first . '", "' . $last . '", "' . $thumbnail . '", ' .
        '"' . $phone . '", "' . $address . '", "' . $description . '", ' .
        '"", "", ' . $display . ', ' . $reminder_time . ', ' .
        '"' . $reminder_repeat . '", "", "", 0) ' .
        'ON DUPLICATE KEY UPDATE first = "' . $first . '", ' .
        'last = "' . $last . '"' . $update_query;
    }
    if (!$mysqli->query($query)) {
      $this->Log('Detail->UpdateUser: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function User($us_user = '') {
    $user = '';
    $email = '';
    $first = '';
    $last = '';
    $img = '';
    $phone = '';
    $address = '';
    $description = '';
    $display = true;
    $reminderTime = 0;
    $reminderRepeat = '';
    $supplierOnly = false;

    $mysqli = connect_db();
    if ($us_user === '' || $us_user === NULL) {
      $user = $this->user->name;
    }
    else {
      $user = $mysqli->escape_string($us_user);
    }
    $query = 'SELECT email, first, last, thumbnail, phone, address, ' .
      'description, display, reminder_time, reminder_repeat, supplier_only ' .
      'FROM users LEFT JOIN user_detail ON users.user = user_detail.user ' .
      'WHERE users.user = "' . $user . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($detail = $mysqli_result->fetch_assoc()) {
        $img = '<img class="thumb u-photo" src="/images/default_thumb.jpg">';
        // Support the SimplePie image cache too.
        $regex = '/^\/php\/image.php\?i=[[:xdigit:]]+$/';
        if (preg_match($regex, $detail['thumbnail'])) {
          $img = '<img class="thumb u-photo" src="' . $detail['thumbnail'].'">';
        }
        else if (preg_match('/^(.+)\.(.+)$/', $detail['thumbnail'], $match)) {
          $name = $match[1];
          $type = $match[2];
          if (in_array($type, ['gif', 'jpeg', 'jpg', 'png'])) {
            $name .= '_thumb.' . $type;
            $img = '<img class="thumb u-photo" src="' . $name . '">';
          }
        }
        $email = htmlspecialchars($detail['email']);
        $first = $detail['first'];
        $last = $detail['last'];
        $phone = $detail['phone'];
        $address = $detail['address'];
        $description = $detail['description'];
        $display = $detail['display'] === '1';
        $reminderTime = $detail['reminder_time'];
        $reminderRepeat = $detail['reminder_repeat'];
        $supplierOnly = $detail['supplier_only'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->User: ' . $mysqli->error);
    }
    $mysqli->close();

    return ['email' => $email, 'first' => $first, 'last' => $last,
            'thumbnail' => $img, 'phone' => $phone, 'address' => $address,
            'description' => $description, 'display' => $display,
            'reminderTime' => $reminderTime,
            'reminderRepeat' => $reminderRepeat,
            'supplierOnly' => $supplierOnly];
  }

  // Private functions below here ////////////////////////////////////////////

  private function Action() {
    $action = '';
    $mysqli = connect_db();
    $query = 'SELECT action FROM user_detail WHERE user = "' . $this->owner.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($user_detail = $mysqli_result->fetch_assoc()) {
        $action = $mysqli->escape_string($user_detail['action']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Detail->Action: ' . $mysqli->error);
    }
    $mysqli->close();
    return $action;
  }

}
