<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class BrowserTest extends TestCase {

  private $browser = NULL;
  private $user = NULL;

  protected function setUp() {
    $this->user = new User();
    $this->browser = new Browser($this->user, 'admin');
  }

  public function testCanAdd() {
    $this->assertTrue($this->browser->CanAdd(''));
  }

  public function testCanEdit() {
    $this->assertFalse($this->browser->CanEdit(0));
  }

  public function testCanRemove() {
    $this->assertFalse($this->browser->CanRemove(0));
  }

  public function testContent() {
    if (!$this->user->loggedIn) {
      $this->assertEquals($this->browser->Content(0), '');
    }
  }

  public function testIncludeScript() {
    $this->assertTrue($this->browser->IncludeScript());
  }

  public function testPlacement() {
    $this->assertEquals($this->browser->Placement(), 'outside');
  }

}
