<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

session_start();

function micropub_config($endpoint, $token) {
  $config = '{}';
  $endpoint .= strpos($endpoint, '?') === false ? '?q=config' : '&q=config';
  $curl_headers = ['Authorization: Bearer ' . $token,
                   'Accept: application/json'];

  $ch = curl_init($endpoint);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  curl_setopt($ch, CURLOPT_ENCODING, '');
  curl_setopt($ch, CURLOPT_HEADER, false);
  log_db('micropub_config 1: curl ' . $endpoint);
  $body = curl_exec($ch);
  if (curl_errno($ch) === 0) {
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($http_code === 200) {
      $config = $body;
    }
    else {
      log_db('micropub_config 2: Error getting ' . $endpoint .
             "\nHTTP code: " . $http_code . "\nBody: " . $body);
    }
  }
  else {
    log_db('micropub_config 3: Error connecting to ' . $endpoint .
           "\nCurl error: " . curl_error($ch));
  }
  curl_close($ch);
  return $config;
}

function verify_code($endpoint, $post_fields, $token = false) {
  $check_me = '';
  $ch = curl_init($endpoint);
  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  curl_setopt($ch, CURLOPT_ENCODING, '');
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
  log_db('verify_code 1: curl ' . $endpoint);
  $body = curl_exec($ch);
  if (curl_errno($ch) === 0) {
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($http_code === 200) {
      if ($json = json_decode($body, true)) {
        if (isset($json['me'])) $check_me = $json['me'];
        $_SESSION['access-token'] = isset($json['access_token']) ?
          $json['access_token'] : NULL;
      }
      // Check if the body was returned as form encoded if not json.
      else if (preg_match('/me=([^&]+)/', $body, $match)) {
        $check_me = urldecode($match[1]);
        if (preg_match('/access_token=([^&]+)/', $body, $match)) {
          $_SESSION['access-token'] = urldecode($match[1]);
        }
      }
      if ($check_me === '') {
        log_db('verify_code 2: \'me\' property not found at ' . $endpoint .
               "\nBody: " . $body);
      }
      else if ($token && !isset($_SESSION['access-token'])) {
        log_db('verify_code 3: Access token not found at ' . $endpoint .
               "\nBody: " . $body);
      }
    }
    else {
      log_db('verify_code 4: Error posting to ' . $endpoint .
             "\nHTTP code: " . $http_code . "\nBody: " . $body);
    }
  }
  else {
    log_db('verify_code 5: Error connecting to ' . $endpoint .
           "\nCurl error: " . curl_error($ch));
  }
  curl_close($ch);
  return $check_me;
}

$domain = $_SERVER['SERVER_NAME'];
$url = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
  'https://' . $domain : 'http://' . $domain;

if (!isset($_GET['code']) || !isset($_GET['state'])) {
  header('HTTP/1.1 401 Unauthorised');
  echo '<html><h2>401 Unauthorised</h2>' .
    '<p>GET parameters: \'code\', and \'state\' must be set.</p>' .
    '<p>Return to <a href="' . $url . '">' . $domain . '</a></p></html>';
  exit;
}
if (!isset($_SESSION['indieauth-input']) ||
    !isset($_SESSION['indieauth-state'])) {
  header('HTTP/1.1 401 Unauthorised');
  echo '<html><h2>401 Unauthorised</h2>' .
    '<p>There was an error checking your address. Please try again.</p>' .
    '<p>Return to <a href="' . $url . '">' . $domain . '</a></p></html>';
  exit;
}

$us_domain = strtolower(trim($_SESSION['indieauth-input'], ' /'));
if (preg_match('/^https?:\/\/(.+)$/', $us_domain, $match)) {
  $us_domain = $match[1];
}

$error = '';
if (strlen($us_domain) > 50) {
  $error = '<p>Your url is too long to be used as a log in for ' .
    'this site. It must be less than 50 characters.</p>';
}
else if (strpos($us_domain, '.') === false) {
  $error = '<p>The url you provided doesn\'t look like a domain.</p>';
}
else if ($_SESSION['indieauth-state'] !== $_GET['state']) {
  $error = '<p>The \'state\' parameter: ' . $_GET['state'] .
    ' doesn\'t match the value originally set.</p>';
}
if ($error !== '') {
  header('HTTP/1.1 401 Unauthorised');
  echo '<html><h2>401 Unauthorised</h2>' . $error .
    '<p>Return to <a href="' . $url . '">' . $domain . '</a></p></html>';
  exit;
}

include 'functions/db.php';

$check_me = '';
$code = $_GET['code'];
if (isset($_SESSION['token-endpoint']) && $_SESSION['token-endpoint'] !== '') {
  $post_fields = 'me=' . urlencode($_SESSION['indieauth-input']) .
    '&code=' . urlencode($code) .
    '&redirect_uri=' . urlencode($url . '/php/auth.php') .
    '&client_id=' . urlencode($url . '/') . '&grant_type=authorization_code';
  $check_me = verify_code($_SESSION['token-endpoint'], $post_fields, true);
}
else if (isset($_SESSION['authorization-endpoint']) &&
         $_SESSION['authorization-endpoint'] !== '') {
  $post_fields = 'code=' . urlencode($code) . '&redirect_uri=' .
    urlencode($url . '/php/auth.php') . '&client_id=' . urlencode($url . '/');
  $check_me = verify_code($_SESSION['authorization-endpoint'], $post_fields);
}
if (preg_match('/^https?:\/\/(.+)$/', $check_me, $match)) {
  $check_me = trim($match[1], ' /');
}
if ($check_me === '' || strpos($check_me, $us_domain) !== 0) {
  header('HTTP/1.1 403 Forbidden');
  echo '<html><h2>403 Forbidden</h2>' .
    '<p>Couldn\'t verify the authorization code provided.</p>' .
    '<p>Return to <a href="' . $url . '">' . $domain . '</a></p></html>';
  exit;
}

include 'config.php';
include 'module.php';
include 'user.php';

// Switch slashes for underscores so that $us_domain can be used as a regular
// username in paths if required. Also switch to the returned me value in case
// it's been modified by the authorization endpoint.
$us_domain = preg_replace('/\//', '_', $check_me);

$exists = false;
$mysqli = connect_db();
$domain = $mysqli->escape_string($us_domain);
$query = 'SELECT user FROM users WHERE user = "' . $domain . '"';
if ($result = $mysqli->query($query)) {
  if ($result->num_rows === 1) $exists = true;
  $result->close();
}
else {
  log_db('auth.php 1: ' . $mysqli->error);
}
if (!$exists) {
  $password = bin2hex(openssl_random_pseudo_bytes(8));
  $password_hash =
    $mysqli->escape_string(password_hash($password, PASSWORD_DEFAULT));
  $system_group = $mysqli->escape_string(substitute('indieauth-group'));
  $query = 'INSERT INTO users (user, system_group, password, confirmed, ' .
    'registration_time) VALUES ("' . $domain . '", "' . $system_group . '", ' .
    '"' . $password_hash . '", 1, ' . time() . ')';
  if (!$mysqli->query($query)) {
    log_db('auth.php 2: ' . $mysqli->error);
  }
  $query = 'INSERT INTO group_names VALUES ' .
    '("admin", "' . $system_group . '", "' . $domain . '", 0)';
  if (!$mysqli->query($query)) {
    log_db('auth.php 3: ' . $mysqli->error);
  }
  mkdir('../' . $domain . '/public', 0755, true);
}
if (isset($_SESSION['access-token'])) {
  // Store the token endpoint for microsub.php.
  $token_endpoint = $mysqli->escape_string($_SESSION['token-endpoint']);
  $query = 'INSERT INTO settings VALUES ("' . $domain . '", "token", ' .
    '"endpoint", "' . $token_endpoint . '") ON DUPLICATE KEY UPDATE ' .
    'value = "' . $token_endpoint . '"';
  if (!$mysqli->query($query)) {
    log_db('auth.php 4: ' . $mysqli->error);
  }
  // Store or update the access token and micropub endpoint in case the session
  // is reset.
  $token = $mysqli->escape_string($_SESSION['access-token']);
  $query = 'INSERT INTO settings VALUES ("' . $domain . '", ' .
    '"micropub", "token", "' . $token . '") ON DUPLICATE KEY UPDATE ' .
    'value = "' . $token . '"';
  if (!$mysqli->query($query)) {
    log_db('auth.php 5: ' . $mysqli->error);
  }
  if (isset($_SESSION['micropub-endpoint'])) {
    $endpoint = $mysqli->escape_string($_SESSION['micropub-endpoint']);
    $query = 'INSERT INTO settings VALUES ("' . $domain . '", ' .
      '"micropub", "endpoint", "' . $endpoint . '") ON DUPLICATE KEY UPDATE ' .
      'value = "' . $endpoint . '"';
    if (!$mysqli->query($query)) {
      log_db('auth.php 6: ' . $mysqli->error);
    }
    // Also fetch and store the micropub config.
    $config = $mysqli->escape_string(micropub_config($endpoint, $token));
    $query = 'INSERT INTO settings VALUES ("' . $domain . '", ' .
      '"micropub", "config", "' . $config . '") ON DUPLICATE KEY UPDATE ' .
      'value = "' . $config . '"';
    if (!$mysqli->query($query)) {
      log_db('auth.php 7: ' . $mysqli->error);
    }
  }
}
else {
  // Try removing any existing access token or micropub endpoint in case it was
  // set previously and no longer used.
  $query = 'DELETE FROM settings WHERE user = "' . $domain . '" ' .
    'AND label = "micropub" ' .
    'AND (name = "token" OR name = "endpoint" OR name = "config")';
  if (!$mysqli->query($query)) {
    log_db('auth.php 8: ' . $mysqli->error);
  }
}
$mysqli->close();

$_SESSION['user'] = $domain;
$_SESSION['indieauth-login'] = true;
$user = new User();
$path = $user->config->FancyUrl() ? '/' : '/index.php?page=';
header('Location: ' . $url . $path . substitute('indieauth-page'));
