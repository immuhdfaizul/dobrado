<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['id', 'label', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name . ' not provided']);
    exit;
  }
}

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/install_module.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

list($page, $owner) = page_owner($_POST['url']);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) exit;

// Remove the '#dobrado-' prefix.
$id = (int)substr($_POST['id'], 9);
// Some module are created but not added to the modules table, they are given
// an id of 0.
if ($id === 0) {
  echo json_encode(['done' => true]);
  exit;
}

$mysqli = connect_db();
$label = $mysqli->escape_string($_POST['label']);
$module_page = '';
$box_order = 0;
$placement = '';

$module = new Module($user, $owner, $label);
if ($module->CanRemove($id)) {
  // Do module specific stuff first. (ie post module needs to remove
  // it's duplicate so that the correct page is found below).
  $module->Remove($id);
  $query = 'SELECT page, class, box_order, placement FROM modules WHERE ' .
    'user = "' . $owner . '" AND box_id = ' . $id . ' AND deleted = 0';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($module_list = $mysqli_result->fetch_assoc()) {
      // Store these values to update other box_order values below.
      $module_page = $module_list['page'];
      $class = $module_list['class'];
      $box_order = $module_list['box_order'];
      $placement = $module_list['placement'];
      $query = 'INSERT INTO modules_history VALUES ("' . $owner . '", ' .
        '"' . $module_page . '", ' . $id . ', "' . $label . '", ' .
        '"' . $class . '", ' . $box_order . ', "' . $placement . '", ' .
        '"delete", "' . $user->name . '", ' . time() . ')';
      if (!$mysqli->query($query)) {
        log_db('remove 1: ' . $mysqli->error, $owner, $user->name, $page);
      }
    }
    $mysqli_result->close();
  }
  else {
    log_db('remove 2: ' . $mysqli->error, $owner, $user->name, $page);
  }

  $query = 'DELETE FROM box_style WHERE (selector LIKE "#dobrado-' . $id . '" '.
    'OR selector LIKE "#dobrado-' . $id . ' %") AND user = "' . $owner . '"';
  if (!$mysqli->query($query)) {
    log_db('remove 3: ' . $mysqli->error, $owner, $user->name, $page);
  }

  // The post module is a special case. It gets removed later by cron, so that
  // readers have a chance to update their cache.
  if ($label !== 'post') {
    $query = 'UPDATE modules SET deleted = 1 WHERE box_id = ' . $id.
      ' AND user = "' . $owner . '"';
    if (!$mysqli->query($query)) {
      log_db('remove 4: ' . $mysqli->error, $owner, $user->name, $page);
    }
    // Update the box_order's in the group that have higher values.
    $query = 'REPLACE INTO modules (user, page, box_id, label, class, ' .
      'box_order, placement, deleted) SELECT user, page, box_id, label, ' .
      'class, box_order - 1, placement, deleted FROM modules WHERE ' .
      'user = "' . $owner . '" AND page = "' . $module_page . '" AND ' .
      'placement = "' . $placement . '" AND box_order > ' . $box_order .
      ' AND deleted = 0';
    if (!$mysqli->query($query)) {
      log_db('remove 5: ' . $mysqli->error, $owner, $user->name, $page);
    }
  }
  echo json_encode(['done' => true]);
}
else {
  echo json_encode(['error' => 'Could not remove module: \'' . $label . '\'']);
}

$mysqli->close();
