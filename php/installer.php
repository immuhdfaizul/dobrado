<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['label', 'version', 'title', 'description', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name . ' not provided']);
    exit;
  }
}

include 'functions/db.php';
include 'functions/install_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'config.php';
include 'user.php';

$mysqli = connect_db();
$label = $mysqli->escape_string(strtolower($_POST['label']));
$version = $mysqli->escape_string(htmlspecialchars($_POST['version']));
$title = $mysqli->escape_string(htmlspecialchars($_POST['title']));
$description = $mysqli->escape_string(htmlspecialchars($_POST['description']));
$display = (int)$_POST['display'];
$url = $mysqli->escape_string($_POST['url']);
$mysqli->close();

list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditSite) {
  echo json_encode(['error' => 'Permission denied installing module.']);
  exit;
}
if ($label === '') {
  echo json_encode(['error' => 'Please provide a module name.']);
  exit;
}
if (!preg_match('/^[a-z0-9_-]{1,50}$/', $label)) {
  echo json_encode(['error' => 'Invalid label name.']);
  exit;
}

$result = install_module($label, $version, $title, $description,
                         $display, $user);
// Let the client know the action completed.
if (isset($result['done']) || isset($result['error'])) {
  echo json_encode($result);
  exit;
}

if (!is_array($result)) {
  $error = 'Dependencies not listed for ' . $label . ' module.';
  echo json_encode(['error' => $error]);
  exit;
}

$error = 'Please also install the ';
$error .= count($result) === 1 ?
  $result[0] . ' module.' : join(', ', $result) . ' modules.';
echo json_encode(['error' => $error]);