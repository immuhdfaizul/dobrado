<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

check_confirm();

session_start();

if (isset($_POST['action'])) {
  echo json_encode(request($_POST['action']));
  exit;
}

echo '<!DOCTYPE html>' . "\n" .
  '<html>' . "\n" .
    '<head>' . "\n" .
      '<title>Dobrado deploy</title>' . "\n" .
      '<meta charset="utf-8">' . "\n" .
      '<meta name="viewport" content="width=device-width">' . "\n" .
      script() . css() .
    '</head>' . "\n" .
    '<body>' . "\n" .
      '<p>Thanks for choosing to install Dobrado!<br>' .
        'This page will check your server setup and install the software ' .
        'for you.</p> ' .
      progress() .
    '</body>' . "\n" .
  '</html>';

function account_form() {
  $content = '';
  $hide_form = '';
  if (isset($_GET['email']) && $_GET['email'] !== '') {
    $content = 'This website will be set up in single user mode and an email ' .
      'sent to <b>' . $_GET['email'] . '</b> containing a link to log in.';
    $hide_form = ' style="display:none;"';
  }
  // This form is returned each time the user clicks continue, so fill in
  // the details they have provided.
  $username = $_POST['username'] ?? 'admin';
  $password = $_POST['password'] ?? '';
  $email = $_POST['email'] ?? '';
  $content .= '<form id="deploy-account-form"' . $hide_form . '>' .
      '<button class="default-action">default</button>' .
      '<div class="form-spacing">' .
        '<label for="deploy-account-username">Username:</label>' .
        '<input type="text" id="deploy-account-username" ' .
          'value="' . $username . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="deploy-account-password">Password:</label>' .
        '<input type="text" id="deploy-account-password" ' .
          'value="' . $password . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="deploy-account-email">Email:</label>' .
        '<input type="text" id="deploy-account-email" value="' . $email . '">' .
      '</div>' .
    '</form>';
  return $content;
}

function check_confirm() {
  $filename = '/private/confirm-deploy.txt';
  if (file_exists($filename)) {
    if (!isset($_GET['confirm'])) {
      echo 'confirm parameter not set.';
      exit;
    }
    if ($handle = fopen($filename, 'r')) {
      $confirm = fread($handle, filesize($filename));
      fclose($handle);
      if ($_GET['confirm'] !== trim($confirm)) {
        echo 'confirm parameter does not match.';
        exit;
      }
    }
    else {
      echo 'could not read ' . $filename;
      exit;
    }
  }
  else if (basename(getcwd()) === 'php') {
    echo 'You can run this file from the php directory, but because this is ' .
      'the default location please use the confirm parameter. The value you ' .
      'use should be written to the file /private/confirm-deploy.txt';
    exit;
  }
}

function content($step, $status) {
  if ($step === 'transfer') {
    if ($status === 'completed') {
      return '<div id="deploy-transfer" class="completed">' .
        '<h2>Transfer Dobrado</h2>' .
        'Dobrado was transferred to your server successfully.</div>' . "\n";
    }
    return '<div id="deploy-transfer" class="' . $status . '">' .
      '<h2>Transfer Dobrado</h2>' .
      'The first step is to transfer a copy of Dobrado to your server.<br>' .
      'Click Start to check if you have a local copy, otherwise a copy will ' .
      'be transferred from <a href="https://dobrado.net">dobrado.net</a>.' .
      '<div class="info"></div></div>' . "\n";
  }
  if ($step === 'file-system') {
    if ($status === 'completed') {
      return '<div id="deploy-file-system" class="completed">' .
        '<h2>File system permissions</h2>' .
        'File system checks completed.</div>' . "\n";
    }
    return '<div id="deploy-file-system" class="' . $status . '">' .
      '<h2>File system permissions</h2>' .
      'Now that Dobrado has been transferred we need to check the file ' .
      'system permissions required by the software.<br>' .
      'Click Continue to check the directories Dobrado needs to use.' .
      '<div class="info"></div></div>' . "\n";
  }
  if ($step === 'database-config') {
    if ($status === 'completed') {
      return '<div id="deploy-database-config" class="completed">' .
        '<h2>Database settings</h2>' .
        'Your database settings has been saved.</div>' . "\n";
    }
    return '<div id="deploy-database-config" class="' . $status . '">' .
      '<h2>Database settings</h2>' .
      'Dobrado needs a MySQL database to run. Click Continue to enter your ' .
      'settings.<div class="info"></div></div>' . "\n";
  }
  if ($step === 'database-tables') {
    if ($status === 'completed') {
      return '<div id="deploy-database-tables" class="completed">' .
        '<h2>Database tables</h2>' .
        'Your database tables have been created.</div>' . "\n";
    }
    return '<div id="deploy-database-tables" class="' . $status . '">' .
      '<h2>Database tables</h2>' .
      'Now that your database settings have been saved we can try and ' .
      'connect. Click Continue to test the connection and create the ' .
      'required tables.<div class="info"></div></div>' . "\n";
  }
  if ($step === 'admin-account') {
    if ($status === 'completed') {
      return '<div id="deploy-admin-account" class="completed">' .
        '<h2>Admin account</h2>' .
        'Your admin account has been created.</div>' . "\n";
    }
    if (isset($_GET['email'])) {
      return '<div id="deploy-admin-account" class="' . $status . '">' .
        '<h2>Admin account</h2>' .
        'This step will create your admin account. Click Continue to ' .
        'use the email address provided as a url parameter.' .
        '<div class="info"></div></div>' . "\n";
    }
    return '<div id="deploy-admin-account" class="' . $status . '">' .
      '<h2>Admin account</h2>' .
      'This step will create your admin account. If you don\'t enter a ' .
      'password here, a one-time login link will be sent to the email ' .
      'address you provide. Click Continue to fill in your details. ' .
      '<div class="info"></div></div>' . "\n";
  }
  if ($step === 'finished') {
    return '<div id="deploy-finished" class="' . $status . '">' .
      '<h2>Finished</h2>' .
      'Well done setup is complete! Click the button below to visit your ' .
      'new site. You can log in using the username and password you ' .
      'provided, or please check your email for a login link.</div>' . "\n";
  }
}

function css() {
  return '<style type="text/css">body { font-family: Arial; }' . "\n" .
      '#deploy-progress .not-started { display: none; }' . "\n" .
      '#deploy-progress .error { border: 1px solid red; }' . "\n" .
      '.default-action { display: none; }' . "\n" .
      '.form-spacing { margin-top: 4px; }' . "\n" .
      '.form-spacing { margin-bottom: 4px; }' . "\n" .
      '.form-spacing { clear: both; }' . "\n" .
      '.form-spacing > input[type=text] { height: 25px; }' . "\n" .
      '.form-spacing > input[type=text] { width: 300px; }' . "\n" .
      '.form-spacing > label { float: left; }' . "\n" .
      '.form-spacing > label { margin-top: 0.3em; }' . "\n" .
      '.form-spacing > label { margin-right: 0.3em; }' . "\n" .
      '.form-spacing > label { text-align: right; }' . "\n" .
      '.form-spacing > label { width: 10em; }' . "\n" .
    '</style>' . "\n";
}

function database_form() {
  // This form is returned each time the user clicks continue, so fill in
  // the details they have provided.
  $server = $_POST['server'] ?? '';
  $username = $_POST['username'] ?? '';
  $password = $_POST['password'] ?? '';
  $name = $_POST['name'] ?? '';
  return '<form id="deploy-database-form">' .
      '<button class="default-action">default</button>' .
      '<div class="form-spacing">' .
        '<label for="deploy-database-server">Server:</label>' .
        '<input type="text" id="deploy-database-server" ' .
          'value="' . $server . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="deploy-database-username">Username:</label>' .
        '<input type="text" id="deploy-database-username" ' .
          'value="' . $username . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="deploy-database-password">Password:</label>' .
        '<input type="text" id="deploy-database-password" ' .
          'value="' . $password . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="deploy-database-name">Database Name:</label>' .
        '<input type="text" id="deploy-database-name" value="' . $name . '">' .
      '</div>' .
    '</form>';
}

function next_step() {
  foreach (step_list() as $index => $step) {
    if ($_SESSION['deploy-step'] === $step) {
      return step_list($index + 1);
    }
  }
}

function progress() {
  $start = step_list(0);
  if (!isset($_SESSION['deploy-step']) ||
      (isset($_GET['reset']) && $_GET['reset'] === '1'))  {
    $_SESSION['deploy-step'] = $start;
  }

  $content = '';
  $status = 'completed';
  $current_step = $_SESSION['deploy-step'];
  foreach (step_list() as $step) {
    if ($current_step === $step) {
      $content .= content($step, 'current');
      $status = 'not-started';
    }
    else {
      $content .= content($step, $status);
    }
  }
  $button_text = $current_step === $start ? 'Start' : 'Continue';
  return '<div id="deploy-progress">' . $content . '</div>' .
    '<button id="deploy-action">' . $button_text . '</button>';
}

function request($us_action) {
  // Assume deploy is being run from either the php directory or the root
  // directory for the website.
  $php = basename(getcwd()) === 'php';
  if ($us_action === 'deploy-transfer') {
    if ($php || is_dir('php')) {
      $_SESSION['deploy-step'] = next_step();
      return ['content' => 'Great it looks like you already have a copy of ' .
                           'Dobrado on your server.',
              'next' => '#deploy-' . $_SESSION['deploy-step']];
    }
    if (is_writeable('.')) {
      return ['content' => 'Transferring... please wait. This message will ' .
                           'update once the transfer is complete.'];
    }
    return ['error' => 'Transfer cannot start because your website directory ' .
                       'is not writable by the current user.<br> You can ' .
                       'click Start again once this has been updated.'];
  }

  if ($us_action === 'start-transfer') {
    set_time_limit(0);
    $file = file_get_contents('https://dobrado.net/public/dobrado.tgz');
    if (!$file) {
      return ['error' => 'There was a problem during transfer. Please click ' .
                         'Start to try again.'];
    }
    // This step is only done when deploy is run from the root directory, so
    // can assume the path to extract the tar file is correct.
    if ($handle = fopen('dobrado.tgz', 'w')) {
      fwrite($handle, $file);
      fclose($handle);
      exec('tar xzf dobrado.tgz');
      unlink('dobrado.tgz');
      $_SESSION['deploy-step'] = next_step();
      return ['content' => 'Done',
              'next' => '#deploy-' . $_SESSION['deploy-step']];
    }
    return ['error' => 'There was a problem opening the new file on your ' .
                       'server. Make sure the the directory is writable ' .
                       'and then click Start to try again.'];
  }

  if ($us_action === 'deploy-file-system') {
    $up_prefix = '';
    $down_prefix = '';
    if ($php) {
      if (!is_writeable('..')) {
        return ['error' => 'The parent directory is not writeable.'];
      }
      $up_prefix = '../';
    }
    else {
      if (!is_writeable('php')) {
        return ['error' => 'The php directory is not writeable.'];
      }
      $down_prefix = 'php/';
    }
    if (!is_writeable('.')) {
      return ['error' => 'The current directory is not writeable.'];
    }
    if (!is_dir($up_prefix . 'install')) mkdir($up_prefix . 'install');
    if (!is_writeable($up_prefix . 'install')) {
      return ['error' => 'The \'install\' directory is not writeable.'];
    }
    if (!is_dir($up_prefix . 'public')) mkdir($up_prefix . 'public');
    if (!is_writeable($up_prefix . 'public')) {
      return ['error' => 'The \'public\' directory is not writeable.'];
    }
    if (!is_writeable($up_prefix . 'js')) {
      return ['error' => 'The \'js\' directory is not writeable.'];
    }
    if (!is_writeable($up_prefix . 'js/source')) {
      return ['error' => 'The \'js/source\' directory is not writeable.'];
    }
    if (file_exists($up_prefix . 'site.css') &&
        !is_writeable($up_prefix . 'site.css')) {
      return ['error' => 'The file \'site.css\' is not writeable.'];
    }
    if (!is_writeable($down_prefix . 'functions')) {
      return ['error' => 'The \'functions\' directory is not writeable.'];
    }
    if (!is_writeable($down_prefix . 'modules')) {
      return ['error' => 'The \'modules\' directory is not writeable.'];
    }
    if (file_exists($down_prefix . 'instance.php') &&
        !is_writeable($down_prefix . 'instance.php')) {
      return ['error' => 'The file \'instance.php\' is not writeable.'];
    }
    // Create a cache directory for SimplePie.
    if (!is_dir($down_prefix . 'cache')) mkdir($down_prefix . 'cache');
    if (!is_writeable($down_prefix . 'cache')) {
      return ['error' => 'The \'cache\' directory is not writeable.'];
    }
    // Create a Serializer directory for HTMLPurifier.
    $serializer = 'library/HTMLPurifier/DefinitionCache/Serializer';
    if (!is_dir($down_prefix . $serializer)) mkdir($down_prefix . $serializer);
    if (!is_writeable($down_prefix . $serializer)) {
      return ['error' => 'The \'Serializer\' directory is not writeable.'];
    }
    $_SESSION['deploy-step'] = next_step();
    return ['content' => 'File system looks good!',
            'next' => '#deploy-' . $_SESSION['deploy-step']];
  }

  if ($us_action === 'deploy-database-config') {
    $prefix = $php ? '' : 'php/';
    if (file_exists($prefix . 'functions/db_config.php')) {
      $_SESSION['deploy-step'] = next_step();
      return ['content' => 'Nice work! Your database config file was found.',
              'next' => '#deploy-' . $_SESSION['deploy-step']];
    }
    return ['content' => database_form()];
  }

  if ($us_action === 'save-database-config') {
    $prefix = $php ? '' : 'php/';
    if (file_exists($prefix . 'functions/db_config.php')) {
      return ['error' => 'Database config file already exists.'];
    }
    if (!preg_match('/^[a-z0-9._-]{1,200}$/i', $_POST['server'])) {
      return ['error' => 'Server must contain only the characters: ' .
                         '<b>a-z 0-9 ._-</b> ' . database_form()];
    }
    if (!preg_match('/^[a-z0-9._-]{1,200}$/i', $_POST['username'])) {
      return ['error' => 'Username must contain only the characters: ' .
                         '<b>a-z 0-9 ._-</b> ' . database_form()];
    }
    if (!preg_match('/^[a-z0-9._-]{1,200}$/i', $_POST['password'])) {
      return ['error' => 'Password must contain only the characters: ' .
                         '<b>a-z 0-9 ._-</b> ' . database_form()];
    }
    if (!preg_match('/^[a-z0-9._-]{1,200}$/i', $_POST['name'])) {
      return ['error' => 'Database Name must contain only the ' .
                         'characters: <b>a-z 0-9 ._-</b> ' . database_form()];
    }
    if ($handle = fopen($prefix . 'functions/db_config.php', 'w')) {
      $config = '<?php ' . "\n\n" .
        '$db_server=\'' . $_POST['server'] . '\';' . "\n" .
        '$db_user=\'' . $_POST['username'] . '\';' . "\n" .
        '$db_password=\'' . $_POST['password'] . '\';' . "\n" .
        '$db_name=\'' . $_POST['name'] . '\';' . "\n";
      fwrite($handle, $config);
      fclose($handle);
      $_SESSION['deploy-step'] = next_step();
      return ['content' => 'Your database settings have been saved.',
              'next' => '#deploy-' . $_SESSION['deploy-step']];
    }
    return ['error' => 'There was a problem opening the config file to save ' .
                       'your settings. Please make sure the functions ' .
                       'directory is writable and then click Continue to try ' .
                       'again.'];
  }

  if ($us_action === 'deploy-database-tables') {
    if (!$php) set_include_path(get_include_path() . ':php');
    include 'functions/db_config.php';
    $mysqli = new mysqli($db_server, $db_user, $db_password, $db_name);
    if ($mysqli->connect_errno) {
      return ['error' => 'There seems to be a problem with the database ' .
                         'connection. MySQL said:<br>' .
                          $mysqli->connect_error];
    }
    $_SESSION['deploy-step'] = next_step();
    // Check if create_tables has already been run.
    if ($mysqli_result = $mysqli->query('SHOW TABLES')) {
      if ($mysqli_result->num_rows !== 0) {
        $mysqli_result->close();
        $mysqli->close();
        return ['content' => 'Great your database connection works! It also ' .
                             'looks like your tables have already been ' .
                             'created.',
                'next' => '#deploy-' . $_SESSION['deploy-step']];
      }
      $mysqli_result->close();
    }
    $mysqli->close();

    include 'functions/db.php';
    include 'functions/create.php';
    create_tables();
    return ['content' => 'Great your database connection works! Your tables ' .
                         'have now been created.',
            'next' => '#deploy-' . $_SESSION['deploy-step']];
  }

  if ($us_action === 'deploy-admin-account') {
    if (!$php) set_include_path(get_include_path() . ':php');
    include 'functions/db_config.php';
    $mysqli = new mysqli($db_server, $db_user, $db_password, $db_name);
    if ($mysqli->connect_errno) {
      return ['error' => 'There seems to be a problem with the database ' .
                         'connection. MySQL said:<br>' .
                          $mysqli->connect_error];
    }
    if ($mysqli_result = $mysqli->query('SELECT * FROM users')) {
      if ($mysqli_result->num_rows !== 0) {
        $mysqli_result->close();
        $mysqli->close();
        $_SESSION['deploy-step'] = next_step();
        return ['content' => 'You already have an account on this server.',
                'next' => '#deploy-' . $_SESSION['deploy-step']];
      }
      $mysqli_result->close();
    }
    $mysqli->close();
    return ['content' => account_form()];
  }

  if ($us_action === 'save-admin-account') {
    if (!preg_match('/^[a-z0-9_-]{1,50}$/i', $_POST['username'])) {
      return ['error' => 'Username must contain only the characters: ' .
                         '<b>a-z 0-9 _-</b> ' . account_form()];
    }

    $us_email = $_POST['email'];
    $us_password = $_POST['password'];
    // deploy can be called with the user's email address as a parameter to
    // automate the setup process.
    if (isset($_GET['email'])) $us_email = $_GET['email'];
    if ($us_password === '' && $us_email === '') {
      return ['error' => 'At least one of password or email must be provided ' .
                         'so that you can log in to your account. ' .
                         account_form()];
    }

    if (!$php) set_include_path(get_include_path() . ':php');

    include 'functions/db_config.php';
    $mysqli = new mysqli($db_server, $db_user, $db_password, $db_name);
    if ($mysqli->connect_errno) {
      return ['error' => 'There seems to be a problem with the database ' .
                         'connection. MySQL said:<br>' .
                          $mysqli->connect_error . account_form()];
    }

    $_SESSION['send-email'] = NULL;
    // If a password isn't given set a random one and email the user a link
    // to log in.
    if ($us_password === '') {
      $us_password = bin2hex(openssl_random_pseudo_bytes(8));
      $_SESSION['send-email'] = $us_email;
    }
    $password_hash = $mysqli->escape_string(password_hash($us_password,
                                                          PASSWORD_DEFAULT));
    $query = 'INSERT INTO users VALUES ("admin", "' . $password_hash . '", ' .
      '"' . $mysqli->escape_string($us_email) . '", "", 1, "", ' .
      time() . ', "", 1)';
    if (!$mysqli->query($query)) {
      $error = $mysqli->error;
      $mysqli->close();
      return ['error' => 'There was a problem creating your account. ' .
                         'MySQL said:<br>' . $error . account_form()];
    }

    include 'functions/create.php';
    include 'functions/db.php';
    include 'functions/new_module.php';
    include 'functions/permission.php';
    include 'functions/write_style.php';
    $default_modules =
      ['account' => ['title' => 'Account Menu', 'display' => 0],
       'control' => ['title' => 'Control Bar', 'display' => 0],
       'extended' => ['title' => 'Extended Editor', 'display' => 0],
       'login' => ['title' => 'Login Form', 'display' => 1],
       'more' => ['title' => '<b>more...</b>', 'display' => 0],
       'notification' => ['title' => 'Notifications','display' => 0],
       'organiser' => ['title' => 'Organiser', 'display' => 1],
       'simple' => ['title' => 'Text Area', 'display' => 1]];
    $get_prefix = $php ? '../' : '';
    $put_prefix = $php ? '../' : '';
    $php_prefix = $php ? '' : 'php/';
    create_default($get_prefix, $put_prefix, $php_prefix, $default_modules);
    // Add default site styles to the site_style table.
    create_site_style();
    // Write out site style information once the modules are installed.
    write_site_style($put_prefix);
    // If the user provides a custom username then their account is still saved
    // as 'admin' and their username preference saved in account settings.
    $username = $mysqli->escape_string(strtolower(trim($_POST['username'])));
    $_SESSION['username'] = $username;
    if ($username !== 'admin') {
      $query = 'INSERT INTO settings VALUES ("admin", "account", "username", ' .
        '"' . $username . '")';
      if (!$mysqli->query($query)) {
        log_db('deploy.php 1: ' . $mysqli->error);
      }
    }
    // The setting above requires single user mode, but when deploy is called
    // with an email address as a parameter, assume this setup process is
    // automated and that single user mode should also be set in this case.
    // The user should get a chance to set their preferred username when they
    // first log in.
    if ($username !== 'admin' || isset($_GET['email'])) {
      $query = 'INSERT INTO template VALUES ' .
        '("account-single-user", "", "true")';
      if (!$mysqli->query($query)) log_db('deploy.php 2: ' . $mysqli->error);
    }
    $_SESSION['deploy-step'] = next_step();
    return ['content' => 'Your account has been created. That\'s it you\'re ' .
                         'all done!',
            'next' => '#deploy-' . $_SESSION['deploy-step']];
  }

  if ($us_action === 'deploy-finished') {
    if (!$php) set_include_path(get_include_path() . ':php');
    // Allow for custom modifications per site. Note that this cannot be done
    // in the previous step because it needs to include module.php after
    // creating a new instance.php file, but that is already included by
    // create_default so a new request is required.
    $filename = '/private/custom_deploy.php';
    if (file_exists($filename)) {
      include 'functions/db.php';
      include $filename;
      if (function_exists('custom_deploy')) custom_deploy();
      else log_db('custom_deploy function not found in ' . $filename);
    }
    if (isset($_SESSION['send-email']) && $_SESSION['send-email'] !== '') {
      include 'functions/db_config.php';
      $mysqli = new mysqli($db_server, $db_user, $db_password, $db_name);
      if ($mysqli->connect_errno) {
        return ['error' => 'There seems to be a problem with the database ' .
                           'connection. MySQL said:<br>' .
                            $mysqli->connect_error . account_form()];
      }

      // A verification parameter can also be set, which is a random string sent
      // to the email address provided, which the user must set in a DNS TXT
      // record to prove that this email address is linked to the domain that
      // they want set up. It is checked in user.php if a code is also set.
      if (isset($_GET['verification']) && $_GET['verification'] !== '') {
        $verification = $mysqli->escape_string($_GET['verification']);
        $query = 'INSERT INTO settings VALUES ("admin", "login", ' .
          '"verification", "' . $verification . '")';
        if (!$mysqli->query($query)) log_db('deploy.php 3: ' . $mysqli->error);
      }
      $code = bin2hex(openssl_random_pseudo_bytes(8));
      $query = 'INSERT INTO settings VALUES ("admin", "login", "code", ' .
        '"' . $code . '")';
      if (!$mysqli->query($query)) log_db('deploy.php 4: ' . $mysqli->error);
      $username = $_SESSION['username'] ?? '';
      $server = $_SERVER['SERVER_NAME'];
      $message = substitute('new-user-message', '',
                            ['/!username/', '/!server/', '/!code/'],
                            [$username, $server, $code]);
      $subject = substitute('new-user-subject');
      $sender = substitute('new-user-sender', '', '/!host/', $server);
      $sender_name = substitute('new-user-sender-name');
      if ($sender_name === '') $sender_name = $sender;
      else $sender_name .= ' <' . $sender . '>';
      $headers = "MIME-Version: 1.0\r\n" .
        "Content-Type: text/html; charset=utf-8\r\n" .
        'From: ' . $sender_name . "\r\n";
      mail($_SESSION['send-email'], $subject, $message, $headers,
           '-f ' . $sender);
    }

    $_SESSION['deploy-step'] = NULL;
    $_SESSION['send-email'] = NULL;
    $_SESSION['username'] = NULL;
    // Remove this file if it was copied to the root directory.
    if (!$php && file_exists('deploy.php')) unlink('deploy.php');
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    return ['location' => $scheme . $server];
  }

  return ['error' => 'Unknown action'];
}

function script() {
  // Look for javascript relative to php directory, otherwise assume that this
  // file has been copied to the root directory for the website and needs to
  // rely on external dependencies.
  $url = file_exists('../js/deploy.js') ? '' : 'https://dobrado.net';

  if (isset($_GET['source']) && $_GET['source'] === '1') {
    return '<script type="text/javascript" ' .
        'src="' . $url . '/js/source/jquery.js"></script>' . "\n" .
      '<script type="text/javascript" ' .
        'src="' . $url . '/js/source/jquery-ui.js"></script>' . "\n" .
      '<script type="text/javascript" ' .
        'src="' . $url . '/js/source/lightbox.js"></script>' . "\n" .
      '<script type="text/javascript" ' .
        'src="' . $url . '/js/source/deploy.js"></script>' . "\n";
  }

  return '<script type="text/javascript" ' .
      'src="' . $url . '/js/3rdparty.js"></script>' . "\n" .
    '<script type="text/javascript" ' .
      'src="' . $url . '/js/deploy.js"></script>' . "\n";
}

function step_list($i = NULL) {
  $step_list = ['transfer', 'file-system', 'database-config',
                'database-tables', 'admin-account', 'finished'];
  if ($i === NULL) return $step_list;
  if ($i < count($step_list)) return $step_list[$i];
  return 'finished';
}