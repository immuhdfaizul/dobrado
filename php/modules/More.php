<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class More extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    $menu = '';
    $mysqli = connect_db();
    $query = 'SELECT label, version, title, description FROM ' .
      'installed_modules WHERE display = 1 ORDER BY label';
    if ($result = $mysqli->query($query)) {
      while ($installed_modules = $result->fetch_assoc()) {
        $label = $installed_modules['label'];
        $title = $installed_modules['title'];
        $module = new Module($this->user, $this->owner, $label);
        if ($module->CanAdd($this->user->page)) {
          if ($title === '') $title = $label;
          $menu .= '<a href="#" id="' . $label . '">' .$title. '</a><br>';
        }
      }
      $result->close();
    }
    else {
      $this->Log('More->Content: ' . $mysqli->error);
    }
    $mysqli->close();
    
    // Only use tabs if also displaying the install tab.
    if (!$this->user->canEditSite ||
        $this->Substitute('account-single-user') === 'true') {
      return '<div id="more-menu">' . $menu . '</div>';
    }
    return '<div id="more-tabs">' .
        '<ul>' .
          '<li><a href="#more-menu">Add to page</a></li>' .
          '<li><a href="#more-install">Install</a></li>' .
        '</ul>' .
      '<div id="more-menu">' . $menu . '</div>' .
      '<div id="more-install">' .
        '<form id="more-installer-form">' .
          '<div class="form-spacing">' .
            '<label for="more-module-label">Label:</label>' .
            '<input id="more-module-label" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="more-module-version">Version:</label>' .
            '<input id="more-module-version" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="more-module-title">Title:</label>' .
            '<input id="more-module-title" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="more-module-description">Description:</label>' .
            '<textarea id="more-module-description"></textarea>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input id="more-display-input" type="checkbox" ' .
              'checked="checked">' .
            '<label for="more-display-input">Display in menu</label>' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
      '</div>' .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.more.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.more.js', false);

    // This module is shown from javascript to avoid flash of unstyled content.
    $site_style = ['"", ".more", "display", "none"',
                   '"","#more-menu a","font-size","1.2em"',
                   '"","#more-menu a","line-height","1.2em"',
                   '"","#more-installer-form label","width","8em"',
                   '"","#more-installer-form .submit","margin-left","8.3em"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.more.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.more.js', false);
  }

}
