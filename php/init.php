<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/permission.php';
include 'functions/page_owner.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$attributes = ['list' => [], 'locked' => false];

// new-login is set when a user first logs in, it's used to clear local storage.
if (isset($_SESSION['new-login']) && $_SESSION['new-login'] === true) {
  unset($_SESSION['new-login']);
  $attributes['clear'] = true;
}

// reload-page is used by copy.php to get the browser to force a page reload
// to get updated css files.
if (isset($_SESSION['reload-page']) && $_SESSION['reload-page'] === true) {
  unset($_SESSION['reload-page']);
  $attributes['reload'] = true;
  echo json_encode($attributes);
  exit;
}

$mysqli = connect_db();
$url = isset($_POST['url']) ? $mysqli->escape_string($_POST['url']) : '';
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if ($user->canEditPage) {
  // The attributes list is used to assign labels as data to id's in jquery.
  $query = 'SELECT label, box_id FROM modules WHERE user = "'.$owner.'" AND '.
    '(page = "'.$page.'" OR page = "") AND deleted = 0';
  if ($result = $mysqli->query($query)) {
    while ($modules = $result->fetch_assoc()) {
      $attributes['list'][] = ['id' => '#dobrado-'.$modules['box_id'],
                               'label' => $modules['label']];
    }
    $result->close();
  }
  else {
    log_db('init: '.$mysqli->error, $owner, $user->name, $page);
  }
}
else {
  $attributes['locked'] = true;
}

$mysqli->close();

// Also allow tooltips to be shown to the user if they have a start module
// added to a page they own (which sets the 'start' session variable).
if (isset($_SESSION['start'])) {
  $start = new Module($user, $owner, 'start');
  if ($start->IsInstalled()) {
    $attributes['tooltip'] = $start->Factory('Tooltip');
  }
}

// The webaction handler is usually set manually, but the status webaction is
// provided here because it doesn't change based on user or page, and it means
// the user doesn't have to set it every time they log in to get the status
// set for actions on reader items.
$status = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
  'https://' : 'http://';
$status .= $_SERVER['SERVER_NAME'].'/php/status.php?{url}';
$attributes['status'] = $status;
// Some user settings are used by javascript.
$attributes['settings'] = (object)$user->settings;

echo json_encode($attributes);
