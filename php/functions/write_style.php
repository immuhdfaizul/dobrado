<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function write_file($filename, $content) {
  // Remove a file if all relevant style rules have been removed.
  if ($content === '') {
    if (file_exists($filename)) unlink($filename);
    return;
  }
  $handle = fopen($filename, 'w');
  if (!$handle) {
    log_db('Error opening file: ' . $filename);
    return;
  }
  fwrite($handle, $content);
  fclose($handle);
}

function style($selector, $rules) {
  if (empty($rules)) return;

  $style = $selector . " {\n";
  foreach ($rules as $property => $value) {
    if ($property === 'position' && $value === 'sticky') {
      $style .= "  position : -webkit-sticky;\n";
    }
    $style .= '  ' . $property . ' : ' . $value . ";\n";
  }
  $style .= "}\n";
  return $style;
}

function fetch_style($result) {
  $style = '';
  // Selectors using the 'hover' pseudo-class go at the end of each group.
  $hover_style = '';
  $media = '';
  $selector = '';
  $rules = array();
  while ($fetch_style = $result->fetch_assoc()) {
    if ($selector !== $fetch_style['selector'] ||
        $media !== $fetch_style['media']) {
      // The media query or current selector has changed, so format
      // the current style block for inclusion in a css file.
      if (preg_match('/a:hover$/', $selector)) {
        $hover_style .= style($selector, $rules);
      }
      else {
        $style .= style($selector, $rules);
      }
      // Once added to style, start the next selector and clear the rules array.
      $selector = $fetch_style['selector'];
      $rules = array();
    }
    $rules[$fetch_style['property']] = $fetch_style['value'];

    // Media queries wrap a group of selectors when present.
    // It's written out at the end of the loop because style is called
    // with the selector from the previous iteration of the loop.
    if ($media !== $fetch_style['media']) {
      // Group hover_style in with the current block before starting
      // the new media query.
      $style .= $hover_style;
      $hover_style = '';
      // Close the last media query group if it exists.
      if ($media !== '' && $style !== '') {
        $style .= "}\n";
      }
      $media = $fetch_style['media'];
      if ($media !== '') {
        $style .= $media . " {\n";
      }
    }
  }
  // Add the last set of rules and hover_style to style.
  $style .= style($selector, $rules);
  $style .= $hover_style;

  // Close the last media query group if it exists.
  if ($media !== '' && $style !== '') {
    $style .= "}\n";
  }
  return $style;
}

function write_site_style($prefix = '../') {
  $mysqli = connect_db();
  $query = 'SELECT media, selector, property, value FROM site_style ' .
    'ORDER BY media, selector, property';
  if ($result = $mysqli->query($query)) {
    write_file($prefix . 'site.css', fetch_style($result));
    $result->close();
  }
  else {
    log_db('write_site_style: ' . $mysqli->error);
  }
  $mysqli->close();
}

function write_page_style($user, $file) {
  $page = '';
  $matches = array();
  if (preg_match('/([^\/]+)\.css$/', $file, $matches)) {
    $page = $matches[1];
  }
  if ($page === '') return;

  $mysqli = connect_db();
  $query = 'SELECT media, selector, property, value FROM page_style WHERE ' .
    'user = "' . $user . '" AND name = "' . $page . '" ORDER BY media, ' .
    'selector, property';
  if ($result = $mysqli->query($query)) {
    write_file($file, fetch_style($result));
    $result->close();
  }
  else {
    log_db('write_page_style: ' . $mysqli->error, $user, '', $page);
  }
  $mysqli->close();
}

function write_box_style($user, $file) {
  $mysqli = connect_db();
  $query = 'SELECT media, selector, property, value FROM box_style WHERE ' .
    'user = "' . $user . '" ORDER BY media, selector, property';
  if ($result = $mysqli->query($query)) {
    write_file($file, fetch_style($result));
    $result->close();
  }
  else {
    log_db('write_box_style: ' . $mysqli->error, $user);
  }
  $mysqli->close();
}

function page_value($user, $page, $selector, $property, $media = '') {
  $value = '';
  $mysqli = connect_db();
  $query = 'SELECT value FROM page_style WHERE user = "' . $user . '" AND ' .
    'name = "' . $page . '" AND media = "' . $media . '" AND ' .
    'selector = "' . $selector . '" AND property = "' . $property . '"';
  if ($result = $mysqli->query($query)) {
    if ($page_style = $result->fetch_assoc()) {
      $value = $page_style['value'];
    }
    $result->close();
  }
  else {
    log_db('page_value: ' . $mysqli->error, $user, '', $page);
  }
  $mysqli->close();
  return $value;
}

function site_value($selector, $property, $media = '') {
  $value = '';
  $mysqli = connect_db();
  $query = 'SELECT value FROM site_style WHERE media = "' . $media . '" AND ' .
    'selector = "' . $selector . '" AND property = "' . $property . '"';
  if ($result = $mysqli->query($query)) {
    if ($site_style = $result->fetch_assoc()) {
      $value = $site_style['value'];
    }
    $result->close();
  }
  else {
    log_db('site_value: ' . $mysqli->error);
  }
  $mysqli->close();
  return $value;
}
