<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// NOTE: This is an optional function called by deploy.php, this file should be
// modified to create a custom install and then copied to the /private directory
// to be included by deploy.php.
function custom_deploy() {
  $php = basename(getcwd()) === 'php';
  // Start at root directory to transfer files.
  if ($php) chdir('..');

  set_time_limit(0);
  $values = '';
  // Version numbers should be kept up to date here so that the Autoupdate
  // module can check updates against the latest versions available.
  $autoupdate_version = 19;
  if (get_module('autoupdate', $autoupdate_version)) {
    $values .= '("admin", "autoupdate", "' . $autoupdate_version . '", ' .
      '"Automatic Updates", "Keep track of updates to Dobrado and install ' .
      'them automatically.", 1)';
  }
  $grid_version = 1;
  if (get_module('grid', $grid_version)) {
    if ($values !== '') $values .= ', ';
    $values .= '("admin", "grid", "' . $grid_version . '", "Grid", ' .
      '"Add a data grid to the page that uses the SlickGrid javascript ' .
      'library. It is designed to be used with other modules that look for a ' .
      'grid module on the page which they can use to display their data.", 1)';
  }
  $reader_version = 10;
  if (get_module('reader', $reader_version)) {
    if ($values !== '') $values .= ', ';
    $values .= '("admin", "reader", "' . $reader_version . '", "Reader", ' .
      '"Follow and respond to people and feeds you subscribe to.", 1)';
  }

  $mysqli = connect_db();
  if ($values !== '') {
    $query = 'INSERT INTO installed_modules VALUES ' . $values;
    if (!$mysqli->query($query)) {
      log_db('custom_deploy 1: ' . $mysqli->error);
    }
  }
  // Also update default config values set by create_default in create.php.
  $query = 'UPDATE config SET development_mode = 0, fancy_url = 1, ' .
    'guest_allowed = 0, secure = 1';
  if (!$mysqli->query($query)) {
    log_db('custom_deploy 2: ' . $mysqli->error);
  }
  $mysqli->close();

  $date = date(DATE_COOKIE);
  $write_3rdparty = false;
  // Move css files from the install directory, which are included with modules.
  if ($handle = opendir('install')) {
    while (($file = readdir($handle)) !== false) {
      if (preg_match('/.*\.css$/', $file)) {
        rename('install/' . $file, 'css/' . $file);
        $write_3rdparty = true;
      }
    }
    closedir($handle);
  }
  if ($write_3rdparty) {
    $handle = fopen('3rdparty.css', 'w');
    foreach (scandir('css') as $file) {
      if (preg_match('/.*\.css$/', $file)) {
        $content = '/* File: ' . $file . "\n" .
          ' * Added by: admin' . "\n" .
          ' * Date: ' . $date . "\n" .
          " */\n";
        $content .= file_get_contents('css/' . $file);
        $content .= "\n";
        fwrite($handle, $content);
      }
    }
    fclose($handle);
  }

  // Switch back to php directory for write_instance.
  chdir('php');
  include 'write_instance.php';
  write_instance();

  include 'functions/microformats.php';
  include 'functions/new_module.php';
  include 'functions/permission.php';
  include 'functions/write_style.php';
  // module.php must be included after generating the new instance.php.
  include 'module.php';
  include 'config.php';
  include 'user.php';

  $owner = 'admin';
  // permission.php checks if this session variable is set for site permission.
  $_SESSION['user'] = $owner;
  $user = new User($owner);
  $autoupdate = new Module($user, $owner, 'autoupdate');
  // IsInstalled checks that the new module was written to instance.php above,
  // the Install method is for module specific install commands, so it's ok to
  // use the two together. Want to add the Autoupdate module to a page for the
  // user so that they have access to other available modules.
  if ($autoupdate->IsInstalled()) {
    $autoupdate->Install('../js');
    $id = new_module($user, $owner, 'autoupdate', 'updates',
                     $autoupdate->Group(), $autoupdate->Placement());
    $autoupdate->Add($id);
  }
  else {
    log_db('custom_deploy 3: Autoupdate module not installed.');
  }
  $grid = new Module($user, $owner, 'grid');
  if ($grid->IsInstalled()) {
    $grid->Install('../js');
    // Autoupdate also asks for a grid module to be added to the same page.
    $id = new_module($user, $owner, 'grid', 'updates', $grid->Group(),
                     $grid->Placement());
    $grid->Add($id);
  }
  else {
    log_db('custom_deploy 4: Grid module not installed.');
  }
  $reader = new Module($user, $owner, 'reader');
  if ($reader->IsInstalled()) {
    $reader->Install('../js');
    // Add a reader module to subscribe to updates.
    $page = 'updates-feed';
    $user->SetPermission($page);
    $id = new_module($user, $owner, 'reader', $page, $reader->Group(),
                     $reader->Placement());
    $reader->Add($id);
    $reader->Factory('AddFeed', [$id, $reader->Substitute('autoupdate-all')]);
  }
  // Update site style information once modules are installed.
  write_site_style();
  $_SESSION['user'] = NULL;
  // If this function wasn't called from the php directory change back now.
  if (!$php) chdir('..');
}

function get_module($label, $version, $script = true) {
  $module = file_get_contents('https://dobrado.net/public/' .
                              'dobrado-' . $label . '-' . $version . '.tgz');
  if (!$module) {
    log_db('get_module 1: Could not transfer ' . $label . ' module.');
    return false;
  }
  if ($handle = fopen('dobrado-' . $label . '-' . $version . '.tgz', 'w')) {
    fwrite($handle, $module);
    fclose($handle);
    exec('tar xzf dobrado-' . $label . '-' . $version . '.tgz');
    unlink('dobrado-' . $label . '-' . $version . '.tgz');
  }
  if (file_exists('install/' . ucfirst($label) . '.php')) {
    copy('install/' . ucfirst($label) . '.php',
         'php/modules/' . ucfirst($label) . '.php');
  }
  else {
    log_db('get_module 2: ' . ucfirst($label) . '.php not found.');
    return false;
  }
  if ($script) {
    if (file_exists('install/dobrado.' . $label . '.js')) {
      copy('install/dobrado.' . $label . '.js', 'js/dobrado.' . $label . '.js');
    }
    else {
      log_db('get_module 3: ' . $label . '.js not found.');
      return false;
    }
    if (file_exists('install/dobrado.' . $label . '.source.js')) {
      copy('install/dobrado.' . $label . '.source.js',
           'js/source/dobrado.' . $label . '.js');
    }
    else {
      log_db('get_module 4: ' . $label . '.source.js not found.');
      return false;
    }
  }
  return true;
}