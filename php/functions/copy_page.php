<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function copy_page($current_page, $current_owner, $new_page, $new_owner,
                   $permission_page = '', $order = -1, $placement = '') {
  if ($current_page === '') {
    log_db('copy_page: current page not given.', $current_owner);
    return;
  }

  $user = new User();
  $mysqli = connect_db();

  if ($permission_page === '') {
    $permission_page = $current_page;
  }
  // Set the same user permissions as the permission_page which may not be
  // the same as the current page. (ie in the case of creating a new post
  // we want permissions from the page containing the writer module, not the
  // default posts page.) Also don't want to copy permission when the owner
  // has changed, as that may not be what the new owner wants.
  if ($new_owner === $current_owner) {
    // Don't need to copy permission where the new owner was a visitor.
    $query = 'INSERT INTO user_permission (user, page, visitor, edit, copy, ' .
      'view) SELECT "' . $new_owner . '", "' . $new_page . '", visitor, edit, '.
      'copy, view FROM user_permission WHERE user = "' . $current_owner . '" ' .
      'AND page = "' . $permission_page . '" AND ' .
      'visitor != "' . $new_owner . '" ON DUPLICATE KEY UPDATE ' .
      'edit = VALUES(edit), copy = VALUES(copy), view = VALUES(view)';
    if (!$mysqli->query($query)) {
      log_db('copy_page 1: ' . $mysqli->error, $current_owner, $user->name,
             $current_page);
    }
    // Set the same group permissions as the permission page.
    $query = 'INSERT INTO group_permission (user, page, name, edit, copy, ' .
      'view) SELECT "' . $new_owner . '", "' . $new_page . '", name, edit, ' .
      'copy, view FROM group_permission WHERE user = "' . $current_owner . '" '.
      'AND page = "' . $permission_page . '" ON DUPLICATE KEY UPDATE ' .
      'edit = VALUES(edit), copy = VALUES(copy), view = VALUES(view)';
    if (!$mysqli->query($query)) {
      log_db('copy_page 2: ' . $mysqli->error, $current_owner, $user->name,
             $current_page);
    }
  }

  // Set published the same as the permission page.
  $query = 'INSERT INTO published (user, page, published) SELECT ' .
    '"' . $new_owner . '", "' . $new_page . '", published FROM published ' .
    'WHERE user = "' . $current_owner . '" AND page = "' .$permission_page.'" '.
    'ON DUPLICATE KEY UPDATE published = VALUES(published)';
  if (!$mysqli->query($query)) {
    log_db('copy_page 3: ' . $mysqli->error, $current_owner, $user->name,
           $current_page);
  }

  // Get the first new unused id for calling each module's Copy method, but the
  // modules table itself will auto-increment new id's when modules are added.
  $id = 0;
  $query = 'SELECT MAX(box_id) AS box_id FROM modules WHERE ' .
    'user = "' . $new_owner . '"';
  if ($result = $mysqli->query($query)) {
    if ($module_list = $result->fetch_assoc()) {
      $id = (int)$module_list['box_id'] + 1;
    }
    $result->close();
  }
  else {
    log_db('copy_page 4: ' . $mysqli->error, $current_owner, $user->name,
           $current_page);
  }

  // Create new modules with auto-incremented box_id's. Note this has to be
  // done before module->Copy, which can also update the modules table but
  // requries the new box_id's to be in place first.
  $query = 'INSERT INTO modules (user, page, label, class, box_order, ' .
    'placement, deleted) SELECT "' . $new_owner . '", "' . $new_page . '", ' .
    'label, class, box_order, placement, deleted FROM modules ' .
    'WHERE user = "' . $current_owner . '" AND page = "' . $current_page . '" '.
    'AND deleted = 0 ORDER BY box_id';
  if (!$mysqli->query($query)) {
    log_db('copy_page 6: ' . $mysqli->error, $current_owner, $user->name,
           $current_page);
  }
  // Also copy to modules_history.
  $query = 'INSERT INTO modules_history (user, page, label, class, box_order, '.
    'placement, action, modified_by, timestamp) SELECT "' . $new_owner . '", ' .
    '"' . $new_page . '", label, class, box_order, placement, "add", ' .
    '"' . $user->name . '", ' . time() . ' FROM modules WHERE ' .
    'user = "' . $current_owner . '" AND page = "' . $current_page . '" ' .
    'AND deleted = 0 ORDER BY box_id';
  if (!$mysqli->query($query)) {
    log_db('copy_page 7: ' . $mysqli->error, $current_owner, $user->name,
           $current_page);
  }

  if ($order !== -1) {
    // Make room for a new module with box_order === $order by updating all
    // modules on the page with box_order >= $order in the same placement.
    $query = 'REPLACE INTO modules (user, page, box_id, label, class, ' .
      'box_order, placement, deleted) SELECT user, page, box_id, label, ' .
      'class, box_order + 1, placement, deleted FROM modules WHERE ' .
      'user = "' . $new_owner . '" AND page = "' . $new_page . '" AND ' .
      'box_order >= ' . $order . ' AND placement = "' . $placement . '" AND ' .
      'deleted = 0';
    if (!$mysqli->query($query)) {
      log_db('copy_page 8: ' . $mysqli->error, $current_owner, $user->name,
             $current_page);
    }
  }
  
  // Note that the current owner and page are still used here because the
  // old box_id's are required by the Copy method.
  $query = 'SELECT box_id, label FROM modules WHERE ' .
    'user = "' . $current_owner . '" AND page = "' . $current_page . '" ' .
    'AND deleted = 0 ORDER BY box_id';
  if ($result = $mysqli->query($query)) {
    // Set $user->page so that modules have access to the current page.
    $user->page = $current_page;
    while ($module_list = $result->fetch_assoc()) {
      $label = $module_list['label'];
      $module = new Module($user, $new_owner, $label);
      $module->Copy($id++, $new_page, $current_owner, $module_list['box_id']);
    }
    $result->close();
  }
  else {
    log_db('copy_page 5: ' . $mysqli->error, $current_owner, $user->name,
           $current_page);
  }

  // Write out a new style sheet after the modules have been copied.
  if ($new_owner === 'admin') write_box_style($new_owner, '../style.css');
  else write_box_style($new_owner, '../' . $new_owner . '/style.css');

  // Also copy the page style rules for the new page.
  $query = 'INSERT INTO page_style (user, name, media, selector, property, ' .
    'value) SELECT "' . $new_owner . '", "' . $new_page . '", media, ' .
    'selector, property, value FROM page_style WHERE ' .
    'user = "' . $current_owner . '" AND name = "' . $current_page . '" ' .
    'ON DUPLICATE KEY UPDATE value = VALUES(value)';
  if (!$mysqli->query($query)) {
    log_db('copy_page 9: ' . $mysqli->error, $current_owner, $user->name,
           $current_page);
  }

  if ($new_owner === 'admin') {
    write_page_style($new_owner, '../' . $new_page . '.css');
  }
  else {
    write_page_style($new_owner, '../' . $new_owner . '/' . $new_page . '.css');
  }

  $mysqli->close();

  // Return the page name for redirect.
  $url = $user->config->FancyUrl() ?
    '/' . $new_page : '/index.php?page=' . $new_page;
  return $new_owner === 'admin' ? $url : '/' . $new_owner . $url;
}
