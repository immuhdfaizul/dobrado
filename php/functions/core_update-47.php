<?php
// Dobrado Content Management System
// Copyright (C) 2020 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Note: This file is included by the Autoupdate module, but it should be copied
// and renamed using the version number of the core update you are building.
function core_update($user) {
  $site_style = ['"",".cke_autocomplete_panel img","width","20px"',
                 '"",".cke_autocomplete_panel img","float","left"',
                 '"",".cke_autocomplete_panel img","margin-right","5px"',
                 '"",".cke_autocomplete_panel .fullname","margin-left","5px"',
                 '"",".cke_autocomplete_panel .fullname","white-space",' .
                   '"nowrap"'];
  $values = '';
  foreach ($site_style as $style) {
    if ($values !== '') $values .= ',';
    $values .= '("admin",' . $style . ')';
  }

  $mysqli = connect_db();
  $query = 'INSERT INTO site_style VALUES ' . $values;
  if (!$mysqli->query($query)) {
    log_db('core_update 1: ' . $mysqli->error);
  }
  if (!$mysqli->query('UPDATE script_version SET dobrado = dobrado + 1')) {
    log_db('core_update 2: ' . $mysqli->error);
  }
  $mysqli->close();

  update_script($user);

  include_once 'functions/write_style.php';
  write_site_style();
}