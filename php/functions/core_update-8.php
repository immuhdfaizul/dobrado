<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Note: This file is included by the Autoupdate module, but it should be copied
// and renamed using the version number of the core update you are building.
function core_update() {
  $mysqli = connect_db();

  $query = 'CREATE TABLE IF NOT EXISTS script_version (' .
    'ckeditor INT UNSIGNED,' .
    '3rdparty INT UNSIGNED,' .
    'dobrado INT UNSIGNED' .
    ') ENGINE=MyISAM';
  if (!$mysqli->query($query)) {
    error_log('core_update 1: ' . $mysqli->error);
  }

  // Set script version numbers to zero so they can be updated later.
  $query = 'INSERT INTO script_version VALUES (0, 0, 0)';
  if (!$mysqli->query($query)) {
    log_db('core_update 2: ' . $mysqli->error);
  }
  $mysqli->close();
}