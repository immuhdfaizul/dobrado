<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (!isset($_GET['url'])) exit;

include 'functions/db.php';
include 'config.php';
include 'module.php';
include 'user.php';

session_start();

$origin = '*';
if (isset($_SERVER['HTTP_ORIGIN'])) {
  $origin = $_SERVER['HTTP_ORIGIN'];
}
else if (isset($_SERVER['HTTP_HOST'])) {
  $origin = $_SERVER['HTTP_HOST'];
}
header('Access-Control-Allow-Origin: ' . $origin);
header('Access-Control-Allow-Credentials: true');

$user = new User();
if ($user->loggedIn) {
  $post = new Module($user, $user->name, 'post');
  $reader = new Module($user, $user->name, 'reader');
  $post_installed = $post->IsInstalled();
  $reader_installed = $reader->IsInstalled();
  // Use the full url for permalinks because status config can be provided to
  // other sites which may choose to update web action urls.
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  if ($user->config->Secure()) $scheme = 'https://';
  $server_name = $user->config->ServerName();
  $me = $scheme . $server_name;
  if ($user->name !== 'admin') $me .= '/' . $user->name;

  $status = [];
  $us_url_list = is_array($_GET['url']) ? $_GET['url'] : [$_GET['url']];
  $mysqli = connect_db();
  foreach ($us_url_list as $us_url) {
    $url = $mysqli->escape_string($us_url);
    if (isset($status[$url])) continue;
    
    if ($post_installed) {
      $post_status = $post->Factory('Status', [$url, $me]);
      if (count($post_status) !== 0) {
        $status[$url] = $post_status;
      }
    }
    if ($reader_installed && $reader->Factory('Following', $url)) {
      if (isset($status[$url])) {
        $status[$url]['follow'] = $url;
      }
      else {
        $status[$url] = ['follow' => $url];
      }
    }
  }
  $mysqli->close();
  header('Content-Type: application/json');
  echo json_encode($status);
}
