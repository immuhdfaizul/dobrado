<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Page {

  public $name = '';
  public $owner = '';
  public $user = NULL;

  function __construct($user, $owner) {
    $this->user = $user;
    $this->owner = $owner;

    if (!isset($_GET['page']) || $_GET['page'] === '') {
      $this->name = 'index';
    }
    else if (preg_match('/^[a-z0-9_-]{1,200}$/i', $_GET['page'])) {
      $this->name = $_GET['page'];
    }
    else {
      $this->DefaultPage('index');
      return;
    }

    // When a user first logs in, redirect to their default page.
    if ($this->user->defaultPage) {
      $_SESSION['user'] = $this->user->name;
      $this->DefaultPage();
      return;
    }
    
    // When secure is true in config check the scheme and redirect if required.
    $http_only = !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === '';
    if ($http_only && $this->user->config->Secure()) {
      header('Location: ' . $this->Url($this->name, $this->owner, true));
      return;
    }

    if ($this->user->loggedIn) $_SESSION['user'] = $this->user->name;
    // If the user is not logged in then no permissions will be set, but the
    // current page is also stored with the user's details here.
    $this->user->SetPermission($this->name, $owner);

    if ($this->Permission()) $this->Display();
    else header('Location: ' . $this->Url($this->user->config->Unavailable()));
  }

  private function DefaultPage($name = '') {
    if ($name === '') $name = $this->user->config->LoginPage();
    // Override current scheme if secure is true in config.
    $secure = $this->user->config->Secure();
    // If not logged in user->name will be an empty string.
    if ($this->user->name === 'admin' || $this->user->name === '') {
      header('Location: ' . $this->Url($name, 'admin', $secure));
    }
    else {
      header('Location: ' . $this->Url($name, $this->user->name, $secure));
    }
  }

  private function Display() {
    $placement = '';
    $class = '';
    $previous_class = '';
    $post_feed = false;
    $comment_feed = false;
    $reader_feed = false;
    $not_modified = false;
    $include_scripts = [];
    $content = ['outside' => '', 'header' => '', 'left' => '',
                'right' => '', 'middle' => '', 'footer' => ''];
    $development_mode = $this->user->config->DevelopmentMode();

    $mysqli = connect_db();
    // Don't display content outside the layout if not logged in.
    $outside = $this->user->loggedIn ? '' : 'AND placement != "outside" ';
    $query = 'SELECT label, box_id, class, placement FROM modules WHERE ' .
      'user = "' . $this->owner . '" AND (page = "' . $this->name . '" OR ' .
      'page = "") AND deleted = 0 ' . $outside .
      'ORDER BY placement, box_order, box_id DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($module_list = $mysqli_result->fetch_assoc()) {
        // If placement has changed check if there was a div class for the
        // previous placement that needs closing.
        if ($previous_class !== '' &&
            $placement !== $module_list['placement']) {
          $content[$placement] .= '</div>';
          // Then reset previous_class for the new placement.
          $previous_class = '';
        }
        $label = $module_list['label'];
        if ($label === 'reader') {
          $reader_feed = true;
        }
        else if ($label === 'subscribe') {
          // Check not modified here, but note this is for the feed page
          // checked by the Subscribe module rather than the current page.
          $feed = 'index';
          if (isset($_GET['feed']) &&
              preg_match('/^[a-z0-9_-]{1,200}$/i', $_GET['feed'])) {
            $feed = $_GET['feed'];
          }
          if ($not_modified = $this->NotModified('feed', $feed)) break;
        }
        else if ($label === 'writer') {
          $post_feed = true;
          if ($not_modified = $this->NotModified('feed')) break;
        }
        else if ($label === 'commenteditor') {
          $comment_feed = true;
          if ($not_modified = $this->NotModified('comment')) break;
        }
        $id = (int)$module_list['box_id'];
        // $class is provided by the module when added, via module->Group().
        // It is used to create a wapper div for a group of modules.
        $class = $module_list['class'];
        $placement = $module_list['placement'];
        $module = new Module($this->user, $this->owner, $label);
        // Modules return false if they should not be displayed on a page.
        $data = $module->Content($id);
        if ($data !== false) {
          // Check if this module needs to be placed inside a div class,
          // or if a previous class needs closing.
          if ($class !== $previous_class) {
            if ($previous_class !== '') {
              $content[$placement] .= '</div>';
            }
            if ($class !== '') {
              if (!$post_feed && $label === 'post') {
                // Assume that this post is on a permalink page and add an
                // h-entry class so that comments are nested for mf2 parsing.
                $content[$placement] .= '<div class="' . $class . ' h-entry">';
              }
              else {
                $content[$placement] .= '<div class="' . $class . '">';
              }
            }
            $previous_class = $class;
          }
          $content[$placement] .= '<div class="' . $label . '" ' .
            'id="dobrado-' . $id . '">' . "\n  " . $data . "\n</div>\n";
          // In development mode check if this module has a javascript file.
          if ($development_mode &&
              $module->IncludeScript() && !in_array($label, $include_scripts)) {
            $include_scripts[] = $label;
          }
        }
      }
      // Make sure the last div class found is closed.
      if ($class !== '') {
        $content[$placement] .= '</div>';
      }
      $mysqli_result->close();
    }
    else {
      log_db('Page->Display: ' . $mysqli->error);
    }
    $mysqli->close();
    if ($not_modified) {
      header('HTTP/1.1 304 Not Modified');
      return;
    }

    $h_feed = '';
    $feed_links = '';
    if ($post_feed) {
      // WebSub headers are provided for the html version of the feed, which
      // requires the reader module to be installed (because it handles the
      // callbacks), but not on the current page because it also handles item
      // display. (The RSS version is still available in this case though.)
      if (!$reader_feed) {
        $reader = new Module($this->user, $this->owner, 'reader');
        if ($reader->IsInstalled()) {
          header('Link: <' . $this->Url($this->name, $this->owner) . '>; ' .
                 'rel="self"');
          header('Link: <' . $this->Url('/php/cloud.php') . '>; rel="hub"',
                 false);
        }
      }
      $h_feed = ' h-feed';
      $feed_links .= '<link rel="alternate" type="application/rss+xml" ' .
        'title="new posts" href="rss/index.php?page=' . $this->name . '">' .
        "\n";
    }
    if ($comment_feed) {
      $feed_links .= '<link rel="alternate" type="application/rss+xml" ' .
        'title="new comments" href="rss/index.php?page=' . $this->name .
        '&action=comment">' . "\n";
    }
    // When the reader module is on the current page link to the OPML version
    // of the current feed list.
    if ($reader_feed) {
      $feed_links .= '<link rel="outline" type="text/x-opml" ' .
        'title="feed list" href="rss/index.php?type=opml&page=' .
        $this->name . '">' . "\n";
    }

    header('X-Frame-Options: DENY');
    $title = $this->owner === 'admin' ?
      $this->name : $this->owner . '/' . $this->name;
    $title = $this->user->config->TitleIncludesPage() ?
      $title . ' - ' . $this->user->config->Title() :
      $this->user->config->Title();
    // Always create a token to send back with requests, but re-use the
    // current one if it exists so that user's can have multiple pages open.
    if (!isset($_SESSION['token'])) {
      $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(16));
    }

    echo '<!DOCTYPE html>' . "\n" .
      '<html>' . "\n" .
        '<head>' . "\n" .
          '<title>' . htmlspecialchars($title) . '</title>' . "\n" .
          '<meta charset="utf-8">' . "\n" .
          '<meta name="viewport" content="width=device-width">' . "\n" .
          $feed_links .
          $this->Stylesheets() .
          $this->Scripts($include_scripts, $development_mode) .
        '</head>' . "\n" .
        '<body>' . "\n" .
          $content['outside'] . "\n" .
          '<div class="main' . $h_feed . '">' . "\n" .
            '<div class="header ui-layout-north sortable">' . "\n" .
              $content['header'] . "\n" .
            '</div>' . "\n" .
            '<div class="left ui-layout-west sortable">' . "\n" .
              $content['left'] . "\n" .
            '</div>' . "\n" .
            '<div class="right ui-layout-east sortable">' . "\n" .
              $content['right'] . "\n" .
            '</div>' . "\n" .
            '<div class="middle ui-layout-center sortable">' . "\n" .
              $content['middle'] . "\n" .
            '</div>' . "\n" .
            '<div class="footer ui-layout-south sortable">' . "\n" .
              $content['footer'] . "\n" .
            '</div>' . "\n" .
          '</div>' . "\n" .
          '<div class="dobrado-token hidden" id="' . $_SESSION['token'] . '">' .
          '</div>' . "\n" .
          '<div class="dobrado-mobile"></div>' . "\n" .
          $this->user->config->Analytics() . "\n" .
        '</body>' . "\n" .
      '</html>';
  }

  private function NotModified($action, $page = '') {
    if ($this->user->loggedIn ||
        (isset($_SESSION['reload']) && $_SESSION['reload'] === true)) {
      unset($_SESSION['reload']);
      return false;
    }

    $timestamp = 0;
    if ($page === '') $page = $this->name;

    $mysqli = connect_db();
    $query = 'SELECT timestamp FROM page_updates WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $page . '" AND ' .
      'action = "' . $action . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($page_updates = $mysqli_result->fetch_assoc()) {
        $timestamp = (int)$page_updates['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      log_db('Page->NotModified: ' . $mysqli->error);
    }
    $mysqli->close();

    header('Cache-Control: private, must-revalidate');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $timestamp));
    // Always report that the page has been modified if the header isn't set.
    if (!isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) return false;
    return $timestamp <= strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
  }

  private function Permission() {
    if ($this->user->canEditPage ||
        $this->user->canCopyPage ||
        $this->user->canViewPage) {
      return true;
    }

    // Don't check permission for the page that user's are directed to
    // when permission is not granted for the referring page.
    if ($this->name === $this->user->config->Unavailable()) return true;

    $published_page = false;
    $mysqli = connect_db();
    $query = 'SELECT published FROM published WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $this->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($published = $mysqli_result->fetch_assoc()) {
        $published_page = $published['published'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      log_db('Page->Permission: ' . $mysqli->error);
    }
    $mysqli->close();
    return $published_page;
  }

  private function Scripts($include_scripts, $development_mode) {
    $scripts = '';
    if ($development_mode) {
      $scripts = '<script type="text/javascript" ' .
        'src="/js/ckeditor/source/ckeditor.js"></script>' . "\n";

      $include_3rdparty = ['jquery', 'jquery-ui', 'lightbox'];
      foreach ($include_3rdparty as $name) {
        $scripts .= '<script type="text/javascript" ' .
          'src="/js/source/' . $name . '.js"></script>' . "\n";
      }
      $scripts .= '<script type="text/javascript" ' .
        'src="/js/source/core.pub.js"></script>' . "\n";
      if ($this->user->loggedIn) {
        $scripts .= '<script type="text/javascript" ' .
          'src="/js/source/core.js"></script>' . "\n";
      }
      foreach ($include_scripts as $label) {
        $scripts .= '<script type="text/javascript" ' .
          'src="/js/source/dobrado.' . $label . '.js"></script>' . "\n";
      }
      return $scripts;
    }

    $mysqli = connect_db();
    $ckeditor = '/js/ckeditor/ckeditor.js';
    $thirdparty = '/js/3rdparty.js';
    $dobrado = $this->user->loggedIn ? '/js/dobrado.js' : '/js/dobrado.pub.js';
    $query = 'SELECT ckeditor, 3rdparty, dobrado FROM script_version';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($script_version = $mysqli_result->fetch_assoc()) {
        $ckeditor_version = (int)$script_version['ckeditor'];
        $thirdparty_version = (int)$script_version['3rdparty'];
        $dobrado_version = (int)$script_version['dobrado'];
        if ($ckeditor_version !== 0) $ckeditor .= '?v=' . $ckeditor_version;
        if ($thirdparty_version !== 0) {
          $thirdparty .= '?v=' . $thirdparty_version;
        }
        if ($dobrado_version != 0) $dobrado .= '?v=' . $dobrado_version;
      }
      $mysqli_result->close();
    }
    else {
      log_db('Page->Scripts: ' . $mysqli->error);
    }
    $mysqli->close();

    // ckeditor.js is only included when logged in. It's not included in
    // 3rdparty.js because it relies on it's own directory structure.
    if ($this->user->loggedIn) {
      $scripts = '<script type="text/javascript" src="' . $ckeditor . '">' .
        '</script>' . "\n";
    }
    return $scripts . '<script type="text/javascript" ' .
        'src="' . $thirdparty . '"></script>' . "\n" .
      '<script type="text/javascript" src="' . $dobrado . '"></script>' . "\n";
  }

  private function Stylesheets() {
    $stylesheets = '';
    $theme = $this->user->config->Theme();
    if ($this->owner === 'admin') {
      $stylesheets .= '<link rel="stylesheet" href="3rdparty.css">' . "\n" .
        '<link rel="stylesheet" href="themes/' . $theme . '/theme.css">' . "\n".
        '<link rel="stylesheet" href="site.css">' . "\n";
    }
    else {
      $stylesheets .= '<link rel="stylesheet" href="../3rdparty.css">' . "\n" .
        '<link rel="stylesheet" href="../themes/' . $theme .
          '/theme.css">' . "\n" .
        '<link rel="stylesheet" href="../site.css">' . "\n";
    }
    if (file_exists('style.css')) {
      $stylesheets .= '<link rel="stylesheet" href="style.css">' . "\n";
    }
    if (file_exists($this->name . '.css')) {
      $stylesheets .=
        '<link rel="stylesheet" href="' . $this->name . '.css">' . "\n";
    }
    return $stylesheets;
  }

  private function Url($name, $owner = 'admin', $secure = false) {
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    if ($secure) $scheme = 'https://';
    // Not using config ServerName here in case CNAME is being used.
    // Also if name is an absolute path just use whatever was passed in.
    if (strpos($name, '/') === 0) {
      return $scheme . $_SERVER['SERVER_NAME'] . $name;
    }

    $path = $this->user->config->FancyUrl() ? '/' : '/index.php?page=';
    if ($owner === 'admin') {
      return $scheme . $_SERVER['SERVER_NAME'] . $path . $name;
    }
    return $scheme . $_SERVER['SERVER_NAME'] . '/' . $owner . $path . $name;
  }

}
