<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function curl($url) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  curl_setopt($ch, CURLOPT_ENCODING, '');
  curl_setopt($ch, CURLOPT_HEADER, true);
  $response = curl_exec($ch);
  // Need to remove headers from the response as it can mess with the parser.
  // (Headers are included to replicate behaviour in functions/microformats.php)
  $body = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
  curl_close($ch);
  return Mf2\parse($body, $url);
}

if (!isset($_GET['url']) || $_GET['url'] === '') {
  echo '<!DOCTYPE html>' . "\n" .
    '<meta charset="utf-8">' . "\n" .
    '<meta name="viewport" content="width=device-width">' . "\n" .
    '<html><head><title>Microformats parser</title>' . "\n" .
    '<style>.form-spacing { margin: 4px; clear: both; }' .
      '.form-spacing > input[type=text] { height: 25px; width: 300px }' .
      '.form-spacing > input[type=checkbox] { height: 20px; }' .
      '.form-spacing > label { float: left; margin-top: 0.3em; ' .
      'margin-right: 0.3em; text-align: right; width: 6em; } ' .
      'button { margin-left: 7.1em; } ' .
    '</style></head><body>' . "\n" .
    '<p>Enter a url to parse microformats found at that address. Options:<ul>' .
      '<li><b>item</b> returns only the data at the selected index.</li>' .
      '<li><b>property</b> used with item returns only this property.</li>' .
      '<li><b>curl</b> fetch the contents before calling the parser.</li>' .
      '</ul></p>' .
    '<form method="get">' .
      '<div class="form-spacing">' .
        '<label>url:</label><input type="text" name="url">' .
      '</div> ' .
      '<div class="form-spacing">' .
        '<label>item:</label><input type="text" name="item">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label>property:</label><input type="text" name="property">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label>curl:</label><input type="checkbox" name="curl">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label>json:</label>' .
        '<input type="checkbox" name="json" checked="checked">' .
      '</div>' .
      '<button>submit</button>' .
    '</form></body></html>';
  exit;
}

include 'library/Masterminds/HTML5.auto.php';
include 'library/Mf2/Parser.php';
$mf = isset($_GET['curl']) ? curl($_GET['url']) : Mf2\fetch($_GET['url']);
if (isset($_GET['item']) && $_GET['item'] !== '') {
  $mf = $mf['items'][(int)$_GET['item']];
}
if (isset($_GET['property']) && $_GET['property'] !== '') {
  $mf = $mf['properties'][$_GET['property']];
}
if (isset($_GET['json']) && !in_array($_GET['json'], ['', '0', 'off'])) {
  header('Content-Type: application/json');
  echo json_encode($mf);
}
else {
  highlight_string("<?php\n\n" . print_r($mf, true));
}