<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['id', 'page', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) {
  $error = 'Could not edit the current page: Permission denied.';
  echo json_encode(['error' => $error]);
  $mysqli->close();
  exit;
}

$new_page = $mysqli->escape_string($_POST['page']);

if ($new_page === $page) {
  echo json_encode(['done' => false]);
  exit;
}

if (!can_edit_page($new_page)) {
  $error = 'Could not move to page: \''.$new_page.'\'.<br/>'.
    'Please make sure you have permission to edit that page.';
  echo json_encode(['error' => $error]);
  exit;
}

$id = $mysqli->escape_string($_POST['id']);

$query = 'UPDATE modules SET page = "'.$new_page.'" WHERE user = "'.$owner.'"'.
  ' AND box_id = '.$id;
if (!$mysqli->query($query)) {
  log_db('move 1: '.$mysqli->error, $owner, $user->name, $page);
}

$query = 'INSERT INTO modules_history (user, page, box_id, label, class, '.
  'box_order, placement, action, modified_by, timestamp) SELECT user, '.
  '"'.$page.'", box_id, label, class, box_order, placement, "move", '.
  '"'.$user->name.'", '.time().' FROM modules WHERE user = "'.$owner.'" AND '.
  'box_id = '.$id;
if (!$mysqli->query($query)) {
  log_db('move 2: '.$mysqli->error, $owner, $user->name, $page);
}

$mysqli->close();

echo json_encode(['done' => true]);
