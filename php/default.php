<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include '../php/functions/copy_page.php';
include '../php/functions/db.php';
include '../php/functions/new_user.php';
include '../php/functions/permission.php';
include '../php/functions/style.php';
include '../php/functions/write_style.php';

include '../php/config.php';
include '../php/module.php';
include '../php/page.php';
include '../php/user.php';

include 'owner.php';

session_start();

$user = new User();
// Need to create an object context here so that $this is defined.
new Page($user, $owner);
