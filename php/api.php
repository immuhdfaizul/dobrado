<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (!isset($_POST['id']) || !isset($_POST['key'])) {
  header('HTTP/1.1 401 Unauthorised');
  exit;
}

include 'functions/db.php';

$feed_id = (int)$_POST['id'];
$api_key = (int)$_POST['key'];

$mysqli = connect_db();
$query = 'SELECT user, label FROM api WHERE feed_id = '.$feed_id.
  ' AND api_key = '.$api_key;
if ($result = $mysqli->query($query)) {
  if ($api = $result->fetch_assoc()) {
    header('HTTP/1.1 200 OK');
  }
  else {
    header('HTTP/1.1 401 Unauthorised');
    $mysqli->close();
    exit;
  }
  $result->close();
}
else {
  log_db('api.php: '.$mysqli->error);
  header('HTTP/1.1 500 Internal Server Error');
  exit;
}

$mysqli->close();

include 'functions/permission.php';
include 'config.php';
include 'module.php';
include 'user.php';

$owner = $api['user'];
$user = new User($owner);
$module = new Module($user, $owner, $api['label']);
$module->Callback();
